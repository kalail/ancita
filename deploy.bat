set REPIP=192.168.5.105
set PTYPE=development

set REPACCESS=ssh://hg@%REPIP%/repos
set PROJECT_PATH=%~dp0
cd %PROJECT_PATH%
hg clone %REPACCESS%/pg_core/%PTYPE%/system system
hg clone %REPACCESS%/pg_core/%PTYPE%/temp temp
hg clone %REPACCESS%/pg_realestate_core/main/%PTYPE%/updates updates
hg clone %REPACCESS%/pg_realestate_core/main/%PTYPE%/application application

cd %PROJECT_PATH%/application
hg clone %REPACCESS%/pg_core/%PTYPE%/application/libraries libraries

cd %PROJECT_PATH%/application/modules
:: Common modules
hg clone %REPACCESS%/pg_core/%PTYPE%/application/modules/install install
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/ausers ausers
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/banners banners
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/contact contact
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/contact_us contact_us
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/content content
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/countries countries
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/cronjob cronjob
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/dynamic_blocks dynamic_blocks
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/export export
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/field_editor field_editor
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/file_uploads file_uploads
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/geomap geomap
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/get_token get_token
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/import import
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/languages languages
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/linker linker
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/listings listings
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/mail_list mail_list
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/mailbox mailbox
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/menu menu
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/mobile mobile
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/moderation moderation
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/news news
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/notifications notifications
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/payments payments
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/polls polls
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/properties properties
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/reviews reviews
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/seo seo
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/services services
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/site_map site_map
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/social_networking social_networking
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/spam spam
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/start start
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/subscriptions subscriptions
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/themes themes
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/upload_gallery upload_gallery
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/uploads uploads
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/users users
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/users_connections users_connections
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/users_payments users_payments
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/users_services users_services
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/video_uploads video_uploads
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/weather weather
hg clone %REPACCESS%/pg_realestate_core/modules/%PTYPE%/widgets widgets

cd %PROJECT_PATH%

echo "hg ci -A -m Init"
hg ci -A -m Init

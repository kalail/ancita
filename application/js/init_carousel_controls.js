function init_carousel_controls(optionArr){
	this.properties = {
		carousel_images_count: 5,
		carousel_total_images:10,
		btnNext: 'directionright',
		btnPrev: 'directionleft',
		rtl: false,
		offsetP : 0,
		offsetN: 0
	}

	var _self = this;


	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.update_controls(0);
	}
	
	this.update_controls = function(ind){
		if(!_self.properties.rtl) {
			var btnPrev = _self.properties.btnPrev;
			var btnNext = _self.properties.btnNext;
		}else{
			var btnPrev = _self.properties.btnNext;
			var btnNext = _self.properties.btnPrev;
		}
		
		if (ind == 0){
			$(btnPrev).removeClass('active');
			$(btnPrev).addClass('inactive');
		} else {
			$(btnPrev).addClass('active');
			$(btnPrev).removeClass('inactive');
		}

		if (ind + _self.properties.carousel_images_count >= _self.properties.carousel_total_images){
			$(btnNext).removeClass('active');
			$(btnNext).addClass('inactive');
		} else {
			$(btnNext).addClass('active');
			$(btnNext).removeClass('inactive');
		}
		
		if (_self.properties.carousel_images_count >= _self.properties.carousel_total_images){
			$(btnPrev).addClass('hide');
			$(btnNext).addClass('hide');
		}
	}
	
	_self.Init(optionArr);
}

function Errors(optionArr){
	this.properties = {
		errorBlockID: 'autogen_error_block',
		errorBlockWidth: '300px',
		showErrorTimeout: 3000,
		position: 'center', //// center, right
		dir: site_rtl_settings /// rtl
	}

	var _self = this;

	this.errors = {
	}

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.create_error_block();
	}

	this.extend_errors = function(errors){
		_self.errors = $.extend(_self.errors, errors);
	}

	this.create_error_block = function(){
		if(!$("#"+_self.properties.errorBlockID).attr("id")){
			$("body").append('<div id="'+_self.properties.errorBlockID+'"></div>');
			$("#"+_self.properties.errorBlockID).css('display', 'none');
			$("#"+_self.properties.errorBlockID).css('position', 'fixed');
			$("#"+_self.properties.errorBlockID).css('z-index', '1001');
			$("#"+_self.properties.errorBlockID).css('width', _self.properties.errorBlockWidth);
			$("#"+_self.properties.errorBlockID).attr('title', _self.errors.dblclick);
			$("#"+_self.properties.errorBlockID).bind('click', function(event){
//			$("#"+_self.properties.errorBlockID).bind('dblclick', function(event){
				_self.hide_error_block();
			});
		}
	}

	this.show_error_block = function(text, type){
		$("#"+_self.properties.errorBlockID).hide();

//		var top = $(window).scrollTop() + 10;
		switch(type){
			case 'error':
				var tpl = '<div class="ajax_notice"><div class="error">'+text+'</div></div>';
			break;
			case "success":
				var tpl = '<div class="ajax_notice"><div class="success">'+text+'</div></div>';
			break;
			case "info":
				var tpl = '<div class="ajax_notice"><div class="info">'+text+'</div></div>';
			break;
		}
		$("#"+_self.properties.errorBlockID).html(tpl);

		if(_self.properties.dir == 'ltr'){
			var posPropertyLeft = "left";
			var posPropertyRight = "right";
		}else{
			var posPropertyLeft = "right";
			var posPropertyRight = "left";
		}

		if(_self.properties.position == 'left'){
			$("#"+_self.properties.errorBlockID).css('top', '10px');
			$("#"+_self.properties.errorBlockID).css(posPropertyLeft, '10px');			
		}else if(_self.properties.position == 'center'){
			$("#"+_self.properties.errorBlockID).css('top', '50px');
			var left = ($(window).width() - $("#"+_self.properties.errorBlockID).width())/2;
			$("#"+_self.properties.errorBlockID).css(posPropertyLeft, left+'px');			
		}else if(_self.properties.position == 'right'){
			$("#"+_self.properties.errorBlockID).css('top', '10px');
			$("#"+_self.properties.errorBlockID).css(posPropertyRight, '10px');			
		}

		$("#"+_self.properties.errorBlockID).fadeIn('slow');

		setTimeout( function(){
			_self.hide_error_block();
		}, _self.properties.showErrorTimeout)

	}

	this.hide_error_block = function(){
		$("#"+_self.properties.errorBlockID).fadeOut('slow');
	}

	_self.Init(optionArr);

};

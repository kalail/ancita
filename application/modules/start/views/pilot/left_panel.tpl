<aside class="lc">
	<div class="inside account_menu">
	{if $user_session_data.user_type}
		{menu gid=`$user_session_data.user_type`'_account_menu' template='account_menu'}
	{else}
		{menu gid='user_footer_menu' template='account_menu'}
	{/if}
	
	{helper func_name=show_profile_info module=users}
	{helper func_name=show_banner_place module=banners func_param='big-left-banner'}
	{helper func_name=show_banner_place module=banners func_param='left-banner'}
	</div>
</aside>

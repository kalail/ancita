{l i='sort_by' gid=$sort_module}:
<select id="sorter-select-{$sort_rand}">
{foreach item=item key=key from=$sort_links}
<option value="{$key}"{if $key eq $sort_order} selected{/if}>{$item}</option>
{/foreach}
</select>
<button id="sorter-dir-{$sort_rand}" name="sorter_btn" class="sorter-btn"><ins class="fa fa-arrow-{if $sort_direction|lower eq 'asc'}up{else}down{/if}"></ins></button>

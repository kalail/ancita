<?php

$_permissions["admin_start"]["ajax_get_example"] = "3";
$_permissions["admin_start"]["date_formats"] = "3";
$_permissions["admin_start"]["error"] = "3";
$_permissions["admin_start"]["index"] = "1";
$_permissions["admin_start"]["lang_inline_editor"] = "3";
$_permissions["admin_start"]["menu"] = "3";
$_permissions["admin_start"]["mod_login"] = "3";
$_permissions["admin_start"]["settings"] = "3";

$_permissions["start"]["ajax_available_contact"] = "1";
$_permissions["start"]["demo"] = "1";
$_permissions["start"]["error"] = "1";
$_permissions["start"]["homepage"] = "2";
$_permissions["start"]["index"] = "1";
$_permissions["start"]["print_version"] = "1";
$_permissions["start"]["test_file_upload"] = "1";

$_permissions["api_start"]["get_search_tabs"] = "1";
$_permissions["api_start"]["get_search_data"] = "1";
$_permissions["api_start"]["get_languages"] = "1";
$_permissions["api_start"]["get_currencies"] = "1";

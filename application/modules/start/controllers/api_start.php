<?php

/**
 * Start api side controller
 *
 * @package PG_RealEstate
 * @subpackage Start
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Api_Start extends Controller
{
	/**
	 * Constructor
	 * 
	 * @return Api_Start
	 */
	public function Api_Start(){
		parent::Controller();
	}

	/**
	 * Return search tabs
	 * 
	 * @return void
	 */
	public function get_search_tabs(){
		$operations = array();
		
		if($this->pg_module->is_module_installed('listings')){
			$this->load->model('Listings_model');
			$operation_types = $this->Listings_model->get_operation_types(true);
			foreach($operation_types as $operation_type){
				$operations[] = array('module'=>'listings', 'type'=> $operation_type, 'name'=>l('header_'.$operation_type.'_listings', 'listings'));
			}
		}
		
		if($this->pg_module->is_module_installed('users')){
			$this->load->model('Users_model');
			$user_types = $this->Users_model->get_user_types();
			foreach($user_types as $user_type){
				$enabled = $this->pg_module->get_module_config('users', $user_type.'_search_enabled');
				if($enabled) $operations[] = array('module'=>'users', 'type'=> $user_type, 'name'=>l($user_type, 'users'));
			}
		}
		
		$this->set_api_content('operations', $operations);
	}
	
	/**
	 * Return language objects action
	 * 
	 * @return void
	 */
	public function get_languages(){
		$this->set_api_content('languages', $this->pg_language->languages);
		$this->set_api_content('current_lang', $this->pg_language->current_lang_id);
	}
	
	/**
	 * Return currency objects action
	 * 
	 * @return void
	 */
	public function get_currencies(){
		$currencies = array();
		if($this->pg_module->is_module_installed('payments')){
			$this->load->model('payments/models/Payment_currency_model');
			$currencies = $this->Payment_currency_model->get_currency_list();
		}
		$this->set_api_content('currencies', $currencies);
	}
}

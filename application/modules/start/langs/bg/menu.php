<?php

$install_lang["admin_menu_main_items"] = "Главно меню";
$install_lang["admin_menu_main_items_admin-home-item"] = "Начало";
$install_lang["admin_menu_main_items_admin-home-item_tooltip"] = "";
$install_lang["admin_menu_main_items_exp-import-items"] = "Експорт/Импорт";
$install_lang["admin_menu_main_items_exp-import-items_tooltip"] = "";
$install_lang["admin_menu_main_items_moderation-items"] = "Модерация";
$install_lang["admin_menu_main_items_moderation-items_tooltip"] = "";
$install_lang["admin_menu_main_items_tooltip"] = "";
$install_lang["admin_menu_other_items"] = "Модули";
$install_lang["admin_menu_other_items_admin-modules-item"] = "Инсталация на модули";
$install_lang["admin_menu_other_items_admin-modules-item_tooltip"] = "";
$install_lang["admin_menu_other_items_newsletters-items"] = "Разпространение";
$install_lang["admin_menu_other_items_newsletters-items_tooltip"] = "";
$install_lang["admin_menu_other_items_tooltip"] = "";
$install_lang["admin_menu_settings_items"] = "Настройки";
$install_lang["admin_menu_settings_items_content_items"] = "Съдържание";
$install_lang["admin_menu_settings_items_content_items_tooltip"] = "";
$install_lang["admin_menu_settings_items_feedbacks-items"] = "Обратна връзка";
$install_lang["admin_menu_settings_items_feedbacks-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_interface-items"] = "Интерфейс";
$install_lang["admin_menu_settings_items_interface-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_system-items"] = "Система";
$install_lang["admin_menu_settings_items_system-items_system-numerics-item"] = "Основни настройки";
$install_lang["admin_menu_settings_items_system-items_system-numerics-item_tooltip"] = "Брой артикули на страница и други настройки";
$install_lang["admin_menu_settings_items_system-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_tooltip"] = "";
$install_lang["admin_menu_settings_items_uploads-items"] = "Зареждане на файлове";
$install_lang["admin_menu_settings_items_uploads-items_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-overview-item"] = "Моят офис";
$install_lang["agent_top_menu_agent-main-overview-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-overview-item"] = "Моят офис";
$install_lang["company_top_menu_company-main-overview-item_tooltip"] = "";
$install_lang["private_top_menu_private-main-overview-item"] = "Моят офис";
$install_lang["private_top_menu_private-main-overview-item_tooltip"] = "";


<?php

$install_lang["header_get_started"] = "Commencer";
$install_lang["header_quick_start"] = "Statistiques rapides";
$install_lang["link_1"] = "Éditer information d'admin";
$install_lang["link_10"] = "Préparer blocs dynamiques";
$install_lang["link_11"] = "Éditer textes de site";
$install_lang["link_12"] = "Ajouter pages d'info";
$install_lang["link_2"] = "Ajouter serveur SMTP";
$install_lang["link_3"] = "Vérifier métadonnées de SEO";
$install_lang["link_4"] = "Initialiser cronjobs";
$install_lang["link_5"] = "Installer pays";
$install_lang["link_6"] = "Vérifier paramètres de téléchargement vidéo";
$install_lang["link_7"] = "Vérifier paramètres de modération";
$install_lang["link_8"] = "Ajouter filigrane";
$install_lang["link_9"] = "Vérifier paramètres de téléchargement de fichiers";
$install_lang["moderator_comment"] = "Vous êtes connectés en tant que modérateur. Vous avez un accès limité au panneau d'administration. Pour plus de permissions contactez un admin.";


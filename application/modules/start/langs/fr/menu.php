<?php

$install_lang["admin_menu_main_items"] = "Principal";
$install_lang["admin_menu_main_items_admin-home-item"] = "Accueil";
$install_lang["admin_menu_main_items_admin-home-item_tooltip"] = "";
$install_lang["admin_menu_main_items_exp-import-items"] = "Exporter/Importer";
$install_lang["admin_menu_main_items_exp-import-items_tooltip"] = "";
$install_lang["admin_menu_main_items_moderation-items"] = "Modération";
$install_lang["admin_menu_main_items_moderation-items_tooltip"] = "";
$install_lang["admin_menu_main_items_tooltip"] = "";
$install_lang["admin_menu_other_items"] = "Modules";
$install_lang["admin_menu_other_items_admin-modules-item"] = "Installation de modules";
$install_lang["admin_menu_other_items_admin-modules-item_tooltip"] = "";
$install_lang["admin_menu_other_items_newsletters-items"] = "Bulletins";
$install_lang["admin_menu_other_items_newsletters-items_tooltip"] = "";
$install_lang["admin_menu_other_items_tooltip"] = "";
$install_lang["admin_menu_settings_items"] = "Paramètres";
$install_lang["admin_menu_settings_items_content_items"] = "Contenu";
$install_lang["admin_menu_settings_items_content_items_tooltip"] = "";
$install_lang["admin_menu_settings_items_feedbacks-items"] = "Commentaires";
$install_lang["admin_menu_settings_items_feedbacks-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_interface-items"] = "Interface";
$install_lang["admin_menu_settings_items_interface-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_system-items"] = "Système";
$install_lang["admin_menu_settings_items_system-items_system-numerics-item"] = "Services généraux";
$install_lang["admin_menu_settings_items_system-items_system-numerics-item_tooltip"] = "Numéro d'objets sur une page et autres paramètres généraux";
$install_lang["admin_menu_settings_items_system-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_tooltip"] = "";
$install_lang["admin_menu_settings_items_uploads-items"] = "Téléchargements";
$install_lang["admin_menu_settings_items_uploads-items_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-overview-item"] = "Tableau de bord";
$install_lang["agent_top_menu_agent-main-overview-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-overview-item"] = "Tableau de bord";
$install_lang["company_top_menu_company-main-overview-item_tooltip"] = "";
$install_lang["private_top_menu_private-main-overview-item"] = "Tableau de bord";
$install_lang["private_top_menu_private-main-overview-item_tooltip"] = "";


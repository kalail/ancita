<?php

$install_lang["admin_menu_main_items"] = "Главная";
$install_lang["admin_menu_main_items_admin-home-item"] = "Домашняя страница";
$install_lang["admin_menu_main_items_admin-home-item_tooltip"] = "";
$install_lang["admin_menu_main_items_exp-import-items"] = "Экспорт/Импорт";
$install_lang["admin_menu_main_items_exp-import-items_tooltip"] = "";
$install_lang["admin_menu_main_items_moderation-items"] = "Модерация";
$install_lang["admin_menu_main_items_moderation-items_tooltip"] = "";
$install_lang["admin_menu_main_items_tooltip"] = "";
$install_lang["admin_menu_other_items"] = "Модули";
$install_lang["admin_menu_other_items_admin-modules-item"] = "Установка модулей";
$install_lang["admin_menu_other_items_admin-modules-item_tooltip"] = "";
$install_lang["admin_menu_other_items_newsletters-items"] = "Рассылки";
$install_lang["admin_menu_other_items_newsletters-items_tooltip"] = "";
$install_lang["admin_menu_other_items_tooltip"] = "";
$install_lang["admin_menu_settings_items"] = "Настройки";
$install_lang["admin_menu_settings_items_content_items"] = "Контент";
$install_lang["admin_menu_settings_items_content_items_tooltip"] = "";
$install_lang["admin_menu_settings_items_feedbacks-items"] = "Обратная связь";
$install_lang["admin_menu_settings_items_feedbacks-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_interface-items"] = "Интерфейс";
$install_lang["admin_menu_settings_items_interface-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_system-items"] = "Система";
$install_lang["admin_menu_settings_items_system-items_system-numerics-item"] = "Прочие настройки";
$install_lang["admin_menu_settings_items_system-items_system-numerics-item_tooltip"] = "Количество элементов на страницу и прочие настройки";
$install_lang["admin_menu_settings_items_system-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_tooltip"] = "";
$install_lang["admin_menu_settings_items_uploads-items"] = "Загрузки";
$install_lang["admin_menu_settings_items_uploads-items_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-overview-item"] = "Личный кабинет";
$install_lang["agent_top_menu_agent-main-overview-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-overview-item"] = "Личный кабинет";
$install_lang["company_top_menu_company-main-overview-item_tooltip"] = "";
$install_lang["private_top_menu_private-main-overview-item"] = "Личный кабинет";
$install_lang["private_top_menu_private-main-overview-item_tooltip"] = "";


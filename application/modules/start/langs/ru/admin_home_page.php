<?php

$install_lang["header_get_started"] = "Начало работы";
$install_lang["header_quick_start"] = "Статистика";
$install_lang["link_1"] = "Редактировать данные администратора";
$install_lang["link_10"] = "Добавить динамические блоки";
$install_lang["link_11"] = "Редактировать тексты сайта";
$install_lang["link_12"] = "Добавить информационные страницы";
$install_lang["link_2"] = "Настроить SMTP серверы";
$install_lang["link_3"] = "Проверить SEO настройки";
$install_lang["link_4"] = "Настроить запланированные задачи";
$install_lang["link_5"] = "Установить страны";
$install_lang["link_6"] = "Настроить загрузку видео файлов";
$install_lang["link_7"] = "Настроить модерацию";
$install_lang["link_8"] = "Настроить водяной знак";
$install_lang["link_9"] = "Настроить загрузку файлов";
$install_lang["moderator_comment"] = "Вы авторизованы как модератор. У вас ограниченный доступ к разделам панели администратора. Чтобы расширить свои полномочия, обратитесь к администратору сайта.";


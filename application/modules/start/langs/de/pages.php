<?php

$install_lang["add_funds_to_my_account"] = "Geld zum Konto hinzufügen";
$install_lang["admin_header_numerics_change"] = "Seitennumerik ändern";
$install_lang["applications"] = "Anwendungen";
$install_lang["arrow_down"] = "&darr;";
$install_lang["arrow_up"] = "&uarr;";
$install_lang["available_brousers_text1"] = "Sie verwenden eine veraltete Browser-Version.";
$install_lang["available_brousers_text2"] = "Bitte aktualisieren Sie auf einen modernen Webbrowser für bessere Erfahrung.";
$install_lang["banners_items_per_page_field"] = "Banners: items per page";
$install_lang["banners_settings_module"] = "Banners settings";
$install_lang["btn_activate"] = "Aktivieren";
$install_lang["btn_add"] = "Hinzufügen";
$install_lang["btn_add_to"] = "Hinzufügen zu";
$install_lang["btn_apply"] = "anwenden";
$install_lang["btn_approve"] = "genehmigen";
$install_lang["btn_back"] = "zurück";
$install_lang["btn_cancel"] = "zurück";
$install_lang["btn_change"] = "ändern";
$install_lang["btn_clear"] = "zurücksetzen";
$install_lang["btn_close"] = "schließen";
$install_lang["btn_create"] = "erstellen";
$install_lang["btn_decline"] = "ablehnen";
$install_lang["btn_delete"] = "entfernen";
$install_lang["btn_edit"] = "bearbeiten";
$install_lang["btn_login"] = "anmelden";
$install_lang["btn_logout"] = "abmelden";
$install_lang["btn_ok"] = "Ok";
$install_lang["btn_preview"] = "Vorschau";
$install_lang["btn_refresh"] = "aktualisieren";
$install_lang["btn_register"] = "registrieren";
$install_lang["btn_reply"] = "antworten";
$install_lang["btn_restore"] = "wiederherstellen";
$install_lang["btn_save"] = "speichern";
$install_lang["btn_saving"] = "Speichert...";
$install_lang["btn_search"] = "Suche";
$install_lang["btn_send"] = "senden";
$install_lang["btn_show"] = "zeigen";
$install_lang["change_my_pass"] = "Passwort ändern";
$install_lang["contact_admin"] = "Administration kontaktieren";
$install_lang["copyright"] = "";
$install_lang["countries_output_address_format_field"] = "Vollständiges Adressen Ausgabeformat";
$install_lang["countries_output_city_format_field"] = "Stadt Ausgabeformat";
$install_lang["countries_output_country_format_field"] = "Land Ausgabeformat";
$install_lang["countries_output_district_format_field"] = "District output format";
$install_lang["countries_output_region_format_field"] = "Region Ausgabeformat";
$install_lang["countries_settings_module"] = "Ländereinstellungen";
$install_lang["countries_use_district_field"] = "Use district";
$install_lang["date_formats_settings_module"] = "Date format";
$install_lang["date_formats_used_in"] = "Used in";
$install_lang["date_formats_date_numeric_field"] = "Date numeric";
$install_lang["date_formats_date_numeric_description"] = "nowhere";
$install_lang["date_formats_date_literal_field"] = "Date literal";
$install_lang["date_formats_date_literal_description"] = "primarily in user mode ";
$install_lang["date_formats_date_time_numeric_field"] = "Datetime numeric";
$install_lang["date_formats_date_time_numeric_description"] = "admin mode";
$install_lang["date_formats_date_time_literal_field"] = "Datetime literal";
$install_lang["date_formats_date_time_literal_description"] = "primarily in admin mode";
$install_lang["date_formats_time_numeric_field"] = "Time numeric";
$install_lang["date_formats_time_numeric_description"] = "nowhere";
$install_lang["date_formats_time_literal_field"] = "Time literal";
$install_lang["date_formats_time_literal_description"] = "nowhere";
$install_lang["date_format_year"] = "Year";
$install_lang["date_format_year_2"] = "Two digits";
$install_lang["date_format_year_4"] = "Four digits";
$install_lang["date_format_month"] = "Month";
$install_lang["date_format_month_without_zero"] = "Number without zero";
$install_lang["date_format_month_with_zero"] = "Number with zero";
$install_lang["date_format_month_short"] = "Short";
$install_lang["date_format_month_full"] = "Full";
$install_lang["date_format_day"] = "Day";
$install_lang["date_format_day_without_zero"] = "Number without zero";
$install_lang["date_format_day_with_zero"] = "Number with zero";
$install_lang["date_format_week_day"] = "Day of the week";
$install_lang["date_format_week_day_short"] = "Short";
$install_lang["date_format_week_day_full"] = "Long";
$install_lang["date_format_hours"] = "Hours";
$install_lang["date_format_hours_24_with_zero"] = "24h with zero";
$install_lang["date_format_hours_24_without_zero"] = "24h without zero";
$install_lang["date_format_hours_12_with_zero"] = "12h with zero";
$install_lang["date_format_hours_12_without_zero"] = "12h without zero";
$install_lang["date_format_minutes"] = "Minutes";
$install_lang["date_format_minutes_without_zero"] = "Number without zero";
$install_lang["date_format_minutes_with_zero"] = "Number with zero";
$install_lang["date_format_seconds"] = "Seconds";
$install_lang["date_format_seconds_without_zero"] = "Number without zero";
$install_lang["date_format_seconds_with_zero"] = "Number with zero";
$install_lang["edit_my_profile"] = "Profil bearbeiten";
$install_lang["error_demo_mode"] = "Einige Funktionen sind in der Online-Demo aus Sicherheitsgründen deaktiviert. <br>Servertestverstion steht zum Testen zur Verfügung. <a href=\"http://pilotgroup.zendesk.com\">Kontaktieren Sie uns</a> für weitere Informationen.";
$install_lang["error_empty_email"] = "Leere E-Mail";
$install_lang["error_invalid_email"] = "Ungültige E-Mail";
$install_lang["error_numerics_empty"] = "Leerer Wert: [field]";
$install_lang["error_product_key_incorrect"] = "Falscher oder leerer Bestellkey";
$install_lang["example"] = "Example";
$install_lang["field_login"] = "Einloggen";
$install_lang["field_order_key"] = "Bestellkey";
$install_lang["field_password"] = "Passwort";
$install_lang["filter_header"] = "Filter";
$install_lang["find_buy_title"] = "Zu kaufen";
$install_lang["find_lease_title"] = "Zu leasen";
$install_lang["find_rent_title"] = "Zu vermieten";
$install_lang["find_sale_title"] = "Zu verkaufen";
$install_lang["find_sold_title"] = "kürzlich verkauft";
$install_lang["header_actions"] = "Aktionen";
$install_lang["header_activity"] = "Aktivität";
$install_lang["header_admin_homepage"] = "Willkommen | Admin Home";
$install_lang["header_error"] = "404 Seite nicht gefunden";
$install_lang["header_featured_employers"] = "";
$install_lang["header_homepage"] = "Übersicht";
$install_lang["header_inline_lang_edit"] = "Übersetzen";
$install_lang["header_modinstaller_login"] = "Bei Modulmanagement einloggen";
$install_lang["header_my_statistics"] = "Meine Statistiken";
$install_lang["header_settings_numerics_list"] = "allgemeine Einstellungen";
$install_lang["header_start_page"] = "Homepage";
$install_lang["lang_editor"] = "Sprach Editor";
$install_lang["latest_added_listings_header"] = "neueste Angebote";
$install_lang["link_edit_date_format"] = "Edit";
$install_lang["link_get_help"] = "Hilfe";
$install_lang["link_less_options"] = "weniger Optionen";
$install_lang["link_more_options"] = "mehr Optionen";
$install_lang["listings"] = "Angebote";
$install_lang["listings_admin_moderation_emails_field"] = "E-Mails";
$install_lang["listings_default_activation_period_field"] = "Standardaktivierungszeitraum (in Tagen)";
$install_lang["listings_for_buy_enabled_field"] = "Erlaube Posten von Angebtoen `Zum Kauf`";
$install_lang["listings_for_lease_enabled_field"] = "Erlaube Posten von Angeboten `Zum Leasen`";
$install_lang["listings_for_rent_enabled_field"] = "Erlaube Posten von Angeboten `Zum Mieten`";
$install_lang["listings_for_sale_enabled_field"] = "Erlaube Posten von Angeboten `Zum Verkauf`";
$install_lang["listings_items_per_page_field"] = "Artikel pro Seite (in Suchergebnissen, etc.)";
$install_lang["listings_moderation_send_mail_field"] = "Sende Moderationsbenachrichtigungen an E-Mailadresse";
$install_lang["listings_rows_per_page_field"] = "Angebote: Anzahl der Reihen mit Angeboten in empfohlenen Immobilien";
$install_lang["listings_settings_module"] = "Angebotseinstellungen";
$install_lang["listings_show_mortgage_calc_field"] = "Aktiviere Hypothekenrechner";
$install_lang["listings_similar_items_field"] = "Max. Anzahl von Angeboten in `Das könnte Sie auch interessieren...` Block";
$install_lang["listings_slide_show_images_field"] = "Anzahl der Fotos die in dem Diashowservice verwedet werden";
$install_lang["listings_slider_auto_field"] = "Auto Rotation für Angebote im Slider";
$install_lang["listings_slider_min_height_field"] = "Min Höhe von Slider Fotos in Pixeln";
$install_lang["listings_slider_min_width_field"] = "Min Breite von Slider Fotos in Pixeln";
$install_lang["listings_slider_rotation_field"] = "Zeit der Rotation für Angebote im Slider in Sek.";
$install_lang["listings_use_map_in_search_field"] = "Aktiviere Karte auf Angebotssuchseite";
$install_lang["listings_use_moderation_field"] = "Aktiviere Angebotsmoderation";
$install_lang["listings_use_poll_in_search_field"] = "Aktiviere Umfrage auf Suchseite";
$install_lang["mailbox_items_per_page_field"] = "Mailbox: Kommunikation pro Seite";
$install_lang["mailbox_settings_module"] = "Mailbox";
$install_lang["make_active"] = "Aktivieren";
$install_lang["make_inactive"] = "Deaktivieren";
$install_lang["max_photo_hint"] = "Maximum";
$install_lang["mod_installer_admin_login_text"] = "Einloggen erforderlich.<br>Finde Einloggen & Passwort in <b>application/config/install.php file</b>";
$install_lang["my_contacts"] = "Meine Nachrichten";
$install_lang["my_listings"] = "Meine Angebote";
$install_lang["nav_first"] = "Erste";
$install_lang["nav_last"] = "Letzte";
$install_lang["nav_next"] = "Nächste";
$install_lang["nav_prev"] = "Vorherige";
$install_lang["nav_user_first"] = "‹ Erste";
$install_lang["nav_user_last"] = "Letzte ›";
$install_lang["nav_user_next"] = "›";
$install_lang["nav_user_prev"] = "‹";
$install_lang["new_messages"] = "Neue Nachrichten";
$install_lang["news_settings_module"] = "Nachrichten";
$install_lang["news_userside_items_per_page_field"] = "Nachrichten: Nachrichtenartikel pro Seite";
$install_lang["no_date_formats"] = "No date formats";
$install_lang["no_information"] = "keine Information";
$install_lang["no_str"] = "Nein";
$install_lang["option_checkbox_no"] = "Nein";
$install_lang["option_checkbox_yes"] = "Ja";
$install_lang["overflow_settings_module"] = "Überlauf";
$install_lang["payments_items_per_page_field"] = "Zahlungen: Zahlungseinträge pro Seite";
$install_lang["payments_settings_module"] = "Zahlungsgeschichte";
$install_lang["post_buy_title"] = "`Zum Kauf` hinzufügen";
$install_lang["post_sale_title"] = "`Zum Verkauf` hinzufügen";
$install_lang["saved_listings"] = "gespeicherte Angebote";
$install_lang["saved_searches"] = "gespeicherte Suchen";
$install_lang["select_all"] = "alle überprüfen";
$install_lang["select_default"] = "alle";
$install_lang["sett_banners_item"] = "Banners";
$install_lang["sett_countries_item"] = "Länder";
$install_lang["sett_date_formats_item"] = "Date format";
$install_lang["sett_formats_item"] = "Formate";
$install_lang["sett_listings_item"] = "Angebote";
$install_lang["sett_mailbox_item"] = "Mailbox";
$install_lang["sett_news_item"] = "Nachrichten";
$install_lang["sett_numerics_item"] = "Nummerierung";
$install_lang["sett_overview_item"] = "Überblick";
$install_lang["sett_payments_item"] = "Zahlungen";
$install_lang["sett_start_item"] = "Standards";
$install_lang["sett_widgets_item"] = "Widgets";
$install_lang["showing"] = "Zeigen";
$install_lang["sort_by"] = "Sortieren nach";
$install_lang["start_admin_items_per_page_field"] = "Standard: Anzahl der Artikel pro Steite im Adminmode";
$install_lang["start_index_items_per_page_field"] = "Standard: Anzahl der Artikel pro Seite im Benutzermode";
$install_lang["start_phone_format_field"] = "Telefonformat (verwende `9` stellige als Maske)";
$install_lang["start_settings_module"] = "Standards";
$install_lang["start_use_phone_format_field"] = "Telefonformatprüfung aktivieren";
$install_lang["stat_header_month"] = "Monat";
$install_lang["stat_header_name"] = "Name";
$install_lang["stat_header_week"] = "Woche";
$install_lang["success_update_numerics_data"] = "Allgemeine Einstellungen sind erfolgreich aktualisiert";
$install_lang["system_version"] = "Systemversion";
$install_lang["system_version_available"] = "<a href=\"http://www.pilotgroup.net/support/\">Neue Version [version] verfügbar</a>";
$install_lang["template"] = "Template";
$install_lang["text_error"] = "Die Seite wurde nicht gefunden";
$install_lang["text_of"] = "von";
$install_lang["today_on_site"] = "Heute auf der Seite";
$install_lang["total"] = "gesamt";
$install_lang["unselect_all"] = "Alle deaktivieren";
$install_lang["update"] = "Aktualisierung";
$install_lang["use_last_search"] = "Verwende aktuelle Suche";
$install_lang["users_use_map_in_search_field"] = "Karte auf Profilsuchseite aktivieren";
$install_lang["users_use_poll_in_search_field"] = "Umfrage auf Benutzersuchseite aktivieren";
$install_lang["view"] = "anzeigen";
$install_lang["view_all"] = "alle anzeigen";
$install_lang["yes_str"] = "Ja";


<?php

$install_lang["header_get_started"] = "Beginnen";
$install_lang["header_quick_start"] = "Schnelle Statistiken";
$install_lang["link_1"] = "Admininfo bearbeiten";
$install_lang["link_10"] = "dynamische Blöcke erstellen";
$install_lang["link_11"] = "Seitentexte bearbeiten";
$install_lang["link_12"] = "Infoseiten hinzufügen";
$install_lang["link_2"] = "SMTP Server hinzufügen";
$install_lang["link_3"] = "SEO Metadaten prüfen";
$install_lang["link_4"] = "Cronjobs aktivieren";
$install_lang["link_5"] = "Länder installieren";
$install_lang["link_6"] = "Video-Upload-Optionen prüfen";
$install_lang["link_7"] = "Moderationsoptionen prüfen";
$install_lang["link_8"] = "Wasserzeichen hinzufügen";
$install_lang["link_9"] = "Dateien-Upload-Optionen prüfen";
$install_lang["moderator_comment"] = "Sie sind als Moderator eingeloggt. Sie haben eingeschränkten Zugriff zum Admin-Panel. Um mehr Berechtigung zu erhalten kontaktieren Sie bitte den Seitenadministrator.";


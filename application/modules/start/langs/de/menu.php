<?php

$install_lang["admin_menu_main_items"] = "Haupt";
$install_lang["admin_menu_main_items_admin-home-item"] = "Home";
$install_lang["admin_menu_main_items_admin-home-item_tooltip"] = "";
$install_lang["admin_menu_main_items_exp-import-items"] = "Export/Import";
$install_lang["admin_menu_main_items_exp-import-items_tooltip"] = "";
$install_lang["admin_menu_main_items_moderation-items"] = "Moderation";
$install_lang["admin_menu_main_items_moderation-items_tooltip"] = "";
$install_lang["admin_menu_main_items_tooltip"] = "";
$install_lang["admin_menu_other_items"] = "Module";
$install_lang["admin_menu_other_items_admin-modules-item"] = "Modul Installation";
$install_lang["admin_menu_other_items_admin-modules-item_tooltip"] = "";
$install_lang["admin_menu_other_items_newsletters-items"] = "Newsletter";
$install_lang["admin_menu_other_items_newsletters-items_tooltip"] = "";
$install_lang["admin_menu_other_items_tooltip"] = "";
$install_lang["admin_menu_settings_items"] = "Einstellungen";
$install_lang["admin_menu_settings_items_content_items"] = "Inhalt";
$install_lang["admin_menu_settings_items_content_items_tooltip"] = "";
$install_lang["admin_menu_settings_items_feedbacks-items"] = "Bewertungen";
$install_lang["admin_menu_settings_items_feedbacks-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_interface-items"] = "Schnittstelle";
$install_lang["admin_menu_settings_items_interface-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_system-items"] = "System";
$install_lang["admin_menu_settings_items_system-items_system-numerics-item"] = "Allgemeine Einstellungen";
$install_lang["admin_menu_settings_items_system-items_system-numerics-item_tooltip"] = "Anzahl der Artikel auf einer Seite & andere allgemeine Einstellungen";
$install_lang["admin_menu_settings_items_system-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_tooltip"] = "";
$install_lang["admin_menu_settings_items_uploads-items"] = "Uploads";
$install_lang["admin_menu_settings_items_uploads-items_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-overview-item"] = "Übersicht";
$install_lang["agent_top_menu_agent-main-overview-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-overview-item"] = "Übersicht";
$install_lang["company_top_menu_company-main-overview-item_tooltip"] = "";
$install_lang["private_top_menu_private-main-overview-item"] = "Übersicht";
$install_lang["private_top_menu_private-main-overview-item_tooltip"] = "";


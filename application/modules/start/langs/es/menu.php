<?php

$install_lang["admin_menu_main_items"] = "Principal";
$install_lang["admin_menu_main_items_admin-home-item"] = "Inicio";
$install_lang["admin_menu_main_items_admin-home-item_tooltip"] = "";
$install_lang["admin_menu_main_items_exp-import-items"] = "Exportación/Importación";
$install_lang["admin_menu_main_items_exp-import-items_tooltip"] = "";
$install_lang["admin_menu_main_items_moderation-items"] = "Moderación";
$install_lang["admin_menu_main_items_moderation-items_tooltip"] = "";
$install_lang["admin_menu_main_items_tooltip"] = "";
$install_lang["admin_menu_other_items"] = "Módulos";
$install_lang["admin_menu_other_items_admin-modules-item"] = "Instalación de módulos";
$install_lang["admin_menu_other_items_admin-modules-item_tooltip"] = "";
$install_lang["admin_menu_other_items_newsletters-items"] = "Boletines";
$install_lang["admin_menu_other_items_newsletters-items_tooltip"] = "";
$install_lang["admin_menu_other_items_tooltip"] = "";
$install_lang["admin_menu_settings_items"] = "Configuración";
$install_lang["admin_menu_settings_items_content_items"] = "Contenido";
$install_lang["admin_menu_settings_items_content_items_tooltip"] = "";
$install_lang["admin_menu_settings_items_feedbacks-items"] = "Opiniones";
$install_lang["admin_menu_settings_items_feedbacks-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_interface-items"] = "Interfaz";
$install_lang["admin_menu_settings_items_interface-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_system-items"] = "Sistema";
$install_lang["admin_menu_settings_items_system-items_system-numerics-item"] = "Configuración general";
$install_lang["admin_menu_settings_items_system-items_system-numerics-item_tooltip"] = "Número de elementos de una página y otros ajustes generales";
$install_lang["admin_menu_settings_items_system-items_tooltip"] = "";
$install_lang["admin_menu_settings_items_tooltip"] = "";
$install_lang["admin_menu_settings_items_uploads-items"] = "Subidas";
$install_lang["admin_menu_settings_items_uploads-items_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-overview-item"] = "Panel de control";
$install_lang["agent_top_menu_agent-main-overview-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-overview-item"] = "Panel de control";
$install_lang["company_top_menu_company-main-overview-item_tooltip"] = "";
$install_lang["private_top_menu_private-main-overview-item"] = "Panel de control";
$install_lang["private_top_menu_private-main-overview-item_tooltip"] = "";


<?php

$install_lang["header_get_started"] = "Comenzar";
$install_lang["header_quick_start"] = "Estadísticas rápidas";
$install_lang["link_1"] = "Editar la información de administración";
$install_lang["link_10"] = "Configure los bloques dinámicos";
$install_lang["link_11"] = "Editar textos del sitio";
$install_lang["link_12"] = "Agregar páginas de información";
$install_lang["link_2"] = "Agregar servidor SMTP";
$install_lang["link_3"] = "Compruebe metadatos de SEO";
$install_lang["link_4"] = "Habilitar tareas programadas";
$install_lang["link_5"] = "Instalar países";
$install_lang["link_6"] = "Revise las opciones de archivos de vídeo";
$install_lang["link_7"] = "Revise las opciones de moderación";
$install_lang["link_8"] = "Añadir marca de agua";
$install_lang["link_9"] = "Revise los archivos de opciones de archivos";
$install_lang["moderator_comment"] = "Ha iniciado la sesión como moderador. Usted tiene un acceso limitado a las secciones del panel de administración. Para obtener más permisos, por favor póngase en contacto con el administrador del sitio.";


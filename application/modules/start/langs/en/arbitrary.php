<?php

$install_lang["seo_tags_admin_description"] = "";
$install_lang["seo_tags_admin_header"] = "";
$install_lang["seo_tags_admin_keyword"] = "";
$install_lang["seo_tags_admin_og_description"] = "";
$install_lang["seo_tags_admin_og_title"] = "";
$install_lang["seo_tags_admin_og_type"] = "";
$install_lang["seo_tags_admin_title"] = "";

$install_lang["seo_tags_index_description"] = "PG Real Estate - professional script for real estate agency website.";
$install_lang["seo_tags_index_header"] = "PG Real Estate - professional script for real estate agency website.";
$install_lang["seo_tags_index_keyword"] = "realestate, realestate agency, realestate script, realestate website";
$install_lang["seo_tags_index_og_description"] = "PG Real Estate - professional script for real estate agency website.";
$install_lang["seo_tags_index_og_title"] = "PG Real Estate - professional script for real estate agency website.";
$install_lang["seo_tags_index_og_type"] = "site";
$install_lang["seo_tags_index_title"] = "PG Real Estate - professional script for real estate agency website.";


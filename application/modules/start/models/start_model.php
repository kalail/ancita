<?php  

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Start Main model
 * 
 * @package PG_RealEstate
 * @subpackage Start
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Start_model extends Model
{
	var $CI;

	function Start_model()
	{
		parent::Model();
		$this->CI = & get_instance();
	}

	function clear_trash_folder(){
		$result = false;
		if ($handle = opendir(SITE_PHYSICAL_PATH . TRASH_FOLDER)){
			while (false !== ($file = readdir($handle))){
				if ($file != "." && $file != ".."){
					@unlink(SITE_PHYSICAL_PATH . TRASH_FOLDER . $file);
				}
			}
			closedir($handle);
			$result = true;
		}
		return $result;
	}

	////// banners callback method
	public function _banner_available_pages(){
		$return[] = array("link"=>"start/index", "name"=> l('header_start_page', 'start'));
		$return[] = array("link"=>"start/homepage", "name"=> l('header_homepage', 'start'));
		return $return;
	}
	
	public function _dynamic_block_get_stat_block() {
		$this->CI->load->helper('start');
		return start_statistics();
	}
	
	public function _dynamic_block_get_search_form(){
		$this->CI->load->helper("start");
		return main_search_form("sale", "short", true);
	}
}

<?php

$install_lang["seo_tags_index_description"] = "Mapa del sitio";
$install_lang["seo_tags_index_header"] = "Mapa del sitio";
$install_lang["seo_tags_index_keyword"] = "mapa del sitio, site pages";
$install_lang["seo_tags_index_og_description"] = "Mapa del sitio";
$install_lang["seo_tags_index_og_title"] = "Mapa del sitio";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Mapa del sitio";


<?php

$install_lang["seo_tags_index_description"] = "Site map";
$install_lang["seo_tags_index_header"] = "Site map";
$install_lang["seo_tags_index_keyword"] = "site map, site pages";
$install_lang["seo_tags_index_og_description"] = "Site map";
$install_lang["seo_tags_index_og_title"] = "Site map";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Site map";


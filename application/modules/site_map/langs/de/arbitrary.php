<?php

$install_lang["seo_tags_index_description"] = "Seitenkarte";
$install_lang["seo_tags_index_header"] = "Seitenkarte";
$install_lang["seo_tags_index_keyword"] = "seitenkarte, site pages";
$install_lang["seo_tags_index_og_description"] = "Seitenkarte";
$install_lang["seo_tags_index_og_title"] = "Seitenkarte";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Seitenkarte";


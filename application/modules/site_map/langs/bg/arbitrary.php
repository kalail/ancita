<?php

$install_lang["seo_tags_index_description"] = "Карта сайта";
$install_lang["seo_tags_index_header"] = "Карта на сайта";
$install_lang["seo_tags_index_keyword"] = "карта на сайта, списък на страниците";
$install_lang["seo_tags_index_og_description"] = "Карта на сайта";
$install_lang["seo_tags_index_og_title"] = "Карта на сайта";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Карта на сайта";


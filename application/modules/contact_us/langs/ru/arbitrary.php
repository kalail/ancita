<?php

$install_lang["seo_tags_index_description"] = "Написать письмо администратору сайта. Задать вопрос о работе сайта.";
$install_lang["seo_tags_index_header"] = "Свяжитесь с нами";
$install_lang["seo_tags_index_keyword"] = "написать письмо, письмо администратору сайта, задать вопрос";
$install_lang["seo_tags_index_og_description"] = "Написать письмо администратору сайта. Задать вопрос о работе сайта.";
$install_lang["seo_tags_index_og_title"] = "Свяжитесь с нами";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "PG Real Estate : Свяжитесь с нами";


<?php

$install_lang["seo_tags_index_description"] = "Съобщение до администратора на сайта. Задаване на въпрос за работата на сайта";
$install_lang["seo_tags_index_header"] = "Свържете се с нас";
$install_lang["seo_tags_index_keyword"] = "писане на съобщение, съобщение до администратора, задаване на въпрос";
$install_lang["seo_tags_index_og_description"] = "Съобщение до администратора на сайта. Задаване на въпрос за работата на сайта";
$install_lang["seo_tags_index_og_title"] = "Свържете се с нас";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "PG Real Estate  : Свържете се с нас";


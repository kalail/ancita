<?php

$install_lang["notification_contact_us_form"] = "Contactez nous";
$install_lang["tpl_contact_us_form_content"] = "La formule de contact est remplie:\n\n\n\n___________________________________\n\n\n\nRaison: [reason];\n\nNom: [user_name];\n\nCourriel: [user_email];\n\nSujet: [subject];\n\nMessage:\n\n[message] \n\n___________________________________";
$install_lang["tpl_contact_us_form_subject"] = "[domain] | Nouvelle formule de contact";


<?php

$install_lang["admin_contacts_menu_reasons_list_item"] = "Razones de contacto";
$install_lang["admin_contacts_menu_reasons_list_item_tooltip"] = "";
$install_lang["admin_contacts_menu_settings_list_item"] = "Configuración";
$install_lang["admin_contacts_menu_settings_list_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_system-items_contactus_menu_item"] = "Contáctenos";
$install_lang["admin_menu_settings_items_system-items_contactus_menu_item_tooltip"] = "Contáctenos";
$install_lang["user_footer_menu_footer-menu-contact-item"] = "Contáctenos";
$install_lang["user_footer_menu_footer-menu-contact-item_tooltip"] = "";


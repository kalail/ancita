<?php

$install_lang["seo_tags_index_description"] = "Contact administrator. Ask questions about site work and services.";
$install_lang["seo_tags_index_header"] = "Contáctenos";
$install_lang["seo_tags_index_keyword"] = "contact administrator, send message to administrator, ask questions";
$install_lang["seo_tags_index_og_description"] = "Contact administrator. Ask questions about site work and services.";
$install_lang["seo_tags_index_og_title"] = "Contáctenos";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "PG Real Estate : Póngase en contacto con nosotros";


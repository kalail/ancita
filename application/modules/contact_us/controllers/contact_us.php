<?php

/**
 * Contact us user side controller
 * 
 * @package PG_RealEstate
 * @subpackage Contact us
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Contact_us extends Controller
{
	/**
	 * link to CodeIgniter object
	 * @var object
	 */

	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::Controller();
		$this->load->model("Contact_us_model");
	}
	
	public function index(){
		$reasons = $this->Contact_us_model->get_reason_list();
		$this->template_lite->assign('reasons', $reasons);

		if($this->input->post('btn_save')){
			$post_data = array(
				"id_reason" => $this->input->post('id_reason', true),
				"user_name" => $this->input->post('user_name', true),
				"user_email" => $this->input->post('user_email', true),
				"subject" => $this->input->post('subject', true),
				"message" => $this->input->post('message', true),
				"captcha_code" => $this->input->post('captcha_code', true),
			);
			$validate_data = $this->Contact_us_model->validate_contact_form($post_data);
            $CI = & get_instance();
			$lang = $CI->pg_language->current_lang_id;
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message('error', $validate_data["errors"]);
				$data = $validate_data["data"];
			}else{
			    $this->Contact_us_model->send_contact_form($validate_data["data"], $lang);
				$this->system_messages->add_message('success', l('success_send_form', 'contact_us'));
			}
		}

		$this->load->plugin('captcha');
		$vals = array(
			'img_path' => TEMPPATH.'/captcha/',
			'img_url' => SITE_VIRTUAL_PATH. 'temp/captcha/',
			'font_path' => BASEPATH.'fonts/arial.ttf',
			'img_width' => '120',
			'img_height' => '27',
			'expiration' => 7200
		);
		
		$cap = create_captcha($vals);
		$data["captcha"] = $cap['image'];
		$_SESSION["captcha_word"] = $cap['word'];
		$this->template_lite->assign('data', $data);

		$this->load->model('Menu_model');
		$this->Menu_model->breadcrumbs_set_active(l('header_contact_us_form', 'contact_us'));

		$this->template_lite->view('form');
	}
	
	public function feedback()
	{
		$questions = $this->Contact_us_model->get_feedback_questions();
		$this->template_lite->assign('feedback', $questions);
		
		$agentlists = $this->Contact_us_model->get_agent_list();
		$strAgents = "[";
		for($i=0; $i<count($agentlists); $i++)
		{
			$strAgents .= ($i > 0)?',':'';
			$strAgents .='{ id: '.$agentlists[$i]["id"].', label: "'.$agentlists[$i]["unique_name"].'"}';
		}
		$strAgents .= "];";
		$this->template_lite->assign('agentslist', $strAgents);
		
		if($this->input->post('btn_save_feedback')){
			$post_data = array(
				"agent" => $this->input->post('agent', true),
				"agent2" => $this->input->post('agent2', true),
				"feedback_radio" => $this->input->post('feedback_radio', true),
				"Message" => $this->input->post('Message', true),
				"name" => $this->input->post('name', true),
				"email" => $this->input->post('email', true),
				"captcha_code" => $this->input->post('keystring', true),
			);
			
			$validate_data["data"]["user_id"] = intval($this->session->userdata("user_id", true));
			
			$arrfeedback = $this->input->post('feedback_radio', true);
			foreach ($arrfeedback as $val) {
				$strfeedbackoption = feedback_.$val;
				$strfeedoptionVal = $this->input->post($strfeedbackoption, true);
				if (isset($strfeedoptionVal)) {
					$strfeedoptionVal != 0 ? $feedback[$val] = $strfeedoptionVal : null;
				}
			}
			$validate_data = $this->Contact_us_model->validate_feedback_form($post_data);
			$validate_data["data"]["feedbackScore"] = $feedback;
			
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message('error', $validate_data["errors"]);
				$data = $validate_data["data"];
			}else{
				$this->Contact_us_model->save_feedback_form($validate_data["data"]);
			 //mail for agent
			$data = $validate_data["data"];
			$this->load->model('Users_model');
            $user = $this->Users_model->get_user_by_id($data["agent"]);
			$adminemail = $this->pg_module->get_module_config('reviews', 'reviews_alert_email');
			$agentemail = ($user["contact_email"] != '')?$user["contact_email"]:$user["email"];
			$agentname = $user['unique_name'];		
            $messageag = '<html><body>';
            $messageag .= '<img src="http://testing.ancita.com/application/views/pilot/logo/logo_en.png" alt="Quest send Request" />';
            $messageag .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
            $messageag .= "<tr style='background: #eee;'><td><strong>Guest Name:</strong> </td><td>" . $data["name"] . "</td></tr>";
            $messageag .= "<tr style='background: #eee;'><td><strong>Guest Email Id:</strong> </td><td>" .  $data["email"] . "</td></tr>";
            $messageag .= "<tr style='background: #eee;'><td><strong>Agent name:</strong> </td><td>" . $agentname . "</td></tr>";
            $messageag .= "<tr style='background: #eee;'><td><strong>Feedback Score:</strong> </td><td>" . $data["feedbackScore"] . "</td></tr>";
            $messageag .= "<tr style='background: #eee;'><td><strong>Message:</strong> </td><td>" . $data["Message"] . "</td></tr>";
            $messageag .= "</table>";
            $messageag .= "</body></html>";
            $subject = 'Guest Send Feedback';
            $headers = "From: " . $data["name"] . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            mail($agentemail, $subject, $messageag, $headers);
			
			//mail for admin			
            mail($adminemail, $subject, $messageag, $headers);
			$this->system_messages->add_message('success', 'Your feedback has been send successfully.');
			redirect(site_url()."feedback");
			}
			
			
		}
		$this->load->plugin('captcha');
		$vals = array(
			'img_path' => TEMPPATH.'/captcha/',
			'img_url' => SITE_VIRTUAL_PATH. 'temp/captcha/',
			'font_path' => BASEPATH.'fonts/arial.ttf',
			'img_width' => '120',
			'img_height' => '27',
			'expiration' => 7200
		);
		
		$cap = create_captcha($vals);
		$data["captcha"] = $cap['image'];
		$_SESSION["captcha_word"] = $cap['word'];
		$this->template_lite->assign('data', $data);
		$CI = & get_instance();
		$count_active = 0;
		foreach($CI->pg_language->languages as $language){
			if($language["status"]){
				$count_active++;
			}
		}
		$CI->template_lite->assign("count_active", $count_active);
		$CI->template_lite->assign("current_lang", $CI->pg_language->current_lang_id);
		$CI->template_lite->assign("languages", $CI->pg_language->languages);
		$this->load->model('Menu_model');
		$this->Menu_model->breadcrumbs_set_active('Feedback');
		$this->template_lite->view('feedback');
	}
	
	public function properties()
	{
		$country_list = $this->Contact_us_model->get_country();
		$region_list = $this->Contact_us_model->get_regoin();
		
		$year = date("Y", time());

		for ($i = 0; $i < 20; $i++) {
			$actual_year = $year-$i;
			if ($i < 5) {
				$cust_ids[] = 4;
			} else if ($i >= 5 && $i < 10) {
				$cust_ids[] = 3;		
			} else if ($i >= 10 && $i < 20) {
				$cust_ids[] = 2;		
			}
			$cust_names[] .= "&nbsp;--&nbsp;&nbsp;" . $actual_year;
		}
		$cust_ids[] .= 1;
		$cust_names[] .= "&nbsp;<&nbsp;&nbsp;". ($year-19);
		
		$Years_Option = array();
		for($row = 0; $row < count($cust_ids); $row++)
		{
			$Years_Option[$row] = array('id'=>$cust_ids[$row],'name'=>$cust_names[$row]);
		}
		
	
		$this->template_lite->assign('country_list', $country_list);
		$this->template_lite->assign('region_list', $region_list);
		$this->template_lite->assign('Years_Option', $Years_Option);
		$this->load->model('Menu_model');
		$this->Menu_model->breadcrumbs_set_active('Properties');
		$CI = & get_instance();
		$count_active = 0;
		foreach($CI->pg_language->languages as $language){
			if($language["status"]){
				$count_active++;
			}
		}
		$CI->template_lite->assign("count_active", $count_active);
		$CI->template_lite->assign("current_lang", $CI->pg_language->current_lang_id);
		$CI->template_lite->assign("languages", $CI->pg_language->languages);
		$this->template_lite->view('properties');
	}
	
	public function ajax_get_calculations()
	{
		//$operation_type = $this->input->post('type', true);
		//$value = $this->input->post('value', true);
		//echo 'hello';
		//die;
		$country_select = $_REQUEST["country_select"];
		$region_select = $_REQUEST["region_select"];
		$city_select = $_REQUEST["city_select"];
		$area_select = $_REQUEST["area_select"];
		
		$prices = $this->Contact_us_model->get_city_price($city_select);
		$adjustment = $this->Contact_us_model->get_district_price($city_select,$area_select);
		
		//echo json_encode($adjustment);
		//echo $adjustment['adjustment'];
		//return;
		$property_type = $_REQUEST["property_type"];
		$beach = $_REQUEST["beach"];
		$view = $_REQUEST["view"];
		$tennis = $_REQUEST["tennis"];
		$football = $_REQUEST["football"];
		$golf = $_REQUEST["golf"];
		$buss = $_REQUEST["buss"];
		$supermarket = $_REQUEST["supermarket"];
		$restaurants = $_REQUEST["restaurants"];
		$bedrooms = $_REQUEST["bedrooms"];
		$bathrooms = $_REQUEST["bathrooms"];
		$living = $_REQUEST["living"];
		
		$beach_factor = "aa_" . $beach;
		$view_factor = "bb_" . $view;
		$tennis_factor = "cc";
		$football_factor = "dd";
		$golf_factor = "ee";
		$buss_factor = "ff";
		$supermarket_factor = "gg";
		$restaurants_factor = "hh";
		if ($bathrooms / $bedrooms < 0.49) {
			$bathroom_factor = "ii_1";
		} else if ($bathrooms / $bedrooms >= 0.49 && $bathrooms / $bedrooms < 0.69) {
			$bathroom_factor = "ii_2";
		} else {
			$bathroom_factor = "ii_3";
		}
		
		if ($property_type == 1) {
			// Apartment variables
				$type = $_REQUEST["type"];
				$elevator = $_REQUEST["elevator"];
				$terrasse = $_REQUEST["terrasse"];
				$terrasse_barbeque = $_REQUEST["terrasse_barbeque"];
				$terrasse_solarium = $_REQUEST["terrasse_solarium"];
				$terrasse_south = $_REQUEST["terrasse_south"];
				$terrasse_west = $_REQUEST["terrasse_west"];
				$garage = $_REQUEST["garage"];
				$pool = $_REQUEST["pool"];
				$standard = $_REQUEST["standard"];
				$years_finished = $_REQUEST["years_finished"];
				$years_renovated = $_REQUEST["years_renovated"];
				$turistico = $_REQUEST["turistico"];
				$sqm_terrasse = $_REQUEST["sqm_terrasse"];
			
			//echo json_encode($terrasse);die;
			// Apartment variables
				$type_factor = "ba_" . $type;
				$elevator_factor = "bb";
				if ($terrasse == 1) {
					$terrasse_factor = "bc_1";
				} else {
					$terrasse_factor = "bc_2";
				}
				if ($terrasse_barbeque == 1) {
					$terrasse_barbeque_factor = "bd_1";
				} else {
					$terrasse_barbeque_factor = "bd_2";
				}
				$terrasse_solarium_factor = "be";
				$terrasse_south_factor = "bf";
				$terrasse_west_factor = "bg";
				if ($garage == 1) {
					$garage_factor = "bh_1";
				} else {
					$garage_factor = "bh_2";
				}
				$pool_factor = "bi_" . $pool;
				$standard_factor = "bj_" . $standard;
				$years_finished_factor = "bk_" . $years_finished;
				
				if ( $years_renovated != 0 ){
					$years_renovated_factor = "bl_" . $years_renovated;
				}
				else{
					$years_renovated_factor = '1';
					}
				
				if ($turistico == 1) {
					$turistico_factor = "bm_1";
				} else {
					$turistico_factor = "bm_2";
				}
			
				$current_version = $this->Contact_us_model->get_version_setting('Commonfactors');
				$month = $current_version[0]['Month'];
				$year = $current_version[0]['Year'];
				
				$strSQL = " SELECT " . $beach_factor . "," . $view_factor . "," . $tennis_factor . "," . $football_factor . "," . $golf_factor . "," . $buss_factor . "," . $supermarket_factor . "," . $restaurants_factor . "," . $bathroom_factor . "," . $type_factor . "," . $elevator_factor . "," . $terrasse_factor . "," . $terrasse_barbeque_factor . "," . $terrasse_solarium_factor . "," . $terrasse_south_factor . "," . $terrasse_west_factor . "," . $garage_factor . "," . $pool_factor . "," . $standard_factor . "," . $years_finished_factor . "," . $years_renovated_factor . "," . $turistico_factor . ", price_min, price_max FROM " . FACTORS_TABLE . " WHERE Month='".$month."' AND Year='".$year."'";
				$row = $this->Contact_us_model->get_factors($strSQL);
				
				if ($row) {
					$data['beach_factor'] = $row[0];
					$data['view_factor'] = $row[1];
					$data['tennis_factor'] = $tennis == 1 ? $row[2] : 1;
					$data['football_factor'] = $football == 1 ? $row[3] : 1;
					$data['golf_factor'] = $golf == 1 ? $row[4] : 1;
					$data['buss_factor'] = $buss == 1 ? $row[5] : 1;
					$data['supermarket_factor'] = $supermarket == 1 ? $row[6] : 1;
					$data['restaurants_factor'] = $restaurants == 1 ? $row[7] : 1;
					$data['bathroom_factor'] = $row[8];
			
					$data['type_factor'] = $row[9];
					$data['elevator_factor'] = $elevator == 1 ? 1 : $row[10];
					$data['terrasse_factor'] = $row[11];
					$data['terrasse_barbeque_factor'] = $row[12];
					$data['terrasse_solarium_factor'] = $terrasse_solarium == 1 ? $row[13] : 1;
					$data['terrasse_south_factor'] = $terrasse_south == 1 ? $row[14] : 1;
					$data['terrasse_west_factor'] = $terrasse_west == 1 ? $row[15] : 1;
					$data['garage_factor'] = $row[16];
					$data['pool_factor'] = $row[17];
					$data['standard_factor'] = $row[18];
					$data['years_finished_factor'] = $row[19];
					$data['years_renovated_factor'] = $row[20];
					$data['turistico_factor'] = $row[21];
					$data['price_min'] = $row[22];
					$data['price_max'] = $row[23];
				}
			
				$apartment_baseline_price = $prices['appartment_price'] * $adjustment['adjustment'];
			
				$common_adjustment_factor = $data['beach_factor'] * $data['view_factor'] * $data['tennis_factor'] * $data['football_factor'] * $data['golf_factor'] * $data['buss_factor'] * $data['supermarket_factor'] * $data['restaurants_factor'] * $data['bathroom_factor'];
			
				$apartment_adjustment_factor = $data['type_factor'] * $data['elevator_factor'] * $data['terrasse_factor'] * $data['terrasse_barbeque_factor'] * $data['terrasse_solarium_factor'] * $data['terrasse_south_factor'] * $data['terrasse_west_factor'] * $data['garage_factor'] * $data['pool_factor'] * $data['standard_factor'] * $data['years_finished_factor'] * $data['years_renovated_factor'] * $data['turistico_factor'];
			
				$apartment_valuation_min = $data['price_min'] * $apartment_baseline_price * $apartment_adjustment_factor * $common_adjustment_factor * ($living + ($sqm_terrasse * 0.5));
				$apartment_valuation_max = $data['price_max'] * $apartment_baseline_price * $apartment_adjustment_factor * $common_adjustment_factor * ($living + ($sqm_terrasse * 0.5));	
			
				$minVal = 0;
				$maxVal = 0;
				
				/*if($apartment_valuation_min > 99 && $apartment_valuation_min < 1000)
					$minVal = ceil($apartment_valuation_min/10)*10;
				elseif($apartment_valuation_min > 999 && $apartment_valuation_min < 10000)
					$minVal = ceil($apartment_valuation_min/100)*100;
				elseif($apartment_valuation_min > 9999 && $apartment_valuation_min < 100000)
					$minVal = ceil($apartment_valuation_min/1000)*1000;
				elseif($apartment_valuation_min > 99999)
					$minVal = ceil($apartment_valuation_min/10000)*10000;
				
				if($apartment_valuation_max > 99 && $apartment_valuation_max < 1000)
					$maxVal = ceil($apartment_valuation_max/10)*10;
				elseif($apartment_valuation_max > 999 && $apartment_valuation_max < 10000)
					$maxVal = ceil($apartment_valuation_max/100)*100;
				elseif($apartment_valuation_max > 9999 && $apartment_valuation_max < 100000)
					$maxVal = ceil($apartment_valuation_max/1000)*1000;
				elseif($apartment_valuation_max > 99999)
					$maxVal = ceil($apartment_valuation_max/10000)*10000;
					
				echo number_format($minVal, 0) . " - " . number_format($maxVal, 0) ." EUR";*/
				
				$minVal = ceil($apartment_valuation_min/500)*500;
				$maxVal = ceil($apartment_valuation_max/500)*500;
				echo number_format($minVal, 0) . " - " . number_format($maxVal, 0) ." EUR";
				
			} else if ($property_type == 2) {

				// House variables
				$type_house = $_REQUEST["type_house"];
				$garden = $_REQUEST["garden"];
				$barbeque_garden = $_REQUEST["barbeque_garden"];
				$one_floor = $_REQUEST["one_floor"];
				$owned = $_REQUEST["owned"];
				$garage_house = $_REQUEST["garage_house"];
				$plot = $_REQUEST["plot"];
				$pool_house = $_REQUEST["pool_house"];
				$standard_house = $_REQUEST["standard_house"];
				$years_finished_house = $_REQUEST["years_finished_house"];
				$years_renovated_house = $_REQUEST["years_renovated_house"];
				$sqm_terrasse_house = $_REQUEST["sqm_terrasse_house"];
				
			// House variables
				$plot_factor = "a_".$plot;
				if ($owned == 1) {
					$owned_factor = "b_1";
				} else {
					$owned_factor = "b_2";        
				}
				$type_house_factor = "c_" . $type_house;
				if ($one_floor == 1) {
					$one_floor_factor = "d_1";
				} else {
					$one_floor_factor = "d_2";        
				}
				if ($garden == 1) {
					$garden_factor = "e_1";
				} else {
					$garden_factor = "e_2";
				}
				if ($barbeque_garden == 1) {
					$barbeque_garden_factor = "f_1";
				} else {
					$barbeque_garden_factor = "f_2";
				}
				$garage_house_factor = "g_" . $garage_house;
				$pool_house_factor = "h_".$pool_house;
				$standard_house_factor = "i_".$standard_house;
				$years_finished_house_factor = "j_" . $years_finished_house;
				
				if ( $years_renovated_house != 0 ){
					$years_renovated_house_factor = "k_" . $years_renovated_house;
				}
				else{
						$years_renovated_house_factor = '1';
					}
				
				$current_version = $this->Contact_us_model->get_version_setting('Commonfactors');
				$month = $current_version[0]['Month'];
				$year = $current_version[0]['Year'];
				
				$strSQL = " SELECT " . $beach_factor . "," . $view_factor . "," . $tennis_factor . "," . $football_factor . "," . $golf_factor . "," . $buss_factor . "," . $supermarket_factor . "," . $restaurants_factor . "," . $bathroom_factor . "," . $plot_factor . "," . $owned_factor . "," . $type_house_factor . "," . $one_floor_factor . "," . $garden_factor . "," . $barbeque_garden_factor . "," . $garage_house_factor . "," . $pool_house_factor . "," . $standard_house_factor . "," . $years_finished_house_factor . "," . $years_renovated_house_factor . ", price_min, price_max FROM " . FACTORS_TABLE . " WHERE Month='".$month."' AND Year='".$year."'";
				
				$row = $this->Contact_us_model->get_factors($strSQL);
			
				if ($row) {
					$data['beach_factor'] = $row[0];
					$data['view_factor'] = $row[1];
					$data['tennis_factor'] = $tennis == 1 ? $row[2] : 1;
					$data['football_factor'] = $football == 1 ? $row[3] : 1;
					$data['golf_factor'] = $golf == 1 ? $row[4] : 1;
					$data['buss_factor'] = $buss == 1 ? $row[5] : 1;
					$data['supermarket_factor'] = $supermarket == 1 ? $row[6] : 1;
					$data['restaurants_factor'] = $restaurants == 1 ? $row[7] : 1;
					$data['bathroom_factor'] = $row[8];
			
					$data['plot_factor'] = $row[9];
					$data['owned_factor'] = $row[10];
					$data['type_house_factor'] = $row[11];
					$data['one_floor_factor'] = $row[12];
					$data['garden_factor'] = $row[13];
					$data['barbeque_garden_factor'] = $row[14];
					$data['garage_house_factor'] = $row[15];
					$data['pool_house_factor'] = $row[16];
					$data['standard_house_factor'] = $row[17];
					$data['years_finished_house_factor'] = $row[18];
					$data['years_renovated_house_factor'] = $row[19];
					$data['price_min'] = $row[20];
					$data['price_max'] = $row[21];
				}    
			
				$house_baseline_price = $prices['house_price'] * $adjustment['adjustment'];
			
				$common_adjustment_factor = $data['beach_factor'] * $data['view_factor'] * $data['tennis_factor'] * $data['football_factor'] * $data['golf_factor'] * $data['buss_factor'] * $data['supermarket_factor'] * $data['restaurants_factor'] * $data['bathroom_factor'];
			
				$house_adjustment_factor = $data['plot_factor'] * $data['owned_factor'] * $data['type_house_factor'] * $data['one_floor_factor'] * $data['garden_factor'] * $data['barbeque_garden_factor'] * $data['garage_house_factor'] * $data['pool_house_factor'] * $data['standard_house_factor'] * $data['years_finished_house_factor'] * $data['years_renovated_house_factor'];
			
				$house_valuation_min = $data['price_min'] * $house_baseline_price * $house_adjustment_factor * $common_adjustment_factor * ($living + ($sqm_terrasse_house * 0.5));
				$house_valuation_max = $data['price_max'] * $house_baseline_price * $house_adjustment_factor * $common_adjustment_factor * ($living + ($sqm_terrasse_house * 0.5));	
				
				$minVal = 0;
				$maxVal = 0;
				$minVal = ceil($house_valuation_min/500)*500;
				$maxVal = ceil($house_valuation_max/500)*500;
				echo number_format($minVal, 0) . " - " . number_format($maxVal, 0) . " EUR";
			}
		return;
	}
	
	public function graphs()
	{
		$country_list = $this->Contact_us_model->get_country();
		$region_list = $this->Contact_us_model->get_regoin();
		
		$dates = $this->Contact_us_model->get_graph_query("SELECT DISTINCT date FROM " . PROVINCE_APARTMENTS_PRICES_GRAPH);
		$iter = 1;
		$dateAry = array();
		foreach($dates as $val) {
		
			array_push($dateAry, substr($val[0],0,4).'-'.substr($val[0],4));
			$iter++;
		}
		
		$this->template_lite->assign('country_list', $country_list);
		$this->template_lite->assign('region_list', $region_list);
		$this->template_lite->assign('dates', json_encode($dateAry));
		$this->load->model('Menu_model');
		$this->Menu_model->breadcrumbs_set_active('Graphs');
		$this->template_lite->view('graphs');
	}
	
	public function ajax_get_calculate_graphs()
	{
		
		$sel = (isset($_REQUEST["sel"]) && !empty($_REQUEST["sel"])) ? $_REQUEST["sel"] : "";
		$city_id = (isset($_REQUEST["city_id"]) && !empty($_REQUEST["city_id"])) ? $_REQUEST["city_id"] : "";
		$region_id = (isset($_REQUEST["region_id"]) && !empty($_REQUEST["region_id"])) ? $_REQUEST["region_id"] : "";
		$area_id = (isset($_REQUEST["area_id"]) && !empty($_REQUEST["area_id"])) ? $_REQUEST["area_id"] : "";
		$date_min = (isset($_REQUEST["date_min"]) && !empty($_REQUEST["date_min"])) ? $_REQUEST["date_min"] : "";
		$date_min = str_replace('-','',$date_min);
		$date_max = (isset($_REQUEST["date_max"]) && !empty($_REQUEST["date_max"])) ? $_REQUEST["date_max"] : "";
		$date_max = str_replace('-','',$date_max);
		
		$current_version = $this->Contact_us_model->get_version_setting('Commonfactors');
		$month = $current_version[0]['Month'];
		$year = $current_version[0]['Year'];
		$strSQL = "SELECT gliding FROM ". FACTORS_TABLE . " WHERE Month='".$month."' AND Year='".$year."'";
		$r = $this->Contact_us_model->get_factors($strSQL);
		
		$gliding = $r[0];
		$finalAry = array();
		
		if ($sel == 2) {
			$strSQL = "SELECT (UNIX_TIMESTAMP(concat(substr(date,1,4),'-',substr(date,5,2),'-01')) * 1000) as date, apartment_price FROM " . PROVINCE_APARTMENTS_PRICES_GRAPH . " WHERE id_province = 1 AND date > " .$date_min. " AND date <= ". $date_max;
			$prices = $this->Contact_us_model->get_graph_query($strSQL);
			$finalAry = $this->recountAry($prices, $gliding);
			
			$ary= array();
			$ary["label"] = 'Spain Apartment Prices';
			$ary["data"] = $finalAry;
		} else {
			
			$strSQL = "SELECT (UNIX_TIMESTAMP(concat(substr(date,1,4),'-',substr(date,5,2),'-01')) * 1000) as date, house_price FROM " . PROVINCE_HOUSES_PRICES_GRAPH . " WHERE id_province = 1 AND date > " .$date_min. " AND date <= ". $date_max;
			$prices = $this->Contact_us_model->get_graph_query($strSQL);
			
			$finalary = $this->recountAry($prices, $gliding);
			//echo json_encode($finalary);die;
			$ary= array();
			$ary["label"] = 'Spain Villas Prices';
			$ary["data"] = $finalary;
		}
		
		if ($sel == 2) {
			$strSQL = "SELECT (UNIX_TIMESTAMP(concat(substr(date,1,4),'-',substr(date,5,2),'-01')) * 1000) as date, apartment_price FROM " . PROVINCE_APARTMENTS_PRICES_GRAPH . " WHERE id_province = 2 AND date > " .$date_min. " AND date <= ". $date_max;
			$prices = $this->Contact_us_model->get_graph_query($strSQL);
			$finalary = $this->recountAry($prices, $gliding);
			
			$ary2= array();
			$ary2["label"] = 'Costa Blanca Apartment Prices';
			$ary2["data"] = $finalary;
		} else {
			$strSQL = "SELECT (UNIX_TIMESTAMP(concat(substr(date,1,4),'-',substr(date,5,2),'-01')) * 1000) as date, house_price FROM " . PROVINCE_HOUSES_PRICES_GRAPH . " WHERE id_province = 2 AND date > " .$date_min. " AND date <= ". $date_max;
			$prices = $this->Contact_us_model->get_graph_query($strSQL);
			$finalary = $this->recountAry($prices, $gliding);
			
			$ary2= array();
			$ary2["label"] = 'Costa Blanca Villas Prices';
			$ary2["data"] = $finalary;
		}
		
		if($city_id != null) {
			
			$strSQL = "SELECT name FROM ".CITIES_TABLE. " WHERE id='".$city_id."'";
			$r2 = $this->Contact_us_model->get_row_custom_query($strSQL);
			$name = $r2[0];
			
			if ($sel == 2) {
				$strSQL = "SELECT (UNIX_TIMESTAMP(concat(substr(date,1,4),'-',substr(date,5,2),'-01')) * 1000) as date, apartment_price FROM " . CITY_APARTMENTS_PRICES_GRAPH . " WHERE id_city = '" .$city_id. "' AND date > " .$date_min. " AND date <= ". $date_max;
				$prices = $this->Contact_us_model->get_graph_query($strSQL);
				$nextAry = $this->recountAry($prices, $gliding);
				
				$param = ($area_id != 0) ? $this->urbanicationFactor($city_id, $area_id) : 1;
				if ($area_id != 0 ) {
					$k = 0;
					foreach ($nextAry as $valAry) {
						array_walk($valAry, "urbanicationCorrect", $param);
						$finalAry[$k] = array();
						$finalAry[$k] = $valAry;
						$k++;
					}
				} else {
					$finalAry = $nextAry;
				}
				
				$ary3= array();
				$ary3["label"] = $name. ' Apartment Prices';
				$ary3["data"] = $finalAry != null ? $finalAry : null;
			} else {
				$strSQL = "SELECT (UNIX_TIMESTAMP(concat(substr(date,1,4),'-',substr(date,5,2),'-01')) * 1000) as date, house_price FROM " . CITY_HOUSES_PRICES_GRAPH . " WHERE id_city = '" .$city_id. "' AND date > " .$date_min. " AND date <= ". $date_max;
				$prices = $this->Contact_us_model->get_graph_query($strSQL);
				$nextAry = $this->recountAry($prices, $gliding);
				
				$param = ($area_id != 0) ? $this->urbanicationFactor($city_id, $area_id) : 1;
				if ($area_id != 0 ) {
					$k = 0;
					foreach ($nextAry as $valAry) {
						array_walk($valAry, "urbanicationCorrect", $param);
						$finalAry[$k] = array();
						$finalAry[$k] = $valAry;
						$k++;
					}
				} else {
					$finalAry = $nextAry;
				}	
			
				$ary3= array();
				$ary3["label"] = $name. ' Villas Prices';
				$ary3["data"] = $finalAry != null ? $finalAry : null;
			}
		}
		
		if($region_id != null) {
			$strSQL = "SELECT name FROM ".REGIONS_TABLE. " WHERE id='".$region_id."'";
			$r2 = $this->Contact_us_model->get_row_custom_query($strSQL);
			$name = $rs2[0];
			
			if ($sel == 2) {
				$strSQL = "SELECT (UNIX_TIMESTAMP(concat(substr(date,1,4),'-',substr(date,5,2),'-01')) * 1000) as date, apartment_price FROM " . PROVINCE_APARTMENTS_PRICES_GRAPH . " WHERE id_province = '" .$region_id. "' AND date > " .$date_min. " AND date <= ". $date_max;
				$prices = $this->Contact_us_model->get_graph_query($strSQL);
				$finalary = $this->recountAry($prices, $gliding);
			
				$ary4= array();
				$ary4["label"] = 'Province ' .$name. ' Apartment Prices';
				$ary4["data"] = $finalary;
			} else {
				$strSQL = "SELECT (UNIX_TIMESTAMP(concat(substr(date,1,4),'-',substr(date,5,2),'-01')) * 1000) as date, house_price FROM " . PROVINCE_HOUSES_PRICES_GRAPH . " WHERE id_province = '" .$region_id. "' AND date > " .$date_min. " AND date <= ". $date_max;
				$prices = $this->Contact_us_model->get_graph_query($strSQL);
				$finalary = $this->recountAry($prices, $gliding);
			
				$ary4= array();
				$ary4["label"] = 'Province' .$name. ' Villas Prices';
				$ary4["data"] = $finalary;
			}
		}

		$response = array();
		$response["country"] = $ary;
		$response["province"] = $ary2;
		$response["city"] = $ary3;
		$response["region"] = $ary4;
		
		echo json_encode($response);
		return;
	}
	
	function recountAry($transformAry, $gfactor) {
		$temp = array();
		$k = 0;
		$valueOn = 0;
		foreach ($transformAry as $key => $value) {
			$valueOn = $valueOn + $value[1];
			$temp[$k] = $valueOn;
			
			if ($k == 0) $transformAry[$key] = array(0 => $value[0], 1 => $valueOn);
			if ($k >= 1 && $k < $gfactor) $transformAry[$key] = array(0 => $value[0], 1 => ($valueOn)/($k+1));		
			if ($k >= $gfactor) $transformAry[$key] = array(0 => $value[0], 1 => ($valueOn - $temp[$k-$gfactor])/$gfactor);		
		
			$k++;
		}
		return $transformAry;
	}
	
	function urbanicationFactor($city_id, $area_id) {
		$strSQL = "SELECT adjustment FROM ".DISTRICTS_ADJUSTMENT_TABLE. " WHERE id_city='".$city_id."' AND id_district ='" .$area_id."'";
		$rs = $this->Contact_us_model->get_row_custom_query($strSQL);
		$param = $rs[0];
		return $param;
	}
	
	function urbanicationCorrect(&$value, $key, $param) {
		if($key == 1) $value = $value * $param;
	}
}

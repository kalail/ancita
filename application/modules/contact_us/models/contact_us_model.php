<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

define('CONTACT_REASONS_TABLE', DB_PREFIX.'contact_us');
define('FEEDBACK_QUESTIONS', DB_PREFIX.'feedback_questions');
define("QUESTIONS_TABLE", DB_PREFIX."feedback_ques");
define('FEEDBACK_SCORING', DB_PREFIX.'feedback_scoring');
define('USERS', DB_PREFIX.'users');

if (!defined('CITIES_TABLE')) define('CITIES_TABLE', DB_PREFIX.'cnt_cities');
if (!defined('REGIONS_TABLE')) define('REGIONS_TABLE', DB_PREFIX.'cnt_regions');
if (!defined('COUNTRIES_TABLE')) define('COUNTRIES_TABLE', DB_PREFIX.'cnt_countries');
if (!defined('DISTRICTS_TABLE')) define('DISTRICTS_TABLE', DB_PREFIX.'cnt_districts');
if (!defined('CITY_PRICES_TABLE')) define('CITY_PRICES_TABLE', DB_PREFIX.'market_city_price');
if (!defined('DISTRICTS_ADJUSTMENT_TABLE')) define('DISTRICTS_ADJUSTMENT_TABLE', DB_PREFIX.'market_district_price');
if (!defined('FACTORS_TABLE')) define('FACTORS_TABLE', DB_PREFIX.'market_factors');
if (!defined('VERSION_TABLE')) define('VERSION_TABLE', DB_PREFIX.'market_version');
if (!defined('CITY_APARTMENTS_PRICES_GRAPH')) define('CITY_APARTMENTS_PRICES_GRAPH', DB_PREFIX.'market_city_apartments_prices_graph');
if (!defined('CITY_HOUSES_PRICES_GRAPH')) define('CITY_HOUSES_PRICES_GRAPH', DB_PREFIX.'market_city_houses_prices_graph');
if (!defined('PROVINCE_APARTMENTS_PRICES_GRAPH')) define('PROVINCE_APARTMENTS_PRICES_GRAPH', DB_PREFIX.'market_province_apartments_prices_graph');
if (!defined('PROVINCE_HOUSES_PRICES_GRAPH')) define('PROVINCE_HOUSES_PRICES_GRAPH', DB_PREFIX.'market_province_houses_prices_graph');
if (!defined('FACTORS_GRAPH_TABLE')) define('FACTORS_GRAPH_TABLE', DB_PREFIX.'market_factors_graph');

/**
 * Contact us main model
 *
 * @package PG_RealEstate
 * @subpackage Contact us
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Contact_us_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Attributes of contact_us object
	 * 
	 * @var array
	 */
	private $fields = array(
		'id',
		'mails',
		'date_add',
	);
	
	private $feedbackQuestiosFields = array(
		"id",
		"questionen",
		"questionno",
		"questionsw"
	);

	/**
	 * Constructor
	 * 
	 * @return Contact_us_model
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return reason object by identifier
	 * 
	 * @param integer $reason_id reason identifier
	 * @return array
	 */
	public function get_reason_by_id($reason_id){
		$result = $this->DB->select(implode(", ", $this->fields))->from(CONTACT_REASONS_TABLE)->where("id", $reason_id)->get()->result_array();
		if(!empty($result)){
			$result = $this->format_reasons($result);
			$data = $result[0];
			return $data;
		}
		return array();
	}

	/**
	 * Return filteres reason objects as array
	 * 
	 * @param array $params filters parameters
	 * @param array $filter_object_ids filters identifiers
	 * @param array $order_by sorting data
	 * @return array
	 */
	public function get_reason_list($params=array(), $filter_object_ids=null, $order_by=null){
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(CONTACT_REASONS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		if(is_array($order_by) && count($order_by)>0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->fields_news)){
					$this->DB->order_by($field." ".$dir);
				}
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return $this->format_reasons($data);
		}
		return array();
	}

	/**
	 * Return number of filtered reason objects
	 * @param array $params filters parameters
	 * @param array $filter_object_ids filters identifiers
	 * @return integer
	 */
	public function get_reason_count($params=array(), $filter_object_ids=null){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(CONTACT_REASONS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	/**
	 * Save reason object to data source
	 * 
	 * @param integer $reason_id reason identifier
	 * @param array $data reason data
	 * @param array $langs languages data
	 * @return integer
	 */
	public function save_reason($reason_id, $data, $langs=null){
		if (is_null($reason_id)){
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(CONTACT_REASONS_TABLE, $data);
			$reason_id = $this->DB->insert_id();
		}else{
			$this->DB->where('id', $reason_id);
			$this->DB->update(CONTACT_REASONS_TABLE, $data);
		}

		if(!empty($langs)){
			$languages = $this->CI->pg_language->languages;
			if(!empty($languages)){
				foreach($languages as $language){
					$lang_ids[] = $language["id"];
				}
				$this->CI->pg_language->pages->set_string_langs('contact_us', "contact_us_reason_".$reason_id, $langs, $lang_ids);
			}
		}
		return $reason_id;
	}

	/**
	 * Validate reason data for saving to data source
	 * 
	 * @param integer $reason_id reason identifier
	 * @param array $data reason data
	 * @param array $langs languages data
	 * @return array
	 */
	public function validate_reason($reason_id, $data, $langs=null){
		$return = array("errors"=> array(), "data" => array(), 'temp' => array(), 'langs' => array());

		if(isset($data["mails"])){
			$data["mails"] = explode(',', $data["mails"]);
			foreach($data["mails"] as $k => $mail){
				$mail = trim(strip_tags($mail));
				if(!empty($mail)){
					$data["mails"][$k] = $mail;
				}else{
					unset($data["mails"][$k]);
				}
			}
			$return["data"]["mails"] = serialize($data["mails"]);
		}

		if(!empty($langs)){
			$default_lang_id = $this->CI->pg_language->current_lang_id;
			if(!isset($langs[$default_lang_id])){
				$return['errors'][] = l('error_empty_name', "contact_us");
			}else{
				$return["langs"][$default_lang_id] = trim(strip_tags($langs[$default_lang_id]));
				if(empty($return["langs"][$default_lang_id])){
					$return['errors'][] = l('error_empty_name', "contact_us");
				}else{
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang_id) continue;
						if(!isset($langs[$lid])){
							$return["langs"][$lid] = $return["langs"][$default_lang_id];
						}else{
							$return["langs"][$lid] = trim(strip_tags($langs[$lid]));
							if(empty($return["langs"][$lid])){
								$return["langs"][$lid] = $return["langs"][$default_lang_id];
							}	
						}
					}
				}
			}
		}
		return $return;
	}

	/**
	 * Remove reason data from data source
	 * 
	 * @param integer $reason_id reason identifier
	 * @return void
	 */
	public function delete_reason($reason_id){
		$this->DB->where("id", $reason_id);
		$this->DB->delete(CONTACT_REASONS_TABLE);

		$this->CI->pg_language->pages->delete_string("contact_us", "contact_us_reason_".$reason_id);
	}

	/**
	 * Format reasons data
	 * 
	 * @param array $data reasons data
	 * @return array
	 */
	public function format_reasons($data){
		foreach($data as $k => $reason){
			$reason["name"] = l('contact_us_reason_'.$reason["id"], 'contact_us');
			$reason["mails"] = unserialize($reason["mails"]);
			if(!empty($reason["mails"]) && is_array($reason["mails"])){
				$reason["mails_string"] = implode(", ", $reason["mails"]);
			}else{
				$reason["mails_string"] = "";
			}
			$data[$k] = $reason;
		}
		return $data;
	}

	/**
	 * Validate contact_us settings
	 * 
	 * @param array $data settings data
	 * @return array
	 */
	public function validate_settings($data){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data["default_alert_email"])){
			$return["data"]["default_alert_email"] = trim(strip_tags($data["default_alert_email"]));

			$this->CI->config->load('reg_exps', TRUE);
			$email_expr =  $this->CI->config->item('email', 'reg_exps');
			if(empty($return["data"]["default_alert_email"]) || !preg_match($email_expr, $return["data"]["default_alert_email"])){
				$return["errors"][] = l('error_default_alert_email_incorrect', 'contact_us');
			}
		}

		return $return;
	}
	
	////// ----------------------- Start feedback form -----------------------------
	
	public function validate_feedback_form($data)
	{
		$return = array("errors"=> array(), "data" => array());
		
		if(isset($data["agent"])){
			$return["data"]["agent"] = trim(strip_tags($data["agent"]));

			if(empty($return["data"]["agent"])){
				$strAgent = trim(strip_tags($data["agent2"]));
				if($strAgent == ""){	
					$return["errors"][] = 'Invalid agent name';
				}
			}
		}
		
		if(isset($data["agent2"])){
			$return["data"]["agent2"] = trim(strip_tags($data["agent2"]));

			if(empty($return["data"]["agent2"])){
				$return["data"]["agent2"] = '';
			}
		}
		
		if(isset($data["Message"])){
			$return["data"]["Message"] = trim(strip_tags($data["Message"]));

			if(empty($return["data"]["Message"])){
				$return["data"]["Message"] = '';
			}
		}
		
		if(isset($data["name"])){
			$return["data"]["name"] = trim(strip_tags($data["name"]));

			if(empty($return["data"]["name"])){
				$return["errors"][] = 'Invalid name';
			}
		}
		
		if(isset($data["email"])){
			$return["data"]["email"] = trim(strip_tags($data["email"]));

			$this->CI->config->load('reg_exps', TRUE);
			$email_expr =  $this->CI->config->item('email', 'reg_exps');
			if(empty($return["data"]["email"]) || !preg_match($email_expr, $return["data"]["email"])){
				$return["errors"][] = l('error_user_email_incorrect', 'contact_us');
			}
		}
		
		if(isset($data["captcha_code"])){
			$return["data"]["captcha_code"] = trim(strip_tags($data["captcha_code"]));

			if(empty($return["data"]["captcha_code"]) || $return["data"]["captcha_code"] != $_SESSION["captcha_word"]){
				$return["errors"][] = l('error_captcha_code_incorrect', 'contact_us');
			}
		}
		
		return $return;
	}
	
	public function save_feedback_form($data){					
		$feedback["question_score"] = json_encode($data["feedbackScore"]);
		$feedback["agent_id"] = $data["agent"];
		$feedback["agent_not_on_list"] = $data["agent2"];
		$feedback["user_id"] = ($data["user_id"] == "")? 0 :$data["user_id"];
		$feedback["name"] = $data["name"];
		$feedback["email"] = $data["email"];
		$feedback["message"] = $data["Message"];;
		$this->DB->insert(FEEDBACK_SCORING, $feedback);
	}
	
	public function get_feedback_questions()
	{
		$this->DB->select(implode(", ", $this->feedbackQuestiosFields))->from(QUESTIONS_TABLE);
		$result = $this->DB->get()->result_array();
		if(!empty($result)){
			return $result;
		}
		return array();
	}
	
	public function get_agent_list()
	{
		$result = $this->DB->select('id,unique_name')->from(USERS)->where("user_type", "company")->get()->result_array();
		
		if(!empty($result)){
			return $result;
		}
		return array();
	}
	
	////// ----------------------- End feedback form -----------------------------

	/**
	 * Return contact_us settings
	 * 
	 * @return array
	 */
	public function get_settings(){
		$data = array(
			"default_alert_email" => $this->CI->pg_module->get_module_config('contact_us', 'default_alert_email'),
		);
		return $data;
	}

	/**
	 * Save contact_us settings
	 * 
	 * @return void
	 */
	public function set_settings($data){
		foreach($data as $setting => $value){
			$this->CI->pg_module->set_module_config('contact_us', $setting, $value);
		}
	}

	/**
	 * Send contact_us mail
	 * 
	 * @param array $data message data
	 * @return array
	 */
	public function send_contact_form($data, $lang){
		$return = array("errors"=> array(), "data" => array());

		$this->CI->load->model('Notifications_model');

		if(!empty($data["reason_data"]) && !empty($data["reason_data"]["mails"])){
			$mails = $data["reason_data"]["mails"];
		}else{
			$mails[] = $this->CI->pg_module->get_module_config('contact_us', 'default_alert_email');
		}

		if(empty($mails)){
			$return["errors"][] = l('error_no_recipients', 'contact_us');
		}else{
			foreach($mails as $mail){
		 $send_data = $this->CI->Notifications_model->send_notification($mail, 'contact_us_form', $data, '', $this->pg_language->current_lang_id);
				if(!empty($send_data["errors"])){
					foreach($send_data["errors"] as $error) $return["errors"][] = $error;
				}
			}
		}
		return $return;
	}
	/**
	 * Validate contact_us form
	 * 
	 * @param array $data message data
	 * @return array
	 */
	public function validate_contact_form($data){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data["user_name"])){
			$return["data"]["user_name"] = trim(strip_tags($data["user_name"]));

			if(empty($return["data"]["user_name"])){
				$return["errors"][] = l('error_user_name_incorrect', 'contact_us');
			}
		}

		if(isset($data["user_email"])){
			$return["data"]["user_email"] = trim(strip_tags($data["user_email"]));

			$this->CI->config->load('reg_exps', TRUE);
			$email_expr =  $this->CI->config->item('email', 'reg_exps');
			if(empty($return["data"]["user_email"]) || !preg_match($email_expr, $return["data"]["user_email"])){
				$return["errors"][] = l('error_user_email_incorrect', 'contact_us');
			}
		}

		if(isset($data["subject"])){
			$return["data"]["subject"] = trim(strip_tags($data["subject"]));

			if(empty($return["data"]["subject"])){
				$return["errors"][] = l('error_subject_incorrect', 'contact_us');
			}
		}

		if(isset($data["message"])){
			$return["data"]["message"] = trim(strip_tags($data["message"]));

			if(empty($return["data"]["message"])){
				$return["errors"][] = l('error_message_incorrect', 'contact_us');
			}
		}

		if(isset($data["id_reason"])){
			$return["data"]["id_reason"] = intval($data["id_reason"]);
			if(!empty($return["data"]["id_reason"])){
				$return["data"]["reason_data"] = $this->get_reason_by_id($return["data"]["id_reason"]);
				$return["data"]["reason"] = $return["data"]["reason_data"]["name"];
			}else{
				$return["data"]["reason"] = l('no_reason_filled', 'contact_us');
			}
		}

		if(isset($data["captcha_code"])){
			$return["data"]["captcha_code"] = trim(strip_tags($data["captcha_code"]));

			if(empty($return["data"]["captcha_code"]) || $return["data"]["captcha_code"] != $_SESSION["captcha_word"]){
				$return["errors"][] = l('error_captcha_code_incorrect', 'contact_us');
			}
		}

		$data["data"]["form_date"] = date("Y-m-d H:i:s");
		return $return;
	}
	
	//// -------------------------------- Start Properity Calculation ---------------------------------
	public function get_country()
	{
		$result = $this->DB->select('id,code,name')->from(COUNTRIES_TABLE)->where("code", "ES")->get()->result_array();
		if(!empty($result)){
			return $result;
		}
		return array();
	}
	
	public function get_regoin()
	{
		$result = $this->DB->select('id,country_code,name')->from(REGIONS_TABLE)->where("country_code", "ES")->order_by("name ASC")->get()->result_array();
		if(!empty($result)){
			return $result;
		}
		return array();
	}
	
	/*public function get_version_setting($tab_id)
	{
		$data = array();
		$query = "SELECT * FROM " . VERSION_TABLE . " WHERE VersionType='".$tab_id."'";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data[0];
	}*/
	
	public function get_version_setting($type)
	{
		$result = $this->DB->select('Id,Month,Year')->from(VERSION_TABLE)->where("VersionType", $type)->get()->result_array();
		if(!empty($result)){
			return $result;
		}
		return array();
	}

	public function get_city_price($cityId)
	{
		$version = $this->get_version_setting('Placefactors');
		$month = $version[0]['Month'];
		$year = $version[0]['Year'];
		
		$factors_graph = $this->get_factor_graph_setting();
		
		$city_price = array();
		
		if($factors_graph['functionality'] == 'on') {
			$query = "SELECT capg.apartment_price as appartment_price, chpg.house_price as house_price FROM
					  " . CITY_APARTMENTS_PRICES_GRAPH . " capg INNER JOIN 
					  " . CITY_HOUSES_PRICES_GRAPH . " chpg ON capg.id_city = chpg.id_city
					   AND capg.date = chpg.date
					   WHERE capg.id_city='" . $cityId . "' AND capg.date = '" . $factors_graph['date'] . "'";
		} else {
			$query = "SELECT appartment_price,house_price FROM ".CITY_PRICES_TABLE." WHERE Month = '".$month."' AND Year = '".$year."' AND id='".$cityId."'";
		}
		
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$city_price[] = $rt;
			}
		}
		return $city_price[0];
	}
	
	public function get_district_price($cityId, $districtId)
	{
		$version = $this->get_version_setting('Placefactors');
		$month = $version[0]['Month'];
		$year = $version[0]['Year'];
		$data = array();
		$query = "SELECT adjustment FROM ".DISTRICTS_ADJUSTMENT_TABLE." WHERE Month='".$month."' AND Year='".$year."' AND id_city = '".$cityId."' AND id_district='".$districtId."'";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data[0];
	}
	
	public function get_factor_graph_setting()
	{
		$data = array();
		$query = "SELECT functionality, date FROM " .FACTORS_GRAPH_TABLE. " WHERE id = 1";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data[0];
	}
	
	public function get_factors($query)
	{
		$data = array();
		$result = $this->DB->query($query)->result_array();
		if(!empty($result)){
			foreach($result as &$val) {
				$data = array_values($val);
			}
		}
		
		
		return $data;
	}
	
	//// --------------------------------- End Properity Calculation ----------------------------------
	
	//// --------------------------------- Start Properity Calculation Graph ----------------------------------
	
	public function get_graph_query($query)
	{
		$data = array();
		$result = $this->DB->query($query)->result_array();
		if(!empty($result)){
			foreach($result as &$val) {
				 array_push($data,array_values($val));
			}
		}
		return $data;
	}
	
	public function get_row_custom_query($query)
	{
		$data = array();
		$result = $this->DB->query($query)->result_array();
		if(!empty($result)){
			foreach($result as &$val) {
				$data = array_values($val);
			}
		}
		return $data;
	}
	//// --------------------------------- End Properity Calculation  Graph ----------------------------------

	////// seo
	
	/**
	 * Return seo settings
	 * 
	 * @param string $method method name
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	function get_seo_settings($method='', $lang_id=''){
		if(!empty($method)){
			return $this->_get_seo_settings($method, $lang_id);
		}else{
			$actions = array('index');
			$return = array();
			foreach($actions as $action){
				$return[$action] = $this->_get_seo_settings($action, $lang_id);
			}
			return $return;
		}
	}

	/**
	 * Return seo settings
	 * 
	 * @param string $method method name
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	function _get_seo_settings($method, $lang_id=''){
		if($method == "index"){
			return array(
				"templates" => array(),
				"url_vars" => array()
			);
		}
	}

	/**
	 * Replace seo parameters values
	 * 
	 * @param string $var_name_from variable from name
	 * @param string $var_name_to variable to name
	 * @param mixed $value parameter value
	 * @return mixed
	 */
	function request_seo_rewrite($var_name_from, $var_name_to, $value){
		$user_data = array();

		if($var_name_from == $var_name_to){
			return $value;
		}
	}

	/**
	 * Return data for xml sitemap
	 * 
	 * @return array
	 */
	function get_sitemap_xml_urls(){
		return array();
	}

	/**
	 * Return data for sitemap page
	 * 
	 * @return array
	 */
	function get_sitemap_urls(){
		$this->CI->load->helper('seo');
		$auth = $this->CI->session->userdata("auth_type");

		$block[] = array(
			"name" => l('header_contact_us_form', 'contact_us'),
			"link" => rewrite_link('contact_us', 'index'),
			"clickable" => true,
		);
		return $block;
	}

	////// banners callback method
	
	/**
	 * Return banner available pages of contact_us
	 * 
	 * @return array
	 */
	public function _banner_available_pages(){
		$return[] = array("link"=>"contact_us/index", "name"=> l('header_contact_us_form', 'contact_us'));
		$return[] = array("link"=>"contact_us/feedback", "name"=> "Feedback");
		$return[] = array("link"=>"contact_us/properties", "name"=> "Properties");
		return $return;
	}

}

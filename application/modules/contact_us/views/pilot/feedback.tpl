{include file="header.tpl"}
{include file="left_panel.tpl" module=start}
<div class="rc">
{js file='jquery-ui.js'}
<script>{literal}
$(function() {
var arraytxt1 = {/literal}{$agentslist}{literal};
$("#agentName").autocomplete({
		source: arraytxt1,
		minLength: 1,
		select: function(event, ui) {
			$("#agent").val(ui.item.id);
			// Corresponding Text Box 2 value to selected.
		}
	});
});
{/literal}
</script>
	<div class="content-block">
		<h1>{l i='fd_title' gid='contact_us'}</h1>

		<div class="content-value">
		<p>{l i='fd_info' gid='contact_us'}</p>
<div class="edit_block">
	<div class="feedback" style="width:100%;">
    	<form action="" method="post">
            <div style="width:100%;">
                <div class="divh_1">{l i='fd_real_estate_agency' gid='contact_us'} <font class="error">*</font></div>
                <div class="divh_2">
                    {l i='fd_agent_name' gid='contact_us'}
                    <input type="text" class="str" style="width: 90%; color: gray" name="agent" id="agentName" value="" onfocus="this.removeAttribute('readonly');" readonly />
                    <input type="hidden" name="agent" id="agent" value="">
                </div>
                <div class="divh_3">
                    {l i='fd_not_agent_name' gid='contact_us'}
                    <input type="text" class="str"  style="width:90%;" name="agent2" value="" onfocus="this.removeAttribute('readonly');" readonly>
                </div>
                <div style="clear:both"></div>
                <div style="border-bottom: 1px dashed #cccccc; width:100%; margin:10px 0;"></div>
                <div class="divfeedtitle">
                	<div class="divfeedfirst">&nbsp;</div>
                    <div class="divfeedoptn">{l i='fd_status_1' gid='contact_us'}</div>
                    <div class="divfeedoptn">{l i='fd_status_2' gid='contact_us'}</div>
                    <div class="divfeedoptn">{l i='fd_status_3' gid='contact_us'}</div>
                    <div class="divfeedoptn">{l i='fd_status_4' gid='contact_us'}</div>
                    <div class="divfeedoptn">{l i='fd_status_5' gid='contact_us'}</div>
                    <div class="divfeedoptn">{l i='fd_status_6' gid='contact_us'}</div>
                </div>
                {foreach item=item from=$feedback}
                <div style="clear:both"></div>
                <div class="divfeedqus">
                    {if $current_lang eq '1'}
                	<div class="divfeedfirst">{$item.questionen}</div>
                    {elseif $current_lang eq '9'}
                    <div class="divfeedfirst">{$item.questionno}</div>
                    {elseif $current_lang eq '10'}
                    <div class="divfeedfirst">{$item.questionsw}</div>
                    {/if}
                    <div class="divfeedoptn"><label><input type="radio" name="feedback_{$item.id}" value="1"><span>{l i='fd_status_1' gid='contact_us'}</span></label></div>
                    <div class="divfeedoptn"><label><input type="radio" name="feedback_{$item.id}" value="2"><span>{l i='fd_status_2' gid='contact_us'}</span></label></div>
                    <div class="divfeedoptn"><label><input type="radio" name="feedback_{$item.id}" value="3"><span>{l i='fd_status_3' gid='contact_us'}</span></label></div>
                    <div class="divfeedoptn"><label><input type="radio" name="feedback_{$item.id}" value="4"><span>{l i='fd_status_4' gid='contact_us'}</span></label></div>
                    <div class="divfeedoptn"><label><input type="radio" name="feedback_{$item.id}" value="5"><span>{l i='fd_status_5' gid='contact_us'}</span></label></div>
                    <div class="divfeedoptn"><label><input type="radio" name="feedback_{$item.id}" value="0"><span>{l i='fd_status_6' gid='contact_us'}</span></label></div>
                </div>
                {/foreach}
                <div style="clear:both;width:100%">
                	<div class="divfeedl">{l i='fd_lbl_your_message' gid='contact_us'}</div>
                    <div class="divfeedr"><textarea name="Message" class="txtfeedwidth" style="height:150px;" rows=10></textarea></div>
                </div>
                <div style="clear:both;width:100%">
                	<div class="divfeedl">{l i='fd_lbl_your_name' gid='contact_us'} <font class="error">*</font></div>
                    <div class="divfeedr"><input type="text" class="txtfeedwidth" name="name" value="" onfocus="this.removeAttribute('readonly');" readonly></div>
                </div>
                <div style="clear:both;width:100%">
                	<div class="divfeedl">{l i='fd_lbl_email' gid='contact_us'} <font class="error">*</font></div>
                    <div class="divfeedr"><input type="text" class="txtfeedwidth" name="email" value="" onfocus="this.removeAttribute('readonly');" readonly></div>
                </div>
                <div style="clear:both;width:100%">
                	<div class="divfeedl">{l i='field_contact_captcha' gid='contact'} <font class="error">*</font></div>
                    <div class="divfeedr">{$data.captcha}<br />
                    <input type="text" name="keystring" id="keystring" style="" onfocus="this.removeAttribute('readonly');" readonly></div>
                </div>
                <div style="clear:both;width:100%">
                	<div class="divfeedl">&nbsp;</div>
                    <div class="divfeedr"><input type="submit" class='btn' value="{l i='fd_btn_send' gid='contact_us'}" name="btn_save_feedback"></div>
                </div>
            </div>
        </form>
    </div>
</div>

<!--<div class="edit_block">
    <form action="" method="post">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="tablefeedback">
            <tr>
            	<td width="12">&nbsp;</td>
                <td valign="top">
            <table class="feedback" cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td rowspan="2" class="first">Real Estate Agency: <font class="error">*</font></td>
                    <td class="second">&nbsp;</td>									
                    <td style="text-align: left; padding-left: 5px; width: 100px;">Write the first letters in the name:</td>
                    <td style="text-align: left; padding-left: 5px; width: 180px;">Write the agent‘s name if not in the list:</td>								
                </tr>
                <tr>
                    <td class="second">&nbsp;</td>							
                    <td>
        				<input type="text" class="str" style="width: 250px; color: gray" name="agent" id="agentName" value=""  />
    					<input type="hidden" name="agent" id="agent" value="">
                    </td>
                    <td>
                        <input type="text" class="str" size="45" name="agent2" value="">
                    </td>								
                </tr>
                        
                                   
            <tr height="10"><td colspan="9" style="border-bottom: 1px dashed #cccccc;"></td></tr>
             <tr height="5"><td colspan="9"></td></tr>
             <tr>
                <td colspan="9" class="skinned-form-controls skinned-form-controls-mac">
                	<table class="feedback" cellpadding="0" cellspacing="3" border="0">
                    	<tr>
                            <td class="first">&nbsp;</td>
                            <td>Not satisfied </td>
                            <td>Below expectations</td>
                            <td>As expected</td>
                            <td>Satisfied</td>
                            <td>Very Satisfied</td>
                            <td>Not applicable</td>
                        </tr>
                        {foreach item=item from=$feedback}							
                        <tr>
                            <td class="first" >{$item.question}</td>
                            <td>
                                <label><input type="radio" name="feedback_{$item.id}" value="1"><span></span></label>
                            </td>
                            <td>
                                <label><input type="radio" name="feedback_{$item.id}" value="2"><span></span></label>
                            </td>
                            <td>
                                <label><input type="radio" name="feedback_{$item.id}" value="3"><span></span></label>
                            </td>
                            <td>
                                <label><input type="radio" name="feedback_{$item.id}" value="4"><span></span></label>
                            </td>
                            <td>
                                <label><input type="radio" name="feedback_{$item.id}" value="5"><span></span></label>
                            </td>
                            <td>
                                <label><input type="radio" name="feedback_{$item.id}" value="0"><span></span></label>
                            </td>
                        <input type='hidden' name='feedback_radio[]' value='{$item.id}'>
                        </tr>
                    {/foreach}
                    </table>
                </td>
             </tr>
            <td class="first" colspan="2" style="text-align:left;">Your Message:</td>
            <td colspan="8" class="third" style="text-align:left;"><textarea name="Message" style="width: 512px; height:150px;" rows=10></textarea></td>
            </tr>
            <tr>
                <td class="first" colspan="2" style="text-align:left;">Your name: <font class="error">*</font></td>
                <td colspan="8" class="third" style="text-align:left;"><input type="text" style="width:512px;" name="name" value=""></td>
            </tr>
            <tr>
                <td class="first" colspan="2" style="text-align:left;">Email: <font class="error">*</font></td>
                <td colspan="8" class="third" style="text-align:left;"><input type="text" style="width:512px;" name="email" value=""></td>
            </tr>
            <tr>
                <td class="first" colspan="2" style="text-align:left;">PIN-code: <font class="error">*</font></td>
                <td colspan="8" class="third" style="text-align:left;">{$data.captcha}<br />
                    <input type="text" name="keystring" id="keystring" style="">
                </td>								
            </tr>
            <tr>
            	<td class="first" colspan="2">
                <td colspan="8" class="third" style="text-align:left;"><input type="submit" class='btn' value="Send" name="btn_save_feedback"></td>
            </tr>	
        </table>
        </table>
    </form>

</div>-->

	</div>
	</div>

	{block name=show_social_networks_like module=social_networking}
	{block name=show_social_networks_share module=social_networking}
	{block name=show_social_networks_comments module=social_networking}
	<br><br><br>
</div>
<div class="clr"></div>
{include file="footer.tpl"}

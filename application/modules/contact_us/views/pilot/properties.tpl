{include file="header.tpl"}
<div class="properties_calc_rc">
    <div class="content-block">
        <h1 style="text-align:center; padding-top:10px;">{l i='calc_header_title' gid='content'}</h1>
        <div class="content-value">
            <div class="edit_block properties_calc">
            <form action="" method="post">
            	<div class="divmain1">
                	<div class="divmain1_left">
                    	<div class="divTitle">
                            <div class="locCalcimg">{l i='calc_title_location' gid='content'}</div>
                        </div>
                        <table class="tblCalc">
                        	<tr>
                            	<td>
                                	{l i='calc_lbl_country' gid='content'}<br />
                                    <select name="country_select" id="country_select" class="selCalc">
                                        {foreach from=$country_list key="key" item="country"}
                                            <option value="{$country.code}" {if $country.code eq 'ES'} selected {/if} >{$country.name}</option>
                                        {/foreach}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	{l i='calc_lbl_province' gid='content'}<br />
                                    <select name="region_select" id="region_select" onchange="javascript:getCity(this);" class="selCalc">
                                        <option value="0">{l i='calc_option_choose_province' gid='content'}</option>
                                        {foreach from=$region_list key="key" item="region"}
                                            <option value="{$region.id}">{$region.name}</option>
                                        {/foreach}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	{l i='calc_lbl_city' gid='content'}<br />
                                    <select name="city_select" id="city_select" onchange="javascript:getArea(this);" class="selCalc">
                                        <option value="0">{l i='calc_option_choose_city' gid='content'}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	{l i='calc_lbl_area' gid='content'}<br />
                                    <select name="area_select" id="area_select" class="selCalc">
                                        <option value="0">{l i='calc_option_select_area' gid='content'}</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="divmain1_right">
                    	<div class="divTitle">
                        	<div class="propertyCalcimg">{l i='calc_title_property' gid='content'}</div>
                        </div>
                    	<table class="tblCalc tblCalc2">
                        	<tr>
                            	<td>
                                	{l i='calc_lbl_property_type' gid='content'}<br />
                                    <select onchange="onChangeType(this.value)" id="property_type" name="property_type" class="selCalc">
                                        <option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">{l i='calc_option_apartment' gid='content'}</option>
                                        <option value="2">{l i='calc_option_house' gid='content'}</option>
                                     </select>
                                </td>
                                <td class="td2Calc2">&nbsp;</td>
                            </tr>
                            <tr>
                            	<td>
                                	{l i='calc_lbl_year_build' gid='content'}<br />
                                    <select name="years_finished" id="years_finished" class="selCalc" onchange="onChangeYearsFinished(this.selectedIndex)">
                                        <option value="0">{l i='calc_option_select' gid='content'}</option>
                                        {foreach from=$Years_Option key="key" item="years"}
                                            <option value="{$years.id}">{$years.name}</option>
                                        {/foreach}
                                    </select>
                                </td>
                                <td class="td2Calc2">
                                	{l i='calc_lbl_year_renovated' gid='content'}<br />
                                    <select name="years_renovated" id="years_renovated" class="selCalc">
                                        <option value="0">{l i='calc_option_select' gid='content'}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	{l i='calc_lbl_bedrooms' gid='content'}<br />
                                    <select id="bedrooms" name="bedrooms" class="selCalc">
                                        <option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6+</option>                                
                                    </select>
                                </td>
                                <td class="td2Calc2">
                                	{l i='calc_lbl_bathrooms' gid='content'}<br />
                                    <select id="bathrooms" name="bathrooms" class="selCalc">
                                        <option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5+</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	{l i='calc_lbl_living_size_sqm' gid='content'}<br />
                                    <input type="text" id="living" name="living" class="txtCalc" />
                                </td>
                                <td class="td2Calc2">
                                	{l i='calc_lbl_sqm_terrasses_total' gid='content'}<br />
                                    <input type="text" id="sqm_terrasse" name="sqm_terrasse" class="txtCalc" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="clear:both"></div>
                    <div class="divsub">
                    	<div class="divTitle">
                        	<div class="benefitsCalcimg">{l i='calc_title_benefits' gid='content'}</div>
                        </div>
                        <table class="tblCalc tblCalc3">
                        	<tr>
                            	<td class="first">
                                	<div class="divlbl" id="selectone{$current_lang}">{l i='calc_lbl_walking_distance_beach' gid='content'}</div>
                                    <select name="beach" id="beach" class="selCalc">
                                    	<option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">{l i='calc_option_1st_line' gid='content'}</option>
                                        <option value="2">{l i='calc_option_lessthen_15min_walk' gid='content'}</option>
                                        <option value="3">{l i='calc_option_morethan_15minutes_walk' gid='content'}</option>
                                    </select>
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third">
                                	<div id="selecttwo{$current_lang}" class="divlbl">{l i='calc_lbl_view_from_property' gid='content'}</div>
                                    <select name="view" id="view" class="selCalc">
                                    	<option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">{l i='calc_option_sea' gid='content'}</option>
                                        <option value="2">{l i='calc_option_mountain_garden' gid='content'}</option>
                                        <option value="3">{l i='calc_option_city_street' gid='content'}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td class="first">
                                	<div id="selectthree{$current_lang}" class="divlbl">{l i='calc_lbl_access_benefits' gid='content'}</div>
                                    <input type="hidden" name="tennis" id="tennis" value="" />
                                    <input type="hidden" name="football" id="football" value="" />
                                    <input type="hidden" name="golf" id="golf" value="" />
                                    
                                    {js file='multiple-select'}
                                    
                                    <select id="ms" multiple="multiple" >
                                        <option value="tennis">{l i='calc_option_tennis' gid='content'}</option>
                                        <option value="football">{l i='calc_option_football' gid='content'}</option>
                                        <option value="golf">{l i='calc_option_golf' gid='content'}</option>
        							</select>
                                    
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third">
                                	<div id="selectfour{$current_lang}" class="divlbl">{l i='calc_lbl_walking_distance_10min' gid='content'}</div>
                                     <select id="ms_walking_distance" multiple="multiple">
                                        <option value="buss">{l i='calc_option_buss' gid='content'}</option>
                                        <option value="supermarket">{l i='calc_option_supermarket' gid='content'}</option>
                                        <option value="restaurants">{l i='calc_option_restaurants' gid='content'}</option>
        							</select>
                                    
                                    <input type="hidden" name="buss" id="buss" value="" />
                                    <input type="hidden" name="supermarket" id="supermarket" value="" />
                                    <input type="hidden" name="restaurants" id="restaurants" value="" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                    <div class="divsub" id="appartment" style="display:none;">
                    	<div class="divTitle">
                        	<div class="appartmentCalcimg">{l i='calc_title_apartments' gid='content'}</div>
                        </div>
                        <table class="tblCalc tblCalc3">
                        	<tr>
                            	<td class="first">
                                	<div id="selectfive{$current_lang}">{l i='calc_lbl_apartment_type' gid='content'}</div>
                                    <select name="type" id="type" class="selCalc">
                                    	<option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">{l i='calc_option_ground_floor' gid='content'}</option>
                                        <option value="2">{l i='calc_option_low_floor' gid='content'}</option>
                                        <option value="3">{l i='calc_option_middle' gid='content'}</option>
                                        <option value="4">{l i='calc_option_upper' gid='content'}</option>
                                        <option value="5">{l i='calc_option_duplex' gid='content'}</option>
                                        <option value="6">{l i='calc_option_penthouse' gid='content'}</option>
                                    </select>
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third"></td>
                            </tr>
                            <tr>
                            	<td class="first">
                                	<div id="selectsix{$current_lang}">{l i='calc_lbl_quality_standard' gid='content'}</div>
                                    <select name="standard" id="standard" class="selCalc">
                                    	<option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">{l i='calc_option_low' gid='content'}</option>
                                        <option value="2">{l i='calc_option_medium' gid='content'}</option>
                                        <option value="3">{l i='calc_option_high' gid='content'}</option>
                                    </select>
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third">
                                	<div id="selectseven{$current_lang}">{l i='calc_lbl_swimmingpool' gid='content'}</div>
                                    <select name="pool" id="pool" class="selCalc">
                                    	<option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">{l i='calc_option_common' gid='content'}</option>
                                        <option value="2">{l i='calc_option_private' gid='content'}</option>
                                        <option value="3">{l i='calc_option_none' gid='content'}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td class="first">
                                	<div id="checkone{$current_lang}" class="divlblrd">{l i='calc_lbl__turistico_apartment' gid='content'}</div>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="turistico" class="onoffswitch-checkbox" id="turistico">{l i='calc_lbl_yes' gid='content'}
                                        <input type="checkbox" name="turistico" class="onoffswitch-checkbox" id="turistico1">{l i='calc_lbl_no' gid='content'} 
                                    </div>
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third">
                                    <div id="checktwo{$current_lang}" class="divlblrd">{l i='calc_lbl_roof_terrace' gid='content'}</div>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="terrasse_solarium" class="onoffswitch-checkbox" id="terrasse_solarium">{l i='calc_lbl_yes' gid='content'}
                                        <input type="checkbox" name="terrasse_solarium" class="onoffswitch-checkbox" id="terrasse_solarium1">{l i='calc_lbl_no' gid='content'}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                            	<td class="first">
                                    <div id="checkthree{$current_lang}" class="divlblrd">{l i='calc_lbl_elevator' gid='content'}</div>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="elevator" class="onoffswitch-checkbox" id="elevator">{l i='calc_lbl_yes' gid='content'}
                                         <input type="checkbox" name="elevator" class="onoffswitch-checkbox" id="elevator1">{l i='calc_lbl_no' gid='content'}
                                    </div>
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third">
                                    <div id="checkfour{$current_lang}" class="divlblrd">{l i='calc_lbl_terrace_south' gid='content'}</div>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="terrasse_south" class="onoffswitch-checkbox" id="terrasse_south">{l i='calc_lbl_yes' gid='content'}
                                         <input type="checkbox" name="terrasse_south" class="onoffswitch-checkbox" id="terrasse_south1">{l i='calc_lbl_no' gid='content'}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                            	<td class="first">
                                    <div id="checkfive{$current_lang}" class="divlblrd">{l i='calc_lbl_terrase' gid='content'}</div>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="terrasse" class="onoffswitch-checkbox" id="terrasse">{l i='calc_lbl_yes' gid='content'}
                                        <input type="checkbox" name="terrasse" class="onoffswitch-checkbox" id="terrasse1">{l i='calc_lbl_no' gid='content'}
                                    </div>
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third">
                                    <div id="checksix{$current_lang}" class="divlblrd">{l i='calc_lbl_terrace_west' gid='content'}</div>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="terrasse_west" class="onoffswitch-checkbox" id="terrasse_west">{l i='calc_lbl_yes' gid='content'}
                                        <input type="checkbox" name="terrasse_west" class="onoffswitch-checkbox" id="terrasse_west1">{l i='calc_lbl_no' gid='content'}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                            	<td class="first">
                                    <div id="checkseven{$current_lang}" class="divlblrd">{l i='calc_lbl_barbeque_on_terrasse' gid='content'}</div>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="terrasse_barbeque" class="onoffswitch-checkbox" id="terrasse_barbeque">{l i='calc_lbl_yes' gid='content'}
                                        <input type="checkbox" name="terrasse_barbeque" class="onoffswitch-checkbox" id="terrasse_barbeque1">{l i='calc_lbl_no' gid='content'}
                                    </div>
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third">
                                    <div id="checkeight{$current_lang}" class="divlblrd">{l i='calc_lbl_garage' gid='content'}</div>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="garage" class="onoffswitch-checkbox" id="garage">{l i='calc_lbl_yes' gid='content'}
                                        <input type="checkbox" name="garage" class="onoffswitch-checkbox" id="garage1">{l i='calc_lbl_no' gid='content'}
                                    </div>
                                </td>
                            </tr>
                      </table>
                    </div>
                    <div class="divsub" id="house" style="display:none;">
                        <div class="divTitle">
                            <div class="hoseCalcimg">{l i='calc_title_house' gid='content'}</div>
                        </div>
                        <table class="tblCalc tblCalc3">
                        	<tr>
                            	<td class="first">
                                	<div id="selecteight{$current_lang}">{l i='calc_lbl_house_type' gid='content'}</div>
                                    <select id="type_house" name="type_house" class="selCalc">
                                        <option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">{l i='calc_option_villa' gid='content'}</option>
                                        <option value="2">{l i='calc_option_Semidetached_villa' gid='content'}</option>
                                        <option value="3">{l i='calc_option_finca' gid='content'}</option>
                                    </select>
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third">&nbsp;</td>
                            </tr>
                            <tr>
                            	<td class="first">
                                	<div id="selectnine{$current_lang}">{l i='calc_lbl_garage' gid='content'}</div>
                                    <select id="garage_house" name="garage_house" class="selCalc">
                                        <option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">{l i='calc_option_car_port' gid='content'}</option>
                                        <option value="2">{l i='calc_option_garage' gid='content'}</option>
                                        <option value="3">{l i='calc_option_no_car' gid='content'}</option>
                                    </select>
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third">
                                	<div id="selectten{$current_lang}">{l i='calc_lbl_swimmingpool' gid='content'}</div>
                                    <select name="pool_house" id="pool_house" class="selCalc">
                                    	<option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">{l i='calc_option_common' gid='content'}</option>
                                        <option value="2">{l i='calc_option_private' gid='content'}</option>
                                        <option value="3">{l i='calc_option_none' gid='content'}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td class="first">
                                	<div id="selecteleven{$current_lang}">{l i='calc_lbl_plot' gid='content'}</div>
                                    <select name="plot" id="plot" class="selCalc">
                                    	<option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">{l i='calc_option_less_than_500sqm' gid='content'}</option>
                                        <option value="2">{l i='calc_option_1500sqm' gid='content'}</option>
                                        <option value="3">{l i='calc_option_3000sqm' gid='content'}</option>
                                    </select>
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third">
                                	<div id="selecttwelve{$current_lang}">{l i='calc_lbl_quality_standard' gid='content'}</div>
                                    <select name="standard_house" id="standard_house" class="selCalc">
                                    	<option value="0">{l i='calc_option_select' gid='content'}</option>
                                        <option value="1">{l i='calc_option_low' gid='content'}</option>
                                        <option value="2">{l i='calc_option_medium' gid='content'}</option>
                                        <option value="3">{l i='calc_option_high' gid='content'}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td class="first">
                                    <div id="checknine{$current_lang}" class="divlblrd">{l i='calc_lbl_garden' gid='content'}</div>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="garden" class="onoffswitch-checkbox" id="garden">{l i='calc_lbl_yes' gid='content'}
                                        <input type="checkbox" name="garden" class="onoffswitch-checkbox" id="garden1">{l i='calc_lbl_no' gid='content'}
                                    </div>
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third">
                                    <div id="checkten{$current_lang}" class="divlblrd">{l i='calc_lbl_one_floor_only' gid='content'}</div>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="one_floor" class="onoffswitch-checkbox" id="one_floor">{l i='calc_lbl_yes' gid='content'}
                                        <input type="checkbox" name="one_floor" class="onoffswitch-checkbox" id="one_floor1">{l i='calc_lbl_no' gid='content'}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                            	<td class="first">
                                    <div id="checkeleven{$current_lang}" class="divlblrd">{l i='calc_lbl_barbeque_in_garden' gid='content'}</div>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="barbeque_garden" class="onoffswitch-checkbox" id="barbeque_garden">{l i='calc_lbl_yes' gid='content'}
                                        <input type="checkbox" name="barbeque_garden" class="onoffswitch-checkbox" id="barbeque_garden1">{l i='calc_lbl_no' gid='content'}
                                    </div>
                                </td>
                                <td class="second">&nbsp;</td>
                                <td class="third">
                                    <div id="checktwelve{$current_lang}" class="divlblrd">{l i='calc_lbl__self_owned_plot' gid='content'}</div>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="owned" class="onoffswitch-checkbox" id="owned">{l i='calc_lbl_yes' gid='content'}
                                        <input type="checkbox" name="owned" class="onoffswitch-checkbox" id="owned1">{l i='calc_lbl_no' gid='content'}
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="margin-top:20px; display:none;; text-align:center;" id="calcResult">
                        <input type="button" value="{l i='calc_btn_text' gid='content'}" class="btnCalc" name="search_button" id="search_button" onclick="javascript: {literal}Calculate();{/literal}">
                        <div class="divResult">
                        {l i='calc_lbl_market_value' gid='content'}<br />
                        <span id="Result"></span></div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

{literal}
<script>
    $(function() {
        $('#ms').change(function() {
			var selectedvalue = $(this).val();

			if (selectedvalue == null){
				$('#tennis').val('');
				$('#football').val('');
				$('#golf').val('');
			}
			else{
				var res = selectedvalue.toString().split(',');
					$('#tennis').val('');
					$('#football').val('');
					$('#golf').val('');
					 for (var i = 0; i < res.length; i++) {
						 if( res[i] == 'tennis'){$('#tennis').val('1');}
						 if( res[i] == 'football'){$('#football').val('1');}
						 if( res[i] == 'golf'){$('#golf').val('1');}
					}
				}
        }).multipleSelect({
            width: '100%',
			placeholder: "{/literal}{l i='calc_option_select' gid='content'}{literal}"
        });
		
		
		
		 $('#ms_walking_distance').change(function() {
			var selectedvalue = $(this).val();

			if (selectedvalue == null){
				$('#buss').val('');
				$('#supermarket').val('');
				$('#restaurants').val('');
			}
			else{
				var res = selectedvalue.toString().split(',');
					$('#buss').val('');
					$('#supermarket').val('');
					$('#restaurants').val('');
					 for (var i = 0; i < res.length; i++) {
						 if( res[i] == 'buss'){$('#buss').val('1');}
						 if( res[i] == 'supermarket'){$('#supermarket').val('1');}
						 if( res[i] == 'restaurants'){$('#restaurants').val('1');}
					}
				}
        }).multipleSelect({
            width: '100%',
			placeholder: "{/literal}{l i='calc_option_select' gid='content'}{literal}"
        });

    });
	
	function getCity(selRegion)
	{
		$.ajax({
			url: '{/literal}{$site_root}{literal}countries/ajax_get_cities/' + selRegion.value,
			dataType: 'json',
			type: 'GET',
			data: {},
			cache: false,
			success: function(data){
				$('#city_select').find("option:gt(0)").remove();
				for(var id in data.items){
					$('#city_select').append('<option value="'+data.items[id].id+'">'+data.items[id].name+'</option>');
				}
			}
		});
	}
	
	function getArea(selArea)
	{
		$.ajax({
			url: '{/literal}{$site_root}{literal}countries/ajax_get_districts/' + selArea.value,
			dataType: 'json',
			type: 'GET',
			data: {},
			cache: false,
			success: function(data){
				$('#area_select').find("option:gt(0)").remove();
				for(var id in data.items){
					$('#area_select').append('<option value="'+data.items[id].id+'">'+data.items[id].name+'</option>');
				}
			}
		});
	}
	
	function onChangeType(id) 
	{
		if(id == 0)
		{
			$('#appartment').css("display", "none");
			$('#house').css("display", "none");
			$('#calcResult').css("display", "none");
			$('#Result').html('');
		}
		if(id == 1)
		{
			$('#appartment').css("display", "block");
			$('#house').css("display", "none");
			$('#calcResult').css("display", "block");
			$('#Result').html('');
		}
		if(id == 2)
		{
			$('#appartment').css("display", "none");
			$('#house').css("display", "block");
			$('#calcResult').css("display", "block");
			$('#Result').html('');
		}
	}
	
	function onAccessBenefits(val)
	{
		$('#tennis').val('');
		$('#football').val('');
		$('#golf').val('');
		if(val != 0)
		{
			$('#'+val).val('1');
		}
	}
	
	function onWalkingDistance(val)
	{
		$('#buss').val('');
		$('#supermarket').val('');
		$('#restaurants').val('');
		if(val != 0)
		{
			$('#'+val).val('1');
		}
	}
	
	$(function(){
		$('input[name=turistico]').change(function() {	
		  var th = $(this), name = th.prop('name'); 
           if(th.is(':checked')){
		   $(this).val("1");
           $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false).val("0");   
           }else{
			   $(this).val("0");
		   }
		 });
		
		$('input[name=terrasse_solarium]').change(function() {
		   var th = $(this), name = th.prop('name'); 
           if(th.is(':checked')){
		   $(this).val("1");
           $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false).val("0");   
           }else{
			   $(this).val("0");
		   }
		});
		
		$('input[name=elevator]').change(function() {
		   var th = $(this), name = th.prop('name'); 
           if(th.is(':checked')){
		   $(this).val("1");
           $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false).val("0");   
           }else{
			   $(this).val("0");
		   }
		});
		
		$('input[name=terrasse_south]').change(function() {
		   var th = $(this), name = th.prop('name'); 
           if(th.is(':checked')){
		   $(this).val("1");
           $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false).val("0");   
           }else{
			   $(this).val("0");
		   }
		});
		
		$('input[name=terrasse]').change(function() {
		  var th = $(this), name = th.prop('name'); 
           if(th.is(':checked')){
		   $(this).val("1");
           $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false).val("0");   
           }else{
			   $(this).val("0");
		   }
		});
		
		$('input[name=terrasse_west]').change(function() {
		   var th = $(this), name = th.prop('name'); 
           if(th.is(':checked')){
		   $(this).val("1");
           $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false).val("0");   
           }else{
			   $(this).val("0");
		   }
		});
		
		$('input[name=terrasse_barbeque]').change(function() {
		   var th = $(this), name = th.prop('name'); 
           if(th.is(':checked')){
		   $(this).val("1");
           $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false).val("0");   
           }else{
			   $(this).val("0");
		   }
		});
		
		$('input[name=garage]').change(function() {
		   var th = $(this), name = th.prop('name'); 
           if(th.is(':checked')){
		   $(this).val("1");
           $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false).val("0");   
           }else{
			   $(this).val("0");
		   }
		});
		
		$('input[name=garden]').change(function() {
		   var th = $(this), name = th.prop('name'); 
           if(th.is(':checked')){
		   $(this).val("1");
           $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false).val("0");   
           }else{
			   $(this).val("0");
		   }
		});
		
		$('input[name=one_floor]').change(function() {
		   var th = $(this), name = th.prop('name'); 
           if(th.is(':checked')){
		   $(this).val("1");
           $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false).val("0");   
           }else{
			   $(this).val("0");
		   }
		});
		
		$('input[name=barbeque_garden]').change(function() {
		   var th = $(this), name = th.prop('name'); 
           if(th.is(':checked')){
		   $(this).val("1");
           $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false).val("0");   
           }else{
			   $(this).val("0");
		   }
		});
		
		$('input[name=owned]').change(function() {
		   var th = $(this), name = th.prop('name'); 
           if(th.is(':checked')){
		   $(this).val("1");
           $(':checkbox[name="'  + name + '"]').not($(this)).prop('checked',false).val("0");   
           }else{
			   $(this).val("0");
		   }
		});
	});
	
	function onChangeYearsFinished(selIndex)
	{
		$('#years_renovated').find("option:gt(0)").remove();
		for(i = 1; i <= parseInt(selIndex); i++)
		{
			var selVal = $('#years_finished option').eq(i).val();
			var selText = $('#years_finished option').eq(i).text();
			$('#years_renovated').append('<option value="'+selVal+'">'+selText+'</option>');
		}
	}
	
	function Calculate()
	{
		var country_select = $("#country_select").val();
		var region_select = $("#region_select").val();
		var city_select = $("#city_select").val();
		var area_select = $("#area_select").val();
		var property_type = $("#property_type").val();
		var beach = $("#beach").val();
		var view = $("#view").val();
		var tennis = $("#tennis").val() == 1 ? 1 : 0;
		var football = $("#football").val() == 1 ? 1 : 0;
		var golf = $("#golf").val() == 1 ? 1 : 0;
		var buss = $("#buss").val() == 1 ? 1 : 0;
		var supermarket = $("#supermarket").val() == 1 ? 1 : 0;
		var restaurants = $("#restaurants").val() == 1 ? 1 : 0;                
		var bedrooms = $("#bedrooms").val();
		var bathrooms = $("#bathrooms").val();
		var living = $("#living").val();
		var years_finished = $("#years_finished").val();
		var years_renovated = $("#years_renovated").val();
		var years_finished_text = $("#years_finished option:selected").text();
		var years_renovated_text = $("#years_renovated option:selected").text();
		var sqm_terrasse = $("#sqm_terrasse").val();
		/*var years_finished_house = $("#years_finished").val();
		var years_renovated_house = $("#years_renovated").val();*/
		var sqm_terrasse_house = $("#sqm_terrasse").val();
		var access_benefits = $("#access_benefits").val();   
		var walking_distance = $("#walking_distance").val();      
			
		// Appartment section
		var type = $("#type").val();
		var turistico = $("#turistico").val();                
		var elevator = $("#elevator").val();
		var terrasse = $("#terrasse").val();
		var terrasse_barbeque = $("#terrasse_barbeque").val();
		var terrasse_solarium = $("#terrasse_solarium").val();
		var terrasse_south = $("#terrasse_south").val();
		var terrasse_west = $("#terrasse_west").val();
		var garage = $("#garage").val();
		var pool = $("#pool").val(); 
		var standard = $("#standard").val();  

		// House section
		var type_house = $("#type_house").val();
		var garden = $("#garden").val();
		var barbeque_garden = $("#barbeque_garden").val();
		var one_floor = $("#one_floor").val();
		var owned = $("#owned").val();
		var garage_house = $("#garage_house").val();
		var plot = $("#plot").val();                  
		var pool_house = $("#pool_house").val(); 
		var standard_house = $("#standard_house").val();                

		if (country_select === '0') {
			alert("{/literal}{l i='calc_error_select_country' gid='content'}{literal}");
			return false;
		}
		if (region_select === '0' || region_select === '') {
			alert("{/literal}{l i='calc_error_select_region' gid='content'}{literal}");
			return false;
		}
		if (city_select === '0') {
			alert("{/literal}{l i='calc_error_select_city' gid='content'}{literal}");
			return false;
		}
		if (area_select === '0') {
			alert("{/literal}{l i='calc_error_select_area' gid='content'}{literal}");
			return false;
		}
		if (property_type === '0') {
			alert("{/literal}{l i='calc_error_select_property_type' gid='content'}{literal}");
			return false;
		}
		
		if (years_finished === '0' || years_finished === '') {
			alert("{/literal}{l i='calc_error_year_built' gid='content'}{literal}");
			return false;
		}
		if (years_renovated === '0' || years_renovated === '') {
			//alert('Year for last renovation!');
			//return false;
			years_renovated = years_finished;
		}
		else
		{
			var strFinished = parseInt($.trim(years_finished_text.replace('--','').replace('<','')));
			var strRenovated = parseInt($.trim(years_renovated_text.replace('--','').replace('<','')));
			if(strFinished > strRenovated)
			{
				alert("{/literal}{l i='calc_error_year_renovation' gid='content'}{literal}");
				return false;
			}
		}
		
		if (bedrooms === '0') {
			alert("{/literal}{l i='calc_error_select_bedrooms' gid='content'}{literal}");
			return false;
		}
		if (bathrooms === '0') {
			alert("{/literal}{l i='calc_error_select_bathrooms' gid='content'}{literal}");
			return false;
		}
		if (living === '0' || living === '') {
			alert("{/literal}{l i='calc_error_living_area' gid='content'}{literal}");
			return false;
		}
		else if(!living.match(/^[0-9.\.]*$/)) {
			alert("{/literal}{l i='calc_error_living_area_valid' gid='content'}{literal}");
			return false;
		}
		if (sqm_terrasse === '') {
			alert("{/literal}{l i='calc_error_terrasse_area' gid='content'}{literal}");
			return false;
		}
		else if(!sqm_terrasse.match(/^[0-9.\.]*$/)) {
			alert("{/literal}{l i='calc_error_terrasse_area' gid='content'}{literal}");
			return false;
		}
		if(beach === '0') {
			alert("{/literal}{l i='calc_error_walking_distance' gid='content'}{literal}");
			return false;                    
		}
		if(view === '0') {
			alert("{/literal}{l i='calc_error_select_view' gid='content'}{literal}");
			return false;                    
		}
		
		prop_type = parseInt(property_type, 10);
		if (prop_type === 1) {
			
			if (type === '0') {
				alert("{/literal}{l i='calc_error_apartment_type' gid='content'}{literal}");
				return false;
			}
			if(standard === '0') {
				alert("{/literal}{l i='calc_error_specify_standard' gid='content'}{literal}");
				return false;                    
			} 
			if(pool === '0') {
				alert("{/literal}{l i='calc_error_swimmingpool' gid='content'}{literal}");
				return false;                    
			}

			$.ajax({
				url: '{/literal}{$site_root}{literal}contact_us/ajax_get_calculations',
				dataType: 'html',
				type: 'POST',
				data: {country_select : country_select, region_select : region_select, city_select : city_select, area_select : area_select, property_type : property_type, beach : beach, view : view, tennis : tennis, football : football, golf : golf, buss : buss, supermarket : supermarket, bedrooms : bedrooms, bathrooms : bathrooms, restaurants : restaurants, living : living, type : type, elevator : elevator, terrasse: terrasse, terrasse_barbeque : terrasse_barbeque, terrasse_solarium : terrasse_solarium, terrasse_south : terrasse_south, terrasse_west : terrasse_west, garage : garage, pool : pool, standard : standard, years_finished : years_finished, years_renovated : years_renovated, turistico : turistico, sqm_terrasse : sqm_terrasse},
				cache: false,
				success: function(data){
					if(data){
						$("#Result").html(data);
					}else{
						$("#Result").html('');
					}
				}
			});
				
		}
			
		if (prop_type === 2) {
			
			if (type_house === '0') {
				alert("{/literal}{l i='calc_error_house_type' gid='content'}{literal}");
				return false;
			}
			if(garage_house === '0') {
				alert("{/literal}{l i='calc_error_is_garage' gid='content'}{literal}");
				return false;                    
			}
			if(pool_house === '0') {
				alert("{/literal}{l i='calc_error_swimmingpool' gid='content'}{literal}");
				return false;                    
			}
			if(plot === '0') {
				alert("{/literal}{l i='calc_error_make_choice_house_area' gid='content'}{literal}");
				return false;                    
			}
			if(standard_house === '0') {
				alert("{/literal}{l i='calc_error_specify_standard' gid='content'}{literal}");
				return false;                    
			}
			                        
			$.ajax({
				url: '{/literal}{$site_root}{literal}contact_us/ajax_get_calculations',
				dataType: 'html',
				type: 'POST',
				data: {country_select : country_select, region_select : region_select, city_select : city_select, area_select : area_select, property_type : property_type, beach : beach, view : view, tennis : tennis, football : football, golf : golf, buss : buss, supermarket : supermarket, bedrooms : bedrooms, bathrooms : bathrooms, restaurants : restaurants, living : living, plot : plot, owned : owned, type_house : type_house, one_floor : one_floor, garden : garden, barbeque_garden : barbeque_garden, garage_house : garage_house, pool_house : pool_house, standard_house : standard_house, years_finished_house : years_finished, years_renovated_house : years_renovated, sqm_terrasse_house : sqm_terrasse_house},
				cache: false,
				success: function(data){
					if(data){
						$("#Result").html(data);
					}else{
						$("#Result").html('');
					}
				}
			});                    
		}
	}
</script>

<style>

.ms-parent {
    display: inline-block;
    position: relative;
    vertical-align: middle;
	width:210px !important;
}

.ms-choice {
    display: block;
    width: 100%;
    height: 34px;
    padding: 0;
    overflow: hidden;
    cursor: pointer;
    border: 1px solid #aaa;
    text-align: left;
    white-space: nowrap;
    line-height: 34px;
    text-decoration: none;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
    background-color: #fff;
}

.ms-choice.disabled {
    background-color: #f4f4f4;
    background-image: none;
    border: 1px solid #ddd;
    cursor: default;
}

.ms-choice > span {
    position: absolute;
    top: 0;
    left: 0;
    right: 20px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    display: block;
    padding-left: 8px;
	font-size: 14px;
}

/*.ms-choice > span.placeholder {
    color: #999;
}*/

.ms-choice > div {
    position: absolute;
    top: 0;
    right: 0;
    width: 20px;
    height: 33px;
    background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDZFNDEwNjlGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDZFNDEwNkFGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NkU0MTA2N0Y3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NkU0MTA2OEY3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuGsgwQAAAA5SURBVHjaYvz//z8DOYCJgUxAf42MQIzTk0D/M+KzkRGPoQSdykiKJrBGpOhgJFYTWNEIiEeAAAMAzNENEOH+do8AAAAASUVORK5CYII=")  no-repeat;
	background-position:right 50%;
}

/*.ms-choice > div.open {
  //  background: url('multiple-select.png') right top no-repeat;
}*/

.ms-drop {
    width: 100%;
    overflow: hidden;
    display: none;
    margin-top: -1px;
    padding: 0;
    position: absolute;
    z-index: 1000;
    background: #fff;
    color: #000;
    border: 1px solid #aaa;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}

.ms-drop.bottom {
    top: 100%;
    -webkit-box-shadow: 0 4px 5px rgba(0, 0, 0, .15);
    -moz-box-shadow: 0 4px 5px rgba(0, 0, 0, .15);
    box-shadow: 0 4px 5px rgba(0, 0, 0, .15);
}

.ms-drop.top {
    bottom: 100%;
    -webkit-box-shadow: 0 -4px 5px rgba(0, 0, 0, .15);
    -moz-box-shadow: 0 -4px 5px rgba(0, 0, 0, .15);
    box-shadow: 0 -4px 5px rgba(0, 0, 0, .15);
}

.ms-search {
    display: inline-block;
    margin: 0;
    min-height: 26px;
    padding: 4px;
    position: relative;
    white-space: nowrap;
    width: 100%;
    z-index: 10000;
}

.ms-search input {
    width: 100%;
    height: auto !important;
    min-height: 24px;
    padding: 0 20px 0 5px;
    margin: 0;
    outline: 0;
    font-family: sans-serif;
    font-size: 1em;
    border: 1px solid #aaa;
    -webkit-border-radius: 0;
    -moz-border-radius: 0;
    border-radius: 0;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
}

.ms-search, .ms-search input {
    -webkit-box-sizing: border-box;
    -khtml-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    box-sizing: border-box;
}

.ms-drop ul {
    overflow: auto;
    margin: 0;
    padding: 5px 2px;
	text-align:left;
}

.ms-drop ul > li {
    list-style: none;
    display: list-item;
    background-image: none;
    position: static;
}

.ms-drop ul > li .disabled {
    opacity: .35;
    filter: Alpha(Opacity=35);
}

.ms-drop ul > li.multiple {
    display: block;
    float: left;
}

.ms-drop ul > li.group {
    clear: both;
}

.ms-drop ul > li.multiple label {
    width: 100%;
    display: block;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}

.ms-drop ul > li label {
    font-weight: normal;
    display: block;
    white-space: nowrap;
}

.ms-drop ul > li label.optgroup {
    font-weight: bold;
}

.ms-drop input[type="checkbox"] {
    vertical-align: middle;
}

.ms-drop .ms-no-results {
    display: none;
}
#selectone1
{
	padding-right:21px;
}
#selectone9
{
	padding-right:52px;
}
#selectone10
{
   padding-right: 47px;
}
#selecttwo1{
	padding-right:86px;
}
#selecttwo9{
	padding-right:104px;
}
#selecttwo10{
	padding-right:84px;
}
#selectthree1{
	padding-right:110px;
}
#selectthree9{
	padding-right:135px;
}
#selectthree10{
	padding-right:98px;
}
#selectfour1{
	padding-right:44px;
}
#selectfour9{
	padding-right:66px;
}
#selectfour10{
	padding-right:67px;
}
#selectfive1{
	padding-right:107px;
}
#selectfive9{
	padding-right:121px;
}
#selectfive10{
	padding-right:123px;
}
#selectsix1{
	padding-right:103px;
}
#selectsix9{
	padding-right:83px;
}
#selectsix10{
	padding-right:77px;
}
#selectseven1{
	padding-right:117px;
}
#selectseven9{
	padding-right:100px;
}
#selectseven10{
	padding-right:132px;
}
#selecteight1{
	padding-right:134px;
}
#selecteight9{
	padding-right:158px;
}
#selecteight10{
	padding-right:165px;
}
#selectnine1{
	padding-right:162px;
}
#selectnine9{
	padding-right:160px;
}
#selectnine10{
	padding-right: 162px;
}
#selectten1{
	padding-right:120px;
}
#selectten9{
	padding-right:100px;
}
#selectten10{
	padding-right:133px;
}
#selecteleven1{
	padding-right:185px;
}
#selecteleven9{
	padding-right:180px;
}
#selecteleven10{
	padding-right:180px;
}
#selecttwelve1{
	padding-right:104px;
}
#selecttwelve9{
	padding-right:85px;
}
#selecttwelve10{
	padding-right:78px;
}
#checkone1{
	float:left;margin-left: 90px;margin-top: 0px;
}
#checkone9{
	float:left;margin-left: 107px;margin-top: 0px;
}
#checkone10{
	float:left;margin-left:125px;margin-top: 0px;
}
#checktwo1{
	float:left;margin-left: 142px;margin-top: 0px;
}
#checktwo9{
	float:left;margin-left: 157px;margin-top: 0px;
}
#checktwo10{
	float:left;margin-left: 164px;margin-top: 0px;
}
#checkthree1{
	float:left;margin-left: 160px;margin-top: 0px;
}
#checkthree9{
	float:left;margin-left: 184px;margin-top: 0px;
}
#checkthree10{
	float:left;margin-left: 189px;margin-top: 0px;
}
#checkfour1{
	float:left;margin-left: 137px;margin-top:0px;
}
#checkfour9{
	float:left;margin-left: 119px;margin-top:0px;
}
#checkfour10{
	float:left;margin-left: 116px;margin-top:0px;
}
#checkfive1{
	float:left;margin-left: 164px;margin-top: 0px;
}
#checkfive9{
	float:left;margin-left: 164px;margin-top: 0px;
}
#checkfive10{
	float:left;margin-left: 167px;margin-top: 0px;
}
#checksix1{
	float:left;margin-left: 145px;margin-top:0px;
}
#checksix9{
	float:left;margin-left: 118px;margin-top:0px;
}
#checksix10{
	float:left;margin-left: 115px;margin-top:0px;
}
#checkseven1{
	float:left;margin-left: 76px;margin-top: 0px;
}
#checkseven9{
	float:left;margin-left: 76px;margin-top: 0px;
}
#checkseven10{
	float:left;margin-left: 56px;margin-top: 0px;
}
#checkeight1{
	float:left;margin-left: 178px;margin-top:0px;
}
#checkeight9{
	float:left;margin-left: 180px;margin-top:0px;
}
#checkeight10{
	float:left;margin-left: 183px;margin-top:0px;
}
#checknine1{
	float:left;margin-left: 164px;margin-top:0px;
}
#checknine9{
	float:left;margin-left: 164px;margin-top:0px;
}
#checknine10{
	float:left;margin-left: 164px;margin-top:0px;
}
#checkten1{
	float:left;margin-left: 136px;margin-top: 0px;
}
#checkten9{
	float:left;margin-left: 133px;margin-top: 0px;
}
#checkten10{
	float:left;margin-left: 128px;margin-top: 0px;
}
#checkeleven1{
	float:left;margin-left: 86px;margin-top: 0px;
}
#checkeleven9{
	float:left;margin-left: 127px;margin-top: 0px;
}
#checkeleven10{
	float:left;margin-left: 144px;margin-top: 0px;
}
#checktwelve1{
	float:left;margin-left: 130px;margin-top: 0px;
}
#checktwelve9{
	float:left;margin-left: 130px;margin-top: 0px;
}
#checktwelve10{
	float:left;margin-left: 132px;margin-top: 0px;
}
</style>

{/literal}
    {block name=show_social_networks_like module=social_networking}
    {block name=show_social_networks_share module=social_networking}
    {block name=show_social_networks_comments module=social_networking}
    <br><br><br>
</div>
<div class="clr"></div>
{include file="footer.tpl"}

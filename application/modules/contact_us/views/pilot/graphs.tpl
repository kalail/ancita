{include file="header.tpl"}
{js file='jquery-ui'}
{js file='jquery.flot'}
{js file='jquery.flot.time'}
<div class="properties_calc_rc">
    <div class="content-block">
        <!--<h1 style="text-align:center; padding-top:10px;">HERE YOU CAN CHECK THE MARKET VALUE OF YOUR PROPERTY</h1>-->
        <div class="content-value">
            <div class="edit_block properties_calc">
            <form action="" method="post">
            	<div class="divmain1">
                	<div class="divmain1_left">
                    	<div class="divTitle">
                            <div class="locCalcimg">Graph option</div>
                        </div>
                        <table class="tblCalc">
                        	<tr>
                            	<td>
                                	Country<br />
                                    <select name="country_select" id="country_select" class="selCalc">
                                        {foreach from=$country_list key="key" item="country"}
                                            <option value="{$country.code}" {if $country.code eq 'ES'} selected {/if} >{$country.name}</option>
                                        {/foreach}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	Province<br />
                                    <select name="region_select" id="region_select" onchange="javascript:getCity(this);" class="selCalc">
                                        <option value="0">Choose Province</option>
                                        {foreach from=$region_list key="key" item="region"}
                                            <option value="{$region.id}">{$region.name}</option>
                                        {/foreach}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	City<br />
                                    <select name="city_select" id="city_select" onchange="javascript:getArea(this);" class="selCalc">
                                        <option value="0">Choose city</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	Area<br />
                                    <select name="area_select" id="area_select" class="selCalc">
                                        <option value="0">Select area</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	Property type<br />
                                    <select name="type" id="type" class="selCalc">
                                        <option value="1">Villa</option>
                                        <option value="2">Apartment</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	Display type<br />
                                    <select name="view" id="view" class="selCalc">
                                        <option value="1">Columns</option>
                                        <option value="2">Line</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	<div style="float:left;margin-left: 52px;margin-top: 4px;">Show region</div>
                                    <div class="onoffswitch" style="margin-right:26px;">
                                        <input type="checkbox" name="region" class="onoffswitch-checkbox" id="region" value="1">
                                        <label class="onoffswitch-label" for="region">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                            	<td>
                                	Date ranges
                                    <p>
										<span>Min: <input type="text" id="amount_min" style="border: 0; width: 80px; color: #f6931f; font-weight: bold;" /></span>
										<span>Max: <input type="text" id="amount_max" style="border: 0; width: 80px; color: #f6931f; font-weight: bold;" /></span>
										</p>
										    <div id="slider" style="margin-top: 3px; margin-left: 5px; margin-right: 15px"></div>
                                </td>
                            </tr>	
                            <tr>
                            	<td style="text-align:center;">
                                	 <input type="button" value="Calculate graph" class="btnCalc" name="search_button" id="search_button" onclick="javascript: {literal}loadGraph();{/literal}">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="divmain1_right">
                    	<div class="divTitle">
                        	<div class="graphcalcimg">Graph</div>
                        </div>
                    	<table class="tblCalc">
                        	<tr>
                            	<td>
                                	<div class="placeholder">
                                		<div id="placeholder_provinces" style="width: 600px; height: 520px;" class="demo-placeholder"></div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="clear:both"></div>
                    <div class="divsub" id="divCityGraph" style="display:none;">
                    	<div class="divTitle">
                        	<div class="graphcalcimg" id="cityTitle">City</div>
                        </div>
                        <table class="tblCalc">
                        	<tr>
                            	<td>
                                	<div class="placeholder">
										<div id="placeholder_cities" style="width: 900px; height: 400px;" class="demo-placeholder"></div>
                                    </div>
                                </td>
                             </tr>
                        </table>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

{literal}
<script>
	var country_id = $('#country_select option:selected').val();
	var region_id;	
	var city_id;
	var indicator = '<div style="padding-top: 135px;padding-left: 270px;"><img height="80px" style="vertical-align:middle;" src="{/literal}{$site_root}{$img_folder}preloader.gif{literal}"></div>';
	var indicator2 = '<div style="padding-top: 200px;padding-left: 450px;"><img height="80px" style="vertical-align:middle;" src="{/literal}{$site_root}{$img_folder}preloader.gif{literal}"></div>';		
	if (country_id!=0){
			region_id = $('#region_select').val();
				
			if (region_id!=0){
					city_id = $('#city_select').val();
			}
	}
	
	$("#view").change(loadGraph);
	$("#type").change(loadGraph);
	$("#region").click(loadGraph);
	
	function onDataReceived(series) {
		
		var view_var = $('#view option:selected').val();
		var region_var = $('#type option:selected').val();
		
		var myObject = eval(series);
		var options = { xaxis: { mode: "time" } };
		if(view_var == 1) {
		var data = [{
						label: myObject.country.label,
						data: myObject.country.data,
						color: '#ff0000',
						bars: {
							show: true,
							barWidth: 300 * 60 * 60 * 600
						}
					},
					{
						label: myObject.province.label,
						data: myObject.province.data,
						color: '#92d5ea',
						bars: {
							show: true,
							barWidth: 800 * 60 * 60 * 600
						}
					}
		];
		} else {
		
		var data = [
					{
						label: myObject.country.label,
						data: myObject.country.data,
						color: '#92d5ea',
						lines: {
							show: true
						},
						points:{
							show:true
						}
					},
					{
						label: myObject.province.label,
						data: myObject.province.data,
						color: '#ff0000',
						lines: {
							show: true
						},
						points:{
							show:true
						}
					}
		];			
		}
		
		$.plot("#placeholder_provinces", data, options);
		if (myObject.city !== null && myObject.region !== null) {
		if (myObject.city.data !== null && myObject.region.data !== null) {			
		if(region_var == 1) {			
		if(view_var == 1) {
		var data2 = [{
						label: myObject.city.label,
						data: myObject.city.data,
						bars: {
							show: true,
							barWidth: 300 * 60 * 60 * 600
						}
					},
					{
						label: myObject.region.label,
						data: myObject.region.data,
						bars: {
							show: true,
							barWidth: 800 * 60 * 60 * 600
						}
					}
		];
		} else {
		
		var data2 = [
					{
						label: myObject.city.label,
						data: myObject.city.data,
						lines: {
							show: true
						},
						points:{
							show:true
						}
					},
					{
						label: myObject.region.label,
						data: myObject.region.data,
						lines: {
							show: true
						},
						points:{
							show:true
						}
					}
		];			
		}
		} else {
		
		if(view_var == 1) {
		var data2 = [{
						label: myObject.city.label,
						data: myObject.city.data,
						bars: {
							show: true,
							barWidth: 300 * 60 * 60 * 600
						}
					}
		];
		} else {
		
		var data2 = [
					{
						label: myObject.city.label,
						data: myObject.city.data,
						lines: {
							show: true
						},
						points:{
							show:true
						}
					}
		];			
		}
		}
		$.plot("#placeholder_cities", data2, options);
		} else {
		$('#placeholder_cities').html("<div id='forminfo' style='border-radius: 10px; text-align: center; font-size: 16px;'><div><span>Sorry, there are no data for this city</span><div></div>");
		}		
		} else {
		$('#placeholder_cities').html("<div id='forminfo' style='border-radius: 10px; text-align: center; font-size: 16px;'><div><span>Sorry, there are no data for this city</span><div></div>");
		}	
}		

	function loadGraph()
	{
		$.ajax({
				url: '{/literal}{$site_root}{literal}contact_us/ajax_get_calculate_graphs',
				dataType: 'json',
				type: 'POST',
				data: {sel : $('#type option:selected').val(), region_id: $('#region_select option:selected').val(), city_id: $('#city_select option:selected').val(), area_id: $('#area_select option:selected').val(), date_min: $( "#amount_min" ).val(), date_max: $( "#amount_max" ).val()},
				cache: false,
				ajaxStart : $('#placeholder_cities').html(indicator2),
				ajaxStart : $('#placeholder_provinces').html(indicator),
				success: onDataReceived
			});
	}
	
	
	
	
	
	function getCity(selRegion)
	{
		$.ajax({
			url: '{/literal}{$site_root}{literal}countries/ajax_get_cities/' + selRegion.value,
			dataType: 'json',
			type: 'GET',
			data: {},
			cache: false,
			success: function(data){
				$('#city_select').find("option:gt(0)").remove();
				for(var id in data.items){
					$('#city_select').append('<option value="'+data.items[id].id+'">'+data.items[id].name+'</option>');
				}
			}
		});
	}
	
	function getArea(selArea)
	{
		if(selArea != selArea.value)
		{
			$('#divCityGraph').css("display", "block");
			$('#cityTitle').text($('#city_select option:selected').text());
		}
		
		$.ajax({
			url: '{/literal}{$site_root}{literal}countries/ajax_get_districts/' + selArea.value,
			dataType: 'json',
			type: 'GET',
			data: {},
			cache: false,
			success: function(data){
				$('#area_select').find("option:gt(0)").remove();
				for(var id in data.items){
					$('#area_select').append('<option value="'+data.items[id].id+'">'+data.items[id].name+'</option>');
				}
			}
		});
	}

$(function()
 {
	var availableDatas = eval({/literal}{$dates}{literal});
	var aryLength = availableDatas.length;
	$( "#slider" ).slider({
		range: "max",
		min: 0,
		max: aryLength-1,
		values: [ aryLength-13, aryLength-1],
		range: true,
		slide: function( event, ui ) {
			var valMin = availableDatas[ui.values[0]];
			var valMax = availableDatas[ui.values[1]];			
			$( "#amount_min" ).val( valMin );
			$( "#amount_max" ).val( valMax );			
		}
	});
	$( "#amount_min" ).val( availableDatas[aryLength-13] );
	$( "#amount_max" ).val( availableDatas[aryLength-1] );	
});

$(function() {
		loadGraph();
	});
</script>
<style>
.onoffswitch-inner::before {
    background-color: #3d95cb;
    color: #ffffff;
    content: "Yes";
    padding-left: 8px;
}
.demo-container {
	box-sizing: border-box;
	width: 850px;
	height: 450px;
	padding: 20px 15px 15px 15px;
	margin: 15px auto 30px auto;
	border: 1px solid #ddd;
	background: #fff;
	background: linear-gradient(#f6f6f6 0, #fff 50px);
	background: -o-linear-gradient(#f6f6f6 0, #fff 50px);
	background: -ms-linear-gradient(#f6f6f6 0, #fff 50px);
	background: -moz-linear-gradient(#f6f6f6 0, #fff 50px);
	background: -webkit-linear-gradient(#f6f6f6 0, #fff 50px);
	box-shadow: 0 3px 10px rgba(0,0,0,0.15);
	-o-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
	-ms-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
	-moz-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
	-webkit-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
}

.demo-placeholder {
	width: 100%;
	height: 100%;
	font-size: 14px;
	line-height: 1.2em;
}

</style>
{/literal}
    {block name=show_social_networks_like module=social_networking}
    {block name=show_social_networks_share module=social_networking}
    {block name=show_social_networks_comments module=social_networking}
    <br><br><br>
</div>
<div class="clr"></div>
{include file="footer.tpl"}

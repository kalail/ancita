<?php

$install_lang["admin_menu_settings_items_feedbacks-items_spam_alert_menu_item"] = "Alertes de spam";
$install_lang["admin_menu_settings_items_feedbacks-items_spam_alert_menu_item_tooltip"] = "Installer et voir alertes de spam";
$install_lang["admin_menu_settings_items_system-items_spam_sett_menu_item"] = "Alertes de spam";
$install_lang["admin_menu_settings_items_system-items_spam_sett_menu_item_tooltip"] = "Installer et voir alertes de spam";
$install_lang["admin_spam_menu_spam_alerts_item"] = "Alertes";
$install_lang["admin_spam_menu_spam_alerts_item_tooltip"] = "";
$install_lang["admin_spam_menu_spam_reasons_item"] = "Raisons";
$install_lang["admin_spam_menu_spam_reasons_item_tooltip"] = "";
$install_lang["admin_spam_menu_spam_settings_item"] = "Paramètres";
$install_lang["admin_spam_menu_spam_settings_item_tooltip"] = "";
$install_lang["admin_spam_menu_spam_types_item"] = "Types de contenu";
$install_lang["admin_spam_menu_spam_types_item_tooltip"] = "";


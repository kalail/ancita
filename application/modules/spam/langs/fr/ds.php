<?php

$install_lang["form_type"]["header"] = "Type de formule";
$install_lang["form_type"]["option"]["checkbox"] = "Bouton";
$install_lang["form_type"]["option"]["select_text"] = "Sélectionner+message";

$install_lang["spam_object"]["header"] = "Raison pour l'alerte";
$install_lang["spam_object"]["option"]["1"] = "Contenu inapproprié";
$install_lang["spam_object"]["option"]["2"] = "Information douteuse";
$install_lang["spam_object"]["option"]["3"] = "Spam";



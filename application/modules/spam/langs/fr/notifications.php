<?php

$install_lang["notification_spam_object"] = "Alerte de spam";
$install_lang["tpl_spam_object_content"] = "Bonjour admin,\n\nIl a une nouvelle alerte de spam:\n[reason]\n[message]\n\nCordialement,\n[name_from]";
$install_lang["tpl_spam_object_subject"] = "[domain] | [type] (ID=[object_id]) marqué comme spam";


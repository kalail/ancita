<?php

$install_lang["admin_menu_settings_items_feedbacks-items_spam_alert_menu_item"] = "Spamwarnungen";
$install_lang["admin_menu_settings_items_feedbacks-items_spam_alert_menu_item_tooltip"] = "Spamwarnungen einstellen und anzeigen";
$install_lang["admin_menu_settings_items_system-items_spam_sett_menu_item"] = "Spamwarnungen";
$install_lang["admin_menu_settings_items_system-items_spam_sett_menu_item_tooltip"] = "Spamwarnungen einstellen und anzeigen";
$install_lang["admin_spam_menu_spam_alerts_item"] = "Warnungen";
$install_lang["admin_spam_menu_spam_alerts_item_tooltip"] = "";
$install_lang["admin_spam_menu_spam_reasons_item"] = "Gründe";
$install_lang["admin_spam_menu_spam_reasons_item_tooltip"] = "";
$install_lang["admin_spam_menu_spam_settings_item"] = "Einstellungen";
$install_lang["admin_spam_menu_spam_settings_item_tooltip"] = "";
$install_lang["admin_spam_menu_spam_types_item"] = "Inhaltstypen";
$install_lang["admin_spam_menu_spam_types_item_tooltip"] = "";


<?php

$install_lang["admin_header_alerts_create"] = "Spamwarnung erstellen";
$install_lang["admin_header_alerts_list"] = "Unangemessener Inhalt";
$install_lang["admin_header_alerts_show"] = "Spamwarnung anzeigen";
$install_lang["admin_header_reasons_create"] = "Spamgrund erstellen";
$install_lang["admin_header_reasons_edit"] = "Spamgrund bearbeiten";
$install_lang["admin_header_reasons_item"] = "Gründe";
$install_lang["admin_header_reasons_list"] = "Spamgründe Module";
$install_lang["admin_header_settings"] = "Unangemessener Inhalt";
$install_lang["admin_header_spam"] = "Spam";
$install_lang["admin_header_types_edit"] = "Spamtyp bearbeiten";
$install_lang["admin_header_types_list"] = "Inhaltstypen";
$install_lang["alert_status_banned"] = "als Spam markieren";
$install_lang["alert_status_none"] = "in Frage";
$install_lang["alert_status_removed"] = "löschen";
$install_lang["alert_status_unbanned"] = "als Spam markieren";
$install_lang["btn_alerts_ban"] = "verbieten";
$install_lang["btn_alerts_delete_object"] = "Inhalt entfernen";
$install_lang["btn_alerts_unban"] = "entsperren";
$install_lang["btn_content_edit"] = "Inhalt bearbeiten";
$install_lang["btn_mark_as_spam"] = "als Spam markieren";
$install_lang["btn_reasons_create"] = "neuen Grund hinzufügen";
$install_lang["btn_reasons_resort"] = "Folge speichern";
$install_lang["error_alert_from_poster"] = "Sie haben bereits eine Spamwarnung gesendet";
$install_lang["error_badwords_message"] = "Nachricht enthält Schimpfwörter";
$install_lang["error_email_incorrect"] = "E-Mail ist ungültig";
$install_lang["error_empty_object"] = "Leeres Spamobjekt";
$install_lang["error_empty_poster"] = "Spam Schreiber leer";
$install_lang["error_empty_reason"] = "Leerer Spamgrund";
$install_lang["error_empty_reason_name"] = "Leerer Grundname";
$install_lang["error_empty_type"] = "Leerer Spamtyp";
$install_lang["error_empty_type_form_type"] = "Leerer Typ Formulartyp";
$install_lang["error_empty_type_gid"] = "Leerer Typ gid";
$install_lang["error_empty_type_status"] = "Leerer Typstatus";
$install_lang["error_invalid_type"] = "Ungültiger Spamtyp";
$install_lang["error_is_owner"] = "Sie sind der Objektbesitzer";
$install_lang["field_alert_content"] = "Inhalt";
$install_lang["field_alert_date_add"] = "melden";
$install_lang["field_alert_poster"] = "gesendet von";
$install_lang["field_alert_status"] = "Status";
$install_lang["field_send_mail"] = "Sende Warnungen an";
$install_lang["field_spam_message"] = "Kommentare";
$install_lang["field_spam_optional"] = "optional";
$install_lang["field_spam_reason"] = "Grund";
$install_lang["field_type_form_type"] = "Formulartyp";
$install_lang["field_type_gid"] = "Inhalt";
$install_lang["field_type_send_mail"] = "E-Mail senden";
$install_lang["field_type_status"] = "Status";
$install_lang["header_spam_form"] = "Spamform";
$install_lang["link_alerts_ban"] = "Warnungen verbieten";
$install_lang["link_alerts_delete"] = "Warnung entfernen";
$install_lang["link_alerts_delete_object"] = "Objekt entfernen";
$install_lang["link_alerts_show"] = "Warnung anzeigen";
$install_lang["link_alerts_unban"] = "Warnung entsperren";
$install_lang["link_reasons_delete"] = "Grund entfernen";
$install_lang["link_reasons_edit"] = "Grund bearbeiten";
$install_lang["link_send_mail_off"] = "Mail senden aus";
$install_lang["link_send_mail_on"] = "Mail senden ein";
$install_lang["link_type_activate"] = "Typ aktivieren";
$install_lang["link_type_deactivate"] = "Typ deaktivieren";
$install_lang["link_types_edit"] = "Typ bearbeiten";
$install_lang["no_alerts"] = "Keine Warnungen";
$install_lang["no_types"] = "Keine Typen";
$install_lang["note_alerts_delete"] = "Sind Sie sicher, dass Sie die Warnung entfernen wollen?";
$install_lang["note_alerts_delete_all"] = "Sind Sie sicher, dass Sie die Warnungen entfernen wollen?";
$install_lang["note_alerts_delete_object"] = "Sind Sie sicher, dass Sie das Objekt entfernen wollen?";
$install_lang["note_alerts_delete_object_all"] = "Sind Sie sicher, dass Sie den gemeldeten Inhalt entfernen wollen?";
$install_lang["note_reasons_delete"] = "Sind Sie sicher, dass Sie den Grund entfernen wollen?";
$install_lang["stat_header_spam"] = "Beschwerden über anstößige Inhalte";
$install_lang["success_banned_alert"] = "Der Inhalt ist erfolgreich als Spam markiert";
$install_lang["success_created_alert"] = "Die Spamwarnung ist erfolgreich gesendet";
$install_lang["success_created_reason"] = "Grund ist erfolgreich erstellt";
$install_lang["success_deleted_alert"] = "Warnung ist erfolgreich entfernt";
$install_lang["success_removed_alert"] = "Es gibt keine Möglichkeit, den Status des anstößigen Inhalten zu ändern, da es nicht auf der Website sein soll.";
$install_lang["success_type_activated"] = "Typ ist erfolgreich aktiviert";
$install_lang["success_type_dectivated"] = "Typ ist erfolgreich deaktiviert";
$install_lang["success_type_send_form_off"] = "Typ Sendeformular ist ausgeschaltet";
$install_lang["success_type_send_form_on"] = "Typ Sendeformular ist eingeschaltet";
$install_lang["success_unbanned_alert"] = "Der Inhalt ist erfolgreich als Spam markiert";
$install_lang["success_updated_reason"] = "Grund ist erfolgreich aktualisiert";
$install_lang["success_updated_type"] = "Typ ist erfolgreich aktualisiert";


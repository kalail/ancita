<?php

$install_lang["notification_spam_object"] = "Spamwarnung";
$install_lang["tpl_spam_object_content"] = "Hallo admin,\n\nEs gibt einen neuen Spam-Alarm:\n[reason]\n[message]\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_spam_object_subject"] = "[domain] | [type] (ID=[object_id]) ist als Spam markiert";


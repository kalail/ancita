<?php

$install_lang["admin_menu_settings_items_feedbacks-items_spam_alert_menu_item"] = "Alertas de spam";
$install_lang["admin_menu_settings_items_feedbacks-items_spam_alert_menu_item_tooltip"] = "Configurar y ver alertas de spam";
$install_lang["admin_menu_settings_items_system-items_spam_sett_menu_item"] = "Alertas de spam";
$install_lang["admin_menu_settings_items_system-items_spam_sett_menu_item_tooltip"] = "Configurar y ver alertas de spam";
$install_lang["admin_spam_menu_spam_alerts_item"] = "Alertas";
$install_lang["admin_spam_menu_spam_alerts_item_tooltip"] = "";
$install_lang["admin_spam_menu_spam_reasons_item"] = "Razones";
$install_lang["admin_spam_menu_spam_reasons_item_tooltip"] = "";
$install_lang["admin_spam_menu_spam_settings_item"] = "Configuración";
$install_lang["admin_spam_menu_spam_settings_item_tooltip"] = "";
$install_lang["admin_spam_menu_spam_types_item"] = "Tipos de contenido";
$install_lang["admin_spam_menu_spam_types_item_tooltip"] = "";


<?php

$install_lang["form_type"]["header"] = "Tipo de formulario";
$install_lang["form_type"]["option"]["checkbox"] = "Botón";
$install_lang["form_type"]["option"]["select_text"] = "Seleccione+mensaje";

$install_lang["spam_object"]["header"] = "Razón spam";
$install_lang["spam_object"]["option"]["1"] = "Contenido inapropiado";
$install_lang["spam_object"]["option"]["2"] = "Información sospechosa";
$install_lang["spam_object"]["option"]["3"] = "Correo no deseado";



<?php

$install_lang["notification_spam_object"] = "Нередности";
$install_lang["tpl_spam_object_content"] = "Здравейте, администратор!\n\nПояви се нов коментар на отзива.\n\nОтзив: [review]\n\nКоментар: [comment]\n\nС уважение,\n[name_from]";
$install_lang["tpl_spam_object_subject"] = "[domain] | [type] (ID=[object_id]) е отбелязан, че има нередности";


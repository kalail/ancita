<?php

$install_lang["admin_menu_settings_items_feedbacks-items_spam_alert_menu_item"] = "Сигнали за нередности";
$install_lang["admin_menu_settings_items_feedbacks-items_spam_alert_menu_item_tooltip"] = "Настройване и преглед на сигнали за нередности";
$install_lang["admin_menu_settings_items_system-items_spam_sett_menu_item"] = "Сигнали за нередности";
$install_lang["admin_menu_settings_items_system-items_spam_sett_menu_item_tooltip"] = "Настройване и преглед на сигнали за нередности";
$install_lang["admin_spam_menu_spam_alerts_item"] = "Уведомления";
$install_lang["admin_spam_menu_spam_alerts_item_tooltip"] = "";
$install_lang["admin_spam_menu_spam_reasons_item"] = "Причини";
$install_lang["admin_spam_menu_spam_reasons_item_tooltip"] = "";
$install_lang["admin_spam_menu_spam_settings_item"] = "Настройки";
$install_lang["admin_spam_menu_spam_settings_item_tooltip"] = "";
$install_lang["admin_spam_menu_spam_types_item"] = "Тип на съдържанието";
$install_lang["admin_spam_menu_spam_types_item_tooltip"] = "";


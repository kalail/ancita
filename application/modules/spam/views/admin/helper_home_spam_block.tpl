<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan=2>{l i='stat_header_spam' gid='spam'}</th>
	</tr>
	{if $stat_spam.index_method}
	{foreach item=item from=$stat_spam.types}
	{counter print=false assign=counter}
	<tr {if $counter is div by 2}class="zebra"{/if}>
		<td class="first"><a href="{$site_url}admin/spam/index/{$item.gid}">{$item.output_name}</a></td>
		<td class="w30"><a href="{$site_url}admin/spam/index/{$item.gid}">{$item.obj_count} ({$item.obj_need_approve})</a></td>
	</tr>
	{/foreach}
	{/if}
</table>


<?php

define("SPAM_TYPES_TABLE", DB_PREFIX."spam_types");

/**
 * Spam Type Model
 *
 * @package PG_RealEstate
 * @subpackage Spam
 * @category models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Spam_type_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to DataBase object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Fields of spam type in data source
	 * 
	 * @var array
	 */
	public $fields = array(
		"id",
		"gid",
		"form_type",
		"send_mail",
		"status",
		"module",
		"model",
		"callback",
		"obj_count",
		"obj_need_approve",
	);
	
	/**
	 * Settings for formatting object of spam type 
	 * 
	 * @var array
	 */
	private $format_settings = array(
		"use_format" => true,
		"get_form_type" => true,
	);
	
	/**
	 * Cache of spam types
	 * 
	 * @var array
	 */
	private $type_cache = array();

	/**
	 * Constructor
	 *
	 * @return Spam_type_model
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Returm spam type object by ID/GUID
	 * 
	 * @param integer $type_id type ID/GUID
	 * @return array/boolean
	 */
	public function get_type_by_id($type_id){
		$field = "id";
		if(!intval($type_id)){
			$field = "gid";
			$type_id= preg_replace("/[^a-z_]/", "", strtolower($type_id));
		}
		if(!$type_id) return false;
		
		if(isset($this->type_cache[$type_id])) return $this->type_cache[$type_id];
		
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(SPAM_TYPES_TABLE);
		$this->DB->where($field, $type_id);

		//_compile_select;
		$result = $this->DB->get()->result();
		if(!empty($result)){
			$rt = get_object_vars($result[0]);
			$this->type_cache[$rt['id']] = $this->type_cache[$rt['gid']] = $rt;
			return $rt;
		}else{
			return false;
		}
	}

	/**
	 * Save spam type object to data source
	 * 
	 * @param integer $id spam type identifier
	 * @param array $data spam type data
	 * @return integer
	 */
	public function save_type($id, $data){
		if(!$id){
			if(!isset($data["gid"]) || !$data["gid"]) return false;
		
			$data["gid"] = preg_replace("/[^a-z_]/", "", strtolower($data["gid"]));

			$type = $this->get_type_by_id($data["gid"]);
			if($type) return $type["id"];
			
			$this->DB->insert(SPAM_TYPES_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			$fields = array_flip($this->fields);
			foreach($data as $key=>$value){
				if(!isset($fields[$key])) unset($data[$key]);
			}
	
			if(empty($data)) return false;
	
			$this->DB->where("id", $id);
			$this->DB->update(SPAM_TYPES_TABLE, $data);
		}
	
		return $id;
	}

	/**
	 * Remove spam type object by ID or GUID
	 * 
	 * @param integer/string $type_id  ID/GID
	 * @return void
	 */
	public function delete_type($type_id){
		$type = $this->get_type_by_id($id);		
		$this->DB->where("id", $type["type_id"]);
		$this->DB->delete(SPAM_TYPES_TABLE); 
		return;
	}
	
	/**
	 * Return all types objects from data source as array
	 * 
	 * @param boolean $status status of spam type
	 * @param array $filter_object_ids filters identifiers
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_types($status=false, $filter_object_ids=null, $formatted=true){
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(SPAM_TYPES_TABLE);
		
		if($status){
			$this->DB->where("status", "1");
		}
		
		if(is_array($filter_object_ids)){
			foreach($filter_object_ids as $value){
				$this->DB->where_in("id", $value);
			}
		}
		
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$this->type_cache[$r['id']] = $this->type_cache[$r['gid']] = $data[$r['id']] = $r;
			}
			if($formatted){
				return $this->format_type($data);
			}else{
				return $data;
			}
		}
		return array();
	}
	
	/**
	 * Validate spam settings
	 * 
	 * @param array $data settings data
	 * @return array
	 */
	public function validate_settings($data){
		$return = array("errors"=> array(), "data" => array());
		
		if(isset($data["send_alert_to_email"]) && !empty($data["send_alert_to_email"])){
			$this->CI->config->load("reg_exps", TRUE);
			$email_expr = $this->CI->config->item("email", "reg_exps");
			$return["data"]["send_alert_to_email"] = strip_tags($data["send_alert_to_email"]);
			if(empty($return["data"]["send_alert_to_email"]) || !preg_match($email_expr, $return["data"]["send_alert_to_email"])){
				$return["errors"][] = l("error_email_incorrect", "spam");
			}
		}

		return $return;
	}
	
	/**
	 * Validate spam type object for saving to data source
	 * 
	 * @param integer $id spam type identifier
	 * @param array $data spam type data
	 * @return array
	 */
	public function validate_type($id, $data){
		$return = array("errors"=> array(), "data" => array());
		
		foreach($this->fields as $field){
			if(isset($data[$field])){
				$return["data"][$field] = $data[$field];
			}
		}
		
		if(isset($data["id"])){
			$return['data']['id'] = intval($data['id']);
		}

		if(isset($data["gid"])){
			$return["data"]["gid"] = trim(strip_tags($data['gid']));
			if(empty($return["data"]["gid"])){
				$return["errors"][] = l("error_empty_type_gid", "spam");
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_type_gid", "spam");
		}

		if(isset($data["form_type"])){
			$return["data"]["form_type"] = trim(strip_tags($data['form_type']));
			if(empty($return["data"]["form_type"])){
				$return["errors"][] = l("error_empty_type_form_type", "spam");
			}
		}elseif(!$id){
			$return["errors"][] = l("error_empty_type_form_type", "spam");
		}
		
		if(isset($data["status"])){
			$return["data"]["status"] = $data["status"] ? 1 : 0; 
		}
		
		if(isset($data["send_mail"])){
			$return["data"]["send_mail"] = $data["send_mail"] ? 1 : 0; 
		}
		
		if(isset($data["module"])){
			$return["data"]["send_mail"] = trim(strip_tags($data["module"])); 
		}
		
		if(isset($data["model"])){
			$return["data"]["model"] = trim(strip_tags($data["model"])); 
		}
		
		if(isset($data["callback"])){
			$return["data"]["callback"] = trim(strip_tags($data["callback"])); 
		}
	
		if(isset($data["obj_count"])){
			$return["data"]["obj_count"] = intval($data["obj_count"]); 
		}
		
		if(isset($data["obj_need_approve"])){
			$return["data"]["obj_need_approve"] = intval($data["obj_need_approve"]); 
		}
		
		return $return;
	}
	
	/**
	 * Format spam type object
	 * 
	 * @param array $data spam type data
	 * @return array
	 */
	public function format_type($data){
		if(!$this->format_settings["use_format"]){
			return $data;
		}
		
		if($this->format_settings["get_form_type"]){
			$form_types = ld("form_type", "spam");
			foreach($data as $key=>$type){
				$data[$key] = $type;
				$data[$key]["form"] = isset($form_types["option"][$type["form_type"]]) ? $form_types["option"][$type["form_type"]] : "-";				
			}
		}
		
		foreach($data as $key=>$type){
			$data[$key]["output_name"] = l('stat_header_spam_'.$data[$key]["gid"], 'spam');
			if(!$data[$key]["output_name"]) $data[$key]["output_name"] = $data[$key]["gid"];
		}
		
		return $data;
	}
	
	/**
	 * Format spam type object by default
	 * 
	 * @param array $type_id spam type identifier
	 * @return array
	 */
	public function format_default_type($type_id){
		$return = array();
		foreach ($this->fields as $field){
			$return[$field] = "-";
		}
		$return["output_name"] = "-";
		return $return;
	}
	
	/**
	 * Import languages of spam types
	 * 
	 * @param array $data spam types data
	 * @param array $langs_file data by languages
	 * @param array $langs_ids languages identifiers
	 * @return void
	 */
	public function update_langs($data, $langs_file, $langs_ids){
		foreach((array)$data as $type_data){
			$this->CI->pg_language->pages->set_string_langs(
				"spam", 
				"stat_header_spam_".$type_data["gid"], 
				$langs_file["stat_header_spam_".$type_data["gid"]], 
				$langs_ids);
		}
	}
	
	/**
	 * Export languages of spam types
	 * 
	 * @param array $data spam types data
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function export_langs($data, $langs_ids=null){
		$gids = array();
		$langs = array();
		foreach($data as $type_data){
			$gids[] = "stat_header_spam_".$type_data["gid"];
		}
		return array_merge($langs, $this->CI->pg_language->export_langs("spam", $gids, $langs_ids));
	}
	
	/**
	 * Activate spam type object
	 * 
	 * @param integer $type_id spam type identifier
	 * @param integer $status spam type status
	 * @return void
	 */
	public function activate_type($type_id, $status=1){
		$data["status"] = intval($status);
		$this->save_type($type_id, $data);
	}
	
	/**
	 * Turn on/off send mail option of spam type
	 * 
	 * @param integer $type_id spam type identifier
	 * @param integer $status send mail status
	 * @return void
	 */
	public function send_mail_type($type_id, $status=1){
		$data["send_mail"] = intval($status);
		$this->save_type($type_id, $data);
	}
}

<?php

$install_lang["admin_header_settings"] = "Paramètres";
$install_lang["btn_agent_cancel"] = "Annuler";
$install_lang["btn_agent_request"] = "Envoyer";
$install_lang["btn_back"] = "Arrière";
$install_lang["btn_buy"] = "Payer";
$install_lang["btn_buy_contact"] = "Payer pour ce contact";
$install_lang["btn_cancel"] = "Annuler";
$install_lang["btn_close"] = "Fermer";
$install_lang["btn_contact"] = "Contacter";
$install_lang["btn_delete"] = "Effacer";
$install_lang["btn_done"] = "Terminé";
$install_lang["btn_leave_company"] = "Quitter l'entreprise";
$install_lang["btn_login"] = "Connexion";
$install_lang["btn_ok"] = "OK";
$install_lang["btn_refresh"] = "Rafraichir";
$install_lang["btn_register"] = "Enregistrer";
$install_lang["btn_save"] = "Sauvegarder";
$install_lang["btn_search"] = "Recherche";
$install_lang["btn_send"] = "Envoyer";
$install_lang["btn_send_message"] = "Envoyer";
$install_lang["btn_update"] = "Mettre à jour";
$install_lang["error"] = "Erreur";
$install_lang["field_address"] = "Adresse";
$install_lang["field_agent_company"] = "Agence";
$install_lang["field_agent_status"] = "Statut";
$install_lang["field_agents"] = "Agents";
$install_lang["field_auction"] = "Négocier le prix est possible";
$install_lang["field_city"] = "Ville";
$install_lang["field_comment"] = "Commentaires";
$install_lang["field_company_name"] = "Nom de l'agence";
$install_lang["field_contact_email"] = "Adresse courriel";
$install_lang["field_contact_info"] = "Information de contacte";
$install_lang["field_contact_phone"] = "Numéro de téléphone";
$install_lang["field_country"] = "Pays";
$install_lang["field_date_created"] = "Enregistré le";
$install_lang["field_date_available"] = "Disponible";
$install_lang["field_date_open"] = "Ouvrir";
$install_lang["field_district"] = "District";
$install_lang["field_email"] = "Courriel";
$install_lang["field_email_new"] = "Nouveau courriel";
$install_lang["field_email_old"] = "Courriel courant";
$install_lang["field_facebook"] = "Facebook";
$install_lang["field_fname"] = "fname";
$install_lang["field_geomap_driver"] = "Directions";
$install_lang["field_guests"] = "Invités";
$install_lang["field_index_block"] = "Type de vente sur première page de l'application";
$install_lang["field_index_count_block"] = "Nombre d'annonces";
$install_lang["field_keyword"] = "Mot-clé";
$install_lang["field_listing_created"] = "Crée le";
$install_lang["field_listing_id"] = "Numéro de référence";
$install_lang["field_listing_buy"] = "Propriétés à acheter";
$install_lang["field_listing_lease"] = "Propriétés à louer";
$install_lang["field_listing_rent"] = "Propriétés à louer";
$install_lang["field_listing_sale"] = "Propriétés à vendre";
$install_lang["field_listing_views"] = "Vues";
$install_lang["field_listings"] = "Propriétés";
$install_lang["field_location"] = "Emplacement";
$install_lang["field_lunch_time"] = "L'heure du diner";
$install_lang["field_messages_new"] = "Nouveaux messages";
$install_lang["field_messages_total"] = "Total";
$install_lang["field_more_info"] = "Plus d'information";
$install_lang["field_move_in"] = "Enregistrement";
$install_lang["field_move_out"] = "Départ";
$install_lang["field_password"] = "Mot de passe";
$install_lang["field_password_new"] = "Nouveau mot de passe";
$install_lang["field_password_old"] = "Mot de passe actuel";
$install_lang["field_phone"] = "Numéro de téléphone";
$install_lang["field_phone_new"] = "Nouveau téléphone";
$install_lang["field_phone_old"] = "Téléphone actuel";
$install_lang["field_price"] = "Prix";
$install_lang["field_price_book"] = "Total";
$install_lang["field_price_type"] = "Prix";
$install_lang["field_property_type"] = "Type de propriété";
$install_lang["field_rating"] = "Évaluation";
$install_lang["field_region"] = "Région";
$install_lang["field_sname"] = "Nom";
$install_lang["field_square"] = "Carré";
$install_lang["field_twitter"] = "Twitter";
$install_lang["field_user_logo"] = "Photo/Logo";
$install_lang["field_user_name"] = "Nom d'utilisateur";
$install_lang["field_user_type"] = "Enregistré comme";
$install_lang["field_vkontakte"] = "Vkontakte";
$install_lang["field_web_url"] = "Site web";
$install_lang["field_working_days"] = "Jours d'ouverture";
$install_lang["field_working_hours"] = "Heures d'overture";
$install_lang["field_zip"] = "Code postal";
$install_lang["geomap_driver_google"] = "Google";
$install_lang["geomap_driver_yandex"] = "Yandex";
$install_lang["header_account"] = "Mon compte";
$install_lang["header_calendar"] = "Calendrier";
$install_lang["header_change_email"] = "Modifier email";
$install_lang["header_change_password"] = "Modifier mot de passe";
$install_lang["header_change_phone"] = "Modifier téléphone";
$install_lang["header_contacts"] = "Contact info";
$install_lang["header_currency"] = "Monnaie";
$install_lang["header_edit_company"] = "Modifier agence";
$install_lang["header_edit_contacts"] = "Modifier les infos de contact";
$install_lang["header_edit_location"] = "Modifier l'emplacement";
$install_lang["header_edit_personal"] = "Informations personnelles";
$install_lang["header_gallery"] = "Galerie";
$install_lang["header_home"] = "Maison";
$install_lang["header_language"] = "Langue";
$install_lang["header_list"] = "Liste";
$install_lang["header_listing"] = "Inscription";
$install_lang["header_listings"] = "Plus d'infos";
$install_lang["header_login"] = "Connexion";
$install_lang["header_map"] = "Carte";
$install_lang["header_messages"] = "Mes messages";
$install_lang["header_news"] = "Nouvelles";
$install_lang["header_order"] = "Demande";
$install_lang["header_orders"] = "Mes demandes";
$install_lang["header_overview"] = "Panorama";
$install_lang["header_password_forgot"] = "Mot de passe oublié";
$install_lang["header_password_restore"] = "Envoyer le mot de passe à l'adresse email";
$install_lang["header_photo"] = "Photo";
$install_lang["header_profile"] = "Mon profil";
$install_lang["header_profile_company"] = "Mon agence";
$install_lang["header_profile_contacts"] = "Contact info";
$install_lang["header_profile_location"] = "Emplacement";
$install_lang["header_profile_personal"] = "Informations personnelles";
$install_lang["header_provider"] = "Fournisseur";
$install_lang["header_register"] = "Inscription";
$install_lang["header_search"] = "Propriétés";
$install_lang["header_search_agents"] = "Agents";
$install_lang["header_search_listings"] = "Maisons";
$install_lang["header_search_results"] = "Résultats de la recherche";
$install_lang["header_search_users"] = "Profils";
$install_lang["header_services"] = "Services";
$install_lang["header_settings"] = "Paramètres";
$install_lang["header_share"] = "Partager";
$install_lang["header_user"] = "Profil";
$install_lang["help_company_name"] = "Ajoutez le nom de l'agence";
$install_lang["help_email"] = "Ajouter email";
$install_lang["help_email_new"] = "Ajouter un nouvel email";
$install_lang["help_fname"] = "Ajouter nom";
$install_lang["help_location"] = "Ville , Etat / Région ou Code postal";
$install_lang["help_message"] = "Tapez votre message...";
$install_lang["help_password"] = "Mot de passe";
$install_lang["help_password_new"] = "Votre nouveau mot de passe";
$install_lang["help_password_old"] = "Mot de passe actuel";
$install_lang["help_phone"] = "Votre téléphone";
$install_lang["help_phone_new"] = "Votre nouveau téléphone";
$install_lang["help_sname"] = "Entrez nom de famille";
$install_lang["index_featured_block"] = "Propriétés en vedette";
$install_lang["index_latest_block"] = "Propriétés récentes";
$install_lang["index_rent_block"] = "Propriétés à louer";
$install_lang["index_sale_block"] = "Propriétés à vendre";
$install_lang["link_change_email"] = "Modifier votre adresse email";
$install_lang["link_change_password"] = "Modifier mot de passe";
$install_lang["link_change_phone"] = "Changer le numéro de téléphone";
$install_lang["link_change_profile"] = "Mon profil";
$install_lang["link_contact"] = "Contacter";
$install_lang["link_language"] = "Langue";
$install_lang["link_logo_delete"] = "Supprimer logo";
$install_lang["link_logoff"] = "Déconnectez-vous";
$install_lang["link_more"] = "Chargez plus";
$install_lang["link_next"] = "Suivant";
$install_lang["link_orders"] = "Mes demandes";
$install_lang["link_prev"] = "Précédent";
$install_lang["link_select_currency"] = "Monnaie";
$install_lang["link_select_language"] = "Langue";
$install_lang["link_user_view"] = "Voir le profil";
$install_lang["loading_text"] = "Chargement en cours...";
$install_lang["login"] = "Connexion";
$install_lang["menu"] = "Menu";
$install_lang["message"] = "Message";
$install_lang["mobile_version"] = "Version mobile";
$install_lang["no_contacts"] = "Aucun contact";
$install_lang["no_information"] = "Aucune information";
$install_lang["no_orders"] = "Pas de demandes";
$install_lang["password"] = "Mot de passe";
$install_lang["personal_information"] = "Informations personnelles";
$install_lang["photos"] = "Photos";
$install_lang["profile"] = "Profil";
$install_lang["register_agent"] = "Inscrivez-vous comme un agent";
$install_lang["register_company"] = "Inscrivez-vous en tant qu'agence";
$install_lang["register_private"] = "Inscrivez-vous en tant que personne privée";
$install_lang["saving"] = "Sauvegarde en cours...";
$install_lang["search_listing_buy"] = "Pour acheter";
$install_lang["search_listing_lease"] = "Pour louer";
$install_lang["search_listing_rent"] = "Pour louer";
$install_lang["search_listing_sale"] = "À vendre";
$install_lang["search_listing_sold"] = "Récemment vendu";
$install_lang["search_user_agent"] = "Agents";
$install_lang["search_user_company"] = "Agences";
$install_lang["search_user_private"] = "Personnes privées";
$install_lang["success"] = "Enregistrée";
$install_lang["success_settings_saved"] = "Les réglages sont sauvegardés avec succès";
$install_lang["text_agency_approval"] = "En attente d'approbation depuis";
$install_lang["text_all"] = "...";
$install_lang["text_contacts"] = "Contacts";
$install_lang["text_date_from"] = "Minimum";
$install_lang["text_date_to"] = "Maximum";
$install_lang["text_from"] = "À partir de";
$install_lang["text_listing_results"] = "Maisons";
$install_lang["text_of"] = "De";
$install_lang["text_photo"] = "Photo";
$install_lang["text_price_from"] = "min";
$install_lang["text_price_to"] = "max";
$install_lang["text_price_unknown"] = "négociée";
$install_lang["text_saving"] = "Sauvegarde en cours...";
$install_lang["text_service_buy"] = "Contact' est un service payé. Visitez le site complet pour acheter des contactes.";
$install_lang["text_user_keyword"] = "Nom, fname ou entreprise";
$install_lang["text_user_results"] = "profils";
$install_lang["tool_listing_order"] = "Réservez maintenant";
$install_lang["tool_listing_share"] = "partager";
$install_lang["tool_send_message"] = "Envoyer un message";
$install_lang["tool_user_listings"] = "annonces";


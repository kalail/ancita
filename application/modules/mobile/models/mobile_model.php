<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

define('MOBILE_USERS_TABLE', DB_PREFIX.'mobile_users');

/**
 * Mobile main model
 *
 * @package PG_RealEstate
 * @subpackage Mobile
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Dmitry Popenov
 * @version $Revision: 1 $ $Date: 2013-12-02 14:53:00 +0300 $ $Author: dpopenov $
 **/
class Mobile_model extends Model {

	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	public $CI;
	
	/**
	 * Link to DataBase object
	 * 
	 * @var object
	 */
	public $DB;
	
	/**
	 * Constructor
	 * 
	 * @return Mobile_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}
	
	/**
	 * Return user secret
	 * 
	 * @param integer $user_id user identifier
	 * @return 
	 */
	public function  get_user_secret($user_id){
		$this->DB->select('secret')->from(MOBILE_USERS_TABLE);
		$this->DB->where('id_user', $user_id);
		$result = $this->DB->get()->result_array();
		if(empty($result)){
			return false;
		}else{
			return $result[0]['secret'];
		}
	}
	
	/**
	 * Save user secret
	 * 
	 * @param integer $user_id user identifier
	 * @return string
	 */
	public function save_user_secret($user_id){
		$secret = md5(date('Y-m-d H:i:s'));
		$data['last_visit_date'] = date('Y-m-d H:i:s');
		$this->DB->where('id_user', $user_id);
		$this->DB->update(MOBILE_USERS_TABLE, $data);
		$sql = "INSERT INTO ".MOBILE_USERS_TABLE." (id_user, secret, last_visit_date) "
			 . "VALUES (".$this->DB->escape($user_id).", ".$this->DB->escape($secret).", ".$this->DB->escape(date('Y-m-d H:i:s')).") "
			 . "ON DUPLICATE KEY UPDATE id_user=".$this->DB->escape($user_id).", secret=".$this->DB->escape($secret).", last_visit_date=".$this->DB->escape(date('Y-m-d H:i:s'));
		$this->DB->query($sql);
		return $secret;
	}
	
	/**
	 * Validate settings
	 * 
	 * @param string $data settings data
	 * @return array
	 */
	public function validate_settings($data){
		$return = array('errors'=> array(), 'data' => array());
		
		if(isset($data['index_type_block'])){
			if(in_array($data['index_type_block'], array('featured', 'latest', 'rent', 'sale'))){
				$return['data']['index_type_block'] = $data['index_type_block'];
			}
		}
		
		if(isset($data['index_count_block'])){
			$return['data']['index_count_block'] = intval($data['index_count_block']);
		}
		
		if(isset($data['geomap_driver'])){
			if(in_array($data['geomap_driver'], array('google', 'yandex'))){
				$return['data']['geomap_driver'] = $data['geomap_driver'];
			}
		}
		
		return $return;
	}
}

<?php

/**
 * Mobile install model
 *
 * @package PG_RealEstate
 * @subpackage Mobile
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Dmitry Popenov
 * @version $Revision: 1 $ $Date: 2013-12-02 14:53:00 +0300 $ $Author: dpopenov $
 **/
class Mobile_install_model extends Model
{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	protected $CI;
	
	/**
	 * Menu configuration
	 * 
	 * @var array
	 */
	protected $menu = array(
		"admin_menu" => array(
			"action" => "none",
			"items" => array(
				"other_items" => array(
					"action" => "none",
					"items" => array(
						"mobile_menu_item" => array("action" => "create", "link" => "admin/mobile/index", "status" => 1, "sorter" => 5),
					),
				),
			),			
		),
		'user_footer_menu' => array(
			'action' => 'none',
			'items' => array(
				'footer-menu-mobile-item' => array('action' => 'create', 'link' => 'm', 'status' => 1, 'sortet' => 7),
			),
		),
	);
	
	/**
	 * Constructor
	 *
	 * @return Mobile_install_model
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Install data of menu mobile
	 * 
	 * @return void
	 */
	public function install_menu(){
		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]['id'] = linked_install_set_menu($gid, $menu_data['action'], $menu_data['name']);
			linked_install_process_menu_items($this->menu, 'create', $gid, 0, $this->menu[$gid]['items']);
		}
	}

	/**
	 * Import languages of menu mobile
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return void
	 */
	public function install_menu_lang_update($langs_ids=null) {
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read('mobile', 'menu', $langs_ids);

		if(!$langs_file) { log_message('info', 'Empty menu langs data'); return false; }

		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, 'update', $gid, 0, $this->menu[$gid]['items'], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages of menu mobile
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_menu_lang_export($langs_ids=null) {
		if(empty($langs_ids)) return false;
		$this->CI->load->helper('menu');

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, 'export', $gid, 0, $this->menu[$gid]['items'], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array( 'menu' => $return );
	}

	/**
	 * Uninstall data of menu mobile
	 * 
	 * @return void
	 */
	public function deinstall_menu(){
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			if($menu_data['action'] == 'create'){
				linked_install_set_menu($gid, 'delete');
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]['items']);
			}
		}
	}
}

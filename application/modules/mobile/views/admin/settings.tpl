{include file="header.tpl"}

<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n250">
		<div class="row">
			<div class="h">{l i='field_index_block' gid='mobile'}:</div>
			<div class="v">
				<select name="index_type_block">
					<option value="featured" {if $data.index_type_block eq 'featured'}selected{/if}>{l i='index_featured_block' gid='mobile'}</option>
					<option value="latest" {if $data.index_type_block eq 'latest'}selected{/if}>{l i='index_latest_block' gid='mobile'}</option>
					<option value="sale" {if $data.index_type_block eq 'sale'}selected{/if}>{l i='index_sale_block' gid='mobile'}</option>
					<option value="rent" {if $data.index_type_block eq 'rent'}selected{/if}>{l i='index_rent_block' gid='mobile'}</option>
				</select>
			</div>
		</div>
		<div class="row zebra">
			<div class="h">{l i='field_index_count_block' gid='mobile'}:</div>
			<div class="v">
				<input type="text" name="index_count_block" value="{$data.index_count_block|escape}">
			</div>
		</div>
		<div class="row">
			<div class="h">{l i='field_geomap_driver' gid='mobile'}:</div>
			<div class="v">
				<input type="radio" name="geomap_driver" value="google" {if $data.geomap_driver eq 'google'}checked{/if} id="google_driver"> <label for="google_driver">{l i='geomap_driver_google' gid='mobile'}</label>
				<input type="radio" name="geomap_driver" value="yandex" {if $data.geomap_driver eq 'yandex'}checked{/if} id="yandex_driver"> <label for="yandex_driver">{l i='geomap_driver_yandex' gid='mobile'}</label>
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/start/menu/system-items">{l i='btn_cancel' gid='start'}</a>
</form>

{include file="footer.tpl"}

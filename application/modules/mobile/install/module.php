<?php

$module['module'] = 'mobile';
$module['install_name'] = 'Mobile version module';
$module['install_descr'] = 'Mobile version';
$module['version'] = '1.01';
$module['files'] = array(
	array('file', 'read', "application/modules/mobile/controllers/admin_mobile.php"),
	array('file', 'read', "application/modules/mobile/controllers/api_mobile.php"),
	array('file', 'read', "application/modules/mobile/controllers/mobile.php"),
	array('file', 'read', "application/modules/mobile/install/module.php"),
	array('file', 'read', "application/modules/mobile/install/permissions.php"),
	array('file', 'read', "application/modules/mobile/install/settings.php"),
	array('file', 'read', "application/modules/mobile/install/structure_deinstall.sql"),
	array('file', 'read', "application/modules/mobile/install/structure_install.sql"),
	array('file', 'read', "application/modules/mobile/models/mobile_model.php"),
	array('file', 'read', "application/modules/mobile/models/mobile_install_model.php"),
	array('file', 'read', "application/modules/mobile/views/admin/settings.tpl"),

	array('dir', 'read', 'application/modules/mobile/langs'),
);

$module['dependencies'] = array(
	'start' => array('version'=>'1.04'),
	'menu' => array('version'=>'3.02'),
	'users' => array('version'=>'3.03'),
	'listings' => array('version'=>'2.02'),
	'mailbox' => array('version'=>'2.04'),
	'payments' => array('version'=>'1.04'),
	'geomap' => array('version'=>'1.25'),
	'get_token' => array('version'=>'1.03'),
);

$module['linked_modules'] = array(
	'install' => array(
		'menu' => 'install_menu',
	),
	'deinstall' => array(
		'menu' => 'uninstall_menu',
	)
);

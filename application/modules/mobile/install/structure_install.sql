DROP TABLE IF EXISTS `[prefix]mobile_users`;
CREATE TABLE IF NOT EXISTS `[prefix]mobile_users` (
	`id_user` bigint(11) NOT NULL auto_increment,
	`secret` varchar(32) NOT NULL,
	`last_visit_date` datetime NOT NULL,
	UNIQUE `id_user` (`id_user`),
	KEY `secret` (`id_user`, `secret`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

<?php

/**
 * Mobile version API controller
 *
 * @package PG_RealEstate
 * @subpackage Mobile
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Dmitry Popenov
 * @version $Revision: 1 $ $Date: 2013-12-02 14:53:00 +0300 $ $Author: dpopenov $
 **/
class Api_Mobile extends Controller {	
	/**
	 * Constructor
	 * 
	 * @return Api_Mobile
	 */
	public function __construct() {
		parent::Controller();
	}

	/**
	 * Init action
	 * 
	 * @param integer $lang_id language identifier
	 * @return void
	 */
	public function init(){
		$theme_data = $this->pg_theme->format_theme_settings($this->router->class);
		$data['data']['site_url'] = site_url();
		$data['data']['logo'] = $theme_data['mini_logo']['path'];
		$data['data']['mapProvider'] = $this->pg_module->get_module_config('mobile', 'geomap_driver');
		
		$current_lang = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
		$data['language'] = $current_lang['code'];
		
		$theme_type = $this->pg_theme->get_current_theme_type();
		$active_settings = $this->pg_theme->return_active_settings($theme_type);
		$theme = $active_settings["theme"];
		$scheme = $active_settings["scheme"];
		$data['data']['css_url'] = $this->pg_theme->theme_default_url . $theme .'/sets/' . $scheme . '/css/mobile-ltr.css';
		
		
		$data['login'] = $this->session->userdata('auth_type') == 'user';
		$data['data']['user_types'] = array();
		$data['data']['login_enabled'] = false;
		
		$this->load->model('Users_model');
		$user_types = $this->Users_model->get_user_types();
		foreach($user_types as $user_type){
			$enabled = $this->pg_module->get_module_config('users', $user_type.'_login_enabled');
			if($enabled) $data['data']['login_enabled'] = true;
			
			$enabled = $this->pg_module->get_module_config('users', $user_type.'_register_enabled');
			if($enabled) $data['data']['register'][] = $user_type;
				
			$enabled = $this->pg_module->get_module_config('users', $user_type.'_register_mail_confirm');
			if($enabled) $data['data']['register_confirm'][] = $user_type;
			
			$enabled = $this->pg_module->get_module_config('users', $user_type.'_search_enabled');
			if($enabled) $data['data']['user_types'][] = $user_type;
		}
		
		
		$this->load->model('Listings_model');
		$data['data']['operation_types'] = $this->Listings_model->get_operation_types(true);
		
		exit(json_encode($data));
	}
	
	/**
	 * Token action
	 * 
	 * @return void
	 */
	public function get_token(){
		$this->load->model('Mobile_model');
	
		$data = array(
			'email' => trim(strip_tags($this->input->get_post('email', true))),
			'password' => trim(strip_tags($this->input->get_post('password', true))),
		);
		if(!empty($data['email']) || !empty($data['password'])){
			$this->load->model('users/models/Auth_model');
			$validate = $this->Auth_model->validate_login_data($data);
			if(!empty($validate["errors"])){
				$errors = array(array_shift($validate["errors"]));
				$this->session->sess_destroy();
				$this->set_api_content('errors', $errors);
			}else{
				$login_return = $this->Auth_model->login_by_email_password($validate["data"]["email"], $this->Users_model->encode_password($validate["data"]["password"]));
				if(!empty($login_return["errors"])){
					$errors = $login_return["errors"];
					$this->set_api_content('errors', $errors);
					$this->session->sess_destroy();
				}else{
					$this->load->model('Mobile_model');
					$user_id = $this->session->userdata('user_id');
					$secret = $this->Mobile_model->save_user_secret($user_id);
					
					$lang_id = $this->session->userdata('lang_id');
					$this->pg_language->current_lang_id = $lang_id;
					$current_lang = $this->pg_language->get_lang_by_id($lang_id);
					
					$data = array(
						'token' => $this->session->sess_create_token(), 
						'user_id' => $user_id,
						'secret' => $secret,
						'language' => $current_lang['code'],
						'l' => $this->pg_language->pages->return_module('mobile', $lang_id),
						'd' => $this->pg_language->ds->return_module('mobile', $lang_id),
					);
					$this->set_api_content('data', $data);
				}
			}
		}else{
			$data = array(
				'user_id' => 0,
				'secret' => '',
			);
			
			$user_id = intval($this->input->get_post('user_id', true));
			$secret = trim(strip_tags($this->input->get_post('user_secret', true)));
		
			if($user_id && $secret){
				$user_secret = $this->Mobile_model->get_user_secret($user_id);
				if($secret == $user_secret){
					$this->load->model('Users_model');
					$this->Users_model->set_format_settings('get_safe', false);
					$user = $this->Users_model->get_user_by_id($user_id);
					$this->Users_model->set_format_settings('get_safe', true);
					if(!empty($user)){
						$this->load->model('users/models/Auth_model');
						$login_return = $this->Auth_model->login_by_email_password($user['email'], $user['password']);
						if (empty($login_return["errors"])) {
							$data = array(
								'user_id' => $user_id,
								'secret' => $secret,
							);
						}
					}
				}
			}
			
			$data['token'] = $this->session->sess_create_token();
			$this->set_api_content('data', $data);
		}
	}
	
	/**
	 * Backend action
	 * 
	 * @return void
	 */
	public function backend(){
		$data = array();
		
		if($this->session->userdata('auth_type') == 'user'){
			$user_id = $this->session->userdata('user_id');
		}else{
			$user_id = 0;
		}
		
		$modules = $this->input->post('gids', true);
		if($modules){
			foreach((array)$modules as $module){
				switch($module){
					case 'mailbox':
						if(!$user_id) break;
						$this->load->model('Mailbox_model');
						$data['mailbox']['new_messages_count'] = 0;
						$chats = $this->Mailbox_model->get_chats_list(array('where'=>array('id_com_user'=>$user_id)));
						foreach($chats as $chat){
							$data['mailbox']['new_messages_count'] += $chat['new'];
						}
					break;
					case 'message':
						if(!$user_id) break;
						$this->load->model('Mailbox_model');
						$chats = $this->input->post('chats', true);
					
						if(!$chats) return;
						foreach($chats as $chat){
							$data['mailbox']['chats'][$chat]['messages'] = array();
						}
						
						$date_format = $this->pg_date->get_format('date_time_literal', 'st');
						
						$this->load->helper('date_format');
						
						$messages = $this->Mailbox_model->get_messages(array('where'=>array('is_new'=>1), 'where_in'=>array('id_chat'=>(array)$chats)));
						foreach($messages as $message){
							$message['date_add_output'] = tpl_date_format($message['date_add'], $date_format);
							$data['mailbox']['chats'][$message['id_chat']]['messages'][] = $message;
						}
						foreach($data['mailbox']['chats'] as $chat_id => $chat_data){ 
							if(empty($chat_data['messages'])) continue;
							$ids = array();
							foreach($chat_data['messages'] as $message){
								$ids[] = $message['id'];
							}
							$this->Mailbox_model->set_read_messages($chat_id, $ids);
						}
					break;
				}
			}
		}
		
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Index action
	 * 
	 * @param string $type block type
	 * @return void
	 */
	public function index(){
		$data = array();
		
		$type = $this->pg_module->get_module_config('mobile', 'index_type_block');
		$count = $this->pg_module->get_module_config('mobile', 'index_count_block');
			
		$filters = array('active'=>1);
		$order_by = array();
		
		$data['type'] = $type;
			
		switch($type){
			case 'latest':
				$order_by = array('date_created'=>'DESC');
			break;
			case 'featured':
				$filters['featured'] = 1;
				$order_by = array('RAND()'=>'');
			break;
			case 'sale':
				$filters['type'] = 'sale';
				$order_by = array('date_created'=>'DESC');
			break;
			case 'rent':
				$filters['type'] = 'rent';
				$order_by = array('date_created'=>'DESC');
			break;
		}
			
		$this->load->model('Listings_model');
			
		$this->Listings_model->set_format_settings('get_user', false);
		$listings = $this->Listings_model->get_listings_list($filters, 1, $count, $order_by);
		$this->Listings_model->set_format_settings('get_user', true);
			
		foreach($listings as $i=>$listing){
			$data['listings'][$i] = array(
				'listing_id' => $listing['id'],
				'logo_image' => $listing['media']['photo']['thumbs'],
			);
		
			unset($listing['id']);
			unset($listing['logo_image']);
			unset($listing['media']);
			unset($listing['listing_file']); 
			unset($listing['listing_file_date']);
			unset($listing['listing_video']);
			unset($listing['listing_video_image']);
			unset($listing['listing_video_data']);
		
			foreach($listing as $name=>$value){
				$data['listings'][$i]['listing_'.$name] = $value;
			}
		}
		
		$this->set_api_content('data', $data);
	}

	/**
	 * Return languages data
	 * 
	 * @param string $type block type
	 * @return void
	 */
	public function languages($lang_id=null){
		if(!$lang_id || !$this->pg_language->is_active($lang_id)) 
			$lang_id = $this->pg_language->current_lang_id;
			
		$data = array(
			'l' => $this->pg_language->pages->return_module('mobile', $lang_id),
			'd' => $this->pg_language->ds->return_module('mobile', $lang_id),
		);
		
		$this->set_api_content('data', $data);
	}
}

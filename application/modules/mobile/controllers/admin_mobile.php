<?php

/**
 * Admin mobile controller
 *
 * @package PG_RealEstate
 * @subpackage Mobile
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Admin_Mobile extends Controller {

	/**
	 * Controller
	 */
	function __construct(){
		parent::Controller();
		$this->load->model('Menu_model');
		$this->Menu_model->set_menu_active_item('admin_menu', 'mobile_menu_item');
	}
	
	/**
	 * Settings page action
	 * 
	 * @return void
	 */
	public function index(){
		if($this->input->post('btn_save')){
			$this->load->model('Mobile_model');
			
			$post_data['index_type_block'] = $this->input->post('index_type_block');
			$post_data['index_count_block'] = $this->input->post('index_count_block');
			$post_data['geomap_driver'] = $this->input->post('geomap_driver');
			
			$validate_data = $this->Mobile_model->validate_settings($post_data);
			if(!empty($validate_data['errors'])){
				$this->system_messages->add_message('error', $validate_data['errors']);
				$data = $validate_data['data'];
			}else{
				foreach($validate_data['data'] as $setting=>$value){
					$this->pg_module->set_module_config('mobile', $setting, $value);
				}
				$this->system_messages->add_message('success', l('success_settings_saved', 'mobile'));
				$data = $validate_data['data'];
			}
		}else{
			$data['index_type_block'] = $this->pg_module->get_module_config('mobile', 'index_type_block');
			$data['index_count_block'] = $this->pg_module->get_module_config('mobile', 'index_count_block');
			$data['geomap_driver'] = $this->pg_module->get_module_config('mobile', 'geomap_driver');
		}
		$this->template_lite->assign('data', $data);
		
		$this->system_messages->set_data('header', l('admin_header_settings', 'mobile'));
		$this->template_lite->view('settings');
	}
}

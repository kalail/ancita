<?php  

/**
 * Dynamic blocks management
 * 
 * @package PG_RealEstate
 * @subpackage Dynamic blocks
 * @category	helpers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Mikhail Makeev <mmakeev@pilotgroup.net>
 * @version $Revision: 68 $ $Date: 2010-01-11 16:02:23 +0300 (Пн, 11 янв 2010) $ $Author: irina $
 **/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('dynamic_blocks_area'))
{
	function dynamic_blocks_area($area_gid){
		$CI = & get_instance();
		$CI->load->model("Dynamic_blocks_model");
		return $CI->Dynamic_blocks_model->html_area_blocks_by_gid($area_gid);
	}
}

if ( ! function_exists('dynamic_blocks_html'))
{
	/**
	 * Output html code
	 * 
	 * @param string $html html text
	 * @param string $title title text
	 * @return string
	 */
	function dynamic_blocks_html($html, $title='', $use_title=false){
		$CI = & get_instance();
		$CI->load->model('Dynamic_blocks_model');
		
		if(!$html) return '';
		
		$CI->template_lite->assign('use_title', $use_title);
		$CI->template_lite->assign('block_title', $title);
		$CI->template_lite->assign('block_html', $html);
		return $CI->template_lite->fetch("helper_html_block", "user", "dynamic_blocks");	
	}
}

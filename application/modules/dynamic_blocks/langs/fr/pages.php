<?php

$install_lang["add_area_block_header"] = "Ajouter bloc";
$install_lang["admin_header_area_add"] = "Ajouter région";
$install_lang["admin_header_area_blocks_edit"] = "Blocs dynamiques";
$install_lang["admin_header_area_change"] = "Èditer paramètres de blocs";
$install_lang["admin_header_area_edit"] = "Blocs dynamiques";
$install_lang["admin_header_area_layout"] = "Blocs dynamiques";
$install_lang["admin_header_areas_list"] = "Blocs dynamiques";
$install_lang["admin_header_block_edit"] = "Blocs dynamiques";
$install_lang["admin_header_blocks_list"] = "Blocs dynamiques";
$install_lang["admin_header_dynblock_add"] = "Ajouter bloc";
$install_lang["admin_header_dynblock_change"] = "Éditer paramètres de bloc";
$install_lang["error_area_already_exists"] = "Région avec ce mot clé existe déja";
$install_lang["error_function_empty"] = "Paramètres de méthode vides";
$install_lang["error_function_invalid"] = "Méthode invalide";
$install_lang["error_gid_empty"] = "Champ de mot clé obligatoire";
$install_lang["error_gid_mandatory_field"] = "Champ de mot clé obligatoire";
$install_lang["error_name_mandatory_field"] = "Champ de nom est obligatoire";
$install_lang["field_cache_time"] = "Période de cache";
$install_lang["field_cache_time_text"] = "in sec.; 0 - aucune cache";
$install_lang["field_gid"] = "Mot clé";
$install_lang["field_method"] = "Méthode";
$install_lang["field_model"] = "Modèle";
$install_lang["field_module"] = "Module";
$install_lang["field_name"] = "Titre";
$install_lang["field_param_default"] = "Défaut";
$install_lang["field_param_gid"] = "Mot clé";
$install_lang["field_param_name"] = "Nom";
$install_lang["field_param_type"] = "Type";
$install_lang["field_params"] = "Paramètres";
$install_lang["field_view"] = "Voir type";
$install_lang["field_views"] = "Vues";
$install_lang["filter_area_blocks"] = "Ajouter blocs";
$install_lang["filter_area_layout"] = "Arranger blocs";
$install_lang["link_add_block"] = "Ajouter";
$install_lang["link_add_dynamic_block"] = "Ajouter un bloc";
$install_lang["link_add_dynamic_block_area"] = "Ajouter une région";
$install_lang["link_add_new_param"] = "Ajouter nouveau";
$install_lang["link_add_new_view"] = "Ajouter nouveau";
$install_lang["link_delete_area"] = "Enlever région";
$install_lang["link_delete_block"] = "Détruire bloc";
$install_lang["link_edit_area"] = "Èditer paramètres de blocs";
$install_lang["link_edit_area_blocks"] = "Gèrer blocs dynamiques";
$install_lang["link_edit_block"] = "Èditer blocs";
$install_lang["link_save_block_sorting"] = "Sauvegarder cet arrangement";
$install_lang["no_areas"] = "Aucune régions pour l'instant";
$install_lang["no_blocks"] = "Aucun blocs pour l'instant";
$install_lang["note_delete_area"] = "Êtes vous sur de vouloir détruire cette région?";
$install_lang["note_delete_area_block"] = "Êtes vous sur de vouloir détruire ce bloc?";
$install_lang["note_delete_block"] = "Êtes vous sur de vouloir détruire ce bloc?";
$install_lang["others_languages"] = "Autre langues";
$install_lang["success_add_area_block"] = "Région ajoutée, SVP éditer ses paramètres";
$install_lang["success_add_block"] = "Bloc ajouté";
$install_lang["success_delete_area"] = "Région détruite";
$install_lang["success_delete_block"] = "Bloc détruit";
$install_lang["success_update_area"] = "Région mise à jour";
$install_lang["success_update_area_block"] = "Bloc sauvé";
$install_lang["success_update_area_layout"] = "Plan sauvegardé";
$install_lang["success_update_block"] = "Bloc mis à jour";


<?php

$install_lang["admin_dynblocks_menu_areas_list_item"] = "Régions";
$install_lang["admin_dynblocks_menu_areas_list_item_tooltip"] = "";
$install_lang["admin_dynblocks_menu_blocks_list_item"] = "Bloquer paramètres";
$install_lang["admin_dynblocks_menu_blocks_list_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_interface-items_dynblock_menu_item"] = "Blocs dynamiques";
$install_lang["admin_menu_settings_items_interface-items_dynblock_menu_item_tooltip"] = "Blocs avec contenu dynamique sur page d'atterrissage";


<?php

$install_lang["admin_dynblocks_menu_areas_list_item"] = "Áreas";
$install_lang["admin_dynblocks_menu_areas_list_item_tooltip"] = "";
$install_lang["admin_dynblocks_menu_blocks_list_item"] = "Configuración de Bloques";
$install_lang["admin_dynblocks_menu_blocks_list_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_interface-items_dynblock_menu_item"] = "Bloques dinámicos";
$install_lang["admin_menu_settings_items_interface-items_dynblock_menu_item_tooltip"] = "Bloques con contenido dinámico en la página delantera del sitio";


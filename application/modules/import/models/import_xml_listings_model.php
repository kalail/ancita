<?php

if (!defined('CONVERSION_XPATH_TABLE')) define('CONVERSION_XPATH_TABLE', DB_PREFIX.'conversion_rules_xpath');
if (!defined('CONVERSION_VALUES_TABLE')) define('CONVERSION_VALUES_TABLE', DB_PREFIX.'conversion_rules_values');
if (!defined('PAYMENT_CURRENCY_TABLE')) define('PAYMENT_CURRENCY_TABLE', DB_PREFIX.'payments_currency');
if (!defined('COUNTRY_TABLE')) define('COUNTRY_TABLE', DB_PREFIX.'cnt_countries');
if (!defined('REGION_TABLE')) define('REGION_TABLE', DB_PREFIX.'cnt_regions');
if (!defined('CITY_TABLE')) define('CITY_TABLE', DB_PREFIX.'cnt_cities');
if (!defined('LISTINGS_TABLE')) define('LISTINGS_TABLE', DB_PREFIX.'listings');
if (!defined('LISTINGS_RESIDENTIAL_TABLE')) define('LISTINGS_RESIDENTIAL_TABLE', DB_PREFIX.'listings_residential');
define('LISTINGS_COMMERCIAL_TABLE', DB_PREFIX.'listings_commercial');
define('LISTINGS_HEADLINE_COMMENTS_TABLE', DB_PREFIX.'listings_headline_comments');
if (!defined('CRON_XML_TABLE')) define('CRON_XML_TABLE', DB_PREFIX.'cron_xml_import');
if (!defined('USERS_TABLE')) define('USERS_TABLE', DB_PREFIX.'users');
if (!defined('CRON_XML_STATUS_TABLE')) define('CRON_XML_STATUS_TABLE', DB_PREFIX.'cron_xml_import_status');
if (!defined('LISTING_TEMP_TABLE')) define('LISTING_TEMP_TABLE', DB_PREFIX.'temp_listing_import_cron');
if (!defined('LISTING_TEMP_TABLE_CRON_MANUAL')) define('LISTING_TEMP_TABLE_CRON_MANUAL', DB_PREFIX.'temp_listing_import_cron_manual');
if (!defined('LISTING_TEMP_IMAGE_TABLE')) define('LISTING_TEMP_IMAGE_TABLE', DB_PREFIX.'temp_listing_import_image_cron');
if (!defined('LISTING_TEMP_TABLE_MANUAL')) define('LISTING_TEMP_TABLE_MANUAL', DB_PREFIX.'temp_listing_import_manual');
if (!defined('UPLOADS_THUMB_TABLE')) define('UPLOADS_THUMB_TABLE', DB_PREFIX.'uploads_thumb');
/**
 * Import model
 * 
 * @package PG_RealEstate
 * @subpackage Import
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Import_xml_listings_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;

	/**
	 * Link to database object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Cache for used drivers
	 * 
	 * @var array
	 */
	private $driver_cache = array();
	
	/**
	 * Cache for used modules
	 * 
	 * @var array
	 */
	private $module_cache = array();
	
	/**
	 * Path to uploaded files
	 * 
	 * @var string
	 */
	private $base_path = "import/";
	
	/**
	 * Import data limit per request
	 * 
	 * @var integer
	 */
	protected $import_limit = 1000;
	
	/**
	 * Constructor
	 *
	 * @return Import_model
	 * @required Import_driver_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}
	
	
	public function import_listing_xml_cron() {
		$this->save_cron_data();
		echo "Letters sent: hello ( 0 errors)";
	}
	
	public function save_cron_data()
	{
		$strSQL = "INSERT INTO pg_zz_test (Status,DateCreated) VALUES(1,now())";
		$this->DB->query($strSQL);
		return;
	}
	
	
	public function update_convertion_xpath($active_rule,$xmlcode){
		$strSQL = "UPDATE ".CONVERSION_XPATH_TABLE." SET active_rule='".$active_rule."' WHERE xml_code='".$xmlcode."'";
		$this->DB->query($strSQL);
		return true;
	}
	
	public function save_Convertion_value($data)
	{
		$this->DB->insert(CONVERSION_VALUES_TABLE, $data);
		return true;
	}
	
	public function save_listings_import_defenition($data, $id=0){
		if($id == 0){
			$this->DB->insert(CRON_XML_TABLE, $data);
		} else {
			$this->DB->where("id", $id);
			$this->DB->update(CRON_XML_TABLE, $data);
		}
		return true;
	}
	
	public function delete_listings_defenition($id){
		$this->DB->where("id", $id);
		$this->DB->delete(CRON_XML_TABLE);
	}
	
	public function save_xml_import_status($data, $id=0){
		if($id == 0){
			$this->DB->insert(CRON_XML_STATUS_TABLE, $data);
			$xml_id = $this->DB->insert_id();
			return $xml_id;
		} else {
			$this->DB->where("Id", $id);
			$this->DB->update(CRON_XML_STATUS_TABLE, $data);
			return true;
		}
	}
	
	public function delete_convertion_value($id,$xmlcode){
		$this->DB->where("id", $id);
		$this->DB->where("xml_code", $xmlcode);
		$this->DB->delete(CONVERSION_VALUES_TABLE);
	}
	public function delete_listings_manual_setting_table($cron_xml_data){
		$this->DB->where("id", $cron_xml_data['id']);
		$this->DB->where("import_type", $cron_xml_data['import_type']);
		$this->DB->delete(LISTING_TEMP_TABLE_MANUAL);
	}
	public function get_convertion_value($xmlcode)
	{
		$data = array();
		$query = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$xmlcode."' ORDER BY ancita_field";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data;
	}
	
	public function save_custom_listings($listing_id,$data)
	{
		$this->CI->load->model('Listings_model');
		$listing_id = $this->CI->Listings_model->save_listing($listing_id,$data);
		return $listing_id;
	}
	
	public function get_operation_type($operation_type)
	{
		$this->CI->load->model('Listings_model');
		$operation_types = array_flip($this->CI->Listings_model->get_operation_types());
		return $operation_types[$operation_type];
	}
	
	public function save_photo_file($listing_id,$upload,$photo_id=0)
	{
		
		$this->CI->load->model('Upload_gallery_model');
		$photo_type = $this->CI->Upload_gallery_model->get_type_by_gid('listings-photo');
		$data = array('type_id'=>$photo_type['id'], 'object_id'=>$listing_id);
		$return = $this->CI->Upload_gallery_model->save_file($photo_id, $data, '', false, true, $upload);
		if(!empty($return['errors'])) return $return;
	
		$file = $this->CI->Upload_gallery_model->get_file_by_id($return['id']);
		$this->CI->load->model('Listings_model');
		if($file['status']) $this->CI->Listings_model->_update_logo_image($listing_id);
			
		return $return;
	}
	
	public function get_convertion_xpath($xmlcode)
	{
		$data = array();
		$query = "SELECT * FROM ".CONVERSION_XPATH_TABLE." WHERE xml_code='".$xmlcode."'";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data;
	}
	
	public function update_status(){
		set_time_limit(30000);
		$this->CI->load->model('Listings_model');
		
		/*$strSQL = "SELECT id,price FROM ".LISTINGS_TABLE;
		$result = $this->get_data_custom($strSQL);
		for($i = 0; $i < count($result); $i++){
			$post_data['price'] = $result[$i]['price'];
			$listing_id = $result[$i]['id'];
			$validate_data = $this->CI->Listings_model->validate_listing($listing_id, $post_data);
			$data['price_sorting'] = $validate_data['data']['price_sorting'];
			$this->CI->Listings_model->save_listing($listing_id, $data);
		}*/
		
		/*$strSQL = "SELECT id FROM pg_z_templistid";
		$result = $this->get_data_custom($strSQL);
		for($i = 0; $i < count($result); $i++){
			$listing_id = $result[$i]['id'];
			$this->CI->Listings_model->activate_listing($listing_id,1);
		}*/
		
		/*$strSQL = "SELECT id,id_type,id_spr,sale,rent,temp_id FROM pg_zzzz_temp_test where id_type=1 LIMIT 0, 500";
		$result = $this->get_data_custom($strSQL);
		$record = 0;
		for($i = 0; $i < count($result); $i++){
			
			$aid = '';
			$columnid = '';
			
			$type = trim($result[$i]['id_type']);
			
			
			if($type == '1'){$aid = '1';}
			if($type == '3') {$aid = '4';}
	
			$ctype = $result[$i]['id_spr'];
			
			if($ctype == '13'){
				$columnid = 'fe_other_equipment_'.$aid;
			} else if($ctype == '14'){
				$columnid = 'fe_environment_'.$aid;
			} else if($ctype == '15'){
				$columnid = 'fe_importants_'.$aid;
			} else if($ctype == '16'){
				$columnid = 'fe_appliances_'.$aid;
			}
			
			$values = array();
			if($type == '1'){
				$values = explode(',',$result[$i]['sale']);
			} else if($type == '3'){
				$values = explode(',',$result[$i]['rent']);
			}
			//echo $type.'|'.$aid.'|'.$columnid;die;
			if($columnid != '' && $aid != '' && $type != ''){
				if(isset($values)){
					$post_data[$columnid] = $values;
					$listing_id = $result[$i]['id'];
					$validate_data = $this->CI->Listings_model->validate_listing($listing_id, $post_data);
					$data[$columnid] = $validate_data['data'][$columnid];
					$this->CI->Listings_model->save_listing($listing_id, $data);
					$this->DB->where("temp_id", $result[$i]['temp_id']);
					$this->DB->delete('pg_zzzz_temp_test');
				}
			}
			$record++;
		}*/
		
		echo 'Completed record '.$record;
	}
	
	public function get_data_custom($query)
	{
		$data = array();
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data;
	}
	
	public function get_default_currency_symbol()
	{
		$data = array();
		$query = "SELECT * FROM " .PAYMENT_CURRENCY_TABLE. " WHERE is_default = 1";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data[0];
	}
	
	public function set_format_data($data){
		foreach($data as $key=>$value){
			if(isset($this->format_data[$key]))
				$this->format_data[$key] = $value;
		}
	}
	
	public function save_temp_listing($strSQL)
	{
		$this->DB->query($strSQL);
	}
	public function update_latlon()
	{
        $strSQL = "UPDATE ".LISTINGS_TABLE." p, ".CITY_TABLE." q SET p.lat = q.latitude, p.lon = q.longitude WHERE p.id_city= q.id";
		$this->DB->query($strSQL);
		$strSQL1 = "UPDATE ".LISTINGS_RESIDENTIAL_TABLE." t1, ".LISTINGS_HEADLINE_COMMENTS_TABLE." t2 SET t1.search_field = t2.headline_comments WHERE t1.id_listing = t2.id_listing and t2.text_type ='headline'";
		$this->DB->query($strSQL1);
		$strSQL2 = "UPDATE ".LISTINGS_COMMERCIAL_TABLE." t1, ".LISTINGS_HEADLINE_COMMENTS_TABLE." t2 SET t1.search_field = t2.headline_comments WHERE t1.id_listing = t2.id_listing and t2.text_type ='headline'";
		$this->DB->query($strSQL2);
		return true;
	}
	public function get_width($id,$config_id){
		$id = 8;
		$config_id = 7;
		$query = "SELECT width FROM ".UPLOADS_THUMB_TABLE." WHERE id = '".$id."' and config_id = '".$config_id."'";
		$result = $this->DB->query($query)->result();
		return $result[0]->width;
	}
	public function get_temp_listings_manual_by_xml_import_id($id) {
		$strSQL = "SELECT * FROM ".LISTING_TEMP_TABLE_CRON_MANUAL." WHERE xml_import_id = '".$id."' AND status IS NULL OR status = '' LIMIT 0, 500";
		$result = $this->get_data_custom($strSQL);
		return $result;
	}
	public function check_manual_temp_table_empty($user_id) {
		$strSQL = "SELECT * FROM ".LISTING_TEMP_TABLE_CRON_MANUAL." WHERE id_user = '".$user_id."'";
		$result = $this->get_data_custom($strSQL);
		return $result;
	}
	public function get_temp_listings_by_xml_import_id($id) {
		$strSQL = "SELECT * FROM ".LISTING_TEMP_TABLE." WHERE xml_import_id = '".$id."' AND status IS NULL OR status = '' LIMIT 0, 500";
		$result = $this->get_data_custom($strSQL);
		return $result;
	}
	
	public function delete_temp_listings($temp_list_del){
		$this->DB->where("id_user", $temp_list_del['id_user']);
		$this->DB->where("id_agent_property", $temp_list_del['id_agent_property']);
		$this->DB->where("xml_import_id", $temp_list_del['xml_import_id']);
		$this->DB->delete(LISTING_TEMP_TABLE);
		if($temp_list_del['import'] == 'manual'){
		$this->DB->where("id_user", $temp_list_del['id_user']);
		$this->DB->where("id_agent_property", $temp_list_del['id_agent_property']);
		$this->DB->where("xml_import_id", $temp_list_del['xml_import_id']);
		$this->DB->delete(LISTING_TEMP_TABLE_CRON_MANUAL);
		}
	}
	
	public function update_temp_listings($temp_list,$data) {
		$this->DB->where("id_user", $temp_list['id_user']);
		$this->DB->where("id_agent_property", $temp_list['id_agent_property']);
		$this->DB->where("xml_import_id", $temp_list['xml_import_id']);
		$this->DB->update(LISTING_TEMP_TABLE, $data);
	}
	
	public function _update_temp_cron_status($data, $id) {
		$count = count($data['update']) + count($data['insert']);
		$sts_xml['import_listings'] = $count;
		$sts_xml['import_listings_ids'] = json_encode($data);
		$this->save_xml_import_status($sts_xml,$id);
	}
	
	public function get_temp_listings_image() {
		$strSQL = "SELECT * FROM ".LISTING_TEMP_IMAGE_TABLE." WHERE status IS NULL OR status = '' LIMIT 0, 25";
		$result = $this->get_data_custom($strSQL);
		return $result;
	}
	
	public function delete_temp_listings_image($listing_id,$image_url){
		$this->DB->where("id_listing", $listing_id);
		$this->DB->where("photo_url", $image_url);
		$this->DB->delete(LISTING_TEMP_IMAGE_TABLE);
	}
	
	public function update_temp_listing_image($data,$listing_id,$image_url) {
		$this->DB->where("id_listing", $listing_id);
		$this->DB->where("photo_url", $image_url);
		$this->DB->update(LISTING_TEMP_IMAGE_TABLE, $data);
	}

}

<?php  

if(!defined("BASEPATH")) exit("No direct script access allowed");

define("IMPORT_MODULES_TABLE", DB_PREFIX."import_modules");

/**
 * Import module model
 * 
 * @package PG_RealEstate
 * @subpackage Import
 * @category models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Import_module_model extends Model{
	/**
	 * Link to CodeIgniter model
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Attibutes of import module object
	 * 
	 * @var array
	 */
	private $attrs = array(
		"id", 
		"module", 
		"model",
		"callback_get_fields",
		"callback_import_data",
		"sorter",
		"date_created", 
		"date_modified",
	);

	/**
	 * Cache module data
	 * 
	 * @var array
	 */
	private $module_cache = array();

	/**
	 * Constructor
	 * 
	 * @return Import_module_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return module object by identifier
	 * 
	 * @param integer $module_id module identifier
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_module_by_id($module_id, $formatted=true){
		if(empty($this->module_cache[$module_id])){
			$this->DB->select(implode(", ", $this->attrs))->from(IMPORT_MODULES_TABLE)->where("id", $module_id);
			$results = $this->DB->get()->result_array();
			if(!empty($results) && is_array($results)){
				if($formatted){
					$this->module_cache[$module_id] = $this->format_module($results[0]);
				}else{
					return $results[0];
				}
			}
		}
		return $this->module_cache[$module_id];
	}

	/**
	 * Return module objects as array
	 * 
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_modules($formatted=true){
		$data = array();
		$this->DB->select(implode(", ", $this->attrs))->from(IMPORT_MODULES_TABLE)->order_by('sorter');
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $result){
				if($formatted){
					$data[] = $this->format_module($result);
				}else{
					$data[] = $result;
				}
			}
		}
		return $data;
	}
	
	/**
	 * Save module object to data source
	 * 
	 * @param integer $module_id module identifier
	 * @param array $data module data
	 * @return integer
	 */
	public function save_module($module_id, $data){
		if(!$module_id){
			$data["date_created"] = date("Y-m-d H:i:s");
			$data["date_modified"] = date("Y-m-d H:i:s");
			$this->DB->insert(IMPORT_MODULES_TABLE, $data);		
			$module_id = $this->DB->insert_id();
		}else{
			$this->DB->where("id", $module_id);
			$this->DB->update(IMPORT_MODULES_TABLE, $data);
		}
		return $module_id;
	}
	
	/**
	 * Remove import module object from data source
	 * 
	 * @param string $module_gid module guid
	 * @return void
	 */
	public function remove_module($module_gid){
		$this->DB->where("module", $module_gid);
		$this->DB->delete(IMPORT_MODULES_TABLE);
	}
	
	/**
	 * Validate module data
	 * 
	 * @param integer $module_id module identifier
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_module($module_id, $data){
		$return = array("errors"=> array(), "data" => array());
		
		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['module'])){
			$return['data']['module'] = trim(strip_tags($data["module"]));
			if(empty($return['data']['module'])) $return["errors"][] = l("error_empty_module", "import");
		}elseif(!$module_id){
			$return["errors"][] = l("error_empty_module", "export");
		}
		
		if(isset($data['model'])){
			$return['data']['model'] = trim(strip_tags($data["model"]));
			if(empty($return['data']['model'])) $return["errors"][] = l("error_empty_model", "import");
		}elseif(!$module_id){
			$return["errors"][] = l("error_empty_model", "import");
		}
		
		if(isset($data['callback_get_fields'])){
			$return['data']['callback_get_fields'] = trim(strip_tags($data["callback_get_fields"]));
			if(empty($return['data']['callback_get_fields'])) $return["errors"][] = l("error_empty_callback_get_fields", "import");
		}elseif(!$module_id){
			$return["errors"][] = l("error_empty_callback_get_fields", "import");
		} 
		
		if(isset($data['callback_import_data'])){
			$return['data']['callback_import_data'] = trim(strip_tags($data["callback_import_data"]));
			if(empty($return['data']['callback_import_data'])) $return["errors"][] = l("error_empty_callback_import_data", "import");
		}elseif(!$module_id){
			$return["errors"][] = l("error_empty_callback_import_data", "import");
		} 
		
		if(isset($data['sorter'])){
			$return['data']['sorter'] = intval($data['sorter']);
		}
		
		if(isset($data['date_created'])){
			$value = strtotime($data['date_created']);
			if($value > 0) $return['data']['date_created'] = date("Y-m-d", $value);
		}
		
		if(isset($data['date_modified'])){
			$value = strtotime($data['date_modified']);
			if($value > 0) $return['data']['date_modified'] = date("Y-m-d", $value);
		}
		
		return $return;
	}
	
	/**
	 * Format module data
	 * 
	 * @param array $data module data
	 * @return array
	 */
	public function format_module($data){
		$data["output_name"] = l("module_".$data["module"], "import");
		return $data;
	}
	
	/**
	 * Return module object by default
	 * 
	 * @param integer $module_id module identifier
	 * @return array
	 */
	public function format_default_module($module_id){
		return null;
	}
	
	/**
	 * Return custom fields which available for import
	 * 
	 * @param integer $module_id module identifier
	 * @return array
	 */
	public function get_module_fields($module_id){
		$module = $this->get_module_by_id($module_id);
		if(empty($module["module"]) || empty($module["model"]) ||
		   empty($module["callback_get_fields"])) return array();
		$this->CI->load->model($module["module"]."/models/".$module["model"], $module["model"]);
		$fields = $this->CI->{$module["model"]}->{$module["callback_get_fields"]}();
		return $fields;
	}
	
	/**
	 * Return import data from module
	 * 
	 * @param array $data selected values
	 * @param integer $module_id module identifier
	 * @param array $relations relations between fields
	 * @param integer $user_id user identifier
	 * @param boolean $use_moderation use data moderation
	 * @return array
	 */
	public function import_data($data=array(), $module_id, $relations=array(), $user_id=0, $use_moderation=false){
		$module = $this->get_module_by_id($module_id);
		if(empty($module["module"]) || empty($module["model"]) ||
		   empty($module["callback_import_data"])) return array();
		$this->CI->load->model($module["module"]."/models/".$module["model"], $module["model"]);
		return $this->CI->{$module["model"]}->{$module["callback_import_data"]}($data, $relations, $user_id);
	}
	
	/**
	 * Update module languages
	 * 
	 * @param array $data module data
	 * @param array $langs_file langauges data from file
	 * @param array $langs_ids langauges identifiers
	 * @return void
	 */
	public function update_lang($data, $langs_file, $langs_ids){
		foreach($data as $value){
			$this->CI->pg_language->pages->set_string_langs(
				"import", 
				$value, 
				$langs_file[$value], 
				$langs_ids);
		}
	}
	
	/**
	 * Export module languages
	 * 
	 * @param array $data module data
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function export_lang($data, $langs_ids){
		$langs = array();
		return array_merge($langs, $this->CI->pg_language->export_langs("export", (array)$data, $langs_ids));
	}
}

{include file="header.tpl"}
<div class="menu-level2">
	<ul>
		<li{if $tab_id eq 'list'} class="active"{/if}><div class="l"><a href="{$site_url}admin/import/import_automatic_listings/list">Import definitions</a></div></li>
        <li{if $tab_id eq 'new' || $tab_id eq 'edit'} class="active"{/if}><div class="l"><a href="{$site_url}admin/import/import_automatic_listings/{if $tab_id eq 'edit'}{$strText.tmplink}{else}new{/if}">{$strText.tabtxt}</a></div></li>
	</ul>
	&nbsp;
</div>
<div class="actions">&nbsp;</div>
<form method="post" name="save_form" action="{$site_url}admin/import/import_automatic_listings/{if $tab_id eq 'edit'}{$strText.tmplink}{else}{$tab_id}{/if}">
{if $tab_id eq 'list'}
<div class="edit-form edit-form-marketing">
<div class="row header">Import Definitions</div>
<div class="row"></div>
<table cellpadding="0" cellspacing="1" width="100%" border="0" class="data">
	<tr>
    	<th>Name</th>
        <th>Link</th>
        <th>Import Type</th>
        <th>Frequency</th>
        <th>Overwrite Type</th>
        <th>Copy english text</th>
        <th>Add property ID</th>
        <th>Started</th>
        <th>&nbsp;</th>
    </tr>
    {if $xmldata}
    {foreach from=$xmldata item=item key=key}
    <tr>
        <td>{$item.user_name}</td>
        <td>{$item.link}</td>
        <td>{$item.import_type}</td>
        <td>{$item.frequency}</td>
        <td>{if $item.overwrite_type eq '1000'}Overwrite all the existing records
            {elseif $item.overwrite_type eq '1001'}Skip update of existing records
            {elseif $item.overwrite_type eq '1002'}Do not overwrite existing records with blank data{/if}
        </td>
        <td>{$item.copy_english_text}</td>
        <td>{$item.add_property_id}</td>
        <td>{$item.date_start|date_format:$page_data.date_format}</td>
        <td>
        	<a href="{$site_url}admin/import/import_automatic_listings/edit/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_user' gid='users' type='button'}" title="{l i='link_edit_data' gid='import' type='button'}"></a>
            <a href="{$site_url}admin/import/import_automatic_listings/list/{$item.id}/delete" onclick="javascript: if(!confirm('Are you sure you want to delete this rule?')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_data' gid='import' type='button'}" title="{l i='link_delete_data' gid='import' type='button'}"></a>
        </td>
    </tr>
    {/foreach}
    {else}
    <tr>
        <td colspan="9" class="error" align="center">Import Definitions is empty</td>
    </tr>
    {/if}
</table>
</div>
{/if}
{if $tab_id eq 'new' || $tab_id eq 'edit'}
<div class="edit-form edit-form-marketing">
<div class="row header">{$strText.subtitle}</div>
<table style="width:100%;">
<tr height="40">
    <td>Name</td><td>{user_select selected=$custVal.user_id max=1 var_name='id_user'}Chose Ancita user for definition assignment</td>
</tr>
<tr height="40">
	<td>Link</td><td><input type="text" style="width:95%;" name="txtlink" value="{$custVal.link}" /></td>
</tr>
<tr height="40">
	<td>Import Type</td>
    <td>
    	<select name="import_type" id="import_type" style="width: 320px;">
            <option value="0">Please make a choice of the import type</option>
            <option value="kyero" {if $custVal.import_type eq 'kyero'} selected {/if}>Kyero</option>
            <option value="alphashare" {if $custVal.import_type eq 'alphashare'} selected {/if}>Alphashare</option>
            <option value="thinkspain" {if $custVal.import_type eq 'thinkspain'} selected {/if}>Thinkspain</option>
        </select>
    </td>
</tr>
<tr height="40">
	<td>Frequency</td>
    <td>
    	<select name="frequency" id="frequency" style="width: 320px;">
            <option value="0">Please make a choice of the import frequency</option>
            <option value="daily" {if $custVal.frequency eq 'daily'} selected {/if}>Daily</option>
            <option value="weekly" {if $custVal.frequency eq 'weekly'} selected {/if}>Weekly</option>
        </select>
    </td>
</tr>
<tr height="40">
	<td>Overwrite Type</td>
    <td>
    	<select name="overwrite_type" id="overwrite_type" style="width: 320px;">
            <option value="0">Please make a choice of the overwrite type</option>
            <option value="1000" {if $custVal.overwrite_type eq '1000'} selected {/if}>Overwrite all the existing records</option>
            <option value="1001" {if $custVal.overwrite_type eq '1001'} selected {/if}>Skip update of existing records</option>
            <option value="1002" {if $custVal.overwrite_type eq '1002'} selected {/if}>Do not overwrite existing records with blank data</option>
        </select>
    </td>
</tr>
<tr height="40">
	<td style="vertical-align:top;">Additional options</td>
	 <td>
    	<table cellpadding="0" cellspacing="1" border="0">
            <tr height="3"></tr>											
            <tr>
                <td>
                    <label>Copy the English heading text into the Norwegian and Swedish language?</label>
                </td>
                <td><input type="radio" name="copy_english_text" value="true" {if $custVal.copy_english_text eq 'true'} checked {/if}/><span>Yes</span></td>
                <td><input type="radio" name="copy_english_text" value="false" {if $custVal.copy_english_text eq 'false'} checked {/if}/><span>No</span></td>
            </tr>
            <tr height="3"></tr>												
            <tr>
                <td>
                    <label>Add Company agent property id to the heading?</label>
                </td>
                <td><input type="radio" name="add_property_id" value="true" {if $custVal.add_property_id eq 'true'} checked {/if}/><span>Yes</span></td>
                <td><input type="radio" name="add_property_id" value="false" {if $custVal.add_property_id eq 'false'} checked {/if}/><span>No</span></td>											
            </tr>
        </table>
    </td>
</tr>
<tr>
	<td></td>
    <td><div class="h"><div class="btn"><div class="l"><input type="submit" name="save_btn_definition" value="{l i='btn_save' gid='start' type='button'}"></div></div></div></td>
</tr>
</table>
</div>
{/if}
</form>
{include file="footer.tpl"}

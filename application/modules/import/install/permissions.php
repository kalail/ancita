<?php

$_permissions["admin_import"]["activate"] = 3;
$_permissions["admin_import"]["activate_driver"] = 3;
$_permissions["admin_import"]["ajax_get_custom_fields"] = 3;
$_permissions["admin_import"]["ajax_make"] = 3;
$_permissions["admin_import"]["ajax_parse"] = 3;
$_permissions["admin_import"]["ajax_upload_form"] = 3;
$_permissions["admin_import"]["data"] = 3;
$_permissions["admin_import"]["delete"] = 3;
$_permissions["admin_import"]["drivers"] = 3;
$_permissions["admin_import"]["edit"] = 3;
$_permissions["admin_import"]["index"] = 3;
$_permissions["admin_import"]["make"] = 3;
$_permissions["admin_import"]["parse"] = 3;
$_permissions["admin_import"]["relations"] = 3;
$_permissions["admin_import"]["upload"] = 3;
$_permissions["api_import"]["index"] = 2;
$_permissions["import"]["index"] = 2;

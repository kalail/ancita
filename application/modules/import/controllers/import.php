<?php

/**
 * Import user side controller
 * 
 * @package PG_RealEstate
 * @subpackage Import
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Import extends Controller{
	
	/**
	 * Constructor
	 *
	 * @return Import
	 */
	public function __construct(){
		log_message('debug', '====================================================================');
		log_message('debug', '++++++++++++++++++++ AUTO IMPORT CLASS STARTED ++++++++++++++++++++');
		log_message('debug', '====================================================================');
		parent::Controller();
		$this->load->model("import/models/Import_xml_listings_model");
		$this->load->model('Properties_model');
	}
	public function __destruct(){
		log_message('debug', '====================================================================');
		log_message('debug', '++++++++++++++++++++ AUTO IMPORT CLASS ENDED ++++++++++++++++++++');
		log_message('debug', '====================================================================');
	}
	
	/**
	 * Render simple import action
	 * 
	 * @param integer $import_id selection identifier
	 * @return void
	 */
	public function index($import_id){
		$this->load->model("import/models/Import_module_model");
		$this->load->model("Import_model");
	}
	
	public function xml_import_listings_day(){
		log_message('debug', '============ xml import per day function');
		$this->process_xml_import('daily');
	}
	
	public function xml_import_listings_week(){
	   log_message('debug', '============ xml import per week function');
		$this->process_xml_import('weekly');
	}
	
	public function update_listings_stats(){
		log_message('debug', '============ listing status update function');
		$this->Import_xml_listings_model->update_status();
	}
	
	public function process_xml_import($import_type){
		log_message('debug', '============ xml import per day function continue');
		error_reporting(0);
		set_time_limit(0);	
		$strSQL = "SELECT * FROM ".CRON_XML_TABLE." WHERE frequency='".$import_type."'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		for($row = 0; $row < count($tempResult); $row++){
			$arrXMLdef['id'] = $tempResult[$row]['id'];
			$arrXMLdef['user_id'] = $tempResult[$row]['user_id'];
			$arrXMLdef['link'] = $tempResult[$row]['link'];
			$arrXMLdef['import_type'] = $tempResult[$row]['import_type'];
			$arrXMLdef['frequency'] = $tempResult[$row]['frequency'];
			$arrXMLdef['overwrite_type'] = $tempResult[$row]['overwrite_type'];
			$arrXMLdef['copy_english_text'] = $tempResult[$row]['copy_english_text'];
			$arrXMLdef['add_property_id'] = $tempResult[$row]['add_property_id'];
			
			$sts_xml['xml_import_id'] = $arrXMLdef['id'];
			$sts_xml['start_datetime'] = date('Y-m-d H:i:s');
			$sts_xml['process_type'] = 'Process XML file';
			$xml_status_id = $this->Import_xml_listings_model->save_xml_import_status($sts_xml);
			log_message('debug', '============ XML status id : '.$xml_status_id);
			unset($sts_xml);
			
			$result = $this->import_temp_listing($arrXMLdef,$xml_status_id);
		}
		log_message('debug', '============ xml import per day function continue to file fetching');
	}
	
	
	public function import_temp_listing($arrXMLdef,$xml_status_id) {
		log_message('debug', '============ File fetching cron function is started');
		error_reporting(0);
		ini_set('time_limit', '-1');
		ini_set('memory_limit', '-1');
		$tempPath = SITE_PATH.SITE_SUBFOLDER.'temp/import';
		log_message('debug', '============ temp path : '.$tempPath);
		log_message('debug', '============ Agent xml link : '.$arrXMLdef['link']);
		$url=$arrXMLdef['link'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents
        $data = curl_exec($ch); // execute curl request
        curl_close($ch);
		$xml = simplexml_load_string($data);
		
		if($xml){
			log_message('debug', '============ Loaded data into XML variable from external agent link.');
			$file = $arrXMLdef['link'];
			$file_name = $arrXMLdef['user_id']."_".$arrXMLdef['id']."_".date("d_m_Y")."_import_listing_xml_auto.xml"; 
			$file_path = $tempPath."/".$file_name; 
			log_message('debug', '============ File going to save into this path :'.$file_path);
			$fileCopyResult = $xml->asXml($tempPath."/".$file_name);
			log_message('debug', '============ File copy result : '.print_r($fileCopyResult, true));
			
		} else {
			log_message('error', '============ Failed to loaded data into XML variable from external agent link.');
			$sts_xml['Status'] = 'Invalid XML URL/File';
			$this->Import_xml_listings_model->save_xml_import_status($sts_xml,$xml_status_id);
			unset($sts_xml);
			return;
		}
		 if($arrXMLdef['user_id'] == 15){
				$email = 'info@ancita.com';
				$path = site_url()."temp/import/".$file_name; 
				$data = array(
				'content' 		=> $path,
			   );
				$this->load->model('Notifications_model');
				$this->Notifications_model->send_notification($email, 'auto_import', $data, '', $this->pg_language->current_lang_id, $path);
				log_message('debug', '============ Send the casas auto import xml link to admin');
		}
		unset($xml->alphashare);
		unset($xml->agent);
	}
	 public function process_cron_settings(){
		log_message('debug', '============ File processing from our server temp path function is started');
		$strSQL = "SELECT * FROM ".CRON_XML_TABLE." WHERE frequency='daily'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		for($row = 0; $row < count($tempResult); $row++){
			$url = site_url()."temp/import/".$tempResult[$row]['user_id']."_".$tempResult[$row]['id']."_".date("d_m_Y")."_import_listing_xml_auto.xml";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents
            $data = curl_exec($ch); // execute curl request
            curl_close($ch);
		    $xml = simplexml_load_string($data);
			
            $strSQL = "SELECT * FROM ".CONVERSION_XPATH_TABLE." WHERE xml_code='".$tempResult[$row]['import_type']."'";
		    $rules = $this->Import_xml_listings_model->get_data_custom($strSQL);
		    $rules = $rules[0];
		    $list_cnt = 0;
	     	foreach($xml->xpath('property') as $key => $val) {
			$country = null;
			$region = null;		
			$city = null;
			$zip = null;
			$address = null;
			$price = null;
			$sale_type = null;
			$property_type = null;		
			$beds = null;
			$baths = null;
			$year = null;
			$garages = null;
			$plot_size = null;
			$square = null;
			$total_floors = null;
			$floor_number = null;
			$distance_to_subway = null;
			$headline_en = null;
			$comments_en = null;
			$headline_se = null;
			$comments_se = null;
			$headline_no = null;
			$comments_no = null;
			$agent_property_id = null;
			
			eval($rules['active_rule']);
			
			$country = isset($country[0]) ? $val->xpath('country') : array(0 => 'Spain');
			$photoCount = count($photos) - 1;
			$photoUrls = "";
			foreach($photos as $key => $photo) {
				$photoUrls .= $photo;
				if($key<$photoCount) $photoUrls .= "|";
			}	
			
			$ancita_country = (string) $country[0];
			$ancita_region = (string) $region[0];
			$ancita_city = (string) $city[0];
			$ancita_zip = (string) $zip[0];
			$ancita_address = (string) $address[0];
			$ancita_price = (string) $price[0];
			$ancita_type = (string) $sale_type[0];
			if((string) $property_type[0] != '')
			{
				$ancita_property = (string) $property_type[0];
			}else{
				$ancita_property = 'Villa';
			}
			$ancita_beds = (string) $beds[0];
			if($ancita_beds >=10){
				$ancita_beds = '10';
			}
			$ancita_baths = (string) $baths[0];
			if($ancita_baths >=10){
				$ancita_baths = '10';
			}
			$ancita_year = (string) $year[0];
			$ancita_garage = (string) $garages[0];
			if($ancita_garage >=5){
				$ancita_garage = '6';
			}
			$ancita_max_square = (string) $plot_size[0];
			$ancita_total_square = (string) $square[0];
			$ancita_floor = (string) $total_floors[0];
			$ancita_floor_num = (string) $floor_number[0];
			$ancita_subway_min = (string) $distance_to_subway[0];
			$ancita_headline_en = (string) $headline_en[0];
			$ancita_comment_en = (string) $comments_en[0];
			$ancita_headline_se = (string) $headline_se[0];
			$ancita_comment_se = (string) $comments_se[0];
			$ancita_headline_no = (string) $headline_no[0];
			$ancita_comment_no = (string) $comments_no[0];
			$ancita_agent_property_id = (string) $agent_property_id[0];
			$ref = (string) $ref[0];
			
			$strSQL1 = " INSERT INTO " . LISTING_TEMP_TABLE . " (id_user, id_agent_property, xml_import_id, country, region, city, zip, address, price, sale_type, property_type, beds, baths, year, garages, max_square, square, floor_number, total_floors, distance_to_subway, headline_en, headline_se, headline_no, comments_en, comments_se, comments_no, photos, ref, date_created) VALUES (" . $tempResult[$row]['user_id'] . ", " . $ancita_agent_property_id . ", " . $tempResult[$row]['id'] . ", '" . $ancita_country . "', '" . $ancita_region . "', '" . $ancita_city . "', '" . $ancita_zip . "', '" . $ancita_address . "', '" . $ancita_price . "', '" . $ancita_type . "', '" . $ancita_property . "', '" . $ancita_beds . "', '" . $ancita_baths . "', '" . $ancita_year . "', '" . $ancita_garage . "', '" . $ancita_max_square . "', '" . $ancita_total_square . "', '" . $ancita_floor_num . "', '" . $ancita_floor . "', '" . $ancita_subway_min . "', '" . addslashes($ancita_headline_en) . "', '" . addslashes($ancita_headline_se) . "', '" . addslashes($ancita_headline_no) . "', '" . addslashes($ancita_comment_en) . "', '" . addslashes($ancita_comment_se) . "', '" . addslashes($ancita_comment_no) . "', '" . $photoUrls . "', '" . $ref . "','".date('Y-m-d H:i:s')."')";
			
			$this->Import_xml_listings_model->save_temp_listing($strSQL1);
					
			$list_cnt++;
		}
		$sts_xml['import_listings'] = $list_cnt;
		$sts_xml['end_datetime'] = date('Y-m-d H:i:s');
		$sts_xml['Status'] = 'Listing Moved to XML file to Temp table';
		$this->Import_xml_listings_model->save_xml_import_status($sts_xml,$xml_status_id);
		unset($sts_xml);
	 }
	 }
	public function process_temp_listing() {
		log_message('debug', '============ File processing from temp table function is starting');
		error_reporting(0);
		set_time_limit(0);
		$strSQL = "SELECT * FROM ".CRON_XML_TABLE;
		$cron_xml_data = $this->Import_xml_listings_model->get_data_custom($strSQL);
		log_message('debug', '============ CRON_XML_TABLE Data: '.print_r($cron_xml_data, true));
		
		foreach($cron_xml_data as $key_cron=>$item_cron){
			
			$api = $item_cron['import_type'];
			$overwrite_type = $item_cron["overwrite_type"];
			$check1 = $item_cron["copy_english_text"];
			$check2 = $item_cron["add_property_id"];
			
			//TODO reduce the sql execution query
			$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='city'";
			$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
			for($t = 0; $t < count($tempResult); $t++)
			{
				$ary_city1[] = '@'.$tempResult[$t]['xml_value'].'$@';
				$ary_city2[] = $tempResult[$t]['ancita_value'];
			}
			
			$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='country'";
			$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
			for($t = 0; $t < count($tempResult); $t++)
			{
				$ary_country1[] = '@'.$tempResult[$t]['xml_value'].'$@';
				$ary_country2[] = $tempResult[$t]['ancita_value'];
			}
			
			$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='region'";
			$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
			for($t = 0; $t < count($tempResult); $t++)
			{
				$ary_region1[] = str_replace(')','\)',str_replace('(','\(','@'.$tempResult[$t]['xml_value'].'$@'));
				$ary_region2[] = $tempResult[$t]['ancita_value'];
			}
			
			$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='property_type'";
			$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
			for($t = 0; $t < count($tempResult); $t++)
			{
				$ary_property1[] = str_replace(')','\)',str_replace('(','\(','@'.$tempResult[$t]['xml_value'].'$@'));
				$ary_property2[] = $tempResult[$t]['ancita_value'];
			}
			
			$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='sale_type'";
			$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
			for($t = 0; $t < count($tempResult); $t++)
			{
				$ary_type1[] = str_replace(')','\)',str_replace('(','\(','@'.$tempResult[$t]['xml_value'].'$@'));
				$ary_type2[] = $tempResult[$t]['ancita_value'];
			}
			
			$temp_listings = $this->Import_xml_listings_model->get_temp_listings_by_xml_import_id($item_cron['id']);
			
			$tempListingCount = count($temp_listings);
			log_message('debug', '============ temp listing count : '.$tempListingCount);
			if($tempListingCount > 0) {
				log_message('debug', '============ temp listing count is greaterthan 0, so entring the process status to cron status table.');
				$sts_xml_arr['xml_import_id'] = $item_cron['id'];
				$sts_xml_arr['start_datetime'] = date('Y-m-d H:i:s');
				$sts_xml_arr['process_type'] = 'Process temp listing table';
				$xml_status_id_temp = $this->Import_xml_listings_model->save_xml_import_status($sts_xml_arr);
				unset($sts_xml_arr);
			}
			else{
				log_message('debug', '============ temp listing count not greaterthan 0. so futher there is no process to run');
			}
			$arr_temp_result = array();
			$arr_temp_result['exists']=null;
			$arr_temp_result['update']=null;
			$arr_temp_result['insert']=null;
			foreach($temp_listings as $key=>$item)
			{
				log_message('debug', '============ Entring for loop key : '.$key);
				
				$temp_list_cond['id_user'] = $item['id_user'];
				$temp_list_cond['id_agent_property'] = $item['id_agent_property'];
				$temp_list_cond['xml_import_id'] = $item['xml_import_id'];
				$temp_list_cond['import'] = 'auto';
				
				$import["country"] = '';
				if(isset($ary_country1) && isset($ary_country2)) { $import["country"] = preg_replace($ary_country1, $ary_country2, $item['country']); } 
				else { $import["country"] = $item['country']; };
				$import["region"] = '';
				if(isset($ary_region1) && isset($ary_region2)) { $import["region"] = preg_replace($ary_region1, $ary_region2, $item['region']); } 
				else { $import["region"] = $item['region']; };	
				$import["city"] = '';	
				if(isset($ary_city1) && isset($ary_city2)) { $import["city"] = preg_replace($ary_city1, $ary_city2, $item['city']); } 
				else { $import["city"] = $item['city']; };
				$import["zip"] = $item['zip'];
				$import["address"] = $item['address'];
				$import["price"] = $item['price'];
				$import["sale_type"] = '';
				if(isset($ary_type1) && isset($ary_type2)) { $import["sale_type"] = preg_replace($ary_type1, $ary_type2, $item['sale_type']); } else { $import["sale_type"] = $item['sale_type']; };	
				$import["property_type"] = '';	
				if(isset($ary_property1) && isset($ary_property2)) {
				$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='property_type' AND xml_value='".$item['property_type']."'";
	         	$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
				$import["property_type"] = $tempResult[0]['ancita_value'];
				 } else { 
				$import["property_type"] = $item['property_type']; };
				if($import["property_type"] == ''){
					$import["property_type"] = 'Villa';
				}
				$tempCategory = $this->get_category_type_id($import["property_type"]);
				if(intval($tempCategory) <= 0) {
					log_message('error', 'WARNING ============ IMPORT PROPERTY TYPE NOT FOUND : '.$import["property_type"].' | AGENT PROPERTY ID : '.$item['id_agent_property']);
					$arru['status'] = 'Invalid Category';
					$this->Import_xml_listings_model->update_temp_listings($temp_list_cond,$arru);
					continue;
				}
				
				$import["category_type"] = $tempCategory;
				$import["beds"] = $item['beds'];
				$import["baths"] = $item['baths'];
				$import["year"] = $item['year'];
				$import["garages"] = $item['garages'];
				$import["max_square"] = $item['max_square'];
				$import["square"] = $item['square'];
				$import["total_floors"] = $item['total_floors'];
				$import["floor_number"] = $item['floor_number'];
				$import["distance_to_subway"] = $item['distance_to_subway'];
				
				$refval = "";
				if($check2 == 'true') $refval = "Ref: " . $item['ref'] . " - ";
				$import["headline_en"] = $refval . substr(stripslashes($item['headline_en']), 0, 150) . "...";
				
				 if($check1 == 'true') { 
				 if($item['headline_se'] != ''){
				  $import["headline_se"] = $refval . substr(stripslashes($item['headline_se']), 0, 150) . "..."; 
				  }else{
				  $import["headline_se"] = $refval . substr(stripslashes($item['headline_en']), 0, 150) . "..."; 
				  }
				  } else { 
				  $import["headline_se"] = $refval . substr(stripslashes($item['headline_se']), 0, 150) . "..."; 
				 };
				
				if($check1 == 'true') { 
				if($item['headline_no'] != ''){
				$import["headline_no"] = $refval . substr(stripslashes($item['headline_no']), 0, 150) . "...";
				}else{
				$import["headline_no"] = $refval . substr(stripslashes($item['headline_en']), 0, 150) . "..."; 
				}
				} else { 
				$import["headline_no"] = $refval . substr(stripslashes($item['headline_no']), 0, 150) . "...";
				 };
				
				$import["comments_en"] = trim(stripslashes($item['comments_en']));
				$import["comments_se"] = trim(stripslashes($item['comments_se']));
				$import["comments_no"] = trim(stripslashes($item['comments_no']));
				
				$photos_cnt = explode("|", $item['photos']);
				$import["photos"] = count($photos_cnt);
				$import["photoUrls"] = $item['photos'];
				$import["id_agent_property"] = $item['id_agent_property'];
				
				$existing_data = $this->GetAdsWithUserPropertyId($item['id_user'], $item['id_agent_property']);
				if(isset($existing_data)) {
					log_message('debug', '============ EXISTING DATA found. | Overwrite Type : '.$overwrite_type);
					if($overwrite_type == '1001') {
						//delete temp listing
						$this->Import_xml_listings_model->delete_temp_listings($temp_list_cond);
						$arr_temp_result['exists'][] = $existing_data['id'];
						$this->Import_xml_listings_model->_update_temp_cron_status($arr_temp_result,$xml_status_id_temp);
						continue;
					} else if($overwrite_type == '1000' || $overwrite_type == '1002') {
						$temp_id = $this->AddListingFromFile($import,$item['id_user'],$temp_list_cond,$existing_data['id']);
						$arr_temp_result['update'][] = $temp_id;
						$this->Import_xml_listings_model->_update_temp_cron_status($arr_temp_result,$xml_status_id_temp);
					} else {
						$temp_id = $this->AddListingFromFile($import,$item['id_user'],$temp_list_cond);
						$arr_temp_result['insert'][] = $temp_id;
						$this->Import_xml_listings_model->_update_temp_cron_status($arr_temp_result,$xml_status_id_temp);
					}
				} else {
					log_message('debug', '============ NEW DATA');
					$temp_id = $this->AddListingFromFile($import,$item['id_user'],$temp_list_cond);
					$arr_temp_result['insert'][] = $temp_id;
					$this->Import_xml_listings_model->_update_temp_cron_status($arr_temp_result,$xml_status_id_temp);
				}
			//end temp listing
			}
			
			if(count($temp_listings) > 0) {
				$sts_xml_arr['end_datetime'] = date('Y-m-d H:i:s');
				$sts_xml_arr['Status'] = 'Completed';
				log_message('debug', '============ Completed temp listing data processed.');
				$this->Import_xml_listings_model->save_xml_import_status($sts_xml_arr,$xml_status_id_temp);
				unset($sts_xml_arr);
			}
		// end cron data	
		}
	}
	
	//for manual listings process from manual temp table
	public function process_temp_manual_listing(){
		log_message('debug', '============ File processing from manual temp table function is starting');
		error_reporting(0);
		set_time_limit(0);
		
		$strSQL = "SELECT * FROM ".LISTING_TEMP_TABLE_MANUAL;
		$cron_xml_data = $this->Import_xml_listings_model->get_data_custom($strSQL);
		foreach($cron_xml_data as $key_cron=>$item_cron){
			$id = $item_cron['id'];
			$user_id = $item_cron['user_id'];
			$api = $item_cron['import_type'];
			$overwrite_type = $item_cron["overwrite_type"];
			$check1 = $item_cron["copy_english_text"];
			$check2 = $item_cron["add_property_id"];
			//TODO write single query
			$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='city'";
			$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
			for($t = 0; $t < count($tempResult); $t++)
			{
				$ary_city1[] = '@'.$tempResult[$t]['xml_value'].'$@';
				$ary_city2[] = $tempResult[$t]['ancita_value'];
			}
			
			$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='country'";
			$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
			for($t = 0; $t < count($tempResult); $t++)
			{
				$ary_country1[] = '@'.$tempResult[$t]['xml_value'].'$@';
				$ary_country2[] = $tempResult[$t]['ancita_value'];
			}
			
			$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='region'";
			$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
			for($t = 0; $t < count($tempResult); $t++)
			{
				$ary_region1[] = str_replace(')','\)',str_replace('(','\(','@'.$tempResult[$t]['xml_value'].'$@'));
				$ary_region2[] = $tempResult[$t]['ancita_value'];
			}
			
			$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='property_type'";
			$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
			for($t = 0; $t < count($tempResult); $t++)
			{
				$ary_property1[] = str_replace(')','\)',str_replace('(','\(','@'.$tempResult[$t]['xml_value'].'$@'));
				$ary_property2[] = $tempResult[$t]['ancita_value'];
			}
			
			$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='sale_type'";
			$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
			for($t = 0; $t < count($tempResult); $t++)
			{
				$ary_type1[] = str_replace(')','\)',str_replace('(','\(','@'.$tempResult[$t]['xml_value'].'$@'));
				$ary_type2[] = $tempResult[$t]['ancita_value'];
			}
			
			$temp_listings = $this->Import_xml_listings_model->get_temp_listings_manual_by_xml_import_id($item_cron['id']);
			
			$tempListiongCount = count($temp_listings);
			log_message('debug', '============ Temp listing count : '.$tempListiongCount);
			if( $tempListiongCount > 0) {
				$sts_xml_arr['xml_import_id'] = $item_cron['id'];
				$sts_xml_arr['start_datetime'] = date('Y-m-d H:i:s');
				$sts_xml_arr['process_type'] = 'Process temp manual listing table';
				$xml_status_id_temp = $this->Import_xml_listings_model->save_xml_import_status($sts_xml_arr);
				unset($sts_xml_arr);
			}
			
			$arr_temp_result = array();
			
			foreach($temp_listings as $key=>$item){
				
				$temp_list_cond['id_user'] = $item['id_user'];
				$temp_list_cond['id_agent_property'] = $item['id_agent_property'];
				$temp_list_cond['xml_import_id'] = $item['xml_import_id'];
				$temp_list_cond['import'] = $item['import'];
				$import["country"] = '';
				if(isset($ary_country1) && isset($ary_country2)) { $import["country"] = preg_replace($ary_country1, $ary_country2, $item['country']); } else { $import["country"] = $item['country']; };
				$import["region"] = '';
				if(isset($ary_region1) && isset($ary_region2)) { $import["region"] = preg_replace($ary_region1, $ary_region2, $item['region']); } else { $import["region"] = $item['region']; };	
				$import["city"] = '';	
			if(isset($ary_city1) && isset($ary_city2)) { $import["city"] = preg_replace($ary_city1, $ary_city2, $item['city']); } else { $import["city"] = $item['city']; };
				$import["zip"] = $item['zip'];
				$import["address"] = $item['address'];
				$import["price"] = $item['price'];
				$import["sale_type"] = '';
				if(isset($ary_type1) && isset($ary_type2)) { $import["sale_type"] = preg_replace($ary_type1, $ary_type2, $item['sale_type']); } else { $import["sale_type"] = $item['sale_type']; };	
				$import["property_type"] = '';
				if(isset($ary_property1) && isset($ary_property2)){ 
				$import["property_type"] = $item['property_type']; 
				} else { $import["property_type"] = $item['property_type']; };
				$tempCategory = $this->get_category_type_id($import["property_type"]);
				if(intval($tempCategory) <= 0) {
					$arru['status'] = 'Invalid Category for manual import';
					$this->Import_xml_listings_model->update_temp_listings($temp_list_cond,$arru);
					continue;
				}
				
				$import["category_type"] = $tempCategory;
				$import["beds"] = $item['beds'];
				$import["baths"] = $item['baths'];
				$import["year"] = $item['year'];
				$import["garages"] = $item['garages'];
				$import["max_square"] = $item['max_square'];
				$import["square"] = $item['square'];
				$import["total_floors"] = $item['total_floors'];
				$import["floor_number"] = $item['floor_number'];
				$import["distance_to_subway"] = $item['distance_to_subway'];
				
				$refval = "";
				//update the headline values(empty checking)
				if($check2 == 'true') $refval =  $item['ref'];
				$import["headline_en"] = $refval . substr(stripslashes($item['headline_en']), 0, 150) . "...";
				
				 if($check1 == 'true') { 
				 if($item['headline_se'] != ''){
				  $import["headline_se"] = $refval . substr(stripslashes($item['headline_se']), 0, 150) . "..."; 
				  }else{
				  $import["headline_se"] = $refval . substr(stripslashes($item['headline_en']), 0, 150) . "..."; 
				  }
				  } else { 
				  $import["headline_se"] = $refval . substr(stripslashes($item['headline_se']), 0, 150) . "..."; 
				 };
				
				if($check1 == 'true') { 
				if($item['headline_no'] != ''){
				$import["headline_no"] = $refval . substr(stripslashes($item['headline_no']), 0, 150) . "...";
				}else{
				$import["headline_no"] = $refval . substr(stripslashes($item['headline_en']), 0, 150) . "..."; 
				}
				} else { 
				$import["headline_no"] = $refval . substr(stripslashes($item['headline_no']), 0, 150) . "...";
				 };
				//update the comment values(empty checking)
				$import["comments_en"] = trim(stripslashes($item['comments_en']));
				$import["comments_se"] = trim(stripslashes($item['comments_se']));
				$import["comments_no"] = trim(stripslashes($item['comments_no']));
				
				$photos_cnt = explode("|", $item['photos']);
				$import["photos"] = count($photos_cnt);
				$import["photoUrls"] = $item['photos'];
				$import["id_agent_property"] = $item['id_agent_property'];
				
				$existing_data = $this->GetAdsWithUserPropertyId($item['id_user'], $item['id_agent_property']);
				if(isset($existing_data)) {
					if($overwrite_type == '1001') {
						//delete temp listing
						$this->Import_xml_listings_model->delete_temp_listings($temp_list_cond);
						$arr_temp_result['exists'][] = $existing_data['id'];
						$this->Import_xml_listings_model->_update_temp_cron_status($arr_temp_result,$xml_status_id_temp);
						continue;
					} else if($overwrite_type == '1000' || $overwrite_type == '1002') {
						$temp_id = $this->AddListingFromFile($import,$item['id_user'],$temp_list_cond,$existing_data['id']);
						$arr_temp_result['update'][] = $temp_id;
						$this->Import_xml_listings_model->_update_temp_cron_status($arr_temp_result,$xml_status_id_temp);
					} else {
						$temp_id = $this->AddListingFromFile($import,$item['id_user'],$temp_list_cond);
						$arr_temp_result['insert'][] = $temp_id;
						$this->Import_xml_listings_model->_update_temp_cron_status($arr_temp_result,$xml_status_id_temp);
					}
				} else {
					$temp_id = $this->AddListingFromFile($import,$item['id_user'],$temp_list_cond);
					$arr_temp_result['insert'][] = $temp_id;
					$this->Import_xml_listings_model->_update_temp_cron_status($arr_temp_result,$xml_status_id_temp);
				}
			//end temp listing
			}
			
			if(count($temp_listings) > 0) {
				$sts_xml_arr['end_datetime'] = date('Y-m-d H:i:s');
				$sts_xml_arr['Status'] = 'Completed manual import';
				$this->Import_xml_listings_model->save_xml_import_status($sts_xml_arr,$xml_status_id_temp);
				unset($sts_xml_arr);
			}
		    //delete_listings_manual_setting_table
			$manualdata = $this->Import_xml_listings_model->check_manual_temp_table_empty($user_id);
			if(count($manualdata) == 0) {
				$this->Import_xml_listings_model->delete_listings_manual_setting_table($cron_xml_data[0]);
			}
		}
		log_message('debug', '============ Process temp manual listing table is ended');
	}
	
	function AddListingFromFile($fields, $nuser, $temp_list_cond, $listing_id = null){
	    log_message('debug', '============ Listings move to listings table function is started');
		error_reporting(0);
		set_time_limit(0);
		
		$country_id = 'ES';
		$region_id = 0;
		$city_id = 0;
		$sale_type_id = $this->Import_xml_listings_model->get_operation_type($fields["sale_type"]);
		$strSQL = "SELECT code FROM ".COUNTRY_TABLE." WHERE name LIKE '".addslashes($fields["country"])."'";
		$result = $this->Import_xml_listings_model->get_data_custom($strSQL);
		if($result){
			$country_id = $result[0]['code'];
		}
		
		$strSQL = "SELECT id FROM ".REGION_TABLE." WHERE name LIKE '".addslashes($fields["region"])."'";
		$result = $this->Import_xml_listings_model->get_data_custom($strSQL);
		if($result){
			$region_id = $result[0]['id'];
		}
		
		$strSQL = "SELECT id FROM ".CITY_TABLE." WHERE name LIKE '".addslashes($fields["city"])."'";
		$result = $this->Import_xml_listings_model->get_data_custom($strSQL);
		if($result){
			$city_id = $result[0]['id'];
		}
		
		$post_data['id_country'] = $country_id;
		$post_data['id_region'] = $region_id;
		$post_data['id_city'] = $city_id;
		$post_data['id_user'] = $nuser;
		$post_data['id_type'] = $sale_type_id;
		
		$post_data['id_category'] = $fields['category_type'];
		$strType = "property_type_".$fields['category_type'];
		
		if($fields['category_type'] == "1")
		{
			if($fields["sale_type"] == "sale")
				$editor_id = 1;
			elseif($fields["sale_type"] == "rent")
				$editor_id = 4;
			
			if($editor_id){		
				$post_data['fe_bd_rooms_'.$editor_id] = $fields['beds'];
				$post_data['fe_bth_rooms_'.$editor_id] = $fields['baths'];
				$post_data['fe_garages_'.$editor_id] = $fields['garages'];
				$post_data['fe_year_'.$editor_id] = $fields['year'];
				$post_data['fe_floor_number_'.$editor_id] = $fields['floor_number'];
				$post_data['fe_total_floors_'.$editor_id] = $fields['total_floors'];
				$post_data['fe_distance_to_subway_'.$editor_id] = $fields['distance_to_subway'];
				$post_data['fe_live_square_'.$editor_id] =  $fields['max_square'];
			}
		}
		
		if($fields['category_type'] == "2")
		{
			if($fields["sale_type"] == "sale")
				$editor_id = 2;
			elseif($fields["sale_type"] == "rent")
				$editor_id = 5;
		}
		
		if($fields['category_type'] == "3")
		{
			if($fields["sale_type"] == "sale")
				$editor_id = 3;
			elseif($fields["sale_type"] == "rent")
				$editor_id = 6;
		}
		
		$arrProperty = $this->get_property_type_id($strType);
		$pId = array_search($fields["property_type"], $arrProperty);
		if(!$pId)
		{
			$pId = array_search("Other", $arrProperty);
			if(!$pId) {$pId = 0;}
		}
		
		$post_data['property_type'] = $pId;
		$post_data['date_open'] = '0000-00-00 00:00:00';
		$post_data['status'] = 0;
		$post_data['date_open_begin'] = '0000-00-00 00:00:00';
		$post_data['date_open_end'] = '0000-00-00 00:00:00';
		$post_data['date_available'] = '0000-00-00 00:00:00';
		$post_data['sold'] = 0;
		$post_data['date_activity'] = '0000-00-00 00:00:00';
		$post_data['date_expire'] = '0000-00-00 00:00:00';
		$post_data['price_negotiated'] = 0;
		
		$cur = $this->Import_xml_listings_model->get_default_currency_symbol();
		$cur_gid = $cur["gid"];
		$post_data['gid_currency'] = $cur_gid;
		$post_data['price'] = floatval($fields['price']);
		$post_data['price_period'] = 1;
		$post_data['address'] = $fields['address'];
		$post_data['zip'] = $fields['zip'];
		$post_data['id_district'] = 0;
		$post_data['lat'] = floatval(0.0000);
		$post_data['lon'] = floatval(0.0000);
		
		$validate_data = $this->Listings_model->validate_listing($listing_id, $post_data);
		$this->load->model("listings/models/listings_model");
		
		$this->load->model("listings/models/listings_model");
		$validate_data = $this->Listings_model->validate_listing($listing_id, $post_data);
		$post_data = $validate_data['data'];
		$post_data['photo_count'] = $fields['photos'];
		$post_data['id_agent_property'] = $fields['id_agent_property'];
		$post_data['square'] =  $fields['square'];
		$post_data['square_unit'] =  '2';
		
		$len = $this->getLangId('en');
		$lse = $this->getLangId('se');
		$lno = $this->getLangId('no');
		$post_data['headline_lang'][$len] = $fields['headline_en'];
		$post_data['headline_lang'][$lse] = $fields['headline_se'];
		$post_data['headline_lang'][$lno] = $fields['headline_no'];
		
		$post_data['comments_lang'][$len] = $fields['comments_en'];
		$post_data['comments_lang'][$lse] = $fields['comments_se'];
		$post_data['comments_lang'][$lno] = $fields['comments_no'];
		

		$list_id = $this->Import_xml_listings_model->save_custom_listings($listing_id,$post_data);
        if($listing_id == ""){
		$tempPath = SITE_PATH.SITE_SUBFOLDER.'temp/import';
		$photos = explode("|", $fields["photoUrls"]);
		
		foreach ($photos AS $key=>$photo_name){  
		$strSQL3 = " REPLACE INTO " . LISTING_TEMP_IMAGE_TABLE . " (id_listing, photo_url, date_created) VALUES (" . $list_id . ", '" . $photo_name . "','".date('Y-m-d H:i:s')."')";
			$this->Import_xml_listings_model->save_temp_listing($strSQL3);
		}
		}		
		$this->Import_xml_listings_model->delete_temp_listings($temp_list_cond);
		 log_message('debug', '============ Listings move to listings table is ended');
		return $list_id;
	}
	
	public function process_temp_listing_image() {
		 log_message('debug', '============ Process temp listing image from temp image table');
		$temp_listings_image = $this->Import_xml_listings_model->get_temp_listings_image();

		if(count($temp_listings_image) > 0){
			
			$sts_xml_arr['xml_import_id'] = 0;
			$sts_xml_arr['start_datetime'] = date('Y-m-d H:i:s');
			$sts_xml_arr['process_type'] = 'Process temp listing image';
			$xml_status_id_temp = $this->Import_xml_listings_model->save_xml_import_status($sts_xml_arr);
			unset($sts_xml_arr);
			
			$arr_temp_result = array();
			$tempPath = SITE_PATH.SITE_SUBFOLDER.'temp/import';
			
			foreach($temp_listings_image as $key=>$item){
				
				$list_id = $item['id_listing'];
				$photo_url_temp = $item['photo_url'];
				$data = getimagesize($photo_url_temp);
				$photowidth = $data[0];
				$width = $this->Import_xml_listings_model->get_width($id,$config_id);
				if($photowidth >= $width){
				if(getimagesize($photo_url_temp) !== false){	
				   $photo_name = $photo_url_temp;
				}
				}
				if (trim($photo_name) == "") {
					//delete temp photo
					$this->Import_xml_listings_model->delete_temp_listings_image($list_id,$photo_url_temp);
					continue;
				}
				if (strstr($photo_name, "http")){
					$source_photo_name = $photo_name;
					$photo_name = $this->AfterLastSlash($source_photo_name);                
					if (!copy($source_photo_name,$tempPath."/".$photo_name)){
						$arr_img_data['status'] = 'Image Copy Error';
						$this->Import_xml_listings_model->update_temp_listing_image($arr_img_data,$list_id,$photo_url_temp);
						unset($arr_img_data);
						
						break;    
					}
				}
				
				$upload["tmp_name"] = $tempPath."/".$photo_name;
				$upload["name"] = $photo_name;
				$file_info = getimagesize($upload["tmp_name"]);
				$upload["type"] = $file_info["mime"];
				
				$this->Import_xml_listings_model->save_photo_file($list_id,$upload);
				
				$arr_temp_result['update'][] = $list_id;
				$this->Import_xml_listings_model->_update_temp_cron_status($arr_temp_result,$xml_status_id_temp);
	
				//delete temp photo
				$this->Import_xml_listings_model->delete_temp_listings_image($list_id,$photo_url_temp);
			}
			
			$sts_xml_arr['end_datetime'] = date('Y-m-d H:i:s');
			$sts_xml_arr['Status'] = 'Completed Image';
			$this->Import_xml_listings_model->save_xml_import_status($sts_xml_arr,$xml_status_id_temp);
			$this->Import_xml_listings_model->update_latlon();
	        log_message('debug', '============ Process temp listing image from temp image table is ended');
			unset($sts_xml_arr);
		}
	}
	
	function GetAdsWithUserPropertyId($nuser, $id_agent_property = 0){
		$strSQL = "SELECT id, id_user, id_agent_property FROM ".LISTINGS_TABLE." WHERE id_user = '".$nuser."' AND id_agent_property = '".$id_agent_property."'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		return $tempResult[0];
	}
	
	public function get_property_type_id($propertytype)
	{
		$lang_id = $this->pg_language->current_lang_id;
		$this->pg_language->ds->return_module($this->Properties_model->module_gid, $lang_id);
		$ds = $this->pg_language->ds->lang[$lang_id][$this->Properties_model->module_gid];
		
		$category_gids = $this->Properties_model->categories;
		$property_gids = $this->Properties_model->properties;
		
		foreach($category_gids as $gid){
			$categories[$gid] = $ds[$gid];
		}
		
		foreach($property_gids as $gid){
			$properties[$gid] = $ds[$gid];
			$properties[$gid]['status'] = $this->pg_module->get_module_config('listings', $gid.'_enabled');
		}
		return $properties[$propertytype]['option'];
		
	}
	
	public function get_category_type_id($propertytype)
	{
		log_message('debug', '============ Category type is checked and updated');
		$lang_id = $this->pg_language->current_lang_id;
		$this->pg_language->ds->return_module($this->Properties_model->module_gid, $lang_id);
		$ds = $this->pg_language->ds->lang[$lang_id][$this->Properties_model->module_gid];
		
		$category_gids = $this->Properties_model->categories;
		$property_gids = $this->Properties_model->properties;
		
		foreach($category_gids as $gid){
			$categories[$gid] = $ds[$gid];
		}
		$properties = array();
		foreach($property_gids as $gid){
			if(isset($ds[$gid])):
				$properties[$gid] = $ds[$gid];
			else:
				log_message('debug', "NOTE ============ ds GID value empty for gid : ".$gid);
			endif;
			$properties[$gid]['status'] = $this->pg_module->get_module_config('listings', $gid.'_enabled');
		}
		$arrCat = $categories['property_types']['option'];
		$catId = 0;
		foreach ($arrCat as $key => $value) {
			$strCatId = $key;
			$arrProp = $properties['property_type_'.$strCatId];
			$result = array_search($propertytype,$arrProp['option']);
			if($result){
				$catId = $strCatId;
			}
		}
		log_message('debug', '============ Category type is checked and updated is ended');
		return $catId;
	}
	
	function AfterLastSlash($str){
		$arr = explode("/", $str);
		return $arr[count($arr)-1];
	}
	
	function GetVarString($name, $default = "", $lenght=3000, $slashes=1, $tags=1) {

		if ( !(isset($_REQUEST[$name]) && !empty($_REQUEST[$name])) )
		return $default;
	
		$value = $_REQUEST[$name];
		$value = substr($value, 0, $lenght);
		$value = trim($value);
	
		if ($tags === 1)
		$value = strip_tags($value);
	
		if ($slashes === 1)
		$value = addslashes($value);
	
		if ($value == "") return $default;
	
		return $value;
	}
	
	function getLangId($langCode){
		$languages = $this->pg_language->languages;
		$this->template_lite->assign('languages', $languages);
		$langId = '0';
		foreach($languages AS $key=>$value){
			foreach($value AS $key2=>$value2){
				if($key2 == 'code' && $value2 == $langCode){
					$langId = $value['id'];
					break;
				}
			}
		}
		return $langId;
	}
	
	function IsMyAdsXml($new_ads, $all_ads){    

		$i = 0; 
		foreach ($all_ads AS $key=>$ads){ 
	
			$i = 0;        
			$flag = 1;        
			foreach ($new_ads AS $key2=>$item){
			
				if ($flag && $key2 == 'id_agent_property'){
					if ($ads[$key2] == $item){                
						$flag = 1;
					}else{
						$flag = 0;                    
					}
				}
	
			}
			if ($flag){            
				return 1;            
			}
		}    
		return 0;
	}
}
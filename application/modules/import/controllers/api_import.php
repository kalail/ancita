<?php

/**
 * Import api side controller
 * 
 * @package PG_RealEstate
 * @subpackage Import
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Api_import extends Controller{	
	/**
	 * Constructor
	 *
	 * @return Api_import
	 */
	public function __construct(){
		parent::Controller();
	}
	
	/**
	 * Render simple import action
	 * 
	 * @param integer $import_id data identifier
	 * @return void
	 */
	public function index($import_id){
		$this->load->model("import/models/Import_module_model");
		$this->load->model("Import_model");		
	}
}

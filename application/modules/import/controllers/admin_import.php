<?php

/**
 * Import admin side controller
 * 
 * @package PG_RealEstate
 * @subpackage Import
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Admin_Import extends Controller{
	/**
	 * Constructor
	 *
	 * @return Admin_Import
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model("import/models/Import_xml_listings_model");
		$this->load->model("Menu_model");
		$this->load->model('Properties_model');
		$this->Menu_model->set_menu_active_item("admin_menu", "exp-import-items");
		$this->system_messages->set_data("header", l("admin_header_import", "import"));
	}
	
	/**
	 * Selections mamagement
	 * 
	 * @return void
	 */
	public function index(){
		$this->data();
	}
	/**
	 * Render data list action
	 * 
	 * @param string $order sorting field
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 * @return void
	 */
	public function data($order="date_created", $order_direction="DESC", $page=1){
		$this->load->model("Import_model");
		$this->load->model("import/models/Import_driver_model");
		$this->load->model("import/models/Import_module_model");
		
		$filters = array();
		
		$current_settings = isset($_SESSION["import_list"]) ? $_SESSION["import_list"] : array();
				
		$current_settings["filters"] = $filters;
				
		if(!isset($current_settings["order"]))
			$current_settings["order"] = "date_add";
		if(!isset($current_settings["order_direction"]))
			$current_settings["order_direction"] = "DESC";
		if (!isset($current_settings["page"]))
			$current_settings["page"] = 1;		
		
		if (!$order) $order = $current_settings["order"];
		$this->template_lite->assign("order", $order);
		$current_settings["order"] = $order;

		if (!$order_direction) $order_direction = $current_settings["order_direction"];
		$this->template_lite->assign("order_direction", $order_direction);
		$current_settings["order_direction"] = $order_direction;

		$data_count = $this->Import_model->get_data_count($filters);

		if(!$page) $page = $current_settings["page"];
		$items_on_page = $this->pg_module->get_module_config("start", "admin_items_per_page");
			
		$this->load->helper("sort_order");
		$page = get_exists_page_number($page, $data_count, $items_on_page);
		$current_settings["page"] = $page;

		$_SESSION["import_list"] = $current_settings;

		$sort_links = array(
			"date_created" => site_url() . "admin/import/index/date_created/".(($order != "date_created" xor $order_direction == "DESC") ? "ASC" : "DESC"),
		);		
		$this->template_lite->assign("sort_links", $sort_links);

		if($data_count > 0){
			$data = $this->Import_model->get_data_list($filters, $page, $items_on_page, array($order => $order_direction));
			$this->template_lite->assign("data", $data);
		}
		
		$this->load->helper('navigation');
		
		$url = site_url()."admin/import/index/".$driver_gid."/".$module_id."/".$order."/".$order_direction."/";
		$page_data = get_admin_pages_data($url, $data_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $this->pg_date->get_format('date_time_literal', 'st');
		$this->template_lite->assign("page_data", $page_data);
		
		$this->template_lite->assign("driver_gid", $driver_gid);
		$this->template_lite->assign("module_id", $module_id);
		
		$this->Menu_model->set_menu_active_item("admin_import_menu", "import_data_item");
		$this->template_lite->view("list");
	}
	
	/**
	 * Upload data for importing
	 * 
	 * @return void
	 */
	public function upload(){
		$this->load->model("Import_model");
		$this->load->model("import/models/Import_driver_model");
	
		$data = array();
	
		if($this->input->post("btn_save")){
			
			$data = $this->input->post("data");
			$validate_data = $this->Import_model->validate_data(null, $data);			
			$validate_upload_data = $this->Import_model->validate_upload("import_file", $data);
			$validate_data["errors"] = array_merge($validate_data["errors"], $validate_upload_data["errors"]);
		
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
			}else{
				$data_id = $this->Import_model->save_data(null, $validate_data["data"]);				
				$upload_data = $this->Import_model->upload("import_file", $data_id);				
				if(!empty($upload_data["errors"])){
					$validate_data = $upload_data;
				}else{
					$validate_data = $this->Import_model->validate_data($data_id, $upload_data["data"]);
				}
			
				if(!empty($validate_data["errors"])){
					$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
					$this->Import_model->delete_file($data_id);
					$this->Import_model->delete_data($data_id);					
				}else{
					$this->Import_model->save_data($data_id, $validate_data["data"]);
					$this->system_messages->add_message("success", l("success_file_uploaded", "import"));
					$url = site_url()."admin/import/relations/".$data_id;
					redirect($url);
				}				
			}
		}
		
		$this->template_lite->assign('data', $data);
		
		$drivers = $this->Import_driver_model->get_drivers();
		foreach($drivers as $key=>$driver){
			if(!$driver["status"]) unset($drivers[$key]);
		}
		$this->template_lite->assign("drivers", $drivers);	
		
		$this->load->model("import/models/Import_module_model");
		$modules = $this->Import_module_model->get_modules();
		$this->template_lite->assign("modules", $modules);	
		
		if(!empty($drivers)){
			$current_driver = current($drivers);
			if(isset($data['gid_driver'])){
				foreach($drivers as $driver){
					if($driver['gid'] = $data['gid_driver']){
						$current_driver = $driver;
						break;
					}
				}
			}
			$model_name = "import_".$current_driver["gid"]."_model";
			$this->load->model("import/models/drivers/".$model_name, $model_name);		
			$this->template_lite->assign("driver_gid", $current_driver["gid"]);
			$this->template_lite->assign("settings", $this->{$model_name}->settings);	
			$settings =  $this->template_lite->fetch("upload_form_settings", "admin", "import");
			$this->template_lite->assign("settings", $settings);
		}				
		$this->template_lite->view("upload_form");
	}
	
	/**
	 * Render upload form by ajax
	 * 
	 * @return void
	 */
	public function ajax_upload_form(){
		$driver_gid = $this->input->post("driver_gid", true);
		$model_name = "import_".$driver_gid."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);		
		$this->template_lite->assign("driver_gid", $driver_gid);
		$this->template_lite->assign("settings", $this->{$model_name}->settings);
		echo $this->template_lite->fetch("upload_form_settings", "admin", "import");
		exit;
	}
	
	/**
	 * Edit data action
	 * 
	 * @param integer $data_id data identifier
	 * @return void
	 */
	public function edit($data_id){
		
		$this->load->model("import/models/Import_module_model");
		$this->load->model("Import_model");

		$data = $this->Import_model->get_data_by_id($data_id, true);
		foreach($data["relations"] as $index=>$relation){
			if(!$relation["link"]) unset($data["relations"][$index]);
		}
		$this->template_lite->assign("data", $data);

		$model_name = "Import_".$data["gid_driver"]."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);
		$this->template_lite->assign("two_step", $this->{$model_name}->two_step);
				
		/*if(!$this->{$model_name}->two_step){
			if($data["total"] > $data["processed"]) $this->parse($data_id);
			$data["processed"] = $data["total"];
		}*/
		
		$this->template_lite->assign("relations", count($data["relations"]));
		$this->template_lite->assign("processed", $data["total"]-$data["processed"]);
		$this->template_lite->assign("imported", !$this->{$model_name}->two_step ? $data["total"]-($data['imported'] + $data['failed']) : $data["processed"]);
		
		$this->system_messages->set_data("header", l("admin_header_data_edit", "import"));
		$this->template_lite->view("edit");
	}
	
	/**
	 * Set fields relations
	 * 
	 * @param integer $data_id
	 * @return void
	 */
	public function relations($data_id){
		$this->load->model("import/models/Import_module_model");
		$this->load->model("Import_model");
		
		$data = $this->Import_model->get_data_by_id($data_id, true);
		
		if($this->input->post("btn_save")){
			$module_fields = $this->Import_module_model->get_module_fields($data["id_object"]);
			$links = $this->input->post("link", true);
			foreach($data["relations"] as $index => $relation){
				if(isset($links[$index]) && $links[$index] != -1 && !empty($module_fields[$links[$index]]["name"])){
					$data["relations"][$index]["type"] = $module_fields[$links[$index]]["type"];
					$data["relations"][$index]["link"] = $module_fields[$links[$index]]["name"];
					$data["relations"][$index]["label"] = $module_fields[$links[$index]]["label"];
				}else{
					$data["relations"][$index]["type"] = "";
					$data["relations"][$index]["link"] = "";
					$data["relations"][$index]["label"] = "";
				}
			}
			$save_data["relations"] = $data["relations"];
			$validate_data = $this->Import_model->validate_data($data_id, $save_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
			}else{
				$this->Import_model->save_data($data_id, $validate_data["data"]);
				$this->system_messages->add_message("success", l("success_relation_updated", "import"));
				$url = site_url()."admin/import/edit/".$data_id;
				redirect($url);			
			}
		}
		
		$this->template_lite->assign("data", $data);
		
		$module_fields = $this->Import_module_model->get_module_fields($data["id_object"]);
		$this->template_lite->assign("module_fields", $module_fields);
		
		$this->system_messages->set_data("header", l("admin_header_relations", "import"));
		$this->template_lite->view("relations");
	}
	
	/**
	 * Remove import data action
	 * 
	 * @param array $ids data identifiers
	 * @return void
	 */
	public function delete($ids=null){
		if(!$ids) $ids = $this->input->post("ids");
		if(!empty($ids)){
			$this->load->model("Import_model");
			foreach((array)$ids as $id){
				$this->Import_model->delete_file($id, true);
				$this->Import_model->delete_data($id);
			}
			$this->system_messages->add_message("success", l("success_data_deleted", "import"));
		}
		$url = site_url()."admin/import";
		redirect($url);
	}
	
	/**
	 * Parse imported data action
	 * 
	 * @param integer $data_id data identifier
	 * @return void
	 */
	public function parse($data_id){
		$this->load->model("Import_model");
		
		$data = $this->Import_model->get_data_by_id($data_id, true);
		$model_name = "Import_".$data["gid_driver"]."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);
		
		try{
			$result = $this->Import_model->parse_data($data_id);
		}catch(Exception $e){
			$result['errors'][] = $e->getMessage();
		}
		
		if($this->{$model_name}->two_step){
			if($result){
				$this->system_messages->add_message("success", l("success_data_parsed", "import"));			
			}
			$url = site_url()."admin/import/edit/".$data_id;
			redirect($url);
		}
		
		return $result;
	}
	
	/**
	 * Parse imported data action by ajax
	 * 
	 * @param integer $data_id data identifier
	 * @return void
	 */
	public function ajax_parse($data_id){
		$response = array("errors"=>array(), "processed"=>0);
		
		$this->load->model("Import_model");
		
		$data = $this->Import_model->get_data_by_id($data_id, true);
		$model_name = "Import_".$data["gid_driver"]."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);
		
		try{
			$result = $this->Import_model->parse_data($data_id);
		}catch(Exception $e){
			$result["errors"][] = $e->getMessage();
		}
	
		
		if(!empty($result["errors"])){
			$response["errors"] = implode("<br>", $result["errors"]);
		}else{
			$response["processed"] = $results["data"]["processed"];
		}
		echo json_encode($response);
		exit;
	}
	
	/**
	 * Process imported data action
	 * 
	 * @param integer $data_id data identifier
	 * @return void
	 */
	public function make($data_id){
		$this->load->model("Import_model");
		
		$data = $this->Import_model->get_data_by_id($data_id, true);
		$model_name = "Import_".$data["gid_driver"]."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);		
		
		if(!$this->{$model_name}->two_step && $data["total"]-$data["processed"]){
			$results = $this->parse($data_id);
			if(!empty($results['errors'])){
				$this->system_messages->add_message("error", implode("<br>", $results["errors"]));
			}
		}
		
		$results = $this->Import_model->make_import_data($data_id);
		if(!empty($results["errors"])){
			$this->system_messages->add_message("error", implode("<br>", $results["errors"]));
		}else{
			if($results["data"]["failed"]){
				$this->system_messages->add_message("error",
					l("error_data_imported", "import").": ".$results["data"]["imported"]."<br>".
					l("error_data_failed", "import").": ".$results["data"]["failed"]
				);
			}else{
				$this->system_messages->add_message("success", l("success_data_imported", "import").": ".$results["data"]["imported"]);
			}
		}	
				
		$url = site_url()."admin/import/edit/".$data_id;	
		redirect($url);
	}
	
	/**
	 * Process imported data action by ajax
	 * 
	 * @param integer $import_id data identifier
	 * @return void
	 */
	public function ajax_make($import_id){
		$this->load->model("Import_model");
		
		$response = array("errors"=>"", "imported"=>0);
		
		$import_data = $this->Import_model->get_data_by_id($import_id, true);
		if(!$import_data){
			$response["error"] = l("error_empty_data", "import");
			echo json_encode($response);
			exit;
		}
		
		$model_name = "Import_".$import_data["gid_driver"]."_model";
		$this->load->model("import/models/drivers/".$model_name, $model_name);		
		
		if(!$this->{$model_name}->two_step && $import_data["total"] > ($import_data["processed"] + $import_data["imported"] + $import_data["failed"])){
			$results = $this->parse($import_id);
			if(!empty($results['errors'])){
				$response["error"] = implode('<br>', $results['errors']);
				echo json_encode($response);
				exit;
			}
			$import_data["processed"] = $results["data"]["processed"];
		}
		
		$results = $this->Import_model->make_import_data($import_id);
		if(!empty($results['errors'])){
			$response['error'] = implode('<br>', $results['errors']);
		}else{
			$response["imported"] = $results["data"]["imported"];
			$response["failed"] = $results["data"]["failed"];
			$response["total"] = $import_data["total"] - ($results["data"]["imported"] + $results["data"]["failed"]);
			if(!$response["continue"]){
				if($response["failed"]){
					$response["error"] =
						l("error_data_imported", "import").": ".$results["data"]["imported"]."<br>".
						l("error_data_failed", "import").": ".$results["data"]["failed"];
				}else{
					$response["success"] = l("success_data_imported", "import").": ".$results["data"]["imported"];
				}
			}
		}
		echo json_encode($response);
		exit;
	}
	
	/**
	 * Render driver list action
	 * 
	 * @return void
	 */
	public function drivers(){
		$this->load->model("import/models/Import_driver_model");
		$drivers = $this->Import_driver_model->get_drivers();
		$this->template_lite->assign("drivers", $drivers);
		$this->Menu_model->set_menu_active_item("admin_import_menu", "import_drivers_items");
		$this->template_lite->view("drivers_list");
	}

	/**
	 * Activate/de-activate driver
	 * 
	 * @param string $dirver_gid driver guid
	 * @param integer $status driver status
	 * @return void
	 */
	public function activate_driver($driver_gid, $status=1){
		$this->load->model("import/models/Import_driver_model");
		if($status){
			$this->Import_driver_model->activate_driver($driver_gid);
		}else{
			$this->Import_driver_model->deactivate_driver($driver_gid);
		}		
		redirect(site_url()."admin/import/drivers");
	}
	
	
	
	//-----------------------------  Import listings ------------------------------------
	public function get_property_type_id($propertytype)
	{
		$lang_id = $this->pg_language->current_lang_id;
		$this->pg_language->ds->return_module($this->Properties_model->module_gid, $lang_id);
		$ds = $this->pg_language->ds->lang[$lang_id][$this->Properties_model->module_gid];
		
		$category_gids = $this->Properties_model->categories;
		$property_gids = $this->Properties_model->properties;
		
		foreach($category_gids as $gid){
			$categories[$gid] = $ds[$gid];
		}
		
		foreach($property_gids as $gid){
			$properties[$gid] = $ds[$gid];
			$properties[$gid]['status'] = $this->pg_module->get_module_config('listings', $gid.'_enabled');
		}
		return $properties[$propertytype]['option'];
		
	}
	
	public function get_category_type_id($propertytype)
	{
		$lang_id = $this->pg_language->current_lang_id;
		$this->pg_language->ds->return_module($this->Properties_model->module_gid, $lang_id);
		$ds = $this->pg_language->ds->lang[$lang_id][$this->Properties_model->module_gid];
		
		$category_gids = $this->Properties_model->categories;
		$property_gids = $this->Properties_model->properties;
		
		foreach($category_gids as $gid){
			$categories[$gid] = $ds[$gid];
		}
		
		foreach($property_gids as $gid){
			$properties[$gid] = $ds[$gid];
			$properties[$gid]['status'] = $this->pg_module->get_module_config('listings', $gid.'_enabled');
		}
		$arrCat = $categories['property_types']['option'];
		$catId = 0;
		foreach ($arrCat as $key => $value) {
			$strCatId = $key;
			$arrProp = $properties['property_type_'.$strCatId];
			$result = array_search($propertytype,$arrProp['option']);
			if($result){
				$catId = $strCatId;
			}
		}

		return $catId;
	}
	
	public function import_automatic_listings($tab_id='list', $id="0", $isdelete = ""){
		$strText = array('tabtxt'=>'Add new definition','subtitle'=>'Add new XML import definition');
		
		if($tab_id == "list"){
			if($isdelete == "delete"){
				$this->Import_xml_listings_model->delete_listings_defenition($id);
				$this->system_messages->add_message('success', 'XML import definition deleted successfully');
				$url = site_url()."admin/import/import_automatic_listings/list";
				redirect($url);
			}
			
			$strSQL = "SELECT xml.*,usr.unique_name AS user_name FROM ".CRON_XML_TABLE." xml
LEFT JOIN ".USERS_TABLE." usr ON usr.id = xml.user_id";
			$xmldata = $this->Import_xml_listings_model->get_data_custom($strSQL);
			$this->template_lite->assign("xmldata", $xmldata);
		}
		
		if($tab_id == "new" || $tab_id == "edit"){
			if($this->input->post('save_btn_definition')){
				$arrData['user_id'] = $_POST['id_user'];
				$arrData['link'] = $_POST['txtlink'];
				$arrData['import_type'] = $_POST['import_type'];
				$arrData['frequency'] = $_POST['frequency'];
				$arrData['overwrite_type'] = $_POST['overwrite_type'];
				$arrData['copy_english_text'] = $_POST['copy_english_text'];
				$arrData['add_property_id'] = $_POST['add_property_id'];
				$arrData['date_start'] = date("Y-m-d H:i:s");
				
				if($arrData['user_id'] == ''){
					$this->system_messages->add_message('error', "Chose Ancita user for definition assignment");
					
				} else if($arrData['link'] == ''){
					$this->system_messages->add_message('error', "Enter the XML file link");
				} else if($arrData['import_type'] == '0'){
					$this->system_messages->add_message('error', "Please make a choice of the import type");
				} else if($arrData['frequency'] == '0'){
					$this->system_messages->add_message('error', "Please make a choice of the import frequency");
				} else if($arrData['overwrite_type'] == '0'){
					$this->system_messages->add_message('error', "Please make a choice of the overwrite type");
				} else {
					$this->Import_xml_listings_model->save_listings_import_defenition($arrData,$id);
					if($tab_id == "edit"){
						$this->system_messages->add_message('success', 'XML import definition updated successfully');
					} else {
						$this->system_messages->add_message('success', 'New XML import definition added successfully');
					}
					$url = site_url()."admin/import/import_automatic_listings/list";
					redirect($url);
				}
			}
			
			if($tab_id == "edit" && $id != "0"){
				$strSQL = "SELECT * FROM ".CRON_XML_TABLE." WHERE id=".$id;
				$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
				$arrData = $tempResult[0];
			}
			
			if(!$arrData){
				$arrData['overwrite_type'] = '1002';
				$arrData['copy_english_text'] = 'false';
				$arrData['add_property_id'] = 'false';
			}
			
			$this->template_lite->assign("custVal", $arrData);
			if($tab_id == "edit"){
				$strText = array('tabtxt'=>'Update definition','subtitle'=>'Update XML import definition','tmplink'=>'edit/'.$id);
			}
		}
		
		$page_data['date_format'] = '%Y-%m-%d';
		$this->template_lite->assign("tab_id", $tab_id);
		$this->template_lite->assign("strText", $strText);
		$this->template_lite->assign("page_data", $page_data);
		$this->system_messages->set_data("header", l("admin_header_import_automatic_listings", "import"));
		$this->template_lite->view("import_automatic_listings");
	}
	
	//-------------Residential
	public function import_listings_kyero($tab_id='',$action='',$refId=''){
		if($tab_id == '')
			$tab_id = "XML";
	
		$strTempResult = '';
		if($tab_id == "XML")
		{
			
			if($this->input->post('save_load_xml')){
				$arrData = $_POST;
				$file = $_FILES['db_file'];
				$result = $this->ProcessXML($arrData,$file,"kyero");
				if($result["Error"] != "")
				{
					$this->system_messages->add_message('error', $result["Error"]);
				}
				else
				{
					$strTempResult = $result["Result"];
				}
				
				$id_user = $arrData["id_user"];
			}
		}
			
		if($tab_id == 'XPath')
		{
			if($this->input->post('save_settings_thinkspain')){
				$active_rule = $this->GetVarString2('active_rule');	
				$this->Import_xml_listings_model->update_convertion_xpath($active_rule,"kyero");
				$this->system_messages->add_message('success', 'XPath rule saved successfully');
			}
		}
			
		if($tab_id == 'Field')
		{
			if($action == "delete")
			{
				$this->Import_xml_listings_model->delete_convertion_value($refId,"kyero");
				$this->system_messages->add_message('success', 'Rule deleted successfully');
				$url = site_url()."admin/import/import_listings_kyero/".$tab_id;
				redirect($url);
			}
			
			if($this->input->post('save_settings_rule_thinkspain')){
				
				if($_POST['ancita_field'] == "0")
				{
				 	$this->system_messages->add_message('error', "Select the Ancita field");
				}
				elseif(empty($_POST['xml_value']))
				{
					$this->system_messages->add_message('error', "Enter the XML value ");
				}
				elseif(empty($_POST['ancita_value']))
				{
					$this->system_messages->add_message('error', "Enter the Ancita value");
				}
				else
				{
					$data['xml_code'] = "kyero";
					$data['ancita_field'] = $this->GetVarString('ancita_field');
					$data['xml_value'] = $this->GetVarString('xml_value');
					$data['ancita_value'] = $this->GetVarString('ancita_value');
					$this->Import_xml_listings_model->save_Convertion_value($data);
					$this->system_messages->add_message('success', 'New rule added successfully');
				}
			}
		}
		
		$rules_values = $this->Import_xml_listings_model->get_convertion_value("kyero");
		$rules = $this->Import_xml_listings_model->get_convertion_xpath("kyero");
		
		$this->template_lite->assign("tab_id", $tab_id);
		$this->template_lite->assign("rules_values", $rules_values);
		$this->template_lite->assign("rules", $rules[0]);
		$this->template_lite->assign("strTempResult", $strTempResult);
		$this->template_lite->assign("id_user", $id_user);
		
		$this->template_lite->assign("tab_id", $tab_id);
		$this->system_messages->set_data("header", l("admin_header_import_xml_kyero", "import"));
		$this->template_lite->view("import_listings_kyero");
	}
	
	//--------------------Commercial
	public function import_listings_alphashare($tab_id='',$action='',$refId=''){
		if($tab_id == '')
			$tab_id = "XML";
			
		$strTempResult = '';
		if($tab_id == "XML")
		{
			if($this->input->post('save_load_xml')){
				$arrData = $_POST;
				$file = $_FILES['db_file'];
				$result = $this->ProcessXML($arrData,$file,"alphashare");
				if($result["Error"] != "")
				{
					$this->system_messages->add_message('error', $result["Error"]);
				}
				else
				{
					$strTempResult = $result["Result"];
				}
				
				$id_user = $arrData["id_user"];
			}
		}
			
		if($tab_id == 'XPath')
		{
			if($this->input->post('save_settings_thinkspain')){
				$active_rule = $this->GetVarString2('active_rule');	
				$this->Import_xml_listings_model->update_convertion_xpath($active_rule,"alphashare");
				$this->system_messages->add_message('success', 'XPath rule saved successfully');
			}
		}
			
		if($tab_id == 'Field')
		{
			if($action == "delete")
			{
				$this->Import_xml_listings_model->delete_convertion_value($refId,"alphashare");
				$this->system_messages->add_message('success', 'Rule deleted successfully');
				$url = site_url()."admin/import/import_listings_alphashare/".$tab_id;
				redirect($url);
			}
			
			if($this->input->post('save_settings_rule_thinkspain')){
				
				if($_POST['ancita_field'] == "0")
				{
				 	$this->system_messages->add_message('error', "Select the Ancita field");
				}
				elseif(empty($_POST['xml_value']))
				{
					$this->system_messages->add_message('error', "Enter the XML value ");
				}
				elseif(empty($_POST['ancita_value']))
				{
					$this->system_messages->add_message('error', "Enter the Ancita value");
				}
				else
				{
					$data['xml_code'] = "alphashare";
					$data['ancita_field'] = $this->GetVarString('ancita_field');
					$data['xml_value'] = $this->GetVarString('xml_value');
					$data['ancita_value'] = $this->GetVarString('ancita_value');
					$this->Import_xml_listings_model->save_Convertion_value($data);
					$this->system_messages->add_message('success', 'New rule added successfully');
				}
			}
		}
		
		$rules_values = $this->Import_xml_listings_model->get_convertion_value("alphashare");
		$rules = $this->Import_xml_listings_model->get_convertion_xpath("alphashare");
		
		$this->template_lite->assign("tab_id", $tab_id);
		$this->template_lite->assign("rules_values", $rules_values);
		$this->template_lite->assign("rules", $rules[0]);
		$this->template_lite->assign("strTempResult", $strTempResult);
		$this->template_lite->assign("id_user", $id_user);
		
		$this->template_lite->assign("tab_id", $tab_id);
		$this->system_messages->set_data("header", l("admin_header_import_xml_alphashare", "import"));
		$this->template_lite->view("import_listings_alphashare");
	}
	
	//--------------------Lots/lands
	public function import_listings_ampervillas($tab_id='',$action='',$refId=''){
		if($tab_id == '')
			$tab_id = "XML";
			
		$strTempResult = '';
		if($tab_id == "XML")
		{
			if($this->input->post('save_load_xml')){
				$arrData = $_POST;
				$file = $_FILES['db_file'];
				$result = $this->ProcessXML($arrData,$file,"thinkspain");
				if($result["Error"] != "")
				{
					$this->system_messages->add_message('error', $result["Error"]);
				}
				else
				{
					$strTempResult = $result["Result"];
				}
				
				$id_user = $arrData["id_user"];
			}
		}
		
		if($tab_id == 'XPath')
		{
			if($this->input->post('save_settings_thinkspain')){
				$active_rule = $this->GetVarString2('active_rule');	
				$this->Import_xml_listings_model->update_convertion_xpath($active_rule,"thinkspain");
				$this->system_messages->add_message('success', 'XPath rule saved successfully');
			}
		}
			
		if($tab_id == 'Field')
		{
			if($action == "delete")
			{
				$this->Import_xml_listings_model->delete_convertion_value($refId,"thinkspain");
				$this->system_messages->add_message('success', 'Rule deleted successfully');
				$url = site_url()."admin/import/import_listings_ampervillas/".$tab_id;
				redirect($url);
			}
			
			if($this->input->post('save_settings_rule_thinkspain')){
				
				if($_POST['ancita_field'] == "0")
				{
				 	$this->system_messages->add_message('error', "Select the Ancita field");
				}
				elseif(empty($_POST['xml_value']))
				{
					$this->system_messages->add_message('error', "Enter the XML value ");
				}
				elseif(empty($_POST['ancita_value']))
				{
					$this->system_messages->add_message('error', "Enter the Ancita value");
				}
				else
				{
					$data['xml_code'] = "thinkspain";
					$data['ancita_field'] = $this->GetVarString('ancita_field');
					$data['xml_value'] = $this->GetVarString('xml_value');
					$data['ancita_value'] = $this->GetVarString('ancita_value');
					$this->Import_xml_listings_model->save_Convertion_value($data);
					$this->system_messages->add_message('success', 'New rule added successfully');
				}
			}
		}
		
		$rules_values = $this->Import_xml_listings_model->get_convertion_value("thinkspain");
		$rules = $this->Import_xml_listings_model->get_convertion_xpath("thinkspain");
		
		$this->template_lite->assign("tab_id", $tab_id);
		$this->template_lite->assign("rules_values", $rules_values);
		$this->template_lite->assign("rules", $rules[0]);
		$this->template_lite->assign("strTempResult", $strTempResult);
		$this->template_lite->assign("id_user", $id_user);
		
		$this->system_messages->set_data("header", l("admin_header_import_ampervillas", "import"));
		$this->template_lite->view("import_listings_ampervillas");
	}
	
	function ProcessXML($arrData,$upload,$api){ 
		error_reporting(0);
		set_time_limit(0);
		$csv_mask = array("Country", "Region", "City", "Zip", "Address", "Price", "Operation type", "Property Type", "Bedrooms", "Bathrooms", "Year", "Garages", "Square", "Plot size", "Total floors", "Floor number", "Distance to subway", "Headline-EN", "Headline-SE", "Headline-NO", "Comment-EN", "Comment-SE", "Comment-NO", "photos", "Id Agent Property");
		$cur = $this->Import_xml_listings_model->get_default_currency_symbol();
		$cur_symbol = $cur["symbol"];
		$nuser = $arrData["id_user"];
		
		if($nuser == "")
		{
			return array("Error"=>"Choose user for XML import","Result"=>"");
		}
		
		if($upload["name"] == "")
		{
			return array("Error"=>"Select XML file","Result"=>"");
		}
		
		$tempPath = SITE_PATH.SITE_SUBFOLDER.'temp/import';
		//$userId = intval($this->session->userdata("user_id", true));
		
		$handle = fopen($upload["tmp_name"], "r");
		$info_file = explode(".", $upload["name"]);    
		$file_name = $nuser."_import_listing_xml.".$info_file[1];    
		$file_path = $tempPath."/".$file_name; 
		
		copy($upload["tmp_name"],$file_path);
		
		if (!$info_file || ($info_file[1]!='zip' && $info_file[1] != 'xml')){
			return array("Error"=>"Invaild XML file type","Result"=>"");
		}
		
		$ads = $this->GetAdsWithUserPropertyId($nuser, 0, 0);
		
		for($c = 0; $c < count($csv_mask); $c++)
		{
			$import[0][] = $csv_mask[$c];
		}
		
		$i = 1;
		$xml = simplexml_load_file(utf8_decode($file_path), null, LIBXML_NOCDATA);
		
		if(!$xml)
		{
			return array("Error"=>"XML Parsing Error - not well-formed","Result"=>"");
		}
		
		unset($xml->alphashare);
		unset($xml->agent);
		$strSQL = "SELECT * FROM ".CONVERSION_XPATH_TABLE." WHERE xml_code='".$api."'";
		$rules = $this->Import_xml_listings_model->get_data_custom($strSQL);
		$rules = $rules[0];
		
		$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='city'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		for($t = 0; $t < count($tempResult); $t++)
		{
			$ary_city1[] = '@'.$tempResult[$t]['xml_value'].'$@';
			$ary_city2[] = $tempResult[$t]['ancita_value'];
		}
		
		$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='country'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		for($t = 0; $t < count($tempResult); $t++)
		{
			$ary_country1[] = '@'.$tempResult[$t]['xml_value'].'$@';
			$ary_country2[] = $tempResult[$t]['ancita_value'];
		}
		
		$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='region'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		for($t = 0; $t < count($tempResult); $t++)
		{
			$ary_region1[] = str_replace(')','\)',str_replace('(','\(','@'.$tempResult[$t]['xml_value'].'$@'));
			$ary_region2[] = $tempResult[$t]['ancita_value'];
		}
		
		$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='property_type'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		for($t = 0; $t < count($tempResult); $t++)
		{
			$ary_property1[] = str_replace(')','\)',str_replace('(','\(','@'.$tempResult[$t]['xml_value'].'$@'));
			$ary_property2[] = $tempResult[$t]['ancita_value'];
		}
		
		$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='sale_type'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		for($t = 0; $t < count($tempResult); $t++)
		{
			$ary_type1[] = str_replace(')','\)',str_replace('(','\(','@'.$tempResult[$t]['xml_value'].'$@'));
			$ary_type2[] = $tempResult[$t]['ancita_value'];
		}
		
		$j = 1;
		foreach($xml->xpath('property') as $key => $val) {
			$country = null;
			$region = null;		
			$city = null;
			$zip = null;
			$address = null;
			$price = null;
			$sale_type = null;
			$property_type = null;		
			$beds = null;
			$baths = null;
			$year = null;
			$garages = null;
			$plot_size = null;
			$square = null;
			$total_floors = null;
			$floor_number = null;
			$distance_to_subway = null;
			$headline_en = null;
			$comments_en = null;
			$headline_se = null;
			$comments_se = null;
			$headline_no = null;
			$comments_no = null;
			$agent_property_id = null;
			
			eval($rules['active_rule']);
			
			$country = isset($country[0]) ? $val->xpath('country') : array(0 => 'Spain');
			$photoCount = count($photos);
	
	
			if(isset($ary_country1) && isset($ary_country2)) { $ancita_country = preg_replace($ary_country1, $ary_country2, (array)(string) $country[0]); } else { $ancita_country = (array)(string) $country[0]; };
			if(isset($ary_region1) && isset($ary_region2)) { $ancita_region = preg_replace($ary_region1, $ary_region2, (array)(string) $region[0]); } else { $ancita_region = (array)(string) $region[0]; };		
			if(isset($ary_city1) && isset($ary_city2)) { $ancita_city = preg_replace($ary_city1, $ary_city2, (array)(string) $city[0]); } else { $ancita_city = (array)(string) $city[0]; };
			$ancita_zip = (array)(string) $zip[0];
			$ancita_address = (array)(string) $address[0];
			$ancita_price = (array)(string) $price[0];
			if(isset($ary_type1) && isset($ary_type2)) { $ancita_type = preg_replace($ary_type1, $ary_type2, (array)(string) $sale_type[0]); } else { $ancita_type = (array)(string) $sale_type[0]; };		
			if(isset($ary_property1) && isset($ary_property2)) {
				$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='property_type' AND xml_value='".(string) $property_type[0]."'";
	         	$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
				$ancita_property = (array)$tempResult[0]['ancita_value'];
				 } else { 
				 $ancita_property = (array)(string) $property_type[0]; };		
			$ancita_beds = (array)(string) $beds[0];
			$ancita_baths = (array)(string) $baths[0];
			$ancita_year = (array)(string) $year[0];
			$ancita_garage = (array)(string) $garages[0];
			$ancita_max_square = (array)(string) $plot_size[0];
			$ancita_total_square = (array)(string) $square[0];
			$ancita_floor = (array)(string) $total_floors[0];
			$ancita_floor_num = (array)(string) $floor_number[0];
			$ancita_subway_min = (array)(string) $distance_to_subway[0];

			if($headline_en) $ancita_headline_en = (array)(string) $headline_en[0];
			if($comments_en) $ancita_comment_en = (array)(string) $comments_en[0];
			if($headline_se) $ancita_headline_se = (array)(string) $headline_se[0];
			if($comments_se) $ancita_comment_se = (array)(string) $comments_se[0];
			if($headline_no) $ancita_headline_no = (array)(string) $headline_no[0];
			if($comments_no) $ancita_comment_no = (array)(string) $comments_no[0];
					
			$ancita_agent_property_id = (array)(string) $agent_property_id[0];
			
			if(!empty($ancita_country[0])) { $import[$j]["country"] = $ancita_country[0]; } else { $import[$j]["country"] = ""; };
			if(!empty($ancita_region[0])) { $import[$j]["region"] = $ancita_region[0]; } else { $import[$j]["region"] = ""; };
			if(!empty($ancita_city[0])) { $import[$j]["city"] = $ancita_city[0]; } else { $import[$j]["city"] = ""; };
			if(!empty($ancita_zip[0])) { $import[$j]["zip"] = $ancita_zip[0]; } else { $import[$j]["zip"] = ""; };
			if(!empty($ancita_address[0])) { $import[$j]["address"] = $ancita_address[0]; } else { $import[$j]["address"] = ""; };
			if(!empty($ancita_price[0])) { $import[$j]["price"] = $ancita_price[0]; } else { $import[$j]["price"] = ""; };			
			if(!empty($ancita_type[0])) { $import[$j]["sale_type"] = $ancita_type[0]; } else { $import[$j]["sale_type"] = ""; };
			if(!empty($ancita_property[0]))
			{
				$tempCategory = $this->get_category_type_id($ancita_property[0]);
				if(intval($tempCategory) > 0)
				{
					$import[$j]["property_type"] = $ancita_property[0]; 
				} else { $import[$j]["property_type"] = ""; }
			} else { $import[$j]["property_type"] = ""; };
			 
			if(!empty($ancita_beds[0])) { $import[$j]["beds"] = $ancita_beds[0]; } else { $import[$j]["beds"] = ""; };
			if(!empty($ancita_baths[0])) { $import[$j]["baths"] = $ancita_baths[0]; } else { $import[$j]["baths"] = ""; };
			if(!empty($ancita_year[0])) { $import[$j]["year"] = $ancita_year[0]; } else { $import[$j]["year"] = ""; };
			if(!empty($ancita_garage[0])) { $import[$j]["garages"] = $ancita_garage[0]; } else { $import[$j]["garages"] = ""; };
			if(!empty($ancita_max_square[0])) { $import[$j]["max_square"] = $ancita_max_square[0]; } else { $import[$j]["max_square"] = "0"; };
			if(!empty($ancita_total_square[0])) { $import[$j]["square"] = $ancita_total_square[0]; } else { $import[$j]["square"] = "0"; };
			if(!empty($ancita_floor[0])) { $import[$j]["total_floors"] = $ancita_floor[0]; } else { $import[$j]["total_floors"] = "0"; };
			if(!empty($ancita_floor_num[0])) { $import[$j]["floor_number"] = $ancita_floor_num[0]; } else { $import[$j]["floor_number"] = "0"; };
			if(!empty($ancita_subway_min[0])) { $import[$j]["distance_to_subway"] = $ancita_subway_min[0]; } else { $import[$j]["distance_to_subway"] = "0"; };		
			if($ancita_headline_en[0] != '') {$import[$j]["headline_en"] = substr($ancita_headline_en[0], 0, 150) . "..."; } else { $import[$j]["headlines_en"] = ""; };
			
			if($ancita_headline_se[0] != '') {$import[$j]["headline_se"] = substr($ancita_headline_se[0], 0, 150) . "..."; } else { $import[$j]["headlines_se"] = ""; };
			if($ancita_headline_no[0] != '') {$import[$j]["headline_no"] = substr($ancita_headline_no[0], 0, 150) . "..."; } else { $import[$j]["headlines_no"] = ""; };
			if($ancita_comment_en[0] != '') { $import[$j]["comments_en"] = trim($ancita_comment_en[0]); } else { $import[$j]["comments_en"] = ""; };
			if($ancita_comment_se[0] != '') { $import[$j]["comments_se"] = trim($ancita_comment_se[0]); } else { $import[$j]["comments_se"] = ""; };
			if($ancita_comment_no[0] != '') { $import[$j]["comments_no"] = trim($ancita_comment_no[0]); } else { $import[$j]["comments_no"] = ""; };
		
			$import[$j]["photos"] = $photoCount;
			$import[$j]["id_agent_property"] = (string)$ancita_agent_property_id[0];
			
			if(intval($import[$j]["id_agent_property"]) == 0){
				$import[$j]["is_old"] = 0;
			} else {
				if ($this->IsMyAdsXml($import[$j], $ads) == 1){
					$import[$j]["is_old"] = 2;
				}else{
					$import[$j]["is_old"] = 0;
				} 
			}
			$j++;
		}
		
		//Make Result
		$str = 'The table below list all data from a file you submited. If some of the recods are not highlighted with green color, that would mean that listing with such parameters already exist and will be updated by default. if you dont want to make an update of existing records, please mark the approprite radiobutton at the bottom of the page<br>';
		
		$str .= '<br><input type="checkbox" name="mark_all" id="approve_all" >Mark / Unmark all listings for import<br><br><table class="table_main" cellpadding="3" cellspacing="1">';
		
		$str2 = '';
		$checkbox_arr = array();
		for($row = 0; $row < count($import); $row++)
		{
			if($row != 0 && $import[$row]['is_old'] == 0)
				$bg_str = 'id="tr_str_'.$row.'" style="background-color:#EBFDF3;"';
			else
				$bg_str = 'id="tr_str_'.$row.'" style="background-color:#FFFFCC;"';
			
			$str .='<tr '. $bg_str .'>';
        	$str2 .='<tr '. $bg_str .'>';
				
			if($row == 0)
			{
				$str .="<td>&nbsp;</td>";
				$str .="<th align='center'>Record No</th>";
				$str2 .="<th align='center'>Record No</th>";
				
				for($r = 0; $r < count($import[$row]); $r++)
				{
					if($r < 13){
				 		$str .= "<th align='center'>" . $import[$row][$r] . "</th>";
					}
					if($r >= 13){
						$str2 .= "<th align='center'>" . $import[$row][$r] . "</th>";
					}
				}
			}
			else
			{
				if($import[$row]['is_old'] == 2)
				{
					$checked_flag = "disabled";
					$checked_class = "";
				}
				else
				{
					$checked_flag = "checked";
					$checked_class = "approve";
				}
				
				$str .= "<td><input type='checkbox' class='".$checked_class."' style='margin:0px;' ".$checked_flag." value='".$row."'>";
				$str .= "<input type='hidden' name='checkbox_arr[]' value='str_".$row."'/></td>";
				$checkbox_arr[$row] = 'str_'.$row;	
				
				$str .="<td align='center'>" . $row . "</td>";
				$str2 .="<td align='center'>" . $row . "</td>";
					
				
				$reset_array_keys = array();
				foreach($import[$row] as $value) {
					$reset_array_keys[] = $value;
				}

				$arrVal = $reset_array_keys;
				for($r2 = 0; $r2 < count($arrVal) - 1; $r2++)
				{
					if($r2 < 13){
						$str .="<td>" . $arrVal[$r2] . "</td>";
					}
					if($r2 >= 13){
						$str2 .="<td>" . $arrVal[$r2] . "</td>";
					}
				}
			}
			
			$str .='</tr>';
        	$str2 .='</tr>';
			
		}
		
		$str .= '</table><br/><br/>
				<table class="table_main" cellpadding="3" cellspacing="1">' . $str2 .'</table>
				<table style="position: relative; left: 20px; bottom: 10px;">
				<tr>
					<td><input type="radio" class="myradio" name="myradio" value="1000"></td>
					<td>Overwrite all the existing records</td>
					<td style="padding-left: 50px;"><input type="checkbox" id="mycheckbox1"></td>
					<td>Copy the English heading text into the Norwegian Swedish language? (if both norwegian and swedish headings are empty)</td>
				</tr>
				<tr>
					<td><input type="radio" class="myradio" name="myradio" value="1001"></td>
					<td>Skip update of existing records</td>
					<td style="padding-left: 50px"><input type="checkbox" id="mycheckbox2"></td>
					<td>Add your Companys property id to the heading?</td>
				</tr>
				<tr>
					<td><input type="radio" class="myradio" name="myradio" checked value="1002"></td>
					<td>Do not overwrite existing records with blank data</td>
				</tr>
				</table>
				<div class="btn"><div class="l"><input type="button" value="Import" class="btn_small" onclick="ImportListing();"></div></div><br><br><span id="import_status"></span>';
				
		return array("Error"=>"","Result"=>$str);
	}
	//process to store the listings in manual temp table
	public function ajax_Import_XML_listing()
	{
		error_reporting(0);
		set_time_limit(0);
		$api = $_REQUEST["api"];
		$listing_ids = $_REQUEST["listing_ids"];
		$skip_ids = $_REQUEST["skip_ids"];
		$check1 = $_REQUEST["check1"];
		$check2 = $_REQUEST["check2"];
		$nuser = $_REQUEST["nuser"];
		
		$index = explode("|", $listing_ids);
		
		$tempPath = SITE_PATH.SITE_SUBFOLDER.'temp/import';
		//read the xml file and save it to temp path
		$file_name = $nuser."_import_listing_xml.xml";    
		$file_path = $tempPath."/".$file_name; 
		
		$xml = simplexml_load_file($file_path, null, LIBXML_NOCDATA);
		unset($xml->alphashare);
		unset($xml->agent);
		
		$strSQL = "SELECT * FROM ".CONVERSION_XPATH_TABLE." WHERE xml_code='".$api."'";
		$rules = $this->Import_xml_listings_model->get_data_custom($strSQL);
		$rules = $rules[0];
		
		$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='city'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		for($t = 0; $t < count($tempResult); $t++)
		{
			$ary_city1[] = '@'.$tempResult[$t]['xml_value'].'$@';
			$ary_city2[] = $tempResult[$t]['ancita_value'];
		}
		
		$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='country'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		for($t = 0; $t < count($tempResult); $t++)
		{
			$ary_country1[] = '@'.$tempResult[$t]['xml_value'].'$@';
			$ary_country2[] = $tempResult[$t]['ancita_value'];
		}
		
		$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='region'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		for($t = 0; $t < count($tempResult); $t++)
		{
			$ary_region1[] = str_replace(')','\)',str_replace('(','\(','@'.$tempResult[$t]['xml_value'].'$@'));
			$ary_region2[] = $tempResult[$t]['ancita_value'];
		}
		
		$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='property_type'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		for($t = 0; $t < count($tempResult); $t++)
		{
			$ary_property1[] = str_replace(')','\)',str_replace('(','\(','@'.$tempResult[$t]['xml_value'].'$@'));
			$ary_property2[] = $tempResult[$t]['ancita_value'];
		}
		
		$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='sale_type'";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		for($t = 0; $t < count($tempResult); $t++)
		{
			$ary_type1[] = str_replace(')','\)',str_replace('(','\(','@'.$tempResult[$t]['xml_value'].'$@'));
			$ary_type2[] = $tempResult[$t]['ancita_value'];
		}
		
		$j = 1;
		foreach($xml->xpath('property') as $key => $val) {
		   if(in_array($j, $index)) {
			$country = null;
			$region = null;		
			$city = null;
			$zip = null;
			$address = null;
			$price = null;
			$sale_type = null;
			$property_type = null;		
			$beds = null;
			$baths = null;
			$year = null;
			$garages = null;
			$plot_size = null;
			$square = null;
			$total_floors = null;
			$floor_number = null;
			$distance_to_subway = null;
			$headline_en = null;
			$comments_en = null;
			$headline_se = null;
			$comments_se = null;
			$headline_no = null;
			$comments_no = null;
			$agent_property_id = null;
			
			eval($rules['active_rule']);
			
			$country = isset($country[0]) ? $val->xpath('country') : array(0 => 'Spain');
			
			$photoCount = count($photos) - 1;
			$photoUrls = "";
			foreach($photos as $key => $photo) {
				$photoUrls .= $photo;
				if($key<$photoCount) $photoUrls .= "|";
			}	
			
			if(isset($ary_country1) && isset($ary_country2)) { $ancita_country = preg_replace($ary_country1, $ary_country2, (array)(string) $country[0]); } else { $ancita_country = (array)(string) $country[0]; };
			if(isset($ary_region1) && isset($ary_region2)) { $ancita_region = preg_replace($ary_region1, $ary_region2, (array)(string) $region[0]); } else { $ancita_region = (array)(string) $region[0]; };		
			if(isset($ary_city1) && isset($ary_city2)) { $ancita_city = preg_replace($ary_city1, $ary_city2, (array)(string) $city[0]); } else { $ancita_city = (array)(string) $city[0]; };
			$ancita_zip = (array)(string) $zip[0];
			$ancita_address = (array)(string) $address[0];
			$ancita_price = (array)(string) $price[0];
			if(isset($ary_type1) && isset($ary_type2)) { $ancita_type = preg_replace($ary_type1, $ary_type2, (array)(string) $sale_type[0]); } else { $ancita_type = (array)(string) $sale_type[0]; };		
			if(isset($ary_property1) && isset($ary_property2)) {
				$strSQL = "SELECT * FROM ".CONVERSION_VALUES_TABLE." WHERE xml_code='".$api."' AND ancita_field='property_type' AND xml_value='".(string) $property_type[0]."'";
	         	$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
				$ancita_property = (array)$tempResult[0]['ancita_value'];
				 } else { 
				 $ancita_property = (array)(string) $property_type[0]; };		
			$ancita_beds = (array)(string) $beds[0];
			$ancita_baths = (array)(string) $baths[0];
			$ancita_year = (array)(string) $year[0];
			$ancita_garage = (array)(string) $garages[0];
			$ancita_max_square = (array)(string) $plot_size[0];
			$ancita_total_square = (array)(string) $square[0];
			$ancita_floor = (array)(string) $total_floors[0];
			$ancita_floor_num = (array)(string) $floor_number[0];
			$ancita_subway_min = (array)(string) $distance_to_subway[0];
			$ancita_headline_en = (array)(string) $headline_en[0];
			$ancita_comment_en = (array)(string) $comments_en[0];
			$ancita_headline_se =(array) (string) $headline_se[0];
			$ancita_comment_se = (array)(string) $comments_se[0];
			$ancita_headline_no =(array) (string) $headline_no[0];
			$ancita_comment_no = (array)(string) $comments_no[0];
			$ancita_agent_property_id = (array)(string) $agent_property_id[0];
		    if( $ancita_property[0] == '')
			{
				$ancita_property[0] = 'Villa';
			}
			$tempCategory = $this->get_category_type_id($ancita_property[0]);
			if(intval($tempCategory) > 0)
			{
				$import[$j]["country"] = $ancita_country[0];
				$import[$j]["region"] = $ancita_region[0];
				$import[$j]["city"] = $ancita_city[0];
				$import[$j]["zip"] = $ancita_zip[0];
				$import[$j]["address"] = $ancita_address[0];
				$import[$j]["price"] = $ancita_price[0];
				$import[$j]["sale_type"] = $ancita_type[0];
				$import[$j]["category_type"] = $tempCategory;
				$import[$j]["property_type"] = $ancita_property[0];
				$import[$j]["beds"] = $ancita_beds[0];
				$import[$j]["baths"] = $ancita_baths[0];
				$import[$j]["year"] = $ancita_year[0];
				$import[$j]["garages"] = $ancita_garage[0];
				$import[$j]["max_square"] = $ancita_max_square[0];
				$import[$j]["square"] = $ancita_total_square[0]; 
				$import[$j]["total_floors"] = $ancita_floor[0];
				$import[$j]["floor_number"] = $ancita_floor_num[0];
				$import[$j]["distance_to_subway"] = $ancita_subway_min[0];
				
				$refval = "";
			    $refval = "Ref: " . (string) $ref[0] . " - ";
				$import[$j]["headline_en"] = $refval . substr($ancita_headline_en[0], 0, 150) . "...";
				
				if($check1 == 'true') { $import[$j]["headline_se"] = $refval . substr($ancita_headline_en[0], 0, 150) . "..."; } else { $import[$j]["headline_se"] = $refval . substr($ancita_headline_se[0], 0, 150) . "..."; };
		if($check1 == 'true') { $import[$j]["headline_no"] = $refval . substr($ancita_headline_en[0], 0, 150) . "..."; } else { $import[$j]["headline_no"] = $refval . substr($ancita_headline_no[0], 0, 150) . "..."; };
				
				$import[$j]["comments_en"] = trim($ancita_comment_en[0]);
				$import[$j]["comments_se"] = trim($ancita_comment_se[0]);
				$import[$j]["comments_no"] = trim($ancita_comment_no[0]);
				
				$import[$j]["photos"] = $photoCount;
				$import[$j]["photoUrls"] = $photoUrls;
				$import[$j]["id_agent_property"] = (string)$ancita_agent_property_id[0];
				if((string)$ancita_beds[0] >=10){
				(string)$ancita_beds[0] = '10';
			    }
				if((string) $ancita_baths[0] >=10){
				(string) $ancita_baths[0] = '10';
			    }
				if((string)$ancita_garage[0] >=5){
				(string)$ancita_garage[0] = '6';
			    }
				//add manual import listings in manual temp table
				$strSQL1 = " REPLACE INTO " . LISTING_TEMP_TABLE_CRON_MANUAL . " (id_user, id_agent_property, xml_import_id, country, region, city, zip, address, price, sale_type, property_type, beds, baths, year, garages, max_square, square, floor_number, total_floors, distance_to_subway, headline_en, headline_se, headline_no, comments_en, comments_se, comments_no, photos, ref, date_created,import) VALUES (" . $nuser . ", " . (string)$ancita_agent_property_id[0] . ", " . $nuser . ", '" . (string)$ancita_country[0] . "', '" . (string)$ancita_region[0] . "', '" . (string)$ancita_city[0] . "', '" . (string)$ancita_zip[0] . "', '" . (string)$ancita_address[0] . "', '" . (string)$ancita_price[0] . "', '" . (string)$ancita_type[0] . "', '" . (string)$ancita_property[0] . "', '" . (string)$ancita_beds[0] . "', '" .(string) $ancita_baths[0] . "', '" . (string)$ancita_year[0] . "', '" . (string)$ancita_garage[0] . "', '" . (string)$ancita_max_square[0] . "', '" . (string)$ancita_total_square[0]. "', '" . (string)$ancita_floor_num[0] . "', '" . (string)$ancita_floor[0] . "', '" . (string)$ancita_subway_min[0] . "', '" . (string)addslashes($ancita_headline_en[0]) . "', '" . (string)addslashes($ancita_headline_se[0]) . "', '" . (string)addslashes($ancita_headline_no[0]) . "', '" . (string)addslashes($ancita_comment_en[0]) . "', '" . (string)addslashes($ancita_comment_se[0]) . "', '" . (string)addslashes($ancita_comment_no[0]) . "', '" . $photoUrls . "', '" . $refval . "','".date('Y-m-d H:i:s')."','manual')";
			
			$this->Import_xml_listings_model->save_temp_listing($strSQL1);
			
			   // add manual import conditions in temp table
			$strSQL1 = " REPLACE INTO " . LISTING_TEMP_TABLE_MANUAL . " (id, user_id, import_type,overwrite_type,copy_english_text,	add_property_id,date_start) VALUES (" . $nuser . ", " . $nuser . ", '" . $api . "', " . $skip_ids . ", '" . $check1 . "', '" . $check2 . "','".date('Y-m-d H:i:s')."')";
			
			$this->Import_xml_listings_model->save_temp_listing($strSQL1);
			}
		 }
			$j++;
		}

	//	$count = 0;
//		$k = 1;
		
		/*foreach($import as $item) {
			if(in_array($k, $index)) {
				$this->AddListingFromFile($item, $nuser, $api);	
				$count++;
			}*/
			
			/*if(!in_array($k, $index)) {
				$ary = null;
				$ary = array();
				$strSQL = "SELECT id FROM ".LISTINGS_TABLE." WHERE id_user=".$nuser." AND id_agent_property = '".$item["id_agent_property"] ."'";
				$ary = $this->Import_xml_listings_model->get_data_custom($strSQL);
				
				foreach ($ary as $val) {
					$id_ad = $val["id"];
					if ($skip_ids == 1000) {
						$this->AddListingFromFile($item, $nuser, $api, $id_ad, 1000); //UpdateListingFromFileAll($item, $id_ad, $nuser);
					}
					if ($skip_ids == 1002) {
						 $this->AddListingFromFile($item, $nuser, $api, $id_ad, 1002); //UpdateListingFromFileClean($item, $id_ad, $nuser);
					}
				
				}
			}
			$k++;
		}*/
		echo 'Updated';
	}
	
	function AddListingFromFile($fields, $nuser, $api, $listing_id = null, $update_type = ""){
		set_time_limit(0);
		$country_id = 'ES';
		$region_id = 0;
		$city_id = 0;
		$sale_type_id = $this->Import_xml_listings_model->get_operation_type($fields["sale_type"]);
		$strSQL = "SELECT code FROM ".COUNTRY_TABLE." WHERE name LIKE '".addslashes($fields["country"])."'";
		$result = $this->Import_xml_listings_model->get_data_custom($strSQL);
		if($result){
			$country_id = $result[0]['code'];
		}
		
		$strSQL = "SELECT id FROM ".REGION_TABLE." WHERE name LIKE '".addslashes($fields["region"])."'";
		$result = $this->Import_xml_listings_model->get_data_custom($strSQL);
		if($result){
			$region_id = $result[0]['id'];
		}
		
		$strSQL = "SELECT id FROM ".CITY_TABLE." WHERE name LIKE '".addslashes($fields["city"])."'";
		$result = $this->Import_xml_listings_model->get_data_custom($strSQL);
		if($result){
			$city_id = $result[0]['id'];
		}
		
		$post_data['id_country'] = $country_id;
		$post_data['id_region'] = $region_id;
		$post_data['id_city'] = $city_id;
		$post_data['id_user'] = $nuser;
		$post_data['id_type'] = $sale_type_id;
		
		$post_data['id_category'] = $fields['category_type'];
		$strType = "property_type_".$fields['category_type'];
		
		if($fields['category_type'] == "1")
		{
			if($fields["sale_type"] == "sale")
				$editor_id = 1;
			elseif($fields["sale_type"] == "rent")
				$editor_id = 4;
			
			if($editor_id){	
			if(!empty($fields['beds'])){
				$post_data['fe_bd_rooms_'.$editor_id] = $fields['beds'];
			}
			if(!empty($fields['baths'])){
				$post_data['fe_bth_rooms_'.$editor_id] = $fields['baths'];
			}
			if(!empty($fields['garages'])){
				$post_data['fe_garages_'.$editor_id] = $fields['garages'];
			}
			if(!empty($fields['year'])){
				$post_data['fe_year_'.$editor_id] = $fields['year'];
			}
			if(!empty($fields['floor_number'])){
				$post_data['fe_floor_number_'.$editor_id] = $fields['floor_number'];
			}
			if(!empty($fields['total_floors'])){
				$post_data['fe_total_floors_'.$editor_id] = $fields['total_floors'];
			}
			if(!empty($fields['distance_to_subway'])){
				$post_data['fe_distance_to_subway_'.$editor_id] = $fields['distance_to_subway'];
			}
			if(!empty($fields['max_square'])){
				$post_data['fe_live_square_'.$editor_id] =  $fields['max_square'];
			}
			}
		}
		
		if($fields['category_type'] == "2")
		{
			if($fields["sale_type"] == "sale")
				$editor_id = 2;
			elseif($fields["sale_type"] == "rent")
				$editor_id = 5;
		}
		
		if($fields['category_type'] == "3")
		{
			if($fields["sale_type"] == "sale")
				$editor_id = 3;
			elseif($fields["sale_type"] == "rent")
				$editor_id = 6;
		}
		
		//$post_data['fe_comments_'.$editor_id] = $fields['comments'];
		
		$arrProperty = $this->get_property_type_id($strType);
		$pId = array_search($fields["property_type"], $arrProperty);
		if(!$pId)
		{
			$pId = array_search("Other", $arrProperty);
		}
		
		$post_data['property_type'] = $pId;
		$post_data['date_open'] = '0000-00-00 00:00:00';
		$post_data['status'] = 0;
		$post_data['date_open_begin'] = '0000-00-00 00:00:00';
		$post_data['date_open_end'] = '0000-00-00 00:00:00';
		$post_data['date_available'] = '0000-00-00 00:00:00';
		$post_data['sold'] = 0;
		/*$post_data['date_created'] = '0000-00-00 00:00:00';
		$post_data['date_modified'] = '0000-00-00 00:00:00';*/
		$post_data['date_activity'] = '0000-00-00 00:00:00';
		$post_data['date_expire'] = '0000-00-00 00:00:00';
		$post_data['price_negotiated'] = 0;
		
		$cur = $this->Import_xml_listings_model->get_default_currency_symbol();
		$cur_gid = $cur["gid"];
		$post_data['gid_currency'] = $cur_gid;
		if(!empty($fields['price'])){
		$post_data['price'] = floatval($fields['price']);
		}
		/*$post_data['price_old'] = floatval(0.0000);
		$post_data['price_max'] = floatval(0.0000);
		$post_data['price_week'] = floatval(0.0000);
		$post_data['price_month'] = floatval(0.0000);
		$post_data['price_reduced'] = floatval(0.0000);
		$post_data['price_sorting'] = floatval($fields['price']);
		$post_data['price_max_sorting'] = floatval(0.0000);
		$post_data['price_period'] = 0;
		$post_data['price_type'] = 0;
		$post_data['price_negotiated'] = 0;
		$post_data['price_auction'] = 0;*/
		$post_data['address'] = $fields['address'];
		$post_data['zip'] = $fields['zip'];
		$post_data['id_district'] = 0;
		$post_data['lat'] = floatval(0.0000);
		$post_data['lon'] = floatval(0.0000);
		
		$this->load->model("listings/models/listings_model");
		$validate_data = $this->Listings_model->validate_listing($listing_id, $post_data);
		$post_data = $validate_data['data'];
		
		//$post_data['headline'] = $fields['headline'];
		$post_data['photo_count'] = $fields['photos'];
		$post_data['id_agent_property'] = $fields['id_agent_property'];
		$post_data['square'] =  $fields['square'];
		$post_data['square_unit'] =  '2';
		
		$len = $this->getLangId('en');
		$lse = $this->getLangId('se');
		$lno = $this->getLangId('no');
		$post_data['headline_lang'][$len] = $fields['headline_en'];
		$post_data['headline_lang'][$lse] = $fields['headline_se'];
		$post_data['headline_lang'][$lno] = $fields['headline_no'];
		
		$post_data['comments_lang'][$len] = $fields['comments_en'];
		$post_data['comments_lang'][$lse] = $fields['comments_se'];
		$post_data['comments_lang'][$lno] = $fields['comments_no'];
		
		$list_id = $this->Import_xml_listings_model->save_custom_listings($listing_id,$post_data);

		$tempPath = SITE_PATH.SITE_SUBFOLDER.'temp/import';
		$photos = explode("|", $fields["photoUrls"]);
		
		foreach ($photos AS $key=>$photo_name){            
            
            if (trim($photo_name) == "") continue;
            
            if (strstr($photo_name, "http")){
                $source_photo_name = $photo_name;
                $photo_name = $this->AfterLastSlash($source_photo_name);                
                if (!copy($source_photo_name,$tempPath."/".$photo_name)){
                    break;    
                }
            }
            $upload["tmp_name"] = $tempPath."/".$photo_name;
            $upload["name"] = $photo_name;
            $file_info = getimagesize($upload["tmp_name"]);
            $upload["type"] = $file_info["mime"];
			
			$this->Import_xml_listings_model->save_photo_file($list_id,$upload);
        }
		return  $list_id;
	}
	
	function getLangId($langCode){
		$languages = $this->pg_language->languages;
		$this->template_lite->assign('languages', $languages);
		$langId = '0';
		foreach($languages AS $key=>$value){
			foreach($value AS $key2=>$value2){
				if($key2 == 'code' && $value2 == $langCode){
					$langId = $value['id'];
					break;
				}
			}
		}
		return $langId;
	}
	
	function AfterLastSlash($str){
		$arr = explode("/", $str);
		return $arr[count($arr)-1];
	}
	
	function GetVarString($name, $default = "", $lenght=3000, $slashes=1, $tags=1) {

		if ( !(isset($_REQUEST[$name]) && !empty($_REQUEST[$name])) )
		return $default;
	
		$value = $_REQUEST[$name];
		$value = substr($value, 0, $lenght);
		$value = trim($value);
	
		if ($tags === 1)
		$value = strip_tags($value);
	
		if ($slashes === 1)
		$value = addslashes($value);
	
		if ($value == "") return $default;
	
		return $value;
	}
	
	function GetVarString2($name, $default = "", $lenght=3000, $slashes=1, $tags=1) {

		if ( !(isset($_REQUEST[$name]) && !empty($_REQUEST[$name])) )
		return $default;
	
		$value = $_REQUEST[$name];
		$value = substr($value, 0, $lenght);
		$value = trim($value);
	
		if ($tags === 1)
		$value = strip_tags($value);
	
		if ($slashes === 1)
		$value = addslashes($value);
	
		if ($value == "") return $default;
	
		return $value;
	}
	
	function GetAdsWithUserPropertyId($nuser, $is_zip_export = 0, $need_copy = 0){
		$export = array();
		/*$strSQL = "SELECT
					lst.id,
					lst.id_user,
					(CASE WHEN lst.id_type = 1 THEN 'Sale' WHEN lst.id_type = 3 THEN 'Rent' END) AS sale_type,
					'' AS property_type,
					lst.id_agent_property,
					lst.headline,
					lst.zip,lst.address,
					cntry.name AS country,
					reg.name AS region,
					cit.name AS city,
					lst.price,
					lst.square AS square,
					lst.square_max AS max_square
					FROM ".LISTINGS_TABLE." lst
					LEFT JOIN ".COUNTRY_TABLE." cntry ON cntry.code = lst.id_country
					LEFT JOIN ".REGION_TABLE." reg ON reg.id = lst.id_region
					LEFT JOIN ".CITY_TABLE." cit ON cit.id = lst.id_city
					WHERE lst.id_user = '".$nuser."' GROUP BY lst.id";  */
		$strSQL = "SELECT
					lst.id,
					lst.id_user,
					lst.id_agent_property
					FROM ".LISTINGS_TABLE." lst
					WHERE lst.id_user = '".$nuser."' GROUP BY lst.id";
		$tempResult = $this->Import_xml_listings_model->get_data_custom($strSQL);
		return $tempResult;
	}
	
	function IsMyAdsXml($new_ads, $all_ads){    

		$i = 0; 
		foreach ($all_ads AS $key=>$ads){ 
	
			$i = 0;        
			$flag = 1;        
			foreach ($new_ads AS $key2=>$item){
			
				if ($flag && $key2 == 'id_agent_property'){
					if ($ads[$key2] == $item){                
						$flag = 1;
					}else{
						$flag = 0;                    
					}
				}
	
			}
			if ($flag){            
				return 1;            
			}
		}    
		return 0;
	}
	//---------------------------- End listings import ----------------------------------
}

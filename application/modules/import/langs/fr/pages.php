<?php

$install_lang["admin_header_data_edit"] = "Éditer import";
$install_lang["admin_header_data_form_upload"] = "Chargement en cours";
$install_lang["admin_header_driver_edit"] = "Éditer pilote";
$install_lang["admin_header_import"] = "Importer";
$install_lang["driver_csv_field_name"] = "Nom";
$install_lang["driver_csv_field_price"] = "Prix";
$install_lang["driver_info"] = "Information";
$install_lang["driver_key_notrequired"] = "La clé n'est pas nécéssaire";
$install_lang["driver_name_csv"] = "CSV";
$install_lang["driver_name_xml"] = "XML";
$install_lang["driver_registration"] = "Obtenir clé";
$install_lang["error_data_failed"] = "Pas importé";
$install_lang["error_data_imported"] = "Importé";
$install_lang["error_empty_data"] = "Vider données pour importer";
$install_lang["error_empty_driver"] = "Pilote vide";
$install_lang["error_empty_name"] = "Nom vide";
$install_lang["error_empty_object"] = "Objet vide";
$install_lang["error_empty_relations"] = "Relations vides";
$install_lang["error_invalid_driver"] = "Pilote invalide";
$install_lang["error_invalid_module"] = "Module invalide";
$install_lang["field"] = "Champ";
$install_lang["field_csv_delimiter"] = "Délimiter";
$install_lang["field_csv_enclosure"] = "Enclos";
$install_lang["field_csv_use_first_row"] = "Utiliser première rangée";
$install_lang["field_custom_link"] = "Champ";
$install_lang["field_custom_name"] = "Nom";
$install_lang["field_custom_type"] = "Type";
$install_lang["field_data_date_created"] = "Crée";
$install_lang["field_data_driver"] = "Pilote";
$install_lang["field_data_file"] = "Filière";
$install_lang["field_data_link"] = "Champ";
$install_lang["field_data_module"] = "Module";
$install_lang["field_data_name"] = "Nom";
$install_lang["field_data_noname"] = "Aucun nom";
$install_lang["field_data_total"] = "Total";
$install_lang["field_data_type"] = "Type";
$install_lang["field_driver_link"] = "Enregistrer";
$install_lang["field_driver_name"] = "Nom";
$install_lang["field_driver_status"] = "Utilisé";
$install_lang["field_setting_name"] = "Nom";
$install_lang["field_setting_type"] = "Type";
$install_lang["field_type_file"] = "Filière";
$install_lang["field_type_int"] = "Nom entier";
$install_lang["field_type_text"] = "Texte";
$install_lang["header_data_import"] = "Importer données";
$install_lang["header_my_import"] = "Importer";
$install_lang["header_set_relations"] = "Relations entre champs";
$install_lang["link_activate_driver"] = "Activer";
$install_lang["link_delete_data"] = "Détruire";
$install_lang["link_edit_data"] = "éditer";
$install_lang["link_import_data"] = "Importer données";
$install_lang["link_import_file"] = "Importer filière";
$install_lang["link_import_form"] = "Importer formule";
$install_lang["link_make_data"] = "Importer";
$install_lang["link_update_relations"] = "éditer";
$install_lang["no_data"] = "Aucune données";
$install_lang["no_drivers"] = "Aucuns formats d'imports";
$install_lang["note_data_delete_all"] = "Êtes vous sur de vouloir détruire ces formats d'imports?";
$install_lang["note_delete_data"] = "Êtes vous sur de vouloir détruire ce format d'import?";
$install_lang["success_data_deleted"] = "Données détruites";
$install_lang["success_data_imported"] = "Données importés";
$install_lang["success_data_parsed"] = "Données analysés";
$install_lang["success_file_uploaded"] = "Filière téléchargée";
$install_lang["success_relation_updated"] = "Relation mise à jour";
$install_lang["text_make_data_count"] = "Objets importés";
$install_lang["text_set_relations"] = "Champs avec relations établies";


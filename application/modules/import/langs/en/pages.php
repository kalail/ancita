<?php

$install_lang["admin_header_data_edit"] = "Edit import";
$install_lang["admin_header_data_form_upload"] = "Loading data";
$install_lang["admin_header_driver_edit"] = "Edit driver";
$install_lang["admin_header_import"] = "Import";
$install_lang["driver_csv_field_name"] = "Name";
$install_lang["driver_csv_field_price"] = "Price";
$install_lang["driver_info"] = "Info";
$install_lang["driver_key_notrequired"] = "The key is not required";
$install_lang["driver_name_csv"] = "CSV";
$install_lang["driver_name_xml"] = "XML";
$install_lang["driver_registration"] = "Get key";
$install_lang["error_data_failed"] = "Not imported";
$install_lang["error_data_imported"] = "Imported";
$install_lang["error_empty_data"] = "Empty data for importing";
$install_lang["error_empty_driver"] = "Empty driver";
$install_lang["error_empty_name"] = "Empty name";
$install_lang["error_empty_object"] = "Empty object";
$install_lang["error_empty_relations"] = "Empty relations";
$install_lang["error_invalid_driver"] = "Invalid driver";
$install_lang["error_invalid_module"] = "Invalid module";
$install_lang["field"] = "Field";
$install_lang["field_csv_delimiter"] = "Delimiter";
$install_lang["field_csv_enclosure"] = "Enclosure";
$install_lang["field_csv_use_first_row"] = "Use first row";
$install_lang["field_custom_link"] = "Field";
$install_lang["field_custom_name"] = "Name";
$install_lang["field_custom_type"] = "Type";
$install_lang["field_data_date_created"] = "Created";
$install_lang["field_data_driver"] = "Driver";
$install_lang["field_data_file"] = "File";
$install_lang["field_data_link"] = "Field";
$install_lang["field_data_module"] = "Module";
$install_lang["field_data_name"] = "Name";
$install_lang["field_data_noname"] = "No name";
$install_lang["field_data_total"] = "Total";
$install_lang["field_data_type"] = "Type";
$install_lang["field_driver_link"] = "Register";
$install_lang["field_driver_name"] = "Name";
$install_lang["field_driver_status"] = "Used";
$install_lang["field_setting_name"] = "Name";
$install_lang["field_setting_type"] = "Type";
$install_lang["field_type_file"] = "File";
$install_lang["field_type_int"] = "Integer";
$install_lang["field_type_text"] = "Text";
$install_lang["header_data_import"] = "Data import";
$install_lang["header_my_import"] = "Import";
$install_lang["header_set_relations"] = "Relations between fields";
$install_lang["link_activate_driver"] = "Activate";
$install_lang["link_delete_data"] = "Delete";
$install_lang["link_edit_data"] = "Edit";
$install_lang["link_import_data"] = "Import data";
$install_lang["link_import_file"] = "Import file";
$install_lang["link_import_form"] = "Import form";
$install_lang["link_make_data"] = "Import";
$install_lang["link_update_relations"] = "Edit";
$install_lang["no_data"] = "No data";
$install_lang["no_drivers"] = "No import formats";
$install_lang["note_data_delete_all"] = "Are you sure you want to delete the selected import formats?";
$install_lang["note_delete_data"] = "Are you sure you want to delete the selcted import format?";
$install_lang["success_data_deleted"] = "Data is removed";
$install_lang["success_data_imported"] = "Data is successfully imported";
$install_lang["success_data_parsed"] = "Data is successfully parsed";
$install_lang["success_file_uploaded"] = "File is successfully upoloaded";
$install_lang["success_relation_updated"] = "Relation is successfully updated";
$install_lang["text_make_data_count"] = "Objects to be imported";
$install_lang["text_set_relations"] = "Fields with established relations";


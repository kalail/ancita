<?php

$install_lang["admin_import_menu_import_data_item"] = "Selections";
$install_lang["admin_import_menu_import_data_item_tooltip"] = "";
$install_lang["admin_import_menu_import_drivers_item"] = "Drivers";
$install_lang["admin_import_menu_import_drivers_item_tooltip"] = "";
$install_lang["admin_menu_exp-import-items_import_menu_item"] = "Import";
$install_lang["admin_menu_exp-import-items_import_menu_item_tooltip"] = "Import listings in CSV & XML format";


<?php

$install_lang["actions"] = "Действия";
$install_lang["admin_header_config_add"] = "Зареждане";
$install_lang["admin_header_config_change"] = "Редактиране на файл";
$install_lang["admin_header_configs_list"] = "Зареждане";
$install_lang["archives"] = "Архиви";
$install_lang["audio"] = "Аудио";
$install_lang["documents"] = "Документи";
$install_lang["error_empty_file_formats"] = "Липсва формат на файловете";
$install_lang["error_gid_invalid"] = "Грешно системно име";
$install_lang["error_title_invalid"] = "Грешно заглавие";
$install_lang["field_file_formats"] = "Формати";
$install_lang["field_gid"] = "Системно име";
$install_lang["field_max_size"] = "Max размер";
$install_lang["field_name"] = "Название";
$install_lang["field_name_format"] = "Име на файла";
$install_lang["graphics"] = "Графика";
$install_lang["images"] = "Изображения";
$install_lang["int_unlimit_condition"] = "(0 - без ограничения)";
$install_lang["link_add_config"] = "Добави настройката";
$install_lang["link_delete_config"] = "Изтрий настройката";
$install_lang["link_edit_config"] = "Редактирай настройката";
$install_lang["no_configs"] = "Няма настройки";
$install_lang["note_delete_config"] = "Сигурни ли сте, че изтривате настройките?";
$install_lang["others"] = "Други";
$install_lang["success_added_config"] = "Настройката е добавена";
$install_lang["success_deleted_config"] = "Настройката е изтрита";
$install_lang["success_updated_config"] = "Настройката е запазена";
$install_lang["test"] = "Тест";
$install_lang["video"] = "Видео";


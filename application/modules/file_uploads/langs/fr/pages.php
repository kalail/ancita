<?php

$install_lang["actions"] = "actions";
$install_lang["admin_header_config_add"] = "Téléchargement de fichiers";
$install_lang["admin_header_config_change"] = "Modifier fichier";
$install_lang["admin_header_configs_list"] = "Télécharger fichier";
$install_lang["archives"] = "archives";
$install_lang["audio"] = "Son";
$install_lang["documents"] = "documents";
$install_lang["error_empty_file_formats"] = "Formats de fichiers vides";
$install_lang["error_gid_invalid"] = "mot-clé non valide";
$install_lang["error_title_invalid"] = "titre invalide";
$install_lang["field_file_formats"] = "formats";
$install_lang["field_gid"] = "Réglage mot-clé";
$install_lang["field_max_size"] = "taille max";
$install_lang["field_name"] = "nom du paramètre";
$install_lang["field_name_format"] = "Nom du fichier";
$install_lang["graphics"] = "graphiques";
$install_lang["images"] = "images";
$install_lang["int_unlimit_condition"] = "(0 - illimité)";
$install_lang["link_add_config"] = "Ajouter réglage";
$install_lang["link_delete_config"] = "supprimer réglage";
$install_lang["link_edit_config"] = "Modifier réglage";
$install_lang["no_configs"] = "Pas de réglages";
$install_lang["note_delete_config"] = "Etes-vous sûr de vouloir effacer le réglage?";
$install_lang["others"] = "autres";
$install_lang["success_added_config"] = "Réglage de téléchargement de fichiers ajouté";
$install_lang["success_deleted_config"] = "Réglage de téléchargement de fichiers supprimé";
$install_lang["success_updated_config"] = "Réglage de téléchargement de fichiers sauvegardé";
$install_lang["test"] = "test";
$install_lang["video"] = "vidéo";


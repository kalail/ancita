<?php

$install_lang["actions"] = "Acciones";
$install_lang["admin_header_config_add"] = "Cargar archivos";
$install_lang["admin_header_config_change"] = "Editar archivo";
$install_lang["admin_header_configs_list"] = "Cargar archivos";
$install_lang["archives"] = "Archivos";
$install_lang["audio"] = "Audio";
$install_lang["documents"] = "Documentos";
$install_lang["error_empty_file_formats"] = "Formato de archivo vacío";
$install_lang["error_gid_invalid"] = "Palabra clave inválida";
$install_lang["error_title_invalid"] = "Título inválido";
$install_lang["field_file_formats"] = "Formatos";
$install_lang["field_gid"] = "Configuración de palabras clave";
$install_lang["field_max_size"] = "Tamaño máx";
$install_lang["field_name"] = "Nombre de la configuración";
$install_lang["field_name_format"] = "Nombre de archivo";
$install_lang["graphics"] = "Gráficos";
$install_lang["images"] = "Imágenes";
$install_lang["int_unlimit_condition"] = "(0-ilimitado)";
$install_lang["link_add_config"] = "Añadir ajuste";
$install_lang["link_delete_config"] = "Borrar configuración";
$install_lang["link_edit_config"] = "Editar ajustes";
$install_lang["no_configs"] = "No hay ajustes";
$install_lang["note_delete_config"] = "¿Está seguro que desea eliminar la configuración?";
$install_lang["others"] = "Otros";
$install_lang["success_added_config"] = "Configuración de archivos cargados agregada correctamente";
$install_lang["success_deleted_config"] = "Configuración de archivos cargados borrada";
$install_lang["success_updated_config"] = "Configuración de archivos cargados guardada correctamente";
$install_lang["test"] = "Prueba";
$install_lang["video"] = "Vídeo";


<?php

$install_lang["actions"] = "Aktionen";
$install_lang["admin_header_config_add"] = "Datei hochladen";
$install_lang["admin_header_config_change"] = "Datei bearbeiten";
$install_lang["admin_header_configs_list"] = "Dateien hochladen";
$install_lang["archives"] = "Archiv";
$install_lang["audio"] = "Audio";
$install_lang["documents"] = "Dokumente";
$install_lang["error_empty_file_formats"] = "Dateienformate leeren";
$install_lang["error_gid_invalid"] = "ungültiges Keywort";
$install_lang["error_title_invalid"] = "ungültiger Titel";
$install_lang["field_file_formats"] = "Formate";
$install_lang["field_gid"] = "Einstellungen Keywort";
$install_lang["field_max_size"] = "max. Größe";
$install_lang["field_name"] = "Einstellungsname";
$install_lang["field_name_format"] = "Dateiname";
$install_lang["graphics"] = "Grafik";
$install_lang["images"] = "Bilder";
$install_lang["int_unlimit_condition"] = "(0 – unbegrenzt)";
$install_lang["link_add_config"] = "Einstellung hinzufügen";
$install_lang["link_delete_config"] = "Einstellung löschen";
$install_lang["link_edit_config"] = "Einstellung bearbeiten";
$install_lang["no_configs"] = "Keine Einstellungen";
$install_lang["note_delete_config"] = "Sind Sie sicher, dass Sie die Einstellungen löschen wollen?";
$install_lang["others"] = "Andere";
$install_lang["success_added_config"] = "hochgeladene Dateien Einstellung sind erfolgreich hinzugefügt";
$install_lang["success_deleted_config"] = "hochgeladene Dateien Einstellung ist erfolgreich gelöscht";
$install_lang["success_updated_config"] = "hochgeladene Dateien Einstellung ist erfolgreich gespeichert";
$install_lang["test"] = "test";
$install_lang["video"] = "Video";


<?php

$install_lang["map_xml_frequency"]["header"] = "Fréquence";
$install_lang["map_xml_frequency"]["option"]["always"] = "Toujours";
$install_lang["map_xml_frequency"]["option"]["hourly"] = "Chaque heure";
$install_lang["map_xml_frequency"]["option"]["daily"] = "Chaque jour";
$install_lang["map_xml_frequency"]["option"]["weekly"] = "Chaque semaine";
$install_lang["map_xml_frequency"]["option"]["monthly"] = "Chaque mois";
$install_lang["map_xml_frequency"]["option"]["yearly"] = "Chaque année";
$install_lang["map_xml_frequency"]["option"]["never"] = "Jamais";



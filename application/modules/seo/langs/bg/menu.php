<?php

$install_lang["admin_menu_settings_items_system-items_seo_menu_item"] = "SEO настройки";
$install_lang["admin_menu_settings_items_system-items_seo_menu_item_tooltip"] = "Мета тагове, аналитикс, трекер и файл robots.txt";
$install_lang["admin_seo_menu_seo_analytics"] = "Seo аналитикс";
$install_lang["admin_seo_menu_seo_analytics_tooltip"] = "";
$install_lang["admin_seo_menu_seo_default_list_item"] = "Основни настройки";
$install_lang["admin_seo_menu_seo_default_list_item_tooltip"] = "";
$install_lang["admin_seo_menu_seo_list_item"] = "Настройки за модула";
$install_lang["admin_seo_menu_seo_list_item_tooltip"] = "";
$install_lang["admin_seo_menu_seo_robots"] = "Robots.txt";
$install_lang["admin_seo_menu_seo_robots_tooltip"] = "";
$install_lang["admin_seo_menu_seo_tracker"] = "Трекер";
$install_lang["admin_seo_menu_seo_tracker_tooltip"] = "";


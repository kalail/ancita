<?php

$install_lang["map_xml_frequency"]["header"] = "Регулярност";
$install_lang["map_xml_frequency"]["option"]["always"] = "Постоянно";
$install_lang["map_xml_frequency"]["option"]["hourly"] = "Всеки час";
$install_lang["map_xml_frequency"]["option"]["daily"] = "Всеки ден";
$install_lang["map_xml_frequency"]["option"]["weekly"] = "Всяка седмица";
$install_lang["map_xml_frequency"]["option"]["monthly"] = "Всеки месец";
$install_lang["map_xml_frequency"]["option"]["yearly"] = "Всяка година";
$install_lang["map_xml_frequency"]["option"]["never"] = "Никога";



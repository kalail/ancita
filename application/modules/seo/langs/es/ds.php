<?php

$install_lang["map_xml_frequency"]["header"] = "Frecuencia";
$install_lang["map_xml_frequency"]["option"]["always"] = "Siempre";
$install_lang["map_xml_frequency"]["option"]["hourly"] = "Cada hora";
$install_lang["map_xml_frequency"]["option"]["daily"] = "Diario";
$install_lang["map_xml_frequency"]["option"]["weekly"] = "Semanal";
$install_lang["map_xml_frequency"]["option"]["monthly"] = "Mensual";
$install_lang["map_xml_frequency"]["option"]["yearly"] = "Anual";
$install_lang["map_xml_frequency"]["option"]["never"] = "Nunca";



<?php

$install_lang["admin_menu_settings_items_system-items_seo_menu_item"] = "Ajustes SEO";
$install_lang["admin_menu_settings_items_system-items_seo_menu_item_tooltip"] = "Meta-tags, análisis, tracker y robots.txt";
$install_lang["admin_seo_menu_seo_analytics"] = "SEO analytics";
$install_lang["admin_seo_menu_seo_analytics_tooltip"] = "";
$install_lang["admin_seo_menu_seo_default_list_item"] = "Configuración global";
$install_lang["admin_seo_menu_seo_default_list_item_tooltip"] = "";
$install_lang["admin_seo_menu_seo_list_item"] = "Configuración del módulo";
$install_lang["admin_seo_menu_seo_list_item_tooltip"] = "";
$install_lang["admin_seo_menu_seo_robots"] = "Robots.txt";
$install_lang["admin_seo_menu_seo_robots_tooltip"] = "";
$install_lang["admin_seo_menu_seo_tracker"] = "Tracker";
$install_lang["admin_seo_menu_seo_tracker_tooltip"] = "";


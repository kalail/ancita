<?php

$install_lang["admin_menu_settings_items_system-items_seo_menu_item"] = "SEO Einstellungen";
$install_lang["admin_menu_settings_items_system-items_seo_menu_item_tooltip"] = "Meta Tags, Analytik, Tracker & robots.txt";
$install_lang["admin_seo_menu_seo_analytics"] = "Seo Analytik";
$install_lang["admin_seo_menu_seo_analytics_tooltip"] = "";
$install_lang["admin_seo_menu_seo_default_list_item"] = "Globale Einstellungen";
$install_lang["admin_seo_menu_seo_default_list_item_tooltip"] = "";
$install_lang["admin_seo_menu_seo_list_item"] = "Moduleinstellungen";
$install_lang["admin_seo_menu_seo_list_item_tooltip"] = "";
$install_lang["admin_seo_menu_seo_robots"] = "Robots.txt";
$install_lang["admin_seo_menu_seo_robots_tooltip"] = "";
$install_lang["admin_seo_menu_seo_tracker"] = "Tracker";
$install_lang["admin_seo_menu_seo_tracker_tooltip"] = "";


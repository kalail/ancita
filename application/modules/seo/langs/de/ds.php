<?php

$install_lang["map_xml_frequency"]["header"] = "Frequenz";
$install_lang["map_xml_frequency"]["option"]["always"] = "immer";
$install_lang["map_xml_frequency"]["option"]["hourly"] = "stündlich";
$install_lang["map_xml_frequency"]["option"]["daily"] = "täglich";
$install_lang["map_xml_frequency"]["option"]["weekly"] = "wöchentlich";
$install_lang["map_xml_frequency"]["option"]["monthly"] = "monatlich";
$install_lang["map_xml_frequency"]["option"]["yearly"] = "jährlich";
$install_lang["map_xml_frequency"]["option"]["never"] = "niemals";



<?php  

if (!defined('BASEPATH')) exit('No direct script access allowed');

define('GEOMAPS_DRIVERS_TABLE', DB_PREFIX.'geomap_drivers');

/**
 * Geomaps main model
 * 
 * @package PG_RealEstate
 * @subpackage Geomap
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Geomap_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Fields of geomap in data source
	 * 
	 * @var array
	 */
	private $attrs = array(
		'id', 
		'gid', 
		'regkey', 
		'need_regkey', 
		'link', 
		'date_add', 
		'date_modified', 
		'status',
	);
	
	/**
	 * Cache of map drivers
	 * 
	 * @var array
	 */
	private $driver_cache = array();

	/**
	 * Map driver by default
	 * 
	 * @var string
	 */
	private $default_driver_gid = '';

	/**
	 * View settings by default
	 * 
	 * @var array
	 */
	private $default_view_settings = array(
		"width" => "500",
		"height" => "300", 
		"class" => "", 
		'zoom_listener' => "", 
		'type_listener' => "",
		'drag_listener' => "",
		'lang' => "en",
	);
	
	/**
	 * Javascript libriry is loaded
	 *
	 * @var boolean 
	 */
	public static $geomap_map_type_js_loaded = false;
	
	/**
	 * Constructor
	 *
	 * @return Geomap_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;

		$this->get_default_driver();
	}

	/**
	 * Return driver object by GUID
	 * 
	 * @param string $gid map GUID
	 * @return array
	 */
	public function get_driver_by_gid($gid){
		if(empty($this->driver_cache[$gid])){
			$this->DB->select(implode(", ", $this->attrs))->from(GEOMAPS_DRIVERS_TABLE)->where("gid", $gid);
			$results = $this->DB->get()->result_array();
			if(!empty($results) && is_array($results)){
				$this->driver_cache[$gid] = $this->format_driver($results[0]);
			}
		}
		return $this->driver_cache[$gid];
	}

	/**
	 * Return driver object by default
	 * 
	 * @return array
	 */
	public function get_default_driver(){
		if(!empty($this->default_driver_gid)){
			return $this->get_driver_by_gid($this->default_driver_gid);
		}

		$this->DB->select(implode(", ", $this->attrs))->from(GEOMAPS_DRIVERS_TABLE)->where("status", "1")->limit(1);
		$results = $this->DB->get()->result_array();
		
		if(!empty($results) && is_array($results)){
			$this->default_driver_gid = $results[0]["gid"];
			$this->driver_cache[$this->default_driver_gid] = $this->format_driver($results[0]);
			return $this->driver_cache[$this->default_driver_gid];
		}
		return array();
	}
	
	/**
	 * Return driver GUID by default
	 * 
	 * @return string
	 */
	public function get_default_driver_gid(){
		return $this->default_driver_gid;
	}
	
	/**
	 * Format driver object
	 * 
	 * @param array $data driver data
	 */
	public function format_driver($data){
		$data["name"] = l('driver_name_'.$data["gid"], 'geomap');
		return $data;
	}

	/**
	 * Set driver object as default
	 * 
	 * @param string $gid driver GUID
	 * @return void
	 */
	public function set_default_driver($gid){
		$this->driver_cache = array();
		$this->default_driver_gid = $gid;

		$data["status"] = 0;
		$this->DB->update(GEOMAPS_DRIVERS_TABLE, $data);

		$data["status"] = 1;
		$this->DB->where('gid', $gid);
		$this->DB->update(GEOMAPS_DRIVERS_TABLE, $data);
		return;
	}

	/**
	 * Return all drivers objects
	 * 
	 * @return array
	 */
	public function get_drivers(){
		$data = array();
		$this->DB->select(implode(", ", $this->attrs))->from(GEOMAPS_DRIVERS_TABLE);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $result){
				$data[] = $this->format_driver($result);
			}
		}
		return $data;
	}

	/**
	 * Save driver data
	 * 
	 * @param string $gid driver GUID
	 * @param array $data driver data
	 * @return void
	 */
	public function set_driver($gid, $data){
		if (is_null($gid)){
			$data["date_add"] = $data["date_modified"] = date("Y-m-d H:i:s");
			if(!isset($data["status"])) $data["status"] = 0;
			$this->DB->insert(GEOMAPS_DRIVERS_TABLE, $data);
		}else{
			$data["date_modified"] = date("Y-m-d H:i:s");
			$this->DB->where('gid', $gid);
			$this->DB->update(GEOMAPS_DRIVERS_TABLE, $data);
			unset($this->driver_cache[$gid]);
		}
		return;
	}

	/**
	 * Validate driver data
	 * 
	 * @param string $gid driver GUID
	 * @param array $data driver data
	 * @return array
	 */
	public function validate_driver($gid, $data){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data["regkey"])){
			$return["data"]["regkey"] = trim(strip_tags($data["regkey"]));
		}

		return $return;
	}

	/**
	 * Remove driver object from data source
	 * 
	 * @param integer $id driver identifier
	 * @return void
	 */
	public function delete_driver($id){
	
	}

	/**
	 * Create map object using driver by default
	 * 
	 * @param $id_user user identifier
	 * @param $id_object linked object identifier
	 * @param string $gid object GUID
	 * @param array $markers merkers data
	 * @param array $view_settings view settings
	 * @param integer $only_load_scripts only javascripts loaded
	 * @param integer $only_load_content only content loaded
	 * @param string $map_id map identifier
	 * @return string
	 */
	public function create_default_map($id_user=0, $id_object=0, $gid="", $markers=array(), $view_settings=array(), $only_load_scripts=false, $only_load_content=false, $map_id=false){
		return $this->create_map($this->default_driver_gid, $id_user, $id_object, $gid, $markers, $view_settings, $only_load_scripts, $only_load_content, $map_id);
	}
	
	/**
	 * Create map object of driver
	 * 
	 * @param string $map_type driver GUID
	 * @param $id_user user identifier
	 * @param $id_object linked object identifier
	 * @param string $gid driver GUID
	 * @param array $markers merkers data
	 * @param array $view_settings view settings
	 * @param integer $only_load_scripts only javascripts loaded
	 * @param integer $only_load_content only content loaded
	 * @param string $map_id map identifier
	 * @return string
	 */
	public function create_map($map_type, $id_user=0, $id_object=0, $gid="", $markers=array(), $view_settings=array(), $only_load_scripts=false, $only_load_content=false, $map_id=false){
		$this->CI->load->model("geomap/models/Geomap_settings_model");
		$driver_settings = $this->get_driver_by_gid($map_type);
		if(empty($driver_settings) /*|| ($driver_settings["need_regkey"] && empty($driver_settings["regkey"]))*/){
			return "";
		}
		
		$this->CI->template_lite->assign('geomap_js_loaded', $this->geomap_map_type_js_loaded);
		$this->geomap_map_type_js_loaded = true;
		
		$this->CI->template_lite->assign('only_load_scripts', $only_load_scripts);
		$this->CI->template_lite->assign('only_load_content', $only_load_content);
		
		$settings = $this->CI->Geomap_settings_model->get_parsed_settings($map_type, $id_user, $id_object, $gid);
		if(empty($view_settings)){
			$view_settings = $this->default_view_settings;
		}
		$view_settings["rand"] = rand(10000, 99999);
	
		$model_name = ucfirst(strtolower($map_type))."_model";
		$this->CI->load->model("geomap/models/".$model_name, $model_name);
	
		return $this->CI->$model_name->create_html($driver_settings["regkey"], $settings, $view_settings, $markers, $map_id);
	}
	
	/**
	 * Update map object of driver by default
	 * 
	 * @param string $map_id map identifier
	 * @param array $markers markers data
	 * @return string
	 */
	public function update_default_map($map_id, $markers=array()){
		return $this->update_map($this->default_driver_gid, $map_id, $markers);
	}
	
	/**
	 * Update map object of driver
	 * 
	 * @param string $map_type driver GUID
	 * @param string $map_id map identifier
	 * @param array $markers markers data
	 * @return string
	 */
	public function update_map($map_type, $map_id, $markers=array()){
		$driver_settings = $this->get_driver_by_gid($map_type);
		if(empty($driver_settings) /*|| ($driver_settings["need_regkey"] && empty($driver_settings["regkey"]))*/){
			return "";
		}
		
		$model_name = ucfirst(strtolower($map_type))."_model";
		$this->CI->load->model("geomap/models/".$model_name, $model_name);
	
		return $this->CI->$model_name->update_html($map_id, $markers);
	}

	/**
	 * Render example map object
	 * 
	 * @param array $params block parameters
	 * @param string $view view mode
	 * @return string
	 */
	public function _dynamic_block_map_example($params, $view=''){
		$this->CI->load->helper('geomap');
		return geomap_map_example($params["lat"], $params["lon"], $params["text"]);
	}
	
	/**
	 * Create feocoder object of driver by default
	 * 
	 * @return string
	 */
	public function create_default_geocoder(){
		return $this->create_geocoder($this->default_driver_gid);
	}
	
	/**
	 * Create feocoder object of driver
	 * 
	 * @param string $map_type driver GUID
	 * @return string
	 */
	public function create_geocoder($map_type){
		$driver_settings = $this->get_driver_by_gid($map_type);
		if(empty($driver_settings) /*|| ($driver_settings["need_regkey"] && empty($driver_settings["regkey"]))*/){
			return "";
		}
		
		static $geomap_geocoder_js_loaded = false;
		if($geomap_geocoder_js_loaded) return '';
		$geomap_geocoder_js_loaded = true;
		
		$this->CI->template_lite->assign('geomap_js_loaded', $this->geomap_map_type_js_loaded);
		$this->geomap_map_type_js_loaded = true;
		
		$model_name = ucfirst(strtolower($map_type))."_model";
		$this->CI->load->model("geomap/models/".$model_name, $model_name);
	
		return $this->CI->$model_name->create_geocoder($driver_settings["regkey"]);
	}
	
	/**
	 * Create street view object of driver by default
	 * 
	 * @return string
	 */
	public function create_default_street_view(){
		return $this->create_street_view($this->default_driver_gid);
	}
	
	/**
	 * Create street view object of driver
	 * 
	 * @param string $map_type driver GUID
	 * @return string
	 */
	public function create_street_view($map_type){
		$driver_settings = $this->get_driver_by_gid($map_type);
		if(empty($driver_settings) /*|| ($driver_settings["need_regkey"] && empty($driver_settings["regkey"]))*/){
			return "";
		}
		
		static $geomap_street_view_js_loaded = false;
		if($geomap_street_view_js_loaded) return '';
		$geomap_street_view_js_loaded = true;
		
		$this->CI->template_lite->assign('geomap_js_loaded', $this->geomap_map_type_js_loaded);
		$this->geomap_map_type_js_loaded = true;
		
		$model_name = ucfirst(strtolower($map_type))."_model";
		$this->CI->load->model("geomap/models/".$model_name, $model_name);
	
		return $this->CI->$model_name->create_street_view($driver_settings["regkey"]);
	}
}

<?php  

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Bing maps driver model
 * 
 * version 7
 * 
 * @package PG_RealEstate
 * @subpackage Geomap
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Bingmapsv7_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;

	/**
	 * Constructor
	 *
	 * @return Bingmapsv7_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Create map code
	 * 
	 * @param string $key map driver key
	 * @param array $settings map driver settings
	 * @param array $view_settings view settings
	 * $param array $markers markers data
	 * @param string $map_id map identifier
	 * @return string
	 */
	public function create_html($key, $settings, $view_settings, $markers=array(), $map_id=false){
		$this->CI->template_lite->assign('map_reg_key', $key);
		$this->CI->template_lite->assign('settings', $settings);
		$this->CI->template_lite->assign('view_settings', $view_settings);
		$this->CI->template_lite->assign('markers', $markers);
		$this->CI->template_lite->assign('map_id', $map_id);
		$this->CI->template_lite->assign('rand', rand(100000, 999999));
		return $this->CI->template_lite->fetch('bingmapsv7_html', 'user', 'geomap');
	}
	
	/**
	 * Update map object
	 * 
	 * @param string $map_id map identifier
	 * @param array $markers markers data
	 * @return string
	 */
	public function update_html($map_id, $markers=array()){
		$this->CI->template_lite->assign('map_id', $map_id);
		$this->CI->template_lite->assign('markers', $markers);
		return $this->CI->template_lite->fetch('bingmapsv7_update', 'user', 'geomap');
	}
	
	/**
	 * Append geocode object
	 * 
	 * @param string $key map driver key
	 * @return string
	 */
	public function create_geocoder($key){
		return $this->CI->template_lite->fetch('bingmapsv7_geocoder', 'user', 'geomap');
	}
	
	/**
	 * Append street view to map
	 * 
	 * @param string $key map driver key
	 * @return string
	 */
	public function create_street_view($key){
		return null;
	}
}

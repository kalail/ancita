<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Google maps driver model
 * 
 * version 3
 * 
 * @package PG_RealEstate
 * @subpackage Geomap
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Googlemapsv3_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Constructor
	 *
	 * @return Googlemapsv3_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Return code of map object
	 * 
	 * @param string $key driver key
	 * @param array $settings settings ofmap object
	 * @param array $view_settings view settings
	 * @param array $markers markers data
	 * @param integer $map_id map identifier
	 * @return string
	 */
	public function create_html($key, $settings, $view_settings, $markers=array(), $map_id=false){
		$amenities = $this->CI->pg_language->ds->get_reference(
			'geomap', 
			'amenities_googlemapsv3', 
			$this->CI->pg_language->current_lang_id
		);
		$amenities_names = array();
		foreach($amenities['option'] as $amenity_code=>$amenity_name){
			$amenities_names[] = "'".str_replace("'", "\'", $amenity_code)."':'".str_replace("'", "\'", $amenity_name)."'";
		}
		$this->CI->template_lite->assign('amenities_names_str', '{'.implode(',', $amenities_names).'}');
		$this->CI->template_lite->assign('map_reg_key', $key);
		$this->CI->template_lite->assign('settings', $settings);
		$this->CI->template_lite->assign('view_settings', $view_settings);
		$this->CI->template_lite->assign('markers', $markers);
		$this->CI->template_lite->assign('map_id', $map_id);
		$this->CI->template_lite->assign('rand', rand(100000, 999999));
		return $this->CI->template_lite->fetch('googlemapsv3_html', 'user', 'geomap');
	}
	
	/**
	 * Return code for updating map object
	 * 
	 * @param integer $map_id map identifier
	 * @param array $markers markers data
	 * @return string
	 */
	public function update_html($map_id, $markers=array()){
		$this->CI->template_lite->assign('map_id', $map_id);
		$this->CI->template_lite->assign('markers', $markers);
		return $this->CI->template_lite->fetch('googlemapsv3_update', 'user', 'geomap');
	}
	
	/**
	 * Return code of geocoder object
	 * 
	 * @param string $key driver key
	 * @return string
	 */
	public function create_geocoder($key){
		return $this->CI->template_lite->fetch('googlemapsv3_geocoder', 'user', 'geomap');
	}
	
	/**
	 * Return code for appending street view to map
	 * 
	 * @param string $key driver key
	 * @return string
	 */
	public function create_street_view($key){
		return $this->CI->template_lite->fetch('googlemapsv3_street_view', 'user', 'geomap');
	}
}

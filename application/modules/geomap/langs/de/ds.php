<?php

$install_lang["geomap_type_googlemaps"]["header"] = "Kartentyp (Google v2)";
$install_lang["geomap_type_googlemaps"]["option"]["1"] = "Normal";
$install_lang["geomap_type_googlemaps"]["option"]["2"] = "Satellit";
$install_lang["geomap_type_googlemaps"]["option"]["3"] = "Hybrid";
$install_lang["geomap_type_googlemaps"]["option"]["4"] = "Terrian";

$install_lang["geomap_type_googlemapsv3"]["header"] = "Kartentyp (Google v3)";
$install_lang["geomap_type_googlemapsv3"]["option"]["1"] = "Straßenkarte";
$install_lang["geomap_type_googlemapsv3"]["option"]["2"] = "Satellit";
$install_lang["geomap_type_googlemapsv3"]["option"]["3"] = "Hybrid";
$install_lang["geomap_type_googlemapsv3"]["option"]["4"] = "Terrian";

$install_lang["geomap_type_yandexmaps"]["header"] = "Kartentyp (Yandex v1)";
$install_lang["geomap_type_yandexmaps"]["option"]["1"] = "Skizze";
$install_lang["geomap_type_yandexmaps"]["option"]["2"] = "Satellit";
$install_lang["geomap_type_yandexmaps"]["option"]["3"] = "Hybrid";

$install_lang["geomap_type_yandexmapsv2"]["header"] = "Kartentyp (Yandex v2)";
$install_lang["geomap_type_yandexmapsv2"]["option"]["1"] = "Skizze";
$install_lang["geomap_type_yandexmapsv2"]["option"]["2"] = "Satellit";
$install_lang["geomap_type_yandexmapsv2"]["option"]["3"] = "Hybrid";
$install_lang["geomap_type_yandexmapsv2"]["option"]["4"] = "Öffentlichkeit";
$install_lang["geomap_type_yandexmapsv2"]["option"]["5"] = "öffentlicher Hybrid";

$install_lang["geomap_type_bingmapsv7"]["header"] = "Kartenty (Bing v7)";
$install_lang["geomap_type_bingmapsv7"]["option"]["1"] = "Antenne";
$install_lang["geomap_type_bingmapsv7"]["option"]["2"] = "Auto";
$install_lang["geomap_type_bingmapsv7"]["option"]["3"] = "Vogelaugen";
$install_lang["geomap_type_bingmapsv7"]["option"]["4"] = "Collin's Bart";
$install_lang["geomap_type_bingmapsv7"]["option"]["5"] = "Merkator";
$install_lang["geomap_type_bingmapsv7"]["option"]["6"] = "Landesvermessungsamt";
$install_lang["geomap_type_bingmapsv7"]["option"]["7"] = "Straße";

$install_lang["amenities_googlemapsv3"]["header"] = "Annehmlichkeiten";
$install_lang["amenities_googlemapsv3"]["option"]["accounting"] = "Bilanzen";
$install_lang["amenities_googlemapsv3"]["option"]["amusement_park"] = "Vergnügungspark";
$install_lang["amenities_googlemapsv3"]["option"]["aquarium"] = "Aquarium";
$install_lang["amenities_googlemapsv3"]["option"]["art_gallery"] = "Kunstgallerie";
$install_lang["amenities_googlemapsv3"]["option"]["atm"] = "Geldautomat";
$install_lang["amenities_googlemapsv3"]["option"]["bakery"] = "Bäckerei";
$install_lang["amenities_googlemapsv3"]["option"]["bank"] = "Bank";
$install_lang["amenities_googlemapsv3"]["option"]["bar"] = "Bar";
$install_lang["amenities_googlemapsv3"]["option"]["beauty_salon"] = "Schönheitssalon";
$install_lang["amenities_googlemapsv3"]["option"]["bicycle_store"] = "Fahrradgeschäft";
$install_lang["amenities_googlemapsv3"]["option"]["book_store"] = "Buchhandlung";
$install_lang["amenities_googlemapsv3"]["option"]["bowling_alley"] = "Kegelbahn";
$install_lang["amenities_googlemapsv3"]["option"]["bus_station"] = "Bushaltestelle";
$install_lang["amenities_googlemapsv3"]["option"]["cafe"] = "Cafe";
$install_lang["amenities_googlemapsv3"]["option"]["campground"] = "Campingplatz";
$install_lang["amenities_googlemapsv3"]["option"]["car_dealer"] = "Autohändler";
$install_lang["amenities_googlemapsv3"]["option"]["car_rental"] = "Autovermietung";
$install_lang["amenities_googlemapsv3"]["option"]["car_repair"] = "Autowerkstatt";
$install_lang["amenities_googlemapsv3"]["option"]["car_wash"] = "Autowäsche";
$install_lang["amenities_googlemapsv3"]["option"]["casino"] = "Kasino";
$install_lang["amenities_googlemapsv3"]["option"]["cemetery"] = "Friedhof";
$install_lang["amenities_googlemapsv3"]["option"]["church"] = "Kirche";
$install_lang["amenities_googlemapsv3"]["option"]["city_hall"] = "Rathaus";
$install_lang["amenities_googlemapsv3"]["option"]["clothing_store"] = "Kleidungsgeschäscht";
$install_lang["amenities_googlemapsv3"]["option"]["convenience_store"] = "Lebensmittlelladen";
$install_lang["amenities_googlemapsv3"]["option"]["courthouse"] = "Gerichtsgebäude";
$install_lang["amenities_googlemapsv3"]["option"]["dentist"] = "Zahnarzt";
$install_lang["amenities_googlemapsv3"]["option"]["department_store"] = "Kaufhaus";
$install_lang["amenities_googlemapsv3"]["option"]["doctor"] = "Arzt";
$install_lang["amenities_googlemapsv3"]["option"]["electrician"] = "Elektriker";
$install_lang["amenities_googlemapsv3"]["option"]["electronics_store"] = "Elektronikgeschäft";
$install_lang["amenities_googlemapsv3"]["option"]["embassy"] = "Botschaft";
$install_lang["amenities_googlemapsv3"]["option"]["establishment"] = "Austellung";
$install_lang["amenities_googlemapsv3"]["option"]["finance"] = "Finzanzen";
$install_lang["amenities_googlemapsv3"]["option"]["fire_station"] = "Feuerwehr";
$install_lang["amenities_googlemapsv3"]["option"]["florist"] = "Blumenhändler";
$install_lang["amenities_googlemapsv3"]["option"]["food"] = "Lebensmittel";
$install_lang["amenities_googlemapsv3"]["option"]["funeral_home"] = "Beerdigungsinstitut";
$install_lang["amenities_googlemapsv3"]["option"]["furniture_store"] = "Möbelhaus";
$install_lang["amenities_googlemapsv3"]["option"]["gas_station"] = "Tankstselle";
$install_lang["amenities_googlemapsv3"]["option"]["general_contractor"] = "Generalunternehmer";
$install_lang["amenities_googlemapsv3"]["option"]["grocery_or_supermarket"] = "Lebensmittelgeschäft oder Supermarkt";
$install_lang["amenities_googlemapsv3"]["option"]["gym"] = "Fitnissstudio";
$install_lang["amenities_googlemapsv3"]["option"]["hair_care"] = "Friseur";
$install_lang["amenities_googlemapsv3"]["option"]["hardware_store"] = "Eisenwarengeschäft";
$install_lang["amenities_googlemapsv3"]["option"]["health"] = "Gesundheit";
$install_lang["amenities_googlemapsv3"]["option"]["hindu_temple"] = "Hindu Tempel";
$install_lang["amenities_googlemapsv3"]["option"]["home_goods_store"] = "Warenhaus";
$install_lang["amenities_googlemapsv3"]["option"]["hospital"] = "Krankenhaus";
$install_lang["amenities_googlemapsv3"]["option"]["insurance_agency"] = "Versicherungsagentur";
$install_lang["amenities_googlemapsv3"]["option"]["jewelry_store"] = "Schmuckgeschäft";
$install_lang["amenities_googlemapsv3"]["option"]["laundry"] = "Wäscherei";
$install_lang["amenities_googlemapsv3"]["option"]["lawyer"] = "Anwalt";
$install_lang["amenities_googlemapsv3"]["option"]["library"] = "Bibliothek";
$install_lang["amenities_googlemapsv3"]["option"]["liquor_store"] = "Spirituosengeschäft";
$install_lang["amenities_googlemapsv3"]["option"]["local_government_office"] = "Lokales Regierungsbüro";
$install_lang["amenities_googlemapsv3"]["option"]["locksmith"] = "Schlosser";
$install_lang["amenities_googlemapsv3"]["option"]["lodging"] = "Unterkunft";
$install_lang["amenities_googlemapsv3"]["option"]["meal_delivery"] = "Lieferdienst von Speisen";
$install_lang["amenities_googlemapsv3"]["option"]["meal_takeaway"] = "Speisen zum Mitnehmen";
$install_lang["amenities_googlemapsv3"]["option"]["mosque"] = "Moschee";
$install_lang["amenities_googlemapsv3"]["option"]["movie_rental"] = "Filmverleih";
$install_lang["amenities_googlemapsv3"]["option"]["movie_theater"] = "Kino";
$install_lang["amenities_googlemapsv3"]["option"]["moving_company"] = "Umzugsfirma";
$install_lang["amenities_googlemapsv3"]["option"]["museum"] = "Museum";
$install_lang["amenities_googlemapsv3"]["option"]["night_club"] = "Nachtclub";
$install_lang["amenities_googlemapsv3"]["option"]["painter"] = "Maler";
$install_lang["amenities_googlemapsv3"]["option"]["park"] = "Park";
$install_lang["amenities_googlemapsv3"]["option"]["parking"] = "Parkplatz";
$install_lang["amenities_googlemapsv3"]["option"]["pet_store"] = "Tiergeschäft";
$install_lang["amenities_googlemapsv3"]["option"]["pharmacy"] = "Apotheke";
$install_lang["amenities_googlemapsv3"]["option"]["physiotherapist"] = "Physiotherapeut";
$install_lang["amenities_googlemapsv3"]["option"]["place_of_worship"] = "Andachtsort";
$install_lang["amenities_googlemapsv3"]["option"]["plumber"] = "Klempner";
$install_lang["amenities_googlemapsv3"]["option"]["police"] = "Polizei";
$install_lang["amenities_googlemapsv3"]["option"]["post_office"] = "Postamt";
$install_lang["amenities_googlemapsv3"]["option"]["real_estate_agency"] = "Immobilienagentur";
$install_lang["amenities_googlemapsv3"]["option"]["restaurant"] = "Restaurant";
$install_lang["amenities_googlemapsv3"]["option"]["roofing_contractor"] = "Dachdecker";
$install_lang["amenities_googlemapsv3"]["option"]["rv_park"] = "RV park";
$install_lang["amenities_googlemapsv3"]["option"]["school"] = "Schule";
$install_lang["amenities_googlemapsv3"]["option"]["shoe_store"] = "Schuhgeschäft";
$install_lang["amenities_googlemapsv3"]["option"]["shopping_mall"] = "Einkaufszentrum";
$install_lang["amenities_googlemapsv3"]["option"]["spa"] = "Spa";
$install_lang["amenities_googlemapsv3"]["option"]["stadium"] = "Stadion";
$install_lang["amenities_googlemapsv3"]["option"]["storage"] = "Lagerung";
$install_lang["amenities_googlemapsv3"]["option"]["store"] = "Geschäft";
$install_lang["amenities_googlemapsv3"]["option"]["subway_station"] = "U-Bahnstation";
$install_lang["amenities_googlemapsv3"]["option"]["synagogue"] = "Synagoge";
$install_lang["amenities_googlemapsv3"]["option"]["taxi_stand"] = "Taxistand";
$install_lang["amenities_googlemapsv3"]["option"]["train_station"] = "Bahnhof";
$install_lang["amenities_googlemapsv3"]["option"]["travel_agency"] = "Reisebüro";
$install_lang["amenities_googlemapsv3"]["option"]["university"] = "Universität";
$install_lang["amenities_googlemapsv3"]["option"]["veterinary_care"] = "Tierarzt";
$install_lang["amenities_googlemapsv3"]["option"]["zoo"] = "Zoo";



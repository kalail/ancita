<?php

$install_lang["geomap_type_googlemaps"]["header"] = "Tipo de mapa (Google v2)";
$install_lang["geomap_type_googlemaps"]["option"]["1"] = "normal";
$install_lang["geomap_type_googlemaps"]["option"]["2"] = "satélite";
$install_lang["geomap_type_googlemaps"]["option"]["3"] = "híbrido";
$install_lang["geomap_type_googlemaps"]["option"]["4"] = "Terrian";

$install_lang["geomap_type_googlemapsv3"]["header"] = "Tipo de mapa (Google v3)";
$install_lang["geomap_type_googlemapsv3"]["option"]["1"] = "Plan de trabajo";
$install_lang["geomap_type_googlemapsv3"]["option"]["2"] = "satélite";
$install_lang["geomap_type_googlemapsv3"]["option"]["3"] = "híbrido";
$install_lang["geomap_type_googlemapsv3"]["option"]["4"] = "Terrian";

$install_lang["geomap_type_yandexmaps"]["header"] = "Tipo de mapa (Yandex v1)";
$install_lang["geomap_type_yandexmaps"]["option"]["1"] = "Sheme";
$install_lang["geomap_type_yandexmaps"]["option"]["2"] = "satélite";
$install_lang["geomap_type_yandexmaps"]["option"]["3"] = "híbrido";

$install_lang["geomap_type_yandexmapsv2"]["header"] = "Tipo de mapa (Yandex v2)";
$install_lang["geomap_type_yandexmapsv2"]["option"]["1"] = "Sheme";
$install_lang["geomap_type_yandexmapsv2"]["option"]["2"] = "satélite";
$install_lang["geomap_type_yandexmapsv2"]["option"]["3"] = "híbrido";
$install_lang["geomap_type_yandexmapsv2"]["option"]["4"] = "Público";
$install_lang["geomap_type_yandexmapsv2"]["option"]["5"] = "Híbrido Pública";

$install_lang["geomap_type_bingmapsv7"]["header"] = "Tipo de mapa (Bing v7)";
$install_lang["geomap_type_bingmapsv7"]["option"]["1"] = "Aéreo";
$install_lang["geomap_type_bingmapsv7"]["option"]["2"] = "Auto";
$install_lang["geomap_type_bingmapsv7"]["option"]["3"] = "Ojo de perdiz";
$install_lang["geomap_type_bingmapsv7"]["option"]["4"] = "Collin's Bart";
$install_lang["geomap_type_bingmapsv7"]["option"]["5"] = "Mercator";
$install_lang["geomap_type_bingmapsv7"]["option"]["6"] = "Ordnance Survey";
$install_lang["geomap_type_bingmapsv7"]["option"]["7"] = "Carretera";

$install_lang["amenities_googlemapsv3"]["header"] = "Electrodoméstico";
$install_lang["amenities_googlemapsv3"]["option"]["accounting"] = "Contabilidad";
$install_lang["amenities_googlemapsv3"]["option"]["amusement_park"] = "Parque de atracciones";
$install_lang["amenities_googlemapsv3"]["option"]["aquarium"] = "Acuario";
$install_lang["amenities_googlemapsv3"]["option"]["art_gallery"] = "Galería de arte";
$install_lang["amenities_googlemapsv3"]["option"]["atm"] = "Cajero automático";
$install_lang["amenities_googlemapsv3"]["option"]["bakery"] = "Panadería";
$install_lang["amenities_googlemapsv3"]["option"]["bank"] = "Banco";
$install_lang["amenities_googlemapsv3"]["option"]["bar"] = "Bar";
$install_lang["amenities_googlemapsv3"]["option"]["beauty_salon"] = "Salón de belleza";
$install_lang["amenities_googlemapsv3"]["option"]["bicycle_store"] = "Tienda de bicicletas";
$install_lang["amenities_googlemapsv3"]["option"]["book_store"] = "Tienda de libros";
$install_lang["amenities_googlemapsv3"]["option"]["bowling_alley"] = "Bolos";
$install_lang["amenities_googlemapsv3"]["option"]["bus_station"] = "Estación de autobús";
$install_lang["amenities_googlemapsv3"]["option"]["cafe"] = "Café";
$install_lang["amenities_googlemapsv3"]["option"]["campground"] = "Campismo";
$install_lang["amenities_googlemapsv3"]["option"]["car_dealer"] = "Concesionario de coches";
$install_lang["amenities_googlemapsv3"]["option"]["car_rental"] = "Renta de autos";
$install_lang["amenities_googlemapsv3"]["option"]["car_repair"] = "Reparación de coches";
$install_lang["amenities_googlemapsv3"]["option"]["car_wash"] = "Lavado de autos";
$install_lang["amenities_googlemapsv3"]["option"]["casino"] = "Casino";
$install_lang["amenities_googlemapsv3"]["option"]["cemetery"] = "Cementerio";
$install_lang["amenities_googlemapsv3"]["option"]["church"] = "Iglesia";
$install_lang["amenities_googlemapsv3"]["option"]["city_hall"] = "Ayuntamiento";
$install_lang["amenities_googlemapsv3"]["option"]["clothing_store"] = "Tienda de ropa";
$install_lang["amenities_googlemapsv3"]["option"]["convenience_store"] = "Tienda de conveniencia";
$install_lang["amenities_googlemapsv3"]["option"]["courthouse"] = "Palacio de justicia";
$install_lang["amenities_googlemapsv3"]["option"]["dentist"] = "Dentista";
$install_lang["amenities_googlemapsv3"]["option"]["department_store"] = "grandes almacenes";
$install_lang["amenities_googlemapsv3"]["option"]["doctor"] = "médico";
$install_lang["amenities_googlemapsv3"]["option"]["electrician"] = "Electricista";
$install_lang["amenities_googlemapsv3"]["option"]["electronics_store"] = "Tienda Electrónica";
$install_lang["amenities_googlemapsv3"]["option"]["embassy"] = "Embajada";
$install_lang["amenities_googlemapsv3"]["option"]["establishment"] = "Establecimiento";
$install_lang["amenities_googlemapsv3"]["option"]["finance"] = "Finanzas";
$install_lang["amenities_googlemapsv3"]["option"]["fire_station"] = "Estación de bomberos";
$install_lang["amenities_googlemapsv3"]["option"]["florist"] = "Florista";
$install_lang["amenities_googlemapsv3"]["option"]["food"] = "Comida";
$install_lang["amenities_googlemapsv3"]["option"]["funeral_home"] = "Funeraria";
$install_lang["amenities_googlemapsv3"]["option"]["furniture_store"] = "Mueblería";
$install_lang["amenities_googlemapsv3"]["option"]["gas_station"] = "Gasolinera";
$install_lang["amenities_googlemapsv3"]["option"]["general_contractor"] = "Contratista general";
$install_lang["amenities_googlemapsv3"]["option"]["grocery_or_supermarket"] = "Comestibles o supermercado";
$install_lang["amenities_googlemapsv3"]["option"]["gym"] = "Gimnasio";
$install_lang["amenities_googlemapsv3"]["option"]["hair_care"] = "Cuidado del cabello";
$install_lang["amenities_googlemapsv3"]["option"]["hardware_store"] = "Droguería";
$install_lang["amenities_googlemapsv3"]["option"]["health"] = "Salud";
$install_lang["amenities_googlemapsv3"]["option"]["hindu_temple"] = "Templo hindú";
$install_lang["amenities_googlemapsv3"]["option"]["home_goods_store"] = "Tienda de artículos para el hogar";
$install_lang["amenities_googlemapsv3"]["option"]["hospital"] = "Hospital";
$install_lang["amenities_googlemapsv3"]["option"]["insurance_agency"] = "Agencia de seguros";
$install_lang["amenities_googlemapsv3"]["option"]["jewelry_store"] = "Joyería";
$install_lang["amenities_googlemapsv3"]["option"]["laundry"] = "Lavandería";
$install_lang["amenities_googlemapsv3"]["option"]["lawyer"] = "Abogado";
$install_lang["amenities_googlemapsv3"]["option"]["library"] = "Biblioteca";
$install_lang["amenities_googlemapsv3"]["option"]["liquor_store"] = "Licorería";
$install_lang["amenities_googlemapsv3"]["option"]["local_government_office"] = "Oficina del gobierno local";
$install_lang["amenities_googlemapsv3"]["option"]["locksmith"] = "Cerrajero";
$install_lang["amenities_googlemapsv3"]["option"]["lodging"] = "Alojamiento";
$install_lang["amenities_googlemapsv3"]["option"]["meal_delivery"] = "Entrega de comidas";
$install_lang["amenities_googlemapsv3"]["option"]["meal_takeaway"] = "Comida para llevar comida";
$install_lang["amenities_googlemapsv3"]["option"]["mosque"] = "Mezquita";
$install_lang["amenities_googlemapsv3"]["option"]["movie_rental"] = "Alquiler de películas";
$install_lang["amenities_googlemapsv3"]["option"]["movie_theater"] = "Cine";
$install_lang["amenities_googlemapsv3"]["option"]["moving_company"] = "Empresa de mudanzas";
$install_lang["amenities_googlemapsv3"]["option"]["museum"] = "Museo";
$install_lang["amenities_googlemapsv3"]["option"]["night_club"] = "Club nocturno";
$install_lang["amenities_googlemapsv3"]["option"]["painter"] = "Pintor";
$install_lang["amenities_googlemapsv3"]["option"]["park"] = "Parque";
$install_lang["amenities_googlemapsv3"]["option"]["parking"] = "Aparcamiento";
$install_lang["amenities_googlemapsv3"]["option"]["pet_store"] = "Tienda de mascotas";
$install_lang["amenities_googlemapsv3"]["option"]["pharmacy"] = "Farmacia";
$install_lang["amenities_googlemapsv3"]["option"]["physiotherapist"] = "Fisioterapeuta";
$install_lang["amenities_googlemapsv3"]["option"]["place_of_worship"] = "Lugar de culto";
$install_lang["amenities_googlemapsv3"]["option"]["plumber"] = "Fontanero";
$install_lang["amenities_googlemapsv3"]["option"]["police"] = "Policía";
$install_lang["amenities_googlemapsv3"]["option"]["post_office"] = "Oficina de correos";
$install_lang["amenities_googlemapsv3"]["option"]["real_estate_agency"] = "Agencia inmobiliaria";
$install_lang["amenities_googlemapsv3"]["option"]["restaurant"] = "Restaurante";
$install_lang["amenities_googlemapsv3"]["option"]["roofing_contractor"] = "Contratista de techos";
$install_lang["amenities_googlemapsv3"]["option"]["rv_park"] = "Parque de casas rodantes";
$install_lang["amenities_googlemapsv3"]["option"]["school"] = "Escuela";
$install_lang["amenities_googlemapsv3"]["option"]["shoe_store"] = "Zapatería";
$install_lang["amenities_googlemapsv3"]["option"]["shopping_mall"] = "Centro comercial";
$install_lang["amenities_googlemapsv3"]["option"]["spa"] = "Spa";
$install_lang["amenities_googlemapsv3"]["option"]["stadium"] = "Estadio";
$install_lang["amenities_googlemapsv3"]["option"]["storage"] = "Almacenamiento";
$install_lang["amenities_googlemapsv3"]["option"]["store"] = "Tienda";
$install_lang["amenities_googlemapsv3"]["option"]["subway_station"] = "Estación del metro";
$install_lang["amenities_googlemapsv3"]["option"]["synagogue"] = "Sinagoga";
$install_lang["amenities_googlemapsv3"]["option"]["taxi_stand"] = "Sitio de taxis";
$install_lang["amenities_googlemapsv3"]["option"]["train_station"] = "Estación de trenes";
$install_lang["amenities_googlemapsv3"]["option"]["travel_agency"] = "Agencia de viajes";
$install_lang["amenities_googlemapsv3"]["option"]["university"] = "Universidad";
$install_lang["amenities_googlemapsv3"]["option"]["veterinary_care"] = "Atención veterinaria";
$install_lang["amenities_googlemapsv3"]["option"]["zoo"] = "Zoológico";



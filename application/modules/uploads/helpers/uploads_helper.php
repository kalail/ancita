<?php 

/**
 * Uploads management
 * 
 * @package PG_RealEstate
 * @subpackage Uploads
 * @category	helpers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Mikhail Makeev <mmakeev@pilotgroup.net>
 * @version $Revision: 68 $ $Date: 2010-01-11 16:02:23 +0300 (Пн, 11 янв 2010) $ $Author: irina $
 **/

if(!function_exists("uploader_block")){
	/**
	 * Return uploader block
	 * @params array $params block parameters
	 * @return string
	 */
	function uploader_block($params){
		$CI = &get_instance();
		
		if(!isset($params["url"]) || empty($params["url"])) return false;
		if(!isset($params["field_name"]) || empty($params["field_name"])) return false;
		
		$CI->template_lite->assign("upload_url", $params["url"]);		
		$CI->template_lite->assign("field_name", $params["field_name"]);
		
		$form_id = '';
		if(isset($params["form_id"])) $form_id = $params["form_id"];
		$CI->template_lite->assign("uploader_form_id", $form_id);
		
		$callback = '';
		if(!isset($params["callback"])) $callback = $params["callback"];
		$CI->template_lite->assign("uploader_callback", $params["callback"]);
		
		$max_file_size = 100000000;
		if(isset($params["max_file_size"])) $max_file_size = $params["max_file_size"];
		$CI->template_lite->assign("max_file_size", $max_file_size);
		
		$use_drop_zone = true;
		if(isset($params["use_drop_zone"])) $use_drop_zone = $params["use_drop_zone"];
		$CI->template_lite->assign("use_drop_zone", $use_drop_zone);
		
		$rand = rand(100000, 999999);
		$CI->template_lite->assign("uploader_rand", $rand);
		
		return $CI->template_lite->fetch("helper_uploader_block", "user", "uploads");
	}
}

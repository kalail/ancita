<?php

$install_lang["wm_font_face"]["header"] = "Шрифт";
$install_lang["wm_font_face"]["option"]["arial"] = "Arial";
$install_lang["wm_font_face"]["option"]["arialbd"] = "Arial(bold)";
$install_lang["wm_font_face"]["option"]["arialbi"] = "Arial(bold/italic)";
$install_lang["wm_font_face"]["option"]["ariali"] = "Arial(italic)";
$install_lang["wm_font_face"]["option"]["avant"] = "Avant";
$install_lang["wm_font_face"]["option"]["decor"] = "Decor";
$install_lang["wm_font_face"]["option"]["lucon"] = "Lucon";
$install_lang["wm_font_face"]["option"]["narnia"] = "Narnia";
$install_lang["wm_font_face"]["option"]["times"] = "Times New Roman";
$install_lang["wm_font_face"]["option"]["vera"] = "Vera";
$install_lang["wm_font_face"]["option"]["verdana"] = "Verdana";

$install_lang["wm_position_ver"]["header"] = "Верт. разположение на водния знак";
$install_lang["wm_position_ver"]["option"]["bottom"] = "Отдолу";
$install_lang["wm_position_ver"]["option"]["middle"] = "По средата";
$install_lang["wm_position_ver"]["option"]["top"] = "Отгоре";

$install_lang["wm_position_hor"]["header"] = "Хор. разположение на водния знак";
$install_lang["wm_position_hor"]["option"]["center"] = "В средата";
$install_lang["wm_position_hor"]["option"]["left"] = "Наляво";
$install_lang["wm_position_hor"]["option"]["right"] = "Надясно";

$install_lang["upload_name_format"]["header"] = "Формат на името на файла";
$install_lang["upload_name_format"]["option"]["generate"] = "Генериране на файла";
$install_lang["upload_name_format"]["option"]["format"] = "Запазване на оригиналното име";

$install_lang["thumb_crop_param"]["header"] = "Настройки изрязване";
$install_lang["thumb_crop_param"]["option"]["color"] = "Фиксиран размер (с избор цвят на подложката)";
$install_lang["thumb_crop_param"]["option"]["crop"] = "Изрязване на излишното";
$install_lang["thumb_crop_param"]["option"]["extend"] = "Разтягане до установените размери";
$install_lang["thumb_crop_param"]["option"]["resize"] = "Пропорционална смяна на размерите";

$install_lang["wm_type"]["header"] = "Тип на водния знак";
$install_lang["wm_type"]["option"]["img"] = "Картинка";
$install_lang["wm_type"]["option"]["text"] = "Текст";

$install_lang["thumb_effect"]["header"] = "Ефекти";
$install_lang["thumb_effect"]["option"]["none"] = "Няма";
$install_lang["thumb_effect"]["option"]["grayscale"] = "Grayscale";



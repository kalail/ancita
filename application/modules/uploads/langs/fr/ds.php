<?php

$install_lang["wm_font_face"]["header"] = "Texte";
$install_lang["wm_font_face"]["option"]["arial"] = "Arial";
$install_lang["wm_font_face"]["option"]["arialbd"] = "Arial(bold)";
$install_lang["wm_font_face"]["option"]["arialbi"] = "Arial(bold/italic)";
$install_lang["wm_font_face"]["option"]["ariali"] = "Arial(italic)";
$install_lang["wm_font_face"]["option"]["avant"] = "Avant";
$install_lang["wm_font_face"]["option"]["decor"] = "Decor";
$install_lang["wm_font_face"]["option"]["lucon"] = "Lucon";
$install_lang["wm_font_face"]["option"]["narnia"] = "Narnia";
$install_lang["wm_font_face"]["option"]["times"] = "Times New Roman";
$install_lang["wm_font_face"]["option"]["vera"] = "Vera";
$install_lang["wm_font_face"]["option"]["verdana"] = "Verdana";

$install_lang["wm_position_ver"]["header"] = "Filigrane verticale";
$install_lang["wm_position_ver"]["option"]["bottom"] = "Bas";
$install_lang["wm_position_ver"]["option"]["middle"] = "Milieu";
$install_lang["wm_position_ver"]["option"]["top"] = "Haut";

$install_lang["wm_position_hor"]["header"] = "Filigrane horizontale";
$install_lang["wm_position_hor"]["option"]["center"] = "Centre";
$install_lang["wm_position_hor"]["option"]["left"] = "Gauche";
$install_lang["wm_position_hor"]["option"]["right"] = "Droite";

$install_lang["upload_name_format"]["header"] = "Formatter le nom de fichier";
$install_lang["upload_name_format"]["option"]["generate"] = "Générer nom de fichier";
$install_lang["upload_name_format"]["option"]["format"] = "Sauver nom original";

$install_lang["thumb_crop_param"]["header"] = "Paramètres de recadrage";
$install_lang["thumb_crop_param"]["option"]["color"] = "Taille fixe";
$install_lang["thumb_crop_param"]["option"]["crop"] = "Cadrer";
$install_lang["thumb_crop_param"]["option"]["extend"] = "Cadrer cette image à taille fixe";
$install_lang["thumb_crop_param"]["option"]["resize"] = "Recadrer cette image";

$install_lang["wm_type"]["header"] = "Type de fligrane";
$install_lang["wm_type"]["option"]["img"] = "Image";
$install_lang["wm_type"]["option"]["text"] = "Texte";

$install_lang["thumb_effect"]["header"] = "Effets";
$install_lang["thumb_effect"]["option"]["none"] = "Aucun";
$install_lang["thumb_effect"]["option"]["grayscale"] = "Noir/blanc";



<?php

$install_lang["wm_font_face"]["header"] = "Fuente";
$install_lang["wm_font_face"]["option"]["arial"] = "Arial";
$install_lang["wm_font_face"]["option"]["arialbd"] = "Arial(negrita)";
$install_lang["wm_font_face"]["option"]["arialbi"] = "Arial(negrita/cursiva)";
$install_lang["wm_font_face"]["option"]["ariali"] = "Arial(cursiva)";
$install_lang["wm_font_face"]["option"]["avant"] = "Avant";
$install_lang["wm_font_face"]["option"]["decor"] = "Decor";
$install_lang["wm_font_face"]["option"]["lucon"] = "Lucon";
$install_lang["wm_font_face"]["option"]["narnia"] = "Narnia";
$install_lang["wm_font_face"]["option"]["times"] = "Times New Roman";
$install_lang["wm_font_face"]["option"]["vera"] = "Vera";
$install_lang["wm_font_face"]["option"]["verdana"] = "Verdana";

$install_lang["wm_position_ver"]["header"] = "Posición vertical de marca de agua";
$install_lang["wm_position_ver"]["option"]["bottom"] = "Fondo";
$install_lang["wm_position_ver"]["option"]["middle"] = "Medio";
$install_lang["wm_position_ver"]["option"]["top"] = "Superior";

$install_lang["wm_position_hor"]["header"] = "Posición horizontal de marca de agua";
$install_lang["wm_position_hor"]["option"]["center"] = "Centro";
$install_lang["wm_position_hor"]["option"]["left"] = "izquierda";
$install_lang["wm_position_hor"]["option"]["right"] = "Derecho";

$install_lang["upload_name_format"]["header"] = "Nombre de archivo Formato";
$install_lang["upload_name_format"]["option"]["generate"] = "Generar archivo";
$install_lang["upload_name_format"]["option"]["format"] = "Guardar nombre original";

$install_lang["thumb_crop_param"]["header"] = "Recortar los ajustes";
$install_lang["thumb_crop_param"]["option"]["color"] = "Tamaño fijo (elija color de fondo)";
$install_lang["thumb_crop_param"]["option"]["crop"] = "Recorte";
$install_lang["thumb_crop_param"]["option"]["extend"] = "Ampliar imagen para tamaños fijos";
$install_lang["thumb_crop_param"]["option"]["resize"] = "Cambiar el tamaño de la imagen";

$install_lang["wm_type"]["header"] = "Tipo de marca de agua";
$install_lang["wm_type"]["option"]["img"] = "Imagen";
$install_lang["wm_type"]["option"]["text"] = "Texto";

$install_lang["thumb_effect"]["header"] = "Efectos";
$install_lang["thumb_effect"]["option"]["none"] = "Ninguno";
$install_lang["thumb_effect"]["option"]["grayscale"] = "Escala de grises";



<?php

$install_lang["wm_font_face"]["header"] = "Schriftart";
$install_lang["wm_font_face"]["option"]["arial"] = "Arial";
$install_lang["wm_font_face"]["option"]["arialbd"] = "Arial(bold)";
$install_lang["wm_font_face"]["option"]["arialbi"] = "Arial(bold/italic)";
$install_lang["wm_font_face"]["option"]["ariali"] = "Arial(italic)";
$install_lang["wm_font_face"]["option"]["avant"] = "Avant";
$install_lang["wm_font_face"]["option"]["decor"] = "Decor";
$install_lang["wm_font_face"]["option"]["lucon"] = "Lucon";
$install_lang["wm_font_face"]["option"]["narnia"] = "Narnia";
$install_lang["wm_font_face"]["option"]["times"] = "Times New Roman";
$install_lang["wm_font_face"]["option"]["vera"] = "Vera";
$install_lang["wm_font_face"]["option"]["verdana"] = "Verdana";

$install_lang["wm_position_ver"]["header"] = "Vertikale Wasserzeichenposition";
$install_lang["wm_position_ver"]["option"]["bottom"] = "Boden";
$install_lang["wm_position_ver"]["option"]["middle"] = "Mittel";
$install_lang["wm_position_ver"]["option"]["top"] = "Top";

$install_lang["wm_position_hor"]["header"] = "Horzentale Wasserzeichenposition";
$install_lang["wm_position_hor"]["option"]["center"] = "Mitte";
$install_lang["wm_position_hor"]["option"]["left"] = "Links";
$install_lang["wm_position_hor"]["option"]["right"] = "Rechts";

$install_lang["upload_name_format"]["header"] = "Formatdateidame";
$install_lang["upload_name_format"]["option"]["generate"] = "Dateiname generieren";
$install_lang["upload_name_format"]["option"]["format"] = "Speicher Originalname";

$install_lang["thumb_crop_param"]["header"] = "Crop Eintsellungen";
$install_lang["thumb_crop_param"]["option"]["color"] = "Feste Größe (wähle bg Farbe)";
$install_lang["thumb_crop_param"]["option"]["crop"] = "Crop";
$install_lang["thumb_crop_param"]["option"]["extend"] = "Bild auf bestimmte Größe vergrößern";
$install_lang["thumb_crop_param"]["option"]["resize"] = "Bildgröße anpassen";

$install_lang["wm_type"]["header"] = "Wasserzeichentyp";
$install_lang["wm_type"]["option"]["img"] = "Bild";
$install_lang["wm_type"]["option"]["text"] = "Text";

$install_lang["thumb_effect"]["header"] = "Effekte";
$install_lang["thumb_effect"]["option"]["none"] = "Keine";
$install_lang["thumb_effect"]["option"]["grayscale"] = "Grauskala";



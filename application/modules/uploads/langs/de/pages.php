<?php

$install_lang["admin_header_config_add"] = "Upload hinzufügen";
$install_lang["admin_header_config_change"] = "Hochladen bearbeiten";
$install_lang["admin_header_configs_edit"] = "Einstellungen bearbeiten";
$install_lang["admin_header_configs_list"] = "Hochladen Einstellungen";
$install_lang["admin_header_preview"] = "Vorschau";
$install_lang["admin_header_thumb_add"] = "Vorschau hinzufügen";
$install_lang["admin_header_thumb_change"] = "Vorschau ändern";
$install_lang["admin_header_thumb_edit"] = "Vorschau bearbeiten";
$install_lang["admin_header_thumbs_list"] = "Vorschau: ";
$install_lang["admin_header_watermark_add"] = "Wasserzeichen hinzufügen";
$install_lang["admin_header_watermark_change"] = "Wasserzeichen wechseln";
$install_lang["admin_header_watermarks_edit"] = "Wasserzeichen";
$install_lang["admin_header_watermarks_list"] = "Wsserzeichen";
$install_lang["btn_choose_file"] = "Wähle Datei vom Computer";
$install_lang["error_empty_file_formats"] = "Datei Formate leer";
$install_lang["error_gid_invalid"] = "Flasches Keywort";
$install_lang["error_height_invalid"] = "Flasche Höhe";
$install_lang["error_prefix_invalid"] = "Präfix ist flasch oder leer";
$install_lang["error_title_invalid"] = "Falscher Titel";
$install_lang["error_width_invalid"] = "Flasche Breite";
$install_lang["field_alpha"] = "Deckkraft";
$install_lang["field_date_add"] = "Datum hinzugefügt";
$install_lang["field_default_img"] = "Standardbild";
$install_lang["field_effects"] = "Effekte";
$install_lang["field_file_formats"] = "Formate";
$install_lang["field_font_color"] = "Farbe";
$install_lang["field_font_family"] = "Schriftart";
$install_lang["field_font_size"] = "Schriftgröße";
$install_lang["field_font_text"] = "Text";
$install_lang["field_gid"] = "Keywort";
$install_lang["field_img"] = "Bild";
$install_lang["field_max_height"] = "Max Höhe";
$install_lang["field_max_size"] = "Max Größe";
$install_lang["field_max_width"] = "Max Breite";
$install_lang["field_min_height"] = "Min Höhe";
$install_lang["field_min_width"] = "Min Breite";
$install_lang["field_name"] = "Name";
$install_lang["field_name_format"] = "Datei Name";
$install_lang["field_position"] = "Position";
$install_lang["field_prefix"] = "Präfix";
$install_lang["field_resize_bg_color"] = "Farbe";
$install_lang["field_resize_type"] = "Größentyp ändern";
$install_lang["field_shadow_color"] = "Schattenfarbe";
$install_lang["field_shadow_distance"] = "Schatten Distanz";
$install_lang["field_sizes"] = "Größen";
$install_lang["field_thumb_watermark"] = "Wasserzeichen nutzen";
$install_lang["field_watermark"] = "Wasserzeichen nutzen";
$install_lang["field_wm_type"] = "Typ";
$install_lang["int_unlimit_condition"] = "(0 - unendlich)";
$install_lang["link_add_config"] = "Konfiguration hinzufügen";
$install_lang["link_add_thumb"] = "Vorschau hinzufügen";
$install_lang["link_add_watermark"] = "Wasserzeichen hinzufügen";
$install_lang["link_delete_config"] = "Konfiguration löschen";
$install_lang["link_delete_thumb"] = "Vorschau löschen";
$install_lang["link_delete_watermark"] = "Wasserzeichen löschen";
$install_lang["link_edit_config"] = "Kofiguration bearbeiten";
$install_lang["link_edit_config_thumbs"] = "Konfiguration oder Vorschau bearbeiten";
$install_lang["link_edit_thumb"] = "Vorschau bearbeiten";
$install_lang["link_edit_watermark"] = "Wasserzeichen bearbeiten";
$install_lang["load_files_by_drag_drop"] = "Ziehen und Ablegen von Dateien vom Computer oder ";
$install_lang["no_configs"] = "Keine Konfiguration";
$install_lang["no_thumbs"] = "Keine Vorschau";
$install_lang["no_watermarks"] = "Kein Wasserzeichen";
$install_lang["note_delete_config"] = "Sind Sie sicher, dass Sie die Konfiguration löschen wollen?";
$install_lang["note_delete_thumb"] = "Sind Sie sicher, dass Sie die Vorschau löschen wollen?";
$install_lang["note_delete_watermark"] = "Sind Sie sicher, dass Sie das Wasserzeichen löschen wollen?";
$install_lang["success_added_config"] = "Einstellungen erfolgreich gespeichert";
$install_lang["success_added_thumb"] = "Vorschau erfolgreich hinzugefügt";
$install_lang["success_added_watermark"] = "Wasserzeichen erfolgreich hinzugefügt";
$install_lang["success_updated_config"] = "Einstellungen erfolgreich gespeichert";
$install_lang["success_updated_thumb"] = "Vorschau erfolgreich aktualisiert";
$install_lang["success_updated_watermark"] = "Wasserzeichen erfolgreich geändert";
$install_lang["view_source_link"] = "Quelle ansehen";


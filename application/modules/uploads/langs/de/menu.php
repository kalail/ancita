<?php

$install_lang["admin_menu_settings_items_uploads-items_uploads_menu_item"] = "Fotos";
$install_lang["admin_menu_settings_items_uploads-items_uploads_menu_item_tooltip"] = "Einstellungen von Fotos, Vorschaubildern & Wasserzeichen";
$install_lang["admin_uploads_menu_configs_list_item"] = "Einstellungen";
$install_lang["admin_uploads_menu_configs_list_item_tooltip"] = "";
$install_lang["admin_uploads_menu_watermarks_list_item"] = "Wasserzeichen";
$install_lang["admin_uploads_menu_watermarks_list_item_tooltip"] = "";


{if count($services) > 0}
<div class="line top">
<span class="btn-text">{l i='you_can_auth' gid='users'}</span>
{foreach item=item from=$services}
	<a href="{$site_url}users_connections/oauth_login/{$item.id}" class="btn-link"><ins class="fa fa-{switch from=$item.gid}{case value='vkontakte'}vk{case value='google'}google-plus{case}{$item.gid}{/switch} fa-lg square edge hover"></ins></a>
{/foreach}
</div>
{/if}

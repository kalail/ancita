<?php

$install_lang["account_link_to"] = "Votre compte est lié à";
$install_lang["account_unlink_from"] = "Votre compte est maintenant délié de";
$install_lang["first_connect_via"] = "Ceci est votre première connection avec l'aide de";
$install_lang["please_set_email"] = "SVP entrez votre courriel et mot de passe pour connecter au site avec cette information";
$install_lang["select_usertype"] = "Choisissez votre type d'utilisateur";
$install_lang["you_can_auth"] = "Ou connectez avec";
$install_lang["you_can_link"] = "Vous pouvez connecter avec";
$install_lang["you_can_unlink"] = "Vos pouvez déconnecter votre compte de ces services:";


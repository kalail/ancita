<?php

$install_lang["account_link_to"] = "Su cuenta está conectada con";
$install_lang["account_unlink_from"] = "Su cuenta está desligada de";
$install_lang["first_connect_via"] = "Esta es su primera conexión con la ayuda de";
$install_lang["please_set_email"] = "Por favor, introduzca su email y contraseña para iniciar sesión en el sitio directamente con esta información";
$install_lang["select_usertype"] = "Por favor, elija su tipo de usuario";
$install_lang["you_can_auth"] = "O inicie sesión con";
$install_lang["you_can_link"] = "Puede conectarse a través";
$install_lang["you_can_unlink"] = "Puede desligar su cuenta de servicios de:";


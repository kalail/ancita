<?php

$install_lang["account_link_to"] = "Вашият профил е включен към";
$install_lang["account_unlink_from"] = "Вашият профил е изключен към";
$install_lang["first_connect_via"] = "Това е вашето първо свързване чрез";
$install_lang["please_set_email"] = "Моля попълнете вашия email и парола за да влезете в сайта директно с тази информация";
$install_lang["select_usertype"] = "Моля, изберете тип потребител";
$install_lang["you_can_auth"] = "Или влезте с";
$install_lang["you_can_link"] = "Вие можете да се свържете използвайки:";
$install_lang["you_can_unlink"] = "Можете да прекратите връзката на профила си от следните услуги:";


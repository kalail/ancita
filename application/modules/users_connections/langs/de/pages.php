<?php

$install_lang["account_link_to"] = "Ihr Konto ist verlinkt mit";
$install_lang["account_unlink_from"] = "Ihr Konto ist nicht verlinkt mit";
$install_lang["first_connect_via"] = "Das ist Ihre erste Verbindung mit der Hilfe von";
$install_lang["please_set_email"] = "Bitte geben Sie Ihre E-Mail und Passwort zum einloggen direkt auf der Seite mit diesen Informationen ein";
$install_lang["select_usertype"] = "Bitte wählen Sie Ihren Typ von Benutzer";
$install_lang["you_can_auth"] = "Oder melden Sie sich mit an";
$install_lang["you_can_link"] = "Sie können verbinden verwenden";
$install_lang["you_can_unlink"] = "Sie können Ihr Konto die Verlinkung mit folgenden Services aufheben:";


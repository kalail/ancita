<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Languages main model
 *
 * @package PG_RealEstate
 * @subpackage Languages
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Languages_model extends Model {
	/**
	 * Link to CodeIgniter
	 * 
	 * @var object
	 */
	private $CI;

	/**
	 * Constructor
	 * 
	 * @return Languages_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Add fields to languages dependes on languages
	 * 
	 * @param integer $lang_id language identifier
	 * @return void
	 */
	/*public function lang_dedicate_module_callback_add($lang_id = false) {
		$this->update_route_langs();
	}*/

	/**
	 * Remove fields to languages dependes on languages
	 * 
	 * @param integer $lang_id language identifier
	 * @return void
	 */
	/*public function lang_dedicate_module_callback_delete($lang_id = false) {
		$this->update_route_langs();
	}*/

	/**
	 * Update or create file config/langs_route.php
	 *
	 * @return boolean
	 */
	/*private function update_route_langs() {

		$langs = $this->CI->pg_language->get_langs();
		if (0 == count($langs)) {
			return false;
		}

		$file_content = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');\n\n";
		$file_content .= "\$config['langs_route'] = array(";
		foreach($langs as $lang) {
			$file_content .= "'" . $lang['code'] . "', ";
		}
		$file_content = substr_replace($file_content, '', -2);
		$file_content .= ');';

		$file = APPPATH . 'config/langs_route' . EXT;
		try {
			$handle = fopen($file, 'w');
			fwrite($handle, $file_content);
			fclose($handle);
		} catch (Exception $e) {
			log_message('error','Error while updating langs_route' . EXT .
					'(' . $e->getMessage() . ') in ' . $e->getFile());
			throw $e;
		}
		return true;
	}*/
}

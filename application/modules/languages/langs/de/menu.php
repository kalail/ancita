<?php

$install_lang["admin_languages_menu_languages_ds_item"] = "Datenquellen";
$install_lang["admin_languages_menu_languages_ds_item_tooltip"] = "";
$install_lang["admin_languages_menu_languages_list_item"] = "Sprachen";
$install_lang["admin_languages_menu_languages_list_item_tooltip"] = "";
$install_lang["admin_languages_menu_languages_pages_item"] = "Steiten";
$install_lang["admin_languages_menu_languages_pages_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_content_items_languages_menu_item"] = "Sprachen";
$install_lang["admin_menu_settings_items_content_items_languages_menu_item_tooltip"] = "Neue Sprachen hinzufügen, Seitensprache bearbeiten";


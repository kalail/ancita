<?php

$install_lang["admin_languages_menu_languages_ds_item"] = "Sources de données";
$install_lang["admin_languages_menu_languages_ds_item_tooltip"] = "";
$install_lang["admin_languages_menu_languages_list_item"] = "Langues";
$install_lang["admin_languages_menu_languages_list_item_tooltip"] = "";
$install_lang["admin_languages_menu_languages_pages_item"] = "Pages";
$install_lang["admin_languages_menu_languages_pages_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_content_items_languages_menu_item"] = "Langues";
$install_lang["admin_menu_settings_items_content_items_languages_menu_item_tooltip"] = "Ajouter nouvelles langues, éditer langue du site";


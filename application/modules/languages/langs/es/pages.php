<?php

$install_lang["admin_header_ds_add"] = "Añadir fuente de datos";
$install_lang["admin_header_ds_change"] = "Cambio de nombre de fuente de datos";
$install_lang["admin_header_ds_edit"] = "Edictando fuente de datos";
$install_lang["admin_header_ds_item_add"] = "Añadir opción";
$install_lang["admin_header_ds_item_change"] = "Cambio de opción";
$install_lang["admin_header_ds_item_edit"] = "Cambiar opción de fuente de datos";
$install_lang["admin_header_ds_items"] = "Opciones de fuente de datos";
$install_lang["admin_header_ds_list"] = "Fuente de datos";
$install_lang["admin_header_language_add"] = "Añadir datos de idioma";
$install_lang["admin_header_language_change"] = "Cambio de datos idioma";
$install_lang["admin_header_language_edit"] = "Editar Idioma";
$install_lang["admin_header_languages_list"] = "Idiomas";
$install_lang["admin_header_page_add"] = "Añadiendo cadena";
$install_lang["admin_header_page_change"] = "Cambiar cadena";
$install_lang["admin_header_pages_list"] = "Páginas";
$install_lang["api_error_empty_lang_id"] = "Identificación del idioma vacío";
$install_lang["default_editable_text"] = "Editar";
$install_lang["error_code_exists"] = "Ya existe código de Idioma";
$install_lang["error_code_incorrect"] = "Código de idioma no es válido";
$install_lang["error_default_inactive_lang"] = "No se puede establecer el idioma predeterminado mientras que su inactividad";
$install_lang["error_delete_default_lang"] = "No se puede eliminar el idioma Predeterminado";
$install_lang["error_gid_exists"] = "Cadena con una palabra clave ya existe";
$install_lang["error_gid_incorrect"] = "Palabra clave incorrecta";
$install_lang["error_name_incorrect"] = "Nombre del idioma no es válida";
$install_lang["error_rtl_incorrect"] = "Parámetro RTL no está configurado";
$install_lang["field_active"] = "Actividad";
$install_lang["field_code"] = "Código";
$install_lang["field_default"] = "Predeterminado";
$install_lang["field_ds_name"] = "Fuente de datos";
$install_lang["field_gid"] = "Palabra clave";
$install_lang["field_language_name"] = "Nombre";
$install_lang["field_name"] = "Nombre";
$install_lang["field_rtl"] = "Dirección del texto";
$install_lang["field_rtl_value_ltr"] = "De izquierda a derecha";
$install_lang["field_rtl_value_rtl"] = "De derecha a izquierda";
$install_lang["field_text"] = "Texto";
$install_lang["link_activate_language"] = "Activar idioma";
$install_lang["link_add_ds"] = "Añadir una referencia";
$install_lang["link_add_ds_item"] = "Añadida opción";
$install_lang["link_add_lang"] = "Agregar idioma";
$install_lang["link_add_page"] = "Añadir cadena";
$install_lang["link_deactivate_language"] = "Desactivar idioma";
$install_lang["link_delete_ds"] = "Eliminar fuente de datos";
$install_lang["link_delete_lang"] = "Eliminar idioma";
$install_lang["link_delete_page"] = "Eliminar cadena";
$install_lang["link_delete_selected"] = "Eliminar seleccionados";
$install_lang["link_edit_ds"] = "Editar nombre de origen de datos";
$install_lang["link_edit_ds_items"] = "Modificar opciones del origen de datos";
$install_lang["link_edit_lang"] = "Editar el idioma";
$install_lang["link_edit_page"] = "Editar cadena";
$install_lang["link_make_default_language"] = "Use un lenguaje Predeterminado";
$install_lang["link_resort_items"] = "Guardar secuencia";
$install_lang["note_delete_ds"] = "¿Está seguro que desea eliminar la fuente de datos?";
$install_lang["note_delete_ds_item"] = "¿Está seguro que desea eliminar la opción?";
$install_lang["note_delete_dses"] = "¿Está seguro de que desea eliminar las fuentes de datos seleccionados?";
$install_lang["note_delete_lang"] = "¿Está seguro que desea eliminar el lenguaje?";
$install_lang["note_delete_page"] = "¿Está seguro que desea eliminar la cadena?";
$install_lang["note_delete_pages"] = "¿Está seguro de que desea eliminar las cadenas seleccionadas?";
$install_lang["others_languages"] = "Otros idiomas";
$install_lang["success_added_ds"] = "Fuente se ha creado correctamente";
$install_lang["success_added_ds_item"] = "Se añade la opción";
$install_lang["success_added_lang"] = "El lenguaje se ha añadido correctamente";
$install_lang["success_added_page"] = "Cadena agregada correctamente";
$install_lang["success_defaulted_lang"] = "Idioma establecedo correctamente como predeterminado";
$install_lang["success_deleted_lang"] = "Idioma eliminado";
$install_lang["success_deleted_page"] = "Cadena eliminada";
$install_lang["success_updated_ds"] = "Fuente de datosactualizada correctamente";
$install_lang["success_updated_ds_item"] = "Cadena cambiada con éxito";
$install_lang["success_updated_lang"] = "Idioma actualizado correctamente";
$install_lang["success_updated_page"] = "Cadena actualizada correctamente";
$install_lang["text_default_language"] = "Idioma establecido de forma predeterminada";
$install_lang["text_module_select"] = "Elija el módulo";


<?php

$install_lang["admin_languages_menu_languages_ds_item"] = "Orígenes de datos";
$install_lang["admin_languages_menu_languages_ds_item_tooltip"] = "";
$install_lang["admin_languages_menu_languages_list_item"] = "Idiomas";
$install_lang["admin_languages_menu_languages_list_item_tooltip"] = "";
$install_lang["admin_languages_menu_languages_pages_item"] = "Páginas";
$install_lang["admin_languages_menu_languages_pages_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_content_items_languages_menu_item"] = "Idiomas";
$install_lang["admin_menu_settings_items_content_items_languages_menu_item_tooltip"] = "Añadir nueva idiomas, el idioma de edición web";


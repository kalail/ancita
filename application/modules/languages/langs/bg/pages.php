<?php

$install_lang["admin_header_ds_add"] = "Добавяне база данни";
$install_lang["admin_header_ds_change"] = "Редактиране названието на източника на данни";
$install_lang["admin_header_ds_edit"] = "Редактиране на източника на данни";
$install_lang["admin_header_ds_item_add"] = "Добавяне на вариант";
$install_lang["admin_header_ds_item_change"] = "Променяне на варианта";
$install_lang["admin_header_ds_item_edit"] = "Променяне източника на данни";
$install_lang["admin_header_ds_items"] = "Варианти на база данни";
$install_lang["admin_header_ds_list"] = "База данни";
$install_lang["admin_header_language_add"] = "Добавяне база данни";
$install_lang["admin_header_language_change"] = "Промяна база данни";
$install_lang["admin_header_language_edit"] = "Редактиране на езика";
$install_lang["admin_header_languages_list"] = "Езици";
$install_lang["admin_header_page_add"] = "Добавяне на фрази";
$install_lang["admin_header_page_change"] = "Редактиране на фрази";
$install_lang["admin_header_pages_list"] = "Страници";
$install_lang["api_error_empty_lang_id"] = "Липсава id на езика";
$install_lang["default_editable_text"] = "Редактиране";
$install_lang["error_code_exists"] = "Такъв код за език вече съществува";
$install_lang["error_code_incorrect"] = "Грешен код за език";
$install_lang["error_default_inactive_lang"] = "Не можете да изберете езика по подразбиране преди да го активирате";
$install_lang["error_delete_default_lang"] = "Не трябва да премахвате езика по подразбиране";
$install_lang["error_gid_exists"] = "Фраза с това системно име вече съществува";
$install_lang["error_gid_incorrect"] = "Грешно системно име";
$install_lang["error_name_incorrect"] = "Грешно име на езика";
$install_lang["error_rtl_incorrect"] = "Не са добавени параметри за RTL текста";
$install_lang["field_active"] = "Активен";
$install_lang["field_code"] = "Код";
$install_lang["field_default"] = "По подразбиране";
$install_lang["field_ds_name"] = "База данни";
$install_lang["field_gid"] = "Системно име";
$install_lang["field_language_name"] = "Название";
$install_lang["field_name"] = "Название";
$install_lang["field_rtl"] = "Направление на текста";
$install_lang["field_rtl_value_ltr"] = "Отляво-надясно";
$install_lang["field_rtl_value_rtl"] = "Отдясно-наляво";
$install_lang["field_text"] = "Текст";
$install_lang["link_activate_language"] = "Активирай езика";
$install_lang["link_add_ds"] = "Добави справочник";
$install_lang["link_add_ds_item"] = "Добави вариант";
$install_lang["link_add_lang"] = "Добави език";
$install_lang["link_add_page"] = "Добави фраза";
$install_lang["link_deactivate_language"] = "Деактивирай език";
$install_lang["link_delete_ds"] = "Премахни базата данни";
$install_lang["link_delete_lang"] = "Изтрий езика";
$install_lang["link_delete_page"] = "Изтрий фраза";
$install_lang["link_delete_selected"] = "Изтрий избраното";
$install_lang["link_edit_ds"] = "Редактирай името на базата данни";
$install_lang["link_edit_ds_items"] = "Редактиране вариантите на базата данни";
$install_lang["link_edit_lang"] = "Редактиране език";
$install_lang["link_edit_page"] = "Редактиране фраза";
$install_lang["link_make_default_language"] = "Използвай езика по подразбиране";
$install_lang["link_resort_items"] = "Запази последователноста";
$install_lang["note_delete_ds"] = "Сигурни ли сте, че искате да изтриете тази база данни?";
$install_lang["note_delete_ds_item"] = "Сигурни ли сте, че искате да изтриете този вариант база данни?";
$install_lang["note_delete_dses"] = "Сигурни ли сте, че искате да изтриете избраната база данни?";
$install_lang["note_delete_lang"] = "Сигурни ли сте, че искате да изтриете този език?";
$install_lang["note_delete_page"] = "Сигурни ли сте, че искате да изтриете тази фраза?";
$install_lang["note_delete_pages"] = "Сигурни ли сте, че искате да изтриете избраните фрази?";
$install_lang["others_languages"] = "Други езици";
$install_lang["success_added_ds"] = "Базата данни е създадена успешно";
$install_lang["success_added_ds_item"] = "Вариантът е добавен";
$install_lang["success_added_lang"] = "Езикът е добавен успешно";
$install_lang["success_added_page"] = "Фразата е добавена";
$install_lang["success_defaulted_lang"] = "Езикът е добавен като основен";
$install_lang["success_deleted_lang"] = "Езикът е изтрит";
$install_lang["success_deleted_page"] = "Фразата е изтрита";
$install_lang["success_updated_ds"] = "Базата данни е обновена";
$install_lang["success_updated_ds_item"] = "Фразата е променена";
$install_lang["success_updated_lang"] = "Езикът е обновен";
$install_lang["success_updated_page"] = "Фразата е обновена";
$install_lang["text_default_language"] = "Езикът е избран за основен";
$install_lang["text_module_select"] = "Избери модул";


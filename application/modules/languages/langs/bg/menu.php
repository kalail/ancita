<?php

$install_lang["admin_languages_menu_languages_ds_item"] = "База данни";
$install_lang["admin_languages_menu_languages_ds_item_tooltip"] = "";
$install_lang["admin_languages_menu_languages_list_item"] = "Езици";
$install_lang["admin_languages_menu_languages_list_item_tooltip"] = "";
$install_lang["admin_languages_menu_languages_pages_item"] = "Страници";
$install_lang["admin_languages_menu_languages_pages_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_content_items_languages_menu_item"] = "Езици";
$install_lang["admin_menu_settings_items_content_items_languages_menu_item_tooltip"] = "Добавяне на нови езици, редактиране езика на сайта";


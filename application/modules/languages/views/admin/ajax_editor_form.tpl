
<div class="load_content_controller">
	<h1>{l i='edit_languages' gid='languages'}</h1>
	<div class="inside">
		<form name="lang_edit" class="edit-form n100">
			{section name=item loop=$langs}
				<div class="row{if $templatelite.section.item.iteration%2 eq 0}-odd{/if} {if $templatelite.section.item.iteration%4 eq 3}zebra{/if}">
					<div class="h">
						<label>{$langs[item].name}</label>: 
					</div>
					<div class="v">
						<input type="text" name="field{$templatelite.section.item.index}" value="" />
					</div>
				</div>
			{/section}
			
			<div class="controller-actions"><div><a id="close_btn" href="#">{l i='btn_close' module='start'}</a></div></div>
		</form>
	</div>
</div>

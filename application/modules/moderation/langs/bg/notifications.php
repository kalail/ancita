<?php

$install_lang["notification_auser_need_moderate"] = "Новият обект очаква проверка";
$install_lang["tpl_auser_need_moderate_content"] = "Здравейте, администратор!\n\nНов обект ([type]) очаква проверка на сайта [domain]. За преглед влезте в панела на администратора > Модерация > Зареждане.\n\nС уважение,\n[name_from]";
$install_lang["tpl_auser_need_moderate_subject"] = "[domain] | Нов обект очаква одобрение";


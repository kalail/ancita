<?php

$install_lang["admin_menu_main_items_moderation-items_badwords_item"] = "Неприлични думи";
$install_lang["admin_menu_main_items_moderation-items_badwords_item_tooltip"] = "База данни с неприлични думи";
$install_lang["admin_menu_main_items_moderation-items_moder_settings_item_2"] = "Настройки на проверките";
$install_lang["admin_menu_main_items_moderation-items_moder_settings_item_2_tooltip"] = "Настройки на зарежданията и проверката за неприлични думи";
$install_lang["admin_menu_main_items_moderation-items_objects_list_item"] = "Заредени";
$install_lang["admin_menu_main_items_moderation-items_objects_list_item_tooltip"] = "Профилни снимки и снимки от галерията очакващи одобрение";
$install_lang["admin_moderation_menu_badwords_item"] = "Неприлични думи";
$install_lang["admin_moderation_menu_badwords_item_tooltip"] = "";
$install_lang["admin_moderation_menu_moder_settings_item"] = "Настройки проверка";
$install_lang["admin_moderation_menu_moder_settings_item_tooltip"] = "";
$install_lang["admin_moderation_menu_moder_setting_item"] = "Настройки проверка";
$install_lang["admin_moderation_menu_moder_setting_item_tooltip"] = "";
$install_lang["admin_moderation_menu_object_list_item"] = "Зареждане";
$install_lang["admin_moderation_menu_object_list_item_tooltip"] = "";


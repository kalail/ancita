<?php 

$install_lang["notification_auser_need_moderate"] = "New object awaiting moderation";
$install_lang["tpl_auser_need_moderate_content"] = "Hello admin,\n\nThere are new object ([type]) awaiting moderation on [domain]. Access administration panel > Moderation > Uploads to view it.\n\nBest regards,\n[name_from]";
$install_lang["tpl_auser_need_moderate_subject"] = "[domain] | New object awaiting moderation";


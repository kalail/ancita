<?php

$install_lang["add_badword_hint"] = "ein oder mehr Wörter geteilt durch eine Leertaste";
$install_lang["admin_header_moderation"] = "Moderation";
$install_lang["admin_header_moderation_badwords_managment"] = "Moderation | Schmimpfwörter";
$install_lang["admin_header_moderation_managment"] = "Moderation | Objekte";
$install_lang["admin_header_moderation_settings_managment"] = "Moderation | Einstellungen";
$install_lang["all_objects"] = "Alle";
$install_lang["approve_object"] = "genehmigen";
$install_lang["badwords_in_base_exists"] = "Der Artikel existiert bereits:";
$install_lang["check_badwords"] = "Schimpfwörter überprüfen";
$install_lang["decline_object"] = "Ablehnen";
$install_lang["delete_object"] = "Objekt löschen";
$install_lang["delete_word"] = "entfernen";
$install_lang["edit_object"] = "Objekt bearbeiten";
$install_lang["error_empty_email"] = "Leere E-Mail";
$install_lang["error_invalid_email"] = "Ungültige E-Mail";
$install_lang["field_admin_moderation_emails"] = "E-Mails";
$install_lang["field_date_add"] = "Datum hinzugefügt";
$install_lang["field_moderation_send_mail"] = "Senden Sie Mitteilung an die E-Mailadresse";
$install_lang["header_add_badword"] = "Schimpfwörter hinzufügen";
$install_lang["header_badword_found"] = "Schimpfwörter gefunden";
$install_lang["header_badwords_base"] = "Schimpfwörter";
$install_lang["header_check_text"] = "Überprüfen Sie den Text auf Schimpfwörter";
$install_lang["moder_object"] = "Details";
$install_lang["moder_object_type"] = "Inhaltstyp";
$install_lang["mtype_0"] = "<b>deaktiveren</b> Moderation";
$install_lang["mtype_1"] = "<b>Beitrag</b>Moderation (Objekte sind unmittelbar nach dem Hinzufügen sichtbar, bis sie deaktiviert sind)";
$install_lang["mtype_2"] = "<b>Pre</b>Moderation (Objekte sind nur sichtbar nach Zustimmung)";
$install_lang["no_objects"] = "Keine Objekte zur Moderation";
$install_lang["no_types"] = "Keinte Arten von Objekten zur Moderation";
$install_lang["note_delete_badword"] = "sind Sie sicher, dass Sie dieses Schimpfwort löschen wollen?";
$install_lang["note_delete_object"] = "Sind Sie sicher, dass Sie dieses Objekt löschen wollen?";
$install_lang["stat_header_moderation"] = "Uploads warten auf Genehmigung";
$install_lang["success_settings_saved"] = "Einstellungen erfolgreich gespeichert";
$install_lang["view_object"] = "Objekt anzeigen";


<?php

$install_lang["admin_menu_main_items_moderation-items_badwords_item"] = "Schimpfwörter";
$install_lang["admin_menu_main_items_moderation-items_badwords_item_tooltip"] = "Schimpfwörter Datenbank";
$install_lang["admin_menu_main_items_moderation-items_moder_settings_item_2"] = "Moderationseinstellungen";
$install_lang["admin_menu_main_items_moderation-items_moder_settings_item_2_tooltip"] = "Erstellen Sie Uploads Moderationen, prüfen Sie nach Schimpfwörter ";
$install_lang["admin_menu_main_items_moderation-items_objects_list_item"] = "Uploads";
$install_lang["admin_menu_main_items_moderation-items_objects_list_item_tooltip"] = "Profilbilder & Angebotsgalerie warten auf Genehmigung";
$install_lang["admin_moderation_menu_badwords_item"] = "Schimpfwörter";
$install_lang["admin_moderation_menu_badwords_item_tooltip"] = "";
$install_lang["admin_moderation_menu_moder_settings_item"] = "Moderationseinstellungen";
$install_lang["admin_moderation_menu_moder_settings_item_tooltip"] = "";
$install_lang["admin_moderation_menu_moder_setting_item"] = "Moderationseinstellungen";
$install_lang["admin_moderation_menu_moder_setting_item_tooltip"] = "";
$install_lang["admin_moderation_menu_object_list_item"] = "Uploads";
$install_lang["admin_moderation_menu_object_list_item_tooltip"] = "";


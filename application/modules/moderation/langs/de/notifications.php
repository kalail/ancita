<?php 

$install_lang["notification_auser_need_moderate"] = "Neues Objekt wartet auf Moderation";
$install_lang["tpl_auser_need_moderate_content"] = "Hallo admin,\n\nEs gibt neues Objekt ([type]), dass auf deine Moderation auf [domain] wartet. Gehe in Administrationsbereich> Moderation> Uploads um es anzuzeigen.\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_auser_need_moderate_subject"] = "[domain] | Neues Objekt wartet auf Moderation";


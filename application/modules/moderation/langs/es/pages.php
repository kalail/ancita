<?php

$install_lang["add_badword_hint"] = "una o más palabras divididas por un espacio";
$install_lang["admin_header_moderation"] = "moderación";
$install_lang["admin_header_moderation_badwords_managment"] = "Moderación | Malas palabras";
$install_lang["admin_header_moderation_managment"] = "Moderación | Objetos";
$install_lang["admin_header_moderation_settings_managment"] = "Moderación | Ajustes";
$install_lang["all_objects"] = "Todo";
$install_lang["approve_object"] = "Aprobar";
$install_lang["badwords_in_base_exists"] = "El objeto ya existe:";
$install_lang["check_badwords"] = "Revisar malas palabras";
$install_lang["decline_object"] = "Rechazar";
$install_lang["delete_object"] = "Eliminar objeto";
$install_lang["delete_word"] = "Eliminar";
$install_lang["edit_object"] = "Editar objeto";
$install_lang["error_empty_email"] = "email vacío";
$install_lang["error_invalid_email"] = "email inválido";
$install_lang["field_admin_moderation_emails"] = "Mensajes de correo electrónico";
$install_lang["field_date_add"] = "Fecha de creación";
$install_lang["field_moderation_send_mail"] = "Enviar una notificación a la dirección de correo electrónico";
$install_lang["header_add_badword"] = "Añadir malas palabras";
$install_lang["header_badword_found"] = "Malas palabras encontradas";
$install_lang["header_badwords_base"] = "Malas palabras";
$install_lang["header_check_text"] = "Revisar el texto para mala palabra";
$install_lang["moder_object"] = "Detalles";
$install_lang["moder_object_type"] = "Tipo de contenido";
$install_lang["mtype_0"] = "<b>Desactivar</b> moderación";
$install_lang["mtype_1"] = "<b>Nota</b>moderación (los objetos son visibles inmediatamente después de la adición hasta que se rechazen)";
$install_lang["mtype_2"] = "<b>Pre</b> moderación (los objetos son visibles sólo después de la aprobación)";
$install_lang["no_objects"] = "No hay objetos de la moderación";
$install_lang["no_types"] = "No hay tipos de objetos a la moderación";
$install_lang["note_delete_badword"] = "¿Está seguro que desea eliminar esta mala palabra?";
$install_lang["note_delete_object"] = "¿Está seguro que desea eliminar este objeto?";
$install_lang["stat_header_moderation"] = "Subidas en espera de aprobación";
$install_lang["success_settings_saved"] = "Configuración guardada exitosamente";
$install_lang["view_object"] = "Ver objeto";


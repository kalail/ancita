<?php

$install_lang["admin_menu_main_items_moderation-items_badwords_item"] = "Malas palabras";
$install_lang["admin_menu_main_items_moderation-items_badwords_item_tooltip"] = "Base de datos de las malas palabras";
$install_lang["admin_menu_main_items_moderation-items_moder_settings_item_2"] = "Configuración de moderación";
$install_lang["admin_menu_main_items_moderation-items_moder_settings_item_2_tooltip"] = "Configurar archivos moderación, compruebe si hay malas palabras";
$install_lang["admin_menu_main_items_moderation-items_objects_list_item"] = "Subidas";
$install_lang["admin_menu_main_items_moderation-items_objects_list_item_tooltip"] = "Imágenes del perfil y galería de espera de la aprobación lista";
$install_lang["admin_moderation_menu_badwords_item"] = "Malas palabras";
$install_lang["admin_moderation_menu_badwords_item_tooltip"] = "";
$install_lang["admin_moderation_menu_moder_settings_item"] = "Configuración de moderación";
$install_lang["admin_moderation_menu_moder_settings_item_tooltip"] = "";
$install_lang["admin_moderation_menu_moder_setting_item"] = "Configuración de moderación";
$install_lang["admin_moderation_menu_moder_setting_item_tooltip"] = "";
$install_lang["admin_moderation_menu_object_list_item"] = "Subidas";
$install_lang["admin_moderation_menu_object_list_item_tooltip"] = "";


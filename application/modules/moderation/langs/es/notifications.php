<?php 

$install_lang["notification_auser_need_moderate"] = "Nuevo objeto en espera de moderación";
$install_lang["tpl_auser_need_moderate_content"] = "Hola admin,\n\nHay nuevo objeto ([type]) en espera de moderación en [domain]. Acceso a panel de administración> Moderación > Cargar para verlo.\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_auser_need_moderate_subject"] = "[domain] | Nuevo objeto moderación";


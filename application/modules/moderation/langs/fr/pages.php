<?php

$install_lang["add_badword_hint"] = "Un ou plusieurs mots divisés par un espace";
$install_lang["admin_header_moderation"] = "Modération";
$install_lang["admin_header_moderation_badwords_managment"] = "Modération | Jurons";
$install_lang["admin_header_moderation_managment"] = "Modération | Objets";
$install_lang["admin_header_moderation_settings_managment"] = "Modération | Paramètres";
$install_lang["all_objects"] = "Tout";
$install_lang["approve_object"] = "Approuver";
$install_lang["badwords_in_base_exists"] = "Cet objet existe déja:";
$install_lang["check_badwords"] = "Vérifier jurons";
$install_lang["decline_object"] = "Déclin";
$install_lang["delete_object"] = "Détruire objet";
$install_lang["delete_word"] = "Supprimer";
$install_lang["edit_object"] = "Éditer objet";
$install_lang["error_empty_email"] = "Courriel vide";
$install_lang["error_invalid_email"] = "Courriel invalide";
$install_lang["field_admin_moderation_emails"] = "Courriels";
$install_lang["field_date_add"] = "Date ajoutée";
$install_lang["field_moderation_send_mail"] = "Envoyer une notification à cette adresse courriel";
$install_lang["header_add_badword"] = "Ajouter jurons";
$install_lang["header_badword_found"] = "Jurons trouvés";
$install_lang["header_badwords_base"] = "Jurons";
$install_lang["header_check_text"] = "Vérifier texte pour un mauvais mot.";
$install_lang["moder_object"] = "Détails";
$install_lang["moder_object_type"] = "Type de contenu";
$install_lang["mtype_0"] = "<b>Désactiver</b> modération";
$install_lang["mtype_1"] = "<b>Post</b>-modération (objets visibles après leur addition jusqu'a temps qu'il sont désapprobés)";
$install_lang["mtype_2"] = "<b>Pre</b>modération";
$install_lang["no_objects"] = "Aucun objets en attente d'approbation";
$install_lang["no_types"] = "Aucun types d'objets en attente d'approbation";
$install_lang["note_delete_badword"] = "Êtes vous sur de vouloir détruire ce juron?";
$install_lang["note_delete_object"] = "Êtes vous sur de vouloir détruire cet objet?";
$install_lang["stat_header_moderation"] = "Téléchargements en attente d'approbation'";
$install_lang["success_settings_saved"] = "Paramètres sauvés";
$install_lang["view_object"] = "Voir objet";


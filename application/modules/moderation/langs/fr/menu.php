<?php

$install_lang["admin_menu_main_items_moderation-items_badwords_item"] = "Jurons";
$install_lang["admin_menu_main_items_moderation-items_badwords_item_tooltip"] = "Base de données (jurons)";
$install_lang["admin_menu_main_items_moderation-items_moder_settings_item_2"] = "Paramètres de modération";
$install_lang["admin_menu_main_items_moderation-items_moder_settings_item_2_tooltip"] = "Préparer la modération des téléchargements, vérifier pour des mauvais mots";
$install_lang["admin_menu_main_items_moderation-items_objects_list_item"] = "Téléchargements";
$install_lang["admin_menu_main_items_moderation-items_objects_list_item_tooltip"] = "Image de profile et d'annonce attendent l'approbation";
$install_lang["admin_moderation_menu_badwords_item"] = "Jurons";
$install_lang["admin_moderation_menu_badwords_item_tooltip"] = "";
$install_lang["admin_moderation_menu_moder_settings_item"] = "Paramètres de modération";
$install_lang["admin_moderation_menu_moder_settings_item_tooltip"] = "";
$install_lang["admin_moderation_menu_moder_setting_item"] = "Paramètres de modération";
$install_lang["admin_moderation_menu_moder_setting_item_tooltip"] = "";
$install_lang["admin_moderation_menu_object_list_item"] = "Téléchargements";
$install_lang["admin_moderation_menu_object_list_item_tooltip"] = "";


<?php 

$install_lang["notification_auser_need_moderate"] = "Nouvel objet en attente d'approbation";
$install_lang["tpl_auser_need_moderate_content"] = "Bonjour admin,\n\nIl a des nouveaux objets ([type])  en attente d'approbation [domain]. Accédez au menu admin > modération > téléchargements pour le voir.\n\nCordialement,\n[name_from]";
$install_lang["tpl_auser_need_moderate_subject"] = "[domain] | Nouvel objet en attente d'approbation";


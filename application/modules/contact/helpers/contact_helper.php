<?php

/**
 * Conact management
 * 
 * @package PG_RealEstate
 * @subpackage Contact
 * @category	helpers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Mikhail Makeev <mmakeev@pilotgroup.net>
 * @version $Revision: 68 $ $Date: 2010-01-11 16:02:23 +0300 (Пн, 11 янв 2010) $ $Author: irina $
 **/

if(!function_exists("show_contact_form")){
	/**
	 * Show contact form
	 * @param integer $user_id
	 */
	function show_contact_form($params){
		$CI = &get_instance();
		
		//$auth_type = $CI->session->userdata("auth_type");
		//if($auth_type) return "";
		
		$CI->load->model("Contact_model");
		$CI->load->plugin("captcha");
		$CI->config->load("captcha_settings");
		$captcha_settings = $CI->config->item("captcha_settings");
		$captcha = create_captcha($captcha_settings);
		$CI->session->set_userdata("captcha_word", $captcha["word"]);
		$data["captcha_image"] = $captcha["image"];
		$data["captcha_word_length"] = strlen($captcha["word"]);
		
		$data["user_id"] = $params['user_id'];
		$data["header"] = $params['header'];
		$CI->template_lite->assign("data", $data);
		
		$CI->load->helper("start");
		$CI->template_lite->assign('phone_format', get_phone_format());
		
		$CI->template_lite->assign('rand', rand(100000, 999999));
		
		// show template from contact module default user theme
		$html = $CI->template_lite->fetch("helper_contact_form", "user", "contact");
		return $html;
	}
}

if(!function_exists("admin_home_contact_block")){
	/**
	 * Admin homepage contact info
	 */
	function admin_home_contact_block(){
		$CI = &get_instance();
		
		$auth_type = $CI->session->userdata("auth_type");
		if($auth_type != "admin") return "";

		$user_type = $CI->session->userdata("user_type");

		$show = true;

		$stat_contact = array(
			"index_method" => true,
		);

		if($user_type == "moderator"){
			$show = false;
			$CI->load->model("Ausers_model");
			$methods_contact = $CI->Ausers_model->get_module_methods("contact");
			if((is_array($methods_contact) && !in_array("index", $methods_contact))){
				$show = true;
			}else{
				$permission_data = $CI->session->userdata("permission_data");
				if((isset($permission_data["contact"]["index"]) && $permission_data["contact"]["index"] == 1)){
					$show = true;
					$stat_contact["index_method"] = (bool)$permission_data["contact"]["index"];
				}
			}
		}

		if(!$show){
			return "";
		}
		
		$CI->load->model("Contact_model");
		
		$stat_contact['data']["contact_make_copies"] = $CI->pg_module->get_module_config("contact", "contact_make_copies");
		
		if($stat_contact['data']["contact_make_copies"]) {
			$stat_contact['data']["contact_need_approve"] = $CI->pg_module->get_module_config("contact", "contact_need_approve");
			
			$stat_contact['data']["all"] = $CI->Contact_model->get_all_contacts_count();
			$stat_contact['data']["unread"] = $CI->Contact_model->get_unread_contacts_count();
			
			if($stat_contact['data']["contact_need_approve"]){
				$stat_contact['data']["need_approve"] = $CI->Contact_model->get_need_approve_contacts_count();
			}
		}		
		$CI->template_lite->assign("stat_contact", $stat_contact);
		
		// show template from contact module default user theme
		$html = $CI->template_lite->fetch("helper_home_contact_block", "admin", "contact");
		return $html;
	}
}

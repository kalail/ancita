<?php

define("CONTACTS_TABLE", DB_PREFIX."contacts");

/**
 * Contact Model
 * 
 * @package PG_RealEstate
 * @subpackage Contact
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Contact_model extends Model{	
	/**
	 * link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;

	/**
	 * link to DataBase object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Attributes of contact object
	 * 
	 * @var array
	 */
	var $fields = array(
		"id",
		"user_id",
		"listing_id",
		"sender",
		"email",
		"phone",
		"message",
		"status",
		"allow_send",
		"send",
		"date_add",
	);
	
	/**
	 * Format settings
	 * 
	 * @var array
	 */
	private $format_settings = array(
		"use_format" => true,
		"get_user" => true,
	);

	/**
	 * Moderation type of contact object
	 * 
	 * @var string
	 */
	private $moderation_type = "contact";
	
	/**
	 * Constructor
	 *
	 * @return Contact_model
	 */
	function __construct()
	{
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return contact by identifier
	 * 
	 * @param integer $contact_id contact identifier
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_contact_by_id($contact_id, $formatted=false){
		if(!$contact_id) return false;
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(CONTACTS_TABLE);
		$this->DB->where("id", $contact_id);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data = array($results[0]);
			if($formatted){
				$data = $this->format_contact($data);
			}
			return $data[0];
		}
		return array();
	}
	
	/**
	 * Save contact to data source
	 * 
	 * @param integer $contact_id contact identifier
	 * @param array $data contact data
	 * @return integer
	 */
	public function save_contact($contact_id, $data){
		if(!$contact_id){
			if(!isset($data["email"])){
				$data["email"] = "";
			}
			if(!isset($data["sender"])){
				$data["sender"] = "";
			}
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(CONTACTS_TABLE, $data);
			$contact_id = $this->DB->insert_id();
		}else{
			$this->DB->where("id", $contact_id);
			$this->DB->update(CONTACTS_TABLE, $data);
		}
		return $contact_id;
	}
	
	/**
	 * Remove contact from data source
	 * 
	 * @param integer $contact_id contact identifier
	 * @return boolean
	 */
	public function delete_contact($contact_id){
		$this->DB->where("id", $contact_id);
		$this->DB->delete(CONTACTS_TABLE);
		return true;
	}
		
	/**
	 * Return filtered contact objects as array
	 * 
	 * @param integer $page page of results
	 * @param string $limits results per page
	 * @param array $order_by sorting data
	 * @param array $params filters parameters
	 * @param boolean $formatted format results
	 * @return array
	 */
	private function _get_contacts_list($page=null, $limits=null, $order_by=null, $params=array(), $formatted=true){
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(CONTACTS_TABLE);
		
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->fields)){
					$this->DB->order_by($field . " " . $dir);
				}
			}
		}elseif($order_by){
			$this->DB->order_by($order_by);
		}

		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r) $data[] = $r;
			if($formatted) $data = $this->format_contact($data);
			return $data;
		}
		return array();
	}
	
	/**
	 * Return number of filters contacts objects
	 * 
	 * @param array $params filters parameters
	 * @return integer
	 */
	private function _get_contacts_count($params=null){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(CONTACTS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}
	
	/**
	 * Return all contact objects as array
	 * 
	 * @param integer $page page of results
	 * @param string $limits results per page
	 * @param array $order_by sirting data
	 * @return array
	 */
	public function get_all_contacts_list($page=null, $limits=null, $order_by=null){
		return $this->_get_contacts_list($page, $limits, $order_by);
	}
	
	/**
	 * Return number of all contact objects
	 * 
	 * @return integer
	 */
	public function get_all_contacts_count(){
		return $this->_get_contacts_count();
	}
	
	/**
	 * Return unread contact onbjects as array
	 * 
	 * @param integer $page page of results
	 * @param string $limits results per page
	 * @param array $order_by sorting data
	 * @return array
	 */
	public function get_unread_contacts_list($page=null, $limits=null, $order_by=null){
		$params = array("where"=>array("status"=>"0"));
		return $this->_get_contacts_list($page, $limits, $order_by, $params);
	}
	
	/**
	 * Return number of unread contact objects
	 * 
	 * @return integer
	 */
	public function get_unread_contacts_count(){
		$params = array("where"=>array("status"=>"0"));
		return $this->_get_contacts_count($params);
	}
	
	/**
	 * Return need approve contact objects as array
	 * 
	 * @param integer $page page of results
	 * @param string $limits results per page
	 * @param array $order_by sorting data
	 * @return array
	 */
	public function get_need_approve_contacts_list($page=null, $limits=null, $order_by=null){
		$params = array("where"=>array("allow_send"=>"1", "send"=>"0"));
		return $this->_get_contacts_list($page, $limits, $order_by, $params);
	}
	
	/**
	 * Return number of need approve contact objects
	 * 
	 * @return integer
	 */
	public function get_need_approve_contacts_count(){
		$params = array("where"=>array("allow_send"=>"1", "send"=>"0"));
		return $this->_get_contacts_count($params);
	}
	
	/**
	 * Validate contact data
	 * 
	 * @param integer $contact_id contact identifier
	 * @param array $data contact data
	 * @return array
	 */
	public function validate_contact($contact_id, $data){
		$return = array("errors"=>array(), "data"=>array());

		// id
		if(isset($data["id"])){
			$return["data"]["id"] = intval($data["id"]);
		}

		// user
		if(isset($data["user_id"])){
			$return["data"]["user_id"] = intval($data["user_id"]);
			if(empty($return["data"]["user_id"])) $return["errors"][] = l("error_empty_user", "contact");
		}elseif(!$contact_id){
			$return["errors"][] = l("error_empty_user", "contact");
		}
	
		// sender
		if(isset($data["sender"])){
			$return["data"]["sender"] = trim(strip_tags($data["sender"]));
			if(empty($return["data"]["sender"])) $return["errors"][] = l("error_empty_sender", "contact");
		}elseif(!$contact_id){
			$return["errors"][] = l("error_empty_sender", "contact");
		}
		
		// email
		if(isset($data["email"])){
			$return["data"]["email"] = trim(strip_tags($data["email"]));
			if(empty($return["data"]["email"])){
				$return["errors"][] = l("error_empty_email", "contact");
			}else{
				$this->CI->config->load("reg_exps", TRUE);
				$email_expr = $this->CI->config->item("email", "reg_exps");
				if(!preg_match($email_expr, $return["data"]["email"])){
					$return["errors"][] = l("error_invalid_email", "contact");
				}
			}
		}elseif(!$contact_id){
			$return["errors"][] = l("error_empty_email", "contact");
		}
		
		// phone
		if(isset($data["phone"])){
			$return["data"]["phone"] = trim(strip_tags($data["phone"]));
			//if(empty($return["data"]["phone"])){
			//	$return["errors"][] = l("error_empty_phone", "contact");
			//}else{
			//	$this->CI->load->helper("start");
			//	$phone_format = get_phone_format('regexp');
			//	if($phone_format !== false && !preg_match($phone_format, $return["data"]["phone"])){
			//		$return["errors"][] = l("error_phone_incorrect", "users");
			//	}
		//	}
	//	}elseif(!$contact_id){
		//	$return["errors"][] = l("error_empty_phone", "contact");
		}
		
		// message
		if(isset($data["message"])){
			$return["data"]["message"] = trim(strip_tags($data["message"]));
			if(empty($return["data"]["message"])){
				$return["errors"][] = l("error_empty_message", "contact");
			}else{
				$this->CI->load->model("moderation/models/Moderation_badwords_model");
				$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return["data"]["message"]);
				if($bw_count){
					$return["errors"][] = l("error_badwords_message", "contact");
				}
			}
		}elseif(!$contact_id){
			$return["errors"][] = l("error_empty_message", "contact");
		}
		
		// status
		if(isset($data["status"])){
			$return["data"]["status"] = $data["status"] ? 1 : 0;
		}
		//listingid
		if(isset($data["listing_id"])){
			$return["data"]["listing_id"] = $data["listing_id"];
		}
		// allow send
		if(isset($data["allow_send"])){
			$return["data"]["allow_send"] = $data["allow_send"] ? 1 : 0;
		}
		
		// send
		if(isset($data["send"])){
			$return["data"]["send"] = $data["send"] ? 1 : 0;
		}
		
		return $return;
	}
	
	/**
	 * Validate contact settings
	 * @param array $data
	 * @return array
	 */
	public function validate_contact_settings($data){
		$return = array("errors"=> array(), "data" => array());

		$return["data"]["contact_make_copies"] = $data["contact_make_copies"] ? 1: 0;
		$return["data"]["contact_need_approve"] = $data["contact_need_approve"] ? 1 : 0;
		$return["data"]["contact_send_mail"] = $data["contact_send_mail"] ? 1 : 0;
		
		
		/// email
		if(isset($data['contact_admin_email'])){
			$return["data"]["contact_admin_email"] = trim(strip_tags($data["contact_admin_email"]));

			if(!empty($return["data"]["contact_admin_email"])){
				$this->CI->config->load("reg_exps", TRUE);
				$email_expr = $this->CI->config->item("email", "reg_exps");
				if(!preg_match($email_expr, $return["data"]["contact_admin_email"])){
					$return["errors"][] = l("error_invalid_email", "contact");
				}
			}elseif($return["data"]["contact_send_mail"]){
				$return["errors"][] = l("error_empty_email", "contact");
			}
		}

		return $return;
	}
	
	/**
	 * Mark contact object as read
	 * 
	 * @param integer $contact_id contact identifier
	 * @return void
	 */
	public function mark_contact_as_read($contact_id){
		$data["status"] = "1";
		$this->save_contact((int)$contact_id, $data);
	}
	
	/**
	 * Mark contact object as unread
	 * @param integer $contact_id contact identifier
	 * @return void
	 */
	public function mark_contact_as_unread($contact_id){
		$data["status"] = "0";
		$this->save_contact((int)$contact_id, $data);
	}
	
	/**
	 * Send contact to user
	 * @param integer $contact_id contact identifier
	 * @return boolean
	 */
	public function send_contact($contact_id){
		if(!$contact_id) return false;		
		
		$data = $this->get_contact_by_id($contact_id);
		if(empty($data) || $data["send"]){
			return false;
		}

		//get user email
		$this->CI->load->model("Users_model");
		$this->CI->Users_model->set_format_settings('get_safe', false);
		$user = $this->CI->Users_model->get_user_by_id($data["user_id"]);
		$this->CI->Users_model->set_format_settings('get_safe', true);
		$email = $user["email"];
		$adminemail = 'info@ancita.com';
		$data['user'] = $user["output_name"];
		
		//send email
		 $this->CI->load->model("Notifications_model");

		 $this->CI->Notifications_model->send_notification($email, "user_guest_contact", $data, '', $user['lang_id']);
			  
	   //admin mail
	    $this->CI->Notifications_model->send_notification($adminemail, "user_guest_contact", $data, '', $user['lang_id']);   
		 unset($data["user"]);
		
		//update		
		$data["send"] = "1";
		$this->save_contact($contact_id, $data);
		return true;
	}
	
	/**
	 * Format contact data
	 * 
	 * @param array $data contact data
	 * @return array
	 */
	public function format_contact($data){
		if(!$this->format_settings["use_format"]){
			return $data;
		}
		
		$users_search = array();
		
		foreach($data as $key=>$contact){
			$data[$key] = $contact;
			//	"get_user"
			if($this->format_settings["get_user"]){
				$users_search[] = $contact["user_id"];
			}
			$data[$key]["message"] = nl2br($contact["message"]);
		}
		
		if($this->format_settings["get_user"] && !empty($users_search)){
			$this->CI->load->model("Users_model");
			$users_data = $this->CI->Users_model->get_users_list_by_key(null, null, null, array(), $users_search);
			foreach($data as $key=>$contact){
				$data[$key]["user"] = (isset($users_data[$contact["user_id"]])) ? 
					$users_data[$contact["user_id"]] : $this->CI->Users_model->format_default_user($contact["user_id"]);
			}
		}
		
		return $data;
	}
}

<?php

$install_lang["admin_contact_menu_contact_list_item"] = "Contactos";
$install_lang["admin_contact_menu_contact_list_item_tooltip"] = "";
$install_lang["admin_contact_menu_contact_settings_item"] = "Configuración";
$install_lang["admin_contact_menu_contact_settings_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_feedbacks-items_contact_menu_item"] = "Contactos de clientes";
$install_lang["admin_menu_settings_items_feedbacks-items_contact_menu_item_tooltip"] = "Gestión de contactos invitados con los proveedores del listado";


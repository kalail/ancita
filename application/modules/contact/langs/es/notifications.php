<?php

$install_lang["notification_auser_guest_contact"] = "Contacto de invitados (por admin)";
$install_lang["notification_user_guest_contact"] = "Contacto de usuario";
$install_lang["tpl_auser_guest_contact_content"] = "Hola admin,\n\nHay un contacto nuevo invitado en [domain]. Panel de administración de acceso > Retroalimentación > Contactos de clientes para verlo.\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_auser_guest_contact_subject"] = "[ domain] | contacto Invitado";
$install_lang["tpl_user_guest_contact_content"] = "Hola [user],\n\nUsuario [sender] ([email], [phone])\nle envía un mensaje:\n\n[message]\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_user_guest_contact_subject"] = "Usuario [sender] le envía un mensaje";


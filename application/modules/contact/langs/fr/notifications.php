<?php

$install_lang["notification_auser_guest_contact"] = "Formule de contacte pour client";
$install_lang["notification_user_guest_contact"] = "Contact par utilisateur";
$install_lang["tpl_auser_guest_contact_content"] = "Bonjour admin,\n\nIl y a un nouveau contact sur [domain]. Accédez au panneau d'aministration > Commentaires > Contactes clients pour le voir.\n\nCordialement,\n[name_from]";
$install_lang["tpl_auser_guest_contact_subject"] = "[domain] | Contacte client";
$install_lang["tpl_user_guest_contact_content"] = "Bonjour [user],\n\nUtilisateur [sender] ([email], [phone])\nvous a envoyé un message:\n\n[message]\n\nCordialement,\n[name_from]";
$install_lang["tpl_user_guest_contact_subject"] = "Utilisateur [sender] vous a envoyé un message";


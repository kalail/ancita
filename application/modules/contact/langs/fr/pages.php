<?php

$install_lang["admin_header_contact_list"] = "Contactes clients";
$install_lang["admin_header_contact_settings"] = "Paramètres";
$install_lang["admin_header_contact_show"] = "Contacte";
$install_lang["admin_header_contacts_list"] = "Contactes";
$install_lang["error_badwords_message"] = "Message contient des jurons";
$install_lang["error_empty_code"] = "Code vide";
$install_lang["error_empty_email"] = "Courriel vide";
$install_lang["error_empty_message"] = "Message vide";
$install_lang["error_empty_phone"] = "Téléphone vide";
$install_lang["error_empty_sender"] = "Expéditeur vide";
$install_lang["error_empty_user"] = "Utilisateur vide";
$install_lang["error_invalid_captcha"] = "Code invalide";
$install_lang["error_invalid_email"] = "Courriel invalide";
$install_lang["error_phone_incorrect"] = "Téléphone incorrect";
$install_lang["field_contact_admin_email"] = "Courriel de l'administrateur";
$install_lang["field_contact_captcha"] = "Code";
$install_lang["field_contact_date_add"] = "Date";
$install_lang["field_contact_email"] = "Courriel";
$install_lang["field_contact_make_copies"] = "Garder copies dans le panneau admin";
$install_lang["field_contact_message"] = "Message";
$install_lang["field_contact_need_approve"] = "Contacte clients";
$install_lang["field_contact_nosend"] = "Ne pas enoyer";
$install_lang["field_contact_optional"] = "Optionel";
$install_lang["field_contact_phone"] = "# de téléphone";
$install_lang["field_contact_send"] = "Envoyer";
$install_lang["field_contact_send_mail"] = "Envoyer courriel à un admin";
$install_lang["field_contact_send_status"] = "Envoyer statut";
$install_lang["field_contact_sender"] = "Nom";
$install_lang["link_decline"] = "Décliner";
$install_lang["link_decline_contact"] = "Décliner contact";
$install_lang["link_delete_contact"] = "Détruire contact";
$install_lang["link_read_contact"] = "Marquer comme lu";
$install_lang["link_send"] = "Envoyer";
$install_lang["link_send_contact"] = "Envoyer contact";
$install_lang["link_show_contact"] = "Montrer contact";
$install_lang["link_unread_contact"] = "Marquer comme non lu";
$install_lang["no_contacts"] = "Aucun contact";
$install_lang["note_delete_all_contacts"] = "Êtes vous sur de vouloir détruire ces contacts?";
$install_lang["note_delete_contact"] = "Êtes vous sur de vouloir détruire ce contact?";
$install_lang["stat_header_contact"] = "Contacts en attente d'approbation";
$install_lang["stat_header_contact_all"] = "Tout";
$install_lang["stat_header_contact_need_approve"] = "En attente";
$install_lang["stat_header_contact_unread"] = "Non lu";
$install_lang["success_contact_delete"] = "Contact détruit";
$install_lang["success_contact_save"] = "Contact sauvé";
$install_lang["success_contact_send"] = "Contact envoyé";
$install_lang["success_settings_saved"] = "Changements sauvés";


<?php

$install_lang["admin_contact_menu_contact_list_item"] = "Contactes";
$install_lang["admin_contact_menu_contact_list_item_tooltip"] = "";
$install_lang["admin_contact_menu_contact_settings_item"] = "Paramètres";
$install_lang["admin_contact_menu_contact_settings_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_feedbacks-items_contact_menu_item"] = "Contactes clients";
$install_lang["admin_menu_settings_items_feedbacks-items_contact_menu_item_tooltip"] = "Gèrer clients avec provisioneurs d'annonce";


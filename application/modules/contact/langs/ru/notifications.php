<?php

$install_lang["notification_auser_guest_contact"] = "Сообщение от гостя сайта (для администратора)";
$install_lang["notification_user_guest_contact"] = "Сообщение от пользователя";
$install_lang["tpl_auser_guest_contact_content"] = "Здравствуйте, администратор!\n\nВам пришло новое сообщение от гостя с сайта [domain]. Для просмотра зайдите в панель администратора > Обратная связь > Контакты гостей.\n\nС уважением,\n[name_from]";
$install_lang["tpl_auser_guest_contact_subject"] = "[domain] | Сообщение от гостя";
$install_lang["tpl_user_guest_contact_content"] = "Здравствуйте, [user]!\n\nПользователь [sender] ([email], [phone])\nотправил Вам сообщение:\n\n[message]\n\nС наилучшими пожеланиями,\n[name_from]";
$install_lang["tpl_user_guest_contact_subject"] = "Сообщение от [sender]";


<?php

$install_lang["notification_auser_guest_contact"] = "Kontakt von Gast (für Admin)";
$install_lang["notification_user_guest_contact"] = "Kontakt von Benutzer";
$install_lang["tpl_auser_guest_contact_content"] = "Hallo admin,\n\nEs gibt einen neuen Gast Kontakt auf [domain]. Gehe zum Administrationsbereich > Bewertungen > Gast Kontakte um ihn anzuzeigen.\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_auser_guest_contact_subject"] = "[domain] Gast Kontakt";
$install_lang["tpl_user_guest_contact_content"] = "Hallo [user],\n\nBenutzer [sender] ([email], [phone])\nhat eine Nachricht an Sie geschickt:\n\n[message]\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_user_guest_contact_subject"] = "Benutzer [sender] hat Ihnen eine Nachricht geschickt";


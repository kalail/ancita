<?php

$install_lang["admin_header_contact_list"] = "Контакти от гости";
$install_lang["admin_header_contact_settings"] = "Настройки";
$install_lang["admin_header_contact_show"] = "Контакт";
$install_lang["admin_header_contacts_list"] = "Контакти";
$install_lang["error_badwords_message"] = "Съобщението съдържа забранени думи";
$install_lang["error_empty_code"] = "Не е посочен код";
$install_lang["error_empty_email"] = "Не е посочен email";
$install_lang["error_empty_message"] = "Празно съобщение";
$install_lang["error_empty_phone"] = "Не е посочен телефон";
$install_lang["error_empty_sender"] = "Не е посочен изпращач";
$install_lang["error_empty_user"] = "Не е посочен потребител";
$install_lang["error_invalid_captcha"] = "Неверен код";
$install_lang["error_invalid_email"] = "Неверен email";
$install_lang["error_phone_incorrect"] = "Неверен телефон";
$install_lang["field_contact_admin_email"] = "Адрес на администратора";
$install_lang["field_contact_captcha"] = "Код";
$install_lang["field_contact_date_add"] = "Дата";
$install_lang["field_contact_email"] = "Email";
$install_lang["field_contact_make_copies"] = "Запази копия в административния раздел";
$install_lang["field_contact_message"] = "Съобщение";
$install_lang["field_contact_need_approve"] = "Трябва одобрение на контакта";
$install_lang["field_contact_nosend"] = "Не е изпратено";
$install_lang["field_contact_optional"] = "не е задължително";
$install_lang["field_contact_phone"] = "Телефонен номер";
$install_lang["field_contact_send"] = "Изпрати";
$install_lang["field_contact_send_mail"] = "Изпрати уведомление до администратора";
$install_lang["field_contact_send_status"] = "Изпрати статус";
$install_lang["field_contact_sender"] = "Име";
$install_lang["link_decline"] = "Отклони";
$install_lang["link_decline_contact"] = "Отклони контакт";
$install_lang["link_delete_contact"] = "Премахни контакт";
$install_lang["link_read_contact"] = "Отбележи като прочетено";
$install_lang["link_send"] = "Изпрати";
$install_lang["link_send_contact"] = "Изпрати контакт";
$install_lang["link_show_contact"] = "Покажи контакт";
$install_lang["link_unread_contact"] = "Отбележи като непрочетено";
$install_lang["no_contacts"] = "Няма контакти";
$install_lang["note_delete_all_contacts"] = "Сигурни ли сте, че искате да премахнете всички контакти?";
$install_lang["note_delete_contact"] = "Сигурни ли сте, че искате да премахнете този контакт?";
$install_lang["stat_header_contact"] = "Контакти очакващи одобрение";
$install_lang["stat_header_contact_all"] = "Всички";
$install_lang["stat_header_contact_need_approve"] = "Очакват одобрение";
$install_lang["stat_header_contact_unread"] = "Непрочетени";
$install_lang["success_contact_delete"] = "Контактът е премахнат";
$install_lang["success_contact_save"] = "Контактът е запазен";
$install_lang["success_contact_send"] = "Контактът е успешно изпратен";
$install_lang["success_settings_saved"] = "Измененията са запазени";


<?php

$install_lang["notification_auser_guest_contact"] = "Съобщение от гости на сайта (за администратора)";
$install_lang["notification_user_guest_contact"] = "Съобщение от потребител";
$install_lang["tpl_auser_guest_contact_content"] = "Здравейте, администратор!\n\nЗа вас пристигна ново съобщение от посетител на сайта [domain]. За преглед влезте в панела на администратора > Обратна връзка > Контакти с посетители.\n\nС уважение,\n[name_from]";
$install_lang["tpl_auser_guest_contact_subject"] = "[domain] | Съобщение от посетител";
$install_lang["tpl_user_guest_contact_content"] = "Здравейте, [user]!\n\nПотребителят [sender] ([email], [phone])\nви е изпратил съобщение:\n\n[message]\n\nС най-добри пожелания,\n[name_from]";
$install_lang["tpl_user_guest_contact_subject"] = "Съобщение за вас от [sender] ";


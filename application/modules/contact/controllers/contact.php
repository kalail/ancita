<?php 

/**
 * Contact user side controller
 * 
 * @package PG_RealEstate
 * @subpackage Contact
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Contact extends Controller{
	
	/**
	 * Constructor
	 *
	 * @return Contact
	 */
	function __construct(){
		parent::Controller();
		$this->load->model("Contact_model");

		/*if ($this->session->userdata("auth_type") == "user") {
			show_404();
		}*/		
	}
	
	/**
	 * Send message from ajax contact form in agencys page
	 */
	public function ajax_send_message(){
		$post_data["user_id"] = intval($this->input->post("user_id"));
		$post_data["sender"] = $this->input->post("sender", true);
		$post_data["phone"] = $this->input->post("phone", true);
		$post_data["email"] = $this->input->post("email", true);
		$post_data["message"] = $this->input->post("message", true);
		$post_data["code"] = $this->input->post("code", true);

		$make_copies = $this->pg_module->get_module_config("contact", "contact_make_copies");
		$need_approve = $this->pg_module->get_module_config("contact", "contact_need_approve");

		$validate_data = $this->Contact_model->validate_contact(null, $post_data);

		$is_captcha_valid = ($this->input->post("code", true) == $this->session->userdata("captcha_word")) ? 1 : 0;
		if(!$is_captcha_valid){
			$validate_data["errors"][] = l("error_invalid_captcha", "contact");
		}
				
		if(!empty($validate_data["errors"])){
			$return["error"] = implode("<br>", $validate_data["errors"]);
			echo json_encode($return);
			return;
		}else{
				$validate_data["data"]["send"] = "1";	
				$send_admin_mail = $this->pg_module->get_module_config("contact", "contact_send_mail");
				if($send_admin_mail){
					$email = $this->pg_module->get_module_config("contact", "contact_admin_email");
					if($email){
						$this->load->model("Notifications_model");	
						$this->Notifications_model->send_notification($email, "auser_guest_contact", array());
					}
				}		
				//get user email
				$this->load->model("Users_model");
				$user = $this->Users_model->get_user_by_id($validate_data["data"]["user_id"]);
				$email = ($user["contact_email"] != '')?$user["contact_email"]:$user["email"];			
				//send mail
				$this->load->model("Notifications_model");
				$this->Notifications_model->send_notification($email, "user_guest_contact", $validate_data["data"], '', $this->pg_language->current_lang_id);
					
			if($make_copies){
				$this->Contact_model->save_contact(null, $validate_data["data"]);
			}
				
			$return["success"] = l("success_contact_save", "contact");
			echo json_encode($return);
			return;
		}
		return;
	}
	
	
	public function ajax_consend_message(){
		$post_data["user_id"] = $this->input->post("user_id");
		$post_data["sender"] = $this->input->post("sender", true);
		$post_data["phone"] = $this->input->post("phone", true);
		$post_data["email"] = $this->input->post("email", true);
		$post_data["message"] = $this->input->post("message", true);
		$post_data["listing_id"]= $this->input->post("listing_id", true);
		$id = $post_data["listing_id"];
        $validate_data = $this->Contact_model->validate_contact(null, $post_data);
				   $validate_data["data"]["send"] = "1";
			     // send admin mail
                $email = $this->pg_module->get_module_config("contact", "contact_admin_email");
				 if($email){
					$this->load->model("Notifications_model");	
					$this->Notifications_model->send_notification($email, "auser_guest_contact", array());
			     }	
				//get agent email
				$this->load->model('Users_model');
                $user = $this->Users_model->get_user_by_id($post_data["user_id"]);
                $agentemail = ($user["contact_email"] != '')?$user["contact_email"]:$user["email"];
				
				//send agent mail
                $this->load->model("Notifications_model");
				$this->Notifications_model->send_notification($agentemail, "user_guest_contact", $validate_data["data"], '', $this->pg_language->current_lang_id,$id);
		        if($this->input->post("macode", true) == $this->input->post("macaptcha_word", true)){
				if ($this->Contact_model->save_contact(null, $validate_data["data"])) {
                echo 1;exit;// Mail sent
		        }else{
		        echo 2;exit;// Mail not send
		       }
		       }   
	       }
	/**
	 * Send message from ajax contact form 
	 */
	public function ajax_check_code(){
		$is_captcha_valid = $this->input->get("code", true) == $this->session->userdata("captcha_word") ? "true" : "false";
		echo $is_captcha_valid;
		return;
	}
}

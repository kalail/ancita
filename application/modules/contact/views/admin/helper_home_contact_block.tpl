{if $stat_contact.index_method && $stat_contact.data.contact_make_copies}
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan=2>{l i='stat_header_contact' gid='contact'}</th>
	</tr>
	<tr>
		<td class="first"><a href="{$site_url}admin/contact/index/all">{l i='stat_header_contact_all' gid='contact'}</a></td>
		<td class="w30"><a href="{$site_url}admin/contact/index/all">{$stat_contact.data.all}</a></td>
	</tr>
	<tr class="zebra">
		<td class="first"><a href="{$site_url}admin/contact/index/unread">{l i='stat_header_contact_unread' gid='contact'}</a></td>
		<td class="w30"><a href="{$site_url}admin/contact/index/unread">{$stat_contact.data.unread}</a></td>
	</tr>
	{if $stat_contact.data.contact_need_approve}
	<tr>
		<td class="first"><a href="{$site_url}admin/contact/index/need_approve">{l i='stat_header_contact_need_approve' gid='contact'}</a></td>
		<td class="w30"><a href="{$site_url}admin/contact/index/need_approve">{$stat_contact.data.need_approve}</a></td>
	</tr>
	{/if}
</table>
{/if}

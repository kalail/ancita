{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_contact_menu'}
<div class="actions">&nbsp;</div>

<form method="post" action="{$data.action|escape}" name="save_form">
	<div class="edit-form n150">
		<div class="row header">{l i='admin_header_contact_settings' gid='contact'}</div>
		<div class="row">
			<div class="h"><label for="contact_settings_make_copies">{l i='field_contact_make_copies' gid='contact'}</label>: </div>
			<div class="v">
				<input type="hidden" name="contact_make_copies" value="0"> 
				<input type="checkbox" name="contact_make_copies" value="1" id="contact_settings_make_copies" {if $data.contact_make_copies}checked="checked"{/if}> 
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><label for="contact_settings_need_approve">{l i='field_contact_need_approve' gid='contact'}</label>: </div>
			<div class="v">
				<input type="hidden" name="contact_need_approve" value="0"> 
				<input type="checkbox" name="contact_need_approve" value="1" id="contact_settings_need_approve" {if $data.contact_need_approve}checked="checked"{/if} {if !$data.contact_make_copies}disabled{/if}> 
			</div>
		</div>		
		<div class="row">
			<div class="h"><label for="contact_send_mail">{l i='field_contact_send_mail' gid='contact'}</label>: </div>
			<div class="v">
				<input type="hidden" name="contact_send_mail" value="0"> 
				<input type="checkbox" name="contact_send_mail" value="1" id="contact_send_mail" {if $data.contact_send_mail}checked="checked"{/if}> 
				&nbsp;&nbsp;
				{l i='field_contact_admin_email' gid='contact'}
				<input type="text" name="contact_admin_email" value="{$data.contact_admin_email|escape}" id="contact_admin_email" {if !$data.contact_send_mail}disabled{/if}> 
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
</form>
<div class="clr"></div>
<script>{literal}
$(function(){
	$('#contact_settings_make_copies').bind('change', function(){
		if(this.checked){
			$('#contact_settings_need_approve').removeAttr('disabled');
		}else{
			$('#contact_settings_need_approve').removeAttr('checked').attr('disabled', 'disabled');
			$('#contact_send_mail').removeAttr('checked').attr('disabled', 'disabled');
			$('#contact_admin_email').attr('disabled', 'disabled');
		}
	});
	$('#contact_settings_need_approve').bind('change', function(){
		if(this.checked){
			$('#contact_send_mail').removeAttr('disabled');
		}else{
			$('#contact_send_mail').removeAttr('checked').attr('disabled', 'disabled');
			$('#contact_admin_email').attr('disabled', 'disabled');
		}
	});
	$('#contact_send_mail').bind('change', function(){
		if(this.checked){
			$('#contact_admin_email').removeAttr('disabled');
		}else{
			$('#contact_admin_email').attr('disabled', 'disabled');
		}
	});
});
{/literal}</script>

{include file="footer.tpl"}

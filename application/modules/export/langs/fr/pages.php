<?php

$install_lang["admin_header_driver_edit"] = "Pilotes";
$install_lang["admin_header_driver_field_create"] = "Formules de pilotes";
$install_lang["admin_header_driver_field_edit"] = "Formules de pilotes";
$install_lang["admin_header_driver_fields"] = "Formules de pilotes";
$install_lang["admin_header_export"] = "Exporter";
$install_lang["admin_header_export_edit"] = "Paramètres de pilotes";
$install_lang["admin_header_selections_edit"] = "Éditer objet";
$install_lang["admin_header_selections_form_edit"] = "Critère";
$install_lang["admin_header_selections_general_edit"] = "Paramètres généraux";
$install_lang["btn_currency_rates_update"] = "Mettre à jour";
$install_lang["btn_driver_field_create"] = "Ajouter";
$install_lang["btn_driver_field_resort"] = "Changer commande";
$install_lang["driver_csv_field_name"] = "Nom";
$install_lang["driver_csv_field_price"] = "Prix";
$install_lang["driver_info"] = "Détails";
$install_lang["driver_key_notrequired"] = "Clé pas requise";
$install_lang["driver_name_csv"] = ".CSV";
$install_lang["driver_name_oodle"] = "Oodle";
$install_lang["driver_name_synda_agents"] = "Agents SyndaFeed";
$install_lang["driver_name_synda_listings"] = "Annonces SyndaFeed";
$install_lang["driver_name_tff"] = "format de feed Trulia.com";
$install_lang["driver_name_vast"] = "format de feed Vast.com";
$install_lang["driver_name_xml"] = ".XML";
$install_lang["driver_name_yrl"] = "Yandex.Properties";
$install_lang["driver_name_zif"] = "Zillow Interchage Format";
$install_lang["driver_registration"] = "Obtenir clé";
$install_lang["error_empty_driver"] = "Pilote vide";
$install_lang["error_empty_field_name"] = "Nom de champ vide";
$install_lang["error_empty_field_type"] = "Type de champ vide";
$install_lang["error_empty_gid"] = "GID de pilote vide";
$install_lang["error_empty_link"] = "Champ personnalisé vide";
$install_lang["error_empty_name"] = "Nom vide";
$install_lang["error_empty_object"] = "Objet vide";
$install_lang["error_empty_output_type"] = "Type d'output vide";
$install_lang["error_empty_relations"] = "Relations vide";
$install_lang["error_empty_scheduler"] = "Programmeur vide";
$install_lang["error_empty_start_type"] = "Type de début vide";
$install_lang["error_invalid_field_type"] = "Type de champ vide";
$install_lang["error_selection_activated"] = "Pas toutes vos connections sont établies";
$install_lang["field_csv_delimiter"] = "Délimiteur";
$install_lang["field_csv_enclosure"] = "Enclos";
$install_lang["field_custom_link"] = "Champ";
$install_lang["field_custom_name"] = "Nom";
$install_lang["field_custom_type"] = "Type";
$install_lang["field_driver_link"] = "Authorisation";
$install_lang["field_driver_name"] = "Nom";
$install_lang["field_driver_output"] = "Format output";
$install_lang["field_driver_regkey"] = "Clé";
$install_lang["field_driver_status"] = "Actif";
$install_lang["field_selection_append_date"] = "Date d'append";
$install_lang["field_selection_date_created"] = "Date crée";
$install_lang["field_selection_driver"] = "Pilote";
$install_lang["field_selection_editable"] = "Permettre aux utilisateurs de changer le critère";
$install_lang["field_selection_file_format"] = "Format de filières";
$install_lang["field_selection_module"] = "Module";
$install_lang["field_selection_name"] = "Nom";
$install_lang["field_selection_output_type"] = "Comment exporter";
$install_lang["field_selection_published"] = "Permettre l'export";
$install_lang["field_selection_scheduler"] = "Quand exporter";
$install_lang["field_selection_status"] = "Statut";
$install_lang["field_setting_name"] = "Nom";
$install_lang["field_setting_required"] = "Requis";
$install_lang["field_setting_type"] = "Type";
$install_lang["field_type_file"] = "Filière";
$install_lang["field_type_int"] = "Nombres entiers";
$install_lang["field_type_text"] = "Texte";
$install_lang["field_type_url"] = "URL";
$install_lang["filter_section_custom_fields"] = "Champs personnalisés";
$install_lang["filter_section_general"] = "Général";
$install_lang["filter_section_search_form"] = "Critère de sélection";
$install_lang["form_driver_fields_header"] = "Champ en exportation";
$install_lang["form_module_fields_header"] = "Champ de base de données";
$install_lang["header_my_export"] = "Exporter";
$install_lang["link_activate_driver"] = "Activer pilote";
$install_lang["link_activate_selection"] = "Activer sélection";
$install_lang["link_custom_field_delete"] = "Supprimer champ personnalisé";
$install_lang["link_custom_field_edit"] = "Édit champ personnalisé";
$install_lang["link_deactivate_driver"] = "Déactiver pilote";
$install_lang["link_deactivate_selection"] = "Désactiver sélection";
$install_lang["link_delete_relation"] = "Désactiver connection";
$install_lang["link_delete_selection"] = "Désactiver sélection";
$install_lang["link_edit_driver"] = "Éditer pilote";
$install_lang["link_edit_selection"] = "Éditer sélection";
$install_lang["link_export_data"] = "Données exportées";
$install_lang["link_export_form"] = "Formule d'export";
$install_lang["link_generate_selection"] = "Export";
$install_lang["no_custom_fields"] = "Aucun champs";
$install_lang["no_driver_fields"] = "Aucun champs";
$install_lang["no_drivers"] = "Aucun pilotes";
$install_lang["no_selections"] = "Aucune sélections";
$install_lang["note_custom_field_delete"] = "Êtes vous sur de vouloir supprimer le champ?";
$install_lang["note_custom_field_delete_all"] = "Êtes vous sur de vouloir supprimer les champs?";
$install_lang["note_delete_selection"] = "Êtes vous sur de vouloir supprimer la sélection?";
$install_lang["note_selections_delete_all"] = "Êtes vous sur de vouloir supprimer les sélections?";
$install_lang["object_form_fields"] = "Champs de formule";
$install_lang["output_browser"] = "Navigateur";
$install_lang["output_file"] = "Filière";
$install_lang["start_cron"] = "par cronjob";
$install_lang["start_in_time"] = "sur cette date";
$install_lang["start_manual"] = "par demande";
$install_lang["start_type_day"] = "Jour";
$install_lang["start_type_month"] = "Mois";
$install_lang["start_type_since"] = "de";
$install_lang["start_type_week"] = "Semaine";
$install_lang["success_driver_updated"] = "Paramètres de pilote mis à jour";
$install_lang["success_field_created"] = "Champ crée";
$install_lang["success_field_updated"] = "Champ mis à jour";
$install_lang["success_form_updated"] = "Champ mis à jour";
$install_lang["success_relation_created"] = "Connection crée";
$install_lang["success_relation_deleted"] = "Connexion supprimée";
$install_lang["success_selection_created"] = "Sélection crée";
$install_lang["success_selection_deleted"] = "Sélection supprimée";
$install_lang["success_selection_generated"] = "Sélection générée";
$install_lang["success_selection_updated"] = "Sélection mise à jour";
$install_lang["success_setting_delete"] = "Paramètre supprimé";


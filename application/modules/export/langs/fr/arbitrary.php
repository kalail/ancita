<?php

$install_lang["seo_tags_index_description"] = "Exporter données.";
$install_lang["seo_tags_index_header"] = "Exporter données";
$install_lang["seo_tags_index_keyword"] = "exporter données";
$install_lang["seo_tags_index_og_description"] = "Exporter données.";
$install_lang["seo_tags_index_og_title"] = "Exporter données";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "PG Real Estate : Exporter données";
$install_lang["seo_tags_advanced_description"] = "Exporter donées par formule";
$install_lang["seo_tags_advanced_header"] = "Exporter donées par formule";
$install_lang["seo_tags_advanced_keyword"] = "exporter données";
$install_lang["seo_tags_advanced_og_description"] = "Exporter donées par formule";
$install_lang["seo_tags_advanced_og_title"] = "Exporter donées par formule";
$install_lang["seo_tags_advanced_og_type"] = "article";
$install_lang["seo_tags_advanced_title"] = "PG Real Estate : Exporter donées par formule";


<?php

$install_lang["seo_tags_index_description"] = "Экпортировать данные.";
$install_lang["seo_tags_index_header"] = "Экспорт данных";
$install_lang["seo_tags_index_keyword"] = "export data";
$install_lang["seo_tags_index_og_description"] = "Экпортировать данные.";
$install_lang["seo_tags_index_og_title"] = "Экспорт данных";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "PG Real Estate : Экспорт данных";
$install_lang["seo_tags_advanced_description"] = "портировать данные.";
$install_lang["seo_tags_advanced_header"] = "Экспорт данных (расширенный)";
$install_lang["seo_tags_advanced_keyword"] = "export data";
$install_lang["seo_tags_advanced_og_description"] = "Экпортировать данные с учетом критериев";
$install_lang["seo_tags_advanced_og_title"] = "Экспорт данных";
$install_lang["seo_tags_advanced_og_type"] = "article";
$install_lang["seo_tags_advanced_title"] = "PG Real Estate : Экспорт данных (расширенный)";


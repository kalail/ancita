<?php

$install_lang["seo_tags_index_description"] = "Export data.";
$install_lang["seo_tags_index_header"] = "Export data";
$install_lang["seo_tags_index_keyword"] = "export data";
$install_lang["seo_tags_index_og_description"] = "Export data.";
$install_lang["seo_tags_index_og_title"] = "Export data";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "PG Real Estate : Export data";
$install_lang["seo_tags_advanced_description"] = "Export data";
$install_lang["seo_tags_advanced_header"] = "Export data by form";
$install_lang["seo_tags_advanced_keyword"] = "export data";
$install_lang["seo_tags_advanced_og_description"] = "Export data";
$install_lang["seo_tags_advanced_og_title"] = "Export data by form";
$install_lang["seo_tags_advanced_og_type"] = "article";
$install_lang["seo_tags_advanced_title"] = "PG Real Estate : Export data by form";


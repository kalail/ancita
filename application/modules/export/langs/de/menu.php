<?php

$install_lang["admin_export_menu_export_drivers_item"] = "Treiber";
$install_lang["admin_export_menu_export_drivers_item_tooltip"] = "";
$install_lang["admin_export_menu_export_selections_item"] = "Auswahl";
$install_lang["admin_export_menu_export_selections_item_tooltip"] = "";
$install_lang["admin_menu_exp-import-items_export_menu_item"] = "Export";
$install_lang["admin_menu_exp-import-items_export_menu_item_tooltip"] = "CSV, XML, Zillow, Trulia, Oodle, Vast, Syndafeed, Yandex";


<?php

$install_lang["seo_tags_index_description"] = "Export data.";
$install_lang["seo_tags_index_header"] = "Export Daten";
$install_lang["seo_tags_index_keyword"] = "exportieren von Daten";
$install_lang["seo_tags_index_og_description"] = "Export data.";
$install_lang["seo_tags_index_og_title"] = "Export Daten";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "PG Real Estate : Export Daten";
$install_lang["seo_tags_advanced_description"] = "Export Daten per Formular";
$install_lang["seo_tags_advanced_header"] = "Export Daten per Formular";
$install_lang["seo_tags_advanced_keyword"] = "exportieren von Daten";
$install_lang["seo_tags_advanced_og_description"] = "Export Daten per Formular";
$install_lang["seo_tags_advanced_og_title"] = "Export Daten per Formular";
$install_lang["seo_tags_advanced_og_type"] = "article";
$install_lang["seo_tags_advanced_title"] = "PG Real Estate : Export Daten per Formular";


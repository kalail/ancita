<?php

$install_lang["seo_tags_index_description"] = "Exportar datos";
$install_lang["seo_tags_index_header"] = "Exportar datos";
$install_lang["seo_tags_index_keyword"] = "exportar datos";
$install_lang["seo_tags_index_og_description"] = "Exportar datos";
$install_lang["seo_tags_index_og_title"] = "Exportar datos";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "PG Real Estate : Exportar datos";
$install_lang["seo_tags_advanced_description"] = "Exportar datos de forma";
$install_lang["seo_tags_advanced_header"] = "Exportar datos de forma";
$install_lang["seo_tags_advanced_keyword"] = "export data";
$install_lang["seo_tags_advanced_og_description"] = "Exportar datos de forma";
$install_lang["seo_tags_advanced_og_title"] = "Exportar datos de forma";
$install_lang["seo_tags_advanced_og_type"] = "article";
$install_lang["seo_tags_advanced_title"] = "PG Real Estate : Exportar datos de forma";


<?php

$install_lang["seo_tags_index_description"] = "Експортиране на данни.";
$install_lang["seo_tags_index_header"] = "Експорт на данни";
$install_lang["seo_tags_index_keyword"] = "експорт данни";
$install_lang["seo_tags_index_og_description"] = "Експортиране на данни.";
$install_lang["seo_tags_index_og_title"] = "Експорт на данни";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "PG Real Estate : Експорт данни";
$install_lang["seo_tags_advanced_description"] = "Експорт данни";
$install_lang["seo_tags_advanced_header"] = "Експорт данни /разширени/";
$install_lang["seo_tags_advanced_keyword"] = "export data";
$install_lang["seo_tags_advanced_og_description"] = "Експорт на данни по критерии";
$install_lang["seo_tags_advanced_og_title"] = "Експорт данни";
$install_lang["seo_tags_advanced_og_type"] = "article";
$install_lang["seo_tags_advanced_title"] = "PG Real Estate : Експорт данни /разширени/";


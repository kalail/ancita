<?php 

if(!defined("BASEPATH")) exit("No direct script access allowed");

/**
 * Zillow driver export model
 * 
 * @package PG_RealEstate
 * @subpackage Export
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Export_zif_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 * 
	 * @var array
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 * 
	 * @var array
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Lease terms options
	 * 
	 * @var array
	 */
	private $lease_term = array('ContactForDetails', 'Monthly', 'SixMonths', 'OneYear', 'RentToOwn');
	
	/**
	 * Property  options
	 * 
	 * @var array
	 */
	private $property_types = array('SingleFamily', 'Condo', 'Townhouse', 'Coop', 'MultiFamily', 
									'Manufactured', 'VacantLand', 'Other', 'Apartment');
	
	/**
	 * Architecture styles options
	 * 
	 * @var array
	 */
	private $architecture_styles = array('Bungalow', 'CapeCod', 'Colonial', 'Contemporary', 'Craftsman',
										 'French', 'Georgian', 'Loft', 'Modern', 'Queen AnneVictorian',
										 'RanchRambler', 'SantaFePuebloStyle', 'Spanish', 'Split-level', 
										 'Tudor', 'Other');
	
	/**
	 * Constructor
	 * @return Export_zif_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Return default filename
	 * 
	 * @return string
	 */
	public function get_filename(){
		return "export.xml";
	}
	
	/**
	 * Generate output file
	 * 
	 * @param string $filename output filename
	 * @param array $data export data
	 * @param array $settings driver settings
	 * @return void
	 */
	public function generate($filename, $data, $settings){
		$this->CI->config->load("reg_exps", true);
		$email_expr = $this->CI->config->item("email", "reg_exps");
	
		$f = fopen($filename, "wb");
		fputs($f, '<?xml version="1.0" encoding="utf-8">'."\n");
		fputs($f, '<Listings>'."\n");
		foreach($data as $i=>$row){
			if(!$i) continue;
			fputs($f, '	<Listing>'."\n");
			fputs($f, '		<Location>'."\n");
			fputs($f, '			<StreetAddress>'.$row['Location.StreetAddress'].'</StreetAddress>'."\n");
			
			if(isset($row['Location.UnitNumber']) && !empty($row['Location.UnitNumber']))
				fputs($f, '			<UnitNumber>'.$row['Location.UnitNumber'].'</UnitNumber>'."\n");
						
			fputs($f, '			<City>'.$row['Location.City'].'</City>'."\n");
			fputs($f, '			<State>'.$row['Location.State'].'</State>'."\n");
			fputs($f, '			<Zip>'.$row['Location.Zip'].'</Zip>'."\n");
			
			if(isset($row['Location.ParcelId']) && !empty($row['Location.ParcelId']))
				fputs($f, '			<ParcelId>'.$row['Location.ParcelId'].'</ParcelId>'."\n");
				
			if(isset($row['Location.ZPID']) && !empty($row['Location.ZPID']))
				fputs($f, '			<ZPID>'.$row['Location.ZPID'].'</ZPID>'."\n");
			
			if(isset($row['Location.Lat']) && !empty($row['Location.Lat']))
				fputs($f, '			<Lat>'.$row['Location.Lat'].'</Lat>'."\n");
			
			if(isset($row['Location.Long']) && !empty($row['Location.Long']))
				fputs($f, '			<Long>'.$row['Location.Long'].'</Long>'."\n");
			
			if(isset($row['Location.County']) && !empty($row['Location.County']))
				fputs($f, '			<County>'.$row['Location.County'].'</County>'."\n");
			
			if(isset($row['Location.StreetIntersection']) && !empty($row['Location.StreetIntersection']))
				fputs($f, '			<StreetIntersection>'.$row['Location.StreetIntersection'].'</StreetIntersection>'."\n");
			
			if(isset($row['Location.DisplayAddress']))
				fputs($f, '			<DisplayAddress>'.$row['Location.DisplayAddress'].'</DisplayAddress>'."\n");
			
			fputs($f, '		</Location>'."\n");
			fputs($f, '		<ListingDetails>'."\n");
			
			$row['ListingDetails.Status'] = $row['ListingDetails.Status'] ? 'Active' : 'Pending';
			if($row['ListingSold']) $row['Status'] = 'Sold';
			if($row['ListingType'] == 'rent' || $row['ListingType'] == 'lease') $row['ListingDetails.Status'] = 'For Rent';
			fputs($f, '			<Status>'.$row['ListingDetails.Status'].'</Status>'."\n");
			fputs($f, '			<Price>'.$row['ListingDetails.Price'].'</Price>'."\n");
			
			if(isset($row['ListingDetails.ListingUrl']) && !empty($row['ListingDetails.ListingUrl']))
				fputs($f, '			<ListingUrl>'.$row['ListingDetails.ListingUrl'].'</ListingUrl>'."\n");
		
			if(isset($row['ListingDetails.MlsId']) && !empty($row['ListingDetails.MlsId']))
				fputs($f, '			<MlsId>'.$row['ListingDetails.MlsId'].'</MlsId>'."\n");
			
			if(isset($row['ListingDetails.MlsName']) && !empty($row['ListingDetails.MlsName']))
				fputs($f, '			<MlsName>'.$row['ListingDetails.MlsName'].'</MlsName>'."\n");
			
			if(isset($row['ListingDetails.DateListed']))
				fputs($f, '			<DateListed>'.$row['ListingDetails.DateListed'].'</DateListed>'."\n");
			
			if(isset($row['ListingDetails.DateSold']))
				fputs($f, '			<DateSold>'.$row['ListingDetails.DateSold'].'</DateSold>'."\n");
			
			if(isset($row['ListingDetails.VirtualTourUrl']) && !empty($row['ListingDetails.VirtualTourUrl']))
				fputs($f, '			<VirtualTourUrl>'.$row['ListingDetails.VirtualTourUrl'].'</VirtualTourUrl>'."\n");
			
			if(isset($row['ListingDetails.ListingEmail']) && !empty($row['ListingDetails.ListingEmail']) && preg_match($email_expr, $row["ListingDetails.ListingEmail"]))
				fputs($f, '			<ListingEmail>'.$row['ListingDetails.ListingEmail'].'</ListingEmail>'."\n");
			
			if(isset($row['ListingDetails.AlwaysEmailAgent']))
				fputs($f, '			<AlwaysEmailAgent>'.$row['ListingDetails.AlwaysEmailAgent'].'</AlwaysEmailAgent>'."\n");			
				
			if(isset($row['ListingDetails.ShortSale']))
				fputs($f, '			<ShortSale>'.$row['ListingDetails.ShortSale'].'</ShortSale>'."\n");			
			
			if(isset($row['ListingDetails.REO']))
				fputs($f, '			<REO>'.$row['ListingDetails.REO'].'</REO>'."\n");			
			
			fputs($f, '		</ListingDetails>'."\n");
			fputs($f, '		<RentalDetails>'."\n");
			
			if(isset($row['RentalDetails.Availability']) && !empty($row['RentalDetails.Availability']))
				fputs($f, '			<Availability>'.$row['RentalDetails.Availability'].'</Availability>'."\n");
			
			if(isset($row['RentalDetails.LeaseTerm']) && !empty($row['RentalDetails.LeaseTerm']))
				fputs($f, '			<LeaseTerm>'.$row['RentalDetails.LeaseTerm'].'</LeaseTerm>'."\n");
			
			if(isset($row['RentalDetails.DepositFees']) && !empty($row['RentalDetails.DepositFees']))
				fputs($f, '			<DepositFees>'.$row['RentalDetails.DepositFees'].'</DepositFees>'."\n");
			
			if(isset($row['RentalDetails.UtilitiesIncluded']) && !empty($row['RentalDetails.UtilitiesIncluded'])){
				fputs($f, '			<UtilitiesIncluded>'."\n");
				
				if(isset($row['RentalDetails.UtilitiesIncluded.Water']))
					fputs($f, '				<Water>'.$row['RentalDetails.UtilitiesIncluded.Water'].'</Water>'."\n");
				
				if(isset($row['RentalDetails.UtilitiesIncluded.Sewage']))
					fputs($f, '				<Sewage>'.$row['RentalDetails.UtilitiesIncluded.Sewage'].'</Sewage>'."\n");
					
				if(isset($row['RentalDetails.UtilitiesIncluded.Garbage']))
					fputs($f, '				<Garbage>'.$row['RentalDetails.UtilitiesIncluded.Garbage'].'</Garbage>'."\n");	
					
				if(isset($row['RentalDetails.UtilitiesIncluded.Electricity']))
					fputs($f, '				<Electricity>'.$row['RentalDetails.UtilitiesIncluded.Electricity'].'</Electricity>'."\n");	
					
				if(isset($row['RentalDetails.UtilitiesIncluded.Gas']))
					fputs($f, '				<Gas>'.$row['RentalDetails.UtilitiesIncluded.Gas'].'</Gas>'."\n");
					
				if(isset($row['RentalDetails.UtilitiesIncluded.Internet']))
					fputs($f, '				<Internet>'.$row['RentalDetails.UtilitiesIncluded.Internet'].'</Internet>'."\n");
					
				if(isset($row['RentalDetails.UtilitiesIncluded.Cable']))
					fputs($f, '				<Cable>'.$row['RentalDetails.UtilitiesIncluded.Cable'].'</Cable>'."\n");
					
				if(isset($row['RentalDetails.UtilitiesIncluded.SatTV']))
					fputs($f, '				<SatTV>'.$row['RentalDetails.UtilitiesIncluded.SatTV'].'</SatTV>'."\n");
					
				fputs($f, '			</UtilitiesIncluded>'."\n");
			}
			if(isset($row['RentalDetails.PetsAllowed']) && !empty($row['RentalDetails.PetsAllowed.LargeDogs'])){
				fputs($f, '			<PetsAllowed>'."\n");
				
				if(isset($row['RentalDetails.PetsAllowed.NoPets']))
					fputs($f, '				<NoPets>'.$row['RentalDetails.PetsAllowed.NoPets'].'</NoPets>'."\n");
				
				if(isset($row['RentalDetails.PetsAllowed.Cats']))
					fputs($f, '				<Cats>'.$row['RentalDetails.PetsAllowed.Cats'].'</Cats>'."\n");
				
				if(isset($row['RentalDetails.PetsAllowed.SmallDogs']))
					fputs($f, '				<SmallDogs>'.$row['RentalDetails.PetsAllowed.SmallDogs'].'</SmallDogs>'."\n");
					
				if(isset($row['RentalDetails.PetsAllowed.LargeDogs']))
					fputs($f, '				<LargeDogs>'.$row['RentalDetails.PetsAllowed.LargeDogs'].'</LargeDogs>'."\n");	
				
				fputs($f, '			</PetsAllowed>'."\n");
			}
						
			fputs($f, '		</RentalDetails>'."\n");
			fputs($f, '		<BasicDetails>'."\n");
			fputs($f, '			<PropertyType>'.$row['BasicDetails.PropertyType'].'</PropertyType>'."\n");
			
			if(isset($row['BasicDetails.Title']) && !empty($row['BasicDetails.Title']))
				fputs($f, '				<Title>'.$row['BasicDetails.Title'].'</Title>'."\n");
			
			if(isset($row['BasicDetails.Description']) && !empty($row['BasicDetails.Description']))
				fputs($f, '				<Description>'.$row['BasicDetails.Description'].'</Description>'."\n");

			if(isset($row['BasicDetails.Bedrooms']) && !empty($row['BasicDetails.Bedrooms']))
				fputs($f, '				<Bedrooms>'.$row['BasicDetails.Bedrooms'].'</Bedrooms>'."\n");
			
			if(isset($row['BasicDetails.Bathrooms']) && !empty($row['BasicDetails.Bathrooms']))
				fputs($f, '				<Bathrooms>'.$row['BasicDetails.Bathrooms'].'</Bathrooms>'."\n");
			
			if(isset($row['BasicDetails.FullBathrooms']) && !empty($row['BasicDetails.FullBathrooms']))
				fputs($f, '				<FullBathrooms>'.$row['BasicDetails.FullBathrooms'].'</FullBathrooms>'."\n");
			
			if(isset($row['BasicDetails.HalfBathrooms']) && !empty($row['BasicDetails.HalfBathrooms']))
				fputs($f, '				<HalfBathrooms>'.$row['BasicDetails.HalfBathrooms'].'</HalfBathrooms>'."\n");
			
			if(isset($row['BasicDetails.LivingArea']) && !empty($row['BasicDetails.LivingArea']))
				fputs($f, '				<LivingArea>'.$row['BasicDetails.LivingArea'].'</LivingArea>'."\n");
			
			if(isset($row['BasicDetails.LotSize']) && !empty($row['BasicDetails.LotSize']))
				fputs($f, '				<LotSize>'.$row['BasicDetails.LotSize'].'</LotSize>'."\n");

			if(isset($row['BasicDetails.YearBuilt']) && !empty($row['BasicDetails.YearBuilt']) && strlen($row['BasicDetails.YearBuilt']) == 4)
				fputs($f, '				<YearBuilt>'.$row['BasicDetails.YearBuilt'].'</YearBuilt>'."\n");
			
			fputs($f, '		</BasicDetails>'."\n");
			
			if(isset($row['Pictures']) && !empty($row['Pictures'])){
				fputs($f, '		<Pictures>'."\n");
				foreach($row['Pictures'] as $picture){
					fputs($f, '			<Picture>'."\n");
					fputs($f, '				<PictureUrl>'.$picture['PictureUrl'].'</PictureUrl>'."\n");
					fputs($f, '				<Caption>'.$picture['Caption'].'</Caption>'."\n");
					fputs($f, '			</Picture>'."\n");
				}			
				fputs($f, '		</Pictures>'."\n");
			}
			
			fputs($f, '		<Agent>'."\n");
				
			if(isset($row['Agent.FirstName']) && !empty($row['Agent.FirstName']))
				fputs($f, '			<FirstName>'.$row['Agent.FirstName'].'</FirstName>'."\n");
				
			if(isset($row['Agent.LastName']) && !empty($row['Agent.LastName']))
				fputs($f, '			<LastName>'.$row['Agent.LastName'].'</LastName>'."\n");
				
			fputs($f, '			<EmailAddress>'.$row['Agent.EmailAddress'].'</EmailAddress>'."\n");
				
			if(isset($row['Agent.PictureUrl']) && !empty($row['Agent.PictureUrl']))
				fputs($f, '			<PictureUrl>'.$row['Agent.PictureUrl'].'</PictureUrl>'."\n");
					
			if(isset($row['Agent.OfficeLineNumber']) && !empty($row['Agent.OfficeLineNumber']))
				fputs($f, '			<OfficeLineNumber>'.$row['Agent.OfficeLineNumber'].'</OfficeLineNumber>'."\n");
				
			if(isset($row['Agent.MobilePhoneLineNumber']) && !empty($row['Agent.MobilePhoneLineNumber']))
				fputs($f, '			<MobilePhoneLineNumber>'.$row['Agent.MobilePhoneLineNumber'].'</MobilePhoneLineNumber>'."\n");
				
			if(isset($row['Agent.FaxLineNumber']) && !empty($row['Agent.FaxLineNumber']))
				fputs($f, '			<FaxLineNumber>'.$row['Agent.FaxLineNumber'].'</FaxLineNumber>'."\n");
					
			fputs($f, '		</Agent>'."\n");
			
			fputs($f, '		<Office>'."\n");
				
			if(isset($row['Office.BrokerageName']) && !empty($row['Office.BrokerageName']))
				fputs($f, '			<BrokerageName>'.$row['Office.BrokerageName'].'</BrokerageName>'."\n");
				
			fputs($f, '			<BrokerPhone>'.$row['Office.BrokerPhone'].'</BrokerPhone>'."\n");

			if(isset($row['Office.BrokerEmail']) && !empty($row['Office.BrokerEmail']))
				fputs($f, '			<BrokerEmail>'.$row['Office.BrokerEmail'].'</BrokerEmail>'."\n");
				
			if(isset($row['Office.BrokerWebsite']) && !empty($row['Office.BrokerWebsite']))
				fputs($f, '			<BrokerWebsite>'.$row['Office.BrokerWebsite'].'</BrokerWebsite>'."\n");
			
			if(isset($row['Office.StreetAddress']) && !empty($row['Office.StreetAddress']))
				fputs($f, '			<StreetAddress>'.$row['Office.StreetAddress'].'</StreetAddress>'."\n");
				
			if(isset($row['Office.UnitNumber']) && !empty($row['Office.UnitNumber']))	
				fputs($f, '			<UnitNumber>'.$row['Office.UnitNumber'].'</UnitNumber>'."\n");
					
			if(isset($row['Office.City']) && !empty($row['Office.City']))		
				fputs($f, '			<City>'.$row['Office.City'].'</City>'."\n");
				
			if(isset($row['Office.State']) && !empty($row['Office.State']))		
				fputs($f, '			<State>'.$row['Office.State'].'</State>'."\n");
				
			if(isset($row['Office.Zip']) && !empty($row['Office.Zip']))
				fputs($f, '			<Zip>'.$row['Office.Zip'].'</Zip>'."\n");
					
			if(isset($row['Office.Name']) && !empty($row['Office.Name']))	
				fputs($f, '			<OfficeName>'.$row['Office.Name'].'</OfficeName>'."\n");
					
			if(isset($row['Office.FranchiseName']) && !empty($row['Office.FranchiseName']))		
				fputs($f, '			<FranchiseName>'.$row['Office.FranchiseName'].'</FranchiseName>'."\n");
				
			fputs($f, '		</Office>'."\n");
			
			if(isset($row['OpenHouses.OpenHouse.Date']) && !empty($row['OpenHouses.OpenHouse.Date'])){
				fputs($f, '		<OpenHouses>'."\n");
				fputs($f, '			<OpenHouse>'."\n");
				fputs($f, '				<Date>'.$row['OpenHouses.OpenHouse.Date'].'</Date>'."\n");
				
				if(isset($row['OpenHouses.OpenHouse.StartTime']) && !empty($row['OpenHouses.OpenHouse.StartTime']))
					fputs($f, '				<StartTime>'.$row['OpenHouses.OpenHouse.StartTime'].'</StartTime>'."\n");
				
				if(isset($row['OpenHouses.OpenHouse.EndTime']) && !empty($row['OpenHouses.OpenHouse.EndTime']))
					fputs($f, '				<EndTime>'.$row['OpenHouses.OpenHouse.EndTime'].'</EndTime>'."\n");
					
				fputs($f, '			</OpenHouse>'."\n");
				fputs($f, '		</OpenHouses>'."\n");
			}
			
			if(isset($row['Schools']) && !empty($row['Schools'])){
				fputs($f, '		<Schools>'."\n");
				
				if(isset($row['Schools.District']) && !empty($row['Schools.District']))
					fputs($f, '			<District>'.$row['Schools.District'].'</District>'."\n");
				
				if(isset($row['Schools.Elementary']) && !empty($row['Schools.Elementary']))
					fputs($f, '			<Elementary>'.$row['Schools.Elementary'].'</Elementary>'."\n");
					
				if(isset($row['Schools.Middle']) && !empty($row['Schools.Middle']))
					fputs($f, '			<Middle>'.$row['Schools.Middle'].'</Middle>'."\n");
					
				if(isset($row['Schools.High']) && !empty($row['Schools.High']))	
					fputs($f, '			<High>'.$row['Schools.High'].'</High>'."\n");
					
				fputs($f, '		</Schools>'."\n");
			}
			
			if(isset($row['Neighborhood']) && !empty($row['Neighborhood'])){
				fputs($f, '		<Neighborhood>'."\n");
				
				if(isset($row['Neighborhood.Name']) && !empty($row['Neighborhood.Name']))
					fputs($f, '			<Name>'.$row['Neighborhood.Name'].'</Name>'."\n");
				
				if(isset($row['Neighborhood.Description']) && !empty($row['Neighborhood.Description']))
					fputs($f, '			<Description>'.$row['Neighborhood.Description'].'</Description>'."\n");
				
				fputs($f, '		</Neighborhood>'."\n");
			}
			
			if(isset($row['RichDetails']) && !empty($row['RichDetails'])){
				fputs($f, '		<RichDetails>'."\n");
				
				if(isset($row['RichDetails.AdditionalFeatures']) && !empty($row['RichDetails.AdditionalFeatures']))
					fputs($f, '			<AdditionalFeatures>'.$row['RichDetails.AdditionalFeatures'].'<AdditionalFeatures>'."\n");
				
				if(isset($row['RichDetails.Appliances']) && !empty($appliances)){
					fputs($f, '			<Appliances>'."\n");
					
					if(isset($row['RichDetails.Appliances.Dishwasher']) && !empty($row['RichDetails.Appliances.Dishwasher']))
						fputs($f, '				<Appliance>Dishwasher</Appliance>'."\n");			
					
					if(isset($row['RichDetails.Appliances.Dryer']) && !empty($row['RichDetails.Appliances.Dryer']))
						fputs($f, '				<Appliance>Dryer</Appliance>'."\n");			
						
					if(isset($row['RichDetails.Appliances.Freezer']) && !empty($row['RichDetails.Appliances.Freezer']))
						fputs($f, '				<Appliance>Freezer</Appliance>'."\n");
						
					if(isset($row['RichDetails.Appliances.GarbageDisposal']) && !empty($row['RichDetails.Appliances.GarbageDisposal']))
						fputs($f, '				<Appliance>GarbageDisposal</Appliance>'."\n");
					
					if(isset($row['RichDetails.Appliances.Microwave']) && !empty($row['RichDetails.Appliances.Microwave']))
						fputs($f, '				<Appliance>Microwave</Appliance>'."\n");
					
					if(isset($row['RichDetails.Appliances.RangeOven']) && !empty($row['RichDetails.Appliances.RangeOven']))
						fputs($f, '				<Appliance>RangeOven</Appliance>'."\n");
						
					if(isset($row['RichDetails.Appliances.Refrigerator']) && !empty($row['RichDetails.Appliances.Refrigerator']))
						fputs($f, '				<Appliance>Refrigerator</Appliance>'."\n");	
				
					if(isset($row['RichDetails.Appliances.TrashCompactor']) && !empty($row['RichDetails.Appliances.TrashCompactor']))
						fputs($f, '				<Appliance>TrashCompactor</Appliance>'."\n");	
						
					if(isset($row['RichDetails.Appliances.Washer']) && !empty($row['RichDetails.Appliances.Washer']))
						fputs($f, '				<Appliance>Washer</Appliance>'."\n");		
					
					fputs($f, '			</Appliances>'."\n");
				}
				
				if(isset($row['RichDetails.ArchitectureStyle']) && !empty($row['RichDetails.ArchitectureStyle']))
					fputs($f, '			<ArchitectureStyle>'.$row['RichDetails.ArchitectureStyle'].'</ArchitectureStyle>'."\n");
				
				if(isset($row['RichDetails.Attic']))
					fputs($f, '			<Attic>'.$row['RichDetails.Attic'].'</Attic>'."\n");
				
				if(isset($row['RichDetails.BarbecueArea']))
					fputs($f, '			<BarbecueArea>'.$row['RichDetails.BarbecueArea'].'</BarbecueArea>'."\n");
				
				if(isset($row['RichDetails.Basement']))
					fputs($f, '			<Basement>'.$row['RichDetails.Basement'].'</Basement>'."\n");
					
				if(isset($row['RichDetails.BuildingUnitCount']) && !empty($row['RichDetails.BuildingUnitCount']))
					fputs($f, '			<BuildingUnitCount>'.$row['RichDetails.BuildingUnitCount'].'</BuildingUnitCount>'."\n");
					
				if(isset($row['RichDetails.CableReady']))
					fputs($f, '			<CableReady>'.$row['RichDetails.CableReady'].'</CableReady>'."\n");
			
				if(isset($row['RichDetails.CeilingFan']))
					fputs($f, '			<CeilingFan>'.$row['RichDetails.CeilingFan'].'</CeilingFan>'."\n");
					
				if(isset($row['RichDetails.CondoFloorNum']) && !empty($row['RichDetails.CondoFloorNum']))
					fputs($f, '			<CondoFloorNum>'.$row['RichDetails.CondoFloorNum'].'</CondoFloorNum>'."\n");
				
				if(isset($row['RichDetails.CoolingSystems']) && !empty($cooling_systems)){
					fputs($f, '			<CoolingSystems>'."\n");
					
					if(isset($row['RichDetails.CoolingSystems.None']) && !empty($row['RichDetails.CoolingSystems.None']))
						fputs($f, '				<CoolingSystem>None</CoolingSystem>'."\n");
						
					if(isset($row['RichDetails.CoolingSystems.Central']) && !empty($row['RichDetails.CoolingSystems.Central']))
						fputs($f, '				<CoolingSystem>Central</CoolingSystem>'."\n");	
					
					if(isset($row['RichDetails.CoolingSystems.Evaporative']) && !empty($row['RichDetails.CoolingSystems.Evaporative']))
						fputs($f, '				<CoolingSystem>Evaporative</CoolingSystem>'."\n");		
					
					if(isset($row['RichDetails.CoolingSystems.Geothermal']) && !empty($row['RichDetails.CoolingSystems.Geothermal']))
						fputs($f, '				<CoolingSystem>Geothermal</CoolingSystem>'."\n");		
						
					if(isset($row['RichDetails.CoolingSystems.Wall']) && !empty($row['RichDetails.CoolingSystems.Wall']))
						fputs($f, '				<CoolingSystem>Wall</CoolingSystem>'."\n");		
						
					if(isset($row['RichDetails.CoolingSystems.Solar']) && !empty($row['RichDetails.CoolingSystems.Solar']))
						fputs($f, '				<CoolingSystem>Solar</CoolingSystem>'."\n");		
						
					if(isset($row['RichDetails.CoolingSystems.Other']) && !empty($row['RichDetails.CoolingSystems.Other']))
						fputs($f, '				<CoolingSystem>Other</CoolingSystem>'."\n");		
					
					fputs($f, '			</CoolingSystems>'."\n");
				}
				
				if(isset($row['RichDetails.Deck']))
					fputs($f, '			<Deck>'.$row['RichDetails.Deck'].'</Deck>'."\n");
					
				if(isset($row['RichDetails.DisabledAccess']))
					fputs($f, '			<DisabledAccess>'.$row['RichDetails.DisabledAccess'].'</DisabledAccess>'."\n");
					
				if(isset($row['RichDetails.Dock']))
					fputs($f, '			<Dock>'.$row['RichDetails.Dock'].'</Dock>'."\n");
				
				if(isset($row['RichDetails.Doorman']))
					fputs($f, '			<Doorman>'.$row['RichDetails.Doorman'].'</Doorman>'."\n");
				
				if(isset($row['RichDetails.DoublePaneWindows']))
					fputs($f, '			<DoublePaneWindows>'.$row['RichDetails.DoublePaneWindows'].'</DoublePaneWindows>'."\n");
				
				if(isset($row['RichDetails.Elevator']))
					fputs($f, '			<Elevator>'.$row['RichDetails.Elevator'].'</Elevator>'."\n");
					
				if(isset($row['RichDetails.ExteriorTypes']) && !empty($row['RichDetails.ExteriorTypes'])){
					fputs($f, '			<ExteriorTypes>'."\n");
					
					if(isset($row['RichDetails.ExteriorTypes.Brick']) && !empty($row['RichDetails.ExteriorTypes.Brick']))
						fputs($f, '				<ExteriorType>Brick</ExteriorType>'."\n");
					
					if(isset($row['RichDetails.ExteriorTypes.CementConcrete']) && !empty($row['RichDetails.ExteriorTypes.CementConcrete']))
						fputs($f, '				<ExteriorType>CementConcrete</ExteriorType>'."\n");
					
					if(isset($row['RichDetails.ExteriorTypes.Composition']) && !empty($row['RichDetails.ExteriorTypes.Composition']))
						fputs($f, '				<ExteriorType>Composition</ExteriorType>'."\n");
					
					if(isset($row['RichDetails.ExteriorTypes.Metal']) && !empty($row['RichDetails.ExteriorTypes.Metal']))
						fputs($f, '				<ExteriorType>Metal</ExteriorType>'."\n");
					
					if(isset($row['RichDetails.ExteriorTypes.Shingle']) && !empty($row['RichDetails.ExteriorTypes.Shingle']))
						fputs($f, '				<ExteriorType>Shingle</ExteriorType>'."\n");
					
					if(isset($row['RichDetails.ExteriorTypes.Stone']) && !empty($row['RichDetails.ExteriorTypes.Stone']))
						fputs($f, '				<ExteriorType>Stone</ExteriorType>'."\n");
					
					if(isset($row['RichDetails.ExteriorTypes.Stucco']) && !empty($row['RichDetails.ExteriorTypes.Stucco']))
						fputs($f, '				<ExteriorType>Stucco</ExteriorType>'."\n");
					
					if(isset($row['RichDetails.ExteriorTypes.Vinyl']) && !empty($row['RichDetails.ExteriorTypes.Vinyl']))
						fputs($f, '				<ExteriorType>Vinyl</ExteriorType>'."\n");
						
					if(isset($row['RichDetails.ExteriorTypes.Wood']) && !empty($row['RichDetails.ExteriorTypes.Wood']))
						fputs($f, '				<ExteriorType>Wood</ExteriorType>'."\n");
					
					if(isset($row['RichDetails.ExteriorTypes.WoodProducts']) && !empty($row['RichDetails.ExteriorTypes.WoodProducts']))
						fputs($f, '				<ExteriorType>WoodProducts</ExteriorType>'."\n");
					
					if(isset($row['RichDetails.ExteriorTypes.Other']) && !empty($row['RichDetails.ExteriorTypes.Other']))
						fputs($f, '				<ExteriorType>Other</ExteriorType>'."\n");
					
					fputs($f, '			</ExteriorTypes>'."\n");
				}
				
				if(isset($row['RichDetails.Fireplace']))
					fputs($f, '			<Fireplace>'.$row['RichDetails.Fireplace'].'</Fireplace>'."\n");
					
				if(isset($row['RichDetails.FloorCoverings']) && !empty($row['RichDetails.FloorCoverings'])){	
					fputs($f, '			<FloorCoverings>'."\n");
					
					if(isset($row['RichDetails.FloorCoverings.Carpet']) && !empty($row['RichDetails.FloorCoverings.Carpet']))
						fputs($f, '				<FloorCovering>Carpet</FloorCovering>'."\n");
						
					if(isset($row['RichDetails.FloorCoverings.Concrete']) && !empty($row['RichDetails.FloorCoverings.Concrete']))
						fputs($f, '				<FloorCovering>Concrete</FloorCovering>'."\n");
			
					if(isset($row['RichDetails.FloorCoverings.Hardwood']) && !empty($row['RichDetails.FloorCoverings.Hardwood']))
						fputs($f, '				<FloorCovering>Hardwood</FloorCovering>'."\n");
			
					if(isset($row['RichDetails.FloorCoverings.Laminate']) && !empty($row['RichDetails.FloorCoverings.Laminate']))
						fputs($f, '				<FloorCovering>Laminate</FloorCovering>'."\n");
			
					if(isset($row['RichDetails.FloorCoverings.LinoleumVinyl']) && !empty($row['RichDetails.FloorCoverings.LinoleumVinyl']))
						fputs($f, '				<FloorCovering>LinoleumVinyl</FloorCovering>'."\n");
						
					if(isset($row['RichDetails.FloorCoverings.Slate']) && !empty($row['RichDetails.FloorCoverings.Slate']))
						fputs($f, '				<FloorCovering>Slate</FloorCovering>'."\n");
						
					if(isset($row['RichDetails.FloorCoverings.Softwood']) && !empty($row['RichDetails.FloorCoverings.Softwood']))
						fputs($f, '				<FloorCovering>Softwood</FloorCovering>'."\n");
			
					if(isset($row['RichDetails.FloorCoverings.Tile']) && !empty($row['RichDetails.FloorCoverings.Tile']))
						fputs($f, '				<FloorCovering>Tile</FloorCovering>'."\n");
			
					if(isset($row['RichDetails.FloorCoverings.Other']) && !empty($row['RichDetails.FloorCoverings.Other']))
						fputs($f, '				<FloorCovering>Other</FloorCovering>'."\n");
			
					fputs($f, '			</FloorCoverings>'."\n");
				}
				
				if(isset($row['RichDetails.Garden']))
					fputs($f, '			<Garden>'.$row['RichDetails.Garden'].'</Garden>'."\n");
			
				if(isset($row['RichDetails.GatedEntry']))
					fputs($f, '			<GatedEntry>'.$row['RichDetails.GatedEntry'].'</GatedEntry>'."\n");
				
				if(isset($row['RichDetails.Greenhouse']))
					fputs($f, '			<Greenhouse>'.$row['RichDetails.Greenhouse'].'</Greenhouse>'."\n");
					
				if(isset($row['RichDetails.HeatingFuels']) && !empty($row['RichDetails.HeatingFuels'])){	
					fputs($f, '			<HeatingFuels>'."\n");
			
					if(isset($row['RichDetails.HeatingFuels.None']) && !empty($row['RichDetails.HeatingFuels.None']))
						fputs($f, '				<HeatingFuel>None</HeatingFuel>'."\n");
					
					if(isset($row['RichDetails.HeatingFuels.Coal']) && !empty($row['RichDetails.HeatingFuels.Coal']))
						fputs($f, '				<HeatingFuel>Coal</HeatingFuel>'."\n");
						
					if(isset($row['RichDetails.HeatingFuels.Electric']) && !empty($row['RichDetails.HeatingFuels.Electric']))
						fputs($f, '				<HeatingFuel>Electric</HeatingFuel>'."\n");
						
					if(isset($row['RichDetails.HeatingFuels.Gas']) && !empty($row['RichDetails.HeatingFuels.Gas']))
						fputs($f, '				<HeatingFuel>Gas</HeatingFuel>'."\n");
						
					if(isset($row['RichDetails.HeatingFuels.Oil']) && !empty($row['RichDetails.HeatingFuels.Oil']))
						fputs($f, '				<HeatingFuel>Oil</HeatingFuel>'."\n");
					
					if(isset($row['RichDetails.HeatingFuels.PropaneButane']) && !empty($row['RichDetails.HeatingFuels.PropaneButane']))
						fputs($f, '				<HeatingFuel>PropaneButane</HeatingFuel>'."\n");
						
					if(isset($row['RichDetails.HeatingFuels.Solar']) && !empty($row['RichDetails.HeatingFuels.Solar']))
						fputs($f, '				<HeatingFuel>Solar</HeatingFuel>'."\n");
						
					if(isset($row['RichDetails.HeatingFuels.WoodPellet']) && !empty($row['RichDetails.HeatingFuels.WoodPellet']))
						fputs($f, '				<HeatingFuel>WoodPellet</HeatingFuel>'."\n");
						
					if(isset($row['RichDetails.HeatingFuels.Other']) && !empty($row['RichDetails.HeatingFuels.Other']))
						fputs($f, '				<HeatingFuel>Other</HeatingFuel>'."\n");
					
					fputs($f, '			</HeatingFuels>'."\n");
				}
				
				if(isset($row['RichDetails.HeatingSystems']) && !empty($row['RichDetails.HeatingSystems'])){	
					fputs($f, '			<HeatingSystems>'."\n");
					
					if(isset($row['RichDetails.HeatingSystems.Baseboard']) && !empty($row['RichDetails.HeatingSystems.Baseboard']))
						fputs($f, '				<HeatingSystem>Baseboard</HeatingSystem>'."\n");
					
					if(isset($row['RichDetails.HeatingSystems.ForcedAir']) && !empty($row['RichDetails.HeatingSystems.ForcedAir']))
						fputs($f, '				<HeatingSystem>ForcedAir</HeatingSystem>'."\n");
					
					if(isset($row['RichDetails.HeatingSystems.HeatPump']) && !empty($row['RichDetails.HeatingSystems.HeatPump']))
						fputs($f, '				<HeatingSystem>HeatPump</HeatingSystem>'."\n");
						
					if(isset($row['RichDetails.HeatingSystems.Radiant']) && !empty($row['RichDetails.HeatingSystems.Radiant']))
						fputs($f, '				<HeatingSystem>Radiant</HeatingSystem>'."\n");
						
					if(isset($row['RichDetails.HeatingSystems.Stove']) && !empty($row['RichDetails.HeatingSystems.Stove']))
						fputs($f, '				<HeatingSystem>Stove</HeatingSystem>'."\n");
					
					if(isset($row['RichDetails.HeatingSystems.Wall']) && !empty($row['RichDetails.HeatingSystems.Wall']))
						fputs($f, '				<HeatingSystem>Wall</HeatingSystem>'."\n");
					
					if(isset($row['RichDetails.HeatingSystems.Other']) && !empty($row['RichDetails.HeatingSystems.Other']))
						fputs($f, '				<HeatingSystem>Other</HeatingSystem>'."\n");
					
					fputs($f, '			</HeatingSystems>'."\n");
				}
				
				if(isset($row['RichDetails.HottubSpa']))
					fputs($f, '			<HottubSpa>'.$row['RichDetails.HottubSpa'].'</HottubSpa>'."\n");
				
				if(isset($row['RichDetails.Intercom']))
					fputs($f, '			<Intercom>'.$row['RichDetails.Intercom'].'</Intercom>'."\n");
			
				if(isset($row['RichDetails.JettedBathTub']))
					fputs($f, '			<JettedBathTub>'.$row['RichDetails.JettedBathTub'].'</JettedBathTub>'."\n");
				
				if(isset($row['RichDetails.Lawn']))
					fputs($f, '			<Lawn>'.$row['RichDetails.Lawn'].'</Lawn>'."\n");
				
				if(isset($row['RichDetails.LegalDescription']) && !empty($row['RichDetails.LegalDescription']))
					fputs($f, '			<LegalDescription>'.$row['RichDetails.LegalDescription'].'</LegalDescription>'."\n");
					
				if(isset($row['RichDetails.MotherInLaw']))	
					fputs($f, '			<MotherInLaw>'.$row['RichDetails.MotherInLaw'].'</MotherInLaw>'."\n");
				
				if(isset($row['RichDetails.NumFloors']) && !empty($row['RichDetails.NumFloors']))
					fputs($f, '			<NumFloors>'.$row['RichDetails.NumFloors'].'</NumFloors>'."\n");
				
				if(isset($row['RichDetails.NumParkingSpaces']) && !empty($row['RichDetails.NumParkingSpaces']))
					fputs($f, '			<NumParkingSpaces>'.$row['RichDetails.NumParkingSpaces'].'</NumParkingSpaces>'."\n");
					
				if(isset($row['RichDetails.ParkingTypes']) && !empty($row['RichDetails.ParkingTypes'])){
					fputs($f, '			<ParkingTypes>'."\n");
					
					if(isset($row['RichDetails.ParkingTypes.Carport']) && !empty($row['RichDetails.ParkingTypes.Carport']))
						fputs($f, '				<ParkingType>Carport</ParkingType>'."\n");
			
					if(isset($row['RichDetails.ParkingTypes.GarageAttached']) && !empty($row['RichDetails.ParkingTypes.GarageAttached']))
						fputs($f, '				<ParkingType>GarageAttached</ParkingType>'."\n");
						
					if(isset($row['RichDetails.ParkingTypes.GarageDetached']) && !empty($row['RichDetails.ParkingTypes.GarageDetached']))
						fputs($f, '				<ParkingType>GarageDetached</ParkingType>'."\n");
			
					if(isset($row['RichDetails.ParkingTypes.OffStreet']) && !empty($row['RichDetails.ParkingTypes.OffStreet']))
						fputs($f, '				<ParkingType>OffStreet</ParkingType>'."\n");
						
					if(isset($row['RichDetails.ParkingTypes.OnStreet']) && !empty($row['RichDetails.ParkingTypes.OnStreet']))
						fputs($f, '				<ParkingType>OnStreet</ParkingType>'."\n");
						
					if(isset($row['RichDetails.ParkingTypes.None']) && !empty($row['RichDetails.ParkingTypes.None']))
						fputs($f, '				<ParkingType>None</ParkingType>'."\n");
			
					fputs($f, '			</ParkingTypes>'."\n");
				}
				
				if(isset($row['RichDetails.Patio']))	
					fputs($f, '			<Patio>'.$row['RichDetails.Patio'].'</Patio>'."\n");
				
				if(isset($row['RichDetails.Pond']))	
					fputs($f, '			<Pond>'.$row['RichDetails.Pond'].'</Pond>'."\n");
				
				if(isset($row['RichDetails.Pool']))
					fputs($f, '			<Pool>'.$row['RichDetails.Pool'].'</Pool>'."\n");
				
				if(isset($row['RichDetails.Porch']))
					fputs($f, '			<Porch>'.$row['RichDetails.Porch'].'</Porch>'."\n");
				
				if(isset($row['RichDetails.RoofTypes']) && !empty($row['RichDetails.RoofTypes'])){
					fputs($f, '			<RoofTypes>'."\n");
					
					if(isset($row['RichDetails.RoofTypes.Asphalt']) && !empty($row['RichDetails.RoofTypes.Asphalt']))
						fputs($f, '				<RoofType>Asphalt</RoofType>'."\n");
						
					if(isset($row['RichDetails.RoofTypes.BuiltUp']) && !empty($row['RichDetails.RoofTypes.BuiltUp']))
						fputs($f, '				<RoofType>BuiltUp</RoofType>'."\n");
						
					if(isset($row['RichDetails.RoofTypes.Composition']) && !empty($row['RichDetails.RoofTypes.Composition']))
						fputs($f, '				<RoofType>Composition</RoofType>'."\n");
						
					if(isset($row['RichDetails.RoofTypes.Metal']) && !empty($row['RichDetails.RoofTypes.Metal']))
						fputs($f, '				<RoofType>Metal</RoofType>'."\n");
						
					if(isset($row['RichDetails.RoofTypes.ShakeShingle']) && !empty($row['RichDetails.RoofTypes.ShakeShingle']))
						fputs($f, '				<RoofType>ShakeShingle</RoofType>'."\n");
						
					if(isset($row['RichDetails.RoofTypes.Slate']) && !empty($row['RichDetails.RoofTypes.Slate']))
						fputs($f, '				<RoofType>Slate</RoofType>'."\n");
						
					if(isset($row['RichDetails.RoofTypes.Tile']) && !empty($row['RichDetails.RoofTypes.Tile']))
						fputs($f, '				<RoofType>Tile</RoofType>'."\n");
						
					if(isset($row['RichDetails.RoofTypes.Other']) && !empty($row['RichDetails.RoofTypes.Other']))
						fputs($f, '				<RoofType>Other</RoofType>'."\n");
						
					fputs($f, '			</RoofTypes>'."\n");
				}
				
				if(isset($row['RichDetails.RoomCount']) && !empty($row['RichDetails.RoomCount']))
					fputs($f, '			<RoomCount>'.$row['RichDetails.RoomCount'].'</RoomCount>'."\n");
				
				if(isset($row['RichDetails.Rooms']) && !empty($row['RichDetails.Rooms'])){
					fputs($f, '			<Rooms>'."\n");
			
					if(isset($row['RichDetails.Rooms.BreakfastNook']) && !empty($row['RichDetails.Rooms.BreakfastNook']))
						fputs($f, '				<Room>BreakfastNook</Room>'."\n");
			
					if(isset($row['RichDetails.Rooms.DiningRoom']) && !empty($row['RichDetails.Rooms.DiningRoom']))
						fputs($f, '				<Room>DiningRoom</Room>'."\n");
						
					if(isset($row['RichDetails.Rooms.FamilyRoom']) && !empty($row['RichDetails.Rooms.FamilyRoom']))
						fputs($f, '				<Room>FamilyRoom</Room>'."\n");
						
					if(isset($row['RichDetails.Rooms.LaundryRoom']) && !empty($row['RichDetails.Rooms.LaundryRoom']))
						fputs($f, '				<Room>LaundryRoom</Room>'."\n");
						
					if(isset($row['RichDetails.Rooms.Library']) && !empty($row['RichDetails.Rooms.Library']))
						fputs($f, '				<Room>Library</Room>'."\n");
						
					if(isset($row['RichDetails.Rooms.MasterBath']) && !empty($row['RichDetails.Rooms.MasterBath']))
						fputs($f, '				<Room>MasterBath</Room>'."\n");
						
					if(isset($row['RichDetails.Rooms.MudRoom']) && !empty($row['RichDetails.Rooms.MudRoom']))
						fputs($f, '				<Room>MudRoom</Room>'."\n");
			
					if(isset($row['RichDetails.Rooms.Office']) && !empty($row['RichDetails.Rooms.Office']))
						fputs($f, '				<Room>Office</Room>'."\n");
			
					if(isset($row['RichDetails.Rooms.Pantry']) && !empty($row['RichDetails.Rooms.Pantry']))
						fputs($f, '				<Room>Pantry</Room>'."\n");
			
					if(isset($row['RichDetails.Rooms.RecreationRoom']) && !empty($row['RichDetails.Rooms.RecreationRoom']))
						fputs($f, '				<Room>RecreationRoom</Room>'."\n");
						
					if(isset($row['RichDetails.Rooms.Workshop']) && !empty($row['RichDetails.Rooms.Workshop']))
						fputs($f, '				<Room>Workshop</Room>'."\n");
						
					if(isset($row['RichDetails.Rooms.SolariumAtrium']) && !empty($row['RichDetails.Rooms.SolariumAtrium']))
						fputs($f, '				<Room>SolariumAtrium</Room>'."\n");	
						
					if(isset($row['RichDetails.Rooms.SunRoom']) && !empty($row['RichDetails.Rooms.SunRoom']))
						fputs($f, '				<Room>SunRoom</Room>'."\n");		
						
					if(isset($row['RichDetails.Rooms.WalkInCloset']) && !empty($row['RichDetails.Rooms.WalkInCloset']))
						fputs($f, '				<Room>WalkInCloset</Room>'."\n");
			
					fputs($f, '			</Rooms>'."\n");
				}
				
				if(isset($row['RichDetails.RvParking']))
					fputs($f, '			<RvParking>'.$row['RichDetails.RvParking'].'</RvParking>'."\n");
					
				if(isset($row['RichDetails.Sauna']))	
					fputs($f, '			<Sauna>'.$row['RichDetails.Sauna'].'</Sauna>'."\n");
				
				if(isset($row['RichDetails.SecuritySystem']))	
					fputs($f, '			<SecuritySystem>'.$row['RichDetails.SecuritySystem'].'</SecuritySystem>'."\n");
				
				if(isset($row['RichDetails.Skylight']))	
					fputs($f, '			<Skylight>'.$row['RichDetails.Skylight'].'</Skylight>'."\n");
				
				if(isset($row['RichDetails.SportsCourt']))	
					fputs($f, '			<SportsCourt>'.$row['RichDetails.SportsCourt'].'</SportsCourt>'."\n");
				
				if(isset($row['RichDetails.SprinklerSystem']))	
					fputs($f, '			<SprinklerSystem>'.$row['RichDetails.SprinklerSystem'].'</SprinklerSystem>'."\n");
					
				if(isset($row['RichDetails.VaultedCeiling']))	
					fputs($f, '			<VaultedCeiling>'.$row['RichDetails.VaultedCeiling'].'</VaultedCeiling>'."\n");
			
				if(isset($row['RichDetails.ViewTypes']) && !empty($row['RichDetails.ViewTypes'])){	
					fputs($f, '			<ViewTypes>'."\n");
					
					if(isset($row['RichDetails.ViewTypes.None']) && !empty($row['RichDetails.ViewTypes.None']))
						fputs($f, '				<ViewType>None</ViewType>');
				
					if(isset($row['RichDetails.ViewTypes.City']) && !empty($row['RichDetails.ViewTypes.City']))
						fputs($f, '				<ViewType>City</ViewType>');
						
					if(isset($row['RichDetails.ViewTypes.Mountain']) && !empty($row['RichDetails.ViewTypes.Mountain']))
						fputs($f, '				<ViewType>Mountain</ViewType>');
						
					if(isset($row['RichDetails.ViewTypes.Park']) && !empty($row['RichDetails.ViewTypes.Park']))
						fputs($f, '				<ViewType>Park</ViewType>');
						
					if(isset($row['RichDetails.ViewTypes.Territorial']) && !empty($row['RichDetails.ViewTypes.Territorial']))
						fputs($f, '				<ViewType>Territorial</ViewType>');
				
					if(isset($row['RichDetails.ViewTypes.Water']) && !empty($row['RichDetails.ViewTypes.Water']))
						fputs($f, '				<ViewType>Water</ViewType>');
				
					fputs($f, '			</ViewTypes>'."\n");
				}
				
				if(isset($row['RichDetails.Waterfront']))	
					fputs($f, '			<Waterfront>'.$row['RichDetails.Waterfront'].'</Waterfront>'."\n");
				
				if(isset($row['RichDetails.Wetbar']))	
					fputs($f, '			<Wetbar>'.$row['RichDetails.Wetbar'].'</Wetbar>'."\n");
			
				if(isset($row['RichDetails.WhatOwnerLoves']) && !empty($row['RichDetails.WhatOwnerLoves']))	
					fputs($f, '			<WhatOwnerLoves>'.$row['RichDetails.WhatOwnerLoves'].'</WhatOwnerLoves>'."\n");
					
				if(isset($row['RichDetails.Wired']))
					fputs($f, '			<Wired>'.$row['RichDetails.Wired'].'</Wired>'."\n");
			
				if(isset($row['RichDetails.YearUpdated']) && !empty($row['RichDetails.YearUpdated']) && strlen($row['RichDetails.YearUpdated'])>4)	
					fputs($f, '			<YearUpdated>'.$row['RichDetails.YearUpdated'].'</YearUpdated>'."\n");
					
				if(isset($row['RichDetails.FitnessCenter']))
					fputs($f, '			<FitnessCenter>'.$row['RichDetails.FitnessCenter'].'</FitnessCenter>'."\n");
				
				if(isset($row['RichDetails.BasketballCourt']))
					fputs($f, '			<BasketballCourt>'.$row['RichDetails.BasketballCourt'].'</BasketballCourt>'."\n");
				
				if(isset($row['RichDetails.TennisCourt']))
					fputs($f, '			<TennisCourt>'.$row['RichDetails.TennisCourt'].'</TennisCourt>'."\n");
				
				if(isset($row['RichDetails.NearTransportation']))
					fputs($f, '			<NearTransportation>'.$row['RichDetails.NearTransportation'].'</NearTransportation>'."\n");
					
				if(isset($row['RichDetails.ControlledAccess']))
					fputs($f, '			<ControlledAccess>'.$row['RichDetails.ControlledAccess'].'</ControlledAccess>'."\n");
			
				if(isset($row['RichDetails.Over55ActiveCommunity']))
					fputs($f, '			<Over55ActiveCommunity>'.$row['RichDetails.Over55ActiveCommunity'].'</Over55ActiveCommunity>'."\n");
				
				if(isset($row['RichDetails.AssistedLivingCommunity']))
					fputs($f, '			<AssistedLivingCommunity>'.$row['RichDetails.AssistedLivingCommunity'].'</AssistedLivingCommunity>'."\n");
				
				if(isset($row['RichDetails.Storage']))
					fputs($f, '			<Storage>'.$row['RichDetails.Storage'].'</Storage>'."\n");
					
				if(isset($row['RichDetails.FencedYard']))
					fputs($f, '			<FencedYard>'.$row['RichDetails.FencedYard'].'</FencedYard>'."\n");

				if(isset($row['RichDetails.PropertyName']) && !empty($row['RichDetails.PropertyName']))	
					fputs($f, '			<PropertyName>'.$row['RichDetails.PropertyName'].'</PropertyName>'."\n");
					
				if(isset($row['RichDetails.Furnished']))
					fputs($f, '			<Furnished>'.$row['RichDetails.Furnished'].'</Furnished>'."\n");
					
				if(isset($row['RichDetails.HighspeedInternet']))
					fputs($f, '			<HighspeedInternet>'.$row['RichDetails.HighspeedInternet'].'</HighspeedInternet>'."\n");
					
				if(isset($row['RichDetails.OnsiteLaundry']))
					fputs($f, '			<OnsiteLaundry>'.$row['RichDetails.OnsiteLaundry'].'</OnsiteLaundry>'."\n");
				
				if(isset($row['RichDetails.CableSatTV']))
					fputs($f, '			<CableSatTV>'.$row['RichDetails.CableSatTV'].'</CableSatTV>'."\n");
					
				if(isset($row['RichDetails.Taxes']) && !empty($row['RichDetails.Taxes'])){
					fputs($f, '			<Taxes>'."\n");
					fputs($f, '				<Tax>'."\n");
					
					foreach((array)$row['RichDetails.Taxes'] as $tax){
						fputs($f, '					<Year>'.$tax['Year'].'</Year>'."\n");
						fputs($f, '					<Amount>'.$tax['Amount'].'</Amount>'."\n");
					}
					
					fputs($f, '				</Tax>'."\n");
					fputs($f, '			</Taxes>'."\n");
				}
				
				fputs($f, '		</RichDetails>'."\n");			
			}
			
			fputs($f, '	</Listing>'."\n");
		}		
		fputs($f, '</Listings>'."\n");		
		fclose($f);
	}
	
	/**
	 * Validate export item data
	 * 
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());
		
		$this->CI->config->load("reg_exps", TRUE);
		
		$email_expr = $this->CI->config->item("email", "reg_exps");
		
		if(isset($data['Location.StreetAddress'])){		
			$return['data']['Location.StreetAddress'] = $data['Location.StreetAddress'];
			if(!empty($return['data']['Location.StreetAddress'])){
				$return['data']['Location.StreetAddress']  = str_replace($this->search, $this->replace, $return['data']['Location.StreetAddress']);
			}else{
				$return["errors"][] = "Location.StreetAddress";
			}
		}else{
			$return["errors"][] = "Location.StreetAddress";
		}
		
		if(isset($data['Location.UnitNumber'])){
			$return['data']['Location.UnitNumber'] = str_replace($this->search, $this->replace, $data['Location.UnitNumber']);
		}
		
		if(isset($data['Location.City'])){
			$return['data']['Location.City'] = $data['Location.City'];
			if(!empty($return['data']['Location.City'])){
				$return['data']['Location.City'] = str_replace($this->search, $this->replace, $return['data']['Location.City']);
			}else{
				$return["errors"][] = "Location.City";
			}
		}else{
			$return["errors"][] = "Location.City";
		}				
		
		if(isset($data['Location.State'])){
			$return['data']['Location.State'] = $data['Location.State'];
			if(!empty($return['data']['Location.State'])){
				$return['data']['Location.State'] = str_replace($this->search, $this->replace, $return['data']['Location.State']);
			}else{
				$return["errors"][] = "Location.State";
			}
		}else{
			$return["errors"][] = "Location.State";
		}
		
		if(isset($data['Location.Zip'])){
			$return['data']['Location.Zip'] = $data['Location.Zip'];
			if(!empty($return['data']['Location.Zip'])){
				$return['data']['Location.Zip'] = str_replace($this->search, $this->replace, $return['data']['Location.Zip']);
			}else{
				$return["errors"][] = "Location.Zip";
			}
		}else{
			$return["errors"][] = "Location.Zip";
		}
		
		if(isset($data['Location.ParcelId'])){
			$return['data']['Location.ParcelId'] = str_replace($this->search, $this->replace, $data['Location.ParcelId']);
		}
		
		if(isset($data['Location.ZPID'])){
			$return['data']['Location.ZPID'] = str_replace($this->search, $this->replace, $data['Location.ZPID']);
		}
		
		if(isset($data['Location.Lat'])){
			$return['data']['Location.Lat'] = floatval($data['Location.Lat']);
		}	
		
		if(isset($data['Location.Long'])){
			$return['data']['Location.Long'] = floatval($data['Location.Long']);
		}	
		
		if(isset($data['Location.County'])){
			$return['data']['Location.County'] = str_replace($this->search, $this->replace, $data['Location.County']);
		}
		
		if(isset($data['Location.StreetIntersection'])){
			$return['data']['Location.StreetIntersection'] = str_replace($this->search, $this->replace, $data['Location.StreetIntersection']);
		}
		
		if(isset($data['Location.DisplayAddress'])){
			$return['data']['Location.DisplayAddress'] = $data['Location.DisplayAddress'] ? 'Yes' : 'No';
		}
		
		if(isset($data['ListingDetails.Status'])){
			switch(true){
				case (isset($data['ListingType']) && in_array($data['ListingType'], array('rent', 'for rent', 'For rent'))):
					$return['data']['ListingDetails.Status'] = 'For rent';
				break;
				case (isset($data['ListingSold'])):
					$return['data']['ListingDetails.Status'] = 'Sold';
				break;
				default:
					$return['data']['ListingDetails.Status'] = $data['ListingDetails.Status'] ? 'Active' : 'Pending';
				break;
			}
		}else{
			$return["errors"][] = "ListingDetails.Status";
		}
		
		if(isset($data['ListingDetails.Price'])){
			$return['data']['ListingDetails.Price'] = $data['ListingDetails.Price'];
			if(!empty($return['data']['ListingDetails.Price'])){
				$return['data']['ListingDetails.Price'] = str_replace($this->search, $this->replace, $return['data']['ListingDetails.Price']);
			}else{
				$return["errors"][] = "ListingDetails.Price";
			}
		}else{
			$return["errors"][] = "ListingDetails.Price";
		}
		
		if(isset($data['ListingDetails.ListingUrl'])){
			$return['data']['ListingDetails.ListingUrl'] = str_replace($this->search, $this->replace, $data['ListingDetails.ListingUrl']);
		}
		
		if(isset($data['ListingDetails.MlsId'])){
			$return['data']['ListingDetails.MlsId'] = str_replace($this->search, $this->replace, $data['ListingDetails.MlsId']);
		}
		
		if(isset($data['ListingDetails.MlsName'])){
			$return['data']['ListingDetails.MlsName'] = str_replace($this->search, $this->replace, $data['ListingDetails.MlsName']);
		}
		
		if(isset($data['ListingDetails.DateListed'])){
			$value = strtotime($data['ListingDetails.DateListed']);
			if($value > 0) $return['data']['ListingDetails.DateListed'] = date('Y-m-d', $value);
		}
			
		if(isset($data['ListingDetails.DateSold'])){
			$value = strtotime($data['ListingDetails.DateSold']);
			if($value > 0) $return['data']['ListingDetails.DateSold'] = date('Y-m-d', $value);
		}

		if(isset($data['ListingDetails.VirtualTourUrl'])){
			$return['data']['ListingDetails.VirtualTourUrl'] = $data['ListingDetails.VirtualTourUrl'];
			if(!empty($data['ListingDetails.VirtualTourUrl'])){
				$return['data']['ListingDetails.VirtualTourUrl'] = str_replace($this->search, $this->replace, array_shift(explode('|', $return['data']['ListingDetails.VirtualTourUrl'])));
			}
		}
			
		if(isset($data['ListingDetails.ListingEmail'])){
			$return['data']['ListingDetails.ListingEmail'] = $data['ListingDetails.ListingEmail'];
			if(!empty($return['data']['ListingDetails.ListingEmail']) && preg_match($email_expr, $return['data']['ListingDetails.ListingEmail'])){
				$return['data']['ListingDetails.ListingEmail'] = str_replace($this->search, $this->replace, $return['data']['ListingDetails.ListingEmail']);
			}
		}
		
		if(isset($data['ListingDetails.AlwaysEmailAgent'])){
			$return['data']['ListingDetails.AlwaysEmailAgent'] = $data['ListingDetails.AlwaysEmailAgent'] ? 1 : 0;
		}	
		
		if(isset($data['ListingDetails.ShortSale'])){
			$return['data']['ListingDetails.ShortSale'] = $data['ListingDetails.ShortSale'] ? 'Yes' : 'No';
		}	
		
		if(isset($data['ListingDetails.REO'])){
			$return['data']['ListingDetails.REO'] = $data['ListingDetails.REO'] ? 'Yes' : 'No';
		}
		
		if(isset($data['RentalDetails.Availability'])){
			switch($data['RentalDetails.Availability']){
				case 'ContactForDetails':
				case 'Now':
					$return['data']['RentalDetails.Availability'] = $data['RentalDetails.Availability'];
				break;
				default:
					$value = strtotime($data['RentalDetails.Availability']);
					if($value > 0) $return['data']['RentalDetails.Availability'] = date('Y-m-d', $value);
				break;
			}
		}
		
		if(isset($data['RentalDetails.LeaseTerm'])){
			if(in_array($data['RentalDetails.LeaseTerm'], $this->lease_term)){
				$return['data']['RentalDetails.LeaseTerm'] = str_replace($this->search, $this->replace, $data['RentalDetails.LeaseTerm']);
			}
		}
		
		if(isset($data['RentalDetails.DepositFees'])){
			$return['data']['RentalDetails.DepositFees'] = str_replace($this->search, $this->replace, $data['RentalDetails.DepositFees']);
		}
		
		if(isset($data['RentalDetails.UtilitiesIncluded.Water'])){
			$return['data']['RentalDetails.UtilitiesIncluded.Water'] = $data['RentalDetails.UtilitiesIncluded.Water'] ? 'Yes' : 'No';
			$return['data']['RentalDetails.UtilitiesIncluded'] = true;
		}
		
		if(isset($data['RentalDetails.UtilitiesIncluded.Sewage'])){
			$return['data']['RentalDetails.UtilitiesIncluded.Sewage'] = $data['RentalDetails.UtilitiesIncluded.Sewage'] ? 'Yes' : 'No';
			$return['data']['RentalDetails.UtilitiesIncluded'] = true;
		}
		
		if(isset($data['RentalDetails.UtilitiesIncluded.Garbage'])){
			$return['data']['RentalDetails.UtilitiesIncluded.Garbage'] = $data['RentalDetails.UtilitiesIncluded.Garbage'] ? 'Yes' : 'No';
			$return['data']['RentalDetails.UtilitiesIncluded'] = true;
		}
		
		if(isset($data['RentalDetails.UtilitiesIncluded.Electricity'])){
			$return['data']['RentalDetails.UtilitiesIncluded.Electricity'] = $data['RentalDetails.UtilitiesIncluded.Electricity'] ? 'Yes' : 'No';
			$return['data']['RentalDetails.UtilitiesIncluded'] = true;
		}
		
		if(isset($data['RentalDetails.UtilitiesIncluded.Gas'])){
			$return['data']['RentalDetails.UtilitiesIncluded.Gas'] = $data['RentalDetails.UtilitiesIncluded.Gas'] ? 'Yes' : 'No';
			$return['data']['RentalDetails.UtilitiesIncluded'] = true;
		}
			   
		if(isset($data['RentalDetails.UtilitiesIncluded.Internet'])){
			$return['data']['RentalDetails.UtilitiesIncluded.Internet'] = $data['RentalDetails.UtilitiesIncluded.Internet'] ? 'Yes' : 'No';
			$return['data']['RentalDetails.UtilitiesIncluded'] = true;
		}
		
		if(isset($data['RentalDetails.UtilitiesIncluded.Cable'])){
			$return['data']['RentalDetails.UtilitiesIncluded.Cable'] = $data['RentalDetails.UtilitiesIncluded.Cable'] ? 'Yes' : 'No';
			$return['data']['RentalDetails.UtilitiesIncluded'] = true;
		}
		
		if(isset($data['RentalDetails.UtilitiesIncluded.SatTV'])){
			$return['data']['RentalDetails.UtilitiesIncluded.SatTV'] = $data['RentalDetails.UtilitiesIncluded.SatTV'] ? 'Yes' : 'No';
			$return['data']['RentalDetails.UtilitiesIncluded'] = true;
		}
		
		if(isset($data['RentalDetails.PetsAllowed.NoPets'])){
			$return['data']['RentalDetails.PetsAllowed.NoPets'] = $data['RentalDetails.PetsAllowed.NoPets'] ? 'Yes' : 'No';
			$return['data']['RentalDetails.PetsAllowed'] = true;
		}
		
		if(isset($data['RentalDetails.PetsAllowed.Cats'])){
			$return['data']['RentalDetails.PetsAllowed.Cats'] = $data['RentalDetails.PetsAllowed.Cats'] ? 'Yes' : 'No';
			$return['data']['RentalDetails.PetsAllowed'] = true;
		}
		
		if(isset($data['RentalDetails.PetsAllowed.SmallDogs'])){
			$return['data']['RentalDetails.PetsAllowed.SmallDogs'] = $data['RentalDetails.PetsAllowed.SmallDogs'] ? 'Yes' : 'No';
			$return['data']['RentalDetails.PetsAllowed'] = true;
		}
		
		if(isset($data['RentalDetails.PetsAllowed.LargeDogs'])){
			$return['data']['RentalDetails.PetsAllowed.LargeDogs'] = $data['RentalDetails.PetsAllowed.LargeDogs'] ? 'Yes' : 'No';
			$return['data']['RentalDetails.PetsAllowed'] = true;
		}
		
		if(isset($data['BasicDetails.PropertyType'])){
			$return['data']['BasicDetails.PropertyType'] = $data['BasicDetails.PropertyType'];
			if(!in_array($data["BasicDetails.PropertyType"], $this->property_types)){
				if(isset($data['BasicDetails.PropertyType2'])){
					$return['data']['BasicDetails.PropertyType'] = $data['BasicDetails.PropertyType2'];
					if(!in_array($data["BasicDetails.PropertyType"], $this->property_types)){
						if(isset($data['BasicDetails.PropertyType3'])){
							$return['data']['BasicDetails.PropertyType'] = $data['BasicDetails.PropertyType3'];
							if(!in_array($data["BasicDetails.PropertyType"], $this->property_types)){
								if(isset($data['BasicDetails.PropertyType4'])){
									$return['data']['BasicDetails.PropertyType'] = $data['BasicDetails.PropertyType4'];
									if(!in_array($data["BasicDetails.PropertyType"], $this->property_types)){
										if(isset($data['BasicDetails.PropertyType5'])){
											$return['data']['BasicDetails.PropertyType'] = $data['BasicDetails.PropertyType5'];
											if(!in_array($data["BasicDetails.PropertyType"], $this->property_types)){
												if(isset($data['BasicDetails.PropertyType6'])){
													$return['data']['BasicDetails.PropertyType'] = $data['BasicDetails.PropertyType6'];
													if(!in_array($data["BasicDetails.PropertyType"], $this->property_types)){
														$return['data']['BasicDetails.PropertyType'] = current($this->property_types);
														//$return["errors"][] = "BasicDetails.PropertyType";
													}
												}else{
													$return['data']['BasicDetails.PropertyType'] = current($this->property_types);
													//$return["errors"][] = "BasicDetails.PropertyType";
												}
											}
										}else{
											$return['data']['BasicDetails.PropertyType'] = current($this->property_types);
											//$return["errors"][] = "BasicDetails.PropertyType";
										}
									}
								}else{
									$return['data']['BasicDetails.PropertyType'] = current($this->property_types);
									//$return["errors"][] = "BasicDetails.PropertyType";
								}
							}
						}else{
							$return['data']['BasicDetails.PropertyType'] = current($this->property_types);
							//$return["errors"][] = "BasicDetails.PropertyType";
						}
					}
				}else{
					$return['data']['BasicDetails.PropertyType'] = current($this->property_types);
					//$return["errors"][] = "BasicDetails.PropertyType";
				}
			}
		}else{
			$return['data']['BasicDetails.PropertyType'] = current($this->property_types);
			//$return["errors"][] = "BasicDetails.PropertyType";
		}
		
		if(isset($data['BasicDetails.Title'])){
			$return['data']['BasicDetails.Title'] = str_replace($this->search, $this->replace, $data['BasicDetails.Title']);
		}
	
		if(isset($data['BasicDetails.Description'])){
			$return['data']['BasicDetails.Description'] = str_replace($this->search, $this->replace, $data['BasicDetails.Description']);
		}
		
		if(isset($data['BasicDetails.Bedrooms'])){
			$return['data']['BasicDetails.Bedrooms'] = intval($data['BasicDetails.Description']);
		}
		
		if(isset($data['BasicDetails.Bathrooms'])){
			$return['data']['BasicDetails.Bathrooms'] = intval($data['BasicDetails.Bathrooms']);
		}
		
		if(isset($data['BasicDetails.FullBathrooms'])){
			$return['data']['BasicDetails.FullBathrooms'] = intval($data['BasicDetails.FullBathrooms']);
		}
			
		if(isset($data['BasicDetails.HalfBathrooms'])){
			$return['data']['BasicDetails.HalfBathrooms'] = intval($data['BasicDetails.HalfBathrooms']);
		}
		
		if(isset($data['BasicDetails.LivingArea'])){
			if($return['data']['BasicDetails.PropertyType'] != 'VacantLand'){
				$return['data']['BasicDetails.LivingArea'] = intval($data['BasicDetails.LivingArea']);
			}
		}	
		
		if(isset($data['BasicDetails.LotSize'])){
			if($return['data']['BasicDetails.PropertyType'] == 'VacantLand'){
				$return['data']['BasicDetails.LotSize'] = intval($data['BasicDetails.LotSize']);
			}
		}	
		
		if(isset($data['BasicDetails.YearBuilt'])){
			if(strlen($data['BasicDetails.YearBuilt']) == 4){
				$return['data']['BasicDetails.YearBuilt'] = intval($data['BasicDetails.YearBuilt']);
			}
		}

		if(isset($data['Pictures.PictureUrl'])){
			if(!empty($data['Pictures.PictureUrl'])){
				$return['data']['Pictures.PictureUrl'] = explode('|', $data['Pictures.PictureUrl']);
				foreach($return['data']['Pictures.PictureUrl'] as $key=>$value){
					$return['data']['Pictures'][$key]['PictureUrl'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['Pictures.Caption'])){
			if(!empty($data['Pictures.Caption'])){
				$return['data']['Pictures.Caption'] = explode('|', $data['Pictures.Caption']);
				foreach($return['data']['Pictures.Caption'] as $key=>$value){
					$return['data']['Pictures'][$key]['Caption'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
				   
		if(isset($data['Agent.FirstName'])){
			$return['data']['Agent.FirstName'] = str_replace($this->search, $this->replace, $data['Agent.FirstName']);
		}
		
		if(isset($data['Agent.LastName'])){
			$return['data']['Agent.LastName'] = str_replace($this->search, $this->replace, $data['Agent.LastName']);
		}
		
		if(isset($data['Agent.EmailAddress'])){
			$return['data']['Agent.EmailAddress'] = $data['Agent.EmailAddress'];
			if(!empty($return['data']['Agent.EmailAddress']) && preg_match($email_expr, $return['data']['Agent.EmailAddress'])){
				$return['data']['Agent.EmailAddress'] = str_replace($this->search, $this->replace, $return['data']['Agent.EmailAddress']);
			}else{
				$return["errors"][] = "Agent.EmailAddress";
			}
		}
		
		if(isset($data['Agent.PictureUrl'])){
			$return['data']['Agent.PictureUrl'] = str_replace($this->search, $this->replace, $data['Agent.PictureUrl']);
		}
	
		if(isset($data['Agent.OfficeLineNumber'])){
			if(preg_match('/^\d{3}-\d{3}-\d{3}/', $data['Agent.OfficeLineNumber'])){
				$return['data']['Agent.OfficeLineNumber'] = str_replace($this->search, $this->replace, $data['Agent.OfficeLineNumber']);
			}
		}
	
		if(isset($data['Agent.MobilePhoneLineNumber'])){
			if(preg_match('/^\d{3}-\d{3}-\d{3}/', $data['Agent.MobilePhoneLineNumber'])){
				$return['data']['Agent.MobilePhoneLineNumber'] = str_replace($this->search, $this->replace, $data['Agent.MobilePhoneLineNumber']);
			}
		}
		
		if(isset($data['Agent.FaxLineNumber'])){
			if(preg_match('/^\d{3}-\d{3}-\d{3}(\sx\d{5})?/', $data['Agent.FaxLineNumber'])){
				$return['data']['Agent.FaxLineNumber'] = str_replace($this->search, $this->replace, $data['Agent.FaxLineNumber']);
			}
		}
		
		if(isset($data['Office.BrokerageName'])){
			$return['data']['Office.BrokerageName'] = str_replace($this->search, $this->replace, $data['Office.BrokerageName']);
		}
	
		if(isset($data['Office.BrokerPhone'])){
			$return['data']['Office.BrokerPhone'] = $data['Office.BrokerPhone'];
			if(!empty($return['data']['Office.BrokerPhone'])){
				$return['data']['Office.BrokerPhone'] = str_replace($this->search, $this->replace, $return['data']['Office.BrokerPhone']);
			}else{
				$return["errors"][] = "Office.BrokerPhone";
			}
		}
	
		if(isset($data['Office.BrokerEmail'])){
			$return['data']['Office.BrokerEmail'] = str_replace($this->search, $this->replace, $data['Office.BrokerEmail']);
		}

		if(isset($data['Office.BrokerWebsite'])){
			$return['data']['Office.BrokerWebsite'] = str_replace($this->search, $this->replace, $data['Office.BrokerWebsite']);
		}
		
		if(isset($data['Office.StreetAddress'])){
			$return['data']['Office.StreetAddress'] = str_replace($this->search, $this->replace, $data['Office.StreetAddress']);
		}
		
		if(isset($data['Office.UnitNumber'])){
			$return['data']['Office.UnitNumber'] = str_replace($this->search, $this->replace, $data['Office.UnitNumber']);
		}
		
		if(isset($data['Office.City'])){
			$return['data']['Office.City'] = str_replace($this->search, $this->replace, $data['Office.City']);
		}
		
		if(isset($data['Office.State'])){
			$return['data']['Office.State'] = str_replace($this->search, $this->replace, $data['Office.State']);
		}
		
		if(isset($data['Office.Zip'])){
			$return['data']['Office.Zip'] = str_replace($this->search, $this->replace, $data['Office.Zip']);
		}
		
		if(isset($data['Office.Name'])){
			$return['data']['Office.Name'] = str_replace($this->search, $this->replace, $data['Office.Name']);
		}
		
		if(isset($data['Office.FranchiseName'])){
			$return['data']['Office.FranchiseName'] = str_replace($this->search, $this->replace, $data['Office.FranchiseName']);
		}
		
		if(isset($data['OpenHouses.OpenHouse.Date'])){
			$value = strtotime($data['OpenHouses.OpenHouse.Date']);
			if($value > 0) $return['data']['OpenHouses.OpenHouse.Date'] = date('Y-m-d', $value);
		}
	
		if(isset($data['OpenHouses.OpenHouse.StartTime'])){
			$return['data']['OpenHouses.OpenHouse.StartTime'] = str_replace($this->search, $this->replace, $data['OpenHouses.OpenHouse.StartTime']);
		}
		
		if(isset($data['OpenHouses.OpenHouse.EndTime'])){
			$return['data']['OpenHouses.OpenHouse.EndTime'] = str_replace($this->search, $this->replace, $data['OpenHouses.OpenHouse.EndTime']);
		}
		
		if(isset($data['Schools.District'])){
			$return['data']['Schools.District'] = str_replace($this->search, $this->replace, $data['Schools.District']);
			$return['data']['Schools'] = true;
		}
		
		if(isset($data['Schools.Elementary'])){
			$return['data']['Schools.Elementary'] = str_replace($this->search, $this->replace, $data['Schools.Elementary']);
			$return['data']['Schools'] = true;
		}
		
		if(isset($data['Schools.Middle'])){
			$return['data']['Schools.Middle'] = str_replace($this->search, $this->replace, $data['Schools.Middle']);
			$return['data']['Schools'] = true;
		}
		
		if(isset($data['Schools.High'])){
			$return['data']['Schools.High'] = $data['Schools.High'];
			if(!empty($return['data']['Schools.High'])){
				$return['data']['Schools.High'] = str_replace($this->search, $this->replace, $return['data']['Schools.High']);
				$return['data']['Schools'] = true;
			}
		}
		
		if(isset($data['Neighborhood.Name'])){
			$return['data']['Neighborhood.Name'] = $data['Neighborhood.Name'];
			if(!empty($return['data']['Neighborhood.Name'])){
				$return['data']['Neighborhood.Name'] = str_replace($this->search, $this->replace, $return['data']['Neighborhood.Name']);
				$return['data']['Neighborhood'] = true;
			}
		}
		
		if(isset($data['Neighborhood.Description'])){
			$return['data']['Neighborhood.Description'] = str_replace($this->search, $this->replace, $data['Neighborhood.Description']);
			$return['data']['Neighborhood'] = true;
		}
		
		if(isset($data['RichDetails.AdditionalFeatures'])){
			$return['data']['RichDetails.AdditionalFeatures'] = $data['RichDetails.AdditionalFeatures'];
			if(!empty($return['data']['RichDetails.AdditionalFeatures'])){
				$return['data']['RichDetails.AdditionalFeatures'] = str_replace($this->search, $this->replace, $return['data']['RichDetails.AdditionalFeatures']);
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Appliances.Dishwasher'])){
			$return['data']['RichDetails.Appliances.Dishwasher'] = $data['RichDetails.Appliances.Dishwasher'] ? 1 : 0;
			if($return['data']['RichDetails.Appliances.Dishwasher']){
				$return['data']['RichDetails.Appliances'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Appliances.Dryer'])){
			$return['data']['RichDetails.Appliances.Dryer'] = $data['RichDetails.Appliances.Dryer'] ? 1 : 0;
			if($return['data']['RichDetails.Appliances.Dryer']){
				$return['data']['RichDetails.Appliances'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Appliances.Freezer'])){
			$return['data']['RichDetails.Appliances.Freezer'] = $data['RichDetails.Appliances.Freezer'] ? 1 : 0;
			if($return['data']['RichDetails.Appliances.Freezer']){
				$return['data']['RichDetails.Appliances'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Appliances.GarbageDisposal'])){
			$return['data']['RichDetails.Appliances.GarbageDisposal'] = $data['RichDetails.Appliances.GarbageDisposal'] ? 1 : 0;
			if($return['data']['RichDetails.Appliances.GarbageDisposal']){
				$return['data']['RichDetails.Appliances'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Appliances.Microwave'])){
			$return['data']['RichDetails.Appliances.Microwave'] = $data['RichDetails.Appliances.Microwave'] ? 1 : 0;
			if($return['data']['RichDetails.Appliances.Microwave']){
				$return['data']['RichDetails.Appliances'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Appliances.RangeOven'])){
			$return['data']['RichDetails.Appliances.RangeOven'] = $data['RichDetails.Appliances.RangeOven'] ? 1 : 0;
			if($return['data']['RichDetails.Appliances.RangeOven']){
				$return['data']['RichDetails.Appliances'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Appliances.Refrigerator'])){
			$return['data']['RichDetails.Appliances.Refrigerator'] = $data['RichDetails.Appliances.Refrigerator'] ? 1 : 0;
			if($return['data']['RichDetails.Appliances.Refrigerator']){
				$return['data']['RichDetails.Appliances'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Appliances.TrashCompactor'])){
			$return['data']['RichDetails.Appliances.TrashCompactor'] = $data['RichDetails.Appliances.TrashCompactor'] ? 1 : 0;
			if($return['data']['RichDetails.Appliances.TrashCompactor']){
				$return['data']['RichDetails.Appliances'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Appliances.Washer'])){
			$return['data']['RichDetails.Appliances.Washer'] = $data['RichDetails.Appliances.Washer'] ? 1 : 0;
			if($return['data']['RichDetails.Appliances.Washer']){
				$return['data']['RichDetails.Appliances'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ArchitectureStyle'])){
			if(in_array($data['RichDetails.ArchitectureStyle'], $this->architecture_styles)){
				$return['data']['RichDetails.ArchitectureStyle'] = $data['RichDetails.ArchitectureStyle'];
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Attic'])){
			$return['data']['RichDetails.Attic'] = $data['RichDetails.Attic'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.BarbecueArea'])){
			$return['data']['RichDetails.BarbecueArea'] = $data['RichDetails.BarbecueArea'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Basement'])){
			$return['data']['RichDetails.Basement'] = $data['RichDetails.Basement'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.BuildingUnitCount'])){
			if(!empty($data['RichDetails.BuildingUnitCount']) && in_array($return['data']['BasicDetails.PropertyType'], array('Condo', 'Townhouse', 'Coop', 'MultiFamily'))){
				$return['data']['RichDetails.BuildingUnitCount'] = $data['RichDetails.BuildingUnitCount'];
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.CableReady'])){
			$return['data']['RichDetails.CableReady'] = $data['RichDetails.CableReady'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.CeilingFan'])){
			$return['data']['RichDetails.CeilingFan'] = $data['RichDetails.CeilingFan'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.CondoFloorNum'])){
			if(!empty($data['RichDetails.CondoFloorNum']) && in_array($return['data']['BasicDetails.PropertyType'], array('Condo', 'Coop'))){
				$return['data']['RichDetails.CondoFloorNum'] = $data['RichDetails.CondoFloorNum'];
				$return['data']['RichDetails'] = true;
			}
		}

		if(isset($data['RichDetails.CoolingSystems.None'])){
			$return['data']['RichDetails.CoolingSystems.None'] = $data['RichDetails.CoolingSystems.None'] ? 1 : 0;
			if($return['data']['RichDetails.CoolingSystems.None']){
				$return['data']['RichDetails.CoolingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.CoolingSystems.Central'])){
			$return['data']['RichDetails.CoolingSystems.Central'] = $data['RichDetails.CoolingSystems.Central'] ? 1 : 0;
			if($return['data']['RichDetails.CoolingSystems.Central']){
				$return['data']['RichDetails.CoolingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.CoolingSystems.Evaporative'])){
			$return['data']['RichDetails.CoolingSystems.Evaporative'] = $data['RichDetails.CoolingSystems.Evaporative'] ? 1 : 0;
			if($return['data']['RichDetails.CoolingSystems.Evaporative']){
				$return['data']['RichDetails.CoolingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.CoolingSystems.Geothermal'])){
			$return['data']['RichDetails.CoolingSystems.Geothermal'] = $data['RichDetails.CoolingSystems.Geothermal'] ? 1 : 0;
			if($return['data']['RichDetails.CoolingSystems.Geothermal']){
				$return['data']['RichDetails.CoolingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.CoolingSystems.Wall'])){
			$return['data']['RichDetails.CoolingSystems.Wall'] = $data['RichDetails.CoolingSystems.Wall'] ? 1 : 0;
			if($return['data']['RichDetails.CoolingSystems.Wall']){
				$return['data']['RichDetails.CoolingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.CoolingSystems.Solar'])){
			$return['data']['RichDetails.CoolingSystems.Solar'] = $data['RichDetails.CoolingSystems.Solar'] ? 1 : 0;
			if($return['data']['RichDetails.CoolingSystems.Solar']){
				$return['data']['RichDetails.CoolingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.CoolingSystems.Other'])){
			$return['data']['RichDetails.CoolingSystems.Other'] = $data['RichDetails.CoolingSystems.Other'] ? 1 : 0;
			if($return['data']['RichDetails.CoolingSystems.Other']){
				$return['data']['RichDetails.CoolingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}

		if(isset($data['RichDetails.Deck'])){
			$return['data']['RichDetails.Deck'] = $data['RichDetails.Deck'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.DisabledAccess'])){
			$return['data']['RichDetails.DisabledAccess'] = $data['RichDetails.DisabledAccess'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Dock'])){
			$return['data']['RichDetails.Dock'] = $data['RichDetails.Dock'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Doorman'])){
			$return['data']['RichDetails.Doorman'] = $data['RichDetails.Doorman'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.DoublePaneWindows'])){
			$return['data']['RichDetails.DoublePaneWindows'] = $data['RichDetails.DoublePaneWindows'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Elevator'])){
			$return['data']['RichDetails.Elevator'] = $data['RichDetails.Elevator'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.ExteriorTypes.Brick'])){
			$return['data']['RichDetails.ExteriorTypes.Brick'] = $data['RichDetails.ExteriorTypes.Brick'] ? 1 : 0;
			if($return['data']['RichDetails.ExteriorTypes.Brick']){
				$return['data']['RichDetails.ExteriorTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ExteriorTypes.CementConcrete'])){
			$return['data']['RichDetails.ExteriorTypes.CementConcrete'] = $data['RichDetails.ExteriorTypes.CementConcrete'] ? 1 : 0;
			if($return['data']['RichDetails.ExteriorTypes.CementConcrete']){
				$return['data']['RichDetails.ExteriorTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ExteriorTypes.Composition'])){
			$return['data']['RichDetails.ExteriorTypes.Composition'] = $data['RichDetails.ExteriorTypes.Composition'] ? 1 : 0;
			if($return['data']['RichDetails.ExteriorTypes.Composition']){
				$return['data']['RichDetails.ExteriorTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ExteriorTypes.Metal'])){
			$return['data']['RichDetails.ExteriorTypes.Metal'] = $data['RichDetails.ExteriorTypes.Metal'] ? 1 : 0;
			if($return['data']['RichDetails.ExteriorTypes.Metal']){
				$return['data']['RichDetails.ExteriorTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ExteriorTypes.Shingle'])){
			$return['data']['RichDetails.ExteriorTypes.Shingle'] = $data['RichDetails.ExteriorTypes.Shingle'] ? 1 : 0;
			if($return['data']['RichDetails.ExteriorTypes.Shingle']){
				$return['data']['RichDetails.ExteriorTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ExteriorTypes.Stone'])){
			$return['data']['RichDetails.ExteriorTypes.Stone'] = $data['RichDetails.ExteriorTypes.Stone'] ? 1 : 0;
			if($return['data']['RichDetails.ExteriorTypes.Stone']){
				$return['data']['RichDetails.ExteriorTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ExteriorTypes.Stucco'])){
			$return['data']['RichDetails.ExteriorTypes.Stucco'] = $data['RichDetails.ExteriorTypes.Stucco'] ? 1 : 0;
			if($return['data']['RichDetails.ExteriorTypes.Stucco']){
				$return['data']['RichDetails.ExteriorTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ExteriorTypes.Vinyl'])){
			$return['data']['RichDetails.ExteriorTypes.Vinyl'] = $data['RichDetails.ExteriorTypes.Vinyl'] ? 1 : 0;
			if($return['data']['RichDetails.ExteriorTypes.Vinyl']){
				$return['data']['RichDetails.ExteriorTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ExteriorTypes.Wood'])){
			$return['data']['RichDetails.ExteriorTypes.Wood'] = $data['RichDetails.ExteriorTypes.Wood'] ? 1 : 0;
			if($return['data']['RichDetails.ExteriorTypes.Wood']){
				$return['data']['RichDetails.ExteriorTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ExteriorTypes.WoodProducts'])){
			$return['data']['RichDetails.ExteriorTypes.WoodProducts'] = $data['RichDetails.ExteriorTypes.WoodProducts'] ? 1 : 0;
			if($return['data']['RichDetails.ExteriorTypes.WoodProducts']){
				$return['data']['RichDetails.ExteriorTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ExteriorTypes.Other'])){
			$return['data']['RichDetails.ExteriorTypes.Other'] = $data['RichDetails.ExteriorTypes.Other'] ? 1 : 0;
			if($return['data']['RichDetails.ExteriorTypes.Other']){
				$return['data']['RichDetails.ExteriorTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Fireplace'])){
			$return['data']['RichDetails.Fireplace'] = $data['RichDetails.Fireplace'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.FloorCoverings.Carpet'])){
			$return['data']['RichDetails.FloorCoverings.Carpet'] = $data['RichDetails.FloorCoverings.Carpet'] ? 1 : 0;
			if($return['data']['RichDetails.FloorCoverings.Carpet']){
				$return['data']['RichDetails.FloorCoverings'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.FloorCoverings.Concrete'])){
			$return['data']['RichDetails.FloorCoverings.Concrete'] = $data['RichDetails.FloorCoverings.Concrete'] ? 1 : 0;
			if($return['data']['RichDetails.FloorCoverings.Concrete']){
				$return['data']['RichDetails.FloorCoverings'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.FloorCoverings.Hardwood'])){
			$return['data']['RichDetails.FloorCoverings.Hardwood'] = $data['RichDetails.FloorCoverings.Hardwood'] ? 1 : 0;
			if($return['data']['RichDetails.FloorCoverings.Hardwood']){
				$return['data']['RichDetails.FloorCoverings'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.FloorCoverings.Laminate'])){
			$return['data']['RichDetails.FloorCoverings.Laminate'] = $data['RichDetails.FloorCoverings.Laminate'] ? 1 : 0;
			if($return['data']['RichDetails.FloorCoverings.Laminate']){
				$return['data']['RichDetails.FloorCoverings'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.FloorCoverings.LinoleumVinyl'])){
			$return['data']['RichDetails.FloorCoverings.LinoleumVinyl'] = $data['RichDetails.FloorCoverings.LinoleumVinyl'] ? 1 : 0;
			if($return['data']['RichDetails.FloorCoverings.LinoleumVinyl']){
				$return['data']['RichDetails.FloorCoverings'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.FloorCoverings.Slate'])){
			$return['data']['RichDetails.FloorCoverings.Slate'] = $data['RichDetails.FloorCoverings.Slate'] ? 1 : 0;
			if($return['data']['RichDetails.FloorCoverings.Slate']){
				$return['data']['RichDetails.FloorCoverings'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.FloorCoverings.Softwood'])){
			$return['data']['RichDetails.FloorCoverings.Softwood'] = $data['RichDetails.FloorCoverings.Softwood'] ? 1 : 0;
			if($return['data']['RichDetails.FloorCoverings.Softwood']){
				$return['data']['RichDetails.FloorCoverings'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.FloorCoverings.Tile'])){
			$return['data']['RichDetails.FloorCoverings.Tile'] = $data['RichDetails.FloorCoverings.Tile'] ? 1 : 0;
			if($return['data']['RichDetails.FloorCoverings.Tile']){
				$return['data']['RichDetails.FloorCoverings'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.FloorCoverings.Other'])){
			$return['data']['RichDetails.FloorCoverings.Other'] = $data['RichDetails.FloorCoverings.Other'] ? 1 : 0;
			if($return['data']['RichDetails.FloorCoverings.Other']){
				$return['data']['RichDetails.FloorCoverings'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Garden'])){
			$return['data']['RichDetails.Garden'] = $data['RichDetails.Garden'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.GatedEntry'])){
			$return['data']['RichDetails.GatedEntry'] = $data['RichDetails.GatedEntry'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Greenhouse'])){
			$return['data']['RichDetails.Greenhouse'] = $data['RichDetails.Greenhouse'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.HeatingFuels.None'])){
			$return['data']['RichDetails.HeatingFuels.None'] = $data['RichDetails.HeatingFuels.None'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingFuels.None']){
				$return['data']['RichDetails.HeatingFuels'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingFuels.Coal'])){
			$return['data']['RichDetails.HeatingFuels.Coal'] = $data['RichDetails.HeatingFuels.Coal'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingFuels.Coal']){
				$return['data']['RichDetails.HeatingFuels'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingFuels.Electric'])){
			$return['data']['RichDetails.HeatingFuels.Electric'] = $data['RichDetails.HeatingFuels.Electric'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingFuels.Electric']){
				$return['data']['RichDetails.HeatingFuels'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingFuels.Gas'])){
			$return['data']['RichDetails.HeatingFuels.Gas'] = $data['RichDetails.HeatingFuels.Gas'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingFuels.Gas']){
				$return['data']['RichDetails.HeatingFuels'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingFuels.Oil'])){
			$return['data']['RichDetails.HeatingFuels.Oil'] = $data['RichDetails.HeatingFuels.Oil'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingFuels.Oil']){
				$return['data']['RichDetails.HeatingFuels'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingFuels.PropaneButane'])){
			$return['data']['RichDetails.HeatingFuels.PropaneButane'] = $data['RichDetails.HeatingFuels.PropaneButane'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingFuels.PropaneButane']){
				$return['data']['RichDetails.HeatingFuels'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingFuels.Solar'])){
			$return['data']['RichDetails.HeatingFuels.Solar'] = $data['RichDetails.HeatingFuels.Solar'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingFuels.Solar']){
				$return['data']['RichDetails.HeatingFuels'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingFuels.WoodPellet'])){
			$return['data']['RichDetails.HeatingFuels.WoodPellet'] = $data['RichDetails.HeatingFuels.WoodPellet'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingFuels.WoodPellet']){
				$return['data']['RichDetails.HeatingFuels'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingFuels.Other'])){
			$return['data']['RichDetails.HeatingFuels.Other'] = $data['RichDetails.HeatingFuels.Other'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingFuels.Other']){
				$return['data']['RichDetails.HeatingFuels'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingSystems.Baseboard'])){
			$return['data']['RichDetails.HeatingSystems.Baseboard'] = $data['RichDetails.HeatingSystems.Baseboard'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingSystems.Baseboard']){
				$return['data']['RichDetails.HeatingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingSystems.ForcedAir'])){
			$return['data']['RichDetails.HeatingSystems.ForcedAir'] = $data['RichDetails.HeatingSystems.ForcedAir'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingSystems.ForcedAir']){
				$return['data']['RichDetails.HeatingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingSystems.HeatPump'])){
			$return['data']['RichDetails.HeatingSystems.HeatPump'] = $data['RichDetails.HeatingSystems.HeatPump'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingSystems.HeatPump']){
				$return['data']['RichDetails.HeatingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingSystems.Radiant'])){
			$return['data']['RichDetails.HeatingSystems.Radiant'] = $data['RichDetails.HeatingSystems.Radiant'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingSystems.Radiant']){
				$return['data']['RichDetails.HeatingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingSystems.Stove'])){
			$return['data']['RichDetails.HeatingSystems.Stove'] = $data['RichDetails.HeatingSystems.Stove'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingSystems.Stove']){
				$return['data']['RichDetails.HeatingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingSystems.Wall'])){
			$return['data']['RichDetails.HeatingSystems.Wall'] = $data['RichDetails.HeatingSystems.Wall'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingSystems.Wall']){
				$return['data']['RichDetails.HeatingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.HeatingSystems.Other'])){
			$return['data']['RichDetails.HeatingSystems.Other'] = $data['RichDetails.HeatingSystems.Other'] ? 1 : 0;
			if($return['data']['RichDetails.HeatingSystems.Other']){
				$return['data']['RichDetails.HeatingSystems'] = true;
				$return['data']['RichDetails'] = true;
			}
		}

		if(isset($data['RichDetails.HottubSpa'])){
			$return['data']['RichDetails.HottubSpa'] = $data['RichDetails.HottubSpa'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Intercom'])){
			$return['data']['RichDetails.Intercom'] = $data['RichDetails.Intercom'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.JettedBathTub'])){
			$return['data']['RichDetails.JettedBathTub'] = $data['RichDetails.JettedBathTub'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Lawn'])){
			$return['data']['RichDetails.Lawn'] = $data['RichDetails.Lawn'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.LegalDescription'])){
			$return['data']['RichDetails.LegalDescription'] = $data['RichDetails.LegalDescription'];
			if(!empty($return['data']['RichDetails.LegalDescription'])){
				$return['data']['RichDetails.LegalDescription'] = str_replace($this->search, $this->replace, $return['data']['RichDetails.LegalDescription']);
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.MotherInLaw'])){
			$return['data']['RichDetails.MotherInLaw'] = $data['RichDetails.MotherInLaw'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.NumFloors'])){
			$return['data']['RichDetails.NumFloors'] = intval($data['RichDetails.NumFloors']);
			if(!empty($return['data']['RichDetails.NumFloors'])) $return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.NumParkingSpaces'])){
			$return['data']['RichDetails.NumParkingSpaces'] = intval($data['RichDetails.NumParkingSpaces']);
			if(!empty($return['data']['RichDetails.NumParkingSpaces'])) $return['data']['RichDetails'] = true;
		}

		if(isset($data['RichDetails.ParkingTypes.Carport'])){
			$return['data']['RichDetails.ParkingTypes.Carport'] = $data['RichDetails.ParkingTypes.Carport'] ? 1 : 0;
			if($return['data']['RichDetails.ParkingTypes.Carport']){
				$return['data']['RichDetails.ParkingTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ParkingTypes.GarageAttached'])){
			$return['data']['RichDetails.ParkingTypes.GarageAttached'] = $data['RichDetails.ParkingTypes.GarageAttached'] ? 1 : 0;
			if($return['data']['RichDetails.ParkingTypes.GarageAttached']){
				$return['data']['RichDetails.ParkingTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ParkingTypes.GarageDetached'])){
			$return['data']['RichDetails.ParkingTypes.GarageDetached'] = $data['RichDetails.ParkingTypes.GarageDetached'] ? 1 : 0;
			if($return['data']['RichDetails.ParkingTypes.GarageDetached']){
				$return['data']['RichDetails.ParkingTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ParkingTypes.OffStreet'])){
			$return['data']['RichDetails.ParkingTypes.OffStreet'] = $data['RichDetails.ParkingTypes.OffStreet'] ? 1 : 0;
			if($return['data']['RichDetails.ParkingTypes.OffStreet']){
				$return['data']['RichDetails.ParkingTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ParkingTypes.OnStreet'])){
			$return['data']['RichDetails.ParkingTypes.OnStreet'] = $data['RichDetails.ParkingTypes.OnStreet'] ? 1 : 0;
			if($return['data']['RichDetails.ParkingTypes.OnStreet']){
				$return['data']['RichDetails.ParkingTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ParkingTypes.None'])){
			$return['data']['RichDetails.ParkingTypes.None'] = $data['RichDetails.ParkingTypes.None'] ? 1 : 0;
			if($return['data']['RichDetails.ParkingTypes.None']){
				$return['data']['RichDetails.ParkingTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}

		if(isset($data['RichDetails.Patio'])){
			$return['data']['RichDetails.Patio'] = $data['RichDetails.Patio'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Pond'])){
			$return['data']['RichDetails.Pond'] = $data['RichDetails.Pond'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Pool'])){
			$return['data']['RichDetails.Pool'] = $data['RichDetails.Pool'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Porch'])){
			$return['data']['RichDetails.Porch'] = $data['RichDetails.Porch'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.RoofTypes.Asphalt'])){
			$return['data']['RichDetails.RoofTypes.Asphalt'] = $data['RichDetails.RoofTypes.Asphalt'] ? 1 : 0;
			if($return['data']['RichDetails.RoofTypes.Asphalt']){
				$return['data']['RichDetails.RoofTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.RoofTypes.BuiltUp'])){
			$return['data']['RichDetails.RoofTypes.BuiltUp'] = $data['RichDetails.RoofTypes.BuiltUp'] ? 1 : 0;
			if($return['data']['RichDetails.RoofTypes.BuiltUp']){
				$return['data']['RichDetails.RoofTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.RoofTypes.Composition'])){
			$return['data']['RichDetails.RoofTypes.Composition'] = $data['RichDetails.RoofTypes.Composition'] ? 1 : 0;
			if($return['data']['RichDetails.RoofTypes.Composition']){
				$return['data']['RichDetails.RoofTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.RoofTypes.Metal'])){
			$return['data']['RichDetails.RoofTypes.Metal'] = $data['RichDetails.RoofTypes.Metal'] ? 1 : 0;
			if($return['data']['RichDetails.RoofTypes.Metal']){
				$return['data']['RichDetails.RoofTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.RoofTypes.ShakeShingle'])){
			$return['data']['RichDetails.RoofTypes.ShakeShingle'] = $data['RichDetails.RoofTypes.ShakeShingle'] ? 1 : 0;
			if($return['data']['RichDetails.RoofTypes.ShakeShingle']){
				$return['data']['RichDetails.RoofTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.RoofTypes.Slate'])){
			$return['data']['RichDetails.RoofTypes.Slate'] = $data['RichDetails.RoofTypes.Slate'] ? 1 : 0;
			if($return['data']['RichDetails.RoofTypes.Slate']){
				$return['data']['RichDetails.RoofTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.RoofTypes.Tile'])){
			$return['data']['RichDetails.RoofTypes.Tile'] = $data['RichDetails.RoofTypes.Tile'] ? 1 : 0;
			if($return['data']['RichDetails.RoofTypes.Tile']){
				$return['data']['RichDetails.RoofTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.RoofTypes.Other'])){
			$return['data']['RichDetails.RoofTypes.Other'] = $data['RichDetails.RoofTypes.Other'] ? 1 : 0;
			if($return['data']['RichDetails.RoofTypes.Other']){
				$return['data']['RichDetails.RoofTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.RoomCount'])){
			$return['data']['RichDetails.RoomCount'] = intval($data['RichDetails.RoomCount']);
			if(!empty($return['data']['RichDetails.RoomCount'])) $return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Rooms.BreakfastNook'])){
			$return['data']['RichDetails.Rooms.BreakfastNook'] = $data['RichDetails.Rooms.BreakfastNook'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.BreakfastNook']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.DiningRoom'])){
			$return['data']['RichDetails.Rooms.DiningRoom'] = $data['RichDetails.Rooms.DiningRoom'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.DiningRoom']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.FamilyRoom'])){
			$return['data']['RichDetails.Rooms.FamilyRoom'] = $data['RichDetails.Rooms.FamilyRoom'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.FamilyRoom']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.LaundryRoom'])){
			$return['data']['RichDetails.Rooms.LaundryRoom'] = $data['RichDetails.Rooms.LaundryRoom'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.LaundryRoom']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.Library'])){
			$return['data']['RichDetails.Rooms.Library'] = $data['RichDetails.Rooms.Library'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.Library']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.MasterBath'])){
			$return['data']['RichDetails.Rooms.MasterBath'] = $data['RichDetails.Rooms.MasterBath'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.MasterBath']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.MudRoom'])){
			$return['data']['RichDetails.Rooms.MudRoom'] = $data['RichDetails.Rooms.MudRoom'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.MudRoom']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.Office'])){
			$return['data']['RichDetails.Rooms.Office'] = $data['RichDetails.Rooms.Office'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.Office']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.Pantry'])){
			$return['data']['RichDetails.Rooms.Pantry'] = $data['RichDetails.Rooms.Pantry'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.Pantry']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.RecreationRoom'])){
			$return['data']['RichDetails.Rooms.RecreationRoom'] = $data['RichDetails.Rooms.RecreationRoom'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.RecreationRoom']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.Workshop'])){
			$return['data']['RichDetails.Rooms.Workshop'] = $data['RichDetails.Rooms.Workshop'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.Workshop']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.SolariumAtrium'])){
			$return['data']['RichDetails.Rooms.SolariumAtrium'] = $data['RichDetails.Rooms.SolariumAtrium'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.SolariumAtrium']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.SunRoom'])){
			$return['data']['RichDetails.Rooms.SunRoom'] = $data['RichDetails.Rooms.SunRoom'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.SunRoom']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Rooms.WalkInCloset'])){
			$return['data']['RichDetails.Rooms.WalkInCloset'] = $data['RichDetails.Rooms.WalkInCloset'] ? 1 : 0;
			if($return['data']['RichDetails.Rooms.WalkInCloset']){
				$return['data']['RichDetails.Rooms'] = true;
				$return['data']['RichDetails'] = true;
			}
		}

		if(isset($data['RichDetails.RvParking'])){
			$return['data']['RichDetails.RvParking'] = $data['RichDetails.RvParking'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Sauna'])){
			$return['data']['RichDetails.Sauna'] = $data['RichDetails.Sauna'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.SecuritySystem'])){
			$return['data']['RichDetails.SecuritySystem'] = $data['RichDetails.SecuritySystem'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Skylight'])){
			$return['data']['RichDetails.Skylight'] = $data['RichDetails.Skylight'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.SportsCourt'])){
			$return['data']['RichDetails.SportsCourt'] = $data['RichDetails.SportsCourt'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.SprinklerSystem'])){
			$return['data']['RichDetails.SprinklerSystem'] = $data['RichDetails.SprinklerSystem'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.VaultedCeiling'])){
			$return['data']['RichDetails.VaultedCeiling'] = $data['RichDetails.VaultedCeiling'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.ViewTypes.None'])){
			$return['data']['RichDetails.ViewTypes.None'] = $data['RichDetails.ViewTypes.None'] ? 1 : 0;
			if($return['data']['RichDetails.ViewTypes.None']){
				$return['data']['RichDetails.ViewTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ViewTypes.City'])){
			$return['data']['RichDetails.ViewTypes.City'] = $data['RichDetails.ViewTypes.City'] ? 1 : 0;
			if($return['data']['RichDetails.ViewTypes.City']){
				$return['data']['RichDetails.ViewTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ViewTypes.Mountain'])){
			$return['data']['RichDetails.ViewTypes.Mountain'] = $data['RichDetails.ViewTypes.Mountain'] ? 1 : 0;
			if($return['data']['RichDetails.ViewTypes.Mountain']){
				$return['data']['RichDetails.ViewTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ViewTypes.Park'])){
			$return['data']['RichDetails.ViewTypes.Park'] = $data['RichDetails.ViewTypes.Park'] ? 1 : 0;
			if($return['data']['RichDetails.ViewTypes.Park']){
				$return['data']['RichDetails.ViewTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ViewTypes.Territorial'])){
			$return['data']['RichDetails.ViewTypes.Territorial'] = $data['RichDetails.ViewTypes.Territorial'] ? 1 : 0;
			if($return['data']['RichDetails.ViewTypes.Territorial']){
				$return['data']['RichDetails.ViewTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.ViewTypes.Water'])){
			$return['data']['RichDetails.ViewTypes.Water'] = $data['RichDetails.ViewTypes.Water'] ? 1 : 0;
			if($return['data']['RichDetails.ViewTypes.Water']){
				$return['data']['RichDetails.ViewTypes'] = true;
				$return['data']['RichDetails'] = true;
			}
		}

		if(isset($data['RichDetails.Waterfront'])){
			$return['data']['RichDetails.Waterfront'] = $data['RichDetails.Waterfront'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Wetbar'])){
			$return['data']['RichDetails.Wetbar'] = $data['RichDetails.Wetbar'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.WhatOwnerLoves'])){
			$return['data']['RichDetails.WhatOwnerLoves'] = $data['RichDetails.WhatOwnerLoves'];
			if(!empty($return['data']['RichDetails.WhatOwnerLoves'])){
				$return['data']['RichDetails.WhatOwnerLoves'] = str_replace($this->search, $this->replace, $return['data']['RichDetails.WhatOwnerLoves']);
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Wired'])){
			$return['data']['RichDetails.Wired'] = $data['RichDetails.Wired'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.YearUpdated'])){
			if(strlen($data['RichDetails.YearUpdated']) == 4 && intval($data['RichDetails.YearUpdated'])){
				$return['data']['RichDetails.YearUpdated'] = $data['RichDetails.YearUpdated'];
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.FitnessCenter'])){
			$return['data']['RichDetails.FitnessCenter'] = $data['RichDetails.FitnessCenter'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.BasketballCourt'])){
			$return['data']['RichDetails.BasketballCourt'] = $data['RichDetails.BasketballCourt'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.TennisCourt'])){
			$return['data']['RichDetails.TennisCourt'] = $data['RichDetails.TennisCourt'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.NearTransportation'])){
			$return['data']['RichDetails.NearTransportation'] = $data['RichDetails.NearTransportation'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.ControlledAccess'])){
			$return['data']['RichDetails.ControlledAccess'] = $data['RichDetails.ControlledAccess'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Over55ActiveCommunity'])){
			$return['data']['RichDetails.Over55ActiveCommunity'] = $data['RichDetails.Over55ActiveCommunity'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.AssistedLivingCommunity'])){
			$return['data']['RichDetails.AssistedLivingCommunity'] = $data['RichDetails.AssistedLivingCommunity'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Storage'])){
			$return['data']['RichDetails.Storage'] = $data['RichDetails.Storage'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.FencedYard'])){
			$return['data']['RichDetails.FencedYard'] = $data['RichDetails.FencedYard'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.PropertyName'])){
			$return['data']['RichDetails.PropertyName'] = $data['RichDetails.PropertyName'];
			if(!empty($return['data']['RichDetails.PropertyName'])){
				$return['data']['RichDetails.PropertyName'] = str_replace($this->search, $this->replace, $return['data']['RichDetails.PropertyName']);
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Furnished'])){
			$return['data']['RichDetails.Furnished'] = $data['RichDetails.Furnished'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.HighspeedInternet'])){
			$return['data']['RichDetails.HighspeedInternet'] = $data['RichDetails.HighspeedInternet'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.OnsiteLaundry'])){
			$return['data']['RichDetails.OnsiteLaundry'] = $data['RichDetails.OnsiteLaundry'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.CableSatTV'])){
			$return['data']['RichDetails.CableSatTV'] = $data['RichDetails.CableSatTV'] ? 'Yes' : 'No';
			$return['data']['RichDetails'] = true;
		}
		
		if(isset($data['RichDetails.Taxes.Amount'])){
			if(!empty($data['RichDetails.Taxes.Amount'])){
				$return['data']['RichDetails.Taxes.Amount'] = explode('|', $data['RichDetails.Taxes.Amount']);
				foreach($return['data']['RichDetails.Taxes.Amount'] as $key=>$value){
					$return['data']['RichDetails.Taxes'][$key]['Amount'] = floatval($value);
				}
				$return['data']['RichDetails'] = true;
			}
		}
		
		if(isset($data['RichDetails.Taxes.Year'])){
			if(!empty($data['RichDetails.Taxes.Year'])){
				$return['data']['RichDetails.Taxes.Year'] = explode('|', $data['RichDetails.Taxes.Year']);
				foreach($return['data']['RichDetails.Taxes.Year'] as $key=>$value){
					$return['data']['RichDetails.Taxes'][$key]['Year'] = floatval($value);
				}
				$return['data']['RichDetails'] = true;
			}
		}

		return $return;
	}
}

<?php 

if(!defined("BASEPATH")) exit("No direct script access allowed");

/**
 * SyndaFeed agents driver export model
 * 
 * @package PG_RealEstate
 * @subpackage Export
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Export_synda_agents_model extends Model{	
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 * 
	 * @var array
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 * 
	 * @var array
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Symbols for replace
	 * 
	 * @var array
	 */
	protected $license_types = array('r', 'a', 'f');
	
	/**
	 * Password types
	 * 
	 * @var array
	 */
	protected $password_types = array('mysql', 'plain');
	
	/**
	 * Constructor
	 * 
	 * @return Export_synda_agents_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Return default filename
	 * 
	 * @return string
	 */
	public function get_filename(){
		return "agents.xml";
	}

	/**
	 * Generate output file
	 * 
	 * @param string $filename output filename
	 * @param array $data export data
	 * @param array $settings driver settings
	 * @return void
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		
		fputs($f, '<?xml version="1.0" encoding="UTF-8">'."\n");
		fputs($f, '<Agents>'."\n");
		foreach($data as $i=>$row){
			if(!$i) continue;
			
			fputs($f, '	<Agent>'."\n");
			fputs($f, '		<PublisherId>'.$row['PublisherId'].'</PublisherId>');
			fputs($f, '		<AgentId>'.$row['AgentId'].'</AgentId>');
				
			if(isset($row['PersonalTitle']) && !empty($row['PersonalTitle']))
				fputs($f, '		<PersonalTitle><![CDATA['.$row['PersonalTitle'].']]></PersonalTitle>');

			fputs($f, '		<FirstName><![CDATA['.$row['FirstName'].']]></FirstName>');
			fputs($f, '		<LastName><![CDATA['.$row['LastName'].']]></LastName>');
			
			fputs($f, '		<LicenseType value="'.$row['LicenseType.value'].'">');
			fputs($f, '			<Jurisdiction><![CDATA['.$row['LicenseType.Jurisdiction'].']]></Jurisdiction>');
			fputs($f, '			<LicenseNumber><![CDATA['.$row['LicenseType.LicenseNumber'].']]></LicenseNumber>');
			fputs($f, '		</LicenseType>');
			
			fputs($f, '		<Email>'.$row['Email'].'</Email>');
			fputs($f, '		<Password type="'.$row['Password.type'].'">'.$row['Password'].'</Password>');
				
			if(isset($row['Telephone']) && !empty($row['Telephone']))
				fputs($f, '		<Telephone>'.$row['Telephone'].'</Telephone>');
				
			if(isset($row['Fax']) && !empty($row['Fax']))
				fputs($f, '		<Fax>'.$row['Fax'].'</Fax>');
					
			if(isset($row['PhotoURL']) && !empty($row['PhotoURL']))
				fputs($f, '		<PhotoURL>'.$row['PhotoURL'].'</PhotoURL>');
				
			if(isset($row['BrokerName']) && !empty($row['BrokerName']))
				fputs($f, '		<BrokerName>'.$row['BrokerName'].'</BrokerName>');
				
			if(isset($row['BrokerPhone']) && !empty($row['BrokerPhone']))
				fputs($f, '		<BrokerPhone>'.$row['BrokerPhone'].'</BrokerPhone>');
				
			if(isset($row['BrokerURL']) && !empty($row['BrokerURL']))
				fputs($f, '		<BrokerURL>'.$row['BrokerURL'].'</BrokerURL>');
				
			if(isset($row['BrokerMlsName']) && !empty($row['BrokerMlsName']))
				fputs($f, '		<BrokerMlsName>'.$row['BrokerMlsName'].'</BrokerMlsName>');
				
			if(isset($row['BrokerMlsCode']) && !empty($row['BrokerMlsCode']))
				fputs($f, '		<BrokerMlsCode>'.$row['BrokerMlsCode'].'</BrokerMlsCode>');
			
			fputs($f, '	</Agent>'."\n");
		}
		fputs($f, '</Agents>'."\n");
		
		fclose($f);
	}
	
	/**
	 * Validate export item data
	 * 
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());
		
		if(isset($data['PublisherId'])){
			$return['data']['PublisherId'] = intval($data['PublisherId']);
			if(empty($return['data']['PublisherId'])) $return["errors"][] = "PublisherId";
		}else{
			$return["errors"][] = "PublisherId";
		}
		
		if(isset($data['AgentId'])){
			$return['data']['AgentId'] = intval($data['AgentId']);
			if(empty($return['data']['AgentId'])) $return["errors"][] = "AgentId";
		}else{
			$return["errors"][] = "AgentId";
		}
		
		if(isset($data['PersonalTitle'])){
			$return['data']['PersonalTitle'] = $data['PersonalTitle'];
		}
		
		if(isset($data['FirstName'])){
			$return['data']['FirstName'] = $data['FirstName'];
			if(empty($return['data']['FirstName'])){
				$return["errors"][] = "FirstName";
			}
		}else{
			$return["errors"][] = "FirstName";
		}
		
		if(isset($data['LastName'])){
			$return['data']['LastName'] = $data['LastName'];
			if(empty($return['data']['LastName'])){
				$return["errors"][] = "LastName";
			}
		}else{
			$return["errors"][] = "LastName";
		}
		
		if(isset($data['LicenseType.value'])){
			$return['data']['LicenseType.value'] = in_array($data['LicenseType.value'], $this->license_types) ? $data['LicenseType.value'] : current($this->license_types);
		}	
		
		if(isset($data['LicenseType.Jurisdiction'])){
			$return['data']['LicenseType.Jurisdiction'] = $data['LicenseType.Jurisdiction'];
			if(empty($return['data']['LicenseType.Jurisdiction'])){
				$return["errors"][] = "LicenseType.Jurisdiction";
			}
		}else{
			$return["errors"][] = "LicenseType.Jurisdiction";
		}
		
		if(isset($data['LicenseType.LicenseNumber'])){
			$return['data']['LicenseType.LicenseNumber'] = $data['LicenseType.LicenseNumber'];
			if(empty($return['data']['LicenseType.LicenseNumber'])){
				$return["errors"][] = "LicenseType.LicenseNumber";
			}
		}else{
			$return["errors"][] = "LicenseType.LicenseNumber";
		}
			
		if(isset($data['Email'])){
			$return['data']['Email'] = $data['Email'];
		}
		
		if(isset($data['Password.type'])){
			$return['data']['Password.type'] = $data['Password.type'];
			if(!in_array($data['Password.type'], $this->password_types)){
				$return["errors"][] = "Password.type";
			}
		}else{
			$return["errors"][] = "Password.type";
		}
		
		if(isset($data['Password'])){
			$return['data']['Password'] = $data['Password'];
			if(empty($data['Password'])){
				$return["errors"][] = "Password";
			}
		}else{
			$return["errors"][] = "Password";
		}
		
		if(isset($data['Telephone'])){
			$return['data']['Telephone'] = $data['Telephone'];
		}		
		
		if(isset($data['Fax'])){
			$return['data']['Fax'] = $data['Fax'];
		}
		
		if(isset($data['PhotoURL'])){
			$return['data']['PhotoURL'] = $data['PhotoURL'];
		}		
		
		if(isset($data['BrokerName'])){
			$return['data']['BrokerName'] = $data['BrokerName'];
		}
					
		if(isset($data['BrokerPhone'])){
			$return['data']['BrokerPhone'] = $data['BrokerPhone'];
		}		
		
		if(isset($data['BrokerURL'])){
			$return['data']['BrokerURL'] = $data['BrokerURL'];
		}
		
		if(isset($data['BrokerMlsName'])){
			$return['data']['BrokerMlsName'] = $data['BrokerMlsName'];
		}		
		
		if(isset($data['BrokerMlsCode'])){
			$return['data']['BrokerMlsCode'] = $data['BrokerMlsCode'];
		}		
		
		return $return;
	}
}

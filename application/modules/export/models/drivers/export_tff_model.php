<?php 

if(!defined("BASEPATH")) exit("No direct script access allowed");

/**
 * Trulia driver export model
 * 
 * @package PG_RealEstate
 * @subpackage Export
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Export_tff_model extends Model{	
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 * 
	 * @var array
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 * 
	 * @var array
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Geocode types options
	 * 
	 * @var array
	 */
	protected $geocode_types = array('exact', 'offset', 'approximate');
	
	/**
	 * Property types options
	 * 
	 * @var array
	 */
	protected $property_types = array('Apartment/Condo/Townhouse', 'Condo', 'Townhouse', 'Coop',
									  'Apartment', 'Loft', 'TIC', 'Mobile/Manufactured', 'Farm/Ranch',
									  'Multi-Family', 'Income/Investment', 'Houseboat', 'Lot/Land',
									  'Single-Family Home');
									  
	/**
	 * Listing types options
	 * 
	 * @var array
	 */
	protected $listing_types = array('resale', 'foreclosure', 'new home', 'rental');
	
	/**
	 * Statuses options
	 * 
	 * @var array
	 */
	protected $statuses = array('for rent', 'for sale', 'pending', 'active contingent', 'sold', 'withdrawn');
	
	/**
	 * Foreclosure statuses options
	 * 
	 * @var array
	 */
	protected $foreclosure_statuses = array('Notice of Default (Pre-Foreclosure)', 'Lis Pendens (Pre-Foreclosure)',
											'Notice of Trustee Sale (Auction)', 'Notice of Foreclosure Sale (Auction)',
											'REO - Bank Owned');
	
	/**
	 * Price terms options
	 * 
	 * @var array
	 */
	protected $price_terms = array('night', 'week', 'month', 'year');
	
	/**
	 * Rental types options
	 * 
	 * @var array
	 */
	protected $rental_types = array('standard', 'corporate', 'senior', 'military', 'campus',
									'market rate apt', 'condominium', 'cooperative', 'assisted living',
									'subsidized', 'nursing home', 'student', 'vacation', 'other');
	
	/**
	 * Credit cards options
	 * 
	 * @var array
	 */
	protected $credit_cards = array('visa', 'mastercard', 'discover', 'american express', 'other');
	
	/**
	 * Lease types options
	 * 
	 * @var array
	 */
	protected $lease_types = array('sublet', 'month-to-month', 'annual', 'bi-annual');
	
	/**
	 * Days of the week options
	 * 
	 * @var array
	 */
	protected $days_of_the_week = array('sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat');
	
	/**
	 * Hoa-periods options
	 * 
	 * @var array
	 */
	protected $hoa_periods = array('monthly', 'annual');	
	
	/**
	 * Appliances options
	 * 
	 * @var array
	 */
	protected $range_types = array('gas', 'electric', 'other');	
	
	/**
	 * Fireplace types options
	 * 
	 * @var array
	 */
	protected $fireplace_types = array('gas', 'wood', 'electric', 'decorative');	
	
	/**
	 * Heating systems options
	 * 
	 * @var array
	 */
	protected $heating_systems = array('gas', 'electric', 'radiant', 'other');	
	
	/**
	 * Heating fuels options
	 * 
	 * @var array
	 */
	protected $heating_fuels = array('coal', 'oil', 'gas', 'electric', 'propane', 'butane', 'solar', 'woodpellet', 'other', 'none');	
	
	/**
	 * Garage types options
	 * 
	 * @var array
	 */
	protected $garage_types = array('attached', 'detached');	
	
	/**
	 * Parking types options
	 * 
	 * @var array
	 */
	protected $parking_types = array('surface lot', 'garage lot', 'covered lot', 'street', 'carport', 'none', 'other');	
	
	/**
	 * Parking space fees options
	 * 
	 * @var array
	 */
	protected $parking_space_fees = array('free', 'paid', 'both');	
	
	/**
	 * Constructor
	 * 
	 * @return Export_tff_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Return default filename
	 * 
	 * @return string
	 */
	public function get_filename(){
		return "export.xml";
	}
	
	/**
	 * Generate output file
	 * 
	 * @param string $filename output filename
	 * @param array $data export data
	 * @param array $settings driver settings
	 * @return void
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		fputs($f, '<?xml version="1.0" encoding="utf-8">'."\n");
		fputs($f, '<properties>'."\n");
		
		foreach($data as $i=>$row){
			if(!$i) continue;
			fputs($f, '	<property>'."\n");		
			fputs($f, '		<location>'."\n");
			
			if(isset($row['location.unit-number']) && !empty($row['location.unit-number']))
				fputs($f, '			<unit-number>'.$row['location.unit-number'].'</unit-number>'."\n");
			
			if(isset($row['location.street-address']) && !empty($row['location.street-address']))
				fputs($f, '			<street-address>'.$row['location.street-address'].'</street-address>'."\n");
			
			if(isset($row['location.city-name']) && !empty($row['location.city-name']))
				fputs($f, '			<city-name>'.$row['location.city-name'].'</city-name>'."\n");
			
			if(isset($row['location.zipcode']) && !empty($row['location.zipcode']))
				fputs($f, '			<zipcode>'.$row['zipcode'].'</zipcode>'."\n");
				
			if(isset($row['location.county']) && !empty($row['location.county']))
				fputs($f, '			<county>'.$row['location.county'].'</county>'."\n");
				
			if(isset($row['location.state-code']) && !empty($row['location.state-code']))	
				fputs($f, '			<state-code>'.$row['location.state-code'].'</state-code>'."\n");
				
			if(isset($row['location.street-intersection']) && !empty($row['location.street-intersection']))		
				fputs($f, '			<street-intersection>'.$row['location.street-intersection'].'</street-intersection>'."\n");
			
			if(isset($row['location.parcel-id']) && !empty($row['location.parcel-id']))		
				fputs($f, '			<parcel-id>'.$row['location.parcel-id'].'</parcel-id>'."\n");
				
			if(isset($row['location.building-name']) && !empty($row['location.building-name']))		
				fputs($f, '			<building-name>'.$row['location.building-name'].'</building-name>'."\n");
			
			if(isset($row['location.subdivision']) && !empty($row['location.subdivision']))		
				fputs($f, '			<subdivision>'.$row['location.subdivision'].'</subdivision>'."\n");
			
			if(isset($row['location.neighborhood-name']) && !empty($row['location.neighborhood-name']))		
				fputs($f, '			<neighborhood-name>'.$row['location.neighborhood-name'].'</neighborhood-name>'."\n");
			
			if(isset($row['location.neighborhood-description']) && !empty($row['location.neighborhood-description']))		
				fputs($f, '			<neighborhood-description>'.$row['location.neighborhood-description'].'</neighborhood-description>'."\n");
			
			if(isset($row['location.elevation']) && !empty($row['location.elevation']))		
				fputs($f, '			<elevation>'.$row['location.elevation'].'</elevation>'."\n");
			
			if(isset($row['location.longitude']) && !empty($row['location.longitude']))
				fputs($f, '			<longitude>'.$row['location.longitude'].'</longitude>'."\n");
			
			if(isset($row['location.latitude']) && !empty($row['location.latitude']))
				fputs($f, '			<latitude>'.$row['location.latitude'].'</latitude>'."\n");
			
			if(isset($row['location.geocode-type']) && !empty($row['location.geocode-type']))
				fputs($f, '			<geocode-type>'.$row['location.geocode-type'].'</geocode-type>'."\n");
				
			fputs($f, '			<display-address>'.$row['location.display-address'].'</display-address>'."\n");
			
			if(isset($row['location.directions']) && !empty($row['location.directions']))		
				fputs($f, '			<directions>'.$row['location.directions'].'</directions>'."\n");
			
			fputs($f, '		</location>'."\n");
			fputs($f, '		<details>'."\n");
			
			if(isset($row['details.listing-title']) && !empty($row['details.listing-title']))
				fputs($f, '			<listing-title>'.$row['details.listing-title'].'</listing-title>'."\n");
				
			fputs($f, '			<price>'.$row['details.price'].'</price>'."\n");
			
			if(isset($row['details.year-built']) && !empty($row['details.year-built']))
				fputs($f, '			<year-built>'.$row['details.year-built'].'</year-built>'."\n");
			
			if(isset($row['details.num-bedrooms']) && !empty($row['details.num-bedrooms']))
				fputs($f, '			<num-bedrooms>'.$row['details.num-bedrooms'].'</num-bedrooms>'."\n");
				
			if(isset($row['details.num-full-bathrooms']) && !empty($row['details.num-full-bathrooms']))
				fputs($f, '			<num-full-bathrooms>'.$row['details.num-full-bathrooms'].'</num-full-bathrooms>'."\n");
			
			if(isset($row['details.num-half-bathrooms']) && !empty($row['details.num-half-bathrooms']))
				fputs($f, '			<num-half-bathrooms>'.$row['details.num-half-bathrooms'].'</num-half-bathrooms>'."\n");
			
			if(isset($row['details.num-bathrooms']) && !empty($row['details.num-bathrooms']))
				fputs($f, '			<num-bathrooms>'.$row['details.num-bathrooms'].'</num-bathrooms>'."\n");
			
			if(isset($row['details.lot-size']) && !empty($row['details.lot-size']))
				fputs($f, '			<lot-size>'.$row['details.lot-size'].'</lot-size>'."\n");
			
			if(isset($row['details.living-area-square-feet']) && !empty($row['details.living-area-square-feet']))
				fputs($f, '			<living-area-square-feet>'.$row['details.living-area-square-feet'].'</living-area-square-feet>'."\n");
				
			if(isset($row['details.date-listed']) && !empty($row['details.date-listed']))
				fputs($f, '			<date-listed>'.$row['details.date-listed'].'</date-listed>'."\n");
			
			if(isset($row['details.date-available']) && !empty($row['details.date-available']))
				fputs($f, '			<date-available>'.$row['details.date-available'].'</date-available>'."\n");
			
			if(isset($row['details.date-sold']) && !empty($row['details.date-sold']))	
				fputs($f, '			<date-sold>'.$row['details.date-sold'].'</date-sold>'."\n");
			
			if(isset($row['details.sale-price']) && !empty($row['details.sale-price']))
				fputs($f, '			<sale-price>'.$row['details.sale-price'].'</sale-price>'."\n");
			
			if(isset($row['details.property-type']) && !empty($row['details.property-type']))
				fputs($f, '			<property-type>'.$row['details.property-type'].'</property-type>'."\n");
			
			if(isset($row['details.description']) && !empty($row['details.description']))
				fputs($f, '			<description>'.$row['details.description'].'</description>'."\n");
				
			if(isset($row['details.mlsId']) && !empty($row['details.mlsId']))	
				fputs($f, '			<mlsId>'.$row['details.mlsId'].'</mlsId>'."\n");
				
			if(isset($row['details.mlsName']) && !empty($row['details.mlsName']))	
				fputs($f, '			<mlsName>'.$row['details.mlsName'].'</mlsName>'."\n");
				
			fputs($f, '			<provider-listingid>'.$row['details.provider-listingid'].'</provider-listingid>'."\n");
			fputs($f, '		</details>'."\n");			
			
			if(isset($row['landing-page.lp-url']) && !empty($row['landing-page.lp-url'])){
				fputs($f, '		<landing-page>'."\n");
				fputs($f, '			<lp-url>'.$row['landing-page.lp-url'].'</lp-url>'."\n");
				fputs($f, '		</landing-page>'."\n");
			}
			
			if(isset($row['listing-type']) && !empty($row['listing-type']))
				fputs($f, '		<listing-type>'.$row['listing-type'].'</listing-type>'."\n");
			
			if(isset($row['status']) && in_array($row['status'], $this->statuses))
				fputs($f, '		<status>'.$row['status'].'</status>'."\n");
			
			if(isset($row['foreclosure-status']) && !empty($row['foreclosure-status']))
				fputs($f, '		<foreclosure-status>'.$row['foreclosure-status'].'</foreclosure-status>'."\n");
				
			fputs($f, '		<site>'."\n");
			fputs($f, '			<site-url>'.$row['site.site-url'].'</site-url>'."\n");
			
			if(isset($row['site.site-name']) && !empty($row['site.site-name']))
				fputs($f, '			<site-name>'.$row['site.site-name'].'</site-name>'."\n");
				
			fputs($f, '		</site>'."\n");			
			fputs($f, '		<rental-terms>'."\n");
			
			if(isset($row['rental-terms.price-term']) && !empty($row['rental-terms.price-term']))
				fputs($f, '			<price-term>'.$row['rental-terms.price-term'].'</price-term>'."\n");
			
			if(isset($row['rental-terms.rental-type']) && !empty($row['rental-terms.rental-type']))
				fputs($f, '			<rental-type>'.$row['rental-terms.rental-type'].'</rental-type>'."\n");
				
			if(isset($row['rental-terms.lease-type']) && !empty($row['rental-terms.lease-type']))
				fputs($f, '			<lease-type>'.$row['rental-terms.lease-type'].'</lease-type>'."\n");
			
			if(isset($row['rental-terms.lease-min-length-months']) && !empty($row['rental-terms.lease-min-length-months']))
				fputs($f, '			<lease-min-length-months>'.$row['rental-terms.lease-min-length-months'].'</lease-min-length-months>'."\n");
			
			if(isset($row['rental-terms.lease-max-length-months']) && !empty($row['rental-terms.lease-max-length-months']))
				fputs($f, '			<lease-max-length-months>'.$row['rental-terms.lease-max-length-months'].'</lease-max-length-months>'."\n");
			
			if(isset($row['rental-terms.lease-periods']) && !empty($row['rental-terms.lease-periods'])){
				fputs($f, '			<lease-periods>'."\n");
				
				foreach((array)$row['rental-terms.lease-periods'] as $lease_period)
					fputs($f, '				<lease-period>'.$lease_period.'</lease-period>'."\n");
					
				fputs($f, '			</lease-periods>'."\n");
			}
			
			if(isset($row['rental-terms.lease-details']) && !empty($row['rental-terms.lease-details']))
				fputs($f, '			<lease-details>'.$row['rental-terms.lease-details'].'</lease-details>'."\n");
			
			if(isset($row['rental-terms.security-deposit']) && !empty($row['rental-terms.security-deposit']))
				fputs($f, '			<security-deposit>'.$row['rental-terms.security-deposit'].'</security-deposit>'."\n");
			
			if(isset($row['rental-terms.security-deposit-description']) && !empty($row['rental-terms.security-deposit-description']))
				fputs($f, '			<security-deposit-description>'.$row['rental-terms.security-deposit-description'].'</security-deposit-description>'."\n");
			
			if(isset($row['rental-terms.application-fee']) && !empty($row['rental-terms.application-fee']))
				fputs($f, '			<application-fee>'.$row['rental-terms.application-fee'].'</application-fee>'."\n");
			
			if(isset($row['rental-terms.application-fee-description']) && !empty($row['rental-terms.application-fee-description']))
				fputs($f, '			<application-fee-description>'.$row['application-fee-description'].'</application-fee-description>'."\n");
				
			if(isset($row['rental-terms.credit-cards-accepted']))	
				fputs($f, '			<credit-cards-accepted>'.$row['rental-terms.credit-cards-accepted'].'</credit-cards-accepted>'."\n");

			if(isset($row['rental-terms.credit-cards']) && !empty($row['rental-terms.credit-cards'])){
				fputs($f, '			<credit-cards>'."\n");
				
				foreach($row['rental-terms.credit-cards'] as $credit_card)
					fputs($f, '				<credit-card>'.$credit_card.'</credit-card>'."\n");
				
				fputs($f, '			</credit-cards>'."\n");
			}
			
			fputs($f, '			<pets>'."\n");
			
			if(isset($row['rental-terms.pets.small-dogs-allowed']))	
				fputs($f, '				<small-dogs-allowed>'.$row['rental-terms.pets.small-dogs-allowed'].'</small-dogs-allowed>'."\n");
			
			if(isset($row['rental-terms.pets.large-dogs-allowed']))
				fputs($f, '				<large-dogs-allowed>'.$row['rental-terms.pets.large-dogs-allowed'].'</large-dogs-allowed>'."\n");
			
			if(isset($row['rental-terms.pets.cats-allowed']))
				fputs($f, '				<cats-allowed>'.$row['rental-terms.pets.cats-allowed'].'</cats-allowed>'."\n");
		
			if(isset($row['rental-terms.pets.pet-other-allowed']))
				fputs($f, '				<pet-other-allowed>'.$row['rental-terms.pets.pet-other-allowed'].'</pet-other-allowed>'."\n");
			
			if(isset($row['rental-terms.pets.max-pets']) && !empty($row['rental-terms.pets.max-pets']))
				fputs($f, '				<max-pets>'.$row['rental-terms.pets.max-pets'].'</max-pets>'."\n");
				
			if(isset($row['rental-terms.pets.pet-deposit']) && !empty($row['rental-terms.pets.pet-deposit']))
				fputs($f, '				<pet-deposit>'.$row['rental-terms.pets.pet-deposit'].'</pet-deposit>'."\n");
			
			if(isset($row['rental-terms.pets.pet-fee']) && !empty($row['rental-terms.pets.pet-fee']))
				fputs($f, '				<pet-fee>'.$row['rental-terms.pets.pet-fee'].'</pet-fee>'."\n");
			
			if(isset($row['rental-terms.pets.pet-rent']) && !empty($row['rental-terms.pets.pet-rent']))
				fputs($f, '				<pet-rent>'.$row['rental-terms.pets.pet-rent'].'</pet-rent>'."\n");
			
			if(isset($row['rental-terms.pets.pet-weight']) && !empty($row['rental-terms.pets.pet-weight']))
				fputs($f, '				<pet-weight>'.$row['rental-terms.pets.pet-weight'].'</pet-weight>'."\n");
			
			if(isset($row['rental-terms.pets.pet-comments']) && !empty($row['rental-terms.pets.pet-comments'])){
				fputs($f, '				<pet-comments>'."\n");
				
				foreach($row['rental-terms.pets.pet-comments'] as $pet_comment)
					fputs($f, '					<pet-comment>'.$pet_comment.'</pet-comment>'."\n");
					
				fputs($f, '				</pet-comments>'."\n");
			}
			
			fputs($f, '			</pets>'."\n");
			
			fputs($f, '			<utilities-included>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-aircon']))
				fputs($f, '				<landlord-pays-aircon>'.$row['rental-terms.utilities-included.landlord-pays-aircon'].'</landlord-pays-aircon>'."\n");

			if(isset($row['rental-terms.utilities-included.landlord-pays-broadbandinternet']))
				fputs($f, '				<landlord-pays-broadbandinternet>'.$row['rental-terms.utilities-included.landlord-pays-broadbandinternet'].'</landlord-pays-broadbandinternet>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-cable']))
				fputs($f, '				<landlord-pays-cable>'.$row['rental-terms.utilities-included.landlord-pays-cable'].'</landlord-pays-cable>'."\n");
				
			if(isset($row['rental-terms.utilities-included.landlord-pays-electric']))	
				fputs($f, '				<landlord-pays-electric>'.$row['rental-terms.utilities-included.landlord-pays-electric'].'</landlord-pays-electric>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-gas']))	
				fputs($f, '				<landlord-pays-gas>'.$row['rental-terms.utilities-included.landlord-pays-gas'].'</landlord-pays-gas>'."\n");

			if(isset($row['rental-terms.utilities-included.landlord-pays-heat']))	
				fputs($f, '				<landlord-pays-heat>'.$row['rental-terms.utilities-included.landlord-pays-heat'].'</landlord-pays-heat>'."\n");
				
			if(isset($row['rental-terms.utilities-included.landlord-pays-hotwater']))
				fputs($f, '				<landlord-pays-hotwater>'.$row['rental-terms.utilities-included.landlord-pays-hotwater'].'</landlord-pays-hotwater>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-satellite']))
				fputs($f, '				<landlord-pays-satellite>'.$row['rental-terms.utilities-included.landlord-pays-satellite'].'</landlord-pays-satellite>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-sewer']))
				fputs($f, '				<landlord-pays-sewer>'.$row['rental-terms.utilities-included.landlord-pays-sewer'].'</landlord-pays-sewer>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-telephone']))
				fputs($f, '				<landlord-pays-telephone>'.$row['rental-terms.utilities-included.landlord-pays-telephone'].'</landlord-pays-telephone>'."\n");
			
			if(isset($row['rental-terms.utilities-included.landlord-pays-trash']))
				fputs($f, '				<landlord-pays-trash>'.$row['rental-terms.utilities-included.landlord-pays-trash'].'</landlord-pays-trash>'."\n");
				
			if(isset($row['rental-terms.utilities-included.landlord-pays-water']))
				fputs($f, '				<landlord-pays-water>'.$row['rental-terms.utilities-included.landlord-pays-water'].'</landlord-pays-water>'."\n");
				
			if(isset($row['rental-terms.utilities-included.landlord-utilities-portion-included']) && !empty($row['rental-terms.utilities-included.landlord-utilities-portion-included']))
				fputs($f, '				<landlord-utilities-portion-included>'.$row['rental-terms.utilities-included.landlord-utilities-portion-included'].'</landlord-utilities-portion-included>'."\n");

			if(isset($row['rental-terms.utilities-included.utilities-comments']) && !empty($row['rental-terms.utilities-included.utilities-comments'])){
				fputs($f, '				<utilities-comments>'."\n");
				
				foreach((array)$row['rental-terms.utilities-included.utilities-comments'] as $utilities_comment)
					fputs($f, '					<utilities-comment>'.$utilities_comment.'</utilities-comment>'."\n");
				
				fputs($f, '				</utilities-comments>'."\n");
			}
			
			fputs($f, '			</utilities-included>'."\n");
			
			if(isset($row['rental-terms.property-manager-on-site']))
				fputs($f, '			<property-manager-on-site>'.($row['rental-terms.property-manager-on-site'] ? 'yes' : 'no').'</property-manager-on-site>'."\n");
			
			if(isset($row['rental-terms.rent-control']))
				fputs($f, '			<rent-control>'.($row['rental-terms.rent-control'] ? 'yes' : 'no').'</rent-control>'."\n");
			
			if(isset($row['rental-terms.subletting-allowed']))
				fputs($f, '			<subletting-allowed>'.($row['rental-terms.subletting-allowed'] ? 'yes' : 'no').'</subletting-allowed>'."\n");
				
			fputs($f, '		</rental-terms>'."\n");
			
			if(isset($row['pictures']) && !empty($row['pictures'])){
				fputs($f, '		<pictures>'."\n");
				
				foreach($row['pictures'] as $picture){
					fputs($f, '			<picture>'."\n");
					fputs($f, '				<picture-url>'.$picture['picture-url'].'</picture-url>'."\n");
					fputs($f, '				<picture-caption>'.$picture['picture-caption'].'</picture-caption>'."\n");
					fputs($f, '				<picture-description>'.$picture['picture-description'].'</picture-description>'."\n");
					fputs($f, '				<picture-seq-number>'.$picture['picture-seq-number'].'</picture-seq-number>'."\n");
					fputs($f, '			</picture>'."\n");
				}
				
				fputs($f, '		</pictures>'."\n");
			}
			
			if(isset($row['virtual-tours']) && !empty($row['virtual-tours'])){
				fputs($f, '		<virtual-tours>'."\n");
				
				foreach($row['virtual-tours'] as $virtual_tour){
					fputs($f, '			<virtual-tour>'."\n");
					fputs($f, '				<virtual-tour-url>'.$virtual_tour['virtual-tour-url'].'</virtual-tour-url>'."\n");
					fputs($f, '				<virtual-tour-caption>'.$virtual_tour['virtual-tour-caption'].'</virtual-tour-caption>'."\n");
					fputs($f, '				<virtual-tour-description>'.$virtual_tour['virtual-tour-description'].'</virtual-tour-description>'."\n");
					fputs($f, '				<virtual-tour-seq-number>'.$virtual_tour['virtual-tour-seq-number'].'</virtual-tour-seq-number>'."\n");
					fputs($f, '			</virtual-tour>'."\n");
				}
				
				fputs($f, '		</virtual-tours>'."\n");
			}
			
			if(isset($row['videos']) && !empty($row['videos'])){
				fputs($f, '		<videos>'."\n");
				
				foreach($row['videos'] as $video){
					fputs($f, '			<video>'."\n");
					fputs($f, '				<video-url>'.$video['video-url'].'</video-url>'."\n");
					fputs($f, '				<video-caption>'.$video['video-caption'].'</video-caption>'."\n");
					fputs($f, '				<video-description>'.$video['video-description'].'</video-description>'."\n");
					fputs($f, '				<video-seq-number>'.$video['video-seq-number'].'</video-seq-number>'."\n");
					fputs($f, '			</video>'."\n");
				}
				
				fputs($f, '		</videos>'."\n");
			}	
				
			if(isset($row['floorplan-layouts']) && !empty($row['floorplan-layouts'])){
				fputs($f, '		<floorplan-layouts>'."\n");
				
				foreach($row['floorplan-layouts'] as $floorplan_layout){
					fputs($f, '			<floorplan-layout>'."\n");
					fputs($f, '				<floorplan-layout-url>'.$floorplan_layout['floorplan-layout-url'].'</floorplan-layout-url>'."\n");
					fputs($f, '				<floorplan-layout-caption>'.$floorplan_layout['floorplan-layout-caption'].'</floorplan-layout-caption>'."\n");
					fputs($f, '				<floorplan-layout-description>'.$floorplan_layout['floorplan-layout-description'].'</floorplan-layout-description>'."\n");
					fputs($f, '				<floorplan-layout-seq-number>'.$floorplan_layout['floorplan-layout-seq-number'].'</floorplan-layout-seq-number>'."\n");
					fputs($f, '			</floorplan-layout>'."\n");
				}
				
				fputs($f, '		</floorplan-layouts>'."\n");
			}
			
			fputs($f, '		<agent>'."\n");
			
			if(isset($row['agent.agent-name']) && !empty($row['agent.agent-name']))
				fputs($f, '			<agent-name>'.$row['agent.agent-name'].'</agent-name>'."\n");
			
			if(isset($row['agent.agent-phone']) && !empty($row['agent.agent-phone']))
				fputs($f, '			<agent-phone>'.$row['agent.agent-phone'].'</agent-phone>'."\n");
			
			if(isset($row['agent.agent-email']) && !empty($row['agent.agent-email']))
				fputs($f, '			<agent-email>'.$row['agent.agent-email'].'</agent-email>'."\n");
				
			if(isset($row['agent.agent-alternate-email']) && !empty($row['agent.agent-alternate-email']))	
				fputs($f, '			<agent-alternate-email>'.$row['agent.agent-alternate-email'].'</agent-alternate-email>'."\n");
			
			if(isset($row['agent.agent-picture-url']) && !empty($row['agent.agent-picture-url']))	
				fputs($f, '			<agent-picture-url>'.$row['agent.agent-picture-url'].'</agent-picture-url>'."\n");
			
			if(isset($row['agent.agent-id']) && !empty($row['agent.agent-id']))	
				fputs($f, '			<agent-id>'.$row['agent.agent-id'].'</agent-id>'."\n");
				
			fputs($f, '		</agent>'."\n");
			fputs($f, '		<brokerage>'."\n");
			
			if(isset($row['brokerage.brokerage-name']) && !empty($row['brokerage.brokerage-name']))	
				fputs($f, '			<brokerage-name>'.$row['brokerage.brokerage-name'].'</brokerage-name>'."\n");
			
			if(isset($row['brokerage.brokerage-broker-name']) && !empty($row['brokerage.brokerage-broker-name']))	
				fputs($f, '			<brokerage-broker-name>'.$row['brokerage.brokerage-broker-name'].'</brokerage-broker-name>'."\n");
			
			if(isset($row['brokerage.brokerage-id']) && !empty($row['brokerage.brokerage-id']))	
				fputs($f, '			<brokerage-id>'.$row['brokerage.brokerage-id'].'</brokerage-id>'."\n");
			
			if(isset($row['brokerage.brokerage-mls-code']) && !empty($row['brokerage.brokerage-mls-code']))	
				fputs($f, '			<brokerage-mls-code>'.$row['brokerage.brokerage-mls-code'].'</brokerage-mls-code>'."\n");
			
			if(isset($row['brokerage.brokerage-phone']) && !empty($row['brokerage.brokerage-phone']))
				fputs($f, '			<brokerage-phone>'.$row['brokerage.brokerage-phone'].'</brokerage-phone>'."\n");
			
			if(isset($row['brokerage.brokerage-email']) && !empty($row['brokerage.brokerage-email']))
				fputs($f, '			<brokerage-email>'.$row['brokerage.brokerage-email'].'</brokerage-email>'."\n");
			
			if(isset($row['brokerage.brokerage-website']) && !empty($row['brokerage.brokerage-website']))
				fputs($f, '			<brokerage-website>'.$row['brokerage.brokerage-website'].'</brokerage-website>'."\n");
			
			if(isset($row['brokerage.brokerage-logo-url']) && !empty($row['brokerage.brokerage-logo-url']))
				fputs($f, '			<brokerage-logo-url>'.$row['brokerage.brokerage-logo-url'].'</brokerage-logo-url>'."\n");
				
			fputs($f, '			<brokerage-address>'."\n");
			
			if(isset($row['brokerage.brokerage-address.brokerage-street-address']) && !empty($row['brokerage.brokerage-address.brokerage-street-address']))
				fputs($f, '				<brokerage-street-address>'.$row['brokerage.brokerage-address.brokerage-street-address'].'</brokerage-street-address>'."\n");
			
			if(isset($row['brokerage.brokerage-address.brokerage-city-name']) && !empty($row['brokerage.brokerage-address.brokerage-city-name']))
				fputs($f, '				<brokerage-city-name>'.$row['brokerage.brokerage-address.brokerage-city-name'].'</brokerage-city-name>'."\n");
			
			if(isset($row['brokerage.brokerage-address.brokerage-zipcode']) && !empty($row['brokerage.brokerage-address.brokerage-zipcode']))
				fputs($f, '				<brokerage-zipcode>'.$row['brokerage.brokerage-address.brokerage-zipcode'].'</brokerage-zipcode>'."\n");
				
			if(isset($row['brokerage.brokerage-address.brokerage-state-code']) && !empty($row['brokerage.brokerage-address.brokerage-state-code']))	
				fputs($f, '				<brokerage-state-code>'.$row['brokerage.brokerage-address.brokerage-state-code'].'</brokerage-state-code>'."\n");
			
			fputs($f, '			</brokerage-address>'."\n");
			fputs($f, '		</brokerage>'."\n");
			fputs($f, '		<office>'."\n");
			
			if(isset($row['office.office-name']) && !empty($row['office.office-name']))
				fputs($f, '			<office-name>'.$row['office.office-name'].'</office-name>'."\n");
				
			if(isset($row['office.office-id']) && !empty($row['office.office-id']))
				fputs($f, '			<office-id>'.$row['office.office-id'].'</office-id>'."\n");
				
			if(isset($row['office.office-mls-code']) && !empty($row['office.office-mls-code']))	
				fputs($f, '			<office-mls-code>'.$row['office.office-mls-code'].'</office-mls-code>'."\n");
			
			if(isset($row['office.office-broker-id']) && !empty($row['office.office-broker-id']))	
				fputs($f, '			<office-broker-id>'.$row['office.office-broker-id'].'</office-broker-id>'."\n");
			
			if(isset($row['office.office-phone']) && !empty($row['office.office-phone']))	
				fputs($f, '			<office-phone>'.$row['office.office-phone'].'</office-phone>'."\n");
			
			if(isset($row['office.office-email']) && !empty($row['office.office-email']))	
				fputs($f, '			<office-email>'.$row['office.office-email'].'</office-email>'."\n");
			
			if(isset($row['office.office-website']) && !empty($row['office.office-website']))	
				fputs($f, '			<office-website>'.$row['office.office-website'].'</office-website>'."\n");
				
			fputs($f, '		</office>'."\n");
			fputs($f, '		<franchise>'."\n");
			
			if(isset($row['franchise.franchise-name']) && !empty($row['franchise.franchise-name']))	
				fputs($f, '			<franchise-name>'.$row['franchise.franchise-name'].'</franchise-name>'."\n");
			
			if(isset($row['franchise.franchise-phone']) && !empty($row['franchise.franchise-phone']))	
				fputs($f, '			<franchise-phone>'.$row['franchise.franchise-phone'].'</franchise-phone>'."\n");
			
			if(isset($row['franchise.franchise-email']) && !empty($row['franchise.franchise-email']))	
				fputs($f, '			<franchise-email>'.$row['franchise.franchise-email'].'</franchise-email>'."\n");
			
			if(isset($row['franchise.franchise-website']) && !empty($row['franchise.franchise-website']))	
				fputs($f, '			<franchise-website>'.$row['franchise.franchise-website'].'</franchise-website>'."\n");
			
			if(isset($row['franchise.franchise-logo-url']) && !empty($row['franchise.franchise-logo-url']))	
				fputs($f, '			<franchise-logo-url>'.$row['franchise.franchise-logo-url'].'</franchise-logo-url>'."\n");
				
			fputs($f, '		</franchise>'."\n");
			fputs($f, '		<builder>'."\n");
			
			if(isset($row['builder.builder-id']) && !empty($row['builder.builder-id']))	
				fputs($f, '			<builder-id>'.$row['builder.builder-id'].'</builder-id>'."\n");
			
			if(isset($row['builder.builder-name']) && !empty($row['builder.builder-name']))	
				fputs($f, '			<builder-name>'.$row['builder.builder-name'].'</builder-name>'."\n");
			
			if(isset($row['builder.builder-phone']) && !empty($row['builder.builder-phone']))	
				fputs($f, '			<builder-phone>'.$row['builder.builder-phone'].'</builder-phone>'."\n");
			
			if(isset($row['builder.builder-email']) && !empty($row['builder.builder-email']))	
				fputs($f, '			<builder-email>'.$row['builder.builder-email'].'</builder-email>'."\n");
				
			if(isset($row['builder.builder-lead-email']) && !empty($row['builder.builder-lead-email']))		
				fputs($f, '			<builder-lead-email>'.$row['builder.builder-lead-email'].'</builder-lead-email>'."\n");
			
			if(isset($row['builder.builder-website']) && !empty($row['builder.builder-website']))	
				fputs($f, '			<builder-website>'.$row['builder.builder-website'].'</builder-website>'."\n");
				
			if(isset($row['builder.builder-logo-url']) && !empty($row['builder.builder-logo-url']))		
				fputs($f, '			<builder-logo-url>'.$row['builder.builder-logo-url'].'</builder-logo-url>'."\n");
				
			fputs($f, '			<builder-address>'."\n");
			
			if(isset($row['builder.builder-address.builder-street-address']) && !empty($row['builder.builder-address.builder-street-address']))	
				fputs($f, '				<builder-street-address>'.$row['builder.builder-address.builder-street-address'].'</builder-street-address>'."\n");
			
			if(isset($row['builder.builder-address.builder-city-name']) && !empty($row['builder.builder-address.builder-city-name']))	
				fputs($f, '				<builder-city-name>'.$row['builder.builder-address.builder-city-name'].'</builder-city-name>'."\n");
			
			if(isset($row['builder.builder-address.builder-zipcode']) && !empty($row['builder.builder-address.builder-zipcode']))	
				fputs($f, '				<builder-zipcode>'.$row['builder.builder-address.builder-zipcode'].'</builder-zipcode>'."\n");
				
			if(isset($row['builder.builder-address.builder-state-code']) && !empty($row['builder.builder-address.builder-state-code']))	
				fputs($f, '				<builder-state-code>'.$row['builder.builder-address.builder-state-code'].'</builder-state-code>'."\n");
			
			fputs($f, '			</builder-address>'."\n");
			fputs($f, '		</builder>'."\n");
			fputs($f, '		<property-manager>'."\n");
			
			if(isset($row['property-manager.property-manager-name']) && !empty($row['property-manager.property-manager-name']))		
				fputs($f, '			<property-manager-name>'.$row['property-manager.property-manager-name'].'</property-manager-name>'."\n");
			
			if(isset($row['property-manager.property-management-company-name']) && !empty($row['property-manager.property-management-company-name']))		
				fputs($f, '			<property-management-company-name>'.$row['property-manager.property-management-company-name'].'</property-management-company-name>'."\n");
			
			if(isset($row['property-manager.property-manager-office-hours']) && !empty($row['property-manager.property-manager-office-hours'])){
				fputs($f, '			<property-manager-office-hours>'."\n");
				
				foreach($row['property-manager.property-manager-office-hours'] as $property_manager_office_hours){
					fputs($f, '			<office-day>'."\n");
					fputs($f, '				<day-of-the-week>'.$property_manager_office_hours['day-of-the-week'].'</day-of-the-week>'."\n");
					fputs($f, '				<office-start-time>'.$property_manager_office_hours['office-start-time'].'</office-start-time>'."\n");
					fputs($f, '				<office-end-time>'.$property_manager_office_hours['office-end-time'].'</office-end-time>'."\n");
					fputs($f, '				<comment>'.$property_manager_office_hours['comment'].'</comment>'."\n");
						
					fputs($f, '			</office-day>'."\n");
				}
				
				fputs($f, '			</property-manager-office-hours>'."\n");
			}

			if(isset($row['property-manager.property-manager-phone']) && !empty($row['property-manager.property-manager-phone']))		
				fputs($f, '			<property-manager-phone>'.$row['property-manager.property-manager-phone'].'</property-manager-phone>'."\n");
			
			if(isset($row['property-manager.property-manager-email']) && !empty($row['property-manager.property-manager-email']))		
				fputs($f, '			<property-manager-email>'.$row['property-manager.property-manager-email'].'</property-manager-email>'."\n");
			
			if(isset($row['property-manager.property-manager-lead-email']) && !empty($row['property-manager.property-manager-lead-email']))		
				fputs($f, '			<property-manager-lead-email>'.$row['property-manager.property-manager-lead-email'].'</property-manager-lead-email>'."\n");
			
			if(isset($row['property-manager.property-manager-website']) && !empty($row['property-manager.property-manager-website']))		
				fputs($f, '			<property-manager-website>'.$row['property-manager.property-manager-website'].'</property-manager-website>'."\n");
			
			if(isset($row['property-manager.property-manager-logo-url']) && !empty($row['property-manager.property-manager-logo-url']))		
				fputs($f, '			<property-manager-logo-url>'.$row['property-manager.property-manager-logo-url'].'</property-manager-logo-url>'."\n");
				
			fputs($f, '		</property-manager>'."\n");
			fputs($f, '		<community>'."\n");
			
			if(isset($row['community.community-building']) && !empty($row['community.community-building']))		
				fputs($f, '			<community-building>'.$row['community.community-building'].'</community-building>'."\n");
				
			if(isset($row['community.community-id']) && !empty($row['community.community-id']))			
				fputs($f, '			<community-id>'.$row['community.community-id'].'</community-id>'."\n");
				
			if(isset($row['community.community-name']) && !empty($row['community.community-name']))				
				fputs($f, '			<community-name>'.$row['community.community-name'].'</community-name>'."\n");
			
			if(isset($row['community.community-phone']) && !empty($row['community.community-phone']))				
				fputs($f, '			<community-phone>'.$row['community.community-phone'].'</community-phone>'."\n");
			
			if(isset($row['community.community-email']) && !empty($row['community.community-email']))				
				fputs($f, '			<community-email>'.$row['community.community-email'].'</community-email>'."\n");
			
			if(isset($row['community.community-fax']) && !empty($row['community.community-fax']))				
				fputs($f, '			<community-fax>'.$row['community.community-fax'].'</community-fax>'."\n");
				
			if(isset($row['community.community-description']) && !empty($row['community.community-description']))					
				fputs($f, '			<community-description>'.$row['community.community-description'].'</community-description>'."\n");
			
			if(isset($row['community.community-website']) && !empty($row['community.community-website']))					
				fputs($f, '			<community-website>'.$row['community.community-website'].'</community-website>'."\n");
				
			fputs($f, '			<community-address>'."\n");
			
			if(isset($row['community.community-address.community-street-address']) && !empty($row['community.community-address.community-street-address']))					
				fputs($f, '				<community-street-address>'.$row['community.community-address.community-street-address'].'</community-street-address>'."\n");

			if(isset($row['community.community-address.community-city-name']) && !empty($row['community.community-address.community-city-name']))					
				fputs($f, '				<community-city-name>'.$row['community.community-address.community-city-name'].'</community-city-name>'."\n");
				
			if(isset($row['community.community-address.community-zipcode']) && !empty($row['community.community-address.community-zipcode']))
				fputs($f, '				<community-zipcode>'.$row['community.community-address.community-zipcode'].'</community-zipcode>'."\n");
			
			if(isset($row['community.community-address.community-state-code']) && !empty($row['community.community-address.community-state-code']))
				fputs($f, '				<community-state-code>'.$row['community.community-address.community-state-code'].'</community-state-code>'."\n");
				
			fputs($f, '			</community-address>'."\n");
			
			if(isset($row['community.building-amenities']) && !empty($row['community.building-amenities'])){
				fputs($f, '			<building-amenities>'."\n");
				
				foreach($row['community.building-amenities'] as $building_amenity)
					fputs($f, '				<building-amenity>'.$building_amenity.'</building-amenity>'."\n");
					
				fputs($f, '			</building-amenities>'."\n");
			}
			
			if(isset($row['community.community-amenities']) && !empty($row['community.community-amenities'])){
				fputs($f, '			<community-amenities>'."\n");
				
				foreach($row['community.community-amenities'] as $community_amenity)
					fputs($f, '				<community-amenity>'.$community_amenity.'</community-amenity>'."\n");

				fputs($f, '			</community-amenities>'."\n");
			}

			fputs($f, '		</community>'."\n");

			fputs($f, '		<plan>'."\n");
			
			if(isset($row['plan.plan-id']) && !empty($row['plan.plan-id']))
				fputs($f, '			<plan-id>'.$row['plan.plan-id'].'</plan-id>'."\n");
				
			if(isset($row['plan.plan-name']) && !empty($row['plan.plan-name']))
				fputs($f, '			<plan-name>'.$row['plan.plan-name'].'</plan-name>'."\n");
			
			if(isset($row['plan.plan-type']) && !empty($row['plan.plan-type']))
				fputs($f, '			<plan-type>'.$row['plan.plan-type'].'</plan-type>'."\n");
			
			if(isset($row['plan.plan-base-price']) && !empty($row['plan.plan-base-price']))
				fputs($f, '			<plan-base-price>'.$row['plan.plan-base-price'].'</plan-base-price>'."\n");
				
			fputs($f, '		</plan>'."\n");
			fputs($f, '		<spec>'."\n");
			
			if(isset($row['spec.is-spec-home']) && !empty($row['spec.is-spec-home']))
				fputs($f, '			<is-spec-home>'.$row['spec.is-spec-home'].'</is-spec-home>'."\n");
			
			if(isset($row['spec.spec-id']) && !empty($row['spec.spec-id']))
				fputs($f, '			<spec-id>'.$row['spec.spec-id'].'</spec-id>'."\n");
				
			fputs($f, '		</spec>'."\n");
			fputs($f, '		<open-home>'."\n");
			
			if(isset($row['open-home.period1-date']) && !empty($row['open-home.period1-date']))
				fputs($f, '			<period1-date>'.$row['open-home.period1-date'].'</period1-date>'."\n");
			
			if(isset($row['open-home.period1-start-time']) && !empty($row['open-home.period1-start-time']))
				fputs($f, '			<period1-start-time>'.$row['open-home.period1-start-time'].'</period1-start-time>'."\n");
				
			if(isset($row['open-home.period1-end-time']) && !empty($row['open-home.period1-end-time']))	
				fputs($f, '			<period1-end-time>'.$row['open-home.period1-end-time'].'</period1-end-time>'."\n");
			
			if(isset($row['open-home.period1-details']) && !empty($row['open-home.period1-details']))	
				fputs($f, '			<period1-details>'.$row['open-home.period1-details'].'</period1-details>'."\n");
			
			if(isset($row['open-home.period2-date']) && !empty($row['open-home.period2-date']))
				fputs($f, '			<period2-date>'.$row['open-home.period2-date'].'</period2-date>'."\n");
				
			if(isset($row['open-home.period2-start-time']) && !empty($row['open-home.period2-start-time']))
				fputs($f, '			<period2-start-time>'.$row['open-home.period2-start-time'].'</period2-start-time>'."\n");
			
			if(isset($row['open-home.period2-end-time']) && !empty($row['open-home.period2-end-time']))	
				fputs($f, '			<period2-end-time>'.$row['open-home.period2-end-time'].'</period2-end-time>'."\n");
				
			if(isset($row['open-home.period2-details']) && !empty($row['open-home.period2-details']))		
				fputs($f, '			<period2-details>'.$row['open-home.period2-details'].'</period2-details>'."\n");
				
			if(isset($row['open-home.period3-date']) && !empty($row['open-home.period3-date']))	
				fputs($f, '			<period3-date>'.$row['open-home.period3-date'].'</period3-date>'."\n");
			
			if(isset($row['open-home.period3-start-time']) && !empty($row['open-home.period3-start-time']))
				fputs($f, '			<period3-start-time>'.$row['open-home.period3-start-time'].'</period3-start-time>'."\n");
			
			if(isset($row['open-home.period3-end-time']) && !empty($row['open-home.period3-end-time']))	
				fputs($f, '			<period3-end-time>'.$row['open-home.period3-end-time'].'</period3-end-time>'."\n");
			
			if(isset($row['open-home.period3-details']) && !empty($row['open-home.period3-details']))		
				fputs($f, '			<period3-details>'.$row['open-home.period3-details'].'</period3-details>'."\n");
				
			if(isset($row['open-home.period4-date']) && !empty($row['open-home.period4-date']))		
				fputs($f, '			<period4-date>'.$row['open-home.period4-date'].'</period4-date>'."\n");
			
			if(isset($row['open-home.period4-start-time']) && !empty($row['open-home.period4-start-time']))
				fputs($f, '			<period4-start-time>'.$row['open-home.period4-start-time'].'</period4-start-time>'."\n");
			
			if(isset($row['open-home.period4-end-time']) && !empty($row['open-home.period4-end-time']))	
				fputs($f, '			<period4-end-time>'.$row['open-home.period4-end-time'].'</period4-end-time>'."\n");
				
			if(isset($row['open-home.period4-details']) && !empty($row['open-home.period4-details']))			
				fputs($f, '			<period4-details>'.$row['open-home.period4-details'].'</period4-details>'."\n");
				
			fputs($f, '		</open-home>'."\n");
			
			if(isset($row['taxes']) && !empty($row['taxes'])){
				fputs($f, '		<taxes>'."\n");
				
				foreach($row['taxes'] as $tax){
					fputs($f, '			<tax>'."\n");
					fputs($f, '				<tax-type>'.$tax['tax-type'].'</tax-type>'."\n");
					fputs($f, '				<tax-year>'.$tax['tax-year'].'</tax-year>'."\n");
					fputs($f, '				<tax-amount>'.$tax['tax-amount'].'</tax-amount>'."\n");
					fputs($f, '				<tax-description>'.$tax['tax-description'].'</tax-description>'."\n");
					fputs($f, '			</tax>'."\n");
				}
				
				fputs($f, '		</taxes>'."\n");
			}
			
			fputs($f, '		<hoa-fees>'."\n");
			
			if(isset($row['hoa-fees.hoa-fee']) && !empty($row['hoa-fees.hoa-fee']))
				fputs($f, '			<hoa-fee>'.$row['hoa-fees.hoa-fee'].'</hoa-fee>'."\n");
			
			if(isset($row['hoa-fees.hoa-period']) && !empty($row['hoa-fees.hoa-period']))
				fputs($f, '			<hoa-period>'.$row['hoa-fees.hoa-period'].'</hoa-period>'."\n");
			
			if(isset($row['hoa-fees.hoa-description']) && !empty($row['hoa-fees.hoa-description']))
				fputs($f, '			<hoa-description>'.$row['hoa-fees.hoa-description'].'</hoa-description>'."\n");
				
			fputs($f, '		</hoa-fees>'."\n");
	
			if(isset($row['additional-fees']) && !empty($row['additional-fees'])){
				fputs($f, '		<additional-fees>'."\n");
	
				foreach($row['additional-fees'] as $fee){
					fputs($f, '			<fee>'."\n");
					fputs($f, '				<fee-type>'.$fee['fee-type'].'</fee-type>'."\n");
					fputs($f, '				<fee-amount>'.$fee['fee-amount'].'</fee-amount>'."\n");
					fputs($f, '				<fee-period>'.$fee['fee-period'].'</fee-period>'."\n");
					fputs($f, '				<fee-description>'.$fee['fee-description'].'</fee-description>'."\n");
					fputs($f, '			</fee>'."\n");
				}
				
				fputs($f, '		</additional-fees>'."\n");
			}
			
			fputs($f, '		<schools>'."\n");
			fputs($f, '			<school-district>'."\n");
			
			if(isset($row['schools.school-district.elementary']) && !empty($row['schools.school-district.elementary']))
				fputs($f, '				<elementary>'.$row['schools.school-district.elementary'].'</elementary>'."\n");
			
			if(isset($row['schools.school-district.middle']) && !empty($row['schools.school-district.middle']))
				fputs($f, '				<middle>'.$row['schools.school-district.middle'].'</middle>'."\n");
			
			if(isset($row['schools.school-district.juniorhigh']) && !empty($row['schools.school-district.juniorhigh']))
				fputs($f, '				<juniorhigh>'.$row['schools.school-district.juniorhigh'].'</juniorhigh>'."\n");
				
			if(isset($row['schools.school-district.high']) && !empty($row['schools.school-district.high']))	
				fputs($f, '				<high>'.$row['schools.school-district.high'].'</high>'."\n");
			
			if(isset($row['schools.school-district.district-name']) && !empty($row['schools.school-district.district-name']))	
				fputs($f, '				<district-name>'.$row['schools.school-district.district-name'].'</district-name>'."\n");
			
			if(isset($row['schools.school-district.district-website']) && !empty($row['schools.school-district.district-website']))				
				fputs($f, '				<district-website>'.$row['schools.school-district.district-website'].'</district-website>'."\n");
			
			if(isset($row['schools.school-district.district-phone-number']) && !empty($row['schools.school-district.district-phone-number']))	
				fputs($f, '				<district-phone-number>'.$row['schools.school-district.district-phone-number'].'</district-phone-number>'."\n");
				
			fputs($f, '			</school-district>'."\n");
			fputs($f, '		</schools>'."\n");

			fputs($f, '		<detailed-characteristics>'."\n");
			fputs($f, '			<appliances>'."\n");
			
			if(isset($row['detailed-characteristics.appliances.has-washer']))
				fputs($f, '				<has-washer>'.$row['detailed-characteristics.appliances.has-washer'].'</has-washer>'."\n");
			
			if(isset($row['detailed-characteristics.appliances.has-dryer']))
				fputs($f, '				<has-dryer>'.$row['detailed-characteristics.appliances.has-dryer'].'</has-dryer>'."\n");
			
			if(isset($row['detailed-characteristics.appliances.has-dishwasher']))
				fputs($f, '				<has-dishwasher>'.$row['detailed-characteristics.appliances.has-dishwasher'].'</has-dishwasher>'."\n");
			
			if(isset($row['detailed-characteristics.appliances.has-refrigerator']))
				fputs($f, '				<has-refrigerator>'.$row['detailed-characteristics.appliances.has-refrigerator'].'</has-refrigerator>'."\n");
				
			if(isset($row['detailed-characteristics.appliances.has-disposal']))	
				fputs($f, '				<has-disposal>'.$row['detailed-characteristics.appliances.has-disposal'].'</has-disposal>'."\n");
			
			if(isset($row['detailed-characteristics.appliances.has-microwave']))	
				fputs($f, '				<has-microwave>'.$row['detailed-characteristics.appliances.has-microwave'].'</has-microwave>'."\n");
				
			if(isset($row['detailed-characteristics.appliances.range-type']) && !empty($row['detailed-characteristics.appliances.range-type']))		
				fputs($f, '				<range-type>'.$row['detailed-characteristics.appliances.range-type'].'</range-type>'."\n");
			
			if(isset($row['detailed-characteristics.appliances.additional-appliances']) && !empty($row['detailed-characteristics.appliances.additional-appliances'])){
				foreach($row['detailed-characteristics.appliances.additional-appliances'] as $additional_appliance){
					fputs($f, '				<additional-appliance>'."\n");
					fputs($f, '					<additional-appliance-name>'.$row['additional-appliance-name'].'</additional-appliance-name>'."\n");
					fputs($f, '					<additional-appliance-description>'.$row['additional-appliance-description'].'</additional-appliance-description>'."\n");
					fputs($f, '				</additional-appliance>'."\n");
				}
			}
			
			if(isset($row['detailed-characteristics.appliances.appliances-comments']) && !empty($row['detailed-characteristics.appliances.appliances-comments']))		
				fputs($f, '				<appliances-comments>'.$row['detailed-characteristics.appliances.appliances-comments'].'</appliances-comments>'."\n");
				
			fputs($f, '			</appliances>'."\n");		
			fputs($f, '			<cooling-systems>'."\n");
			
			if(isset($row['detailed-characteristics.cooling-systems.has-air-conditioning']))
				fputs($f, '				<has-air-conditioning>'.$row['detailed-characteristics.cooling-systems.has-air-conditioning'].'</has-air-conditioning>'."\n");
			
			if(isset($row['detailed-characteristics.cooling-systems.has-ceiling-fan']))
				fputs($f, '				<has-ceiling-fan>'.$row['detailed-characteristics.cooling-systems.has-ceiling-fan'].'</has-ceiling-fan>'."\n");
			
			if(isset($row['detailed-characteristics.cooling-systems.other-cooling']) && !empty($row['detailed-characteristics.cooling-systems.other-cooling']))
				fputs($f, '				<other-cooling>'.$row['detailed-characteristics.cooling-systems.other-cooling'].'</other-cooling>'."\n");
			
			fputs($f, '			</cooling-systems>'."\n");
			fputs($f, '			<heating-systems>'."\n");
			
			if(isset($row['detailed-characteristics.heating-systems.has-fireplace']))
				fputs($f, '				<has-fireplace>'.$row['detailed-characteristics.heating-systems.has-fireplace'].'</has-fireplace>'."\n");
			
			if(isset($row['detailed-characteristics.heating-systems.fireplace-type']) && !empty($row['detailed-characteristics.heating-systems.fireplace-type']))
				fputs($f, '				<fireplace-type>'.$row['detailed-characteristics.heating-systems.fireplace-type'].'</fireplace-type>'."\n");
				
			if(isset($row['detailed-characteristics.heating-systems.heating-system']) && !empty($row['detailed-characteristics.heating-systems.heating-system']))
				fputs($f, '				<heating-system>'.$row['detailed-characteristics.heating-systems.heating-system'].'</heating-system>'."\n");
			
			if(isset($row['detailed-characteristics.heating-systems.heating-fuel']) && !empty($row['detailed-characteristics.heating-systems.heating-fuel']))
				fputs($f, '				<heating-fuel>'.$row['detailed-characteristics.heating-systems.heating-fuel'].'</heating-fuel>'."\n");
			
			fputs($f, '			</heating-systems>'."\n");
			
			if(isset($row['detailed-characteristics.floor-coverings']) && !empty($row['detailed-characteristics.floor-coverings']))
				fputs($f, '			<floor-coverings>'.$row['detailed-characteristics.floor-coverings'].'</floor-coverings>'."\n");
				
			if(isset($row['detailed-characteristics.total-unit-parking-spaces']) && !empty($row['detailed-characteristics.total-unit-parking-spaces']))
				fputs($f, '			<total-unit-parking-spaces>'.$row['detailed-characteristics.total-unit-parking-spaces'].'</total-unit-parking-spaces>'."\n");
				
			if(isset($row['detailed-characteristics.has-garage']))	
				fputs($f, '			<has-garage>'.$row['detailed-characteristics.has-garage'].'</has-garage>'."\n");
			
			if(isset($row['detailed-characteristics.garage-type']) && !empty($row['detailed-characteristics.garage-type']))	
				fputs($f, '			<garage-type>'.$row['detailed-characteristics.garage-type'].'</garage-type>'."\n");
			
			if(isset($row['detailed-characteristics.parking-types']) && !empty($row['detailed-characteristics.parking-types'])){
				fputs($f, '			<parking-types>'."\n");		
				fputs($f, '				<parking-type>'.$row['detailed-characteristics.parking-types'].'</parking-type>'."\n");
				fputs($f, '			</parking-types>'."\n");
			}
			
			if(isset($row['detailed-characteristics.has-assigned-parking-space']))	
				fputs($f, '			<has-assigned-parking-space>'.$row['detailed-characteristics.has-assigned-parking-space'].'</has-assigned-parking-space>'."\n");

			if(isset($row['detailed-characteristics.parking-space-fee']) && !empty($row['detailed-characteristics.parking-space-fee']))	
				fputs($f, '			<parking-space-fee>'.$row['detailed-characteristics.parking-space-fee'].'</parking-space-fee>'."\n");
			
			if(isset($row['detailed-characteristics.assigned-parking-space-cost']) && !empty($row['detailed-characteristics.assigned-parking-space-cost']))	
				fputs($f, '			<assigned-parking-space-cost>'.$row['detailed-characteristics.assigned-parking-space-cost'].'</assigned-parking-space-cost>'."\n");
				
			if(isset($row['detailed-characteristics.parking-comment']) && !empty($row['detailed-characteristics.parking-comment']))		
				fputs($f, '			<parking-comment>'.$row['detailed-characteristics.parking-comment'].'</parking-comment>'."\n");
				
			if(isset($row['detailed-characteristics.foundation-type']) && !empty($row['detailed-characteristics.foundation-type']))			
				fputs($f, '			<foundation-type>'.$row['detailed-characteristics.foundation-type'].'</foundation-type>'."\n");
				
			if(isset($row['detailed-characteristics.roof-type']) && !empty($row['detailed-characteristics.roof-type']))			
				fputs($f, '			<roof-type>'.$row['detailed-characteristics.roof-type'].'</roof-type>'."\n");
				
			if(isset($row['detailed-characteristics.architecture-style']) && !empty($row['detailed-characteristics.architecture-style']))				
				fputs($f, '			<architecture-style>'.$row['detailed-characteristics.architecture-style'].'</architecture-style>'."\n");
			
			if(isset($row['detailed-characteristics.exterior-type']) && !empty($row['detailed-characteristics.exterior-type']))				
				fputs($f, '			<exterior-type>'.$row['detailed-characteristics.exterior-type'].'</exterior-type>'."\n");
				
			if(isset($row['detailed-characteristics.room-count']) && !empty($row['detailed-characteristics.room-count']))					
				fputs($f, '			<room-count>'.$row['detailed-characteristics.room-count'].'</room-count>'."\n");
			
			if(isset($row['detailed-characteristics.rooms']) && !empty($row['detailed-characteristics.rooms'])){
				fputs($f, '			<rooms>'."\n");
				
				foreach($row['detailed-characteristics.rooms'] as $room){
					fputs($f, '				<room>'."\n");
					fputs($f, '					<room-type>'.$room['room-type'].'</room-type>'."\n");
					fputs($f, '					<room-size>'.$room['room-size'].'</room-size>'."\n");
					fputs($f, '					<room-description>'.$room['room-description'].'</room-description>'."\n");
					fputs($f, '				</room>'."\n");
				}
				
				fputs($f, '			</rooms>'."\n");
			}

			if(isset($row['detailed-characteristics.year-updated']) && !empty($row['detailed-characteristics.year-updated']) && strlen($row['detailed-characteristics.year-updated'])==4)
				fputs($f, '			<year-updated>'.$row['detailed-characteristics.year-updated'].'</year-updated>'."\n");
			
			if(isset($row['detailed-characteristics.total-units-in-building']) && !empty($row['detailed-characteristics.total-units-in-building']))
				fputs($f, '			<total-units-in-building>'.$row['detailed-characteristics.total-units-in-building'].'</total-units-in-building>'."\n");
			
			if(isset($row['detailed-characteristics.total-floors-in-building']) && !empty($row['detailed-characteristics.total-floors-in-building']))
				fputs($f, '			<total-floors-in-building>'.$row['detailed-characteristics.total-floors-in-building'].'</total-floors-in-building>'."\n");
			
			if(isset($row['detailed-characteristics.num-floors-in-unit']) && !empty($row['detailed-characteristics.num-floors-in-unit']))
				fputs($f, '			<num-floors-in-unit>'.$row['detailed-characteristics.num-floors-in-unit'].'</num-floors-in-unit>'."\n");
				
			if(isset($row['detailed-characteristics.has-attic']))
				fputs($f, '			<has-attic>'.$row['detailed-characteristics.has-attic'].'</has-attic>'."\n");
				
			if(isset($row['detailed-characteristics.has-balcony']))	
				fputs($f, '			<has-balcony>'.$row['detailed-characteristics.has-balcony'].'</has-balcony>'."\n");
				
			if(isset($row['detailed-characteristics.has-barbeque-area']))		
				fputs($f, '			<has-barbeque-area>'.$row['detailed-characteristics.has-barbeque-area'].'</has-barbeque-area>'."\n");
				
			if(isset($row['detailed-characteristics.has-basement']))
				fputs($f, '			<has-basement>'.$row['detailed-characteristics.has-basement'].'</has-basement>'."\n");
			
			if(isset($row['detailed-characteristics.has-cable-satellite']))
				fputs($f, '			<has-cable-satellite>'.$row['detailed-characteristics.has-cable-satellite'].'</has-cable-satellite>'."\n");
				
			if(isset($row['detailed-characteristics.has-courtyard']))
				fputs($f, '			<has-courtyard>'.$row['detailed-characteristics.has-courtyard'].'</has-courtyard>'."\n");
			
			if(isset($row['detailed-characteristics.has-deck']))
				fputs($f, '			<has-deck>'.$row['detailed-characteristics.has-deck'].'</has-deck>'."\n");
			
			if(isset($row['detailed-characteristics.has-disabled-access']))
				fputs($f, '			<has-disabled-access>'.$row['detailed-characteristics.has-disabled-access'].'</has-disabled-access>'."\n");

			if(isset($row['detailed-characteristics.has-dock']))
				fputs($f, '			<has-dock>'.$row['detailed-characteristics.has-dock'].'</has-dock>'."\n");
				
			if(isset($row['detailed-characteristics.has-doublepane-windows']))	
				fputs($f, '			<has-doublepane-windows>'.$row['detailed-characteristics.has-doublepane-windows'].'</has-doublepane-windows>'."\n");
			
			if(isset($row['detailed-characteristics.has-garden']))	
				fputs($f, '			<has-garden>'.$row['detailed-characteristics.has-garden'].'</has-garden>'."\n");
				
			if(isset($row['detailed-characteristics.has-gated-entry']))		
				fputs($f, '			<has-gated-entry>'.$row['detailed-characteristics.has-gated-entry'].'</has-gated-entry>'."\n");
			
			if(isset($row['detailed-characteristics.has-greenhouse']))		
				fputs($f, '			<has-greenhouse>'.$row['detailed-characteristics.has-greenhouse'].'</has-greenhouse>'."\n");
				
			if(isset($row['detailed-characteristics.has-handrails']))			
				fputs($f, '			<has-handrails>'.$row['detailed-characteristics.has-handrails'].'</has-handrails>'."\n");
				
			if(isset($row['detailed-characteristics.has-hot-tub-spa']))			
				fputs($f, '			<has-hot-tub-spa>'.$row['detailed-characteristics.has-hot-tub-spa'].'</has-hot-tub-spa>'."\n");
			
			if(isset($row['detailed-characteristics.has-intercom']))			
				fputs($f, '			<has-intercom>'.$row['detailed-characteristics.has-intercom'].'</has-intercom>'."\n");
				
			if(isset($row['detailed-characteristics.has-jetted-bath-tub']))				
				fputs($f, '			<has-jetted-bath-tub>'.$row['detailed-characteristics.has-jetted-bath-tub'].'</has-jetted-bath-tub>'."\n");
			
			if(isset($row['detailed-characteristics.has-lawn']))
				fputs($f, '			<has-lawn>'.$row['has-lawn'].'</has-lawn>'."\n");
			
			if(isset($row['detailed-characteristics.has-mother-in-law']))
				fputs($f, '			<has-mother-in-law>'.$row['detailed-characteristics.has-mother-in-law'].'</has-mother-in-law>'."\n");
				
			if(isset($row['detailed-characteristics.has-patio']))	
				fputs($f, '			<has-patio>'.$row['detailed-characteristics.has-patio'].'</has-patio>'."\n");
			
			if(isset($row['detailed-characteristics.has-pond']))	
				fputs($f, '			<has-pond>'.$row['detailed-characteristics.has-pond'].'</has-pond>'."\n");
			
			if(isset($row['detailed-characteristics.has-pool']))	
				fputs($f, '			<has-pool>'.$row['detailed-characteristics.has-pool'].'</has-pool>'."\n");
			
			if(isset($row['detailed-characteristics.has-porch']))	
				fputs($f, '			<has-porch>'.$row['detailed-characteristics.has-porch'].'</has-porch>'."\n");
			
			if(isset($row['detailed-characteristics.has-private-balcony']))	
				fputs($f, '			<has-private-balcony>'.$row['detailed-characteristics.has-private-balcony'].'</has-private-balcony>'."\n");
			
			if(isset($row['detailed-characteristics.has-private-patio']))	
				fputs($f, '			<has-private-patio>'.$row['detailed-characteristics.has-private-patio'].'</has-private-patio>'."\n");
			
			if(isset($row['detailed-characteristics.has-rv-parking']))	
				fputs($f, '			<has-rv-parking>'.$row['detailed-characteristics.has-rv-parking'].'</has-rv-parking>'."\n");
			
			if(isset($row['detailed-characteristics.has-sauna']))	
				fputs($f, '			<has-sauna>'.$row['detailed-characteristics.has-sauna'].'</has-sauna>'."\n");
			
			if(isset($row['detailed-characteristics.has-security-system']))	
				fputs($f, '			<has-security-system>'.$row['detailed-characteristics.has-security-system'].'</has-security-system>'."\n");
			
			if(isset($row['detailed-characteristics.has-skylight']))	
				fputs($f, '			<has-skylight>'.$row['detailed-characteristics.has-skylight'].'</has-skylight>'."\n");
			
			if(isset($row['detailed-characteristics.has-sportscourt']))	
				fputs($f, '			<has-sportscourt>'.$row['detailed-characteristics.has-sportscourt'].'</has-sportscourt>'."\n");
			
			if(isset($row['detailed-characteristics.has-sprinkler-system']))	
				fputs($f, '			<has-sprinkler-system>'.$row['detailed-characteristics.has-sprinkler-system'].'</has-sprinkler-system>'."\n");
			
			if(isset($row['detailed-characteristics.has-terrace']))	
				fputs($f, '			<has-terrace>'.$row['detailed-characteristics.has-terrace'].'</has-terrace>'."\n");
			
			if(isset($row['detailed-characteristics.has-vaulted-ceiling']))	
				fputs($f, '			<has-vaulted-ceiling>'.$row['detailed-characteristics.has-vaulted-ceiling'].'</has-vaulted-ceiling>'."\n");
			
			if(isset($row['detailed-characteristics.has-view']))	
				fputs($f, '			<has-view>'.$row['detailed-characteristics.has-view'].'</has-view>'."\n");
			
			if(isset($row['detailed-characteristics.has-washer-dryer-hookup']))	
				fputs($f, '			<has-washer-dryer-hookup>'.$row['detailed-characteristics.has-washer-dryer-hookup'].'</has-washer-dryer-hookup>'."\n");
			
			if(isset($row['detailed-characteristics.has-wet-bar']))	
				fputs($f, '			<has-wet-bar>'.$row['detailed-characteristics.has-wet-bar'].'</has-wet-bar>'."\n");
			
			if(isset($row['detailed-characteristics.has-window-coverings']))	
				fputs($f, '			<has-window-coverings>'.$row['detailed-characteristics.has-window-coverings'].'</has-window-coverings>'."\n");

			if(isset($row['detailed-characteristics.building-has-concierge']))		
				fputs($f, '			<building-has-concierge>'.$row['detailed-characteristics.building-has-concierge'].'</building-has-concierge>'."\n");
			
			if(isset($row['detailed-characteristics.building-has-doorman']))		
				fputs($f, '			<building-has-doorman>'.$row['detailed-characteristics.building-has-doorman'].'</building-has-doorman>'."\n");
			
			if(isset($row['detailed-characteristics.building-has-elevator']))		
				fputs($f, '			<building-has-elevator>'.$row['detailed-characteristics.building-has-elevator'].'</building-has-elevator>'."\n");
				
			if(isset($row['detailed-characteristics.building-has-fitness-center']))		
				fputs($f, '			<building-has-fitness-center>'.$row['detailed-characteristics.building-has-fitness-center'].'</building-has-fitness-center>'."\n");
				
			if(isset($row['detailed-characteristics.building-has-on-site-maintenance']))		
				fputs($f, '			<building-has-on-site-maintenance>'.$row['detailed-characteristics.building-has-on-site-maintenance'].'</building-has-on-site-maintenance>'."\n");
			
			if(isset($row['detailed-characteristics.is-waterfront']))		
				fputs($f, '			<is-waterfront>'.$row['detailed-characteristics.is-waterfront'].'</is-waterfront>'."\n");
			
			if(isset($row['detailed-characteristics.other-amenities']) && !empty($row['detailed-characteristics.other-amenities'])){
				fputs($f, '			<other-amenities>'."\n");
				fputs($f, '				<other-amenity>'.$row['detailed-characteristics.other-amenities'].'</other-amenity>'."\n");
				fputs($f, '			</other-amenities>'."\n");
			}
			
			if(isset($row['detailed-characteristics.furnished']))		
				fputs($f, '			<furnished>'.$row['detailed-characteristics.furnished'].'</furnished>'."\n");
				
			if(isset($row['detailed-characteristics.view-type']) && !empty($row['detailed-characteristics.view-type']))			
				fputs($f, '			<view-type>'.$row['detailed-characteristics.view-type'].'</view-type>'."\n");
				
			fputs($f, '		</detailed-characteristics>'."\n");
			fputs($f, '		<advertise-with-us>'."\n");
			
			if(isset($row['advertise-with-us.channel']) && !empty($row['advertise-with-us.channel']))			
				fputs($f, '			<channel>'.$row['advertise-with-us.channel'].'</channel>'."\n");
			
			if(isset($row['advertise-with-us.featured']) && !empty($row['advertise-with-us.featured']))			
				fputs($f, '			<featured>'.$row['advertise-with-us.featured'].'</featured>'."\n");
			
			if(isset($row['advertise-with-us.branded']) && !empty($row['advertise-with-us.branded']))			
				fputs($f, '			<branded>'.$row['advertise-with-us.branded'].'</branded>'."\n");
				
			if(isset($row['advertise-with-us.branded-logo-url']) && !empty($row['advertise-with-us.branded-logo-url']))				
				fputs($f, '			<branded-logo-url>'.$row['advertise-with-us.branded-logo-url'].'</branded-logo-url>'."\n");
				
			fputs($f, '		</advertise-with-us>'."\n");
			fputs($f, '	</property>'."\n");
		}
		
		fputs($f, '</properties>'."\n");		
		fclose($f);
	}
	
	/**
	 * Validate export item data
	 * 
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());		

		if(isset($data['location.unit-number'])){
			$return['data']['location.unit-number'] = intval($data['location.unit-number']);
			if(!empty($return['data']['location.unit-number'])){
				$return['data']['location.unit-number'] = $return['data']['location.unit-number'];
			}
		}
			
		if(isset($data['location.street-address'])){
			$return['data']['location.street-address'] = $data['location.street-address'];
			if(!empty($return['data']['location.street-address'])){
				$return['data']['location.street-address'] = str_replace($this->search, $this->replace, $return['data']['location.street-address']);
			}
		}
			
		if(isset($data['location.city-name'])){
			$return['data']['location.city-name'] = $data['location.city-name'];
			if(!empty($return['data']['location.city-name'])){
				$return['data']['location.city-name'] = str_replace($this->search, $this->replace, $return['data']['location.city-name']);
			}
		}
		
		if(isset($data['location.zipcode'])){
			$return['data']['location.zipcode'] = $data['location.zipcode'];
			if(!empty($return['data']['location.zipcode'])){
				$return['data']['location.zipcode'] = str_replace($this->search, $this->replace, $return['data']['location.zipcode']);
			}
		}
		
		if(isset($data['location.county'])){
			$return['data']['location.county'] = $data['location.county'];
			if(!empty($return['data']['location.county'])){
				$return['data']['location.county'] = str_replace($this->search, $this->replace, $return['data']['location.county']);
			}
		}
				
		if(isset($data['location.state-code'])){
			$return['data']['location.state-code'] = $data['location.state-code'];
			if(!empty($return['data']['location.state-code'])){
				$return['data']['location.state-code'] = str_replace($this->search, $this->replace, $return['data']['location.state-code']);
			}
		}
		
		if(isset($data['location.street-intersection'])){
			$return['data']['location.street-intersection'] = $data['location.street-intersection'];
			if(!empty($return['data']['location.street-intersection'])){
				$return['data']['location.street-intersection'] = str_replace($this->search, $this->replace, $return['data']['location.street-intersection']);
			}
		}		
				
		if(isset($data['location.parcel-id'])){
			$return['data']['location.parcel-id'] = $data['location.parcel-id'];
			if(!empty($return['data']['location.parcel-id'])){
				$return['data']['location.parcel-id'] = str_replace($this->search, $this->replace, $return['data']['location.parcel-id']);
			}
		}
			
		if(isset($data['location.building-name'])){
			$return['data']['location.building-name'] = $data['location.building-name'];
			if(!empty($return['data']['location.building-name'])){
				$return['data']['location.building-name'] = str_replace($this->search, $this->replace, $return['data']['location.building-name']);
			}
		}
		
		if(isset($data['location.subdivision'])){
			$return['data']['location.subdivision'] = $data['location.subdivision'];
			if(!empty($return['data']['location.subdivision'])){
				$return['data']['location.subdivision'] = str_replace($this->search, $this->replace, $return['data']['location.subdivision']);
			}
		}
		
		if(isset($data['location.neighborhood-name'])){
			$return['data']['location.neighborhood-name'] = $data['location.neighborhood-name'];
			if(!empty($return['data']['location.neighborhood-name'])){
				$return['data']['location.neighborhood-name'] = str_replace($this->search, $this->replace, $return['data']['location.neighborhood-name']);
			}
		}
			
		if(isset($data['location.neighborhood-description'])){
			$return['data']['location.neighborhood-description'] = $data['location.neighborhood-description'];
			if(!empty($return['data']['location.neighborhood-description'])){
				$return['data']['location.neighborhood-description'] = str_replace($this->search, $this->replace, $return['data']['location.neighborhood-name']);
			}
		}
			
		if(isset($data['location.elevation'])){
			$return['data']['location.elevation'] = floatval($data['location.elevation']);
		}
		
		if(isset($data['location.longitude'])){
			$return['data']['location.longitude'] = floatval($data['location.longitude']);
		}
			
		if(isset($data['location.latitude'])){
			$return['data']['location.latitude'] = floatval($data['location.latitude']);
		}	
			
		if(isset($data['location.geocode-type'])){
			if(!empty($data['location.geocode-type']) && in_array($data['location.geocode-type'], $this->geocode_types)){
				$return['data']['location.geocode-type'] = str_replace($this->search, $this->replace, $data['location.geocode-type']);
			}
		}	
		
		if(isset($data['location.display-address'])){
			$return['data']['location.display-address'] = $data['location.display-address'] ? 'yes' : 'no';
		}else{
			$return['data']['location.display-address'] = 'yes';
		}
		
		if(isset($data['location.directions'])){
			$return['data']['location.directions'] = $data['location.directions'];
			if(!empty($return['data']['location.directions'])){
				$return['data']['location.directions'] = str_replace($this->search, $this->replace, $return['data']['location.directions']);
			}
		}
		
		if(isset($data['details.listing-title'])){
			$return['data']['details.listing-title'] = $data['details.listing-title'];
			if(!empty($return['data']['details.listing-title'])){
				$return['data']['details.listing-title'] = str_replace($this->search, $this->replace, $return['data']['details.listing-title']);
			}
		}
		
		if(isset($data['details.price'])){
			$return['data']['details.price'] = intval($data['details.price']);
			if(empty($return['data']['details.price'])) $return["errors"][] = "details.price";
		}else{
			$return["errors"][] = "details.price";
		}
				
		if(isset($data['details.year-built'])){
			if(strlen($data['details.year-built']) == 4){
				$return['data']['details.year-built'] = intval($data['details.year-built']);
			}
		}
		
		if(isset($data['details.num-bedrooms'])){
			$return['data']['details.num-bedrooms'] = intval($data['details.num-bedrooms']);
		}
			
		if(isset($data['details.num-full-bathrooms'])){
			$return['data']['details.num-full-bathrooms'] = intval($data['details.num-full-bathrooms']);
		}
		
		if(isset($data['details.num-half-bathrooms'])){
			$return['data']['details.num-half-bathrooms'] = intval($data['details.num-half-bathrooms']);
		}
		
		if(isset($data['details.num-bathrooms'])){
			$return['data']['details.num-bathrooms'] = intval($data['details.num-bathrooms']);
		}		
			
		if(isset($data['details.lot-size'])){
			$return['data']['details.lot-size'] = intval($data['details.lot-size']);
		}	
			
		if(isset($data['details.living-area-square-feet'])){
			$return['data']['details.living-area-square-feet'] = intval($data['details.living-area-square-feet']);
		}	
		
		if(isset($data['details.date-listed'])){
			$value = strtotime($data['details.date-listed']);
			if($value > 0) $return['data']['details.date-listed'] = date('Y-m-d', $value);
		}
		
		if(isset($data['details.date-available'])){
			$value = strtotime($data['details.date-available']);
			if($value > 0) $return['data']['details.date-available'] = date('Y-m-d', $value);
		}
			
		if(isset($data['details.date-sold'])){
			$value = strtotime($data['details.date-sold']);
			if($value > 0) $return['data']['details.date-sold'] = date('Y-m-d', $value);
		}	
		
		if(isset($data['details.sale-price'])){
			$return['data']['details.sale-price'] = intval($data['details.sale-price']);
		}	
		
		if(isset($data['details.property-type'])){
			$return['data']['details.property-type'] = $data['details.property-type'];
			if(!in_array($data['details.property-type'], $this->property_types)){
				if(isset($data['details.property-type2'])){
					$return['data']['details.property-type'] = $data['details.property-type2'];
					if(!in_array($data['details.property-type'], $this->property_types)){
						if(isset($data['details.property-type3'])){
							$return['data']['details.property-type'] = $data['details.property-type3'];
							if(!in_array($data['details.property-type'], $this->property_types)){
								if(isset($data['details.property-type4'])){
									$return['data']['details.property-type'] = $data['details.property-type4'];
									if(!in_array($data['details.property-type'], $this->property_types)){
										if(isset($data['details.property-type5'])){
											$return['data']['details.property-type'] = $data['details.property-type5'];
											if(!in_array($data['details.property-type'], $this->property_types)){
												if(isset($data['details.property-type6'])){
													$return['data']['details.property-type'] = $data['details.property-type6'];
													if(!in_array($data['details.property-type'], $this->property_types)){
														$return['data']['details.property-type'] = current($this->property_types);
														//$return['errors'][] = 'details.property-type';
													}
												}
											}else{
												$return['data']['details.property-type'] = current($this->property_types);
												//$return['errors'][] = 'details.property-type';
											}
										}
									}else{
										$return['data']['details.property-type'] = current($this->property_types);
										//$return['errors'][] = 'details.property-type';
									}
								}
							}
						}else{
							$return['data']['details.property-type'] = current($this->property_types);
							//$return['errors'][] = 'details.property-type';
						}
					}
				}else{
					$return['data']['details.property-type'] = current($this->property_types);
					//$return['errors'][] = 'details.property-type';
				}	
			}
		}else{
			$return['data']['details.property-type'] = current($this->property_types);
			//$return['errors'][] = 'details.property-type';
		}
			
		if(isset($data['details.description'])){
			$return['data']['details.description'] = str_replace($this->search, $this->replace, $data['details.description']);
		}	
			
		if(isset($data['details.mlsId'])){
			$return['data']['details.mlsId'] = str_replace($this->search, $this->replace, $data['details.mlsId']);
		}	
		
		if(isset($data['details.mlsName'])){
			$return['data']['details.mlsName'] = str_replace($this->search, $this->replace, $data['details.mlsName']);
		}	
		
		if(isset($data['details.provider-listingid'])){
			$return['data']['details.provider-listingid'] = intval($data['details.provider-listingid']);
			if(empty($return['data']['details.provider-listingid'])){
				$return["errors"][] = "details.provider-listingid";
			}
		}else{
			$return["errors"][] = "details.provider-listingid";
		}
		
		if(isset($data['landing-page.lp-url'])){
			$return['data']['landing-page.lp-url'] = str_replace($this->search, $this->replace, $data['landing-page.lp-url']);
			if(empty($return['data']['landing-page.lp-url'])) $return["errors"][] = "landing-page.lp-url";
		}else{
			$return["errors"][] = "landing-page.lp-url";
		}
				
		if(isset($data['listing-type'])){
			if(in_array($data['listing-type'], $this->listing_types)){
				$return['data']['listing-type'] = $data['listing-type'];
			}
		}
	
		if(isset($data['status'])){
			if(in_array($data['status'], $this->statuses)){
				$return['data']['status'] = $data['status'];
			}else{
				$return['data']['status'] = str_replace($this->search, $this->replace, $data['status']);
			}
		}else{
			$return['data']['status'] = current($this->statuses);
		}
			
		if(isset($data['foreclosure-status'])){
			if(in_array($data['foreclosure-status'], $this->foreclosure_statuses)){
				$return['data']['foreclosure-status'] = $data['foreclosure-status'];
			}
		}	
		
		if(isset($data['site.site-url'])){
			$return['data']['site.site-url'] = $data['site.site-url'];
			if(!empty($return['data']['site.site-url'])){
				$return['data']['site.site-url'] = str_replace($this->search, $this->replace, $return['data']['site.site-url']);
			}else{
				$return['data']['site.site-url'] = str_replace($this->search, $this->replace, preg_replace('/http(s)?:\/\//', '', SITE_SERVER));
				//$return["errors"][] = "site.site-url";
			}
		}else{
			$return['data']['site.site-url'] = str_replace($this->search, $this->replace, preg_replace('/http(s)?:\/\//', '', SITE_SERVER));
			//$return["errors"][] = "site.site-url";
		}
	
		if(isset($data['site.site-name'])){
			$return['data']['site.site-name'] = $data['site.site-name'];
			if(!empty($return['data']['site.site-name'])){
				$return['data']['site.site-name'] = str_replace($this->search, $this->replace, $return['data']['site.site-name']);
			}else{
				$return['data']['site.site-name'] = trim(preg_replace('#^http(s)?://#i', '', $return['data']['site.site-url']), '/');
				//$return["errors"][] = "site.site-name";
			}
		}else{
			$return['data']['site.site-name'] = trim(preg_replace('#^http(s)?://#i', '', $return['data']['site.site-url']), '/');
			//$return["errors"][] = "site.site-name";
		}
			
		if(isset($data['rental-terms.price-term'])){
			if(in_array($data['rental-terms.price-term'], $this->price_terms)){
				$return['data']['rental-terms.price-term'] = $data['rental-terms.price-term'];
			}
		}	
		
		if(isset($data['rental-terms.rental-type'])){
			if(in_array($data['rental-terms.rental-type'], $this->rental_types)){
				$return['data']['rental-terms.rental-type'] = $data['rental-terms.rental-type'];
			}
		}
			
		if(isset($data['rental-terms.lease-type'])){
			if(in_array($data['rental-terms.lease-type'], $this->lease_types)){
				$return['data']['rental-terms.lease-type'] = $data['rental-terms.lease-type'];
			}
		}		
			
		if(isset($data['rental-terms.lease-min-length-months'])){
			$return['data']['rental-terms.lease-min-length-months'] = intval($data['rental-terms.lease-min-length-months']);
		}
			
		if(isset($data['rental-terms.lease-max-length-months'])){
			$return['data']['rental-terms.lease-max-length-months'] = intval($data['rental-terms.lease-max-length-months']);
		}
		
		if(isset($data['rental-terms.lease-periods'])){
			if(!empty($data['rental-terms.lease-periods'])){
				$return['data']['rental-terms.lease-periods'] = explode('|', $data['rental-terms.lease-periods']);
				foreach($return['data']['rental-terms.lease-periods'] as $key=>$value){
					$return['data']['rental-terms.lease-periods'][$key] = str_replace($this->search, $this->replace, $value);
				}
			}
		}	
			
		if(isset($data['rental-terms.lease-details'])){
			$return['data']['rental-terms.lease-details'] = str_replace($this->search, $this->replace, $data['rental-terms.lease-details']);
		}
			
		if(isset($data['rental-terms.security-deposit'])){
			$return['data']['rental-terms.security-deposit'] = str_replace($this->search, $this->replace, $data['rental-terms.security-deposit']);
		}
		
		if(isset($data['rental-terms.security-deposit-description'])){
			$return['data']['rental-terms.security-deposit-description'] = str_replace($this->search, $this->replace, $data['rental-terms.security-deposit-description']);
		}	
			
		if(isset($data['rental-terms.application-fee'])){
			$return['data']['rental-terms.application-fee'] = intval($data['rental-terms.application-fee']);
		}
		
		if(isset($data['rental-terms.application-fee-description'])){
			$return['data']['rental-terms.application-fee-description'] = str_replace($this->search, $this->replace, $data['rental-terms.application-fee-description']);
		}
			
		if(isset($data['rental-terms.credit-cards-accepted'])){
			$return['data']['rental-terms.credit-cards-accepted'] = $data['rental-terms.credit-cards-accepted'] ? 'yes' : 'no';
		}		
		
		if(isset($data['rental-terms.credit-cards'])){
			if(!empty($data['rental-terms.credit-cards'])){
				$return['data']['rental-terms.credit-cards'] = array_intersect(explode('|', $data['rental-terms.credit-cards']), $this->credit_cards);
			}
		}	
		
		if(isset($data['rental-terms.pets.small-dogs-allowed'])){
			$return['data']['rental-terms.pets.small-dogs-allowed'] = $data['rental-terms.pets.small-dogs-allowed'] ? 'yes' : 'no';
		}
			
		if(isset($data['rental-terms.pets.large-dogs-allowed'])){
			$return['data']['rental-terms.pets.large-dogs-allowed'] = $data['rental-terms.pets.large-dogs-allowed'] ? 'yes' : 'no';
		}
		
		if(isset($data['rental-terms.pets.cats-allowed'])){
			$return['data']['rental-terms.pets.cats-allowed'] = $data['rental-terms.pets.cats-allowed'] ? 'yes' : 'no';
		}	
			
		if(isset($data['rental-terms.pets.pet-other-allowed'])){
			$return['data']['rental-terms.pets.pet-other-allowed'] = $data['rental-terms.pets.pet-other-allowed'] ? 'yes' : 'no';
		}
			
		if(isset($data['rental-terms.pets.max-pets'])){
			$return['data']['rental-terms.max-pets'] = intval($data['rental-terms.pets.max-pets']);
		}
		
		if(isset($data['rental-terms.pets.pet-deposit'])){
			$return['data']['rental-terms.pet-deposit'] = str_replace($this->search, $this->replace, $data['rental-terms.pets.pet-deposit']);
		}	
				
		if(isset($data['rental-terms.pets.pet-fee'])){
			$return['data']['rental-terms.pet-fee'] = intval($data['rental-terms.pets.pet-fee']);
		}	
		
		if(isset($data['rental-terms.pets.pet-rent'])){
			$return['data']['rental-terms.pet-rent'] = intval($data['rental-terms.pets.pet-rent']);
		}	
		
		if(isset($data['rental-terms.pets.pet-weight'])){
			$return['data']['rental-terms.pet-weight'] = floatval($data['rental-terms.pets.pet-weight']);
		}	
			
		if(isset($data['rental-terms.pets.pet-comments'])){
			if(!empty($data['rental-terms.pets.pet-comments'])){
				$return['data']['rental-terms.pet-comments'] = explode('|', $data['rental-terms.pets.pet-comments']);
				foreach($return['data']['rental-terms.pet-comments'] as $key=>$value){
					$return['data']['rental-terms.pet-comments'][$key] = str_replace($this->search, $this->replace, $value);
				}
			}
		}	
		
		if(isset($data['rental-terms.utilities-included.landlord-pays-aircon'])){
			$return['data']['rental-terms.utilities-included.landlord-pays-aircon'] = $data['rental-terms.utilities-included.landlord-pays-aircon'] ? 'yes' : 'no';
		}
		
		if(isset($data['rental-terms.utilities-included.landlord-pays-broadbandinternet'])){
			$return['data']['rental-terms.utilities-included.landlord-pays-broadbandinternet'] = $data['rental-terms.utilities-included.landlord-pays-broadbandinternet'] ? 'yes' : 'no';
		}
		
		if(isset($data['rental-terms.utilities-included.landlord-pays-cable'])){
			$return['data']['rental-terms.utilities-included.landlord-pays-cable'] = $data['rental-terms.utilities-included.landlord-pays-cable'] ? 'yes' : 'no';
		}	
		
		if(isset($data['rental-terms.utilities-included.landlord-pays-electric'])){
			$return['data']['rental-terms.utilities-included.landlord-pays-electric'] = $data['rental-terms.utilities-included.landlord-pays-electric'] ? 'yes' : 'no';
		}		
		
		if(isset($data['rental-terms.utilities-included.landlord-pays-gas'])){
			$return['data']['rental-terms.utilities-included.landlord-pays-gas'] = $data['rental-terms.utilities-included.landlord-pays-gas'] ? 'yes' : 'no';
		}
		
		if(isset($data['rental-terms.utilities-included.landlord-pays-heat'])){
			$return['data']['rental-terms.utilities-included.landlord-pays-heat'] = $data['rental-terms.utilities-included.landlord-pays-heat'] ? 'yes' : 'no';
		}
		
		if(isset($data['rental-terms.utilities-included.landlord-pays-hotwater'])){
			$return['data']['rental-terms.utilities-included.landlord-pays-hotwater'] = $data['rental-terms.utilities-included.landlord-pays-hotwater'] ? 'yes' : 'no';
		}	
		
		if(isset($data['rental-terms.utilities-included.landlord-pays-satellite'])){
			$return['data']['rental-terms.utilities-included.landlord-pays-satellite'] = $data['rental-terms.utilities-included.landlord-pays-satellite'] ? 'yes' : 'no';
		}	
			
		if(isset($data['rental-terms.utilities-included.landlord-pays-sewer'])){
			$return['data']['rental-terms.utilities-included.landlord-pays-sewer'] = $data['rental-terms.utilities-included.landlord-pays-sewer'] ? 'yes' : 'no';
		}	
		
		if(isset($data['rental-terms.utilities-included.landlord-pays-telephone'])){
			$return['data']['rental-terms.utilities-included.landlord-pays-telephone'] = $data['rental-terms.utilities-included.landlord-pays-telephone'] ? 'yes' : 'no';
		}
			
		if(isset($data['rental-terms.utilities-included.landlord-pays-trash'])){
			$return['data']['rental-terms.utilities-included.landlord-pays-trash'] = $data['rental-terms.utilities-included.landlord-pays-trash'] ? 'yes' : 'no';
		}	
		
		if(isset($data['rental-terms.utilities-included.landlord-pays-water'])){
			$return['data']['rental-terms.utilities-included.landlord-pays-water'] = $data['rental-terms.utilities-included.landlord-pays-water'] ? 'yes' : 'no';
		}
				
		if(isset($data['rental-terms.utilities-included.landlord-utilities-portion-included'])){
			$return['data']['rental-terms.utilities-included.landlord-utilities-portion-included'] = str_replace($this->search, $this->replace, $data['rental-terms.utilities-included.landlord-utilities-portion-included']);
		}
	
		if(isset($data['rental-terms.utilities-included.utilities-comment'])){
			if(!empty($data['rental-terms.utilities-included.utilities-comment'])){
				$return['data']['rental-terms.utilities-included.utilities-comment'] = explode('|', $data['rental-terms.utilities-included.utilities-comment']);
				foreach($return['data']['rental-terms.utilities-included.utilities-comment'] as $key=>$value){
					$return['data']['rental-terms.utilities-included.utilities-comment'][$key] = str_replace($this->search, $this->replace, $value);
				}
			}
		}		
			
		if(isset($data['rental-terms.property-manager-on-site'])){
			$return['data']['rental-terms.property-manager-on-site'] = $data['rental-terms.property-manager-on-site'] ? 'yes' : 'no';
		}
		
		if(isset($data['rental-terms.rent-control'])){
			$return['data']['rental-terms.rent-control'] = $data['rental-terms.rent-control'] ? 'yes' : 'no';
		}
		
		if(isset($data['rental-terms.subletting-allowed'])){
			$return['data']['rental-terms.subletting-allowed'] = $data['rental-terms.subletting-allowed'] ? 'yes' : 'no';
		}
			
		if(isset($data['pictures.picture-url'])){
			if(!empty($data['pictures.picture-url'])){
				$return['data']['pictures.picture-url'] = explode('|', $data['pictures.picture-url']);
				foreach($return['data']['pictures.picture-url'] as $key=>$value){
					$return['data']['pictures'][$key]['picture-url'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['pictures.picture-caption'])){
			if(!empty($data['pictures.picture-caption'])){
				$return['data']['pictures.picture-caption'] = explode('|', $data['pictures.picture-caption']);
				foreach($return['data']['pictures.picture-caption'] as $key=>$value){
					$return['data']['pictures'][$key]['picture-caption'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['pictures.picture-description'])){
			if(!empty($data['pictures.picture-description'])){
				$return['data']['pictures.picture-description'] = explode('|', $data['pictures.picture-description']);
				foreach($return['data']['pictures.picture-description'] as $key=>$value){
					$return['data']['pictures'][$key]['picture-description'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['pictures.picture-seq-number'])){
			if(!empty($data['pictures.picture-seq-number'])){
				$return['data']['pictures.picture-seq-number'] = explode('|', $data['pictures.picture-seq-number']);
				foreach($return['data']['pictures.picture-seq-number'] as $key=>$value){
					$return['data']['pictures'][$key]['picture-seq-number'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
			
		if(isset($data['virtual-tours.virtual-tour-url'])){
			if(!empty($data['virtual-tours.virtual-tour-url'])){
				$return['data']['virtual-tours.virtual-tour-url'] = explode('|', $data['virtual-tours.virtual-tour-url']);
				foreach($return['data']['virtual-tours.virtual-tour-url'] as $key=>$value){
					$return['data']['virtual-tours'][$key]['virtual-tour-url'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}	
		
		if(isset($data['virtual-tours.virtual-tour-caption'])){
			if(!empty($data['virtual-tours.virtual-tour-caption'])){
				$return['data']['virtual-tours.virtual-tour-caption'] = explode('|', $data['virtual-tours.virtual-tour-caption']);
				foreach($return['data']['virtual-tours.virtual-tour-caption'] as $key=>$value){
					$return['data']['virtual-tours'][$key]['virtual-tour-caption'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['virtual-tours.virtual-tour-description'])){
			if(!empty($data['virtual-tours.virtual-tour-description'])){
				$return['data']['virtual-tours.virtual-tour-description'] = explode('|', $data['virtual-tours.virtual-tour-description']);
				foreach($return['data']['virtual-tours.virtual-tour-description'] as $key=>$value){
					$return['data']['virtual-tours'][$key]['virtual-tour-description'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['virtual-tours.virtual-tour-seq-number'])){
			if(!empty($data['virtual-tours.virtual-tour-seq-number'])){
				$return['data']['virtual-tours.virtual-tour-seq-number'] = explode('|', $data['virtual-tours.virtual-tour-seq-number']);
				foreach($return['data']['virtual-tours.virtual-tour-seq-number'] as $key=>$value){
					$return['data']['virtual-tours'][$key]['virtual-tour-seq-number'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
			
		if(isset($data['videos.video-url'])){
			if(!empty($data['videos.video-url'])){
				$return['data']['videos.video-url'] = explode('|', $data['videos.video-url']);
				foreach($return['data']['videos.video-url'] as $key=>$value){
					$return['data']['videos'][$key]['video-url'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['videos.video-caption'])){
			if(!empty($data['videos.video-caption'])){
				$return['data']['videos.video-caption'] = explode('|', $data['videos.video-caption']);
				foreach($return['data']['videos.video-caption'] as $key=>$value){
					$return['data']['videos'][$key]['video-caption'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['videos.video-description'])){
			if(!empty($data['videos.video-description'])){
				$return['data']['videos.video-description'] = explode('|', $data['videos.video-description']);
				foreach($return['data']['videos.video-description'] as $key=>$value){
					$return['data']['videos'][$key]['video-description'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['videos.video-seq-number'])){
			if(!empty($data['videos.video-seq-number'])){
				$return['data']['videos.video-seq-number'] = explode('|', $data['videos.video-seq-number']);
				foreach($return['data']['videos.video-seq-number'] as $key=>$value){
					$return['data']['videos'][$key]['video-seq-number'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['floorplan-layouts.floorplan-layout-url'])){
			if(!empty($data['floorplan-layouts.floorplan-layout-url'])){
				$return['data']['floorplan-layouts.floorplan-layout-url'] = explode('|', $data['floorplan-layouts.floorplan-layout-url']);
				foreach($return['data']['floorplan-layouts.floorplan-layout-url'] as $key=>$value){
					$return['data']['floorplan-layouts'][$key]['floorplan-layout-url'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
			
		if(isset($data['floorplan-layouts.floorplan-layout-caption'])){
			if(!empty($data['floorplan-layouts.floorplan-layout-caption'])){
				$return['data']['floorplan-layouts.floorplan-layout-caption'] = explode('|', $data['floorplan-layouts.floorplan-layout-caption']);
				foreach($return['data']['floorplan-layouts.floorplan-layout-caption'] as $key=>$value){
					$return['data']['floorplan-layouts'][$key]['floorplan-layout-caption'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['floorplan-layouts.floorplan-layout-description'])){
			if(!empty($data['floorplan-layouts.floorplan-layout-description'])){
				$return['data']['floorplan-layouts.floorplan-layout-description'] = explode('|', $data['floorplan-layouts.floorplan-layout-description']);
				foreach($return['data']['floorplan-layouts.floorplan-layout-description'] as $key=>$value){
					$return['data']['floorplan-layouts'][$key]['floorplan-layout-description'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}	
		
		if(isset($data['floorplan-layouts.floorplan-layout-seq-number'])){
			if(!empty($data['floorplan-layouts.floorplan-layout-seq-number'])){
				$return['data']['floorplan-layouts.floorplan-layout-seq-number'] = explode('|', $data['floorplan-layouts.floorplan-layout-seq-number']);
				foreach($return['data']['floorplan-layouts.floorplan-layout-seq-number'] as $key=>$value){
					$return['data']['floorplan-layouts'][$key]['floorplan-layout-seq-number'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['agent.agent-name'])){
			$return['data']['agent.agent-name'] = str_replace($this->search, $this->replace, $data['agent.agent-name']);
		}
				
		if(isset($data['agent.agent-phone'])){
			$return['data']['agent.agent-phone'] = str_replace($this->search, $this->replace, $data['agent.agent-phone']);
		}
		
		if(isset($data['agent.agent-email'])){
			$return['data']['agent.agent-email'] = str_replace($this->search, $this->replace, $data['agent.agent-email']);
		}	

		if(isset($data['agent.agent-alternate-email'])){
			$return['data']['agent.agent-alternate-email'] = str_replace($this->search, $this->replace, $data['agent.agent-alternate-email']);
		}	
		
		if(isset($data['agent.agent-picture-url'])){
			$return['data']['agent.agent-picture-url'] = str_replace($this->search, $this->replace, $data['agent.agent-picture-url']);
		}
		
		if(isset($data['agent.agent-id'])){
			$return['data']['agent.agent-id'] = intval($data['agent.agent-id']);
		}		
			
		if(isset($data['brokerage.brokerage-name'])){
			$return['data']['brokerage.brokerage-name'] = str_replace($this->search, $this->replace, $data['brokerage.brokerage-name']);
		}
		
		if(isset($data['brokerage.brokerage-broker-name'])){
			$return['data']['brokerage.brokerage-broker-name'] = str_replace($this->search, $this->replace, $data['brokerage.brokerage-broker-name']);
		}
		
		if(isset($data['brokerage.brokerage-id'])){
			$return['data']['brokerage.brokerage-id'] = intval($data['brokerage.brokerage-id']);
		}	
		
		if(isset($data['brokerage.brokerage-mls-code'])){
			$return['data']['brokerage.brokerage-mls-code'] = str_replace($this->search, $this->replace, $data['brokerage.brokerage-mls-code']);
		}	
		
		if(isset($data['brokerage.brokerage-phone'])){
			$return['data']['brokerage.brokerage-phone'] = str_replace($this->search, $this->replace, $data['brokerage.brokerage-phone']);
		}
			
		if(isset($data['brokerage.brokerage-email'])){
			$return['data']['brokerage.brokerage-email'] = str_replace($this->search, $this->replace, $data['brokerage.brokerage-email']);
		}	
		
		if(isset($data['brokerage.brokerage-website'])){
			$return['data']['brokerage.brokerage-website'] = str_replace($this->search, $this->replace, $data['brokerage.brokerage-website']);
		}
		
		if(isset($data['brokerage.brokerage-logo-url'])){
			$return['data']['brokerage.brokerage-logo-url'] = str_replace($this->search, $this->replace, $data['brokerage.brokerage-logo-url']);
		}
		
		if(isset($data['brokerage.brokerage-address.brokerage-street-address'])){
			$return['data']['brokerage.brokerage-address.brokerage-street-address'] = str_replace($this->search, $this->replace, $data['brokerage.brokerage-address.brokerage-street-address']);
		}	
		
		if(isset($data['brokerage.brokerage-address.brokerage-city-name'])){
			$return['data']['brokerage.brokerage-address.brokerage-city-name'] = str_replace($this->search, $this->replace, $data['brokerage.brokerage-address.brokerage-city-name']);
		}	
			
		if(isset($data['brokerage.brokerage-address.brokerage-zipcode'])){
			$return['data']['brokerage.brokerage-address.brokerage-zipcode'] = str_replace($this->search, $this->replace, $data['brokerage.brokerage-address.brokerage-zipcode']);
		}
			
		if(isset($data['brokerage.brokerage-address.brokerage-state-code'])){
			$return['data']['brokerage.brokerage-address.brokerage-state-code'] = str_replace($this->search, $this->replace, $data['brokerage.brokerage-address.brokerage-state-code']);
		}	
		
		if(isset($data['office.office-name'])){
			$return['data']['office.office-name'] = str_replace($this->search, $this->replace, $data['office.office-name']);
		}	

		if(isset($data['office.office-id'])){
			$return['data']['office.office-id'] = intval($data['office.office-id']);
		}		
			
		if(isset($data['office.office-mls-code'])){
			$return['data']['office.office-mls-code'] = str_replace($this->search, $this->replace, $data['office.office-mls-code']);
		}
				
		if(isset($data['office.office-broker-id'])){
			$return['data']['office.office-broker-id'] = intval($data['office.office-broker-id']);
		}	
		
		if(isset($data['office.office-phone'])){
			$return['data']['office.office-phone'] = str_replace($this->search, $this->replace, $data['office.office-phone']);
		}
		
		if(isset($data['office.office-email'])){
			$return['data']['office.office-email'] = str_replace($this->search, $this->replace, $data['office.office-email']);
		}	
			
		if(isset($data['office.office-website'])){
			$return['data']['office.office-website'] = str_replace($this->search, $this->replace, $data['office.office-website']);
		}
		
		if(isset($data['franchise.franchise-name'])){
			$return['data']['franchise.franchise-name'] = str_replace($this->search, $this->replace, $data['franchise.franchise-name']);
		}	
		
		if(isset($data['franchise.franchise-phone'])){
			$return['data']['franchise.franchise-phone'] = str_replace($this->search, $this->replace, $data['franchise.franchise-phone']);
		}

		if(isset($data['franchise.franchise-email'])){
			$return['data']['franchise.franchise-email'] = str_replace($this->search, $this->replace, $data['franchise.franchise-email']);
		}
		
		if(isset($data['franchise.franchise-website'])){
			$return['data']['franchise.franchise-website'] = str_replace($this->search, $this->replace, $data['franchise.franchise-website']);
		}	
			
		if(isset($data['franchise.franchise-logo-url'])){
			$return['data']['franchise.franchise-logo-url'] = str_replace($this->search, $this->replace, $data['franchise.franchise-logo-url']);
		}	
		
		if(isset($data['builder.builder-id'])){
			$return['data']['builder.builder-id'] = intval($data['builder.builder-id']);
		}
		
		if(isset($data['builder.builder-name'])){
			$return['data']['builder.builder-name'] = str_replace($this->search, $this->replace, $data['builder.builder-name']);
		}	
		
		if(isset($data['builder.builder-phone'])){
			$return['data']['builder.builder-phone'] = str_replace($this->search, $this->replace, $data['builder.builder-phone']);
		}		
		
		if(isset($data['builder.builder-email'])){
			$return['data']['builder.builder-email'] = str_replace($this->search, $this->replace, $data['builder.builder-email']);
		}
			
		if(isset($data['builder.builder-lead-email'])){
			$return['data']['builder.builder-lead-email'] = str_replace($this->search, $this->replace, $data['builder.builder-lead-email']);
		}	
		
		if(isset($data['builder.builder-website'])){
			$return['data']['builder.builder-website'] = str_replace($this->search, $this->replace, $data['builder.builder-website']);
		}
			
		if(isset($data['builder.builder-logo-url'])){
			$return['data']['builder.builder-logo-url'] = str_replace($this->search, $this->replace, $data['builder.builder-logo-url']);
		}		
				
		if(isset($data['builder.builder-address.builder-street-address'])){
			$return['data']['builder.builder-address.builder-street-address'] = str_replace($this->search, $this->replace, $data['builder.builder-address.builder-street-address']);
		}
			
		if(isset($data['builder.builder-address.builder-city-name'])){
			$return['data']['builder.builder-address.builder-city-name'] = str_replace($this->search, $this->replace, $data['builder.builder-address.builder-city-name']);
		}	
		
		if(isset($data['builder.builder-address.builder-zipcode'])){
			$return['data']['builder.builder-address.builder-zipcode'] = str_replace($this->search, $this->replace, $data['builder.builder-address.builder-zipcode']);
		}
			
		if(isset($data['builder.builder-address.builder-state-code'])){
			$return['data']['builder.builder-address.builder-state-code'] = str_replace($this->search, $this->replace, $data['builder.builder-address.builder-state-code']);
		}
		
		if(isset($data['property-manager.property-manager-name'])){
			$return['data']['property-manager.property-manager-name'] = str_replace($this->search, $this->replace, $data['property-manager.property-manager-name']);
		}
		
		if(isset($data['property-manager.property-management-company-name'])){
			$return['data']['property-manager.property-management-company-name'] = str_replace($this->search, $this->replace, $data['property-manager.property-management-company-name']);
		}
		
		if(isset($data['property-manager.property-manager-office-hours.day-of-the-week'])){
			if(!empty($data['property-manager.property-manager-office-hours.day-of-the-week'])){
				$return['data']['property-manager.property-manager-office-hours.day-of-the-week'] = explode('|', $data['property-manager.property-manager-office-hours.day-of-the-week']);
				foreach($return['data']['property-manager.property-manager-office-hours.day-of-the-week'] as $key=>$value){
					if(in_array($value, $this->days_of_the_week)){
						$return['data']['property-manager.property-manager-office-hours'][$key]['day-of-the-week'] = str_replace($this->search, $this->replace, $value);
					}
				}
			}
		}
		
		if(isset($data['property-manager.property-manager-office-hours.office-start-time'])){
			if(!empty($data['property-manager.property-manager-office-hours.office-start-time'])){
				$return['data']['property-manager.property-manager-office-hours.office-start-time'] = explode('|', $data['property-manager.property-manager-office-hours.office-start-time']);
				foreach($return['data']['property-manager.property-manager-office-hours.office-start-time'] as $key=>$value){
					$return['data']['property-manager.property-manager-office-hours'][$key]['office-start-time'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['property-manager.property-manager-office-hours.office-end-time'])){
			if(!empty($data['property-manager.property-manager-office-hours.office-end-time'])){
				$return['data']['property-manager.property-manager-office-hours.office-end-time'] = explode('|', $data['property-manager.property-manager-office-hours.office-end-time']);
				foreach($return['data']['property-manager.property-manager-office-hours.office-end-time'] as $key=>$value){
					$return['data']['property-manager.property-manager-office-hours'][$key]['office-end-time'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['property-manager.property-manager-office-hours.comment'])){
			if(!empty($data['property-manager.property-manager-office-hours.comment'])){
				$return['data']['property-manager.property-manager-office-hours.comment'] = explode('|', $data['property-manager.property-manager-office-hours.comment']);
				foreach($return['data']['property-manager.property-manager-office-hours.comment'] as $key=>$value){
					$return['data']['property-manager.property-manager-office-hours'][$key]['comment'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['property-manager.property-manager-phone'])){
			$return['data']['property-manager.property-manager-phone'] = str_replace($this->search, $this->replace, $data['property-manager.property-manager-phone']);
		}	
		
		if(isset($data['property-manager.property-manager-email'])){
			$return['data']['property-manager.property-manager-email'] = str_replace($this->search, $this->replace, $data['property-manager.property-manager-email']);
		}
		
		if(isset($data['property-manager.property-manager-lead-email'])){
			$return['data']['property-manager.property-manager-lead-email'] = str_replace($this->search, $this->replace, $data['property-manager.property-manager-lead-email']);
		}
		
		if(isset($data['property-manager.property-manager-website'])){
			$return['data']['property-manager.property-manager-website'] = str_replace($this->search, $this->replace, $data['property-manager.property-manager-website']);
		}	
		
		if(isset($data['property-manager.property-manager-logo-url'])){
			$return['data']['property-manager.property-manager-logo-url'] = str_replace($this->search, $this->replace, $data['property-manager.property-manager-logo-url']);
		}	
		
		if(isset($data['community.community-building'])){
			$return['data']['community.community-building'] = str_replace($this->search, $this->replace, $data['community.community-building']);
		}

		if(isset($data['community.community-id'])){
			$return['data']['community.community-id'] = intval($data['community.community-id']);
		}
		
		if(isset($data['community.community-name'])){
			$return['data']['community.community-name'] = str_replace($this->search, $this->replace, $data['community.community-name']);
		}		
				
		if(isset($data['community.community-phone'])){
			$return['data']['community.community-phone'] = str_replace($this->search, $this->replace, $data['community.community-phone']);
		}
		
		if(isset($data['community.community-email'])){
			$return['data']['community.community-email'] = str_replace($this->search, $this->replace, $data['community.community-email']);
		}
		
		if(isset($data['community.community-fax'])){
			$return['data']['community.community-fax'] = str_replace($this->search, $this->replace, $data['community.community-fax']);
		}
			
		if(isset($data['community.community-description'])){
			$return['data']['community.community-description'] = str_replace($this->search, $this->replace, $data['community.community-description']);
		}	
				
		if(isset($data['community.community-website'])){
			$return['data']['community.community-website'] = str_replace($this->search, $this->replace, $data['community.community-website']);
		}
		
		if(isset($data['community.community-address.community-street-address'])){
			$return['data']['community.community-address.community-street-address'] = str_replace($this->search, $this->replace, $data['community.community-address.community-street-address']);
		}
		
		if(isset($data['community.community-address.community-city-name'])){
			$return['data']['community.community-address.community-city-name'] = str_replace($this->search, $this->replace, $data['community.community-address.community-city-name']);
		}
			
		if(isset($data['community.community-address.community-zipcode'])){
			$return['data']['community.community-address.community-zipcode'] = str_replace($this->search, $this->replace, $data['community.community-address.community-zipcode']);
		}		
		
		if(isset($data['community.community-address.community-state-code'])){
			$return['data']['community.community-address.community-state-code'] = str_replace($this->search, $this->replace, $data['community.community-address.community-state-code']);
		}	
		
		if(isset($data['community.building-amenities'])){
			if(!empty($data['community.building-amenities'])){
				$return['data']['community.building-amenities'] = explode('|', $data['community.building-amenities']);
				foreach($return['data']['community.building-amenities'] as $key=>$value){
					$return['data']['community.building-amenities'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['community.community-amenities'])){
			if(!empty($data['community.community-amenities'])){
				$return['data']['community.community-amenities'] = explode('|', $data['community.community-amenities']);
				foreach($return['data']['community.community-amenities'] as $key=>$value){
					$return['data']['community.community-amenities'][$key] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['plan.plan-id'])){
			$return['data']['plan.plan-id'] = intval($data['plan.plan-id']);
		}			
		
		if(isset($data['plan.plan-name'])){
			$return['data']['plan.plan-name'] = str_replace($this->search, $this->replace, $data['plan.plan-name']);
		}
		
		if(isset($data['plan.plan-type'])){
			$return['data']['plan.plan-type'] = str_replace($this->search, $this->replace, $data['plan.plan-type']);
		}		
			
		if(isset($data['plan.plan-base-price'])){
			$return['data']['plan.plan-base-price'] = $data['plan.plan-type'] ? 'yes' : 'no';
		}	
			
		if(isset($data['spec.is-spec-home'])){
			$return['data']['spec.is-spec-home'] = $data['spec.is-spec-home'] ? 'yes' : 'no';
		}	
		
		if(isset($data['spec.spec-id'])){
			$return['data']['spec.spec-id'] = intval($data['spec.spec-id']);
		}	

		if(isset($data['open-home.period1-date'])){
			$return['data']['open-home.period1-date'] = str_replace($this->search, $this->replace, $data['open-home.period1-date']);
		}
		
		if(isset($data['open-home.period1-start-time'])){
			$return['data']['open-home.period1-start-time'] = str_replace($this->search, $this->replace, $data['open-home.period1-start-time']);
		}
			
		if(isset($data['open-home.period1-end-time'])){
			$return['data']['open-home.period1-end-time'] = str_replace($this->search, $this->replace, $data['open-home.period1-end-time']);
		}
		
		if(isset($data['open-home.period1-details'])){
			$return['data']['open-home.period1-details'] = str_replace($this->search, $this->replace, $data['open-home.period1-details']);
		}		
			
		if(isset($data['open-home.period2-date'])){
			$return['data']['open-home.period2-date'] = str_replace($this->search, $this->replace, $data['open-home.period2-date']);
		}	
			
		if(isset($data['open-home.period2-start-time'])){
			$return['data']['open-home.period2-start-time'] = str_replace($this->search, $this->replace, $data['open-home.period2-start-time']);
		}		
		
		if(isset($data['open-home.period2-end-time'])){
			$return['data']['open-home.period2-end-time'] = str_replace($this->search, $this->replace, $data['open-home.period2-end-time']);
		}
		
		if(isset($data['open-home.period2-details'])){
			$return['data']['open-home.period2-details'] = str_replace($this->search, $this->replace, $data['open-home.period2-details']);
		}
			
		if(isset($data['open-home.period3-date'])){
			$return['data']['open-home.period3-date'] = str_replace($this->search, $this->replace, $data['open-home.period3-date']);
		}
		
		if(isset($data['open-home.period3-start-time'])){
			$return['data']['open-home.period3-start-time'] = str_replace($this->search, $this->replace, $data['open-home.period3-start-time']);
		}		
		
		if(isset($data['open-home.period3-end-time'])){
			$return['data']['open-home.period3-end-time'] = str_replace($this->search, $this->replace, $data['open-home.period3-end-time']);
		}	
		
		if(isset($data['open-home.period3-details'])){
			$return['data']['open-home.period3-details'] = str_replace($this->search, $this->replace, $data['open-home.period3-details']);
		}
			
		if(isset($data['open-home.period4-date'])){
			$return['data']['open-home.period4-date'] = str_replace($this->search, $this->replace, $data['open-home.period4-date']);
		}		
		
		if(isset($data['open-home.period4-start-time'])){
			$return['data']['open-home.period4-start-time'] = str_replace($this->search, $this->replace, $data['open-home.period4-start-time']);
		}	
		
		if(isset($data['open-home.period4-end-time'])){
			$return['data']['open-home.period4-end-time'] = str_replace($this->search, $this->replace, $data['open-home.period4-end-time']);
		}
		
		if(isset($data['open-home.period4-details'])){
			$return['data']['open-home.period4-details'] = str_replace($this->search, $this->replace, $data['open-home.period4-details']);
		}	
		
		if(isset($data['taxes.tax-type'])){
			if(!empty($data['taxes.tax-type'])){
				$return['data']['taxes.tax-type'] = explode('|', $data['taxes.tax-type']);
				foreach($return['data']['taxes.tax-type'] as $key=>$value){
					$return['data']['taxes'][$key]['tax-type'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['taxes.tax-year'])){
			if(!empty($data['taxes.tax-year'])){
				$return['data']['taxes.tax-year'] = explode('|', $data['taxes.tax-year']);
				foreach($return['data']['taxes.tax-year'] as $key=>$value){
					if(strlen($value) == 4){
						$return['data']['taxes'][$key]['tax-year'] = intval($value);
					}
				}
			}
		}
		
		if(isset($data['taxes.tax-amount'])){
			if(!empty($data['taxes.tax-amount'])){
				$return['data']['taxes.tax-amount'] = explode('|', $data['taxes.tax-amount']);
				foreach($return['data']['taxes.tax-amount'] as $key=>$value){
					$return['data']['taxes'][$key]['tax-amount'] = floatval($value);
				}
			}
		}
		
		if(isset($data['taxes.description'])){
			if(!empty($data['taxes.description'])){
				$return['data']['taxes.description'] = explode('|', $data['taxes.description']);
				foreach($return['data']['taxes.description'] as $key=>$value){
					$return['data']['taxes'][$key]['description'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
			
		if(isset($data['hoa-fees.hoa-fee'])){
			$return['data']['hoa-fees.hoa-fee'] = floatval($data['hoa-fees.hoa-fee']);
		}
			
		if(isset($data['hoa-fees.hoa-period'])){
			if(in_array($data['hoa-fees.hoa-period'], $this->hoa_periods)){
				$return['data']['hoa-fees.hoa-period'] = $data['hoa-fees.hoa-period'];
			}
		}	
		
		if(isset($data['hoa-fees.hoa-description'])){
			$return['data']['hoa-fees.hoa-description'] = str_replace($this->search, $this->replace, $data['hoa-fees.hoa-description']);
		}	
				
		if(isset($data['additional-fees.fee-type'])){
			if(!empty($data['additional-fees.fee-type'])){
				$return['data']['additional-fees.fee-type'] = explode('|', $data['additional-fees.fee-type']);
				foreach($return['data']['additional-fees.fee-type'] as $key=>$value){
					$return['data']['additional-fees'][$key]['fee-type'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['additional-fees.fee-amount'])){
			if(!empty($data['additional-fees.fee-amount'])){
				$return['data']['additional-fees.fee-amount'] = explode('|', $data['additional-fees.fee-amount']);
				foreach($return['data']['additional-fees.fee-amount'] as $key=>$value){
					$return['data']['additional-fees'][$key]['fee-amount'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['additional-fees.fee-period'])){
			if(!empty($data['additional-fees.fee-period'])){
				$return['data']['additional-fees.fee-period'] = explode('|', $data['additional-fees.fee-period']);
				foreach($return['data']['additional-fees.fee-period'] as $key=>$value){
					$return['data']['additional-fees'][$key]['fee-period'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['additional-fees.fee-description'])){
			if(!empty($data['additional-fees.fee-description'])){
				$return['data']['additional-fees.fee-description'] = explode('|', $data['additional-fees.fee-description']);
				foreach($return['data']['additional-fees.fee-description'] as $key=>$value){
					$return['data']['additional-fees'][$key]['fee-description'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['schools.school-district.elementary'])){
			$return['data']['schools.school-district.elementary'] = str_replace($this->search, $this->replace, $data['schools.school-district.elementary']);
		}
		
		if(isset($data['schools.school-district.middle'])){
			$return['data']['schools.school-district.middle'] = str_replace($this->search, $this->replace, $data['schools.school-district.middle']);
		}
		
		if(isset($data['schools.school-district.juniorhigh'])){
			$return['data']['schools.school-district.juniorhigh'] = str_replace($this->search, $this->replace, $data['schools.school-district.juniorhigh']);
		}	
		
		if(isset($data['schools.school-district.high'])){
			$return['data']['schools.school-district.high'] = str_replace($this->search, $this->replace, $data['schools.school-district.high']);
		}	
		
		if(isset($data['schools.school-district.district-name'])){
			$return['data']['schools.school-district.district-name'] = str_replace($this->search, $this->replace, $data['schools.school-district.district-name']);
		}		
		
		if(isset($data['schools.school-district.district-website'])){
			$return['data']['schools.school-district.district-website'] = str_replace($this->search, $this->replace, $data['schools.school-district.district-website']);
		}		
		
		if(isset($data['schools.school-district.district-phone-number'])){
			$return['data']['schools.school-district.district-phone-number'] = str_replace($this->search, $this->replace, $data['schools.school-district.district-phone-number']);
		}	
		
		if(isset($data['detailed-characteristics.appliances.has-washer'])){
			$return['data']['detailed-characteristics.appliances.has-washer'] = $data['detailed-characteristics.appliances.has-washer'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.appliances.has-dryer'])){
			$return['data']['detailed-characteristics.appliances.has-dryer'] = $data['detailed-characteristics.appliances.has-dryer'] ? 'yes' : 'no';
		}
			
		if(isset($data['detailed-characteristics.appliances.has-dishwasher'])){
			$return['data']['detailed-characteristics.appliances.has-dishwasher'] = $data['detailed-characteristics.appliances.has-dishwasher'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.appliances.has-refrigerator'])){
			$return['data']['detailed-characteristics.appliances.has-refrigerator'] = $data['detailed-characteristics.appliances.has-refrigerator'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.appliances.has-disposal'])){
			$return['data']['detailed-characteristics.appliances.has-disposal'] = $data['detailed-characteristics.appliances.has-disposal'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.appliances.has-microwave'])){
			$return['data']['detailed-characteristics.appliances.has-microwave'] = $data['detailed-characteristics.appliances.has-microwave'] ? 'yes' : 'no';
		}
				
		if(isset($data['detailed-characteristics.appliances.range-type'])){
			if(in_array($data['detailed-characteristics.appliances.range-type'], $this->range_types)){
				$return['data']['detailed-characteristics.appliances.range-type'] = $data['detailed-characteristics.appliances.range-type'];
			}
		}
		
		if(isset($data['detailed-characteristics.appliances.additional-appliances.additional-appliance-name'])){
			if(!empty($data['detailed-characteristics.appliances.additional-appliances.additional-appliance-name'])){
				$return['data']['detailed-characteristics.appliances.additional-appliances.additional-appliance-name'] = explode('|', $data['detailed-characteristics.appliances.additional-appliances.additional-appliance-name']);
				foreach($return['data']['detailed-characteristics.appliances.additional-appliances.additional-appliance-name'] as $key=>$value){
					$return['data']['detailed-characteristics.appliances.additional-appliances'][$key]['additional-appliance-name'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['detailed-characteristics.appliances.additional-appliances.additional-appliance-description'])){
			if(!empty($data['detailed-characteristics.appliances.additional-appliances.additional-appliance-description'])){
				$return['data']['detailed-characteristics.appliances.additional-appliances.additional-appliance-description'] = explode('|', $data['detailed-characteristics.appliances.additional-appliances.additional-appliance-description']);
				foreach($return['data']['detailed-characteristics.appliances.additional-appliances.additional-appliance-description'] as $key=>$value){
					$return['data']['detailed-characteristics.appliances.additional-appliances'][$key]['additional-appliance-description'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
			
		if(isset($data['detailed-characteristics.appliances.appliances-comments'])){
			$return['data']['detailed-characteristics.appliances.appliances-comments'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.appliances.appliances-comments']);
		}	
		
		if(isset($data['detailed-characteristics.cooling-systems.has-air-conditioning'])){
			$return['data']['detailed-characteristics.cooling-systems.has-air-conditioning'] = $data['detailed-characteristics.cooling-systems.has-air-conditioning'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.cooling-systems.has-ceiling-fan'])){
			$return['data']['detailed-characteristics.cooling-systems.has-ceiling-fan'] = $data['detailed-characteristics.cooling-systems.has-ceiling-fan'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.cooling-systems.other-cooling'])){
			$return['data']['detailed-characteristics.cooling-systems.other-cooling'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.cooling-systems.other-cooling']);
		}	
			
		if(isset($data['detailed-characteristics.heating-systems.has-fireplace'])){
			$return['data']['detailed-characteristics.heating-systems.has-fireplace'] = $data['detailed-characteristics.heating-systems.has-fireplace'] ? 'yes' : 'no';
		}	
	
		if(isset($data['detailed-characteristics.heating-systems.fireplace-type'])){
			if(in_array($data['detailed-characteristics.heating-systems.fireplace-type'], $this->fireplace_types)){
				$return['data']['detailed-characteristics.heating-systems.fireplace-type'] = $data['detailed-characteristics.heating-systems.fireplace-type'];
			}
		}	
		
		if(isset($data['detailed-characteristics.heating-systems.heating-system'])){
			if(in_array($data['detailed-characteristics.heating-systems.heating-system'], $this->heating_systems)){
				$return['data']['detailed-characteristics.heating-systems.heating-system'] = $data['detailed-characteristics.heating-systems.heating-system'];
			}
		}
		
		if(isset($data['detailed-characteristics.heating-systems.heating-fuel'])){
			if(in_array($data['detailed-characteristics.heating-systems.heating-fuel'], $this->heating_fuels)){
				$return['data']['detailed-characteristics.heating-systems.heating-fuel'] = $data['detailed-characteristics.heating-systems.heating-fuel'];
			}
		}		
			
		if(isset($data['detailed-characteristics.floor-coverings'])){
			$return['data']['detailed-characteristics.floor-coverings'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.floor-coverings']);
		}		
		
		if(isset($data['detailed-characteristics.total-unit-parking-spaces'])){
			$return['data']['detailed-characteristics.total-unit-parking-spaces'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.total-unit-parking-spaces']);
		}		
		
		if(isset($data['detailed-characteristics.has-garage'])){
			$return['data']['detailed-characteristics.has-garage'] = $data['detailed-characteristics.has-garage'] ? 'yes' : 'no';
		}
				
		if(isset($data['detailed-characteristics.garage-type'])){
			if(in_array($data['detailed-characteristics.garage-type'], $this->garage_types)){
				$return['data']['detailed-characteristics.garage-type'] = $data['detailed-characteristics.garage-type'];
			}
		}	
		
		if(isset($data['detailed-characteristics.parking-types'])){
			if(in_array($data['detailed-characteristics.parking-types'], $this->parking_types)){
				$return['data']['detailed-characteristics.parking-types'] = $data['detailed-characteristics.parking-types'];
			}
		}	
		
		if(isset($data['detailed-characteristics.has-assigned-parking-space'])){
			$return['data']['detailed-characteristics.has-assigned-parking-space'] = $data['detailed-characteristics.has-assigned-parking-space'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.parking-space-fee'])){
			$return['data']['detailed-characteristics.parking-space-fee'] = floatval($data['detailed-characteristics.parking-space-fee']);
		}
		
		if(isset($data['detailed-characteristics.assigned-parking-space-cost'])){
			$return['data']['detailed-characteristics.assigned-parking-space-cost'] = floatval($data['detailed-characteristics.assigned-parking-space-cost']);
		}
			
		if(isset($data['detailed-characteristics.parking-comment'])){
			$return['data']['detailed-characteristics.parking-comment'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.parking-comment']);
		}	
		
		if(isset($data['detailed-characteristics.foundation-type'])){
			$return['data']['detailed-characteristics.foundation-type'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.foundation-type']);
		}
				
		if(isset($data['detailed-characteristics.roof-type'])){
			$return['data']['detailed-characteristics.roof-type'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.roof-type']);
		}		
		
		if(isset($data['detailed-characteristics.architecture-style'])){
			$return['data']['detailed-characteristics.architecture-style'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.architecture-style']);
		}		
		
		if(isset($data['detailed-characteristics.exterior-type'])){
			$return['data']['detailed-characteristics.exterior-type'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.exterior-type']);
		}	
		
		if(isset($data['detailed-characteristics.room-count'])){
			$return['data']['detailed-characteristics.room-count'] = intval($data['detailed-characteristics.room-count']);
		}	
		
		if(isset($data['detailed-characteristics.rooms.room-type'])){
			if(!empty($data['detailed-characteristics.rooms.room-type'])){
				$return['data']['detailed-characteristics.rooms.room-type'] = explode('|', $data['detailed-characteristics.rooms.room-type']);
				foreach($return['data']['detailed-characteristics.rooms.room-type'] as $key=>$value){
					$return['data']['detailed-characteristics.rooms'][$key]['room-type'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['detailed-characteristics.rooms.room-size'])){
			if(!empty($data['detailed-characteristics.rooms.room-size'])){
				$return['data']['detailed-characteristics.rooms.room-size'] = explode('|', $data['detailed-characteristics.rooms.room-size']);
				foreach($return['data']['detailed-characteristics.rooms.room-size'] as $key=>$value){
					$return['data']['detailed-characteristics.rooms'][$key]['room-size'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}	
		
		if(isset($data['detailed-characteristics.rooms.room-description'])){
			if(!empty($data['detailed-characteristics.rooms.room-description'])){
				$return['data']['detailed-characteristics.rooms.room-description'] = explode('|', $data['detailed-characteristics.rooms.room-description']);
				foreach($return['data']['detailed-characteristics.rooms.room-description'] as $key=>$value){
					$return['data']['detailed-characteristics.rooms'][$key]['room-description'] = str_replace($this->search, $this->replace, $value);
				}
			}
		}
		
		if(isset($data['detailed-characteristics.year-updated'])){
			if(strlen($data['detailed-characteristics.year-updated']) == 4){
				$return['data']['detailed-characteristics.year-updated'] = intval($data['detailed-characteristics.year-updated']);
			}
		}
			
		if(isset($data['detailed-characteristics.total-units-in-building'])){
			$return['data']['detailed-characteristics.total-units-in-building'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.total-units-in-building']);
		}	
		
		if(isset($data['detailed-characteristics.total-floors-in-building'])){
			$return['data']['detailed-characteristics.total-floors-in-building'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.total-floors-in-building']);
		}	
		
		if(isset($data['detailed-characteristics.num-floors-in-unit'])){
			$return['data']['detailed-characteristics.num-floors-in-unit'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.num-floors-in-unit']);
		}	
		
		if(isset($data['detailed-characteristics.has-attic'])){
			$return['data']['detailed-characteristics.has-attic'] = $data['detailed-characteristics.has-attic'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.has-balcony'])){
			$return['data']['detailed-characteristics.has-balcony'] = $data['detailed-characteristics.has-balcony'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.has-barbeque-area'])){
			$return['data']['detailed-characteristics.has-barbeque-area'] = $data['detailed-characteristics.has-barbeque-area'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.has-basement'])){
			$return['data']['detailed-characteristics.has-basement'] = $data['detailed-characteristics.has-basement'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.has-cable-satellite'])){
			$return['data']['detailed-characteristics.has-cable-satellite'] = $data['detailed-characteristics.has-cable-satellite'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.has-courtyard'])){
			$return['data']['detailed-characteristics.has-courtyard'] = $data['detailed-characteristics.has-courtyard'] ? 'yes' : 'no';
		}		
			
		if(isset($data['detailed-characteristics.has-deck'])){
			$return['data']['detailed-characteristics.has-deck'] = $data['detailed-characteristics.has-deck'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.has-disabled-access'])){
			$return['data']['detailed-characteristics.has-disabled-access'] = $data['detailed-characteristics.has-disabled-access'] ? 'yes' : 'no';
		}	
			
		if(isset($data['detailed-characteristics.has-dock'])){
			$return['data']['detailed-characteristics.has-dock'] = $data['detailed-characteristics.has-dock'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.has-doublepane-windows'])){
			$return['data']['detailed-characteristics.has-doublepane-windows'] = $data['detailed-characteristics.has-doublepane-windows'] ? 'yes' : 'no';
		}		
			
		if(isset($data['detailed-characteristics.has-garden'])){
			$return['data']['detailed-characteristics.has-garden'] = $data['detailed-characteristics.has-garden'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.has-gated-entry'])){
			$return['data']['detailed-characteristics.has-gated-entry'] = $data['detailed-characteristics.has-gated-entry'] ? 'yes' : 'no';
		}
				
		if(isset($data['detailed-characteristics.has-greenhouse'])){
			$return['data']['detailed-characteristics.has-greenhouse'] = $data['detailed-characteristics.has-greenhouse'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.has-handrails'])){
			$return['data']['detailed-characteristics.has-handrails'] = $data['detailed-characteristics.has-handrails'] ? 'yes' : 'no';
		}	
				
		if(isset($data['detailed-characteristics.has-hot-tub-spa'])){
			$return['data']['detailed-characteristics.has-hot-tub-spa'] = $data['detailed-characteristics.has-hot-tub-spa'] ? 'yes' : 'no';
		}		
		
		if(isset($data['detailed-characteristics.has-intercom'])){
			$return['data']['detailed-characteristics.has-intercom'] = $data['detailed-characteristics.has-intercom'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.has-jetted-bath-tub'])){
			$return['data']['detailed-characteristics.has-jetted-bath-tub'] = $data['detailed-characteristics.has-jetted-bath-tub'] ? 'yes' : 'no';
		}	
				
		if(isset($data['detailed-characteristics.has-lawn'])){
			$return['data']['detailed-characteristics.has-lawn'] = $data['detailed-characteristics.has-lawn'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.has-mother-in-law'])){
			$return['data']['detailed-characteristics.has-mother-in-law'] = $data['detailed-characteristics.has-mother-in-law'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.has-patio'])){
			$return['data']['detailed-characteristics.has-patio'] = $data['detailed-characteristics.has-patio'] ? 'yes' : 'no';
		}		
		
		if(isset($data['detailed-characteristics.has-pond'])){
			$return['data']['detailed-characteristics.has-pond'] = $data['detailed-characteristics.has-pond'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.has-pool'])){
			$return['data']['detailed-characteristics.has-pool'] = $data['detailed-characteristics.has-pool'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.has-porch'])){
			$return['data']['detailed-characteristics.has-porch'] = $data['detailed-characteristics.has-porch'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.has-private-balcony'])){
			$return['data']['detailed-characteristics.has-private-balcony'] = $data['detailed-characteristics.has-private-balcony'] ? 'yes' : 'no';
		}
			
		if(isset($data['detailed-characteristics.has-private-patio'])){
			$return['data']['detailed-characteristics.has-private-patio'] = $data['detailed-characteristics.has-private-patio'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.has-rv-parking'])){
			$return['data']['detailed-characteristics.has-rv-parking'] = $data['detailed-characteristics.has-rv-parking'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.has-sauna'])){
			$return['data']['detailed-characteristics.has-sauna'] = $data['detailed-characteristics.has-sauna'] ? 'yes' : 'no';
		}	
			
		if(isset($data['detailed-characteristics.has-security-system'])){
			$return['data']['detailed-characteristics.has-security-system'] = $data['detailed-characteristics.has-security-system'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.has-skylight'])){
			$return['data']['detailed-characteristics.has-skylight'] = $data['detailed-characteristics.has-skylight'] ? 'yes' : 'no';
		}	
			
		if(isset($data['detailed-characteristics.has-sportscourt'])){
			$return['data']['detailed-characteristics.has-sportscourt'] = $data['detailed-characteristics.has-sportscourt'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.has-sprinkler-system'])){
			$return['data']['detailed-characteristics.has-sprinkler-system'] = $data['detailed-characteristics.has-sprinkler-system'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.has-terrace'])){
			$return['data']['detailed-characteristics.has-terrace'] = $data['detailed-characteristics.has-terrace'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.has-vaulted-ceiling'])){
			$return['data']['detailed-characteristics.has-vaulted-ceiling'] = $data['detailed-characteristics.has-vaulted-ceiling'] ? 'yes' : 'no';
		}	
			
		if(isset($data['detailed-characteristics.has-view'])){
			$return['data']['detailed-characteristics.has-view'] = $data['detailed-characteristics.has-view'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.has-washer-dryer-hookup'])){
			$return['data']['detailed-characteristics.has-washer-dryer-hookup'] = $data['detailed-characteristics.has-washer-dryer-hookup'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.has-wet-bar'])){
			$return['data']['detailed-characteristics.has-wet-bar'] = $data['detailed-characteristics.has-wet-bar'] ? 'yes' : 'no';
		}	
			
		if(isset($data['detailed-characteristics.has-window-coverings'])){
			$return['data']['detailed-characteristics.has-window-coverings'] = $data['detailed-characteristics.has-window-coverings'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.building-has-concierge'])){
			$return['data']['detailed-characteristics.building-has-concierge'] = $data['detailed-characteristics.building-has-concierge'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.building-has-doorman'])){
			$return['data']['detailed-characteristics.building-has-doorman'] = $data['detailed-characteristics.building-has-doorman'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.building-has-elevator'])){
			$return['data']['detailed-characteristics.building-has-elevator'] = $data['detailed-characteristics.building-has-elevator'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.building-has-fitness-center'])){
			$return['data']['detailed-characteristics.building-has-fitness-center'] = $data['detailed-characteristics.building-has-fitness-center'] ? 'yes' : 'no';
		}	
				
		if(isset($data['detailed-characteristics.building-has-on-site-maintenance'])){
			$return['data']['detailed-characteristics.building-has-on-site-maintenance'] = $data['detailed-characteristics.building-has-on-site-maintenance'] ? 'yes' : 'no';
		}		
		
		if(isset($data['detailed-characteristics.is-waterfront'])){
			$return['data']['detailed-characteristics.is-waterfront'] = $data['detailed-characteristics.is-waterfront'] ? 'yes' : 'no';
		}
		
		if(isset($data['detailed-characteristics.other-amenities'])){
			$return['data']['detailed-characteristics.other-amenities'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.other-amenities']);
		}
		
		if(isset($data['detailed-characteristics.furnished'])){
			$return['data']['detailed-characteristics.furnished'] = $data['detailed-characteristics.furnished'] ? 'yes' : 'no';
		}	
		
		if(isset($data['detailed-characteristics.view-type'])){
			$return['data']['detailed-characteristics.view-type'] = str_replace($this->search, $this->replace, $data['detailed-characteristics.view-type']);
		}
		
		if(isset($data['advertise-with-us.channel'])){
			$return['data']['advertise-with-us.channel'] = str_replace($this->search, $this->replace, $data['advertise-with-us.channel']);
		}
		
		if(isset($data['advertise-with-us.featured'])){
			$return['data']['advertise-with-us.featured'] = $data['advertise-with-us.featured'] ? 'yes' : 'no';
		}	
		
		if(isset($data['advertise-with-us.branded'])){
			$return['data']['advertise-with-us.branded'] = $data['advertise-with-us.branded'] ? 'yes' : 'no';
		}	
		
		if(isset($data['advertise-with-us.branded-logo-url'])){
			$return['data']['advertise-with-us.branded-logo-url'] = str_replace($this->search, $this->replace, $data['advertise-with-us.branded-logo-url']);
		}		

		return $return;
	}
}

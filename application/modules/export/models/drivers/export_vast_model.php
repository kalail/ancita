<?php 

if(!defined("BASEPATH")) exit("No direct script access allowed");

/**
 * Vast xml driver export model
 * 
 * @package PG_RealEstate
 * @subpackage Export
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Export_vast_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 * 
	 * @var array
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 * 
	 * @var array
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Constructor
	 * 
	 * @return Export_vast_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Return default filename
	 * 
	 * @return string
	 */
	public function get_filename(){
		return "export.xml";
	}

	/**
	 * Generate output file
	 * 
	 * @param string $filename output filename
	 * @param array $data export data
	 * @param array $settings driver settings
	 * @return void
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		fputs($f, '<?xml version="1.0" encoding="utf-8">'."\n");
		fputs($f, '<listings>'."\n");
		foreach($data as $i=>$row){
			if(!$i) continue;
			fputs($f, '	<listing>'."\n");
			
			if(isset($row['record_id']) && !empty($row['record_id']))
				fputs($f, '		<record_id>'.$row['record_id'].'</record_id>'."\n");
				
			if(isset($row['title']) && !empty($row['title']))
				fputs($f, '		<title>'.$row['title'].'</title>'."\n");
				
			if(isset($row['url']) && !empty($row['url']))
				fputs($f, '		<url>'.$row['url'].'</url>'."\n");
					
			if(isset($row['category']) && !empty($row['category']))
				fputs($f, '		<category>'.$row['category'].'</category>'."\n");
					
			if(isset($row['subcategory']) && !empty($row['subcategory']))
				fputs($f, '		<subcategory>'.$row['subcategory'].'</subcategory>'."\n");
					
			if(isset($row['image']) && !empty($row['image']))
				fputs($f, '		<image>'.$row['image'].'</image>'."\n");
					
			if(isset($row['address']) && !empty($row['address']))
				fputs($f, '		<address>'.$row['address'].'</address>'."\n");
					
			if(isset($row['city']) && !empty($row['city']))
				fputs($f, '		<city>'.$row['city'].'</city>'."\n");
					
			if(isset($row['state']) && !empty($row['state']))
				fputs($f, '		<state>'.$row['state'].'</state>'."\n");
					
			if(isset($row['zip']) && !empty($row['zip']))
				fputs($f, '		<zip>'.$row['zip'].'</zip>'."\n");
					
			if(isset($row['country']) && !empty($row['country']))
				fputs($f, '		<country>'.$row['country'].'</country>'."\n");
				
			if(isset($row['bedrooms']) && !empty($row['bedrooms']))				
				fputs($f, '		<bedrooms>'.$row['bedrooms'].'</bedrooms>'."\n");
					
			if(isset($row['bathrooms']) && !empty($row['bathrooms']))
				fputs($f, '		<bathrooms>'.$row['bathrooms'].'</bathrooms>'."\n");
					
			if(isset($row['square_footage']) && !empty($row['square_footage']))
				fputs($f, '		<square_footage>'.$row['square_footage'].'</square_footage>'."\n");
					
			if(isset($row['stories']) && !empty($row['stories']))	
				fputs($f, '		<stories>'.$row['stories'].'</stories>'."\n");
					
			if(isset($row['lot_size']) && !empty($row['lot_size']))
				fputs($f, '		<lot_size>'.$row['lot_size'].'</lot_size>'."\n");
					
			if(isset($row['parking_spots']) && !empty($row['parking_spots']))
				fputs($f, '		<parking_spots>'.$row['parking_spots'].'</parking_spots>'."\n");
					
			if(isset($row['year_built']) && !empty($row['year_built']))
				fputs($f, '		<year_built>'.$row['year_built'].'</year_built>'."\n");
					
			if(isset($row['currency']) && !empty($row['currency']))
				fputs($f, '		<currency>'.$row['currency'].'</currency>');
					
			if(isset($row['price']) && !empty($row['price']))
				fputs($f, '		<price>'.$row['price'].'</price>'."\n");
				
			if(isset($row['amenities']) && !empty($row['amenities']))
				fputs($f, '		<amenities>'.$row['amenities'].'</amenities>'."\n");
					
			if(isset($row['description']) && !empty($row['description']))
				fputs($f, '		<description>'.$row['description'].'</description>'."\n");
					
			if(isset($row['listing_time']) && !empty($row['listing_time']))
				fputs($f, '		<listing_time>'.$row['listing_time'].'</listing_time>'."\n");
					
			if(isset($row['expire_time']) && !empty($row['expire_time']))
				fputs($f, '		<expire_time>'.$row['expire_time'].'</expire_time>'."\n");
			
			fputs($f, '	</listing>'."\n");
		}
		fputs($f, '</listings>'."\n");
		fclose($f);
	}
	
	/**
	 * Validate export item data
	 * 
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());		
		
		if(isset($data['record_id'])){
			$return['data']['record_id'] = intval($data['record_id']);
		}
		
		if(isset($data['title'])){
			$return['data']['title'] = str_replace($this->search, $this->replace, $data['title']);
		}		
		
		if(isset($data['url'])){
			$return['data']['url'] = str_replace($this->search, $this->replace, $data['url']);
		}
		
		if(isset($data['category'])){
			$return['data']['category'] = str_replace($this->search, $this->replace, $data['category']);
		}			
		
		if(isset($data['subcategory'])){
			$return['data']['subcategory'] = str_replace($this->search, $this->replace, $data['subcategory']);
		}			
		
		if(isset($data['image'])){
			$return['data']['image'] = str_replace($this->search, $this->replace, $data['image']);
		}
		
		if(isset($data['image'])){
			$return['data']['image'] = str_replace($this->search, $this->replace, $data['image']);
			if(!empty($return['data']['image'])){
				$return['data']['image'] = array_shift(explode('|', $return['data']['image']));
			}
		}			
		
		if(isset($data['address'])){
			$return['data']['address'] = str_replace($this->search, $this->replace, $data['address']);
		}
		
		if(isset($data['city'])){
			$return['data']['city'] = str_replace($this->search, $this->replace, $data['city']);
		}			
		
		if(isset($data['state'])){
			$return['data']['state'] = str_replace($this->search, $this->replace, $data['state']);
		}
		
		if(isset($data['zip'])){
			$return['data']['zip'] = str_replace($this->search, $this->replace, $data['zip']);
		}			
		
		if(isset($data['country'])){
			$return['data']['country'] = str_replace($this->search, $this->replace, $data['country']);
		}			
		
		if(isset($data['bedrooms'])){
			$return['data']['bedrooms'] = intval($data['bedrooms']);
		}
		
		if(isset($data['bathrooms'])){
			$return['data']['bathrooms'] = intval($data['bathrooms']);
		}			
		
		if(isset($data['square_footage'])){
			$return['data']['square_footage'] = intval($data['square_footage']);
		}
				
		if(isset($data['stories'])){
			$return['data']['stories'] = str_replace($this->search, $this->replace, $data['stories']);
		}		
					
		if(isset($data['lot_size'])){
			$return['data']['lot_size'] = intval($data['lot_size']);
		}			
					
		if(isset($data['parking_spots'])){
			$return['data']['parking_spots'] = intval($data['parking_spots']);
		}
	
		if(isset($data['year_built'])){
			if(strlen($data['year_built']) == 4){
				$return['data']['year_built'] = intval($data['year_built']);
			}
		}
		
		if(isset($data['currency'])){
			$return['data']['currency'] = str_replace($this->search, $this->replace, $data['currency']);
		}		
			
		if(isset($data['price'])){
			$return['data']['price'] = intval($data['price']);
		}			
		
		if(isset($data['amenities'])){
			$return['data']['amenities'] = str_replace($this->search, $this->replace, $data['amenities']);
		}
				
		if(isset($data['description'])){
			$return['data']['description'] = str_replace($this->search, $this->replace, $data['description']);
		}			
		
		if(isset($data['listing_time'])){
			$value = strtotime($data['listing_time']);
			if($value > 0)	$return['data']['listing_time'] = date('Y-m-d H:i:s', $value);
		}	
		
		if(isset($data['expire_time'])){
			$value = strtotime($data['expire_time']);
			if($value > 0)	$return['data']['expire_time'] = date('Y-m-d H:i:s', $value);
		}			
	
		return $return;
	}
}

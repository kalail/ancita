<?php 

if(!defined("BASEPATH")) exit("No direct script access allowed");

/**
 * XML driver export model
 * 
 * @package PG_RealEstate
 * @subpackage Export
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Export_xml_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 * 
	 * @var array
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 * 
	 * @var array
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Constructor
	 * 
	 * @return Export_xml_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Return default filename
	 * 
	 * @return string
	 */
	public function get_filename(){
		return "export.xml";
	}

	/**
	 * Generate output file
	 * 
	 * @param string $filename output filename
	 * @param array $data export data
	 * @param array $settings driver settings
	 * @return void
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		fputs($f, '<?xml version="1.0" encoding="utf-8"?>'."\n");
		fputs($f, '<items>'."\n");
		foreach($data as $i => $row){
			if(!$i) continue;
			fputs($f, '	<item>'."\n");
			foreach($row as $key=>$value){
				$value = str_replace($this->search, $this->replace, $value);
				fputs($f, '			<'.$key.'>'.$value.'</'.$key.'>'."\n");
			}
			fputs($f, '	</item>'."\n");
		}
		fputs($f, '</items>'."\n");
		fclose($f);
	}
	
	/**
	 * Validate export item data
	 * 
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());		
		$return["data"] = $data;	
		return $return;
	}
}

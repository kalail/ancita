<?php 

if(!defined("BASEPATH")) exit("No direct script access allowed");

/**
 * SyndaFeed listings driver export model
 * 
 * @package PG_RealEstate
 * @subpackage Export
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Export_synda_listings_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 * 
	 * @var array
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 * 
	 * @var array
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Property types options
	 * 
	 * @var array
	 */
	protected $property_types = array('sfam', 'apt', 'land', 'duplex', 'multi', 'town', 'condo', 'comm', 'manu');
	
	/**
	 * Listing status codes options
	 * 
	 * @var array
	 */
	protected $listing_status_codes = array('a', 's');
	
	/**
	 * Rental period codes options
	 * 
	 * @var array
	 */
	protected $rental_period_codes = array('a', 's', 'w', 'm');
	
	/**
	 * Distress sale codes options
	 * 
	 * @var array
	 */
	protected $distress_sale_codes = array('v', 's', 'f', 'b');
	
	/**
	 * Constructor
	 * @return Export_synda_listings_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Return default filename
	 * 
	 * @var string
	 */
	public function get_filename(){
		return "listings.xml";
	}

	/**
	 * Generate output file
	 * 
	 * @param string $filename output filename
	 * @param array $data export data
	 * @param array $settings driver settings
	 * @return void
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		
		fputs($f, '<?xml version="1.0" encoding="UTF-8">'."\n");
		fputs($f, '<Listings>'."\n");
		foreach($data as $i=>$row){
			if(!$i) continue;
			
			fputs($f, '	<Listing>'."\n");
			fputs($f, '			<PublisherId>'.$row['PublisherId'].'</PublisherId>'."\n");
			fputs($f, '			<ListingId>'.$row['ListingId'].'</ListingId>'."\n");
				
			if(isset($row['PropertyId']) && !empty($row['PropertyId']))
				fputs($f, '			<PropertyId>'.$row['PropertyId'].'</PropertyId>'."\n");
				
			fputs($f, '			<GMTLastModified>'.$row['GMTLastModified'].'</GMTLastModified>'."\n");				
			fputs($f, '			<ListingStatus>'.$row['ListingStatus'].'</ListingStatus>'."\n");					
			fputs($f, '			<PropertyType>'.$row['PropertyType'].'</PropertyType>'."\n");
			fputs($f, '			<AgentId>'.$row['AgentId'].'</AgentId>'."\n");
			
			if(isset($row['DistressSale']) && !empty($row['DistressSale']))
				fputs($f, '			<DistressSale>'.$row['DistressSale'].'</DistressSale>'."\n");
				
			fputs($f, '			<RentalPeriod>'.$row['RentalPeriod'].'</RentalPeriod>'."\n");
			
			if(isset($row['CapRate']) && !empty($row['CapRate']))
				fputs($f, '			<CapRate>'.$row['CapRate'].'</CapRate>'."\n");
					
			fputs($f, '			<Currency>'.$row['Currency'].'</Currency>'."\n");
			fputs($f, '			<Price>'.$row['Price'].'</Price>'."\n");
				
			if(isset($row['PropertyTax']) && !empty($row['PropertyTax']))
				fputs($f, '			<PropertyTax>'.$row['PropertyTax'].'</PropertyTax>'."\n");
				
			if(isset($row['HomeownerDues']) && !empty($row['HomeownerDues']))
				fputs($f, '			<HomeownerDues>'.$row['HomeownerDues'].'</HomeownerDues>'."\n");
			
			if(isset($row['StreetAddress']) && !empty($row['StreetAddress']))
				fputs($f, '			<StreetAddress hide="false"><![CDATA['.$row['StreetAddress'].']]></StreetAddress>'."\n");
			
			if(isset($row['District']) && !empty($row['District']))
				fputs($f, '			<District><![CDATA['.$row['District'].']]></District>'."\n");
					
			fputs($f, '			<City><![CDATA['.$row['City'].']]></City>'."\n");
			fputs($f, '			<StateProvince><![CDATA['.$row['StateProvince'].']]></StateProvince>'."\n");				
			fputs($f, '			<Country>'.$row['Country'].'</Country>'."\n");
				
			if(isset($row['PostalCode']) && !empty($row['PostalCode']))
				fputs($f, '			<PostalCode>'.$row['PostalCode'].'</PostalCode>'."\n");
					
			if(isset($row['StructureArea']) && !empty($row['StructureArea']))
				fputs($f, '			<StructureArea>'.$row['StructureArea'].'</StructureArea>'."\n");
				
			if(isset($row['LotLengthWidth']) && !empty($row['LotLengthWidth']))
				fputs($f, '			<LotLengthWidth>'.$row['LotLengthWidth'].'</LotLengthWidth>'."\n");
				
			if(isset($row['IrregularLot']) && !empty($row['IrregularLot']))
				fputs($f, '			<IrregularLot>'.$row['IrregularLot'].'</IrregularLot>'."\n");
				
			if(isset($row['LandArea']) && !empty($row['LandArea']))
				fputs($f, '			<LandArea>'.$row['LandArea'].'</LandArea>'."\n");
				
			if(isset($row['BedRooms']) && !empty($row['BedRooms']))
				fputs($f, '			<BedRooms>'.$row['BedRooms'].'</BedRooms>'."\n");
				
			if(isset($row['BathRooms']) && !empty($row['BathRooms']))
				fputs($f, '			<BathRooms>'.$row['BathRooms'].'</BathRooms>'."\n");
				
			if(isset($row['ReceptionRooms']) && !empty($row['ReceptionRooms']))
				fputs($f, '			<ReceptionRooms>'.$row['ReceptionRooms'].'</ReceptionRooms>'."\n");
				
			if(isset($row['TotalRooms']) && !empty($row['TotalRooms']))
				fputs($f, '			<TotalRooms>'.$row['TotalRooms'].'</TotalRooms>'."\n");
				
			if(isset($row['YearBuilt']) && !empty($row['YearBuilt']))
				fputs($f, '			<YearBuilt>'.$row['YearBuilt'].'</YearBuilt>'."\n");
				
			if(isset($row['AVMDeny']) && $row['AVMDeny'])
				fputs($f, '			<AVMDeny>1</AVMDeny>'."\n");
				
			if(isset($row['Latitude']) && !empty($row['Latitude']))
				fputs($f, '			<Latitude>'.$row['Latitude'].'</Latitude>'."\n");
					
			if(isset($row['Longitude']) && !empty($row['Longitude']))
				fputs($f, '			<Longitude>'.$row['Longitude'].'</Longitude>'."\n");
				
			if(isset($row['PhotoURL']) && !empty($row['PhotoURL']))				
				fputs($f, '			<PhotoURL>'.$row['PhotoURL'].'</PhotoURL>'."\n");
				
			if(isset($row['VirtualTourURL']) && !empty($row['VirtualTourURL']))				
				fputs($f, '			<VirtualTourURL>'.$row['VirtualTourURL'].'</VirtualTourURL>'."\n");
				
			if(isset($row['Previews']) && !empty($row['Previews'])){
				fputs($f, '			<Previews>'."\n");
					
				foreach((array)$row['Previews'] as $preview){
					fputs($f, '				<Preview>'."\n");
					fputs($f, '					<Begins>'.$preview['Begins'].'</Begins>'."\n");
					fputs($f, '					<Ends>'.$preview['Ends'].'</Ends>'."\n");
					fputs($f, '					<Notes><![CDATA['.$preview['Notes'].']]></Notes>'."\n");
					fputs($f, '				</Preview>'."\n");
				}
				
				fputs($f, '			</Previews>'."\n");
			}
				
			if(isset($row['GMTOffset']) && !empty($row['GMTOffset']))
				fputs($f, '			<GMTOffset>'.$row['GMTOffset'].'</GMTOffset>'."\n");

			fputs($f, '			<DescriptionLang value="'.$this->CI->pg_language->get_lang_code_by_id($this->CI->pg_language->current_lang_id).'">'."\n");
			fputs($f, '				<Caption><![CDATA['.$row['DescriptionLang.Caption'].']]></Caption>'."\n");
			fputs($f, '				<Description><![CDATA['.$row['DescriptionLang.Description'].']]></Description>'."\n");
			fputs($f, '				<DetailsURL>'.$row['DescriptionLang.DetailsURL'].'</DetailsURL>'."\n");
			fputs($f, '			</DescriptionLang>'."\n");
			fputs($f, '	</Listing>'."\n");
			
		}
		fputs($f, '</Listings>'."\n");
		
		fclose($f);
	}
	
	/**
	 * Validate export item data
	 * 
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());		
		
		if(isset($data['PublisherId'])){
			$return['data']['PublisherId'] = intval($data['PublisherId']);
			if(empty($return['data']['PublisherId'])){
				$return["errors"][] = "PublisherId";
			}
		}else{
			$return["errors"][] = "PublisherId";
		}
		
		if(isset($data['ListingId'])){
			$return['data']['ListingId'] = intval($data['ListingId']);
			if(empty($return['data']['ListingId'])){
				$return["errors"][] = "ListingId";
			}
		}else{
			$return["errors"][] = "ListingId";
		}
		
		if(isset($data['PropertyId'])){
			$return['data']['PropertyId'] = $data['PropertyId'];
		}
		
		if(isset($data['GMTLastModified'])){
			$value = strtotime($data['GMTLastModified']);
			if($value > 0){
				$return['data']['GMTLastModified'] = date('c', $value);
			}else{
				$return["errors"][] = "GMTLastModified";
			}
		}else{
			$return["errors"][] = "GMTLastModified";
		}
		
		if(isset($data["ListingStatus"])){
			$return['data']['ListingStatus'] = 'a';
			if(empty($return['data']['ListingStatus'])){
				$return["errors"][] = "ListingStatus";
			}
		}else{
			$return['data']['ListingStatus'] = 'a';
		}
		
		if(isset($data['PropertyType'])){
			$return['data']['PropertyType'] = $data['PropertyType'];
			if(!in_array($data["PropertyType"], $this->property_types)){
				if(isset($data['PropertyType2'])){
					$return['data']['PropertyType'] = $data['PropertyType2'];
					if(!in_array($data["PropertyType"], $this->property_types)){
						if(isset($data['PropertyType3'])){
							$return['data']['PropertyType'] = $data['PropertyType3'];
							if(!in_array($data["PropertyType"], $this->property_types)){
								if(isset($data['PropertyType4'])){
									$return['data']['PropertyType'] = $data['PropertyType4'];
									if(!in_array($data["PropertyType"], $this->property_types)){
										if(isset($data['PropertyType5'])){
											$return['data']['PropertyType'] = $data['PropertyType5'];
											if(!in_array($data["PropertyType"], $this->property_types)){
												if(isset($data['PropertyType6'])){
													$return['data']['PropertyType'] = $data['PropertyType6'];
													if(!in_array($data["PropertyType"], $this->property_types)){
														$return['data']['PropertyType'] = current($this->property_types);
														//$return["errors"][] = "PropertyType";
													}
												}
											}
										}else{
											$return['data']['PropertyType'] = current($this->property_types);
											//$return["errors"][] = "PropertyType";
										}
									}
								}else{
									$return['data']['PropertyType'] = current($this->property_types);
									//$return["errors"][] = "PropertyType";
								}
							}
						}else{
							$return['data']['PropertyType'] = current($this->property_types);
							//$return["errors"][] = "PropertyType";
						}
					}
				}else{
						$return['data']['PropertyType'] = current($this->property_types);
					//$return["errors"][] = "PropertyType";
				}
			}
		}else{
			$return['data']['PropertyType'] = current($this->property_types);
			//$return["errors"][] = "PropertyType";
		}	
		
		if(isset($data['AgentId'])){
			$return['data']['AgentId'] = intval($data['AgentId']);
			if(empty($data["AgentId"])){
				$return["errors"][] = "AgentId";
			}
		}else{
			$return["errors"][] = "AgentId";
		}
	
		if(isset($data['DistressSale'])){
			if(in_array($data["DistressSale"], $this->distress_sale_codes)){
				$return['data']['DistressSale'] = $data['DistressSale'];
			}
		}
		
		if(isset($data['RentalPeriod'])){
			if(in_array($data["RentalPeriod"], $this->rental_period_codes)){
				$return['data']['RentalPeriod'] = $data['RentalPeriod'];
			}else{
				//$return["errors"][] = "RentalPeriod";
			}
		}	
				
		if(isset($data['CapRate'])){
			$return['data']['CapRate'] = $data['CapRate'];
		}	
		
		if(isset($data['Currency'])){
			$return['data']['Currency'] = $data['Currency'];
			if(empty($return['data']['Currency'])) $return["errors"][] = "Currency";
		}else{
			$return["errors"][] = "Currency";
		}			
		
		if(isset($data['Price'])){
			$return['data']['Price'] = $data['Price'];
			if(empty($return['data']['Price'])) $return["errors"][] = "Price";
		}else{
			$return["errors"][] = "Price";
		}
		
		if(isset($data['PropertyTax'])){
			$return['data']['PropertyTax'] = $data['PropertyTax'];
		}
		
		if(isset($data['HomeownerDues'])){
			$return['data']['HomeownerDues'] = $data['HomeownerDues'];
		}		
		
		if(isset($data['StreetAddress'])){
			$return['data']['StreetAddress'] = $data['StreetAddress'];
		}	
		
		if(isset($data['District'])){
			$return['data']['District'] = $data['District'];
		}	
			
		if(isset($data['City'])){
			$return['data']['City'] = $data['City'];
			if(empty($return['data']['City'])){
				$return["errors"][] = "City";
			}
		}else{
			$return["errors"][] = "City";
		}
		
		if(isset($data['StateProvince'])){
			$return['data']['StateProvince'] = $data['StateProvince'];
			if(empty($return['data']['StateProvince'])){
				$return["errors"][] = "StateProvince";
			}
		}else{
			$return["errors"][] = "StateProvince";
		}
		
		if(isset($data['Country'])){
			$return['data']['Country'] = $data['Country'];
			if(empty($return['data']['Country'])){
				$return["errors"][] = "Country";
			}
		}else{
			$return["errors"][] = "Country";
		}
					
		if(isset($data['PostalCode'])){
			$return['data']['PostalCode'] = $data['PostalCode'];
		}
		
		if(isset($data['StructureArea'])){
			$return['data']['StructureArea'] = $data['StructureArea'];
		}			
		
		if(isset($data['LotLengthWidth'])){
			$return['data']['LotLengthWidth'] = intval($data['LotLengthWidth']);
		}
		
		if(isset($data['IrregularLot'])){
			$return['data']['IrregularLot'] = $data['IrregularLot'];
		}
				
		if(isset($data['LandArea'])){
			$return['data']['LandArea'] = $data['LandArea'];
		}	
		
		if(isset($data['BedRooms'])){
			$return['data']['BedRooms'] = $data['BedRooms'];
		}		
		
		if(isset($data['BathRooms'])){
			$return['data']['BathRooms'] = $data['BathRooms'];
		}		
		
		if(isset($data['ReceptionRooms'])){
			$return['data']['ReceptionRooms'] = $data['ReceptionRooms'];
		}	
		
		if(isset($data['TotalRooms'])){
			$return['data']['TotalRooms'] = $data['TotalRooms'];
		}		
		
		if(isset($data['YearBuilt'])){
			if(strlen($data['YearBuilt']) == 4){
				$return['data']['YearBuilt'] = intval($data['YearBuilt']);
			}
		}
				
		if(isset($data['AVMDeny'])){
			$return['data']['AVMDeny'] = $data['AVMDeny'] ? 1 : 0;
		}		
		
		if(isset($data['Latitude'])){
			$return['data']['Latitude'] = floatval($data['Latitude']);
		}		
		
		if(isset($data['Longitude'])){
			$return['data']['Longitude'] = floatval($data['Longitude']);
		}			
		
		if(isset($data['PhotoURL'])){
			if(!empty($data['PhotoURL'])){
				$return['data']['PhotoURL'] = array_shift(explode('|', $data['PhotoURL']));
			}
		}		
		
		if(isset($data['VirtualTourURL'])){
			if(!empty($data['VirtualTourURL'])){
				$return['data']['VirtualTourURL'] = array_shift(explode('|', $data['VirtualTourURL']));
			}
		}	
				
		if(isset($data['Previews.Begins'])){
			if(!empty($data['Previews.Begins'])){
				$return['data']['Previews.Begins'] = explode('|', $data['Previews.Begins']);
				foreach($return['data']['Previews.Begins'] as $key=>$value){
					$value = strtotime($value);
					if($value > 0) $return['data']['Previews'][$key]['Begins'] = date('Y-m-d H:m:s', $value);
				}
			}
		}
		
		if(isset($data['Previews.Ends'])){
			if(!empty($data['Previews.Ends'])){
				$return['data']['Previews.Ends'] = explode('|', $data['Previews.Ends']);
				foreach($return['data']['Previews.Ends'] as $key=>$value){
					$value = strtotime($value);
					if($value > 0) $return['data']['Previews'][$key]['Ends'] = date('Y-m-d H:m:s', $value);
				}
			}
		}
		
		if(isset($data['Previews.Notes'])){
			if(!empty($data['Previews.Notes'])){
				$return['data']['Previews.Notes'] = explode('|', $data['Previews.Notes']);
				foreach($return['data']['Previews.Notes'] as $key=>$value){
					$return['data']['Previews'][$key]['Notes'] = $value;
				}
			}
		}
				
		if(isset($data['GMTOffset'])){
			$return['data']['GMTOffset'] = $data['GMTOffset'];
		}		
		
		if(isset($data['DescriptionLang.Caption'])){
			$return['data']['DescriptionLang.Caption'] = $data['DescriptionLang.Caption'];
			if(empty($return['data']['DescriptionLang.Caption'])){
				$return["errors"][] = "DescriptionLang.Caption";
			}
		}else{
			$return["errors"][] = "DescriptionLang.Caption";
		}
		
		if(isset($data['DescriptionLang.Description'])){
			$return['data']['DescriptionLang.Description'] = $data['DescriptionLang.Description'];
			if(empty($return['data']['DescriptionLang.Description'])){
				$return["errors"][] = "DescriptionLang.Description";
			}
		}else{
			$return["errors"][] = "DescriptionLang.Description";
		}
		
		if(isset($data['DescriptionLang.DetailsURL'])){
			$return['data']['DescriptionLang.DetailsURL'] = $data['DescriptionLang.DetailsURL'];
			if(empty($return['data']['DescriptionLang.DetailsURL'])){
				$return["errors"][] = "DescriptionLang.DetailsURL";
			}
		}else{
			$return["errors"][] = "DescriptionLang.DetailsURL";
		}

		return $return;
	}
}

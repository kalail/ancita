<?php 

if(!defined("BASEPATH")) exit("No direct script access allowed");

/**
 * CSV driver export model
 * 
 * @package PG_RealEstate
 * @subpackage Export
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Export_csv_model extends Model{	
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;

	/**
	 * Content type
	 * 
	 * @var object
	 */
	public $content_file = "text_csv";
	
	/**
	 * Constructor
	 * 
	 * @return Export_csv_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Return default filename
	 * 
	 * @return string
	 */
	public function get_filename(){
		return "export.csv";
	}
	
	/**
	 * Generate output file
	 * 
	 * @param string $filename output filename
	 * @param array $data export data
	 * @param array $settings driver settings
	 * @return void
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		foreach($data as $row){
			fputcsv($f, $row, $settings["delimiter"] ? $settings["delimiter"] : ";", $settings["enclosure"] ? $settings["enclosure"] : "\"");
		}
		fclose($f);
	}
	
	/**
	 * Validate export item data
	 * 
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());
		$return["data"] = $data;
		return $return;
	}
}

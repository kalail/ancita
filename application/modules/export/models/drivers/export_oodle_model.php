<?php 

if(!defined("BASEPATH")) exit("No direct script access allowed");

/**
 * Oodle xml driver export model
 * 
 * @package PG_RealEstate
 * @subpackage Export
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Export_oodle_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Not available symbols
	 * 
	 * @var array
	 */
	protected $search = array('&', "'", '"', ">", "<");
	
	/**
	 * Symbols for replace
	 * 
	 * @var array
	 */
	protected $replace = array('&amp;', '&apos;', '&quot;', "&gt;", "&lt;");
	
	/**
	 * Amenities options
	 * 
	 * @var array
	 */
	protected $amenities = array('AC', 'alarm', 'basketball', 'cable', 'clubhouse', 
								 'dishwasher', 'doorman', 'elevator', 'fireplace', 
								 'gated', 'gym', 'hot tub', 'internet', 'parking', 
								 'patio', 'playground', 'pool', 'refrigerator', 
								 'satellite', 'storage', 'TV', 'tennis', 'washer dryer', 
								 'wood floors');
								 
	/**
	 * Categories options
	 * 
	 * @var array
	 */
	protected $categories = array('Community', 'Announcements', 'Auction Sales', 'Celebrations',
								  'Anniversary Announcements', 'Birthday Announcements',
								  'Birth Announcements', 'Congratulation Announcements',
								  'Engagement Announcements', 'Marriage Announcements',
								  'Other Celebrations', 'Seasonal Announcements',
								  'Thank You Announcements', 'Company Notices, Tenders & Contracts',
								  'Business Opportunities', 'Death Announcements',
								  'Funeral Announcements', 'Government & Electoral Notices',
								  'Legal Announcements', 'Lost Announcements', 'Other Announcements',
								  'Proposals-RFPs', 'Trustee Sales', 'Groups', 'Art & Music News',
								  'Film News & Announcements', 'Musician & Band News & Announcements',
								  'Other Art News & Announcements', 'Artist News & Announcements',
								  'Political News & Opinions', 'Rants', 'Rideshare', 'Wanted',
								  'Housing', 'Property For Rent', 'Apartments for Rent',
								  'Commercial & Office Space for Rent', 'Industrial Property for Rent',
								  'Office Space for Rent', 'Retail Property for rent', 'Condos & Townhouses for Rent',
								  'Garages for Rent', 'Homes for Rent', 'Mobile Homes for Rent',
								  'Open Houses for Rent', 'Other Housing for Rent', 'Roommates',
								  'Short Term Rentals', 'Storage Space for Rent', 'Vacation Rentals',
								  'Real Estate', 'Commercial & Office Space for Sale', 'Industrial Property for Sale',
								  'Office Space for Sale', 'Retail Property for Sale', 'Condos, Townhouses & Apts for Sale',
								  'Farms & Ranches for Sale', 'Foreclosures', 'Single-Family Houses', 'Land for Sale',
								  'Mobile Homes for Sale', 'Multi-Family Residences for Sale', 'Open Houses',
								  'Other Housing for Sale', 'Storage Space for Sale', 'Vacation Property for Sale',
								  'Jobs', 'Administrative, Clerical & Support Services Jobs',
								  'Advertising, Marketing & Public Relations Jobs', 'Architecture & Design Jobs',
								  'Art Jobs', 'Other Art & Media Jobs', 'Visual Art Jobs', 'Automotive Jobs',
								  'Banking Jobs', 'Science, Pharmaceutical & Biotech Jobs', 'Business Opportunities',
								  'Government Jobs', 'Computer & Software Jobs', 'Construction & Skilled Labor Jobs',
								  'Consulting Jobs', 'Customer Service & Call Center Jobs', 'Distribution Jobs',
								  'Domestic Help, Child & Adult Care Jobs', 'Teaching, Training & Library Jobs',
								  'Oil, Gas & Solar Power Jobs', 'Engineering & Product Development Jobs',
								  'TV, Film & Musician Jobs', 'Acting Jobs', 'Adult Acting Jobs', 'Dancing Jobs',
								  'Adult Dancing Jobs', 'Modeling Jobs', 'Adult Modeling Jobs', 'Music Jobs',
								  'Post-Production Jobs', 'Production Jobs', 'Facilities & Maintenance Jobs',
								  'Accounting & Finance Jobs', 'Recreation & Fitness Jobs', 'Franchise Jobs',
								  'General Business Jobs', 'General Labor Jobs', 'Grocery Jobs', 'Healthcare & Nurse Jobs',
								  'Hospitality, Tourism & Travel Jobs', 'Human Resources & Recruiting Jobs',
								  'Insurance Jobs', 'Inventory Jobs', 'Law Enforcement & Security Jobs',
								  'Legal Jobs', 'Transportation, Travel & Logistics Jobs', 'Management & Executive Jobs',
								  'Military Jobs', 'Non-Profit & Fundraising Jobs', 'Other Jobs', 'Paralegal Jobs',
								  'Production & Operations Jobs', 'Professional Services Jobs', 'Quality Assurance & Control Jobs',
								  'Research & Development Jobs', 'Real Estate Jobs', 'Installation, Maintenance & Repair Jobs',
								  'Restaurant & Food Service Jobs', 'Retail, Grocery & Wholesale Jobs', 
								  'Sales & Business Development Jobs', 'Salon & Spa Jobs', 'Science Jobs', 
								  'Social Services & Counseling Jobs', 'Strategy & Planning Jobs', 'Information Technology Jobs',
								  'Telecommunications Jobs', 'Training & Instructor Jobs', 'Veterinary & Other Animal Care Jobs',
								  'Warehouse Jobs', 'Work from Home & Self Employed Jobs', 'Media, Journalism & Newspaper Jobs',
								  'Personals', 'Men Seeking Both', 'Men Seeking Men', 'Men Seeking Women', 'Missed Connections',
								  'Everyone Else', 'Women Seeking Both', 'Women Seeking Men', 'Women Seeking Women', 'Merchandise',
								  'Adult Merchandise', 'Antiques', 'Appliances', 'Dishwashers', 'Microwave Ovens', 'Other Appliances',
								  'Cooktops, Ovens & Ranges', 'Refrigerators & Freezers', 'Freezers', 'Refrigerators',
								  'Small Appliances', 'Blenders', 'Bread Machines', 'Coffee Makers', 'Food Processors',
								  'Juicers', 'Mixers', 'Other Small Appliances', 'Toasters', 'Water Filters', 'Washing Machines & Dryers',
								  'Clothes Dryers', 'Washing Machines', 'Water Heaters', 'Arts & Crafts', 'Artwork', 'Arts & Crafts Supplies',
								  'Bags and Luggage', 'Backpacks & Briefcases', 'Briefcases', 'Handbags & Purses', 'Luggage', 'Wallets',
								  'Books & Magazines', 'Audio Books', 'Books', 'Kids Books', 'Magazines', 'Manuals', 'Textbooks',
								  'Office & Business', 'Building Supplies', 'Agriculture & Forestry', 'Medical Equip & Supplies',
								  'Office Equip & Supplies', 'Restaurant & Retail', 'Business for Sale', 'CDs and DVDs', 'Blu-Ray Discs',
								  'CDs', 'DVDs', 'Other Movies and Music', 'VHS Movies', 'Mobile Phones', 'Android Phones',
								  'Blackberry Phones', 'iPhones', 'Other Mobile Phones', 'Windows Phones', 'Clothes & Accessories',
								  'Accessories', 'Belts', 'Gloves & Mittens', 'Hair Accessories', 'Hats & Caps', 'Neckware, Ties & Scarfs',
								  'Sunglasses', 'Coats, Parkas & Wind Breakers', 'Dresses', 'Exercise & Fitness Clothes',
								  'Military Gear & Surplus', 'Other Clothing', 'Outfits & Sets', 'Pants & Jeans', 'Shirts & Tops',
								  'Shoes', 'Shorts', 'Skirts', 'Sleepwear & Underwear', 'Socks & Hosiery', 'Suits, Blazers & Jackets',
								  'Sweaters', 'Swimsuits & Swimwear', 'Wedding Dresses & Accessories', 'Collectibles', 'Trading Cards',
								  'Coins', 'Comics', 'Military Memorabilia', 'Sports Memorabilia', 'Stamps', 'Computers & Accessories',
								  'Computer Components', 'Desktop PCs', 'Keyboards & Input Devices', 'Computer Keyboards', 'Computer Mouse',
								  'Laptop Computers', 'Computer Displays', 'CRT Monitors', 'LCD Monitors', 'Network Equipment', 'Computer Hubs',
								  'Computer Load Balancers', 'Other Network Equipment', 'Computer Routers', 'Computer Switches',
								  'Wireless Networking', 'Other Computer Equipment', 'Printers, Scanners & Accessories',
								  'All-in-One Printers', 'Inkjet Printers', 'Laser Printers', 'Scanners', 'Printer Supplies',
								  'Computer Servers', 'Computer Software', 'Computer Storage', 'CD Drives', 'DVD Drives',
								  'External Hard Drives', 'Flash Memory Drives', 'Floppy Drives', 'Internal Hard Drives',
								  'Blank Media', 'Electronics', 'Audio', 'Home Audio', 'Home Audio Accessories', 'Amplifiers & Preamps',
								  'Cassette Decks', 'CD Players', 'Equalizers', 'Audio Media', 'Receivers', 'Speakers', 'Home Theater System',
								  'Turntables', 'Personal Audio', 'Boomboxes', 'Cassette Players', 'Personal CD Players', 'Clock Radios',
								  'Headphones', 'Cameras', 'Camera Accessories', 'Digital Cameras', 'Film Cameras', 'Telescopes & Binoculars',
								  'Car Electronics', 'Car Navigation Systems', 'Radar Detectors', 'Car Stereo', 'DVD players', 'DVRs',
								  'GPS Devices', 'iPods & MP3 Players', 'iPods', 'MP3 Players', 'Gadgets & Other Electronics', 
								  'Home and Office Phones', 'Satellite Dishes & Receivers', 'iPads and Tablets', 'Android Tablets',
								  'iPads', 'Other Tablets', 'Televisions', 'VCRs', 'Video Cameras', 'Free Merchandise', 'Furniture',
								  'Beds', 'Bunk Beds', 'Bedroom Sets', 'Water Beds', 'Benches & Stools', 'Benches', 'Stools',
								  'Bookcases & Shelves', 'Cabinets, Armoires & Cupboards', 'Armoires', 'Cabinets', 'Cupboards',
								  'Chairs', 'Recliner Chairs', 'Rocking Chairs', 'Desks', 'Dresser & Vanities', 'Other Furniture',
								  'Clocks', 'Credenza', 'Lamps', 'Mirrors', 'Rugs', 'Sofas', 'Couches', 'Futons', 'Love Seats',
								  'Sectional Sofas', 'Sofa Beds & Sleepers', 'Tables & Stands', 'Changing Tables', 'Coffee Tables',
								  'End Tables', 'Stands', 'Health & Beauty', 'Makeup & Cosmetics', 'Hair Products', 'Skin Care',
								  'Supplements', 'Home & Garden', 'Bedding', 'Cleaning & Vacuuming', 'Home Decor', 
								  'Carpets & Floor Materials', 'Food & Produce', 'Heating, Cooling & Air', 'Air Conditioners',
								  'Firewood', 'Heaters, Fireplaces & Furnaces', 'Kitchen & Cooking', 'Lawn, Garden & Patio',
								  'Lawnmowers', 'Lamps, Lighting & Ceiling Fans', 'Pools, Spas & Hot Tubs', 'Home Security',
								  'Windows, Drapes & Curtains', 'Jewelry', 'Bracelets', 'Earrings', 'Necklaces', 'Rings',
								  'Watches', 'Baby & Kid Stuff', 'Baby Grooming, Bathing, and Feeding', 'Baby Backpacks & Carriers',
								  'Kid\'s Bedding', 'Kid\'s Bikes', 'Kid\'s Books', 'Car Seats', 'Kid\'s Clothes', 'Cribs', 
								  'Kid\'s Furniture', 'Baby Safety Products', 'Baby Strollers', 'Toys', 'Infant & Toddler Toys', 
								  'Toy Cars, Trains, Airplanes', 'Dolls & Action Figures', 'Educational Toys', 'Kids Games',
								  'Outdoor Toys & Structures', 'Stuffed Animals', 'Musical Instruments', 'Music Accessories',
								  'Metronomes', 'Music Stands', 'Sheet Music', 'Tuning Forks', 'Brass Instruments', 'Baritones',
								  'Bugles', 'Cornets', 'Flugelhorns', 'French Horns', 'Trombones', 'Trumpets', 'Tubas',
								  'Drums & Percussion', 'Base Drums', 'Bongos', 'Musical Instrument Chimes', 'Conga Drums',
								  'Cowbells', 'Cymbals', 'Drums', 'Glockespiels', 'Gongs', 'Hi-Hat Drums', 'Maracas', 'Percussions',
								  'Snare Drums', 'Tambourines', 'Timpanis', 'Tom-Tom Drums', 'Instruments: Triangles', 'Xylophones',
								  'Guitars & Basses', 'Banjos', 'Bass Instruments', 'Dulcimers', 'Guitars', 'Lutes', 'Mandolins',
								  'Ukuleles', 'Other Musical Instruments', 'Pianos & Keyboards', 'Accordians', 'Instruments: Keyboards',
								  'Instruments: Organs', 'Pianos', 'Synthesizers', 'Classic Stringed Instruments', 'Autoharps',
								  'Cellos', 'Harps', 'Violas', 'Violins', 'Woodwinds', 'Bassoons', 'Clarinets', 'Instruments: Flutes',
								  'Harmonicas', 'Oboes', 'Piccolos', 'Instruments: Recorders', 'Saxophones', 'Everything Else',
								  'Pets', 'Birds', 'Cats', 'Dogs', 'Fish', 'Horses', 'Livestock', 'Livestock Supplies', 'Other Pets',
								  'Rabbits', 'Reptiles', 'Small & Furry Animals', 'Pet Supplies', 'Pet Supplies, Birds', 
								  'Pet Supplies, Cats', 'Pet Supplies, Dogs', 'Pet Supplies, Fish', 'Pet Supplies, Horses',
								  'Auctions, Estate, Yard & Garage Sales', 'Auctions', 'Estate Sales', 'Garage & Yard Sales', 
								  'Sporting Goods & Bicycles', 'Bicycles', 'Bicycling Clothing', 'Bicycle Helmets', 'Kids Bikes',
								  'Mountain Bikes', 'Bicycle Parts & Accessories', 'Road Bikes', 'Camping Equipment', 'Equestrian Equipment',
								  'Exercise Equipment', 'Treadmills & Stair Machines', 'Stair Machines', 'Treadmills', 'Hunting & Fishing',
								  'Fishing Equipment', 'Hunting Equipment', 'Indoor Games', 'Air Hockey', 'Darts', 'Foosball', 'Ping Pong',
								  'Pool Tables & Equipment', 'Other Sports & Fitness', 'Snow Sports', 'Skiing', 'Snowboarding',
								  'Sports Equipment', 'Baseball Equipment', 'Basketball Equipment', 'Football Equipment',
								  'Golf Equipment', 'Hockey Equipment', 'Other Sports Equipment', 'Soccer Equipment', 'Tennis & Racquet Sports Equipment',
								  'Water Sports', 'Scuba & Snorkeling', 'Other Water Sports', 'Canoe, Kayaks, Rafts', 'Canoe',
								  'Kayaks', 'Rafts', 'Waterskiing & Wakeboarding', 'Surfing', 'Wind Surfing', 'Weights & Weightlifting Equipment',
								  'Tickets', 'Concert Tickets', 'Group Events', 'Business & Networking Events', 'Classes & Workshops',
								  'Conferences & Seminars', 'Dance Events', 'Festivals', 'Food & Wine Events', 'Free Events',
								  'Health & Spiritual Events', 'Kids & Family Events', 'Other Events', 'Sports & Outdoor Events',
								  'Other Tickets', 'Movie & Film Event Tickets', 'Travel Tickets', 'Sports Tickets', 'Auto Racing Tickets',
								  'Baseball Tickets', 'Basketball Tickets', 'Boxing Tickets', 'Cricket Tickets', 'Football Tickets',
								  'Golf Tickets', 'Hockey Tickets', 'Horse Racing Tickets', 'Olympics Tickets', 'Other Sports Tickets',
								  'Rugby Tickets', 'Soccer Tickets', 'Tennis Tickets', 'Track and Field Tickets', 'Wrestling Tickets',
								  'Theater Tickets', 'Tools', 'Hand Tools', 'Axes', 'Files & Chisels', 'Clamps & Vises', 'Hammers',
								  'Knives & Cutters', 'Ladders', 'Levels', 'Measuring Tools', 'Other Hand Tools', 'Tools: Planes',
								  'Pliers', 'Pry Bars', 'Tools: Pullers', 'Tools: Punches', 'Saws', 'Sawhorses', 'Screwdrivers',
								  'Sledge Hammers', 'Sockets', 'Tools: Squares', 'Stud Sensors', 'Tool Sets', 'Wrenches',
								  'Other Home Tools', 'Power Tools', 'Air Compressors', 'Air Hammers', 'Air Ratchets', 'Wood Chipper',
								  'Drills', 'Engravers', 'Generators', 'Grinders', 'Impact Wrenches', 'Lathes', 'Leaf Blower',
								  'Nail & Staple Guns', 'Planers', 'Polishers', 'Pressure Washers', 'Tools: Routers', 'Sanders',
								  'Saws', 'Band Saws', 'Chain Saws', 'Circular Saws', 'Miter Saws', 'Reciprocating Saws',
								  'Scroll Saws', 'Table Saws', 'Tile Saws', 'Welding & Soldering', 'Woodworking Tools', 
								  'Video Games and Consoles', 'Video Game Consoles', 'Video Games', 'Portable Game Machines',
								  'Services', 'Auto & Other Vehicle Services', 'Auto Detailing & Cleaning', 'Auto Lube & Oil Change', 
								  'Vehicles for Rent', 'Auto Repair', 'Auto Tire Services', 'Child & Elderly Care Services',
								  'Child Care', 'Babysitters', 'Child Safety Services', 'Elderly Care', 'Home Nurse', 'Cleaning Services',
								  'Carpet & Upholstery Cleaning', 'Chimney & Fireplace Cleaning', 'Exterior Home Cleaning', 
								  'Home Cleaning & Maid Services', 'Removal of Junk or Building Materials', 'Window Cleaning',
								  'Coupons & Offers', 'Art and Collectibles', 'Books and Magazines', 'Automotive', 
								  'Clothing and Accessories', 'Dining', 'Education and Learning', 'Computers and Electronics',
								  'Entertainment', 'Groceries', 'Gifts, Flowers and Gourmet Foods', 'Health and Beauty', 'Home and Garden',
								  'Baby and Kids', 'Office and Business', 'Other', 'Pet Products and Services', 'Phone and Internet Services',
								  'Services', 'Sports and Fitness', 'Travel', 'Creative Services', 'Design Services', 'Film/Production Services',
								  'Recording Services', 'Other Creative', 'Photographic Services', 'Lessons & Classes', 
								  'Arts & Craft Lessons', 'Cooking Classes', 'Dance Lessons', 'Driving Instruction', 'Language Lessons',
								  'Music Lessons', 'Other Lessons & Classes', 'Instruction Schools', 'Sports Lessons', 'Golf Lessons',
								  'Technology Classes', 'Private Instruction & Tutoring', 'Party & Entertainment Services', 'Catering',
								  'Event Planners', 'Kids Parties', 'Music & DJs', 'Other Party & Entertainment Services', 'Party Rentals',
								  'Financial & Legal Services', 'Accounting & Bookkeeping', 'Help with Credit', 'Financial Advisors',
								  'Insurance', 'Investment Services', 'Financial Loans', 'Auto Loans', 'Home Loans', 
								  'Other Financial & Legal Services', 'Tax Preparation', 'Restaurants, Clubs & Food', 'Bars, Pubs & Clubs',
								  'Restaurants', 'Health & Beauty Services', 'Alternative Medicine', 'Body Art', 'Chiropractors', 
								  'Counseling', 'Exercise & Fitness Services', 'Hair Care & Styling', 'Massage Services',
								  'Medical Care', 'Other Health & Beauty Services', 'Manicure & Pedicure', 'Skin Care, Cosmetics & Tanning',
								  'Home Repair & Maintenance', 'Appliance Repair & Installation', 'Appliance Installation',
								  'Appliance Repair or Service', 'Construction & Remodeling', 'Home Additions', 'Demolition',
								  'New Home Construction', 'Home Construction Professionals', 'Appraisers', 'Architects',
								  'Engineers', 'Other Construction Professionals', 'Land Surveyor', 'Home Remodels & Renovations',
								  'Sheds & Exterior Structures', 'Home Decorating Services', 'Windows & Doors', 
								  'Awning & Patio Cover Services', 'Door Installation & Repair', 'Garage Door Installation & Repair',
								  'Door Hardware, Locks & Screens', 'Window Sales, Installation & Repair', 'Window Protection & Tinting',
								  'Window Treatment Services', 'Electrical & Lighting Services', 'Electrical Services',
								  'Generator Installation & Repair', 'Lighting Design & Installation', 'Holiday Lighting Services',
								  'Home Security & Alarm Services', 'Flooring', 'Carpet Installation', 'Concrete Floors', 
								  'Brick & Stone Floors', 'Sport Surfaces', 'Tile Flooring', 'Vinyl & Laminate Floors',
								  'Hardwood Floors', 'Furniture Repair & Upholstery', 'Furniture Repair', 'Upholstery',
								  'Handyman Services', 'Heating & Cooling Services', 'Air Conditioning & Cooling', 
								  'Central Heating Systems', 'Ducts & Vents', 'Fans', 'Gas Fireplaces & Inserts', 'Fuel Delivery & Storage',
								  'Furnace, Boilers & Radiators', 'Other HVAC', 'Radiant Heat Systems', 'Thermostats & Accessories',
								  'Water Heater Services', 'Other Home Services', 'Home Disability Services', 
								  'Asbestos, Lead, Mold & Radon Services', 'Metal Work & Sign Makers', 'Pest Control', 'Recovery Services',
								  'Home Service Rentals', 'Home Security & Safety', 'Painting & Staining Services', 
								  'Exterior Painting or Staining', 'Interior Painting or Staining', 'Paint Removal & Cleaning',
								  'Special Paint Finishes', 'Plumbing Services', 'Drain Clearing', 'Fire Sprinkler Systems',
								  'Faucets, Fixtures & Pipes', 'Other Plumbing Related Services', 'Plumbing Pump Services',
								  'Septic & Sewer Systems', 'Water Softening & Purification', 'Concrete, Stone & Brick',
								  'Driveways, Patios & Walks', 'Chimney, Fireplace, Stove & BBQ', 'Foundations',
								  'Other Concrete & Masonry Services', 'Roofing, Siding & Gutters', 'Gutter Cleaning',
								  'Gutter Installation & Repair', 'Roof Sales & Installation', 'Roof Repair', 
								  'Siding Installation & Repair', 'Skylight Installation & Repair', 'Wall & Ceiling Services',
								  'Ceilings', 'Drywall & Plaster', 'Insulation', 'Wallpapering', 'Carpentry & Cabinets', 
								  'Cabinetry Services', 'Closet Design & Installation', 'Countertops', 'Finish Carpentry',
								  'House Framing', 'Stairs & Railings', 'Career Services', 'Lawn & Garden Services',
								  'Blacktop & Paving', 'Decks', 'Fences', 'Fountains, Ponds & Water Features', 'Lawn Care & Gardening',
								  'Landscaping', 'Design & Installation', 'Sprinkler Systems', 'Other Lawn & Garden Services',
								  'Hardscaping', 'Outdoor Patios, Steps & Walkways', 'Outdoor Walls & Retaining Walls', 'Snow Removal',
								  'Tree & Shrub Service', 'Legal Services', 'Moving & Storage Services', 'Hauling', 'Moving',
								  'Storage', 'Other Services', 'Special Offers on Services', 'Pet & Animal Services',
								  'Washing & Grooming', 'Kennels', 'Other Pet Services', 'Pet Sitting', 'Veterinarians',
								  'Dog Walking', 'Pool, Spa & Sauna Services', 'Swimming Pools', 'Pool Maintenance', 
								  'Pool Sales & Installation', 'Pool Repair', 'Saunas', 'Sauna Maintenance', 'Sauna Sales & Installation',
								  'Sauna Repair', 'Hot Tubs & Spas', 'Spa Maintenance', 'Spa Sales & Installation', 'Spa Repair',
								  'Metaphysical & Psychic Services', 'Real Estate Agents & Services', 'Real Estate Agents & Brokers',
								  'Appraisal Services', 'Home Inspectors', 'Pest Inspectors', 'Roof Inspectors', 'Waterproofing Inspectors',
								  'Other Real Estate Services', 'Staging Services', 'Technical Repair & Services', 
								  'Audio & Video Setup & Repair', 'Computer Setup & Repair', 'Home Networking Services', 
								  'Electronics Repair', 'Travel & Transportation Services', 'Transportation Services',
								  'Travel Services', 'Cars & Vehicles', 'Airplanes & Aviation', 'ATVs', 'Boats', 'Commercial Boats',
								  'Fishing Boats', 'Fresh Water Fishing Boats', 'Salt Water Fishing Boats', 'High Performance Motor Boats',
								  'House Boats', 'Inflatable & Rib Boats', 'JetSkis & Watercraft', 'Motorboats & Powerboats',
								  'Pontoon & Deck Boats', 'Sailboats', 'Small Boats', 'Boat Trailers', 'Ski & Wakeboard Boats',
								  'Yachts', 'Cars', 'Classic Cars', 'Convertibles', 'Coupes', 'Hatchbacks', 'Hybrids', 'Mini-Vans',
								  'Sedans', 'Station Wagons', 'SUVs', 'Trucks', 'Vans', 'Commercial Trucks & Trailers', 
								  'Auto Carrier Trucks', 'Bucket Trucks', 'Truck Cabs & Chassis', 'Cabover Trucks', 'Cabover Sleeper Trucks',
								  'Construction & Vocational Trucks', 'Conventional Sleeper Trucks', 'Crane Trucks', 'Day Cab Trucks',
								  'Dump Trucks', 'Expeditor & Hot Shot Trucks', 'Flatbed Trucks', 'Grain Trucks', 'Logging Trucks',
								  'Mixer Trucks', 'Other Commercial Trucks', 'Refrigerated Trucks', 'Refuse Trucks', 
								  'Front-Loading Refuse Trucks', 'Rear-Loading Refuse Trucks', 'Recycling Trucks', 'Rollback Trucks',
								  'Roll Off Trucks', 'Service & Utility Trucks', 'Severe Duty Trucks', 'Tank Trucks', 'Tow Trucks',
								  'Commercial Truck Trailers', 'Beam Trailers', 'Belt Trailers', 'Car Carrier Trailers', 'Chassis Trailers',
								  'Container Trailers', 'Flip, Converter Dollies & Jeep Trailers', 'Curtainside Trailers', 
								  'Drop Deck Trailers', 'Double Drop Trailers', 'Expandable Drop Deck Trailers', 
								  'Expandable Double Drop Trailers', 'Dump Trailers', 'Equipment Trailers', 'Flatbed Trailers',
								  'Expandable Flatbed Trailers', 'Grain Trailers', 'Hopper Trailers', 'Horse & Livestock Trailers',
								  'Log Trailers', 'Lowboy Trailers', 'Open Top Trailers', 'Other Trailers', 'Platform Trailers',
								  'Reefer Trailers', 'Roll Off Trailers', 'Specialty Trailers', 'Storage Trailers', 'Tag Trailers',
								  'Tank Trailers', 'Van Trailers', 'Van Trucks', 'Yard Trucks', 'Heavy Equipment Vehicles',
								  'Motorcycles', 'Classic Motorcycles', 'Competition Motorcycles', 'Custom Motorcycles', 
								  'Electric Motorcycles', 'Golf Carts', 'Mini & Pocket Bikes', 'Off Road Motorcycles',
								  'Dirt Bikes', 'MX Bikes', 'Supercross Bikes', 'Road Bikes', 'Cruiser Motorcycles',
								  'Dual Sport Motorbikes', 'Sport Touring Motorcycles', 'Sport Bikes', 'Standard Motorcycles',
								  'Touring Motorcycles', 'Scooters & Mopeds', 'Motorcycle Trailers', 'Motorcycles Trike',
								  'Other Vehicles', 'Vehicle Parts, Accessories & Storage', 'Aviation Parts & Accessories',
								  'ATV Parts & Accessories', 'Boat Parts & Accessories', 'Car & Truck Parts & Accessories',
								  'Motorcycle Parts & Accessories', 'RV Parts & Accessories', 'Snowmobile Parts & Accessories',
								  'Power Sports', 'RVs & Motorhomes', 'Campers', 'Motorhomes', 'Travel Trailers', 'Snowmobiles',
								  'Vehicle Storage', 'Aviation Storage', 'Boat Storage', 'RV Parks & Spaces');
								  
	/**
	 * Conditions options
	 * 
	 * @var array
	 */
	protected $conditions = array('pre-construction', 'new', 'existing', 'resale');
	
	/**
	 * Facings options
	 * 
	 * @var array
	 */
	protected $facings = array('north', 'northwest', 'west', 'southwest', 'south', 'southeast', 'east', 'northeast');

	/**
	 * Furnished options
	 * 
	 * @var array
	 */
	protected $furnished = array('furnished', 'partly furnished', 'unfurnished');
	
	/**
	 * Constructor
	 * 
	 * @return Export_oodle_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Return default filename
	 * 
	 * @return string
	 */
	public function get_filename(){
		return "export.xml";
	}

	/**
	 * Generate output file
	 * 
	 * @param string $filename output filename
	 * @param array $data export data
	 * @param array $settings driver settings
	 * @return void
	 */
	public function generate($filename, $data, $settings){
		$f = fopen($filename, "wb");
		fputs($f, '<?xml version="1.0" encoding="utf-8">'."\n");
		fputs($f, '<listings>'."\n");
		foreach($data as $i=>$row){
			if(!$i) continue;
			fputs($f, '	<listing>'."\n");

			if(isset($row['category']) && !empty($row['category']))
				fputs($f, '		<category>'.$row['category'].'</category>'."\n");
				
			if(isset($row['description']) && !empty($row['description']))
				fputs($f, '		<description>'.$row['description'].'</description>'."\n");
				
			if(isset($row['id']) && !empty($row['id']))
				fputs($f, '		<id>'.$row['id'].'</id>'."\n");
					
			if(isset($row['title']) && !empty($row['title']))
				fputs($f, '		<title>'.$row['title'].'</title>'."\n");
					
			if(isset($row['url']) && !empty($row['url']))
				fputs($f, '		<url>'.$row['url'].'</url>'."\n");
					
			if(isset($row['address']) && !empty($row['address']))
				fputs($f, '		<address>'.$row['address'].'</address>'."\n");
					
			if(isset($row['city']) && !empty($row['city']))
				fputs($f, '		<city>'.$row['city'].'</city>'."\n");
					
			if(isset($row['country']) && !empty($row['country']))
				fputs($f, '		<country>'.$row['country'].'</country>'."\n");
					
			if(isset($row['latitude']))
				fputs($f, '		<latitude>'.$row['latitude'].'</latitude>'."\n");
				
			if(isset($row['longitude']))
				fputs($f, '		<longitude>'.$row['longitude'].'</longitude>'."\n");
					
			if(isset($row['neighborhood']) && !empty($row['neighborhood']))
				fputs($f, '		<neighborhood>'.$row['neighborhood'].'</neighborhood>'."\n");
					
			if(isset($row['state']) && !empty($row['state']))
				fputs($f, '		<state>'.$row['state'].'</state>'."\n");
					
			if(isset($row['zip_code']) && !empty($row['zip_code']))
				fputs($f, '		<zip_code>'.$row['zip_code'].'</zip_code>'."\n");
					
			if(isset($row['agent']) && !empty($row['agent']))
				fputs($f, '		<agent>'.$row['agent'].'</agent>'."\n");
					
			if(isset($row['agent_email']) && !empty($row['agent_email']))
				fputs($f, '		<agent_email>'.$row['agent_email'].'</agent_email>'."\n");
					
			if(isset($row['agent_phone']) && !empty($row['agent_phone']))
				fputs($f, '		<agent_phone>'.$row['agent_phone'].'</agent_phone>'."\n");
					
			if(isset($row['agent_url']) && !empty($row['agent_url']))
				fputs($f, '		<agent_url>'.$row['agent_url'].'</agent_url>'."\n");
				
			if(isset($row['amenities']) && !empty($row['amenities']))
				fputs($f, '		<amenities>'.implode(', ', $row['amenities']).'</amenities>'."\n");
				
			if(isset($row['balconies']) && !empty($row['balconies']))
				fputs($f, '		<balconies>'.$row['balconies'].'</balconies>'."\n");
				
			if(isset($row['bathrooms']) && !empty($row['bathrooms']))
				fputs($f, '		<bathrooms>'.$row['bathrooms'].'</bathrooms>'."\n");
					
			if(isset($row['bedrooms']) && !empty($row['bedrooms']))
				fputs($f, '		<bedrooms>'.$row['bedrooms'].'</bedrooms>'."\n");
					
			if(isset($row['broker']) && !empty($row['broker']))
				fputs($f, '		<broker>'.$row['broker'].'</broker>'."\n");
					
			if(isset($row['broker_email']) && !empty($row['broker_email']))
				fputs($f, '		<broker_email>'.$row['broker_email'].'</broker_email>'."\n");
					
			if(isset($row['broker_phone']) && !empty($row['broker_phone']))
				fputs($f, '		<broker_phone>'.$row['broker_phone'].'</broker_phone>'."\n");
					
			if(isset($row['broker_url']) && !empty($row['broker_url']))
				fputs($f, '		<broker_url>'.$row['broker_url'].'</broker_url>'."\n");
				
			if(isset($row['condition']) && !empty($row['condition']))
				fputs($f, '		<condition>'.implode(', ', $row['condition']).'</condition>'."\n");
				
			if(isset($row['create_time']) && !empty($row['create_time']))
				fputs($f, '		<create_time>'.$row['create_time'].'</create_time>'."\n");
					
			if(isset($row['currency']) && !empty($row['currency']))
				fputs($f, '		<currency>'.$row['currency'].'</currency>'."\n");
					
			if(isset($row['event_date']) && !empty($row['event_date']))
				fputs($f, '		<event_date>'.$row['event_date'].'</event_date>'."\n");
					
			if(isset($row['expire_time']) && !empty($row['expire_time']))
				fputs($f, '		<expire_time>'.$row['expire_time'].'</expire_time>'."\n");
				
			if(isset($row['facing']) && !empty($row['facing']))
				fputs($f, '		<facing>'.implode(', ', $row['facing']).'</facing>'."\n");
					
			if(isset($row['featured']))
				fputs($f, '		<featured>'.$row['featured'].'</featured>'."\n");
					
			if(isset($row['fee']))
				fputs($f, '		<fee>'.$row['fee'].'</fee>'."\n");
				
			if(isset($row['furnished']) && !empty($row['furnished']))
				fputs($f, '		<furnished>'.$row['furnished'].'</furnished>'."\n");

			if(isset($row['image_url']) && !empty($row['image_url']))
				fputs($f, '		<image_url>'.$row['image_url'].'</image_url>'."\n");
					
			if(isset($row['ip_address']) && !empty($row['ip_address']))
				fputs($f, '		<ip_address>'.$row['ip_address'].'</ip_address>'."\n");
				
			if(isset($row['lead_email']) && !empty($row['lead_email']))
				fputs($f, '		<lead_email>'.$row['lead_email'].'</lead_email>'."\n");
					
			if(isset($row['lot_size']) && !empty($row['lot_size']))
				fputs($f, '		<lot_size>'.$row['lot_size'].'</lot_size>'."\n");
				
			if(isset($row['lot_size_units']) && !empty($row['lot_size_units']))
				fputs($f, '		<lot_size_units>'.$row['lot_size_units'].'</lot_size_units>'."\n");
				
			if(isset($row['mls_id']) && !empty($row['mls_id']))
				fputs($f, '		<mls_id>'.$row['mls_id'].'</mls_id>'."\n");
				
			if(isset($row['price']) && !empty($row['price']))
				fputs($f, '		<price>'.$row['price'].'</price>'."\n");
					
			if(isset($row['registration']))
				fputs($f, '		<registration>'.$row['registration'].'</registration>'."\n");
					
			if(isset($row['secondary_source']) && !empty($row['secondary_source']))
				fputs($f, '		<secondary_source>'.$row['secondary_source'].'</secondary_source>'."\n");
			
			if(isset($row['seller_email']) && !empty($row['seller_email']))
				fputs($f, '		<seller_email>'.$row['seller_email'].'</seller_email>'."\n");
					
			if(isset($row['seller_name']) && !empty($row['seller_name']))
				fputs($f, '		<seller_name>'.$row['seller_name'].'</seller_name>'."\n");
					
			if(isset($row['seller_phone']) && !empty($row['seller_phone']))
				fputs($f, '		<seller_phone>'.$row['seller_phone'].'</seller_phone>'."\n");
					
			if(isset($row['seller_type']) && !empty($row['seller_type']))
				fputs($f, '		<seller_type>'.$row['seller_type'].'</seller_type>'."\n");
				
			if(isset($row['seller_url']) && !empty($row['seller_url']))
				fputs($f, '		<seller_url>'.$row['seller_url'].'</seller_url>'."\n");
			
			if(isset($row['square_feet']) && !empty($row['square_feet']))
				fputs($f, '		<square_feet>'.$row['square_feet'].'</square_feet>'."\n");
				
			if(isset($row['vastu_compliant']))
				fputs($f, '		<vastu_compliant>'.$row['vastu_compliant'].'</vastu_compliant>'."\n");
					
			if(isset($row['year']) && !empty($row['year']))
				fputs($f, '		<year>'.$row['year'].'</year>'."\n");

			fputs($f, '	</listing>'."\n");
		}
		fputs($f, '</listings>'."\n");
		fclose($f);
	}
	
	/**
	 * Validate export item data
	 * 
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_data($data){
		$return = array("errors" => array(), "data" => array());
		
		if(isset($data['category'])){
			$return['data']['category'] = $data['category'];
			if(!in_array($data['category'], $this->categories)){
				if(isset($data['category2'])){
					$return['data']['category'] = $data['category2'];
					if(!in_array($data['category'], $this->categories)){
						if(isset($data['category3'])){
							$return['data']['category'] = $data['category3'];
							if(!in_array($data['category'], $this->categories)){
								if(isset($data['category4'])){
									$return['data']['category'] = $data['category4'];
									if(!in_array($data['category'], $this->categories)){
										if(isset($data['category5'])){
											$return['data']['category'] = $data['category5'];
											if(!in_array($data['category'], $this->categories)){
												if(isset($data['category6'])){
													$return['data']['category'] = $data['category6'];
												}else{
													$return['data']['category'] = current($this->categories);
													//$return['errors'][] = 'category';
												}
											}
										}else{
											$return['data']['category'] = current($this->categories);
											//$return['errors'][] = 'category';
										}
									}
								}else{
									$return['data']['category'] = current($this->categories);
									//$return['errors'][] = 'category';
								}
							}
						}else{
							$return['data']['category'] = current($this->categories);
							//$return['errors'][] = 'category';
						}
					}
				}else{
					$return['data']['category'] = current($this->categories);
					//$return['errors'][] = 'category';
				}
			}
		}else{
			$return['data']['category'] = current($this->categories);
				//$return['errors'][] = 'category';
		}
		
		if(isset($data['description'])){
			$return['data']['description'] = str_replace($this->search, $this->replace, $data['description']);
		}		
		
		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
		}	
		
		if(isset($data['title'])){
			$return['data']['title'] = str_replace($this->search, $this->replace, $data['title']);
		}			
		
		if(isset($data['url'])){
			$return['data']['url'] = str_replace($this->search, $this->replace, $data['url']);
		}	
		
		if(isset($data['address'])){
			$return['data']['address'] = str_replace($this->search, $this->replace, $data['address']);
		}			
		
		if(isset($data['city'])){
			$return['data']['city'] = str_replace($this->search, $this->replace, $data['city']);
		}		
		
		if(isset($data['country'])){
			$return['data']['country'] = str_replace($this->search, $this->replace, $data['country']);
		}
		
		if(isset($data['latitude'])){
			$return['data']['latitude'] = floatval($data['latitude']);
		}			
		
		if(isset($data['longitude'])){
			$return['data']['longitude'] = floatval($data['longitude']);
		}		
		
		if(isset($data['neighborhood'])){
			$return['data']['neighborhood'] = str_replace($this->search, $this->replace, $data['neighborhood']);
		}	
		
		if(isset($data['state'])){
			$return['data']['state'] = str_replace($this->search, $this->replace, $data['state']);
		}			
		
		if(isset($data['zip_code'])){
			$return['data']['zip_code'] = str_replace($this->search, $this->replace, $data['zip_code']);
		}		
		
		if(isset($data['agent'])){
			$return['data']['agent'] = str_replace($this->search, $this->replace, $data['agent']);
		}
		
		if(isset($data['agent_email'])){
			$return['data']['agent_email'] = str_replace($this->search, $this->replace, $data['agent_email']);
		}			
		
		if(isset($data['agent_phone'])){
			$return['data']['agent_phone'] = str_replace($this->search, $this->replace, $data['agent_phone']);
		}			
			
		if(isset($data['agent_url'])){
			$return['data']['agent_url'] = str_replace($this->search, $this->replace, $data['agent_url']);
		}			
		
		if(isset($data['amenities.AC'])){
			if($data['amenities.AC']){
				$return['data']['amenities'][] = 'AC';
			}
		}
		
		if(isset($data['amenities.alarm'])){
			if($data['amenities.alarm']){
				$return['data']['amenities'][] = 'alarm';
			}
		}
		
		if(isset($data['amenities.basketball'])){
			if($data['amenities.basketball']){
				$return['data']['amenities'][] = 'basketball';
			}
		}
		
		if(isset($data['amenities.cable'])){
			if($data['amenities.cable']){
				$return['data']['amenities'][] = 'cable';
			}
		}
		
		if(isset($data['amenities.clubhouse'])){
			if($data['amenities.clubhouse']){
				$return['data']['amenities'][] = 'clubhouse';
			}
		} 
		
		if(isset($data['amenities.dishwasher'])){
			if($data['amenities.dishwasher']){
				$return['data']['amenities'][] = 'dishwasher';
			}
		}
		
		if(isset($data['amenities.doorman'])){
			if($data['amenities.doorman']){
				$return['data']['amenities'][] = 'doorman';
			}
		}
		
		if(isset($data['amenities.elevator'])){
			if($data['amenities.elevator']){
				$return['data']['amenities'][] = 'elevator';
			}
		}
		
		if(isset($data['amenities.fireplace'])){
			if($data['amenities.fireplace']){
				$return['data']['amenities'][] = 'fireplace';
			}
		}
		
		if(isset($data['amenities.gated'])){
			if($data['amenities.gated']){
				$return['data']['amenities'][] = 'gated';
			}
		}
		
		if(isset($data['amenities.gym'])){
			if($data['amenities.gym']){
				$return['data']['amenities'][] = 'gym';
			}
		}
		
		if(isset($data['amenities.hottub'])){
			if($data['amenities.hottub']){
				$return['data']['amenities'][] = 'hot tub';
			}
		}
		
		if(isset($data['amenities.internet'])){
			if($data['amenities.internet']){
				$return['data']['amenities'][] = 'internet';
			}
		}
		
		if(isset($data['amenities.parking'])){
			if($data['amenities.parking']){
				$return['data']['amenities'][] = 'parking';
			}
		}
		
		if(isset($data['amenities.patio'])){
			if($data['amenities.patio']){
				$return['data']['amenities'][] = 'patio';
			}
		}	
		
		if(isset($data['amenities.playground'])){
			if($data['amenities.playground']){
				$return['data']['amenities'][] = 'playground';
			}
		}
		
		if(isset($data['amenities.pool'])){
			if($data['amenities.pool']){
				$return['data']['amenities'][] = 'pool';
			}
		}
		
		if(isset($data['amenities.refrigerator'])){
			if($data['amenities.refrigerator']){
				$return['data']['amenities'][] = 'refrigerator';
			}
		}
		
		if(isset($data['amenities.satellite'])){
			if($data['amenities.satellite']){
				$return['data']['amenities'][] = 'satellite';
			}
		}
		
		if(isset($data['amenities.storage'])){
			if($data['amenities.storage']){
				$return['data']['amenities'][] = 'storage';
			}
		}
		
		if(isset($data['amenities.TV'])){
			if($data['amenities.TV']){
				$return['data']['amenities'][] = 'TV';
			}
		}
		
		if(isset($data['amenities.tennis'])){
			if($data['amenities.tennis']){
				$return['data']['amenities'][] = 'tennis';
			}
		}
		
		if(isset($data['amenities.washer_dryer'])){
			if($data['amenities.washer_dryer']){
				$return['data']['amenities'][] = 'washer dryer';
			}
		}
		
		if(isset($data['amenities.wood_floors'])){
			if($data['amenities.wood_floors']){
				$return['data']['amenities'][] = 'wood floors';
			}
		}
		
		if(isset($data['balconies'])){
			$return['data']['balconies'] = intval($data['balconies']);
		}		
		
		if(isset($data['bathrooms'])){
			$return['data']['bathrooms'] = intval($data['bathrooms']);
		}
		
		if(isset($data['bedrooms'])){
			$return['data']['bedrooms'] = intval($data['bedrooms']);
		}	
		
		if(isset($data['broker'])){
			$return['data']['broker'] = str_replace($this->search, $this->replace, $data['broker']);
		}		
		
		if(isset($data['broker_email'])){
			$return['data']['broker_email'] = str_replace($this->search, $this->replace, $data['broker_email']);
		}	
					
		if(isset($data['broker_phone'])){
			$return['data']['broker_phone'] = str_replace($this->search, $this->replace, $data['broker_phone']);
		}			
		
		if(isset($data['broker_url'])){
			$return['data']['broker_url'] = str_replace($this->search, $this->replace, $data['broker_url']);
		}
		
		if(isset($data['condition.pre-construction'])){
			if($data['condition.pre-construction']){
				$return['data']['condition'][] = 'pre-construction';
			}
		}
		
		if(isset($data['condition.new'])){
			if($data['condition.new']){
				$return['data']['condition'][] = 'new';
			}
		}		
		
		if(isset($data['condition.existing'])){
			if($data['condition.existing']){
				$return['data']['condition'][] = 'existing';
			}
		}
		
		if(isset($data['condition.resale'])){
			if($data['condition.resale']){
				$return['data']['condition'][] = 'resale';
			}
		}
		
		if(isset($data['create_time'])){
			$value = strtotime($data['create_time']);
			if($value > 0) $return['data']['create_time'] = date('c', $value);
		}		
		
		if(isset($data['currency'])){
			$return['data']['currency'] = str_replace($this->search, $this->replace, $data['currency']);
		}			
		
		if(isset($data['event_date'])){
			$value = strtotime($data['event_date']);
			if($value > 0) $return['data']['event_date'] = date('Y-m-d', $value);
		}			
		
		if(isset($data['expire_time'])){
			$value = strtotime($data['expire_time']);
			if($value > 0) $return['data']['expire_time'] = date('c', $value);
		}		
		
		if(isset($data['facing.north'])){
			if($data['facing.north']){
				$return['data']['facing'][] = 'north';
			}
		}		
		
		if(isset($data['facing.northwest'])){
			if($data['facing.northwest']){
				$return['data']['facing'][] = 'northwest';
			}
		}
		
		if(isset($data['facing.west'])){
			if($data['facing.west']){
				$return['data']['facing'][] = 'west';
			}
		}
		
		if(isset($data['facing.southwest'])){
			if($data['facing.southwest']){
				$return['data']['facing'][] = 'southwest';
			}
		}
		
		if(isset($data['facing.south'])){
			if($data['facing.south']){
				$return['data']['facing'][] = 'south';
			}
		}
		
		if(isset($data['facing.southeast'])){
			if($data['facing.southeast']){
				$return['data']['facing'][] = 'southeast';
			}
		}
		
		if(isset($data['facing.east'])){
			if($data['facing.east']){
				$return['data']['facing'][] = 'east';
			}
		}
		
		if(isset($data['facing.northeast'])){
			if($data['facing.northeast']){
				$return['data']['facing'][] = 'northeast';
			}
		}
		
		if(isset($data['featured'])){
			$return['data']['featured'] = $data['featured'] ? 'yes' : 'no';
		}
		
		if(isset($data['fee'])){
			$return['data']['fee'] = $data['fee'] ? 'yes' : 'no';
		}			
		
		if(isset($data['furnished'])){
			if(in_array($data['furnished'], $this->furnished)){
				$return['data']['furnished'] = $data['furnished'];
			}
		}
		
		if(isset($data['image_url'])){
			if(!empty($data['image_url'])){
				$return['data']['image_url'] = $return['data']['image_url'] = array_shift(explode('|', $data['image_url']));
			}
		}
		
		if(isset($data['ip_address'])){
			$return['data']['ip_address'] = str_replace($this->search, $this->replace, $data['ip_address']);
		}
			
		if(isset($data['lead_email'])){
			$return['data']['lead_email'] = str_replace($this->search, $this->replace, $data['lead_email']);
		}		
		
		if(isset($data['lot_size'])){
			$return['data']['lot_size'] = intval($data['lot_size']);
		}		
		
		if(isset($data['lot_size_units'])){
			$return['data']['lot_size_units'] = str_replace($this->search, $this->replace, $data['lot_size_units']);
		}			
				
		if(isset($data['mls_id'])){
			$return['data']['mls_id'] = str_replace($this->search, $this->replace, $data['mls_id']);
		}			
		
		if(isset($data['price'])){
			$return['data']['price'] = intval($data['price']);
		}
		
		if(isset($data['registration'])){
			$return['data']['registration'] = $data['registration'] ? 'yes' : 'no';
		}
		
		if(isset($data['secondary_source'])){
			$return['data']['secondary_source'] = str_replace($this->search, $this->replace, $data['secondary_source']);
		}
				
		if(isset($data['seller_email'])){
			$return['data']['seller_email'] = str_replace($this->search, $this->replace, $data['seller_email']);
		}			
		
		if(isset($data['seller_name'])){
			$return['data']['seller_name'] = str_replace($this->search, $this->replace, $data['seller_name']);
		}			
		
		if(isset($data['seller_phone'])){
			$return['data']['seller_phone'] = str_replace($this->search, $this->replace, $data['seller_phone']);
		}			
		
		if(isset($data['seller_type'])){
			$return['data']['seller_type'] = str_replace($this->search, $this->replace, $data['seller_type']);
		}			
		
		if(isset($data['seller_url'])){
			$return['data']['seller_url'] = str_replace($this->search, $this->replace, $data['seller_url']);
		}			
		
		if(isset($data['square_feet'])){
			$return['data']['square_feet'] = intval($data['square_feet']);
		}		
		
		if(isset($data['vastu_compliant'])){
			$return['data']['vastu_compliant'] = $data['vastu_compliant'] ? 'yes' : 'no';
		}	
				
		if(isset($data['year'])){
			if(strlen($data['year']) == 4){
				$return['data']['year'] = intval($data['year']);
			}
		}			

		return $return;
	}
}

<?php  

if(!defined("BASEPATH")) exit("No direct script access allowed");

define("EXPORT_DRIVERS_TABLE", DB_PREFIX."export_drivers");

/**
 * Export driver model
 * 
 * @package PG_RealEstate
 * @subpackage Export
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Export_driver_model extends Model{
	/**
	 * Link to CodeIgniter model
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Attributes of export object
	 * 
	 * @var array
	 */
	private $_fields = array(
		"id", 
		"gid", 
		"link",
		"settings",
		"elements",
		"date_created", 
		"date_modified", 
		"editable",
		"status",
	);

	/**
	 * Cache driver objects
	 * 
	 * @var array
	 */
	private $driver_cache = array();
	
	/**
	 * Field types
	 * 
	 * @var array
	 */
	public $field_types = array("text", "int", "file", "url");

	/**
	 * Constructor
	 * 
	 * @return Export_driver_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return driver object by guid
	 * 
	 * @param string $driver_gid driver guid
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_driver_by_gid($driver_gid, $formatted=true){
		if(empty($this->driver_cache[$driver_gid])){
			$this->DB->select(implode(", ", $this->_fields))->from(EXPORT_DRIVERS_TABLE)->where("gid", $driver_gid);
			$results = $this->DB->get()->result_array();
			if(!empty($results) && is_array($results)){
				if($formatted){
					$this->driver_cache[$driver_gid] = $this->format_driver($results[0]);
				}else{
					return $results[0];
				}
			}
		}
		return $this->driver_cache[$driver_gid];
	}

	/**
	 * Return export driver objects as array
	 * 
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_drivers($formatted=true){
		$data = array();
		$this->DB->select(implode(", ", $this->_fields))->from(EXPORT_DRIVERS_TABLE);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $result){
				if($formatted){
					$data[] = $this->format_driver($result);
				}else{
					$data[] = $result;
				}
			}
		}
		return $data;
	}
	
	/**
	 * Save driver object to data source
	 * 
	 * @param string $driver_gid driver GUID
	 * @param array $data
	 * @return void
	 */
	public function save_driver($driver_gid, $data){
		if(!$driver_gid){
			$data["date_created"] = date("Y-m-d H:i:s");
			$data["date_modified"] = date("Y-m-d H:i:s");
			$this->DB->insert(EXPORT_DRIVERS_TABLE, $data);		
			$this->DB->insert_id();
		}else{
			$this->DB->where("gid", $driver_gid);
			$this->DB->update(EXPORT_DRIVERS_TABLE, $data);
		}		
	}
	
	/**
	 * Remove export driver object by guid
	 * 
	 * @param string $driver_gid driver guid
	 * @return void
	 */
	public function remove_driver($driver_gid){
		$this->DB->where("gid", $driver_gid);
		$this->DB->delete(EXPORT_DRIVERS_TABLE);
	}
	
	/**
	 * Activate export driver object
	 * 
	 * @param string $driver_gid driver guid
	 * @return void
	 */
	public function activate_driver($driver_gid){
		$data["status"] = "1";
		$this->save_driver($driver_gid, $data);
	}
	
	/**
	 * De-activate export driver object
	 * 
	 * @param string $driver_gid driver guid
	 * @return void
	 */
	public function deactivate_driver($driver_gid){
		$data["status"] = "0";
		$this->save_driver($driver_gid, $data);
	}

	/**
	 * Validate export driver data
	 * 
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_driver($driver_gid, $data){
		$return = array("errors"=> array(), "data" => array());
		
		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['gid'])){
			$return['data']['gid'] = trim(strip_tags($data["gid"]));
			if(empty($return['data']['gid'])) $return["errors"][] = l("error_empty_gid", "export");
		}elseif(!$driver_gid){
			$return["errors"][] = l("error_empty_gid", "export");
		}
		
		if(isset($data['link'])){
			$return['data']['link'] = trim(strip_tags($data["link"]));
			if(empty($return['data']['link'])) unset($return['data']['link']);
		}
		
		if(isset($data["settings"])){
			if(!is_array($data["settings"])) $data["settings"] = array();
			$return["data"]["settings"] = serialize($data["settings"]);
		}
		
		if(isset($data["elements"])){
			if(!is_array($data["elements"])) $data["elements"] = array();
			$return["data"]["elements"] = serialize($data["elements"]);
			foreach($data['elements'] as $field_data){
				if(empty($field_data['name'])) $return["errors"][] = l("error_empty_field_name", "export");
				if(empty($field_data['type'])){
					$return["errors"][] = l("error_empty_field_type", "export");
				}elseif(!in_array($field_data['type'], $this->field_types)){
					$return["errors"][] = l("error_invalid_field_type", "export");
				}
			}
		}

		if(isset($data['date_created'])){
			$value = strtotime($data['date_created']);
			if($value > 0) $return['data']['date_created'] = date("Y-m-d", $value);
		}
		
		if(isset($data['date_modified'])){
			$value = strtotime($data['date_modified']);
			if($value > 0) $return['data']['date_modified'] = date("Y-m-d", $value);
		}
		
		if(isset($data["editable"])){
			$return["data"]["editable"] = intval($data["editable"]);
		}
		
		if(isset($data["status"])){
			$return["data"]["status"] = intval($data["status"]);
		}

		return $return;
	}
	
	/**
	 * Format export driver data
	 * 
	 * @param array $data driver data
	 * @return array
	 */
	public function format_driver($data){
		$data["output_name"] = l("driver_name_".$data["gid"], "export");
		$data["settings"] = $data["settings"] ? (array)unserialize($data["settings"]) : array();
		$data["elements"] = $data["elements"] ? (array)unserialize($data["elements"]) : array();
		return $data;
	}
	
	/**
	 * Return default export driver format 
	 * 
	 * @param array $driver_gid driver guid
	 * @return array
	 */
	public function format_default_driver($driver_gid){
		return null;
	}
	
	/**
	 * Return default filename
	 * 
	 * @return string
	 */
	public function get_filename(){
		return "export.txt";
	}
	
	/**
	 * Generate output file
	 * 
	 * @param string $filename output filename
	 * @param array $data export data
	 * @param array $settings driver settings
	 * @return void
	 */
	public function generate($filename, $data, $settings){
		
	}
}

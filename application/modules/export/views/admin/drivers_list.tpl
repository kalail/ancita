{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_export_menu'}
<div class="actions">
	&nbsp;
</div>

<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first">{l i='field_driver_name' gid='export'}</th>
	<th class="w50">{l i='field_driver_status' gid='export'}</th>
	<th class="w50">{l i='field_driver_link' gid='export'}</th>
	<th class="w70">&nbsp;</th>
</tr>
{foreach item=item from=$drivers}
{counter print=false assign=counter}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td>{$item.output_name|truncate:100}</td>
	<td class="center">
		{if $item.status}
		<a href="{$site_url}admin/export/activate_driver/{$item.gid}/0"><img src="{$site_root}{$img_folder}icon-full.png" width="16" height="16" border="0" alt="{l i='link_deactivate_driver' gid='export' type='button'}" title="{l i='link_deactivate_driver' gid='export' type='button'}">
		{else}
		<a href="{$site_url}admin/export/activate_driver/{$item.gid}/1"><img src="{$site_root}{$img_folder}icon-empty.png" width="16" height="16" border="0" alt="{l i='link_activate_driver' gid='export' type='button'}" title="{l i='link_activate_driver' gid='export' type='button'}"></a>
		{/if}
	</td>
	<td class="center">
		<a href="{$item.link}" target="_blank">
			{if $item.need_regkey}
				{l i='driver_registration' gid='export'}
			{else}
				{l i='driver_info' gid='export'}
			{/if}
		</a>
	</td>
	<td class="icons">
		{if $item.editable}
		<a href="{$site_url}admin/export/driver_fields/{$item.gid}"><img src="{$site_root}{$img_folder}icon-list.png" width="16" height="16" border="0" alt="{l i='link_driver_fields' gid='export' type='button'}" title="{l i='link_driver_fields' gid='export' type='button'}"></a>
		{if $item.settings|count}<a href="{$site_url}admin/export/edit_driver/{$item.gid}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_driver' gid='export' type='button'}" title="{l i='link_edit_driver' gid='export' type='button'}"></a>{/if}
		{/if}
	</td>
</tr>
{foreachelse}
<tr><td colspan="5" class="center">{l i='no_drivers' gid='export'}</td></tr>
{/foreach}
</table>
{include file="pagination.tpl"}

{include file="footer.tpl"}

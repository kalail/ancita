<?php

/**
 * Export admin side controller
 * 
 * @package PG_RealEstate
 * @subpackage Export
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Admin_Export extends Controller{
	
	/**
	 * Constructor
	 *
	 * @return Admin_Export
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model("Menu_model");
		$this->Menu_model->set_menu_active_item("admin_menu", "exp-import-items");
		$this->system_messages->set_data("header", l("admin_header_export", "export"));
	}
	
	/**
	 * Render selections list action
	 * @param string $order sorting field
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 */
	public function index($order="date_created", $order_direction="DESC", $page=1){
		$this->load->model("Export_model");
		$this->load->model("export/models/Export_driver_model");
		$this->load->model("export/models/Export_module_model");
		
		$filters = array();
		
		$current_settings = isset($_SESSION["export_list"]) ? $_SESSION["export_list"] : array();
				
		$current_settings["filters"] = $filters;
				
		if(!isset($current_settings["order"]))
			$current_settings["order"] = "date_add";
		if(!isset($current_settings["order_direction"]))
			$current_settings["order_direction"] = "DESC";
		if (!isset($current_settings["page"]))
			$current_settings["page"] = 1;		
		
		if (!$order) $order = $current_settings["order"];
		$this->template_lite->assign("order", $order);
		$current_settings["order"] = $order;

		if (!$order_direction) $order_direction = $current_settings["order_direction"];
		$this->template_lite->assign("order_direction", $order_direction);
		$current_settings["order_direction"] = $order_direction;

		$selections_count = $this->Export_model->get_selections_count($filters);
		
		if(!$page) $page = $current_settings["page"];
		$items_on_page = $this->pg_module->get_module_config("start", "admin_items_per_page");
			
		$this->load->helper("sort_order");
		$page = get_exists_page_number($page, $selections_count, $items_on_page);
		$current_settings["page"] = $page;

		$_SESSION["export_list"] = $current_settings;

		$sort_links = array(
			"date_created" => site_url() . "admin/export/index/date_created/".(($order != "date_created" xor $order_direction == "DESC") ? "ASC" : "DESC"),
		);		
		$this->template_lite->assign("sort_links", $sort_links);

		if($selections_count > 0){
			$selections = $this->Export_model->get_selections_list($filters, $page, $items_on_page, array($order => $order_direction));
			$this->template_lite->assign("selections", $selections);
		}
		
		$this->load->helper('navigation');
		
		$url = site_url()."admin/export/index/".$driver_gid."/".$module_id."/".$order."/".$order_direction."/";
		$page_data = get_admin_pages_data($url, $selections_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $this->pg_date->get_format('date_time_literal', 'st');
		$this->template_lite->assign("page_data", $page_data);
		
		$this->template_lite->assign("driver_gid", $driver_gid);
		$this->template_lite->assign("module_id", $module_id);
		
		$this->Menu_model->set_menu_active_item("admin_export_menu", "export_selections_items");
		$this->template_lite->view("list");
	}
	
	/**
	 * Edit selection action
	 * @param integer $selection_id selection identifier
	 * @param string $section_gid section guid
	 */
	public function edit($selection_id=null, $section_gid="general"){
		
		$this->load->model("Export_model");
		$this->load->model("export/models/Export_module_model");

		if($selection_id){
			$data = $this->Export_model->get_selection_by_id($selection_id, true);
		}else{
			$this->load->model("export/models/Export_driver_model");
			
			$drivers = $this->Export_driver_model->get_drivers();
			foreach($drivers as $key=>$driver){
				if(!$driver["status"]) unset($drivers[$key]);
			}
			$this->template_lite->assign("drivers", $drivers);		
			
			$modules = $this->Export_module_model->get_modules();
			$this->template_lite->assign("modules", $modules);		
			
			$data = array();
		}

		if($this->input->post("btn_save")){
			$post_data = (array)$this->input->post("data", true);
			$scheduler = (array)$this->input->post("scheduler", true);
			if(isset($post_data["scheduler"]["type"])){
				if(isset($scheduler[$post_data["scheduler"]["type"]])){
					$post_data["scheduler"] = array_merge($post_data["scheduler"], $scheduler[$post_data["scheduler"]["type"]]);
				}
			}

			$validate_data = $this->Export_model->validate_selection($selection_id, $post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
			}else{
				if($selection_id){
					$this->Export_model->save_selection($selection_id, $validate_data["data"]);
					$this->system_messages->add_message("success", l("success_selection_updated", "export"));
				}else{
					$selection_id = $this->Export_model->save_selection(null, $validate_data["data"]);
					$this->system_messages->add_message("success", l("success_selection_created", "export"));
					
				}	
				$url = site_url()."admin/export/edit/".$selection_id;
				redirect($url);
			}
			
			$data = array_merge_recursive($data, $post_data);
		}
		
		switch($section_gid){
			case "general":
				//output
				$output_types = $this->Export_model->output_types;
				$this->template_lite->assign("output_types", $output_types);
				//time
				$this->template_lite->assign("hours", range(0, 24, 1));
				$this->template_lite->assign("minutes", range(0, 50, 10));
			break;
			case "custom_fields":
				if($data["driver"]["editable"]){
					foreach($data["driver"]["elements"] as $index=>$item){
						if(isset($data["relations"][$index]) && $data["relations"][$index]["name"] == $item["name"]) 
							unset($data["driver"]["elements"][$index]);
					}	
					$custom_fields = $this->Export_module_model->get_module_fields($data["id_object"]);
					
					foreach($data["relations"] as $index=>$item){
						foreach($custom_fields as $custom_field){
							if($item['link'] != $custom_field['name']) continue;
							$data["relations"][$index]['label'] = $custom_field['label'];
						}
					}			
					if(!empty($data["driver"]["elements"])){
						$element = current($data["driver"]["elements"]);
						foreach($custom_fields as $i=>$field){
							if($element["type"] == $field["type"]) continue;
							unset($custom_fields[$i]);
						}
						$this->template_lite->assign("custom_fields", $custom_fields);
					}
				}else{
					$custom_fields =  $this->Export_module_model->get_module_fields($data["id_object"]);
					foreach($data["driver"]["elements"] as $index=>$element){
						$data["driver"]["elements"][$index]["custom_fields"] = array();
						foreach($custom_fields as $i=>$field){
							if($element["type"] != $field["type"]) continue;
							$data["driver"]["elements"][$index]["custom_fields"][$i] = $field;
						}
					}
				}
				$this->template_lite->assign("driver_fields", $data["driver"]["elements"]);
			break;
			case "search_form":
				$search_form = $this->Export_module_model->get_search_form($data["id_object"], $data["admin_form_data"], true);
				$this->template_lite->assign("search_form", $search_form);
			break;
		}
		
		$this->template_lite->assign("data", $data);
		$this->template_lite->assign("section_gid", $section_gid);

		$this->system_messages->set_data("header", l("admin_header_selections_edit", "export"));
		$this->template_lite->view("edit");
	}
	
	/**
	 * Remove selection action
	 * @param integer $selection_id selection identifier
	 */
	public function delete($ids=null){
		if(!$ids) $ids = $this->input->post("ids");
		if(!empty($ids)){
			$this->load->model("Export_model");
			foreach((array)$ids as $id){
				$this->Export_model->delete_selection($id);
			}
			$this->system_messages->add_message("success", l("success_selection_deleted", "export"));
		}
		$url = site_url()."admin/export";
		redirect($url);
	}
	
	/**
	 * Load custom fields
	 */
	public function ajax_get_module_fields($selection_id){
		$this->load->model("Export_model");
		
		$index = $this->input->post("index");
		
		$data = $this->Export_model->get_selection_by_id($selection_id, true);
		
		if(!isset($data["driver"]["elements"][$index])){echo json_encode(array()); exit;}
		
		$element = $data["driver"]["elements"][$index];

		$custom_fields = $this->Export_module_model->get_module_fields($data["id_object"]);
		foreach($custom_fields as $i=>$field){
			if($element["type"] == $field["type"]) continue;
			if(($element["type"]=="text" || $element["type"]=="int") && $field["type"] == "select") continue;
			unset($custom_fields[$i]);
		}
		
		echo json_encode($custom_fields); 
		exit;
	}
	
	/**
	 * Create relation action
	 * @param integer $selection_id selection identifier
	 */
	public function create_relation($selection_id){
		$this->load->model("Export_model");
		$data = $this->Export_model->get_selection_by_id($selection_id, true);
		$custom_fields = $this->Export_module_model->get_module_fields($data["id_object"]);
		$relation_name_index = $this->input->post("name", true);
		$relation_link_index = $this->input->post("link", true);
		$data["relations"][$relation_name_index] = array(
			"name" => $data["driver"]["elements"][$relation_name_index]["name"],
			"type" => $data["driver"]["elements"][$relation_name_index]["type"],
			"link" => $custom_fields[$relation_link_index]["name"],
			"label" => $custom_fields[$relation_link_index]["label"],
		);
		if($relation_link_index != -1){
			$validate_data = $this->Export_model->validate_selection($selection_id, $data);
		}else{
			$validate_data = array("errors", l("error_empty_link", "export"));
		}
		if(!empty($validate_data["errors"])){
			$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
		}else{
			$this->Export_model->save_selection($selection_id, $validate_data["data"]);
			$this->system_messages->add_message("success", l("success_relation_created", "export"));
		}
		$url = site_url()."admin/export/edit/".$selection_id."/custom_fields";
		redirect($url);
	}
	
	/**
	 * Save relations action
	 * @param integer $selection_id selection identifier
	 */
	public function save_relations($selection_id){
		
		$this->load->model("Export_model");
		$data = $this->Export_model->get_selection_by_id($selection_id, true);
		$custom_fields = $this->Export_module_model->get_module_fields($data["id_object"]);
		$links = $this->input->post("link", true);

		foreach($data["driver"]["elements"] as $index => $element){
			if(isset($links[$index]) && $links[$index] != -1){
				$data["relations"][$index] = array(
					"name" => $data["driver"]["elements"][$index]["name"],
					"type" => $data["driver"]["elements"][$index]["type"],
					"link" => $custom_fields[$links[$index]]["name"],
					"label" => $custom_fields[$links[$index]]["label"],
				);
			}elseif(isset($data["relations"][$index])){
				unset($data["relations"][$index]);
			}			
		}
		$validate_data = $this->Export_model->validate_selection($selection_id, $data);
		if(!empty($validate_data["errors"])){
			$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
			$url = site_url()."admin/export/edit/".$selection_id."/custom_fields";
		}else{
			$this->Export_model->save_selection($selection_id, $validate_data["data"]);
			$this->system_messages->add_message("success", l("success_relation_created", "export"));
			$url = site_url()."admin/export";
		}
		redirect($url);
	}
	
	/**
	 * Remove relation action
	 * @param integer $selection_id selection identifier
	 */
	public function delete_relation($selection_id, $index){
		$this->load->model("Export_model");
		$data = $this->Export_model->get_selection_by_id($selection_id, true);
		if(isset($data["relations"][$index])) unset($data["relations"][$index]);
		$validate_data = $this->Export_model->validate_selection($selection_id, $data);
		
		if(!empty($validate_data["errors"])){
			$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
		}else{
			$this->Export_model->save_selection($selection_id, $validate_data["data"]);
			$this->system_messages->add_message("success", l("success_relation_deleted", "export"));
		}
		$url = site_url()."admin/export/edit/".$selection_id."/custom_fields";
		redirect($url);
	}
	
	/**
	 * Save form data action
	 * @param integer $selection_id selection identifier
	 */
	public function save_form_data($selection_id){
		$this->load->model("Export_model");
		$data = $this->Export_model->get_selection_by_id($selection_id, true);
		$data["admin_form_data"] = $this->Export_module_model->process_search_form($data["id_object"], true);
		$validate_data = $this->Export_model->validate_selection($selection_id, $data);
		if(!empty($validate_data["errors"])){
			$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
		}else{
			$this->Export_model->save_selection($selection_id, $validate_data["data"]);
			$this->system_messages->add_message("success", l("success_form_updated", "export"));
		}
		$url = site_url()."admin/export/edit/".$selection_id.'/search_form';
		redirect($url);
	}
	
	/**
	 * Activate/deactivate selection action
	 * @param integer $selection_id selection identifier
	 * @param integer $status activation status
	 */
	public function activate($selection_id, $status){
		$this->load->model("Export_model");
		if($status){
			$return = $this->Export_model->activate_selection($selection_id);
			if(!$return){
				$this->system_messages->add_message("error", l("error_selection_activated", "export"));
			}
		}else{
			$this->Export_model->deactivate_selection($selection_id);
		}		
		redirect(site_url()."admin/export/index");
	}
	
	/**
	 * Generate selection action
	 * @param integer $selection_id selection identifier
	 */
	public function generate($selection_id){
		$this->load->model("Export_model");
		$this->Export_model->generate_selection($selection_id);
		$this->system_messages->add_message("success", l("success_selection_generated", "export"));
		$url = site_url()."admin/export/index";
		redirect($url);
	}
	
	/**
	 * Render driver list action
	 */
	public function drivers(){
		$this->load->model("export/models/Export_driver_model");
		$drivers = $this->Export_driver_model->get_drivers();
		$this->template_lite->assign("drivers", $drivers);
		$this->Menu_model->set_menu_active_item("admin_export_menu", "export_drivers_items");
		$this->template_lite->view("drivers_list");
	}

	/**
	 * Edit driver elements
	 * @param string $driver_gid
	 * @param string $index
	 */
	public function edit_driver($driver_gid){
		$this->load->model("export/models/Export_driver_model");
		
		$driver = $this->Export_driver_model->get_driver_by_gid($driver_gid);
		$this->template_lite->assign("data", $driver["settings"]);
		
		if($this->input->post("btn_save")){
			$post_data["settings"] = $this->input->post("data", true);
			$validate_data = $this->Export_driver_model->validate_driver($driver_gid, $post_data);
			if(!empty($validate_data["error"])){				
				$this->template_lite->assign("data", $validate_data["data"]);
			}else{
				$this->Export_driver_model->save_driver($driver_gid, $validate_data["data"]);
				$this->system_messages->add_message("success", l("success_driver_updated", "export"));
				$url = site_url()."admin/export/drivers/edit_driver/".$driver_gid;
				redirect($url);
			}
		}
		
		$this->template_lite->assign("driver_gid", $driver_gid);
		$this->system_messages->set_data("header", l("admin_header_driver_edit", "export"));
		$this->Menu_model->set_menu_active_item("admin_export_menu", "export_drivers_item");
		$this->template_lite->view("driver_edit");
	}
	
	/**
	 * Edit export driver
	 * @param string $driver_gid
	 */
	public function driver_fields($driver_gid){
		$this->load->model("export/models/Export_driver_model");
		$driver = $this->Export_driver_model->get_driver_by_gid($driver_gid);
		$this->template_lite->assign("driver_data", $driver);
		$this->system_messages->set_data('back_link', site_url()."admin/export/drivers/");
		$this->system_messages->set_data("header", l("admin_header_driver_fields", "export"));
		$this->Menu_model->set_menu_active_item("admin_export_menu", "export_drivers_item");
		$this->template_lite->view("driver_fields");
	}
	
	/**
	 * Edit driver setting
	 * @param string $driver_gid
	 * @param string $index
	 */
	public function edit_driver_field($driver_gid, $index=null){
		$this->load->model("export/models/Export_driver_model");
		if($index != null){
			$add_flag = false;
		}else{
			$add_flag = true;
		}
		$this->template_lite->assign("add_flag", $add_flag);
		
		$driver = $this->Export_driver_model->get_driver_by_gid($driver_gid);
		$this->template_lite->assign("data", $driver["elements"][$index]);
		
		if($this->input->post("btn_save")){
		
			if($add_flag){
				if(!empty($driver["elements"])){
					end($driver["elements"]);
					$index = key($driver["elements"])+1;
				}else{
					$index = 0;
				}
			}			
			
			$post_data["elements"] = $driver["elements"];
			$post_data["elements"][$index] = $this->input->post("data", true);
			
			$validate_data = $this->Export_driver_model->validate_driver($driver_gid, $post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", implode('<br>', $validate_data["errors"]));				
			}else{
				$this->Export_driver_model->save_driver($driver_gid, $validate_data["data"]);
				$message = $add_flag ? "success_field_created" : "success_field_updated";
				$this->system_messages->add_message("success", l($message, "export"));
				$url = site_url()."admin/export/driver_fields/".$driver_gid;
				redirect($url);
			}
			
			$this->template_lite->assign("data", $$post_data["elements"][$index]);
		}
		
		$this->template_lite->assign("types", $this->Export_driver_model->field_types);
		$this->template_lite->assign("driver_gid", $driver_gid);
		$this->system_messages->set_data("header", l("admin_header_driver_field_edit", "export"));
		$this->Menu_model->set_menu_active_item("admin_export_menu", "export_drivers_item");
		$this->template_lite->view("driver_field_edit");
	}
	
	/**
	 * Remove driver setting
	 * @param string $driver_gid
	 * @param string $index
	 */
	public function delete_driver_field($driver_gid, $index){
		$this->load->model("export/models/Export_driver_model");
		$driver = $this->Export_driver_model->get_driver_by_gid($driver_gid, true);
		$save_data = array('elements'=>$driver['elements']);
		if(isset($save_data['elements'][$index])) unset($save_data["elements"][$index]);
		$validate_data = $this->Export_driver_model->validate_driver($driver_gid, $save_data);
		if(!empty($validate_data['errors'])){
			$this->system_messages->add_message("errors", implode('<br>', $validate_data['errors']));
		}else{
			$this->Export_driver_model->save_driver($driver_gid, $validate_data["data"]);
			$this->system_messages->add_message("success", l("success_setting_delete", "export"));
		}
		$url = site_url()."admin/export/driver_fields/".$driver_gid;
		redirect($url);
	}
	
	/**
	 * Activate driver
	 * @param string $dirver_gid driver guid
	 * @param integer $status
	 */
	public function activate_driver($driver_gid, $status=1){
		$this->load->model("export/models/Export_driver_model");
		if($status){
			$this->Export_driver_model->activate_driver($driver_gid);
		}else{
			$this->Export_driver_model->deactivate_driver($driver_gid);
		}		
		redirect(site_url()."admin/export/drivers");
	}
}

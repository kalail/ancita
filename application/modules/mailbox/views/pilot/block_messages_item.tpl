	{foreach item=item from=$messages}
		<li id="message_{$item.id}" class="{$item.message_type}">
			<a href="#" onclick="javascript: if(confirm('{l i='note_delete_message' gid='mailbox' type='js'}')) {literal}{{/literal} mb.delete_message({$item.id}); {literal}}{/literal} return false;" class="btn-link fright"><ins class="fa fa-times fa-lg edge hover"></ins></a>
			<div class="top {if $item.message_type eq 'inbox'}friend{else}owner{/if}">
				<font class="date">{$item.date_add|date_format:$date_format}</font>
				<font class="user">{$item.user_data.output_name}:</font>
			</div>
			<div class="message">{$item.message}</div>
		</li>
	{/foreach}

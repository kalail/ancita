<div class="actions" >
	<a href="#" onclick="javascript: mb.edit_folder(0); return false;" class="btn-link fright"><ins class="fa fa-plus fa-lg edge hover"></ins><span>{l i='add_new_folder' gid='mailbox'}</span></a>
	<a href="{$site_url}mailbox/index" class="btn-link fleft"><ins class="fa fa-arrow-left fa-lg edge hover"></ins>{l i='btn_back' gid='start'}</a>
</div>
<div class="clr"></div>
<div>	
		<table class="list">
		<tr id="sorter_block">
			<th>{l i='field_folder' gid='mailbox'}</th>		
			<th class="w70">{l i='field_chats' gid='mailbox'}</th>		
			<th class="w100">{l i='actions' gid='mailbox'}</th>	
		</tr>
		{foreach item=item from=$folders}
		<tr>
			<td>{$item.name}</td>
			<td>{$item.chats}</td>
			<td class="center">
			{if $item.id_user}
				<a href="#" onclick="javascript: if(confirm('{l i='note_delete_folder' gid='mailbox' type='js'}')) {literal}{{/literal} mb.delete_folder({$item.id}); {literal}}{/literal} return false;" class="btn-link fright"><ins class="fa fa-trash-o fa-lg edge hover"></ins></a>
				<a href="#" onclick="javascript: mb.edit_folder({$item.id}); return false;" class="btn-link fright"><ins class="fa fa-pencil fa-lg edge hover"></ins></a>
			{else}
			<i>{l i='text_default_folder' gid='mailbox'}</i>
			{/if}
			</td>
		</tr>
		{foreachelse}
			<tr>
				<td colspan="5" class="empty">{l i='no_folders' gid='mailbox'}</td>
			</tr>
		{/foreach}
		</table>	

</div>
<div id="pages_block_2">{include file="pagination.tpl"}</div>

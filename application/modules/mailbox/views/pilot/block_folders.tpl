	<select name="folder">
	{foreach item=item key=key from=$folders}
	<option gid="{$item.folder_type}" value="{$item.id}" {if $item.id == $id_active_folder}selected="selected"{/if}>{$item.name|truncate:12:'...':true} ({$item.chats})</option>
	{/foreach}
	</select>
	
	<a href="{$site_url}mailbox/folders" class="btn-link fright"><ins class="fa fa-folder fa-lg edge hover"><ins class="fa fa-plus icon-mini-stack bottomright hover"></ins><ins class="fa fa-plus icon-mini-stack icon-mini-stack-shadow bottomright hover"></ins></ins></a>

<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

define('MAILBOX_FOLDERS_TABLE', DB_PREFIX.'com_folders');
define('MAILBOX_CHATS_TABLE', DB_PREFIX.'com_chats');
define('MAILBOX_MESSAGES_TABLE', DB_PREFIX.'com_messages');

/**
 * Mailbox Model
 *
 * @package PG_RealEstate
 * @subpackage Mailbox
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Mailbox_model extends Model
{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * 
	 * @var object
	 */
	private $DB;
	
	/**
	 * Folder object properties
	 * 
	 * @var array
	 */
	private $fields_folders = array(
		'id',
		'name',
		'folder_type',
		'id_user',
		'date_add',
	);

	/**
	 * Chat object properties
	 * 
	 * @var array
	 */
	private $fields_chats = array(
		'id',
		'id_com_user',
		'id_from_user',
		'new',
		'total',
		'id_folder',
		'id_last_message',
	);

	/**
	 * Message object properties
	 * 
	 * @var array
	 */
	private $fields_messages = array(
		'id',
		'id_chat',
		'message',
		'message_type',
		'is_new',
		'date_add',
	);

	/**
	 * Identifier of folder by default
	 * 
	 * @var integer
	 */
	public $default_folder = 1;
	
	/**
	 * Moderation type of module
	 * 
	 * @var string
	 */
	private $moderation_type = "mailbox";
	
	/**
	 * Settings for formatting chat object
	 * 
	 * @var array
	 */
	private $chats_format_settings = array(
		'use_format' => true,
		'get_last_message' => false,
	);
	
	/**
	 * Settings for formatting message object
	 * 
	 * @var array
	 */
	private $messages_format_settings = array(
		'use_format' => true,
		'get_user' => true,
	);
	
	/**
	 * Constructor
	 *
	 * @return Mailbox object
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return folder object by identifier
	 * 
	 * @param integer $id_folder folder identifier
	 * @return array
	 */
	public function get_folder_by_id($id_folder){
		$this->DB->select(implode(", ", $this->fields_folders))->from(MAILBOX_FOLDERS_TABLE)->where("id", $id_folder);
		$result = $this->DB->get()->result_array();
		if(!empty($result)){
			$format = $this->format_folders(array($result[0]));
			return $format[0];
		}
		return array();
	}

	/**
	 * Return folders of user
	 * 
	 * @param integer $id_user user identifier
	 * @return array
	 */
	public function get_user_folders($id_user){
		$this->DB->select(implode(", ", $this->fields_folders))->from(MAILBOX_FOLDERS_TABLE);
		$this->DB->where("(id_user='0' AND folder_type IN ('default', 'trash', 'favorites')) OR (id_user='".$id_user."' AND folder_type='user')");
		$this->DB->order_by("id ASC");

		$result = $this->DB->get()->result_array();
		$folders = array();
		foreach($result as $folder){
			$folders[$folder['id']] = $folder;
		}
		if(!empty($result)){
			return $this->format_folders($folders, $id_user);
		}
		return array();
	}

	/**
	 * Validate folder object for saving to data source
	 * 
	 * @param integer $id folder identifier
	 * @param integer $id_user user identifier
	 * @param array $data folder data
	 * @return array
	 */
	public function validate_folder($id, $id_user, $data){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data["name"])){
			$return["data"]["name"] = trim(strip_tags($data["name"]));
			if(empty($return["data"]["name"])){
				$return["errors"][] = l('error_name_incorrect', 'mailbox');
			}
		}

		if($id){
			$folder_data = $this->get_folder_by_id($id);
			if($folder_data["id_user"] != $id_user){
				$return["errors"][] = l('error_user_is_not_owner', 'mailbox');
			}

			if($folder_data["folder_type"] != 'user'){
				$return["errors"][] = l('error_user_has_not_permissions', 'mailbox');
			}
		}
		return $return;
	}

	/**
	 * Save folder object to data source
	 * 
	 * @param integer $id folder idnetifier
	 * @param array $data folder data
	 * @return integer
	 */
	public function save_folder($id, $data){
		if (empty($id)){
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(MAILBOX_FOLDERS_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where('id', $id);
			$this->DB->update(MAILBOX_FOLDERS_TABLE, $data);
		}
		return $id;
	}

	/**
	 * Remove folder object from data source
	 * 
	 * @param integer $id folder identifier
	 * @return void
	 */
	public function delete_folder($id){
		$this->DB->where('id', $id);
		$this->DB->delete(MAILBOX_FOLDERS_TABLE);

		$this->DB->where('id_folder', $id);
		$this->DB->delete(MAILBOX_CHATS_TABLE);
		return;
	}

	/**
	 * Format folder object
	 * 
	 * @param array $data folders data
	 * @param integer $id_user user identifier
	 * @return array
	 */
	public function format_folders($data, $id_user=null){
		foreach($data as $folder){
			$folder_ids[] = $folder["id"];
		}

		$this->DB->select("id_folder, COUNT(*) AS chats")->from(MAILBOX_CHATS_TABLE);
		$this->DB->where_in("id_folder", $folder_ids);
		if(!empty($id_user)){
			$this->DB->where("id_com_user", $id_user);
		}
		$this->DB->group_by("id_folder");
		$result = $this->DB->get()->result_array();

		$chat_counts = array();
		if(!empty($result)){
			foreach($result as $r){
				$chat_counts[$r["id_folder"]] = $r["chats"];
			}
		}

		foreach($data as $k => $folder){
			if($folder["folder_type"] != "user"){
				$data[$k]["name"] = l('folder_name_'.$folder["folder_type"], 'mailbox');
			}
			$data[$k]["chats"] = isset($chat_counts[$folder["id"]])?$chat_counts[$folder["id"]]:0;
		}
		return $data;
	}

	/////// chat functions
	
	/**
	 * Return chat object by identifier
	 * 
	 * @param integer $id_chat chat identifier
	 * @return array
	 */
	public function get_chat_by_id($id_chat){
		$this->DB->select(implode(", ", $this->fields_chats));
		$this->DB->from(MAILBOX_CHATS_TABLE)->where("id", $id_chat);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data = array($results[0]);
			$data = $this->format_chats($data);
			return $data[0];
		}
		return array();
	}

	/**
	 * Return chat object by linked contacts
	 * 
	 * @param integer $id_com_user user owner identifier
	 * @param integer $id_from_user user contact identifier
	 * @return void
	 */
	public function get_chat($id_com_user, $id_from_user){
		$this->DB->select(implode(", ", $this->fields_chats));
		$this->DB->from(MAILBOX_CHATS_TABLE)->where("id_com_user", $id_com_user)->where('id_from_user', $id_from_user);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data = array($results[0]);
			$data = $this->format_chats($data);
			return $data[0];
		}
		return array();
	}

	/**
	 * Return formatted chats objects as array
	 * 
	 * @param array $params sql criteria
	 * @param integer $page page of results
	 * @param integer $limits items per page
	 * @param array $order_by sorting data
	 * @return array
	 */
	public function get_chats_list($params=array(), $page=null, $limits=null, $order_by=null){
		$this->DB->select(implode(", ", $this->fields_chats));
		$this->DB->from(MAILBOX_CHATS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if (is_array($order_by) && count($order_by) > 0) {
			foreach ($order_by as $field => $dir) {
				if (in_array($field, $this->fields_chats)) {
					$this->DB->order_by($field . " " . $dir);
				}
			}
		} else if ($order_by) {
			$this->DB->order_by($order_by);
		}

		if(!is_null($page) ){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return $this->format_chats($data);
		}
		return array();
	}

	/**
	 * Return number of chats objects
	 * 
	 * @param array $params sql criteria
	 * @return integer
	 */
	public function get_chats_count($params){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(MAILBOX_CHATS_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	/**
	 * Check the chat is exists 
	 * 
	 * @param integer $id_com_user user owner identifier
	 * @param integer $id_form_user user contact identifier
	 * @return array/false
	 */
	public function is_chat_exists($id_com_user, $id_from_user){
		$params["where"]['id_com_user'] = $id_com_user;
		$params["where"]['id_from_user'] = $id_from_user;
		$data = $this->get_chats_list($params);
		if(count($data) > 0){
			return $data[0];
		}else{
			return false;
		}
	}

	/**
	 * Create new chat object
	 * 
	 * @param integer $id_com_user chat owner identifier
	 * @param integer $id_from_user chat contact identifier
	 * @return integer
	 */
	public function create_chat($id_com_user, $id_from_user){
		$data["id_com_user"] = $id_com_user;
		$data["id_from_user"] = $id_from_user;
		$data["id_folder"] = $this->default_folder;
		$this->DB->insert(MAILBOX_CHATS_TABLE, $data);
		$id = $this->DB->insert_id();
		return $id;
	}

	/**
	 * Move chat to the folder
	 * 
	 * @param integer $id_chat chat identifier
	 * @param integer $id_folder folder identifier
	 * @return void
	 */
	public function move_chat($id_chat, $id_folder){
		$data["id_folder"] = intval($id_folder);
		if(is_array($id_chat)){
			$this->DB->where_in('id', $id_chat);
		}else{
			$this->DB->where('id', $id_chat);
		}
		$this->DB->update(MAILBOX_CHATS_TABLE, $data);
		return;
	}

	/**
	 * Remove chat object
	 * 
	 * @param integer $id chat identifier
	 * @return void
	 */
	public function delete_chat($id){
		$this->DB->where('id', $id);
		$this->DB->delete(MAILBOX_CHATS_TABLE);

		$this->DB->where('id_chat', $id);
		$this->DB->delete(MAILBOX_MESSAGES_TABLE);
		return;
	}
	
	/**
	 * Change settings for formatting chat object
	 * 
	 * @param string $name parameter name
	 * @param mixed $value parameter value
	 * @return void
	 */
	public function set_chats_format_settings($name, $value=false){
		if(!is_array($name)) $name = array($name=>$value);
		foreach($name as $key => $item)	$this->chats_format_settings[$key] = $item;
	}
	
	/**
	 * Change settings for formatting message object
	 * 
	 * @param string $name parameter name
	 * @param mixed $value parameter value
	 * @return void
	 */
	public function set_messages_format_settings($name, $value=false){
		if(!is_array($name)) $name = array($name=>$value);
		foreach($name as $key => $item)	$this->messages_format_settings[$key] = $item;
	}

	/**
	 * Format chats objects
	 * 
	 * @param array $data chats data
	 * @return array
	 */
	public function format_chats($data){
		if(!$this->chats_format_settings['use_format'] || empty($data)) return $data;
		
		$users_ids = array();
		$last_messages_ids = array();
		foreach($data as $chat){
			if(!in_array($chat["id_com_user"], $users_ids)) $users_ids[] = $chat["id_com_user"];
			if(!in_array($chat["id_from_user"], $users_ids)) $users_ids[] = $chat["id_from_user"];
		}

		$this->CI->load->model('Users_model');
		$temp = $this->CI->Users_model->get_users_list(null, null, null, array(), $users_ids);
		foreach($temp as $d){
			$user_data[$d["id"]] = $d;
		}

		foreach($data as $k => $chat){
			if(!empty($user_data[$chat["id_from_user"]])){
				$data[$k]["user_data"] = $user_data[$chat["id_from_user"]];
			}
			if(!empty($user_data[$chat["id_com_user"]])){
				$data[$k]["owner_data"] = $user_data[$chat["id_com_user"]];
			}
			
			if($chat['id_last_message']) $last_messages_ids[] = $chat['id_last_message'];
		}
		
		if($this->chats_format_settings['get_last_message'] && !empty($last_messages_ids)){
			$get_user = $this->messages_format_settings['get_user'];
			$this->messages_format_settings['get_user'] = false;
			$this->chats_format_settings['get_last_message'] = false;
			$result = $this->get_messages(array('where_in'=>array('id'=>$last_messages_ids)));
			$this->chats_format_settings['get_last_message'] = true;
			$this->messages_format_settings['get_user'] = $get_user;
			$messages = array();
			foreach($result as $k=>$message){
				$messages[$message['id']] = $message;
			}
			foreach($data as $k=>$chat){
				if(!isset($messages[$chat['id_last_message']])) continue;
				$chat['last_message'] =  $messages[$chat['id_last_message']];
				$data[$k] = $chat;
			}
		}
		
		return $data;
	}

	/**
	 * Update statistics data of chat object
	 * 
	 * @param integer $id chat identifier
	 * @return void
	 */
	public function update_chat_statistic($id){
		$this->DB->select('COUNT(*) AS total_count')->from(MAILBOX_MESSAGES_TABLE)->where('id_chat', $id);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data["total"] = intval($results[0]["total_count"]);
		}else{
			$data["total"] = 0;
		}

		$this->DB->select('COUNT(*) AS new_count')->from(MAILBOX_MESSAGES_TABLE)->where('id_chat', $id)->where("is_new", 1);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data["new"] = intval($results[0]["new_count"]);
		}else{
			$data["new"] = 0;
		}
		
		$this->DB->select('id')->from(MAILBOX_MESSAGES_TABLE)->where('id_chat', $id)->order_by('date_add DESC')->limit(1);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data["id_last_message"] = intval($results[0]["id"]);
		}else{
			$data["id_last_message"] = 0;
		}

		$this->DB->where('id', $id);
		$this->DB->update(MAILBOX_CHATS_TABLE, $data);
	}

	//// messages functions
	
	/**
	 * Return message object by identifier
	 * 
	 * @param integer $id message identifier
	 * @return array
	 */
	public function get_message_by_id($id){
		$this->DB->select(implode(", ", $this->fields_messages))->from(MAILBOX_MESSAGES_TABLE)->where("id", $id);
		$result = $this->DB->get()->result_array();
		if(!empty($result)){
			$format = $this->format_messages(array($result[0]));
			return $format[0];
		}
		return array();
	}

	/**
	 * Create message object
	 * 
	 * @param integer $id_chat chat identifier
	 * @param string $message message text
	 * @param string $message_type message type
	 * @param boolean $is_new message is unread 
	 * @return integer
	 */
	public function create_message($id_chat, $message, $message_type, $is_new){
		$data["id_chat"] = $id_chat;
		$data["message"] = $message;
		$data["message_type"] = $message_type;
		$data["is_new"] = $is_new;
		$data["date_add"] = date("Y-m-d H:i:s");
		$this->DB->insert(MAILBOX_MESSAGES_TABLE, $data);
		$id = $this->DB->insert_id();

		$this->update_chat_statistic($id_chat);
		return $id;
	}

	/**
	 * Validate message data for saving to data source
	 * 
	 * @param array $data message data
	 * @return array
	 */
	public function validate_message($data){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data["message"])){
			$return["data"]["message"] = trim(strip_tags($data["message"]));
			if(empty($return["data"]["message"])){
				$return["errors"][] = l('error_empty_message', 'mailbox');
			}
			$this->CI->load->model('moderation/models/Moderation_badwords_model');
			$bw_count = $this->CI->Moderation_badwords_model->check_badwords($this->moderation_type, $return["data"]["message"]);
			if($bw_count){
				$return["errors"][] = l('error_badwords_message', 'mailbox');
			}
		}

		return $return;
	}

	/**
	 * Remove message object
	 * 
	 * @param integer $id message identifier
	 * @return void
	 */
	public function delete_message($id){
		$message_data = $this->get_message_by_id($id);
		$id_chat = $message_data["id_chat"];

		$this->DB->where('id', $id);
		$this->DB->delete(MAILBOX_MESSAGES_TABLE);

		$this->update_chat_statistic($id_chat);
	}

	/**
	 * Return formatted messages objects as array
	 * 
	 * @param array $params sql criteria of query to data source
	 * @param integer $offset offset row
	 * @param integer $limits items per page
	 * @return array
	 */
	public function get_messages($params=array(), $offset=null, $limits=null){
		$this->DB->select(implode(", ", $this->fields_messages))->from(MAILBOX_MESSAGES_TABLE);
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if($limits){
			$this->DB->limit($limits, intval($offset));
		}
		
		$this->DB->order_by("date_add DESC");
	
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			return $this->format_messages($data);
		}
		return array();
	}

	/**
	 * Return number of unread message object
	 * 
	 * @param integer $id_user user identifier
	 * @return integer
	 */
	public function get_new_messages_count($id_user){
		$this->DB->select('SUM(new) AS cnt')->from(MAILBOX_CHATS_TABLE)->where('id_com_user', $id_user)->where('new <>', 0);

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	/**
	 * Return number of messages objects
	 * 
	 * @param array $params sql criteria of query to data source
	 * @return integer
	 */
	public function get_messages_count($params = array()){
		$this->DB->select('COUNT(*) AS cnt')->from(MAILBOX_MESSAGES_TABLE);
		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	/**
	 * Set status of chat messages as read
	 * 
	 * @param integer $id_chat chat identifier
	 * @param array $ids messages identifiers
	 * @return void
	 */
	public function set_read_messages($id_chat, $ids){
		$data["is_new"] = 0;
		$this->DB->where_in('id', $ids);
		$this->DB->where('id_chat', $id_chat);
		$this->DB->update(MAILBOX_MESSAGES_TABLE, $data);
		$this->update_chat_statistic($id_chat);
	}
	
	/**
	 * Format messages objects
	 * 
	 * @param array $data messages data
	 * @return array
	 */
	public function format_messages($data){
		if(!$this->messages_format_settings['use_format'] || empty($data)) return $data;
		
		$chats_ids = array();
		
		foreach($data as $k=>$v){
			$data[$k]["message"] = nl2br($v["message"]);
			if(!in_array($m["id_chat"], $chats_ids)) $chats_ids[] = $v["id_chat"];
		}
		
		if($this->messages_format_settings['get_user'] && !empty($chats_ids)){
			$this->CI->load->model('Users_model');
			
			$params["where_in"]["id"] = $chats_ids;
			$temp = $this->get_chats_list($params);
			foreach($temp as $d){
				$chats[$d["id"]] = $d;
			}
			
			foreach($data as $k=>$v){
				if($v["message_type"] == 'inbox'){
					$data[$k]["user_data"] = isset($chats[$v["id_chat"]]["user_data"]) ? $chats[$v["id_chat"]]["user_data"] : $this->CI->Users_model->format_default_user(0);
				}else{
					$data[$k]["user_data"] = isset($chats[$v["id_chat"]]["owner_data"]) ? $chats[$v["id_chat"]]["owner_data"] : $this->CI->Users_model->format_default_user(0);
				}
			}
		}

		return $data;
	}

	////// seo
	
	/**
	 * Return settings for rewriting seo urls
	 * 
	 * @param string $method method name
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function get_seo_settings($method='', $lang_id=''){
		if(!empty($method)){
			return $this->_get_seo_settings($method, $lang_id);
		}else{
			$actions = array('index');
			$return = array();
			foreach($actions as $action){
				$return[$action] = $this->_get_seo_settings($action, $lang_id);
			}
			return $return;
		}
	}

	/**
	 * Return settings for rewriting seo urls (internal)
	 * 
	 * @param string $method method name
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function _get_seo_settings($method, $lang_id=''){
		if($method == "index"){
			return array(
				"templates" => array(),
				"url_vars" => array()
			);
		}
	}

	/**
	 * Return c variable value from url query to method
	 * 
	 * @param string $var_name_from variable name from url query
	 * @param string $var_name_to variable name to method
	 * @return mixed
	 */
	public function request_seo_rewrite($var_name_from, $var_name_to, $value){
		return $value;
	}

	/**
	 * Return data for generating xml site map
	 * 
	 * @return array
	 */
	public function get_sitemap_xml_urls(){
		return array();
	}

	/**
	 * Return data for generating site map
	 * 
	 * @return array
	 */
	public function get_sitemap_urls(){
		$this->CI->load->helper('seo');
		$auth = $this->CI->session->userdata("auth_type");

		$block[] = array(
			"name" => l('header_mailbox', 'mailbox'),
			"link" => rewrite_link('mailbox', 'index'),
			"clickable" => ($auth=="user")?true:false,
		);
		return $block;
	}
}

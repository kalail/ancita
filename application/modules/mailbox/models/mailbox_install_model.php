<?php

/**
 * Mailbox Install Model
 *
 * @package PG_RealEstate
 * @subpackage Mailbox
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Mailbox_install_model extends Model
{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	protected $CI;
	
	/**
	 * Menu configureation
	 * 
	 * @var array
	 */
	protected $menu = array(
		'private_account_menu' => array(
			'action' => 'none',
			'items' => array(
				'private_mailbox_item' => array('action' => 'create', 'link' => 'mailbox/index', 'status' => 1, 'sorter' => 6)
			)
		),
		'company_account_menu' => array(
			'action' => 'none',
			'items' => array(
				'company_mailbox_item' => array('action' => 'create', 'link' => 'mailbox/index', 'status' => 1, 'sorter' => 6)
			)
		),
		'agent_account_menu' => array(
			'action' => 'none',
			'items' => array(
				'agent_mailbox_item' => array('action' => 'create', 'link' => 'mailbox/index', 'status' => 1, 'sorter' => 6)
			)
		),
		"private_top_menu" => array(
			"action" => "none",
			"items" => array(
				"private-main-my-messages-item" => array("action" => "create", "link" => "mailbox", "status" => 1, "sorter" => 6),
			),
		),
		"company_top_menu" => array(
			"action" => "none",
			"name" => "User mode - Top menu for agencies",
			"items" => array(
				"company-main-my-messages-item" => array("action" => "create", "link" => "mailbox", "status" => 1, "sorter" => 6),
			),
		),
		
		"agent_top_menu" => array(
			"action" => "none",
			"items" => array(
				"agent-main-my-messages-item" => array("action" => "create", "link" => "mailbox", "status" => 1, "sorter" => 6),
			),
		),
	);

	/**
	 * Moderation configuration
	 * 
	 * @var array
	 */
	protected $moderation_types = array(
		array(
			"name" => "mailbox",
			"mtype" => "-1",
			"module" => "mailbox",
			"model" => "Mailbox_model",
			"check_badwords" => "1",
			"method_get_list" => "",
			"method_set_status" => "",
			"method_delete_object" => "",
			"allow_to_decline" => "0",
			"template_list_row" => "",
		),
 	);
	
	/**
	 * Constructor
	 *
	 * @return Mailbox_install
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}

	/**
	 * Install data of menu module
	 * 
	 * @return void
	 */
	public function install_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]['id'] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, 'create', $gid, 0, $this->menu[$gid]["items"]);
		}
	}

	/**
	 * Import languages of menu module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_menu_lang_update($langs_ids = null) {
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read('mailbox', 'menu', $langs_ids);

		if(!$langs_file) { log_message('info', 'Empty menu langs data'); return false; }

		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, 'update', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages of menu module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_menu_lang_export($langs_ids) {
		if(empty($langs_ids)) return false;
		$this->CI->load->helper('menu');

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, 'export', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array( "menu" => $return );
	}

	/**
	 * Uninstall data of menu module
	 * 
	 * @return void
	 */
	public function deinstall_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			if($menu_data['action'] == 'create'){
				linked_install_set_menu($gid, 'delete');
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]['items']);
			}
		}
	}

	/**
	 * Install data of moderation module
	 * 
	 * @return void
	 */
	public function install_moderation() {
		$this->CI->load->model('moderation/models/Moderation_type_model');
		foreach($this->moderation_types as $mtype) {
			$mtype['date_add'] = date("Y-m-d H:i:s");
			$this->CI->Moderation_type_model->save_type(null, $mtype);
		}
	}

	/**
	 * Import languages of moderation module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_moderation_lang_update($langs_ids = null) {
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$langs_file = $this->CI->Install_model->language_file_read('mailbox', 'moderation', $langs_ids);

		if(!$langs_file){log_message('info', 'Empty moderation langs data'); return false;}

		$this->CI->load->model('moderation/models/Moderation_type_model');
		$this->CI->Moderation_type_model->update_langs($this->moderation_types, $langs_file);
		
		return true;
	}

	/**
	 * Export languages of moderation module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_moderation_lang_export($langs_ids = null) {
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model('moderation/models/Moderation_type_model');
		return array('moderation' => $this->CI->Moderation_type_model->export_langs($this->moderation_types, $langs_ids));
	}

	/**
	 * Uninstall data of moderation module
	 * 
	 * @return void
	 */
	public function deinstall_moderation() {
		$this->CI->load->model('moderation/models/Moderation_type_model');
		foreach($this->moderation_types as $mtype) {
			$type = $this->CI->Moderation_type_model->get_type_by_name($mtype["name"]);
			$this->CI->Moderation_type_model->delete_type($type['id']);
		}
	}

	/**
	 * Install data of site map module
	 * 
	 * @return void
	 */
	public function install_site_map() {
		$this->CI->load->model('Site_map_model');
		$site_map_data = array(
			'module_gid' => 'mailbox',
			'model_name' => 'Mailbox_model',
			'get_urls_method' => 'get_sitemap_urls',
		);
		$this->CI->Site_map_model->set_sitemap_module('mailbox', $site_map_data);
	}
	
	/**
	 * Uninstall data of site map module
	 * 
	 * @return void
	 */
	public function deinstall_site_map() {
		$this->CI->load->model('Site_map_model');
		$this->CI->Site_map_model->delete_sitemap_module('mailbox');
	}

	/**
	 * Install module data
	 * 
	 * @return void
	 */
	public function _arbitrary_installing() {
		$seo_data = array(
			'module_gid' => 'mailbox',
			'model_name' => 'Mailbox_model',
			'get_settings_method' => 'get_seo_settings',
			'get_rewrite_vars_method' => 'request_seo_rewrite',
			'get_sitemap_urls_method' => 'get_sitemap_xml_urls',
		);
		$this->CI->pg_seo->set_seo_module('mailbox', $seo_data);
		
		// add seo link
		$this->CI->load->model('Seo_model');
		
		$xml_data = $this->CI->Seo_model->get_xml_route_file_content();
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '[text:mailbox]',
		);		
		$this->CI->pg_seo->set_settings('user', 'mailbox', 'index', $data);
		
		$xml_data['mailbox']['index'] = $this->CI->pg_seo->url_template_transform('mailbox', 'index', $data["url_template"], 'base', 'xml');
		
		$this->CI->Seo_model->set_xml_route_file_content($xml_data);
		$this->CI->Seo_model->rewrite_route_php_file();
	}
	
	/**
	 * Import module languages
	 * 
	 * @param array $langs_ids array languages identifiers
	 * @return void
	 */
	public function _arbitrary_lang_install($langs_ids=null){
		$langs_file = $this->CI->Install_model->language_file_read("mailbox", "arbitrary", $langs_ids);
		if(!$langs_file){log_message("info", "Empty mailbox arbitrary langs data"); return false;}
		
		$post_data = array(
			"title" => $langs_file["seo_tags_index_title"],
			"keyword" => $langs_file["seo_tags_index_keyword"],
			"description" => $langs_file["seo_tags_index_description"],
			"header" => $langs_file["seo_tags_index_header"],
			"og_title" => $langs_file["seo_tags_index_og_title"],
			"og_type" => $langs_file["seo_tags_index_og_type"],
			"og_description" => $langs_file["seo_tags_index_og_description"],
		);
		$this->CI->pg_seo->set_settings("user", "mailbox", "index", $post_data);
	}

	/**
	 * Export module languages
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function _arbitrary_lang_export($langs_ids=null){
		if(empty($langs_ids)) return false;

		//// arbitrary
		$settings = $this->CI->pg_seo->get_settings("user", "mailbox", "index", $langs_ids);
		$arbitrary_return["seo_tags_index_title"] = $settings["title"];
		$arbitrary_return["seo_tags_index_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_index_description"] = $settings["description"];
		$arbitrary_return["seo_tags_index_header"] = $settings["header"];
		$arbitrary_return["seo_tags_index_og_title"] = $settings["og_title"];
		$arbitrary_return["seo_tags_index_og_type"] = $settings["og_type"];
		$arbitrary_return["seo_tags_index_og_description"] = $settings["og_description"];

		return array("arbitrary" => $arbitrary_return);
	}

	/**
	 * Uninstall module data
	 * 
	 * @return void
	 */
	public function _arbitrary_deinstalling() {
		// remove seo link
		$this->CI->load->model('Seo_model');
		
		$xml_data = $this->CI->Seo_model->get_xml_route_file_content();
		
		$data = array(
			'noindex' => 0,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'mailbox', 'index', $data);
		
		$xml_data['mailbox']['index'] = $this->CI->pg_seo->url_template_transform('mailbox', 'index', $data["url_template"], 'base', 'xml');
		
		$this->CI->Seo_model->set_xml_route_file_content($xml_data);
		$this->CI->Seo_model->rewrite_route_php_file();
		
		$this->CI->pg_seo->delete_seo_module('mailbox');
	}
}

<?php

$install_lang["seo_tags_index_description"] = "My communication. View user messages. Write message to user.";
$install_lang["seo_tags_index_header"] = "Mis mensajes";
$install_lang["seo_tags_index_keyword"] = "read message, write messages, message to user, contact user";
$install_lang["seo_tags_index_og_description"] = "My communication. View messages from other users. Write message to user.";
$install_lang["seo_tags_index_og_title"] = "Mis mensajes";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Grupo Piloto : Contactos";


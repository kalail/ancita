<?php

$install_lang["seo_tags_index_description"] = "Мой почтовый ящик. Просмотр сообщений от пользоватей. Написать сообщение пользователю.";
$install_lang["seo_tags_index_header"] = "Мои сообщения";
$install_lang["seo_tags_index_keyword"] = "прочитать сообщение, написать сообщение, сообщение пользователю, связаться с пользователем";
$install_lang["seo_tags_index_og_description"] = "Мой почтовый ящик. Просмотр сообщений от пользоватей. Написать сообщение пользователю.";
$install_lang["seo_tags_index_og_title"] = "My messages";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Pilot Group : Контакты";


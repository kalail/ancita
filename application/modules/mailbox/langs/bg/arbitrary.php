<?php

$install_lang["seo_tags_index_description"] = "Моята поща. Преглед на съобщения от потребители. Писане на съобщение до потребители";
$install_lang["seo_tags_index_header"] = "Мои съобщения";
$install_lang["seo_tags_index_keyword"] = "четене на съобщение, писане на съобщение, съобщение за потребител, връзка с потребители";
$install_lang["seo_tags_index_og_description"] = "Моята поща. Преглед на съобщения от потребители. Писане на съобщение до потребителя.";
$install_lang["seo_tags_index_og_title"] = "Мои съобщения";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Мои съобщения";


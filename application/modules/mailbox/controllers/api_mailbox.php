<?php

/**
 * Mailbox api controller
 *
 * @package PG_RealEstate
 * @subpackage Mailbox
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Alexander Batukhtin <abatukhtin@pilotgroup.net>
 **/
class Api_Mailbox extends Controller {

	private $owner_id = 0;

	/**
	 * Constructor
	 */
	function __construct() {
		parent::Controller();
		$this->load->model('Mailbox_model');

		if ($this->session->userdata('auth_type') != 'user') {
			show_404();
		}
		$this->owner_id = $this->session->userdata('user_id');
		$this->load->model('Menu_model');
	}

	/**
	 * Chat
	 *
	 * @param int $id_from_user
	 */
	public function chat() {
		$id_from_user = intval($this->input->post('id_from_user'));
		if(!$id_from_user) {
			log_message('error', 'mailbox API: Empty user id');
			$this->set_api_content('errors', l('api_error_empty_user_id', 'mailbox'));
			return false;
		}
		if($this->pg_module->is_module_installed('users_services')) {
			$this->load->model('Users_services_model');
			$return = $this->Users_services_model->available_contact_action($this->owner_id, $id_from_user);
			if(!$return['available']){
				$this->set_api_content('errors', l('api_error_contact_unavailable', 'mailbox'));
				return false;
			}
		}

		$chat_data = $this->Mailbox_model->is_chat_exists($this->owner_id, $id_from_user);
		if (false === $chat_data) {
			$chat_id = $this->Mailbox_model->create_chat($this->owner_id, $id_from_user);
			$folder_id = $this->Mailbox_model->default_folder;
			$chat_data = $this->Mailbox_model->get_chat_by_id($chat_id);
		} else {
			$chat_id = $chat_data['id'];
			$folder_id = $chat_data['id_folder'];
		}

		$data = array(
			'folder_id' => $folder_id,
			'chat_id' => $chat_id,
			'chat' => $chat_data,
			'active_chat' => $chat_id,
			'active_folder' => $folder_id,
			'has_next' => false
		);
		$params = array();
		$params['where']['id_chat'] = $chat_id;
		$data['messages_count'] = $messages_count = $this->Mailbox_model->get_messages_count($params);
		if($messages_count){
			$messages = $this->Mailbox_model->get_messages($params, 0, $this->pg_module->get_module_config('mailbox', 'items_per_page'));
			$data['messages'] = $messages;
			if (!empty($messages)) {
				foreach ($messages as $m) {
					$ids[] = $m['id'];
				}
				$this->Mailbox_model->set_read_messages($chat_id, $ids);
				
				$data['date_format'] = $this->pg_date->get_format('date_time_literal', 'st');
			}
			if(count($messages) < $messages_count) {
				$data['has_next'] = true;
			}
		}
		$this->set_api_content('data', $data);
	}

	/**
	 * Returns chats list
	 *
	 * @param type $folder_id
	 * @param string $order_key Default: 'new'
	 * @param string $order_direction Default: 'ASC'
	 * @param int $page Default: 1
	 */
	public function chats(){
		$folder_id = intval($this->input->post('folder_id'));
		if(!$folder_id) {
			$folder_id = $this->Mailbox_model->default_folder;
		};
		$order_key = $this->input->post('order_key');
		if(!$order_key) {
			$order_key = 'new';
		};
		$order_direction = $this->input->post('order_direction');
		if(!$order_direction) {
			$order_direction = 'ASC';
		};
		$page = $this->input->post('page', true);
		if(!$page) {
			$page = 1;
		};
		$data = array(
			'order_key' => $order_key,
			'order_direction' => $order_direction,
			'page' => $page,
			'folder_id' => $folder_id,
			'folders' => $this->Mailbox_model->get_user_folders($this->owner_id),
			'chats' => $this->_chats_data($folder_id, $order_key, $order_direction, $page)
		);
		
		$date_formats = $this->pg_date->get_format('date_literal', 'st');
		
		$this->load->helper('date_format');
		
		foreach($data['chats'] as $i=>$chat){
			if(!isset($chat['last_message']) || !is_array($chat['last_message'])) continue;
			$data['chats'][$i]['last_message']['message'] = strip_tags($data['chats'][$i]['last_message']['message']);
			$data['chats'][$i]['last_message']['date_add_output'] = 
				tpl_date_format($chat['last_message']['date_add'], $date_formats);
		}
		
		$this->set_api_content('data', $data);
	}

	/**
	 * Delets chats
	 *
	 * @param array $chats_ids
	 */
	public function delete_chats() {
		$chats_ids = $this->input->post('chats_ids');
		if (empty($chats_ids)) {
			log_message('error', 'realestate API: Empty user id');
			$this->set_api_content('errors', l('error_select_chats', 'mailbox'));
			return false;
		}
		$chat_params = array();
		$chat_params['where_in']['id'] = $chats_ids;
		$chat_params['where']['id_com_user'] = $this->owner_id;
		$chat_list = $this->Mailbox_model->get_chats_list($chat_params);
		if (!empty($chat_list)) {
			foreach ($chat_list as $chat) {
				$this->Mailbox_model->delete_chat($chat['id']);
			}
		}
		$this->set_api_content('data', array('chats_ids' => $chats_ids));
		$this->set_api_content('messages', l('success_chats_delete', 'mailbox'));
	}

	/**
	 * Moves chats to another folder
	 *
	 * @param array $chats
	 * @param int $folder_id
	 */
	public function move_chats() {
		$folder_id = intval($this->input->post('folder_id'));
		if(!$folder_id) {
			$this->set_api_content('errors', l('api_error_empty_folder_id', 'mailbox'));
			return false;
		}
		$chats = $this->input->post('chats', 1);
		if (empty($chats)) {
			$this->set_api_content('errors', l('error_select_chats', 'mailbox'));
			return false;
		}

		$chat_params = array();
		$chat_params['where_in']['id'] = $chats;
		$chat_params['where']['id_com_user'] = $this->owner_id;
		$chat_list = $this->Mailbox_model->get_chats_list($chat_params);

		if (!empty($chat_list)) {
			foreach ($chat_list as $chat) {
				$chat_id[] = $chat['id'];
			}
			$this->Mailbox_model->move_chat($chat_id, $folder_id);
			$this->set_api_content('messages', l('success_chats_moved', 'mailbox'));
		} else {
			$this->set_api_content('messages', l('no_chats', 'mailbox'));
		}
		$this->set_api_content('data', array('folder_id' => $folder_id, 'chats' => $chats));
	}

	/**
	 * Folder
	 *
	 * @param int $folder_id
	 */
	public function folder() {
		$folder_id = intval($this->input->post('folder_id'));
		if(!$folder_id){
			$this->set_api_content('errors', l('api_error_empty_folder_id', 'mailbox'));
			return false;
		}
		$folder = $this->Mailbox_model->get_folder_by_id($folder_id);
		$this->set_api_content('data', array('folder' => $folder));
	}

	/**
	 * Folders list
	 *
	 */
	public function folders() {
		$this->set_api_content('data',
				array('folders' => $this->Mailbox_model->get_user_folders($this->owner_id)));
	}

	/**
	 * Saves folder
	 *
	 * @param int $folder_id
	 * @param string $folder_name
	 */
	public function save_folder() {
		$folder_id = intval($this->input->post('folder_id'));
		$id_user = $this->owner_id;
		$post_data['name'] = $this->input->post('folder_name', true);

		$validate_data = $this->Mailbox_model->validate_folder($folder_id, $id_user, $post_data);
		if (!empty($validate_data['errors'])) {
			$this->set_api_content('errors', array('folders' => $validate_data['errors']));
			return false;
		}
		$data = $validate_data['data'];
		$data['folder_type'] = 'user';
		$data['id_user'] = $id_user;
		$data['id'] = $this->Mailbox_model->save_folder($folder_id, $data);
		$this->set_api_content('data', array('folder' => $data));
		$this->set_api_content('messages', l('success_folder_save', 'mailbox'));
	}

	/**
	 * Deletes folders
	 *
	 * @param int|array $folder_id
	 */
	public function delete_folders() {
		$folder_id = array($this->input->post('folder_id', true));
		foreach($folder_id as $id) {
			$this->Mailbox_model->delete_folder($id);
		}
		$this->set_api_content('data', array('folder_id' => $folder_id));
		$this->set_api_content('messages', l('success_folder_delete', 'mailbox'));
	}

	/**
	 * Sends message
	 *
	 * @param int $chat_id
	 * @param string $message
	 * @return type
	 */
	public function send_message() {
		$chat_id = intval($this->input->post('chat_id', true));
		if (!$chat_id) {
			$this->set_api_content('errors', l('api_error_empty_chat_id', 'mailbox'));
			return false;
		}
		$post_data['message'] = $this->input->post('message', true);
		$validate_data = $this->Mailbox_model->validate_message($post_data);
		if (!empty($validate_data['errors'])) {
			$this->set_api_content('errors', array('folders' => $validate_data['errors']));
			return false;
		}
		$this->Mailbox_model->create_message($chat_id, $validate_data['data']['message'], 'outbox', 0);
		// get friend user chat and put message on it
		$chat = $this->Mailbox_model->get_chat_by_id($chat_id);
		$friend_chat = $this->Mailbox_model->is_chat_exists($chat['id_from_user'], $chat['id_com_user']);
		if ($friend_chat === false) {
			$id_friend_chat = $this->Mailbox_model->create_chat($chat['id_from_user'], $chat['id_com_user']);
		} else {
			$id_friend_chat = $friend_chat['id'];
		}
		$this->Mailbox_model->create_message($id_friend_chat, $validate_data['data']['message'], 'inbox', 1);
		$this->set_api_content('messages', l('success_message_save', 'mailbox'));
		$this->set_api_content('data', array('chat_id' => $chat_id, 'message' => $post_data['message']));
	}

	/**
	 * Returns chat messages
	 *
	 * @param int $chat_id
	 * @param bool $new
	 * @param int $offset
	 */
	public function get_messages(){

		$chat_id = intval($this->input->post('chat_id', true));
		if (!$chat_id) {
			$this->set_api_content('errors', l('api_error_empty_chat_id', 'mailbox'));
			return false;
		}
		$new = (bool)$this->input->post('new', true);
		$offset = (int)$this->input->post('offset', true);

		$chat = $this->Mailbox_model->get_chat_by_id($chat_id);
		if($this->pg_module->is_module_installed('users_services')) {
			$this->load->model('Users_services_model');
			$data = $this->Users_services_model->available_contact_action($chat['id_com_user'], $chat['id_from_user']);
			if(!$data['available']){
				$this->set_api_content('errors', l('api_error_contact_unavailable', 'mailbox'));
				return false;
			}
		}
		$params = array();
		$params['where']['id_chat'] = $chat_id;
		if($new){
			$params['where']['is_new'] = 1;
			$offset = $limit = 0;
		}else{
			$limit = $this->pg_module->get_module_config('mailbox', 'items_per_page');
		}
		
		$data['messages_count'] = $this->Mailbox_model->get_messages_count($params);
		
		$this->Mailbox_model->set_messages_format_settings('get_user', false);
		$data['messages'] = $this->Mailbox_model->get_messages($params, $offset, $limit);
		$this->Mailbox_model->set_messages_format_settings('get_user', true);
		
		$data['date_format'] = $this->pg_date->get_format('date_time_literal', 'st');
		$data['has_next'] = false;
		
		$this->load->helper('date_format');
		
		$ids = array();
		foreach($data['messages'] as $i=>$m){
			$m['message'] = strip_tags($m['message']);
			$m['date_add_output'] = tpl_date_format($m['date_add'], $data['date_format']);
			
			$data['messages'][$i] = $m;
			
			if(!$m['new']) continue;
			$ids[] = $m['id'];
		}
		if(!empty($ids)){
			$this->Mailbox_model->set_read_messages($chat_id, $ids);
		}
	
		if($data['messages_count'] > $offset + count($data['messages'])){
			$data['has_next'] = true;
		}
		$data['user_data'] = $chat['user_data'];
		$this->set_api_content('data', $data);
	}

	/**
	 * Deletes message
	 *
	 * @param int $message_id
	 */
	public function delete_message() {
		$message_id = intval($this->input->post('message_id', true));
		if (!$message_id) {
			$this->set_api_content('errors', l('api_error_empty_message_id', 'mailbox'));
			return false;
		}

		$message = $this->Mailbox_model->get_message_by_id($message_id);
		$chat = $this->Mailbox_model->get_chat_by_id($message['id_chat']);
		if ($chat['id_com_user'] != $this->owner_id) {
			$this->set_api_content('errors', l('error_not_permissions_delete_message', 'mailbox'));
			return false;
		}
		$this->Mailbox_model->delete_message($message_id);
		$this->set_api_content('messages', l('message_successfully_deleted', 'mailbox'));
		$this->set_api_content('data', array('message_id', $message_id));
	}

	private function _chats_data($folder_id = null, $order_key = 'new', $order_direction = 'ASC', $page = 1) {
		$folder = $this->Mailbox_model->get_folder_by_id($folder_id);
		$chat_params = array(
			'where' => array(
				'id_folder' => $folder_id,
				'id_com_user' => $this->owner_id
			)
		);
		$chats_count = $this->Mailbox_model->get_chats_count($chat_params);
		if (!$page) {
			$page = 1;
		}
		$items_on_page = $this->pg_module->get_module_config('mailbox', 'items_per_page');
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $chats_count, $items_on_page);
		
		$this->Mailbox_model->set_chats_format_settings('get_last_message', true);
		$chats = $this->Mailbox_model->get_chats_list($chat_params, $page, $items_on_page, array($order_key => $order_direction, 'id' => 'ASC'));
		$this->Mailbox_model->set_chats_format_settings('get_last_message', false);
		$chats['folder'] = $folder;
		$chats['chats_count'] = $chats_count;
		return $chats;
	}

}

<?php

$install_lang["admin_header_menu_add"] = "Añadir menú";
$install_lang["admin_header_menu_change"] = "menú Edición";
$install_lang["admin_header_menu_edit"] = "menú Edición";
$install_lang["admin_header_menu_item_add"] = "Añadir un nuevo tema";
$install_lang["admin_header_menu_item_change"] = "Cambie los datos del elemento";
$install_lang["admin_header_menu_item_edit"] = "Elemento de menú Editar";
$install_lang["admin_header_menu_items"] = "Menú : ";
$install_lang["admin_header_menu_list"] = "Menús del sitio";
$install_lang["error_empty_item_name"] = "Nombre del elemento de menú vacío";
$install_lang["error_menu_gid_invalid"] = "Palabra clave del menú incrrecta";
$install_lang["error_menu_item_menu_required"] = "Indefinido ID de menú";
$install_lang["error_menu_name_invalid"] = "Nombre del menú no es válido";
$install_lang["field_date_created"] = "Creado";
$install_lang["field_indicator"] = "Indicador";
$install_lang["field_menu_check_permissions"] = "Ocultar elementos que no están disponibles para un tipo específico de usuario";
$install_lang["field_menu_gid"] = "Palabra clave";
$install_lang["field_menu_item_gid"] = "Palabra clave (opcional)";
$install_lang["field_menu_item_link"] = "Enlace";
$install_lang["field_menu_item_value"] = "Nombre";
$install_lang["field_menu_name"] = "Nombre";
$install_lang["link_add_menu"] = "Añadir menú";
$install_lang["link_add_menu_item"] = "Añadir elemento de menú";
$install_lang["link_delete_menu"] = "Remover menú";
$install_lang["link_edit_menu"] = "Parámetros del menú Editar";
$install_lang["link_home"] = "Inicio";
$install_lang["link_items"] = "Editar elementos de menú";
$install_lang["link_save_sorter"] = "Guardar secuencia";
$install_lang["no_indicator"] = "Sin indicador";
$install_lang["note_delete_menu"] = "¿Está seguro que desea eliminar el menú?";
$install_lang["note_delete_menu_item"] = "¿Está seguro que desea eliminar el artículo?";
$install_lang["success_add_menu"] = "Menu agregado correctamente";
$install_lang["success_add_menu_item"] = "El artículo se ha añadido correctamente";
$install_lang["success_delete_menu"] = "Menú eliminado";
$install_lang["success_delete_menu_item"] = "Artículo eliminado";
$install_lang["success_update_menu"] = "Menú actualizado correctamente";
$install_lang["success_update_menu_item"] = "El artículo se ha actualizado correctamente";


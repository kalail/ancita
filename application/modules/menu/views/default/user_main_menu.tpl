<ul>
{foreach key=key item=item from=$menu}
<li {if $item.active}class="active"{/if}>
	<a href="{$item.link}">{$item.value}</a>
	{if $item.sub}
	<div class="sub_menu_block">
		<span>{$item.value}</span>
		<ul>
			{foreach key=key2 item=item2 from=$item.sub}
			<li><a href="{$item2.link}">{$item2.value} {if $item.indicator}<span class="num">{$item.indicator}</span>{/if}</a></li>
			{/foreach}
		</ul>
	</div>
	{/if}
</li>
{/foreach}
</ul>

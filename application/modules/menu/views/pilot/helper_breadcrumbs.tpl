{if $breadcrumbs}
<div class="breadcrumb">
<a href="{$site_url}{if !$is_guest}start/homepage{/if}">{l i='link_home' gid='menu'}</a>
{foreach item=item from=$breadcrumbs}
&nbsp;>&nbsp;{if $item.url}
{if $item.text eq 'Want to rent?' || $item.text eq 'Leie bolig?' || $item.text eq 'Hyra bostad?'}
<a href="{$item.url}" id="{$item.text}">{l i='agents_search_page_title' gid='users'}</a>
{else}
<a href="{$item.url}" id="{$item.text}">{$item.text}</a>
{/if}
{else}{$item.text}
{/if}
{/foreach}
</div>
{/if}

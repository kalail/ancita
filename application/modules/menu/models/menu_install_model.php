<?php

/**
 * Menu install model
 *
 * @package PG_RealEstate
 * @subpackage Menu
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Menu_install_model extends Model
{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	protected $CI;

	/**
	 * Menu configuration
	 * 
	 * @var array
	 */
	protected $menu = array(
		/// admin menu
		'admin_menu' => array(
			"action" => "none",
			"items" => array(
				'interface-items' => array(
					"action" => "none",
					"items" => array(
						'admin-menus-item' => array("action" => "create", 'link' => 'admin/menu', 'status' => 1, 'sorter' => 4),
					)
				)
			)
		)
	);

	/**
	 * Ausers configuration
	 * 
	 * @var array
	 */
	protected $ausers_methods = array(
		array( "module" => 'menu', 'method' => 'index', 'is_default' => 1),
	);
	/**
	 * Constructor
	 *
	 * @return Menu_install_model
	 */
	public function Menu_install_model(){
		parent::Model();
		$this->CI = & get_instance();
		$this->CI->load->model('Install_model');
	}

	/*
	 * Install data of menu module
	 *
	 * @return void
	 */
	public function install_menu() {
		$this->CI->load->model('Menu_model');
		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]['id'] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, 'create', $gid, 0, $this->menu[$gid]["items"]);
		}

	}

	/**
	 * Import languages of menu module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_menu_lang_update($langs_ids = null) {
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read('menu', 'menu', $langs_ids);

		if(!$langs_file) { log_message('info', 'Empty menu langs data'); return false; }

		$this->CI->load->model('Menu_model');
		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, 'update', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages of menu module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_menu_lang_export($langs_ids) {
		if(empty($langs_ids)) return false;
		$this->CI->load->model('Menu_model');
		$this->CI->load->helper('menu');

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, 'export', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array( "menu" => $return );
	}
	
	/*
	 * Install data of ausers module
	 *
	 * @return void
	 */
	public function install_ausers() {
		///// install ausers permissions
		$this->CI->load->model("Ausers_model");

		foreach($this->ausers_methods as $method){
			$this->CI->Ausers_model->save_method(null, $method);
		}
	}

	/**
	 * Import languages of ausers module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return void
	 */
	public function install_ausers_lang_update($langs_ids = null) {
		$langs_file = $this->CI->Install_model->language_file_read('menu', 'ausers', $langs_ids);

		///// install ausers permissions
		$this->CI->load->model("Ausers_model");
		$params['where']['module'] = 'menu';
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method['method']])){
				$this->CI->Ausers_model->save_method($method["id"], array(), $langs_file[$method['method']]);
			}
		}
	}

	/**
	 * Export languages of menu module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_ausers_lang_export($langs_ids) {
		$this->CI->load->model("Ausers_model");
		$params['where']['module'] = 'menu';
		$methods =  $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method['method']] = $method['langs'];
		}
		return array('ausers' => $return);
	}

	/**
	 * Uninstall data of ausers module
	 * 
	 * @return void
	 */
	public function deinstall_ausers() {
		///// delete moderation methods in ausers
		$this->CI->load->model("Ausers_model");
		$params['where']['module'] = 'menu';
		$this->CI->Ausers_model->delete_methods($params);
	}
	
	/**
	 * Install data of conjob module
	 * 
	 * @return void
	 */
	public function install_cronjob () {
		// Remove old indicators
		$this->CI->load->model('Cronjob_model');
		$cron_data = array(
			'name'		=> 'Remove old menu indicators',
			'module'	=> 'menu',
			'model'		=> 'Indicators_model',
			'method'	=> 'delete_old',
			'cron_tab'	=> '0 3 * * *',
			'status'	=> '1'
		);
		$this->CI->Cronjob_model->save_cron(null, $cron_data);
	}

	/**
	 * Uninstall data of conjob module
	 * 
	 * @return void
	 */
	public function deinstall_cronjob () {
		$this->CI->load->model('Cronjob_model');
		$cron_data = array();
		$cron_data['where']['module'] = 'menu';
		$this->CI->Cronjob_model->delete_cron_by_param($cron_data);
	}
	
	/**
	 * Install module data
	 * 
	 * @return void
	 */
	public function _arbitrary_installing() {
		$lang_dm_data = array(
			'module' => 'menu',
			'model' => 'Indicators_model',
			'method_add' => 'lang_dedicate_module_callback_add',
			'method_delete' => 'lang_dedicate_module_callback_delete'
		);
		$this->CI->pg_language->add_dedicate_modules_entry($lang_dm_data);
		$this->CI->load->model("menu/models/Indicators_model");
		foreach ($this->CI->pg_language->languages as $value){
			$this->CI->Indicators_model->lang_dedicate_module_callback_add($value['id']);
		}
		return;
	}

	/**
	 * Uninstall module data
	 * 
	 * @return void
	 */
	public function _arbitrary_deinstalling() {
		$lang_dm_data['where'] = array(
			'module' => 'indicators',
			'model' => 'Indicators_model',
		);
		$this->CI->pg_language->delete_dedicate_modules_entry($lang_dm_data);
	}
}

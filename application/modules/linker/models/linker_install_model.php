<?php

/**
 * Linker install model
 * 
 * @package PG_RealEstate
 * @subpackage Linker
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Linker_install_model extends Model
{
	protected $CI;

	/**
	 * Constructor
	 *
	 * @return Linker_install_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Install module data
	 * 
	 * @return void
	 */
	public function _arbitrary_installing(){
		
	}
	
	/**
	 * Uninstall module data
	 * 
	 * @return void
	 */
	public function _arbitrary_deinstalling(){
		
	}
}

<?php

$install_lang["admin_menu_settings_items_interface-items_social_network_1_menu_item"] = "Soziale Netzwerke";
$install_lang["admin_menu_settings_items_interface-items_social_network_1_menu_item_tooltip"] = "Einloggen, liken, teilen & Widgets kommentieren";
$install_lang["admin_menu_settings_items_system-items_social_network_2_menu_item"] = "Soziale Netzwerke";
$install_lang["admin_menu_settings_items_system-items_social_network_2_menu_item_tooltip"] = "Einloggen, liken, teilen & Widgets kommentieren";
$install_lang["admin_social_networking_menu_sn_pages_item"] = "Seiten";
$install_lang["admin_social_networking_menu_sn_pages_item_tooltip"] = "";
$install_lang["admin_social_networking_menu_sn_services_item"] = "Soziale Netzwerke";
$install_lang["admin_social_networking_menu_sn_services_item_tooltip"] = "";


<?php

$install_lang["seo_tags_title"] = "RealEstate Site Pro | Immobiliensuche, Angebote & Vermietungen";
$install_lang["seo_tags_keyword"] = "Immobilie";
$install_lang["seo_tags_description"] = "Suche nach Immobilien auf RealEstate Site Pro.";

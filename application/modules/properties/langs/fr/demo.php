<?php

$install_lang["property_types"]["header"] = "À vendre/acheter";
$install_lang["property_types"]["option"]["1"] = "Résidentiel";
$install_lang["property_types"]["option"]["2"] = "Commercial";
$install_lang["property_types"]["option"]["3"] = "Terres";

$install_lang["property_type_1"]["header"] = "Résidentiel";
$install_lang["property_type_1"]["option"]["1"] = "Apartement";
$install_lang["property_type_1"]["option"]["2"] = "Chambre";
$install_lang["property_type_1"]["option"]["3"] = "Duplex";
$install_lang["property_type_1"]["option"]["4"] = "Bungalow";
$install_lang["property_type_1"]["option"]["5"] = "Logement multi-famille";
$install_lang["property_type_1"]["option"]["6"] = "Condo";
$install_lang["property_type_1"]["option"]["7"] = "Maisonette";
$install_lang["property_type_1"]["option"]["8"] = "Maison mobile";
$install_lang["property_type_1"]["option"]["9"] = "Autre";

$install_lang["property_type_2"]["header"] = "Commercial";
$install_lang["property_type_2"]["option"]["1"] = "Café";
$install_lang["property_type_2"]["option"]["2"] = "Bureau";
$install_lang["property_type_2"]["option"]["3"] = "Magasin";
$install_lang["property_type_2"]["option"]["4"] = "Restaurant";
$install_lang["property_type_2"]["option"]["5"] = "Hotel";
$install_lang["property_type_2"]["option"]["6"] = "Lieu sportif";
$install_lang["property_type_2"]["option"]["7"] = "Entrepot";
$install_lang["property_type_2"]["option"]["8"] = "Manufacturé";
$install_lang["property_type_2"]["option"]["9"] = "Autre";

$install_lang["property_type_3"]["header"] = "Terres";
$install_lang["property_type_3"]["option"]["1"] = "Ferme";
$install_lang["property_type_3"]["option"]["2"] = "Prairie";
$install_lang["property_type_3"]["option"]["3"] = "Foret";
$install_lang["property_type_3"]["option"]["4"] = "Terre non-développée";
$install_lang["property_type_3"]["option"]["5"] = "Autre";

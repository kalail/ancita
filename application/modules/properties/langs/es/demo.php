<?php

$install_lang["property_types"]["header"] = "En venta/compra";
$install_lang["property_types"]["option"]["1"] = "Residencial";
$install_lang["property_types"]["option"]["2"] = "Comercial";
$install_lang["property_types"]["option"]["3"] = "Lotes/tierras";

$install_lang["property_type_1"]["header"] = "Residencial";
$install_lang["property_type_1"]["option"]["1"] = "Apartamento";
$install_lang["property_type_1"]["option"]["2"] = "Habitación";
$install_lang["property_type_1"]["option"]["3"] = "Dúplex";
$install_lang["property_type_1"]["option"]["4"] = "Hogar unfimiliar";
$install_lang["property_type_1"]["option"]["5"] = "Hogar multifamiliar";
$install_lang["property_type_1"]["option"]["6"] = "Condominio";
$install_lang["property_type_1"]["option"]["7"] = "Casa de pueblo";
$install_lang["property_type_1"]["option"]["8"] = "Casa rodante";
$install_lang["property_type_1"]["option"]["9"] = "Otro";

$install_lang["property_type_2"]["header"] = "Comercial";
$install_lang["property_type_2"]["option"]["1"] = "Café";
$install_lang["property_type_2"]["option"]["2"] = "Oficina";
$install_lang["property_type_2"]["option"]["3"] = "Tienda";
$install_lang["property_type_2"]["option"]["4"] = "Restaurante";
$install_lang["property_type_2"]["option"]["5"] = "Hotel";
$install_lang["property_type_2"]["option"]["6"] = "Instalaciones deportivas";
$install_lang["property_type_2"]["option"]["7"] = "Almacén";
$install_lang["property_type_2"]["option"]["8"] = "Fabricado";
$install_lang["property_type_2"]["option"]["9"] = "Otro";

$install_lang["property_type_3"]["header"] = "Lotes/tierras";
$install_lang["property_type_3"]["option"]["1"] = "Granja";
$install_lang["property_type_3"]["option"]["2"] = "Pasto";
$install_lang["property_type_3"]["option"]["3"] = "Bosque";
$install_lang["property_type_3"]["option"]["4"] = "Suelo no urbanizable";
$install_lang["property_type_3"]["option"]["5"] = "Otro";

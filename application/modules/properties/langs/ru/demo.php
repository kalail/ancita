<?php

$install_lang["property_types"]["header"] = "Продажа/Покупка";
$install_lang["property_types"]["option"]["1"] = "Жилая недвижимость";
$install_lang["property_types"]["option"]["2"] = "Коммерческая недвижимость";
$install_lang["property_types"]["option"]["3"] = "Земельные участки";

$install_lang["property_type_1"]["header"] = "Жилая недвижимость";
$install_lang["property_type_1"]["option"]["1"] = "Квартира";
$install_lang["property_type_1"]["option"]["2"] = "Комната";
$install_lang["property_type_1"]["option"]["3"] = "Дуплекс";
$install_lang["property_type_1"]["option"]["4"] = "Коттедж";
$install_lang["property_type_1"]["option"]["5"] = "Многосемейный дом";
$install_lang["property_type_1"]["option"]["6"] = "Квартирный дом";
$install_lang["property_type_1"]["option"]["7"] = "Таунхаус";
$install_lang["property_type_1"]["option"]["8"] = "Жилой прицеп";
$install_lang["property_type_1"]["option"]["9"] = "Прочее";

$install_lang["property_type_2"]["header"] = "Коммерческая недвижимость";
$install_lang["property_type_2"]["option"]["1"] = "Кафе";
$install_lang["property_type_2"]["option"]["2"] = "Офис";
$install_lang["property_type_2"]["option"]["3"] = "Магазин";
$install_lang["property_type_2"]["option"]["4"] = "Ресторан";
$install_lang["property_type_2"]["option"]["5"] = "Гостиница";
$install_lang["property_type_2"]["option"]["6"] = "Спорткомплекс";
$install_lang["property_type_2"]["option"]["7"] = "Склад";
$install_lang["property_type_2"]["option"]["8"] = "Производственное помещение";
$install_lang["property_type_2"]["option"]["9"] = "Прочее";

$install_lang["property_type_3"]["header"] = "Земельные участки";
$install_lang["property_type_3"]["option"]["1"] = "Ферма";
$install_lang["property_type_3"]["option"]["2"] = "Пастбище";
$install_lang["property_type_3"]["option"]["3"] = "Подлесная земля";
$install_lang["property_type_3"]["option"]["4"] = "Невозделанная земля";
$install_lang["property_type_3"]["option"]["5"] = "Прочее";

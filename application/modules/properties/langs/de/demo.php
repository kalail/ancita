<?php

$install_lang["property_types"]["header"] = "Zu Verkaufen/Kaufen";
$install_lang["property_types"]["option"]["1"] = "Wohnfläche";
$install_lang["property_types"]["option"]["2"] = "Kommerziell";
$install_lang["property_types"]["option"]["3"] = "Grundstücke/Länder";

$install_lang["property_type_1"]["header"] = "Wohnfläche";
$install_lang["property_type_1"]["option"]["1"] = "Appartement";
$install_lang["property_type_1"]["option"]["2"] = "Zimmer";
$install_lang["property_type_1"]["option"]["3"] = "Doppelhaus";
$install_lang["property_type_1"]["option"]["4"] = "Einfamilienhaus";
$install_lang["property_type_1"]["option"]["5"] = "Mehrfamilienhaus";
$install_lang["property_type_1"]["option"]["6"] = "Eigentumswohnung";
$install_lang["property_type_1"]["option"]["7"] = "Stadthaus";
$install_lang["property_type_1"]["option"]["8"] = "Wohnwagen";
$install_lang["property_type_1"]["option"]["9"] = "andere";

$install_lang["property_type_2"]["header"] = "Kommerziell";
$install_lang["property_type_2"]["option"]["1"] = "Café";
$install_lang["property_type_2"]["option"]["2"] = "Büro";
$install_lang["property_type_2"]["option"]["3"] = "Geschäft";
$install_lang["property_type_2"]["option"]["4"] = "Restaurant";
$install_lang["property_type_2"]["option"]["5"] = "Hotel";
$install_lang["property_type_2"]["option"]["6"] = "Sporteinrichtung";
$install_lang["property_type_2"]["option"]["7"] = "Lager";
$install_lang["property_type_2"]["option"]["8"] = "hergestellt";
$install_lang["property_type_2"]["option"]["9"] = "andere";

$install_lang["property_type_3"]["header"] = "Grundstücke/Länder";
$install_lang["property_type_3"]["option"]["1"] = "Bauernhof";
$install_lang["property_type_3"]["option"]["2"] = "Weide";
$install_lang["property_type_3"]["option"]["3"] = "Forst";
$install_lang["property_type_3"]["option"]["4"] = "unbebautes Grundstück";
$install_lang["property_type_3"]["option"]["5"] = "andere";

<?php

$install_lang["admin_header_ds_item_add"] = "Добавяне вариант";
$install_lang["admin_header_ds_item_change"] = "Редактиране вариант";
$install_lang["admin_header_properties"] = "Видове имоти";
$install_lang["admin_header_property_edit"] = "Редактировать категорию";
$install_lang["admin_header_property_item_edit"] = "Редактиране вариант";
$install_lang["api_error_category_not_found"] = "Категорията не е открита";
$install_lang["api_error_empty_category_id"] = "Липсва id на категорията";
$install_lang["error_category_deactivate"] = "Все категории не могут быть отключены";
$install_lang["error_empty_category_name"] = "Не указано название категории";
$install_lang["error_empty_property_item"] = "Посочете вариант";
$install_lang["field_gid"] = "Системно име";
$install_lang["field_name"] = "Название";
$install_lang["from"] = "от";
$install_lang["header_category_select"] = "Избор на категории";
$install_lang["header_job_categories"] = "Категории";
$install_lang["job_categories"] = "Категории";
$install_lang["link_add_ds_item"] = "Добавяне вариант";
$install_lang["link_category_activate"] = "Активировать категорию";
$install_lang["link_category_deactivate"] = "Деактивировать категорию";
$install_lang["link_category_edit"] = "Редактировать категорию";
$install_lang["link_properties_edit"] = "Варианты";
$install_lang["link_reset_all"] = "Изчисти всичко";
$install_lang["link_resort_items"] = "Промени подреждането";
$install_lang["link_select_another_category"] = "Обратно";
$install_lang["link_select_categories"] = "Избор на категории";
$install_lang["link_select_category"] = "Избор на категория";
$install_lang["no_categories"] = "Категории отсутсвуют";
$install_lang["note_delete_ds_item"] = "Сигурни ли сте , че искате да изтриете този вариант?";
$install_lang["properties"] = "Справки";
$install_lang["select_empty_option"] = "Всички категории";
$install_lang["selected"] = "избрано";
$install_lang["success_added_ds_item"] = "Вариантът е добавен";
$install_lang["success_added_property_item"] = "Вид имоти е добавен";
$install_lang["success_category_updated"] = "Категория успешно обновлена";
$install_lang["success_updated_ds_item"] = "Вариантът е обновен";
$install_lang["success_updated_property_item"] = "Вариантът е изменен";
$install_lang["text_availbale_select_categories"] = "Можете да изберете от следните категории:";


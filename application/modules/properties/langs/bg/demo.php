<?php

$install_lang["property_types"]["header"] = "Продава,Купува";
$install_lang["property_types"]["option"]["1"] = "Жилищни имоти";
$install_lang["property_types"]["option"]["2"] = "Бизнес имоти";
$install_lang["property_types"]["option"]["3"] = "Парцели, земи";

$install_lang["property_type_1"]["header"] = "Жилищни имоти";
$install_lang["property_type_1"]["option"]["1"] = "Апартамент";
$install_lang["property_type_1"]["option"]["2"] = "Стая";
$install_lang["property_type_1"]["option"]["3"] = "Мезонет";
$install_lang["property_type_1"]["option"]["4"] = "Вила";
$install_lang["property_type_1"]["option"]["5"] = "Многофамилна къща";
$install_lang["property_type_1"]["option"]["6"] = "Студио";
$install_lang["property_type_1"]["option"]["7"] = "Градска къща";
$install_lang["property_type_1"]["option"]["8"] = "Сглобяема къща";
$install_lang["property_type_1"]["option"]["9"] = "Други";

$install_lang["property_type_2"]["header"] = "Бизнес имоти";
$install_lang["property_type_2"]["option"]["1"] = "Кафе";
$install_lang["property_type_2"]["option"]["2"] = "Офис";
$install_lang["property_type_2"]["option"]["3"] = "Магазин";
$install_lang["property_type_2"]["option"]["4"] = "Ресторант";
$install_lang["property_type_2"]["option"]["5"] = "Хотел";
$install_lang["property_type_2"]["option"]["6"] = "Спортен комплекс";
$install_lang["property_type_2"]["option"]["7"] = "Склад";
$install_lang["property_type_2"]["option"]["8"] = "Фабрика";
$install_lang["property_type_2"]["option"]["9"] = "Друго";

$install_lang["property_type_3"]["header"] = "Парцели,земи";
$install_lang["property_type_3"]["option"]["1"] = "Ферма";
$install_lang["property_type_3"]["option"]["2"] = "Пасище";
$install_lang["property_type_3"]["option"]["3"] = "Горска територия";
$install_lang["property_type_3"]["option"]["4"] = "Нерегулирана земя";
$install_lang["property_type_3"]["option"]["5"] = "Друга";



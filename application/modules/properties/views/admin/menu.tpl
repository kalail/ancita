{include file="header.tpl"}
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first">{l i='field_name' gid='properties'}</th>
	<th class="w100">&nbsp;</th>	
</tr>
{foreach item=item key=gid from=$categories}
	{foreach item=item2 key=gid2 from=$properties}
		{if $gid2|replace:$property_prefix:''|array_key_exists:$item.option}
		{counter print=false assign=counter}
		<tr{if $counter is div by 2} class="zebra"{/if}>
			<td>{$item2.header}</td>
			<td class="icons">
				{if $item2.status}
				<a href="{$site_url}admin/properties/activate/{$gid2}/0"><img src="{$site_root}{$img_folder}icon-full.png" width="16" height="16" border="0" alt="{l i='link_category_deactivate' gid='properties' type='button'}" title="{l i='link_category_deactivate' gid='properties' type='button'}"></a>
				{else}
				<a href="{$site_url}admin/properties/activate/{$gid2}/1"><img src="{$site_root}{$img_folder}icon-empty.png" width="16" height="16" border="0" alt="{l i='link_category_activate' gid='properties' type='button'}" title="{l i='link_category_activate' gid='properties' type='button'}"></a>
				{/if}
				<a href="{$site_url}admin/properties/property/{$gid2}"><img src="{$site_root}{$img_folder}icon-settings.png" width="16" height="16" border="0" alt="{l i='link_properties_edit' gid='properties'}" title="{l i='link_properties_edit' gid='properties'}"></a>
				<a href="{$site_url}admin/properties/edit/{$gid2}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_category_edit' gid='properties' type='button'}" title="{l i='link_category_edit' gid='properties' type='button'}"></a>
			</td>
		</tr>
		{/if}
		{foreachelse}
		<tr><td colspan="2" class="center">{l i='no_categories' gid='properties'}</td></tr>
	{/foreach}
{/foreach}
</table>

{include file="footer.tpl"}

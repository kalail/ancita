<?php

/**
 * Properties admin side controller
 *
 * @package PG_RealEstate
 * @subpackage Properties
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Admin_properties extends Controller
{
	/**
	 * link to CodeIgniter object
	 * @var object
	 */
	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::Controller();
		$this->load->model('Menu_model');
		$this->load->model('Properties_model');
		$this->Menu_model->set_menu_active_item('admin_menu', 'content_items');
	}

	public function index(){
		$lang_id = $this->pg_language->current_lang_id;

		$this->pg_language->ds->return_module($this->Properties_model->module_gid, $lang_id);
		$ds = $this->pg_language->ds->lang[$lang_id][$this->Properties_model->module_gid];

		$category_gids = $this->Properties_model->categories;
		$property_gids = $this->Properties_model->properties;
		
		foreach($category_gids as $gid){
			$categories[$gid] = $ds[$gid];
		}
		
		$this->template_lite->assign('categories', $categories);

		foreach($property_gids as $gid){
			$properties[$gid] = $ds[$gid];
			$properties[$gid]['status'] = $this->pg_module->get_module_config('listings', $gid.'_enabled');
		}
		$this->template_lite->assign('properties', $properties);

		$this->template_lite->assign('property_prefix', $this->Properties_model->property_prefix);

		$this->system_messages->set_data('header', l('admin_header_properties', 'properties'));
		$this->template_lite->view('menu');
	}

	public function property($ds_gid, $lang_id = null){
		if (!$ds_gid) {
			redirect(site_url() . 'admin/properties/index');
		}elseif(!$lang_id || !array_key_exists($lang_id, $this->pg_language->languages)) {
			$lang_id = $this->pg_language->current_lang_id;
		}
		$reference = $this->pg_language->ds->get_reference($this->Properties_model->module_gid, $ds_gid, $lang_id);
		$header = l('admin_header_properties', 'properties') . ' : ' . $reference['header'];

		$this->system_messages->set_data('back_link', site_url() . 'admin/properties/index');
		$this->system_messages->set_data('header', $header);

		$this->template_lite->assign('current_gid', $ds_gid);
		$this->template_lite->assign('langs', $this->pg_language->languages);
		$this->template_lite->assign('current_lang_id', $lang_id);
		$this->template_lite->assign('reference', $reference);
		$this->template_lite->assign('module_gid', $this->Properties_model->module_gid);
		$this->template_lite->assign('ds_gid', $ds_gid);
		$this->template_lite->view('list');
	}

	public function property_items($lang_id, $ds_gid, $option_gid=null){

		if(!$lang_id || !array_key_exists($lang_id, $this->pg_language->languages)) {
			$lang_id = $this->pg_language->current_lang_id;
		}

		$reference = $this->pg_language->ds->get_reference($this->Properties_model->module_gid, $ds_gid, $lang_id);
		if($option_gid){
			$add_flag = false;
			foreach($this->pg_language->languages as $lid => $lang){
				$r = $this->pg_language->ds->get_reference($this->Properties_model->module_gid, $ds_gid, $lid);
				$lang_data[$lid] = $r["option"][$option_gid];
			}
		}else{
			$option_gid = "";
			$lang_data = array();
			$add_flag = true;
		}

		if($this->input->post('btn_save')){
			$lang_data = $this->input->post('lang_data', true);

			if(empty($option_gid)){
				if(!empty($reference["option"])){
					$array_keys = array_keys($reference["option"]);
				}else{
					$array_keys = array(0);
				}
				$option_gid = max($array_keys) + 1;
			}

			$default_lang_id = $lang_id ? $lang_id : $this->pg_language->current_lang_id;
			if(!isset($lang_data[$default_lang_id]) || empty($lang_data[$default_lang_id])){
				$this->system_messages->add_message('error', l('error_empty_property_item', 'properties'));
			}else{
				$reference = $this->pg_language->ds->get_reference($this->Properties_model->module_gid, $ds_gid, $default_lang_id);
				$reference["option"][$option_gid] = $lang_data[$default_lang_id];
				$this->pg_language->ds->set_module_reference($this->Properties_model->module_gid, $ds_gid, $reference, $default_lang_id);
				foreach($this->pg_language->languages as $lid => $ldata){
					if($lid == $default_lang_id) continue;
					$reference = $this->pg_language->ds->get_reference($this->Properties_model->module_gid, $ds_gid, $lid);
					if(!isset($lang_data[$lid]) || empty($lang_data[$lid])){
						$reference["option"][$option_gid] = $lang_data[$default_lang_id];
					}else{
						$reference["option"][$option_gid] = $lang_data[$lid];
					}
					$this->pg_language->ds->set_module_reference($this->Properties_model->module_gid, $ds_gid, $reference, $lid);
				}
				$this->system_messages->add_message('success', ($add_flag?l('success_added_property_item', 'properties'):l('success_updated_property_item', 'properties')));
				$url = site_url()."admin/properties/property/".$ds_gid."/".$lang_id;
				redirect($url);
			}
		}

		$this->template_lite->assign('lang_data', $lang_data);
		$this->template_lite->assign('langs', $this->pg_language->languages);
		$this->template_lite->assign('current_lang_id', $lang_id);
		$this->template_lite->assign('current_module_gid', $this->Properties_model->module_gid);
		$this->template_lite->assign('current_gid', $ds_gid);
		$this->template_lite->assign('option_gid', $option_gid);
		$this->template_lite->assign('add_flag', $add_flag);

		$this->system_messages->set_data('header', l('admin_header_property_item_edit', 'properties'));
		$this->template_lite->view('edit_ds_item');
	}

	public function ajax_ds_item_delete($gid, $option_gid){
		if($gid && $option_gid){
			foreach($this->pg_language->languages as $lid => $lang){
				$reference = $this->pg_language->ds->get_reference($this->Properties_model->module_gid, $gid, $lid);
				if(isset($reference["option"][$option_gid])){
					unset($reference["option"][$option_gid]);
					$this->pg_language->ds->set_module_reference($this->Properties_model->module_gid, $gid, $reference, $lid);
				}
			}
		}
		return;
	}

	public function ajax_ds_item_save_sorter($gid){
		$sorter = $this->input->post("sorter");
		foreach($sorter as $parent_str => $items_data){
			foreach($items_data as $item_str =>$sort_index){
				$option_gid = str_replace("item_", "", $item_str);
				$sorter_data[$sort_index] = $option_gid;
			}
		}

		if(empty($sorter_data)) return;
		ksort($sorter_data);
		$this->pg_language->ds->set_reference_sorter($this->Properties_model->module_gid, $gid, $sorter_data);
		return;
	}

	/**
	 * Activate property action
	 * 
	 * @param string $property_gid property guid
	 * @param integer $status property status
	 * @return void
	 */
	public function activate($property_gid, $status){
		if(!$status){
			$can_deactivate = false;
			$property_gids = $this->Properties_model->properties;
			foreach($property_gids as $gid){
				if($gid == $property_gid) continue;
				$property_enabled = $this->pg_module->get_module_config('listings', $gid.'_enabled');
				if($property_enabled){
					$can_deactivate = true;
					break;
				}
			}
			if(!$can_deactivate){
				$this->system_messages->add_message('error', l('error_category_deactivate', 'properties'));
				redirect(site_url().'admin/properties/index');
			}
		}
		$this->pg_module->set_module_config('listings', $property_gid.'_enabled', $status ? 1 : 0);
		redirect(site_url().'admin/properties/index');
	}
	
	/**
	 * Edit property action
	 * 
	 * @param string $property_gid property guid
	 * @return void
	 */
	public function edit($property_gid){
		if(!in_array($property_gid, $this->Properties_model->properties)){
			show_404();
		}
		
		$property_gid = str_replace('property_type_', '', $property_gid);
		foreach($this->pg_language->languages as $lid => $lang){
			$categories = $this->pg_language->ds->get_reference($this->Properties_model->module_gid, 'property_types', $lid);
			$lang_data[$lid] = $categories['option'][$property_gid];
		}
		
		if($this->input->post('btn_save')){
			$errors = array();
			
			$lang_data = $this->input->post('lang_data', true);
			
			foreach($lang_data as $lid => $string){
				$lang_data[$lid] = trim(strip_tags($string));
				if(empty($lang_data[$lid])){
					$errors[] = l('error_empty_category_name', 'properties');
					break;
				}
			}
			
			if(!empty($errors)){
				$this->system_messages->add_message('error', $errors);
			}else{
				foreach($lang_data as $lid => $string){
					$this->pg_language->pages->set_string($this->Properties_model->module_gid, 'property_types', $string, $lid);
				}
				$value1 = $lang_data[1];
				$value3 = $lang_data[3];
				$value4 = $lang_data[4];
				$value9 = $lang_data[9];
				$value10 = $lang_data[10];
				$option_gid = $property_gid;
				$gid= 'property_types';
				$this->Properties_model->set_string($gid,$option_gid,$value1,$value3,$value4,$value9,$value10);
				$gidn = 'property_type_'.$option_gid.'';
				$option_gidn = ' ';
				$this->Properties_model->chang_string($gidn,$option_gidn,$value1,$value3,$value4,$value9,$value10);
				$this->system_messages->add_message('success', l('success_category_updated', 'properties'));
				redirect(site_url().'admin/properties/index');
			}
		}
		
		$this->template_lite->assign('lang_data', $lang_data);
		$this->template_lite->assign('langs', $this->pg_language->languages);
		
		$this->template_lite->view('edit');
	}
}

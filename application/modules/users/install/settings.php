<?php

$install_settings["deactivated_send_mail"]  = "0";
$install_settings["deactivated_send_alert"] = "1";
$install_settings["deactivated_email"] = "";
$install_settings["private_login_enabled"] = "1";
$install_settings["private_register_enabled"] = "1";
$install_settings["private_register_mail_confirm"] = "1";
$install_settings["company_login_enabled"] = "1";
$install_settings["company_register_enabled"] = "1";
$install_settings["company_register_mail_confirm"] = "1";
$install_settings["agent_login_enabled"] = "1";
$install_settings["agent_register_enabled"] = "1";
$install_settings["agent_register_mail_confirm"] = "1";
$install_settings["use_email_confirmation"] = "1";
$install_settings["import_default_user"] = "0";
$install_settings["company_search_enabled"] = "1";
$install_settings["agent_search_enabled"] = "1";
$install_settings["private_search_enabled"] = "1";
$install_settings["use_map_in_search"] = "1";

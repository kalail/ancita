<?php

/**
 * Deactivated reason model
 * 
 * @package PG_RealEstate
 * @subpackage Users
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Users_deactivated_reason_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;

	/**
	 * Link to DataBase object
	 * 
	 * @var object
	 */
	private $DB;
	
	/**
	 * GUIDs of resaons
	 * 
	 * @var array
	 */
	public $content = array("deactivated_reason_object");
	
	/**
	 * Module GUID
	 * 
	 * @var string
	 */
	public $module_gid = "users";

	/**
	 * Constructor
	 * 
	 * @return Users_deactivated_reason_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}
	
	/**
	 * Return reason objects
	 *  
	 * @param integer $lang_id languages identifier
	 * @return array
	 */
	public function get_reason($lang_id = null){
		if(!$lang_id) $lang_id = $this->CI->session->userdata("lang_id");
		return $this->pg_language->ds->get_reference($this->module_gid, "user_object", $lang_id);
	}
	
	/**
	 * Validate reason object for saving to data source
	 * 
	 * @param string $option_gid option guid
	 * @param array $langs languages data
	 * @return array
	 */
	public function validate_reason($option_gid, $langs){
		$return = array("errors"=> array(), 'langs' => array());
		
		if(!empty($langs)){
			foreach($this->pg_language->languages as $lid => $lang_data){
				if(!isset($langs[$lid])){
					$return['errors'][] = l('error_empty_deactivated_reason_name', "users");
					break;
				}else{
					$return["langs"][$lid] = trim(strip_tags($langs[$lid]));
					if(empty($return["langs"][$lid])){
						$return['errors'][] = l('error_empty_deactivated_reason_name', "users");
						break;
					}
				}
			}
		}
		
		return $return;
	}
	
	/**
	 * Save reason objects to data source
	 * 
	 * @param string $option_gid option guid
	 * @param array $langs languages data
	 * @return array
	 */
	public function save_reason($option_gid, $langs_data){
		if(empty($option_gid)){
			if(!empty($reference["option"])){
				$array_keys = array_keys($reference["option"]);
			}else{
				$array_keys = array(0);
			}
			$option_gid = max($array_keys) + 1;
		}
			
		foreach($langs_data as $lid => $string){
			$reference = $this->pg_language->ds->get_reference($this->module_gid, $this->content[0], $lid);
			$reference["option"][$option_gid] = $string;
			$this->pg_language->ds->set_module_reference($this->module_gid, $this->content[0], $reference, $lid);
		}
	}
}

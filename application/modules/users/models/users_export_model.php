<?php

/**
 * Users Export Model
 * 
 * @package PG_RealEstate
 * @subpackage Users
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Users_export_model extends Model{	
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;

	/**
	 * Transfer configuration
	 */
	private $fields_for_transfer = array();
	
	/**
	 * Constructor
	 *
	 * return Users_export_model
	 */
	public function __construct(){
		
		parent::Model();
		
		$this->CI = & get_instance();
		$this->CI->load->model("Users_model");
		
		$this->fields_for_transfer = array(
			array("field_name"=>"id", "field_type"=>"int", "name"=>l("field_id", "users"), "required"=>false),
			array("field_name"=>"date_created", "field_type"=>"text", "name"=>l("field_date_created", "users"), "required"=>false),
			array("field_name"=>"date_modified", "field_type"=>"text", "name"=>l("field_date_modified", "users"), "required"=>false),
			array("field_name"=>"email", "field_type"=>"text", "name"=>l("field_email", "users"), "required"=>true),
			array("field_name"=>"unique_name", "field_type"=>"text", "name"=>l("field_unique_name", "users"), "required"=>false),
			array("fk"=>"user_type_str", "field_name"=>"user_type", "field_type"=>"select", "options"=>array("header"=>l("field_user_type", "users")), "required"=>true),
//			array("field_name"=>"password", "field_type"=>"text", "name"=>l("field_password", "users"), "required"=>true),
			array("fk"=>"status_output", "field_name"=>"status", "field_type"=>"select", "options"=>array("header"=>l("field_status", "users")), "required"=>false),
			array("field_name"=>"confirm", "field_type"=>"int", "name"=>l("field_confirm", "users"), "required"=>false),
			array("field_name"=>"confirm_code", "field_type"=>"text", "name"=>l("field_confirm_code", "users"), "required"=>false),
			array("field_name"=>"fname", "field_type"=>"text", "name"=>l("field_fname", "users"), "required"=>true),
			array("field_name"=>"sname", "field_type"=>"text", "name"=>l("field_sname", "users"), "required"=>true),
			array("field_name"=>"company_name", "field_type"=>"text", "name"=>l("field_name", "users"), "required"=>false),
			array("fk"=>"language_output", "field_name"=>"lang_id", "field_type"=>"select", "options"=>array("header"=>l("field_language", "users")), "required"=>false),
			array("fk"=>"currency_output", "field_name"=>"id_currency", "field_type"=>"select", "options"=>array("header"=>l("field_currency", "users")), "required"=>false),
//			array("field_name"=>"user_open_id", "field_type"=>"text", "name"=>l("field_open_id", "users"), "required"=>false),
			array("field_name"=>"user_logo", "field_type"=>"file", "name"=>l("field_user_logo", "users"), "required"=>false),
			array("fk"=>"group_output", "field_name"=>"group_id", "field_type"=>"select", "options"=>array("header"=>l("field_group", "users")), "required"=>false),
			array("field_name"=>"account", "field_type"=>"text", "name"=>l("field_account", "users"), "required"=>false),
			array("fk"=>"country", "field_name"=>"id_country", "field_type"=>"select", "options"=>array("header"=>l("field_country", "users")), "required"=>false),
			array("fk"=>"region", "field_name"=>"id_region", "field_type"=>"select", "options"=>array("header"=>l("field_state", "users")), "required"=>false),
			array("fk"=>"city", "field_name"=>"id_city", "field_type"=>"select", "options"=>array("header"=>l("field_city", "users")), "required"=>false),
			array("fk"=>"district", "field_name"=>"id_district", "field_type"=>"select", "options"=>array("header"=>l("field_district", "users")), "required"=>false),
			array("field_name"=>"phone", "field_type"=>"text", "name"=>l("field_phone", "users"), "required"=>false),
			array("field_name"=>"contact_email", "field_type"=>"text", "name"=>l("field_contact_email", "users"), "required"=>false),
			array("field_name"=>"contact_phone", "field_type"=>"text", "name"=>l("field_contact_phone", "users"), "required"=>false),
			array("field_name"=>"contact_info", "field_type"=>"text", "name"=>l("field_contact_info", "users"), "required"=>false),
			array("field_name"=>"address", "field_type"=>"text", "name"=>l("field_address", "users"), "required"=>false),
			array("field_name"=>"postal_code", "field_type"=>"text", "name"=>l("field_postal_code", "users"), "required"=>false),
			array("field_name"=>"web_url", "field_type"=>"text", "name"=>l("field_web_url", "users"), "required"=>false),
			array("field_name"=>"featured_end_date", "field_type"=>"text", "name"=>l("field_featured_end_date", "users"), "required"=>false),
			array("field_name"=>"show_logo_end_date", "field_type"=>"text", "name"=>l("field_show_logo_end_date", "users"), "required"=>false),
			array("fk"=>"working_days_str", "field_name"=>"working_days", "field_type"=>"select", "options"=>array("header"=>l("field_working_days", "users")), "required"=>false),
			array("fk"=>"working_hours_begin_text", "field_name"=>"working_hours_begin", "field_type"=>"select", "options"=>array("header"=>l("field_working_hours_begin", "users")), "required"=>false),
			array("fk"=>"working_hours_end_text", "field_name"=>"working_hours_end", "field_type"=>"select", "options"=>array("header"=>l("field_working_hours_end", "users")), "required"=>false),
			array("fk"=>"lunch_time_begin_text", "field_name"=>"lunch_time_begin", "field_type"=>"select", "options"=>array("header"=>l("field_lunch_time_begin", "users")), "required"=>false),
			array("fk"=>"lunch_time_end_text", "field_name"=>"lunch_time_end", "field_type"=>"select", "options"=>array("header"=>l("field_lunch_time_end", "users")), "required"=>false),
			array("field_name"=>"lat", "field_type"=>"text", "name"=>l("field_lat", "users"), "required"=>false),
			array("field_name"=>"lon", "field_type"=>"text", "name"=>l("field_lon", "users"), "required"=>false),
			array("fk"=>"company.output_name", "field_name"=>"agent_company", "field_type"=>"select", "options"=>array("header"=>l("field_agent_company", "users")), "required"=>false),
			array("field_name"=>"agent_status", "field_type"=>"int", "name"=>l("field_agent_status", "users"), "required"=>false),
//			array("field_name"=>"listings_for_sale_count", "field_type"=>"int", "name"=>l("field_listings_for_sale_count", "users"), "required"=>false),
//			array("field_name"=>"listings_for_rent_count", "field_type"=>"int", "name"=>l("field_listings_for_rent_count", "users"), "required"=>false),
			array("field_name"=>"twitter", "field_type"=>"text", "name"=>l("field_twitter", "users"), "required"=>false),
			array("field_name"=>"facebook", "field_type"=>"text", "name"=>l("field_facebook", "users"), "required"=>false),
			array("field_name"=>"vkontakte", "field_type"=>"text", "name"=>l("field_vkontakte", "users"), "required"=>false),
//			array("field_name"=>"views", "field_type"=>"int", "name"=>l("field_views", "users"), "required"=>false),
//			array("field_name"=>"banned", "field_type"=>"int", "name"=>l("field_banned", "users"), "required"=>false),
			array('field_name'=>'seolink', 'field_type'=>'text', 'name'=>l('field_seolink', 'users'), 'required'=>false),
		);
	}
	
	/**
	 * Return custom fields for module export
	 * 
	 * @return array
	 */
	public function callback_get_fields(){
		foreach($this->fields_for_transfer as $i=>$field){
			if(isset($property_types[$field["section_gid"]])){
				$section = $property_types[$field["section_gid"]]["name"]."-";
			}else{
				$section = "";
			}
			switch($field["field_type"]){
				case "select":
					switch($field['field_name']){
						case 'user_type':
							$custom_fields[] = array("name" => 'user_type_code', "type"=>"text", "label"=>$section.$field["options"]["header"]." (text, code)");
						break;
						case 'id_country':
							$custom_fields[] = array('name' => 'country_code', 'type'=>'text', 'label'=>$section.$field['options']['header'].' (text, code)');
						break;
						case 'id_region':
							$custom_fields[] = array('name' => 'region_code', 'type'=>'text', 'label'=>$section.$field['options']['header'].' (text, code)');
							$custom_fields[] = array('name' => 'id_region', 'type'=>'int', 'label'=>$section.$field['options']['header'].' (int, id)');
						break;
						case 'lang_id':
							$custom_fields[] = array('name' => 'language_gid', 'type'=>'text', 'label'=>$section.$field['options']['header'].' (text, GUID)');
						break;
						case 'id_currency':
							$custom_fields[] = array('name' => 'currency_gid', 'type'=>'text', 'label'=>$section.$field['options']['header'].' (text, GUID)');
						break;
						case 'working_days':
							$custom_fields[] = array('name' => 'working_days_code', 'type'=>'text', 'label'=>$section.$field['options']['header'].' (text, code)');
						break;
						default:
							$custom_fields[] = array("name" => $field["field_name"], "type"=>"int", "label"=>$section.$field["options"]["header"]." (int, id)");
						break;
					}
					if(isset($field['fk'])){
						$custom_fields[] = array("fk"=>$field["fk"], "name" => $field["field_name"], "type"=>"text", "label"=>$section.$field["options"]["header"]." (text)");
					}else{
						$custom_fields[] = array("fk"=>"field_editor.".$field["field_name"]."_output", "name" => $field["field_name"], "type"=>"text", "label"=>$section.$field["options"]["header"]." (text)");
					}
				break;
				case "textarea":
					$custom_fields[] = array("name" => $field["field_name"], "type"=>"text", "label"=>$section.$field["name"]." (text)");
				break;
				case "file":
					$custom_fields[] = array("name" => $field["field_name"], "type"=>"url", "label"=>$section.$field["name"]." (url)");
					$custom_fields[] = array("name" => $field["field_name"], "type"=>"file", "label"=>$section.$field["name"]." (file)");
				break;
				default:
					$custom_fields[] = array("name" => $field["field_name"], "type"=>$field["field_type"], "label"=>$section.$field["name"]." (".$field["field_type"].")");
				break;
			}
		}	
		return $custom_fields;		
	}

	/**
	 * Return admin form for module export
	 * 
	 * @param array $data selected values
	 * @return string
	 */
	public function callback_get_admin_form($data){
		$user_types = $this->CI->Users_model->get_user_types();
		$this->CI->template_lite->assign('user_types', $user_types);
		
		$this->CI->template_lite->assign('data', $data);
		return $this->CI->template_lite->fetch("export_form", "admin", "users");
	}
	
	/**
	 * Return user form for module export
	 * 
	 * @param array $data selected values
	 * @return string
	 */
	public function callback_get_user_form($data){
		$user_types = $this->CI->Users_model->get_user_types();
		$this->CI->template_lite->assign('user_types', $user_types);
		
		$this->CI->template_lite->assign('data', $data);
		return $this->CI->template_lite->fetch("export_form", "user", "users");
	}
	
	/**
	 * Process data from export form for module export
	 * 
	 * @param boolean $admin_mode admin mode
	 * @return string
	 */
	public function callback_process_form($admin_mode=true){
		$post_data = (array)$this->input->post('filters', true);
		
		$form = $this->input->post('form', true);
		
		foreach($post_data as $key => $value){
			if(empty($value)) unset($post_data[$key]);
		}
		
		return $post_data;
	}
	
	/**
	 * Return export data for module export
	 * 
	 * @param array $filter_data filters data
	 * @param array $relations relations data
	 * @return array
	 */
	public function callback_export_data($filter_data, $relations=array()){
		$this->CI->Users_model->set_format_settings(array('get_safe'=>false, 'get_language'=>true, 'get_currency'=>true, 'get_group'=>true, 'get_company'=>true));
		$data = $this->CI->Users_model->get_users_list_by_filters($filter_data, null, null, null, true, true);
		$this->CI->Users_model->set_format_settings(array('get_safe'=>true, 'get_language'=>false, 'get_currency'=>false, 'get_group'=>false, 'get_company'=>false));

		$return = array('data'=>$data);
		
		foreach($relations as $i=>$relation){
			if($relation['link'] == 'user_type_code'){
				foreach($data as $j=>$row){
					$return['fk'][$j]['user_type_code'] = $row['user_type'];
				}
			}elseif($relation['link'] == 'country_code'){
				foreach($data as $j=>$row){
					$return['fk'][$j]['country_code'] = $row['id_country'];
				}
			}elseif($relation['link'] == 'region_code'){
				foreach($data as $j=>$row){
					$return['fk'][$j]['region_code'] = $row['region_code'];
				}
			}elseif($relation['link'] == 'language_gid'){
				foreach($data as $j=>$row){
					$return['fk'][$j]['language_gid'] = isset($row['language']) ? $row['language']['gid'] : '';
				}
			}elseif($relation['link'] == 'currency_gid'){
				foreach($data as $j=>$row){
					$return['fk'][$j]['currency_gid'] = isset($row['currency']) ? $row['currency']['gid'] : '';
				}
			}elseif($relation['link'] == 'working_days_code'){
				foreach($data as $j=>$row){
					$return['fk'][$j]['working_days_code'] = $row['working_days'];
				}
			}elseif($relation['link'] == 'user_logo'){
				foreach($data as $j=>$row){
					if(!empty($row['user_logo_moderation'])){
						$return['data'][$j]['user_logo'] = $row['media']['user_logo_moderation']['file_url'];
					}elseif(!empty($row['user_logo'])){
						$return['data'][$j]['user_logo'] = $row['media']['user_logo']['file_url'];
					}else{
						$return['data'][$j]['user_logo'] = '';
					}
				}
			}elseif($relation['link'] == 'seolink'){
				$this->CI->load->helper('seo');
				
				$current_lang = $this->CI->pg_language->get_lang_by_id($this->CI->pg_language->current_lang_id);
				$current_lang_code = $current_lang['code'];

				foreach($data as $j=>$row){
					$return['fk'][$j]['seolink'] = rewrite_link('users', 'view', $row);
				}
			}elseif($relation["type"] == "text"){
				foreach($this->fields_for_transfer as $field){
					if($relation["link"] == $field["field_name"]){
						if(isset($field["fk"])){
							foreach($data as $j=>$row){
								if(strpos($field["fk"], ".")){
									$chunks = explode(".", $field["fk"]);
									$return['fk'][$j][$relation["link"]] = isset($data[$j][$chunks[0]][$chunks[1]]) ? $data[$j][$chunks[0]][$chunks[1]] : '';
								}else{
									$return['fk'][$j][$relation["link"]] = $data[$j][$field["fk"]];
								}
							}
						}
						break;
					}					
				}
			}
		}

		return $return;
	}
}

<?php

if (!defined("BASEPATH")) exit("No direct script access allowed");

define("USERS_VISITOR_TABLE", DB_PREFIX."user_profile_visitors");
define('LISTINGS_VISITOR_TABLE', DB_PREFIX.'listing_profile_visitors');
/**
 * Users visitor model
 *
 * @package PG_RealEstate
 * @subpackage Users
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Users_visitor_model extends Model{
	/**
	 * Lonk to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link Database object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Fields of visit in data source
	 * 
	 * @var array
	 */
	private $fields = array(
		"id",
		"id_user",
		"id_visitor",
		"last_visit_date",
		"visits_count",	
	);

	/**
	 * Constructor
	 *
	 * @return Users_visitor_model
	 */
	public function Users_visitor_model(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return visit data from data source by identifier
	 * 
	 * @param integer $id visit identifier
	 * @return array/false
	 */
	public function get_visitor_by_id($id){
		$result = $this->DB->select(implode(", ", $this->fields))
						   ->from(USERS_VISITOR_TABLE)
						   ->where('id', $id)
						   ->get()->result_array();
		if(empty($result)) return false;
		return $result[0];
	}
	
	/**
	 * Return visit data from data source by user and visitor identifier
	 * 
	 * @param integer $user_id user identfier
	 * @param integer $visitor_id visitor identifier
	 * @return array/false
	 */
	public function get_visitor_by_visitor($user_id, $visitor_id){
		$result = $this->DB->select(implode(", ", $this->fields))
						   ->from(USERS_VISITOR_TABLE)
						   ->where('id_user', $user_id)
						   ->where('id_visitor', $visitor_id)
						   ->get()->result_array();
		if(empty($result)) return false;
		return $result[0];
	}
	
	/**
	 * Save visit data to data source
	 * 
	 * @param integer $id visit identifier
	 * @param array $data visit data
	 * @return integer
	 */
	public function save_visitor($id, $data=array()){
		if(!isset($data['last_visit_date'])) $data["last_visit_date"] = date("Y-m-d H:i:s");
		if(!$id){
			if(!isset($data['visits_count'])) $data['visits_count'] = 1;
			$this->DB->insert(USERS_VISITOR_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			if(!isset($data['visits_count'])) $this->DB->set("visits_count", "visits_count + 1", FALSE);
			$this->DB->where("id", $id);
			$this->DB->update(USERS_VISITOR_TABLE, $data);
		}
		return $id;
	}
	
	/**
	 * Return criteria of searching users
	 * 
	 * @param array $filters filters data
	 * @return array
	 */
	private function _get_search_criteria($filters){
		$params = array();

		$fields = array_flip($this->fields);
		foreach($filters as $filter_name=>$filter_data){
			switch($filter_name){
				// By user
				case "user":
					if(!$filter_data) continue;
					$params = array_merge_recursive($params, array("where"=>array(USERS_VISITOR_TABLE.".id_user"=>$filter_data)));
				break;
				// By visitor
				case "visitor":
					if(!$filter_data) continue;
					$params = array_merge_recursive($params, array("where"=>array(USERS_VISITOR_TABLE.".id_visitor"=>$filter_data)));
				break;
				// By week
				case "week":
					if(!$filter_data) continue;
					$params = array_merge_recursive($params, array("where_sql"=>array('('.USERS_VISITOR_TABLE.".last_visit_date >= DATE_SUB(CURDATE(), INTERVAL 1 WEEK))")));
				break;
				// By month
				case "month":
					if(!$filter_data) continue;
					$params = array_merge_recursive($params, array("where_sql"=>array('('.USERS_VISITOR_TABLE.".last_visit_date >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH))")));
				break;
				default:
					if(isset($fields[$filter_name])){
						if(empty($filter_data)) break;
						$params = array_merge_recursive($params, array("where_in"=>array(USERS_VISITOR_TABLE.".".$filter_name=>$filter_data)));	
					}else{
						$params = array_merge_recursive($params, array("where"=>array(USERS_VISITOR_TABLE.".".$filter_name=>$filter_data)));	
					}
				break;
			}
		}
		
		return $params;
	}
	
	/**
	 * Return visitors objects from data source as array
	 * 
	 * @param integer $page page of results
	 * @param string $limits items per page
	 * @param array $order_by sorting data
	 * @param array $params sql criteria
	 * @return array
	 */
	private function _get_visitors_list($params){
		$query = "SELECT * FROM ".LISTINGS_VISITOR_TABLE." WHERE id_user='".$params["user"]."' GROUP BY id_visitor";
		$result = $this->DB->query($query)->result_array();
		return $result;
	}
	
	/**
	 * Return number of visitors objects in data source
	 * 
	 * @param array $params sql criteria
	 * @return integer
	 */
	private function _get_visitors_count($params=null){
		$query = "SELECT * FROM ".LISTINGS_VISITOR_TABLE." WHERE id_user='".$params["user"]."' GROUP BY id_visitor";
		$result = $this->DB->query($query)->result();
		return sizeof($result);
	}

	/**
	 * Return filtered visitors objects from data source as array
	 * 
	 * @param array $filters filters data
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param string $order_by sorting data
	 * @return array
	 */
	public function get_visitors_list($filters=array(), $page=null, $items_on_page=null, $order_by=null){
		$params = $this->_get_search_criteria($filters);	
		return $this->_get_visitors_list($filters);
	}
	
	/**
	 * Return number of filtered visitors objects in data source
	 * 
	 * @param array $filters filters data
	 * @return array
	 */
	public function get_visitors_count($filters=array()){
		$params = $this->_get_search_criteria($filters);
		return $this->_get_visitors_count($filters);
	}
	
	/**
	 * Validate visitor object for saving to data source
	 * 
	 * @param integer $id visitor identifier
	 * @param array $data visitor data
	 * @return array
	 */
	public function validate_visitor($id, $data){
		$return = array("errors"=>array(), "data"=>array());
		
		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['id_user'])){
			$return['data']['id_user'] = intval($data['id_user']);
		}
		
		if(isset($data['id_visitor'])){
			$return['data']['id_visitor'] = intval($data['id_visitor']);
		}
		
		if(isset($data['last_visit_date'])){
			$value = strtotime($data['last_visit_date']);
			if($value > 0) $return['data']['last_visit_date'] = date("Y-m-d", $value);
		}
		
		if(isset($data['visit_counts'])){
			$return['data']['visit_counts'] = intval($data['visit_counts']);
		}
		
		return $return;
	}
}

<?php

/**
 * Users install model
 *
 * @package PG_RealEstate
 * @subpackage Users
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Users_install_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	protected $CI;

	/**
	 * Menu configuration
	 * 
	 * @var array
	 */
	protected $menu = array(
		"guest_main_menu" => array(
			"action" => "none",
			"items" => array(
				"main-menu-pro-item" => array(
					"action" => "create", 
					"link" => "users/index", 
					"status" => 1, 
					"sorter" => 3,
					"items" => array(
						"main-menu-pro-agent-item" => array("action" => "create", "link" => "users/index/agent", "status" => 1, "sorter" => 1),	
						"main-menu-pro-company-item" => array("action" => "create", "link" => "users/index/company", "status" => 1, "sorter" => 2),
						"main-menu-pro-private-item" => array("action" => "create", "link" => "users/index/private", "status" => 1, "sorter" => 3),
					),
				),
			),
		),
		
		"private_main_menu" => array(
			"action" => "create",
			"name" => "User mode - Main menu for private persons",
			"items" => array(
				"private-main-menu-pro-item" => array(
					"action" => "create", 
					"link" => "users/index", 
					"status" => 1, 
					"sorter" => 3,
					"items" => array(
						"private-main-menu-pro-agent-item" => array("action" => "create", "link" => "users/index/agent", "status" => 1, "sorter" => 1),	
						"private-main-menu-pro-company-item" => array("action" => "create", "link" => "users/index/company", "status" => 1, "sorter" => 2),
						"private-main-menu-pro-private-item" => array("action" => "create", "link" => "users/index/private", "status" => 1, "sorter" => 3),
					),
				),
			),
		),
		
		"company_main_menu" => array(
			"action" => "create",
			"name" => "User mode - Main menu for agencies",
			"items" => array(
				"company-main-menu-pro-item" => array(
					"action" => "create", 
					"link" => "users/index", 
					"status" => 1, 
					"sorter" => 3,
					"items" => array(
						"company-main-menu-pro-agent-item" => array("action" => "create", "link" => "users/index/agent", "status" => 1, "sorter" => 1),	
						"company-main-menu-pro-company-item" => array("action" => "create", "link" => "users/index/company", "status" => 1, "sorter" => 2),
						"company-main-menu-pro-private-item" => array("action" => "create", "link" => "users/index/private", "status" => 1, "sorter" => 3),
					),
				),
			),
		),
		
		"agent_main_menu" => array(
			"action" => "create",
			"name" => "User mode - Main menu for agents",
			"items" => array(
				"agent-main-menu-pro-item" => array(
					"action" => "create", 
					"link" => "users/index", 
					"status" => 1, 
					"sorter" => 3,
					"items" => array(
					    "agent-main-menu-pro-agent-item" => array("action" => "create", "link" => "users/index/agent", "status" => 1, "sorter" => 1),	
					    "agent-main-menu-pro-company-item" => array("action" => "create", "link" => "users/index/company", "status" => 1, "sorter" => 2),
					    "agent-main-menu-pro-private-item" => array("action" => "create", "link" => "users/index/private", "status" => 1, "sorter" => 3),
					),
				),
			),
		),
		
		"private_account_menu" => array(
			"action" => "create",
			"name" => "User mode - Left menu for private persons",
			"items" => array(
				"private_overview_item" => array("action" => "create", "link" => "start/homepage", "status" => 1, "sorter" => 1),
				"private_my_profile_item" => array(
					"action" => "create", 
					"link" => "users/profile", 
					"status" => 1, 
					"sorter" => 2,
					"items" => array(
						"private_my_profile_edit_item" => array("action" => "create", "link" => "users/profile", "status" => 1, "sorter" => 1),
						"private_my_profile_change_regdata_item" => array("action" => "create", "link" => "users/change_reg_data", "status" => 1, "sorter" => 2),
//						"private_my_profile_services_item" => array("action" => "create", "link" => "users/account_services", "status" => 1, "sorter" => 3),
						"private_my_profile_deactivate_item" => array("action" => "create", "link" => "users/deactivate_profile", "status" => 1, "sorter" => 4),
					),
				),
				'private_payments_item' => array(
					'action' => 'create',
					'link' => 'payments/statistic',
					'status' => 1,
					'sorter' => 6,
					'items' => array(
						'private_my_account_balance_item' => array('action' => 'create', 'link' => 'users/account', 'status' => 1, 'sorter' => 2)
					)
				),
			),
		),
		
		"company_account_menu" => array(
			"action" => "create",
			"name" => "User mode - Left menu for agencies",
			"items" => array(
				"company_overview_item" => array("action" => "create", "link" => "start/homepage", "status" => 1, "sorter" => 1),
				"company_my_profile_item" => array(
					"action" => "create", 
					"link" => "users/profile", 
					"status" => 1, 
					"sorter" => 2,
					"items" => array(
						"company_my_profile_edit_item" => array("action" => "create", "link" => "users/profile", "status" => 1, "sorter" => 1),
						"company_my_profile_change_regdata_item" => array("action" => "create", "link" => "users/change_reg_data", "status" => 1, "sorter" => 2),
						"company_my_profile_services_item" => array("action" => "create", "link" => "users/account_services", "status" => 1, "sorter" => 3),
						"company_my_profile_deactivate_item" => array("action" => "create", "link" => "users/deactivate_profile", "status" => 1, "sorter" => 4),
					),
				),
				'company_payments_item' => array(
					'action' => 'create',
					'link' => 'payments/statistic',
					'status' => 1,
					'sorter' => 6,
					'items' => array(
						'company_my_account_balance_item' => array('action' => 'create', 'link' => 'users/account', 'status' => 1, 'sorter' => 2)
					)
				),
				"company_my_agents_item" => array(
					"action" => "create", 
					"link" => "users/my_agents", 
					"status" => 1, 
					"sorter" => 8,
					"items" => array(
						"company_my_agents_list_item" => array("action" => "create", "link" => "users/my_agents/list", "status" => 1, "sorter" => 1),
						"company_my_agents_requests_item" => array("action" => "create", "link" => "users/my_agents/requests", "status" => 1, "sorter" => 2),
					),
				),
			),
		),
		
		"agent_account_menu" => array(
			"action" => "create",
			"name" => "User mode - Left menu for agents",
			"items" => array(
				"agent_overview_item" => array("action" => "create", "link" => "start/homepage", "status" => 1, "sorter" => 1),
				"agent_my_profile_item" => array(
					"action" => "create", 
					"link" => "users/profile", 
					"status" => 1, 
					"sorter" => 2,
					"items" => array(
						"agent_my_profile_edit_item" => array("action" => "create", "link" => "users/profile", "status" => 1, "sorter" => 1),
						"agent_my_profile_change_regdata_item" => array("action" => "create", "link" => "users/change_reg_data", "status" => 1, "sorter" => 2),
						"agent_my_profile_services_item" => array("action" => "create", "link" => "users/account_services", "status" => 1, "sorter" => 3),
						"agent_my_profile_deactivate_item" => array("action" => "create", "link" => "users/deactivate_profile", "status" => 1, "sorter" => 4),
					),
				),
				'agent_payments_item' => array(
					'action' => 'create',
					'link' => 'payments/statistic',
					'status' => 1,
					'sorter' => 6,
					'items' => array(
						'agent_my_account_balance_item' => array('action' => 'create', 'link' => 'users/account', 'status' => 1, 'sorter' => 2)
					)
				),
			),
		),
		
		"private_top_menu" => array(
			"action" => "none",
			"items" => array(
				"private-main-my-profile-item" => array("action" => "create", "link" => "users/profile", "status" => 1, "sorter" => 2),
				"private-main-my-account-item" => array("action" => "create", "link" => "users/account", "status" => 1, "sorter" => 5),
				"private-main-logout-item" => array("action" => "create", "link" => "users/logout", "status" => 1, "sorter" => 12),
			),
		),
		
		"company_top_menu" => array(
			"action" => "none",
			"name" => "User mode - Top menu for agencies",
			"items" => array(
				"company-main-my-profile-item" => array("action" => "create", "link" => "users/profile", "status" => 1, "sorter" => 2),
				"company-main-my-account-item" => array("action" => "create", "link" => "users/account", "status" => 1, "sorter" => 5),
				"company-main-my-agents-item" => array("action" => "create", "link" => "users/my_agents", "status" => 1, "sorter" => 8),
				"company-main-logout-item" => array("action" => "create", "link" => "users/logout", "status" => 1, "sorter" => 12),
			),
		),
		
		"agent_top_menu" => array(
			"action" => "none",
			"items" => array(
				"agent-main-my-profile-item" => array("action" => "create", "link" => "users/profile", "status" => 1, "sorter" => 2),
				"agent-main-my-account-item" => array("action" => "create", "link" => "users/account", "status" => 1, "sorter" => 5),
				"agent-main-logout-item" => array("action" => "create", "link" => "users/logout", "status" => 1, "sorter" => 12),
			),
		),
		
		"admin_menu" => array(
			"action" => "none",
			"items" => array(
				"main_items" => array(
					"action" => "none",
					"items" => array(
						"users_menu_item" => array("action" => "create", "link" => "admin/users", "status" => 1, "sorter" => 3),
					),
				),
				"settings_items" => array(
					"action" => "none",
					"items" => array(
						"feedbacks-items" => array(
							"action" => "none",
							"items" => array(
								"users_deact_alers_menu_item" => array("action" => "create", "link" => "admin/users/deactivated_alerts", "status" => 1, "sorter" => 4),
							),
						),
						"system-items" => array(
							"action" => "none",
							"items" => array(
								"users_deact_sett_menu_item" => array("action" => "create", "link" => "admin/users/settings", "status" => 1, "sorter" => 8),
							),
						),
					),
				),
			),
		),
		
		"admin_users_menu" => array(
			"action" => "create",
			"name" => "Admin mode - Users",
			"items" => array(
				"users_list_item" => array("action" => "create", "link" => "admin/users", "status" => 1, "sorter" => 1),
				"groups_list_item" => array("action" => "create", "link" => "admin/users/groups", "status" => 1, "sorter" => 2),
			),
		),
		
		"admin_users_types_menu" => array(
			"action" => "create",
			"name" => "Admin mode - Users - tabs",
			"items" => array(
				"users_types_private_item" => array("action" => "create", "link" => "admin/users/index/private", "status" => 1, "sorter" => 1),
				"users_types_company_item" => array("action" => "create", "link" => "admin/users/index/company", "status" => 1, "sorter" => 2),
				"users_types_agent_item" => array("action" => "create", "link" => "admin/users/index/agent", "status" => 1, "sorter" => 3),
				"users_types_settings_item" => array("action" => "create", "link" => "admin/users/types_settings", "status" => 1, "sorter" => 4),
			),
		),
		
		"admin_users_settings_menu" => array(
			"action" => "create",
			"name" => "Admin mode - Content - Profile de-activation",
			"items" => array(
				"users_deactivated_alerts_item" => array("action" => "create", "link" => "admin/users/deactivated_alerts", "status" => 1, "sorter" => 1),
				"users_deactivated_settings_item" => array("action" => "create", "link" => "admin/users/deactivated_settings", "status" => 1, "sorter" => 2),
				"users_deactivated_reasons_item" => array("action" => "create", "link" => "admin/users/deactivated_reasons", "status" => 1, "sorter" => 3),
			),
		),
	);
	
	/**
	 * Uploads configuration
	 * 
	 * @var array
	 */
	protected $uploads = array(
		array(
			"gid" => "user-logo", 
			"name" => "User icon", 
			"max_height" => 1000, 
			"max_width" => 1000, 
			"max_size" => 100000, 
			"name_format" => "generate", 
			"file_formats" => array("jpg", "gif", "png"), 
			"default_img" => "default_user-logo.png",
			"thumbs" => array(
				"big" => array("width"=>200, "height"=>200, "effect"=>"none", "crop_param"=>"crop", "crop_color"=>"ffffff"),
				"middle" => array("width"=>100, "height"=>100, "effect"=>"none", "crop_param"=>"crop", "crop_color"=>"ffffff"),
				"small" => array("width"=>60, "height"=>60, "effect"=>"none", "crop_param"=>"crop", "crop_color"=>"ffffff"),
			),
		),
	);

	/**
	 * Ausers configuration
	 * 
	 * @var array
	 */
	protected $ausers = array(
		array("module"=>"users", "method"=>"index", "is_default"=>1),
	);
	
	/**
	 * Groups configuration
	 * 
	 * @var array
	 */
	protected $groups = array(
		array("gid"=>"default", "is_default"=>"1"),
		array("gid"=>"simple-users", "is_default"=>"0"),
		array("gid"=>"vip-users", "is_default"=>"0"),
	);
	
	/**
	 * Seo configuration
	 * 
	 * @var array
	 */
	protected $seo = array(
		"module_gid" => "users",
		"model_name" => "Users_model",
		"get_settings_method" => "get_seo_settings",
		"get_rewrite_vars_method" => "request_seo_rewrite",
		"get_sitemap_urls_method" => "get_sitemap_xml_urls",
	);
	
	/**
	 * Dynamic block configuration
	 * 
	 * @var array
	 */
	protected $dynamic_blocks = array(
		/*array(
			"gid" => "featured_users_block", 
			"module" => "users",
			"model"	=> "Users_model",
			"method" => "_dynamic_block_get_featured_users",
			"params" => array("count"=>array("gid"=>"count", "type"=>"int", "default" => 8)),
			"views" => array(
				array("gid"=>"gallery_big"),
				array("gid"=>"gallery_middle"),
			),
		),*/
		array(
			"gid" => "featured_companies_block", 
			"module" => "users",
			"model"	=> "Users_model",
			"method" => "_dynamic_block_get_featured_companies",
			"params" => array("count"=>array("gid"=>"count", "type"=>"int", "default" => 8)),
			"views" => array(
				array("gid"=>"gallery_big"),
				array("gid"=>"gallery_middle"),
			),
		),
		array(
			"gid" => "featured_agents_block", 
			"module" => "users",
			"model"	=> "Users_model",
			"method" => "_dynamic_block_get_featured_agents",
			"params" => array("count"=>array("gid"=>"count", "type"=>"int", "default" => 8)),
			"views" => array(
				array("gid"=>"gallery_big"),
				array("gid"=>"gallery_middle"),
			),
		),
	);

	/**
	 * Moderation configuration
	 * 
	 * @var array
	 */
	protected $moderation_types = array(
		array(
			"name" => "users",
			"mtype" => "-1",
			"module" => "users",
			"model" => "Users_model",
			"check_badwords" => "1",
			"method_get_list" => "",
			"method_set_status" => "",
			"method_delete_object" => "",
			"allow_to_decline" => "0",
			"template_list_row" => "",
		),
		array(
			"name" => "user_logo",
			"mtype" => "1", 
			"module" => "users", 
			"model" => "Users_model", 
			"check_badwords" => "2",
			"method_get_list" => "_moder_get_list",
			"method_set_status" => "_moder_set_status",
			"method_delete_object" => "",
			"allow_to_decline" => "1",
			"template_list_row" => "moder_block",
		),
	);

	/**
	 * Notifications configuration
	 * 
	 * @var array
	 */
	protected $notifications = array(
		"templates" => array(
			array("gid"=>"users_fogot_password", "name"=>"Forgot password mail", "vars"=>array("password", "email", "fname", "sname"), "content_type"=>"text"),
			array("gid"=>"users_account_create", "name"=>"Account create letter", "vars"=>array("password", "email", "fname", "sname", "nickname"), "content_type"=>"text"),
			array("gid"=>"users_change_password", "name"=>"Change password notification", "vars"=>array("password", "email", "fname", "sname", "nickname"), "content_type"=>"text"),
			array("gid"=>"users_change_email", "name"=>"Email changed", "vars"=>array("password", "email", "fname", "sname", "nickname"), "content_type"=>"text"),
			array("gid"=>"users_email_restore", "name"=>"Email changed (notification to old email)", "vars"=>array("password", "email", 'email_restore', "fname", "sname", "nickname", 'restore_link'), "content_type"=>"text"),
			array("gid"=>"users_registration", "name"=>"User registration letter", "vars"=>array("password", "email", "fname", "sname", "nickname", "confirm_block"), "content_type"=>"text"),
			array("gid"=>"users_deactivated", "name"=>"Deactivate account", "vars"=>array("email", "fname", "sname"), "content_type"=>"text"),
			array("gid"=>"ausers_deactivated", "name"=>"Deactivate account (for admin)", "vars"=>array("email", "fname", "sname"), "content_type"=>"text"),
			array("gid"=>"agent_send_request", "name"=>"Company notification informing that a member wants to become their agent", "vars"=>array("fname", "sname", "user_name", "company_name"), "content_type"=>"text"),
			array("gid"=>"agent_cancel_request", "name"=>"Company notification informing that a member canceled his request to become an agent of the company", "vars"=>array("fname", "sname", "user_name", "company_name"), "content_type"=>"text"),
			array("gid"=>"agent_unjoin", "name"=>"Company notification informing that a member removed himself from the list of their agents", "vars"=>array("fname", "sname", "user_name", "company_name"), "content_type"=>"text"),
			array("gid"=>"agent_deleted", "name"=>"Member notification informing that a company removed him from the list of their agents", "vars"=>array("fname", "sname", "user_name", "company_name"), "content_type"=>"text"),
			array("gid"=>"agent_request_accepted", "name"=>"Member notification informing that the company accepted his request to become an agent", "vars"=>array("fname", "sname", "user_name", "company_name"), "content_type"=>"text"),
			array("gid"=>"agent_request_declined", "name"=>"Member notification informing that the company declined his request to become their agent", "vars"=>array("fname", "sname", "user_name", "company_name"), "content_type"=>"text"),
			array("gid"=>"user_account_create_by_admin", "name"=>"User created by admin mail", "vars"=>array("fname", "sname", "email", "password"), "content_type"=>"text"),
		),
		"notifications" => array(
			array("gid"=>"users_fogot_password", "template"=>"users_fogot_password", "send_type"=>"simple"),
			array("gid"=>"users_account_create", "template"=>"users_account_create", "send_type"=>"simple"),
			array("gid"=>"users_change_password", "template"=>"users_change_password", "send_type"=>"simple"),
			array("gid"=>"users_change_email", "template"=>"users_change_email", "send_type"=>"simple"),
			array("gid"=>"users_email_restore", "template"=>"users_email_restore", "send_type"=>"simple"),
			array("gid"=>"users_registration", "template"=>"users_registration", "send_type"=>"simple"),
			array("gid"=>"users_deactivated", "template"=>"users_deactivated", "send_type"=>"simple"),
			array("gid"=>"ausers_deactivated", "template"=>"ausers_deactivated", "send_type"=>"simple"),
			array("gid"=>"agent_send_request", "template"=>"agent_send_request", "send_type"=>"simple"),
			array("gid"=>"agent_cancel_request", "template"=>"agent_cancel_request", "send_type"=>"simple"),
			array("gid"=>"agent_unjoin", "template"=>"agent_unjoin", "send_type"=>"simple"),
			array("gid"=>"agent_deleted", "template"=>"agent_deleted", "send_type"=>"simple"),
			array("gid"=>"agent_request_accepted", "template"=>"agent_request_accepted", "send_type"=>"simple"),
			array("gid"=>"agent_request_declined", "template"=>"agent_request_declined", "send_type"=>"simple"),
			array("gid"=>"user_account_create_by_admin", "template"=>"user_account_create_by_admin", "send_type"=>"simple"),
		),
	);
	
	/**
	 * Reviews configuration
	 * 
	 * @var array
	 */
	protected $reviews = array(
		"reviews_fields" => array(
			"review_data" => array("type" => "TEXT", "null" => TRUE),
			"review_count" => array("type" => "smallint(5)", "null" => FALSE),
			"review_sorter"	=> array("type" => "decimal(5,3)", "null" => FALSE),
			"review_value"	=> array("type" => "decimal(5,3)", "null" => FALSE),
			"review_type" => array("type" => "varchar(20)", "null" => FALSE),
		),

		"reviews" => array(
			array("gid"=>"users_object", "name"=>"Reviews in user profiles", "rate_type"=>"stars", "module"=>"users", "model"=>"users", "callback"=>"callback_reviews"),
		),

		"rate_types" => array(
			"stars" => array(
				"main" => array(1, 2, 3, 4, 5),
				"dop1" => array(1, 2, 3, 4, 5),
				"dop2" => array(1, 2, 3, 4, 5),
			),
			"hands" => array(
				"main" => array(1, 5),
				"dop1" => array(1, 5),
				"dop2" => array(1, 5),
			),
		),
	);
	
	/**
	 * Spam configuration
	 * 
	 * @var array
	 */
	protected $spam = array(
		array("gid"=>"users_object", "form_type"=>"select_text", "send_mail"=>true, "status"=>true, "module"=>"users", "model"=>"Users_model", "callback"=>"spam_callback"),
	);
	
	/**
	 * Geomap configuration
	 * 
	 * @var array
	 */
	protected $geomap = array(
		array(
			"map_gid" => "googlemapsv3",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "user_profile",
			"use_type_selector" => 1,
			"use_searchbox" => 1,
			"use_search_radius" => 1,
			"use_search_auto" => 1,
			"use_show_details" => 1,
			"use_amenities" => 1,
			"amenities" => array("bank"),
		),
		array(
			"map_gid" => "yandexmapsv2",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "user_profile",
			"use_type_selector" => 1,
			"use_searchbox" => 1,
			"use_tools" => 1,
			"use_clusterer" => 1,
			"use_click_zoom" => 1,
		),
		array(
			"map_gid" => "bingmapsv7",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "user_profile",
			"use_type_selector" => 1,
			"use_searchbox" => 1,
		),
		array(
			"map_gid" => "googlemapsv3",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "user_info",
			"zoom" => 10,
			"use_show_details" => 1,
		),
		array(
			"map_gid" => "yandexmapsv2",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "user_info",
			"zoom" => 10,
		),
		array(
			"map_gid" => "bingmapsv7",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "user_info",
			"zoom" => 10,
		),
		array(
			"map_gid" => "googlemapsv3",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "user_search",
			"zoom" => 5,
			"use_smart_zoom" => 1,
			"use_panorama" => 0,
			"use_router" => 0,
			"use_searchbox" => 0,
			"use_search_radius" => 0,
			"use_search_auto" => 0,
			"use_show_details" => 0,
			"use_amenities" => 0,
			"amenities" => array("bank"),
		),
		array(
			"map_gid" => "yandexmapsv2",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "user_search",
			"view_type" => 1,
			"zoom" => 5,
			"lat" => 0,
			"lon" => 0,
			"use_type_selector" => 1,
			"use_smart_zoom" => 1,
			"use_router" => 0,
			"use_searchbox" => 0,
			"use_tools" => 0,
			"use_clusterer" => 1,
			"use_click_zoom" => 1,
		),
		array(
			"map_gid" => "bingmapsv7",
			"id_user" => 0,
			"id_object" => 0,
			"gid" => "user_search",
			"view_type" => 1,
			"zoom" => 5,
			"lat" => 0,
			"lon" => 0,
			"use_type_selector" => 1,
			"use_smart_zoom" => 1,
			"use_router" => 0,
			"use_searchbox" => 0,
		),
	);
	
	/**
	 * Social networking configuration
	 * 
	 * @var array
	 */
	protected $social_networking = array(
		array(
			'controller' => 'users',
			'method' => 'view',
			'name' => 'View user page',
			'data' => array(
				'like' => array('facebook'=>'on', 'vkontakte'=>'on', 'google'=>'on'),
			),
		),
		array(
			"controller" => "users",
			"method" => "reg_private",
			"name" => "Private registration page",
			"data" => array(
				"like"=>array(
					"facebook"	=> "on",
					"vkontakte"	=> "on",
					"google"	=> "on",
				),
				"share" => array(
					"facebook" 	=> "on",
					"vkontakte" => "on",
					"linkedin"	=> "on",
					"twitter"	=> "on",
				),
				"comments" => "1",
			),
		),
		array(
			"controller" => "users",
			"method" => "reg_company",
			"name" => "Company registration page",
			"data" => array(
				"like"=>array(
					"facebook"	=> "on",
					"vkontakte"	=> "on",
					"google"	=> "on",
				),
				"share" => array(
					"facebook" 	=> "on",
					"vkontakte" => "on",
					"linkedin"	=> "on",
					"twitter"	=> "on",
				),
				"comments" => "1",
			),
		),
		array(
			"controller" => "users",
			"method" => "reg_agent",
			"name" => "Agent registration page",
			"data" => array(
				"like"=>array(
					"facebook"	=> "on",
					"vkontakte"	=> "on",
					"google"	=> "on",
				),
				"share" => array(
					"facebook" 	=> "on",
					"vkontakte" => "on",
					"linkedin"	=> "on",
					"twitter"	=> "on",
				),
				"comments" => "1",
			),
		),
	);
	
	/**
	 * Listings configuration
	 * 
	 * @var array
	 */
	protected $listings = array(
		"listings_fields" => array(
			"listings_for_sale_count" => array("type" => "int(3)", "null" => FALSE),
			"listings_for_buy_count" => array("type" => "int(3)", "null" => FALSE),
			"listings_for_rent_count" => array("type" => "int(3)", "null" => FALSE),
			"listings_for_lease_count" => array("type" => "int(3)", "null" => FALSE),
		),
	);
	
	/**
	 * Export configuration
	 * 
	 * @var array
	 */
	protected $export_data = array(
		"module" => "users",
		"model"	 => "Users_export_model",
		"callback_get_fields" => "callback_get_fields",
		"callback_get_admin_form" => "callback_get_admin_form",
		"callback_get_user_form" => "callback_get_user_form",
		"callback_process_form" => "callback_process_form",
		"callback_export_data" => "callback_export_data",
		"sorter" => "2",
	);
	
	/**
	 * Import configuration
	 * 
	 * @var array
	 */
	protected $import_data = array(
		"module" => "users",
		"model"	 => "Users_import_model",
		"callback_get_fields" => "callback_get_fields",
		"callback_import_data" => "callback_import_data",
		"sorter" => "2",
	);
	
	/**
	 * Constructor
	 *
	 * @return Users_install_model
	 */
	public function Users_install_model(){
		parent::Model();
		$this->CI = & get_instance();
	}
	
	/**
	 * Install data of menu module
	 * 
	 * @return void
	 */
	public function install_menu(){
		
		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]["id"] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, "create", $gid, 0, $this->menu[$gid]["items"]);
		}
	}
	
	/**
	 * Update languages of menu module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_menu_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read("users", "menu", $langs_ids);

		if(!$langs_file){log_message("info", "Empty menu langs data"); return false;}

		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, "update", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages of menu module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_menu_lang_export($langs_ids){
		if(empty($langs_ids)) return false;
		$this->CI->load->helper("menu");

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, "export", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array("menu" => $return);
	}
	
	/**
	 * Uninstall data of menu module
	 * 
	 * @return void
	 */
	public function deinstall_menu(){
		
		$this->CI->load->helper("menu");
		foreach($this->menu as $gid => $menu_data){
			if($menu_data["action"] == "create"){
				linked_install_set_menu($gid, "delete");
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]["items"]);
			}
		}
	}

	/**
	 * Install data of uploades module
	 * 
	 * @return void
	 */
	public function install_uploads(){
		///// upload config
		$this->CI->load->model("uploads/models/Uploads_config_model");
		
		$watermark_ids = array();
		
		foreach((array)$this->uploads as $upload_data){
			$config_data = array(
				"gid" 			=> $upload_data["gid"],
				"name" 			=> $upload_data["name"],
				"max_height" 	=> $upload_data["max_height"],
				"max_width" 	=> $upload_data["max_width"],
				"max_size" 		=> $upload_data["max_size"],
				"name_format" 	=> $upload_data["name_format"],
				"file_formats" 	=> serialize((array)$upload_data["file_formats"]),
				"default_img" 	=> $upload_data["default_img"],
				"date_add" => date("Y-m-d H:i:s"),
			);
			$config_id = $this->CI->Uploads_config_model->save_config(null, $config_data);
		
			$wm_data = $this->CI->Uploads_config_model->get_watermark_by_gid("image-wm");
			$wm_id = isset($wm_data["id"])?$wm_data["id"]:0;
			
			foreach((array)$upload_data["thumbs"] as $thumb_gid => $thumb_data){
				if(isset($thumb_data["watermark"])){
					if(!isset($watermark_ids[$thumb_data["watermark"]])){
						$wm_data = $this->CI->Uploads_config_model->get_watermark_by_gid($thumb_data["watermark"]);
						$watermark_ids[$thumb_data["watermark"]] = isset($wm_data["id"])?$wm_data["id"]:0;
					}
					$watermark_id = $watermark_ids[$thumb_data["watermark"]];
				}else{
					$watermark_id = 0;
				}
				
				$thumb_data = array(
					"config_id" 	=> $config_id,
					"prefix"		=> $thumb_gid,
					"width" 		=> $thumb_data["width"],
					"height" 		=> $thumb_data["height"],
					"effect" 		=> "none",
					"watermark_id" 	=> $watermark_id,
					"crop_param" 	=> $thumb_data["crop_param"],
					"crop_color" 	=> $thumb_data["crop_color"],
					"date_add" 		=> date("Y-m-d H:i:s"),
				);
				$this->CI->Uploads_config_model->save_thumb(null, $thumb_data);
			}
		}
	}

	/**
	 * Uninstall data of uploads module
	 * 
	 * @return void
	 */
	public function deinstall_uploads(){
		$this->CI->load->model("uploads/models/Uploads_config_model");
		
		foreach((array)$this->uploads as $upload_data){
			$config_data = $this->CI->Uploads_config_model->get_config_by_gid($upload_data["gid"]);
			if(!empty($config_data["id"])){
				$this->CI->Uploads_config_model->delete_config($config_data["id"]);
			}
		}
	}

	/**
	 * Install data of site_map module
	 * 
	 * @return void
	 */
	public function install_site_map(){
		//////// Site Map
		$this->CI->load->model("Site_map_model");
		$site_map_data = array(
			"module_gid" => "users",
			"model_name" => "Users_model",
			"get_urls_method" => "get_sitemap_urls",
		);
		$this->CI->Site_map_model->set_sitemap_module("users", $site_map_data);
	}
	
	/**
	 * Uninstall data of site_map module
	 * 
	 * @return void
	 */
	public function deinstall_site_map(){
		$this->CI->load->model("Site_map_model");
		$this->CI->Site_map_model->delete_sitemap_module("users");
	}

	/**
	 * Install data of banners module
	 * 
	 * @return void
	 */
	public function install_banners(){
		///// add banners module
		$this->CI->load->model("banners/models/Banner_group_model");
		$this->CI->Banner_group_model->set_module("users", "Users_model", "_banner_available_pages");
		$this->add_banners();
	}
	
	/**
	 * Import languages of banners module
	 * 
	 * @return void
	 */
	public function install_banners_lang_update(){
		$lang_ids = array_keys($this->CI->pg_language->languages);
		$lang_id = $this->CI->pg_language->get_default_lang_id();
 		$lang_data[$lang_id] = "Users pages";
		$this->CI->pg_language->pages->set_string_langs("banners", "banners_group_users_groups", $lang_data, $lang_ids);
	}

	/**
	 * Unistall data of banners module
	 * 
	 * @return void
	 */
	public function deinstall_banners(){
		// delete banners module
		$this->CI->load->model("banners/models/Banner_group_model");
		$this->CI->Banner_group_model->delete_module("users");
		$this->remove_banners();
	}
	
	/**
	 * Add banners to users group
	 * 
	 * @return void
	 */
	public function add_banners(){
		$lang_id = $this->CI->pg_language->get_default_lang_id();
		$lang_ids = array_keys($this->CI->pg_language->languages);

		$this->CI->load->model("Users_model");
		$this->CI->load->model("banners/models/Banner_group_model");
		$this->CI->load->model("banners/models/Banner_place_model");

		$group_id = $this->CI->Banner_group_model->get_group_id_by_gid("users_groups");
		///add pages in group
		$pages = $this->CI->Users_model->_banner_available_pages();
		if ($pages){
			foreach($pages  as $key => $value){
				$page_attrs = array(
					"group_id" => $group_id,
					"name" => $value["name"],
					"link" => $value["link"],
				);
				$this->CI->Banner_group_model->add_page($page_attrs);
			}
		}

		$lang_data[$lang_id] = "Users pages";
		$this->CI->pg_language->pages->set_string_langs("banners", "banners_group_users_groups", $lang_data, $lang_ids);
	}	

	/**
	 * Remove banners of users groups
	 * 
	 * @return void
	 */
	public function remove_banners(){
		$this->CI->load->model("banners/models/Banner_group_model");
		$group_id = $this->CI->Banner_group_model->get_group_id_by_gid("users_groups");
		$this->CI->Banner_group_model->delete($group_id);
	}

	/**
	 * Install data of contacts links
	 * 
	 * @return void
	 */
	public function install_linker(){
		///// add linker entry
		$this->CI->load->model("linker/models/linker_type_model");
		$this->CI->linker_type_model->create_type("users_contacts");
	}
	
	/**
	 * Uninstall data of contacts links
	 * 
	 * @return void
	 */
	public function deinstall_linker(){
		$this->CI->load->model("linker/models/linker_type_model");
		$this->CI->linker_type_model->delete_type("users_contacts");
	}

	/**
	 * Install data of moderation module
	 * 
	 * @return void
	 */
	public function install_moderation(){
		$this->CI->load->model('moderation/models/Moderation_type_model');
		foreach($this->moderation_types as $mtype) {
			$mtype['date_add'] = date("Y-m-d H:i:s");
			$this->CI->Moderation_type_model->save_type(null, $mtype);
		}
	}


	/**
	 * Uninstall languages of moderation module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_moderation_lang_update($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$langs_file = $this->CI->Install_model->language_file_read('users', 'moderation', $langs_ids);

		if(!$langs_file){log_message('info', 'Empty moderation langs data'); return false;}

		$this->CI->load->model('moderation/models/Moderation_type_model');
		$this->CI->Moderation_type_model->update_langs($this->moderation_types, $langs_file);
		
		return true;
	}

	/**
	 * Export languages of moderation module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_moderation_lang_export($langs_ids=null){
		if(!is_array($langs_ids)) $langs_ids = (array)$langs_ids;
		$this->CI->load->model('moderation/models/Moderation_type_model');
		$moderation = $this->CI->Moderation_type_model->export_langs($this->moderation_types, $langs_ids);
		return array('moderation' => $moderation);
	}

	/**
	 * Uninstall data of moderation module
	 * 
	 * @return void
	 */
	public function deinstall_moderation(){
		$this->CI->load->model('moderation/models/Moderation_type_model');
		foreach($this->moderation_types as $mtype) {
			$type = $this->CI->Moderation_type_model->get_type_by_name($mtype["name"]);
			$this->CI->Moderation_type_model->delete_type($type['id']);
		}	
	}

	/**
	 * Install data of moderators
	 * 
	 * @return void
	 */
	public function install_ausers(){
		// install ausers permissions
		$this->CI->load->model("Ausers_model");
		
		foreach((array)$this->ausers as $method_data){
			//$validate_data = $this->CI->Ausers_model->validate_method($method_data, true);
			$validate_data = array("errors"=>array(), "data"=>$method_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Ausers_model->save_method(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import languages of moderators
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_ausers_lang_update($langs_ids=null){
		$langs_file = $this->CI->Install_model->language_file_read("users", "ausers", $langs_ids);
		if(!$langs_file){log_message("info", "Empty ausers langs data"); return false;}

		// install ausers permissions
		$this->CI->load->model("Ausers_model");
		$params["where"]["module"] = "users";
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method["method"]])){
				$this->CI->Ausers_model->save_method($method["id"], array(), $langs_file[$method["method"]]);
			}
		}
		
		return true;
	}

	/**
	 * Export languages of moderators
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_ausers_lang_export($langs_ids){
		$this->CI->load->model("Ausers_model");
		$params["where"]["module"] = "users";
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method["method"]] = $method["langs"];
		}
		return array("ausers" => $return);
	}

	/**
	 * Uninstall data of moderators
	 * 
	 * @return void
	 */
	public function deinstall_ausers(){
		///// delete moderation methods in ausers
		$this->CI->load->model("Ausers_model");
		$params = array();
		$params["where"]["module"] = "users";
		$this->CI->Ausers_model->delete_methods($params);
	}

	/**
	 * Install data of notifications
	 * 
	 * @return void
	 */
	public function install_notifications(){
		// add notification
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");

		$templates_ids = array();

		foreach((array)$this->notifications["templates"] as $template_data){
			if(is_array($template_data["vars"])) $template_data["vars"] = implode(", ", $template_data["vars"]);			
			$validate_data = $this->CI->Templates_model->validate_template(null, $template_data);
			if(!empty($validate_data["errors"])) continue;
			$templates_ids[$template_data['gid']] = $this->CI->Templates_model->save_template(null, $validate_data["data"]);			
		}
		
		foreach((array)$this->notifications["notifications"] as $notification_data){
			if(!isset($templates_ids[$notification_data["template"]])){
				 $template = $this->CI->Templates_model->get_template_by_gid($notification_data["template"]);
				 $templates_ids[$notification_data["template"]] = $template["id"];
			}
			$notification_data["id_template_default"] = $templates_ids[$notification_data["template"]];
			$validate_data = $this->CI->Notifications_model->validate_notification(null, $notification_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Notifications_model->save_notification(null, $validate_data["data"], $lang_data);
		}
	}
	
	/**
	 * Import languages of notifiactions
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_notifications_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$this->CI->load->model("Notifications_model");
		
		$langs_file = $this->CI->Install_model->language_file_read("users", "notifications", $langs_ids);
		if(!$langs_file){log_message("info", "Empty notifications langs data");return false;}
		
		$this->CI->Notifications_model->update_langs($this->notifications, $langs_file, $langs_ids);
		return true;
	}
	
	/**
	 * Export languages of notifications
	 * 
	 * @param array $langs_ids languages identfiers
	 * @return array
	 */
	public function install_notifications_lang_export($langs_ids=null){
		$this->CI->load->model("Notifications_model");
		$langs = $this->CI->Notifications_model->export_langs($this->notifications, $langs_ids);
		return array("notifications" => $langs);
	}
	
	/**
	 * Unistall data of notifacations
	 * 
	 * @return void
	 */
	public function deinstall_notifications(){
		$this->CI->load->model("Notifications_model");
		$this->CI->load->model("notifications/models/Templates_model");
		
		foreach((array)$this->notifications["notifications"] as $notification_data){
			$this->CI->Notifications_model->delete_notification_by_gid($notification_data["gid"]);
		}
		
		foreach((array)$this->notifications["templates"] as $template_data){
			$this->CI->Templates_model->delete_template_by_gid($template_data["gid"]);
		}
	}

	/**
	 * Install data of dynamic blocks
	 * 
	 * @return void
	 */
	public function install_dynamic_blocks(){
		$this->CI->load->model("Dynamic_blocks_model");

		$area_ids = array();
		
		foreach((array)$this->dynamic_blocks as $block_data){
			
			$validate_data = $this->CI->Dynamic_blocks_model->validate_block(null, $block_data);
			if(!empty($validate_data["errors"])) continue;
			$id_block = $this->CI->Dynamic_blocks_model->save_block(null, $validate_data["data"]);
		
			if(empty($block_data["area"])) continue;
		
			if(!isset($area_ids[$block_data["area"]])){
				$area = $this->CI->Dynamic_blocks_model->get_area_by_gid($block_data["area"]);
				$area_ids[$block_data["area"]] = $area["id"];
			}

			// index area
			$area_data = array(
				"id_area" 		=> $area_ids[$block_data["area"]], 
				"id_block" 		=> $id_block, 
				"params" 		=> $validate_data["data"]["params"], 
				"view_str" 		=> $block_data["view_str"], 
				"cache_time" 	=> $block_data["cache_time"],
			);
			$validate_data = $this->CI->Dynamic_blocks_model->validate_area_block($area_data, true);
			if(!empty($validate_data["errors"])) continue;
			$temp_id = $this->CI->Dynamic_blocks_model->save_area_block(NULL, $validate_data["data"]);
			$this->CI->Dynamic_blocks_model->save_area_block_sorter($temp_id, $block_data["sorter"]);
		}
	}
	
	/**
	 * Install languages of dynamic blocks
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_dynamic_blocks_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		
		$langs_file = $this->CI->Install_model->language_file_read("users", "dynamic_blocks", $langs_ids);
		if(!$langs_file){log_message("info", "Empty dynamic_blocks langs data");return false;}
		
		$this->CI->load->model("Dynamic_blocks_model");
		
		$data = array();
		
		foreach((array)$this->dynamic_blocks as $block_data){
			$block = $this->CI->Dynamic_blocks_model->get_block_by_gid($block_data["gid"]);
			if(!$block) continue;
			$data[] = $block;
		}
		
		$this->CI->Dynamic_blocks_model->update_langs($data, $langs_file, $langs_ids);
		
		return true;
	}
	
	/**
	 * Export languages of dynamic blocks
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_dynamic_blocks_lang_export($langs_ids=null){
		$this->CI->load->model("Dynamic_blocks_model");
		
		$data = array();
		
		foreach((array)$this->dynamic_blocks as $block_data){
			$block = $this->CI->Dynamic_blocks_model->get_block_by_gid($block_data["gid"]);
			if(!$block) continue;
			$data[] = $block;
		}
		
		$langs = $this->CI->Dynamic_blocks_model->export_langs($data, $langs_ids);
		return array("dynamic_blocks" => $langs);
	}	

	/**
	 * Unistall data of dynamic blocks
	 * 
	 * @return void
	 */
	public function deinstall_dynamic_blocks(){
		$this->CI->load->model("Dynamic_blocks_model");
		
		foreach((array)$this->dynamic_blocks as $block_data){
			$this->CI->Dynamic_blocks_model->delete_block_by_gid($block_data["gid"]);
		}
	}
	
	/**
	 * Install data of reviews
	 * 
	 * @return void
	 */
	public function install_reviews(){
		
		$this->CI->load->model("Users_model");
		
		// add reviews type
		$this->CI->load->model("reviews/models/Reviews_type_model");		
		
		$this->CI->Users_model->install_reviews_fields((array)$this->reviews["reviews_fields"]);
	
		foreach((array)$this->reviews["reviews"] as $review_data){
			$validate_data = $this->CI->Reviews_type_model->validate_type(null, $review_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Reviews_type_model->save_type(null, $validate_data["data"]);
		}
		
		///// DEMO
		if(!INSTALL_DONE){
			include MODULEPATH."users/install/demo_content.php";
			
			$this->CI->load->model('Reviews_model');
			foreach((array)$reviews as $review){
				$validate_data = $this->CI->Reviews_model->validate_review(null, $review);
				if(!empty($validate_data['errors'])) continue;
				$this->CI->Reviews_model->save_review(null, $validate_data['data']);
			}
		}
	}
	
	/**
	 * Install languages of reviews
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_reviews_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$this->CI->load->model("Reviews_model");
		
		$langs_file = $this->CI->Install_model->language_file_read("users", "reviews", $langs_ids);
		if(!$langs_file){log_message("info", "Empty reviews langs data");return false;}
		
		foreach((array)$this->reviews["reviews"] as $review_data){
			$this->CI->Reviews_model->update_langs($review_data, $langs_file, $langs_ids);
		}
		
		foreach($langs_ids as $lang_id){
			foreach ((array)$this->reviews["rate_types"] as $type_gid=>$type_data){
				$types_data = array();
				foreach($type_data as $rate_type=>$votes){
					$votes_data = array();
					foreach($votes as $vote){
						$votes_data[$vote] = isset($langs_file[$type_gid.'_'.$rate_type."_votes_".$vote][$lang_id]) ?
							$langs_file[$type_gid.'_'.$rate_type."_votes_".$vote][$lang_id] : $vote;
					}
					$types_data[$rate_type] = array(
						"header" => $langs_file[$type_gid.'_'.$rate_type."_header"][$lang_id],
						"votes" => $votes_data,
					);
				}	
				$this->CI->Reviews_model->add_rate_type($type_gid, $types_data, $lang_id);
			}			
		}
		
		return true;
	}

	/**
	 * Export languages of reviews
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_reviews_lang_export($langs_ids){
		if(empty($langs_ids)) return false;
		$this->CI->load->model("Reviews_model");
		$langs = array();
		foreach((array)$this->reviews["reviews"] as $review_data){
			$langs = array_merge($langs, $this->CI->Reviews_model->export_langs($review_data['gid'], $langs_ids));
		}
		return array("reviews" => $langs);
	}

	/**
	 * Uninstall data of reviews
	 * 
	 * @return void
	 */
	public function deinstall_reviews(){
		
		$this->CI->load->model("Users_model");
		
		//add reviews type
		$this->CI->load->model("reviews/models/Reviews_type_model");

		foreach((array)$this->reviews["reviews"] as $review_data){
			$this->CI->Reviews_type_model->delete_type($review_data["gid"]);
		}
		
		$this->CI->Users_model->deinstall_reviews_fields(array_keys((array)$this->reviews["reviews_fields"]));
	}

	/**
	 * Install data of spam
	 * 
	 * @return void
	 */
	public function install_spam(){
		// add spam type
		$this->CI->load->model("spam/models/Spam_type_model");

		foreach((array)$this->spam as $spam_data){
			$validate_data = $this->CI->Spam_type_model->validate_type(null, $spam_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Spam_type_model->save_type(null, $validate_data["data"]);
		}
	}
	
	/**
	 * Import languages of spam
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_spam_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		
		$this->CI->load->model("spam/models/Spam_type_model");
		
		$langs_file = $this->CI->Install_model->language_file_read("users", "spam", $langs_ids);
		if(!$langs_file){log_message("info", "Empty spam langs data");return false;}
	
		$this->CI->Spam_type_model->update_langs($this->spam, $langs_file, $langs_ids);
		return true;
	}
	
	/**
	 * Export languages of spam
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_spam_lang_export($langs_ids=null){
		$this->CI->load->model("spam/models/Spam_type_model");
		$langs = $this->CI->Spam_type_model->export_langs((array)$this->spam, $langs_ids);
		return array("spam" => $langs);
	}
	
	/**
	 * Uninstall data of spam
	 * 
	 * @return void
	 */
	public function deinstall_spam(){
		//add spam type
		$this->CI->load->model("spam/models/Spam_type_model");

		foreach((array)$this->spam as $spam_data){
			$this->CI->Spam_type_model->delete_type($spam_data["gid"]);
		}
	}
	
	/**
	 * Install data of geomap
	 * 
	 * @return void
	 */
	public function install_geomap(){
		//add geomap settings
		$this->CI->load->model("geomap/models/Geomap_settings_model");
		
		foreach((array)$this->geomap as $settings){
			$map_gid = $settings["map_gid"];
			unset($settings["map_gid"]);
			
			$id_user = $settings["id_user"];
			unset($settings["id_user"]);
			
			$id_object = $settings["id_object"];
			unset($settings["id_object"]);
			
			$gid = $settings["gid"];
			unset($settings["gid"]);
		
			$validate_data = $this->CI->Geomap_settings_model->validate_settings($settings);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Geomap_settings_model->save_settings($map_gid, $id_user, $id_object, $gid, $validate_data["data"]);
		}
	}
	
	/**
	 * Install languages of geomap module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_geomap_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		
		$langs_file = $this->CI->Install_model->language_file_read("users", "geomap", $langs_ids);
		if(!$langs_file){log_message("info", "Empty geomap langs data");return false;}
		
		$this->CI->load->model("geomap/models/Geomap_settings_model");
		
		$gids = array();
		foreach((array)$this->geomap as $geomap_data){
			$gids[] = 'map_'.$geomap_data['gid'];
		}
		$this->CI->Geomap_settings_model->update_lang($gids, $langs_file, $langs_ids);
		
		return true;
	}
	
	/**
	 * Export languages of geomap module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_geomap_lang_export($langs_ids=null){
		$this->CI->load->model("geomap/models/Geomap_settings_model");
		
		$gids = array();
		foreach((array)$this->geomap as $geomap_data){
			$gids[] = 'map_'.$geomap_data['gid'];
		}
		$langs = $this->CI->Geomap_settings_model->export_lang($gids, $langs_ids);
		return array("geomap" => $langs);
	}
	
	/**
	 * Uninstall data of geomap module
	 * 
	 * @return void
	 */
	public function deinstall_geomap(){
		//add geomap settings
		$this->CI->load->model("geomap/models/Geomap_settings_model");
		
		foreach((array)$this->geomap as $settings){
			$map_gid = $settings["map_gid"];
			unset($settings["data"]["map_gid"]);
			
			$id_user = $settings["id_user"];
			unset($settings["id_user"]);
			
			$id_object = $settings["id_object"];
			unset($settings["id_object"]);
			
			$gid = $settings["gid"];
			unset($settings["gid"]);
			
			$this->CI->Geomap_settings_model->delete_settings($map_gid, $id_user, $id_object, $gid);
		}
	}

	/**
	 * Install data of social networking module
	 * 
	 * @return void
	 */
	public function install_social_networking() {
		///// add social netorking page
		$this->CI->load->model('social_networking/models/Social_networking_pages_model');
		foreach((array)$this->social_networking as $social_networking_data){
			$validation_data = $this->CI->Social_networking_pages_model->validate_page(null, $social_networking_data);
			if(!empty($validation_data['errors'])) continue;	
			$this->CI->Social_networking_pages_model->save_page(null, $validation_data['data']);
		}
	}
	
	/**
	 * Uninstall data of social networking module
	 * 
	 * @return void
	 */
	public function deinstall_social_networking() {
		///// delete social netorking page
		$this->CI->load->model('social_networking/models/Social_networking_pages_model');
		$this->CI->Social_networking_pages_model->delete_pages_by_controller('users');
	}

	/**
	 * Install data of lisitngs module
	 * 
	 * @return void
	 */
	public function install_listings(){
		$this->CI->load->model("Users_model");
		$this->CI->Users_model->install_listings_fields((array)$this->listings["listings_fields"]);
	}
	
	/**
	 * Uninstall data of listings module
	 * 
	 * @return void
	 */
	public function deinstall_listings(){
		$this->CI->load->model("Users_model");
		$this->CI->Users_model->deinstall_listings_fields(array_keys((array)$this->listings["listings_fields"]));
	}
	
	/**
	 * Install data of export module
	 * 
	 * @return void
	 */
	public function install_export(){
		$this->CI->load->model("export/models/Export_module_model");
		
		$validate_data = $this->CI->Export_module_model->validate_module(null, (array)$this->export_data);
		if(!empty($validate_data["errors"])) return;
		$this->CI->Export_module_model->save_module(null, $validate_data["data"]);
	}
	
	/**
	 * Import languages of export module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_export_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		
		$langs_file = $this->CI->Install_model->language_file_read("users", "export", $langs_ids);
		if(!$langs_file){log_message("info", "Empty export langs data");return false;}
		
		$this->CI->load->model("export/models/Export_module_model");
		$this->CI->Export_module_model->update_lang(array("module_users"), $langs_file, $langs_ids);
		
		return true;
	}
	
	/**
	 * Export languages of export module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_export_lang_export($langs_ids=null){
		$this->CI->load->model("export/models/Export_module_model");
		$langs = $this->CI->Export_module_model->export_lang(array("module_users"), $langs_ids);
		return array("export" => $langs);
	}
	
	/**
	 * Unistall data of export module
	 * 
	 * @return void
	 */
	public function deinstall_export(){
		$this->CI->load->model("export/models/Export_module_model");
		$this->CI->Export_module_model->remove_module("users");		
	}
	
	/**
	 * Install data of import module
	 * 
	 * @return void
	 */
	public function install_import(){
		$this->CI->load->model("import/models/Import_module_model");
		
		$validate_data = $this->CI->Import_module_model->validate_module(null, (array)$this->import_data);
		if(!empty($validate_data["errors"])) return;
		$this->CI->Import_module_model->save_module(null, $validate_data["data"]);
	}
	
	/**
	 * Import languages of import module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_import_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		
		$langs_file = $this->CI->Install_model->language_file_read("users", "import", $langs_ids);
		if(!$langs_file){log_message("info", "Empty import langs data");return false;}
		
		$this->CI->load->model("import/models/Import_module_model");
		$this->CI->Import_module_model->update_lang(array("module_users"), $langs_file, $langs_ids);
		
		return true;
	}
	
	/**
	 * Export languages of import module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_import_lang_export($langs_ids=null){
		$this->CI->load->model("import/models/Import_module_model");
		
		$langs = $this->CI->Import_module_model->export_lang(array("module_users"), $langs_ids);
		return array("import" => $langs);
	}
	
	/**
	 * Unistall data of import module
	 * 
	 * @return void
	 */
	public function deinstall_import(){
		$this->CI->load->model("import/models/Import_module_model");
		$this->CI->Import_module_model->remove_module("users");		
	}

	/**
	 * Install module data
	 * 
	 * @return void
	 */
	public function _arbitrary_installing(){

		$this->CI->load->model("users/models/Groups_model");
	
		// create groups
		foreach((array)$this->groups as $group_data){
			$validate_data = $this->CI->Groups_model->validate_group(null, $group_data);
			if(!empty($validate_data["errors"])) continue;
			$this->CI->Groups_model->save_group(null, $validate_data["data"]);
		}
		
		///// seo
		$this->CI->pg_seo->set_seo_module("users", (array)$this->seo);
		
		// add seo link
		$this->CI->load->model('Seo_model');
		
		$xml_data = $this->CI->Seo_model->get_xml_route_file_content();
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '[text:account]',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'account', $data);
		
		$xml_data['users']['account'] = $this->CI->pg_seo->url_template_transform('users', 'account', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '[text:login]',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'login_form', $data);
		
		$xml_data['users']['login_form'] = $this->CI->pg_seo->url_template_transform('users', 'login_form', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '[text:restore]',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'restore', $data);
		
		$xml_data['users']['restore'] = $this->CI->pg_seo->url_template_transform('users', 'restore', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '[text:profile/][tpl:1:action:literal:view]',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'profile', $data);
		
		$xml_data['users']['profile'] = $this->CI->pg_seo->url_template_transform('users', 'profile', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '[text:users/][tpl:1:user_type:literal:all][text:-index]',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'index', $data);
		
		$xml_data['users']['index'] = $this->CI->pg_seo->url_template_transform('users', 'index', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '[text:users/][tpl:1:user_type:literal:all][text:-search/][tpl:2:keyword:literal:empty]',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'search', $data);
		
		$xml_data['users']['search'] = $this->CI->pg_seo->url_template_transform('users', 'search', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 0,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '[text:users/][opt:user_type:literal:all][text:/][opt:name:literal:noname][text:-][tpl:1:id:numeric:][text:/][tpl:2:section:literal:contacts]',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'view', $data);

		$xml_data['users']['view'] = $this->CI->pg_seo->url_template_transform('users', 'view', $data['url_template'], 'base', 'xml');

		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '[text:registration-as-private-person]',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'reg_private', $data);
		
		$xml_data['users']['reg_private'] = $this->CI->pg_seo->url_template_transform('users', 'reg_private', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '[text:registration-as-company]',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'reg_company', $data);
		
		$xml_data['users']['reg_company'] = $this->CI->pg_seo->url_template_transform('users', 'reg_company', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '[text:registration-as-agent]',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'reg_agent', $data);

		$xml_data['users']['reg_agent'] = $this->CI->pg_seo->url_template_transform('users', 'reg_agent', $data['url_template'], 'base', 'xml');

		$this->CI->Seo_model->set_xml_route_file_content($xml_data);
		$this->CI->Seo_model->rewrite_route_php_file();
		
		//get administrator email
		if($this->pg_module->is_module_installed('ausers')){
			$this->CI->load->model("Ausers_model");
			$users = $this->CI->Ausers_model->get_users_list(null, 1, null, array('where'=>array('user_type'=>'admin')));
			if(!empty($users)){ 
				$this->CI->pg_module->set_module_config("users", 'deactivated_email', $users[0]["email"]);			
			}
		}
		
		///// DEMO
		if(!INSTALL_DONE){
			$this->add_demo_content();
		}
	}

	/**
	 * Import module languages
	 * 
	 * @return boolean
	 */
	public function _arbitrary_lang_install(){
		$langs_file = $this->CI->Install_model->language_file_read("users", "arbitrary");
		if(!$langs_file){log_message("info", "Empty arbitrary langs data");return false;}

		$this->CI->load->model("users/models/Groups_model");
		
		foreach((array)$this->groups as $group_data){
			$group = $this->CI->Groups_model->get_group_by_gid($group_data["gid"]);

			$this->CI->pg_language->pages->set_string_langs("groups_langs",
															"group_item_".$group["id"],
															$langs_file["groups_demo_".$group_data["gid"]],
															array_keys($langs_file["groups_demo_".$group_data["gid"]]));
		}
		
		$post_data = array(
			"title" => $langs_file["seo_tags_account_title"],
			"keyword" => $langs_file["seo_tags_account_keyword"],
			"description" => $langs_file["seo_tags_account_description"],
			"header" => $langs_file["seo_tags_account_header"],
			"og_title" => $langs_file["seo_tags_account_og_title"],
			"og_type" => $langs_file["seo_tags_account_og_type"],
			"og_description" => $langs_file["seo_tags_account_og_description"],
		);
		$this->CI->pg_seo->set_settings("user", "users", "account", $post_data);
		
		$post_data = array(
			"title" => $langs_file["seo_tags_index_title"],
			"keyword" => $langs_file["seo_tags_index_keyword"],
			"description" => $langs_file["seo_tags_index_description"],
			"header" => $langs_file["seo_tags_index_header"],
			"og_title" => $langs_file["seo_tags_index_og_title"],
			"og_type" => $langs_file["seo_tags_index_og_type"],
			"og_description" => $langs_file["seo_tags_index_og_description"],
		);
		$this->CI->pg_seo->set_settings("user", "users", "index", $post_data);
		
		$post_data = array(
			"title" => $langs_file["seo_tags_login_form_title"],
			"keyword" => $langs_file["seo_tags_login_form_keyword"],
			"description" => $langs_file["seo_tags_login_form_description"],
			"header" => $langs_file["seo_tags_login_form_header"],
			"og_title" => $langs_file["seo_tags_login_form_og_title"],
			"og_type" => $langs_file["seo_tags_login_form_og_type"],
			"og_description" => $langs_file["seo_tags_login_form_og_description"],
		);
		$this->CI->pg_seo->set_settings("user", "users", "login_form", $post_data);
		
		$post_data = array(
			"title" => $langs_file["seo_tags_profile_title"],
			"keyword" => $langs_file["seo_tags_profile_keyword"],
			"description" => $langs_file["seo_tags_profile_description"],
			"header" => $langs_file["seo_tags_profile_header"],
			"og_title" => $langs_file["seo_tags_profile_og_title"],
			"og_type" => $langs_file["seo_tags_profile_og_type"],
			"og_description" => $langs_file["seo_tags_profile_og_description"],
		);
		$this->CI->pg_seo->set_settings("user", "users", "profile", $post_data);
		
		$post_data = array(
			"title" => $langs_file["seo_tags_register_agent_title"],
			"keyword" => $langs_file["seo_tags_register_agent_keyword"],
			"description" => $langs_file["seo_tags_register_agent_description"],
			"header" => $langs_file["seo_tags_register_agent_header"],
			"og_title" => $langs_file["seo_tags_register_agent_og_title"],
			"og_type" => $langs_file["seo_tags_register_agent_og_type"],
			"og_description" => $langs_file["seo_tags_register_agent_og_description"],
		);
		$this->CI->pg_seo->set_settings("user", "users", "reg_agent", $post_data);
		
		$post_data = array(
			"title" => $langs_file["seo_tags_register_company_title"],
			"keyword" => $langs_file["seo_tags_register_company_keyword"],
			"description" => $langs_file["seo_tags_register_company_description"],
			"header" => $langs_file["seo_tags_register_company_header"],
			"og_title" => $langs_file["seo_tags_register_company_og_title"],
			"og_type" => $langs_file["seo_tags_register_company_og_type"],
			"og_description" => $langs_file["seo_tags_register_company_og_description"],
		);
		$this->CI->pg_seo->set_settings("user", "users", "reg_company", $post_data);
		
		$post_data = array(
			"title" => $langs_file["seo_tags_register_private_title"],
			"keyword" => $langs_file["seo_tags_register_private_keyword"],
			"description" => $langs_file["seo_tags_register_private_description"],
			"header" => $langs_file["seo_tags_register_private_header"],
			"og_title" => $langs_file["seo_tags_register_private_og_title"],
			"og_type" => $langs_file["seo_tags_register_private_og_type"],
			"og_description" => $langs_file["seo_tags_register_private_og_description"],
		);
		$this->CI->pg_seo->set_settings("user", "users", "reg_private", $post_data);
		
		$post_data = array(
			"title" => $langs_file["seo_tags_restore_title"],
			"keyword" => $langs_file["seo_tags_restore_keyword"],
			"description" => $langs_file["seo_tags_restore_description"],
			"header" => $langs_file["seo_tags_restore_header"],
			"og_title" => $langs_file["seo_tags_restore_og_title"],
			"og_type" => $langs_file["seo_tags_restore_og_type"],
			"og_description" => $langs_file["seo_tags_restore_og_description"],
		);
		$this->CI->pg_seo->set_settings("user", "users", "restore", $post_data);
		
		$post_data = array(
			"title" => $langs_file["seo_tags_search_title"],
			"keyword" => $langs_file["seo_tags_search_keyword"],
			"description" => $langs_file["seo_tags_search_description"],
			"header" => $langs_file["seo_tags_search_header"],
			"og_title" => $langs_file["seo_tags_search_og_title"],
			"og_type" => $langs_file["seo_tags_search_og_type"],
			"og_description" => $langs_file["seo_tags_search_og_description"],
		);
		$this->CI->pg_seo->set_settings("user", "users", "search", $post_data);
		
		$post_data = array(
			"title" => $langs_file["seo_tags_view_title"],
			"keyword" => $langs_file["seo_tags_view_keyword"],
			"description" => $langs_file["seo_tags_view_description"],
			"header" => $langs_file["seo_tags_view_header"],
			"og_title" => $langs_file["seo_tags_view_og_title"],
			"og_type" => $langs_file["seo_tags_view_og_type"],
			"og_description" => $langs_file["seo_tags_view_og_description"],
		);
		$this->CI->pg_seo->set_settings("user", "users", "view", $post_data);
	}

	/**
	 * Export module languages
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function _arbitrary_lang_export($langs_ids=null){
		$this->CI->load->model("users/models/Groups_model");
		
		foreach((array)$this->groups as $group_data){
			$group = $this->CI->Groups_model->get_group_by_gid($group_data["gid"]);
			$blocks['groups_demo_'.$group_data["gid"]] = "group_item_".$group["id"];
		}

		$group_langs = $this->CI->pg_language->export_langs("groups_langs", $blocks, $langs_ids);
		$langs = array();
		if(!empty($group_langs)){
			$blocks = array_flip($blocks);
			foreach($group_langs as $group_gid=>$group_data){
				if(!isset($blocks[$group_gid])) continue;
				$langs[$blocks[$group_gid]] = $group_data;
			}
		}
		
		//// arbitrary
		$settings = $this->CI->pg_seo->get_settings("user", "users", "account", $langs_ids);
		$arbitrary_return["seo_tags_account_title"] = $settings["title"];
		$arbitrary_return["seo_tags_account_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_account_description"] = $settings["description"];
		$arbitrary_return["seo_tags_account_header"] = $settings["header"];
		$arbitrary_return["seo_tags_account_og_title"] = $settings["og_title"];
		$arbitrary_return["seo_tags_account_og_type"] = $settings["og_type"];
		$arbitrary_return["seo_tags_account_og_description"] = $settings["og_description"];
		
		$settings = $this->CI->pg_seo->get_settings("user", "users", "index", $langs_ids);
		$arbitrary_return["seo_tags_index_title"] = $settings["title"];
		$arbitrary_return["seo_tags_index_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_index_description"] = $settings["description"];
		$arbitrary_return["seo_tags_index_header"] = $settings["header"];
		$arbitrary_return["seo_tags_index_og_title"] = $settings["og_title"];
		$arbitrary_return["seo_tags_index_og_type"] = $settings["og_type"];
		$arbitrary_return["seo_tags_index_og_description"] = $settings["og_description"];
		
		$settings = $this->CI->pg_seo->get_settings("user", "users", "login_form", $langs_ids);
		$arbitrary_return["seo_tags_login_form_title"] = $settings["title"];
		$arbitrary_return["seo_tags_login_form_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_login_form_description"] = $settings["description"];
		$arbitrary_return["seo_tags_login_form_header"] = $settings["header"];
		$arbitrary_return["seo_tags_login_form_og_title"] = $settings["og_title"];
		$arbitrary_return["seo_tags_login_form_og_type"] = $settings["og_type"];
		$arbitrary_return["seo_tags_login_form_og_description"] = $settings["og_description"];
		
		$settings = $this->CI->pg_seo->get_settings("user", "users", "profile", $langs_ids);
		$arbitrary_return["seo_tags_profile_title"] = $settings["title"];
		$arbitrary_return["seo_tags_profile_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_profile_description"] = $settings["description"];
		$arbitrary_return["seo_tags_profile_header"] = $settings["header"];
		$arbitrary_return["seo_tags_profile_og_title"] = $settings["og_title"];
		$arbitrary_return["seo_tags_profile_og_type"] = $settings["og_type"];
		$arbitrary_return["seo_tags_profile_og_description"] = $settings["og_description"];
		
		$settings = $this->CI->pg_seo->get_settings("user", "users", "reg_agent", $langs_ids);
		$arbitrary_return["seo_tags_register_agent_title"] = $settings["title"];
		$arbitrary_return["seo_tags_register_agent_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_register_agent_description"] = $settings["description"];
		$arbitrary_return["seo_tags_register_agent_header"] = $settings["header"];
		$arbitrary_return["seo_tags_register_agent_og_title"] = $settings["og_title"];
		$arbitrary_return["seo_tags_register_agent_og_type"] = $settings["og_type"];
		$arbitrary_return["seo_tags_register_agent_og_description"] = $settings["og_description"];
		
		$settings = $this->CI->pg_seo->get_settings("user", "users", "reg_company", $langs_ids);
		$arbitrary_return["seo_tags_register_company_title"] = $settings["title"];
		$arbitrary_return["seo_tags_register_company_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_register_company_description"] = $settings["description"];
		$arbitrary_return["seo_tags_register_company_header"] = $settings["header"];
		$arbitrary_return["seo_tags_register_company_og_title"] = $settings["og_title"];
		$arbitrary_return["seo_tags_register_company_og_type"] = $settings["og_type"];
		$arbitrary_return["seo_tags_register_company_og_description"] = $settings["og_description"];
		
		$settings = $this->CI->pg_seo->get_settings("user", "users", "reg_private", $langs_ids);
		$arbitrary_return["seo_tags_register_private_title"] = $settings["title"];
		$arbitrary_return["seo_tags_register_private_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_register_private_description"] = $settings["description"];
		$arbitrary_return["seo_tags_register_private_header"] = $settings["header"];
		$arbitrary_return["seo_tags_register_private_og_title"] = $settings["og_title"];
		$arbitrary_return["seo_tags_register_private_og_type"] = $settings["og_type"];
		$arbitrary_return["seo_tags_register_private_og_description"] = $settings["og_description"];
		
		$settings = $this->CI->pg_seo->get_settings("user", "users", "restore", $langs_ids);
		$arbitrary_return["seo_tags_restore_title"] = $settings["title"];
		$arbitrary_return["seo_tags_restore_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_restore_description"] = $settings["description"];
		$arbitrary_return["seo_tags_restore_header"] = $settings["header"];
		$arbitrary_return["seo_tags_restore_og_title"] = $settings["og_title"];
		$arbitrary_return["seo_tags_restore_og_type"] = $settings["og_type"];
		$arbitrary_return["seo_tags_restore_og_description"] = $settings["og_description"];
		
		$settings = $this->CI->pg_seo->get_settings("user", "users", "search", $langs_ids);
		$arbitrary_return["seo_tags_search_title"] = $settings["title"];
		$arbitrary_return["seo_tags_search_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_search_description"] = $settings["description"];
		$arbitrary_return["seo_tags_search_header"] = $settings["header"];
		$arbitrary_return["seo_tags_search_og_title"] = $settings["og_title"];
		$arbitrary_return["seo_tags_search_og_type"] = $settings["og_type"];
		$arbitrary_return["seo_tags_search_og_description"] = $settings["og_description"];
		
		$settings = $this->CI->pg_seo->get_settings("user", "users", "view", $langs_ids);
		$arbitrary_return["seo_tags_view_title"] = $settings["title"];
		$arbitrary_return["seo_tags_view_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_view_description"] = $settings["description"];
		$arbitrary_return["seo_tags_view_header"] = $settings["header"];
		$arbitrary_return["seo_tags_view_og_title"] = $settings["og_title"];
		$arbitrary_return["seo_tags_view_og_type"] = $settings["og_type"];
		$arbitrary_return["seo_tags_view_og_description"] = $settings["og_description"];
		
		return array('arbitrary' => $langs, "arbitrary" => $arbitrary_return);
	}

	/**
	 * Unistall module data
	 * 
	 * @return void
	 */
	public function _arbitrary_deinstalling(){
		// remove seo link
		$this->CI->load->model('Seo_model');
		
		$xml_data = $this->CI->Seo_model->get_xml_route_file_content();
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'account', $data);
		
		$xml_data['users']['account'] = $this->CI->pg_seo->url_template_transform('users', 'account', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'login_form', $data);
		
		$xml_data['users']['login_form'] = $this->CI->pg_seo->url_template_transform('users', 'login_form', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'restore', $data);
		
		$xml_data['users']['restore'] = $this->CI->pg_seo->url_template_transform('users', 'restore', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'profile', $data);
		
		$xml_data['users']['profile'] = $this->CI->pg_seo->url_template_transform('users', 'profile', $data['url_template'], 'base', 'xml');
		
		
		$data = array(
			'noindex' => 0,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'index', $data);
		
		$xml_data['users']['index'] = $this->CI->pg_seo->url_template_transform('users', 'index', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 0,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'search', $data);
		
		$xml_data['users']['search'] = $this->CI->pg_seo->url_template_transform('users', 'search', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 0,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'view', $data);

		$xml_data['users']['view'] = $this->CI->pg_seo->url_template_transform('users', 'view', $data['url_template'], 'base', 'xml');

		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'reg_private', $data);
		
		$xml_data['users']['view'] = $this->CI->pg_seo->url_template_transform('users', 'reg_private', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'reg_company', $data);
		
		$xml_data['users']['reg_company'] = $this->CI->pg_seo->url_template_transform('users', 'reg_company', $data['url_template'], 'base', 'xml');
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '',
		);
		$this->CI->pg_seo->set_settings('user', 'users', 'reg_agent', $data);

		$xml_data['users']['reg_agent'] = $this->CI->pg_seo->url_template_transform('users', 'reg_agent', $data['url_template'], 'base', 'xml');

		$this->CI->Seo_model->set_xml_route_file_content($xml_data);
		$this->CI->Seo_model->rewrite_route_php_file();
		
		$this->CI->pg_seo->delete_seo_module("users");
	}

	// looks like not in use
	
	/**
	 * Return languages for menu of remove module
	 * 
	 * @params array $langs languages data
	 * @param string $menu_gid menu guid
	 * @param string $item_gid menu item guid
	 * @return array
	 */
	public function get_menu_lang_delete($langs, $menu_gid, $item_gid){
		$lang_data = array();
		foreach ($this->CI->pg_language->languages as $lang) {
			$lang_data[$lang["id"]] = $langs[$lang["code"]][$menu_gid][$item_gid];
		}
		return $lang_data;
	}
	
	/**
	 * Install demo content
	 * 
	 * @return void
	 */
	public function add_demo_content(){
		$this->CI->load->model("users/models/Users_import_model");
		
		include MODULEPATH."users/install/demo_content.php";
		
		foreach((array)$demo_content as $user_gid=>$user_data){
			if(isset($user_data['id']) && isset($user_data['user_logo'])){
				$user_logo = $user_data['user_logo'];
				unset($user_data['user_logo']);
			}else{
				$user_logo;
			}
			$this->CI->Users_import_model->callback_import_data($user_data, (array)$relations);
			if($user_logo) $this->CI->Users_model->save_user($user_data['id'], array('user_logo'=>$user_logo));
		}
	
		return true;
	}
}

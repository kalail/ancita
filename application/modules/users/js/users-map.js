function usersMap(optionArr){
	this.properties = {
		siteUrl: '',
		mapAjaxUrl: 'users/ajax_users',
		
		order: 'date_created',
		orderDirection: 'DESC',
		page: 1,
		
		mapBlockId: 'users_map',
		sectionId: 'users_sections',
		errorObj: new Errors(),
		tIds: []
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.init_links();
	}
	
	this.init_links = function(){
		$('#' + _self.properties.sectionId + ' li').bind('click', function(){
			var id = $(this).attr('id');
			$('#' + _self.properties.sectionId + ' li').removeClass('active');
			$('#'+id).addClass('active');
			_self.properties.userType = $('#'+id).attr('sgid');
			_self.properties.page = 1;
			_self.loading_map();
			return false;
		});
		
		if(_self.properties.tIds.length){
			for(var index in _self.properties.tIds){
				var id = _self.properties.tIds[index];
				$(document).on('change', '#'+id+' select', function(){
					_self.properties.order = $(this).val();
					_self.loading_map();
					return false;
				});
				$(document).on('click', '#'+id+' [name=sorter_btn]', function(){
					if(_self.properties.orderDirection == 'ASC'){
						_self.properties.orderDirection = 'DESC';
					}else{
						_self.properties.orderDirection = 'ASC';
					}
					_self.loading_map();
					return false;
				});
				$(document).on('click', '#'+id+'>.pages a[data-page]', function(){
					_self.properties.page = $(this).attr('data-page');
					_self.loading_map();
					return false;
				});				
			}
		}
	}
	
	this.loading_map = function(url){
		if(!url){
			url = _self.properties.siteUrl + _self.properties.mapAjaxUrl + '/';
			if(_self.properties.userType) url += _self.properties.userType + '/';
			if(_self.properties.order) url += _self.properties.order + '/';
			if(_self.properties.orderDirection) url += _self.properties.orderDirection + '/';
			url +=  _self.properties.page;
		}
		
		var randNumber = Math.round(Math.random(1000)*1000);
		
		$('body').append('<div id="search_load_content'+randNumber+'" style="position: absolute; top: 0; left: 0; bottom: 0; right: 0; text-align: center; background: rgba(0,0,0,0.2); z-index: 10;"><!--div class="ajax_notice" style="display: inline-block;"><div class="loading">Loading...</div></div--></div>').css({'position': 'relative'});
		
		$.ajax({
			url: url, 
			type: 'GET',
			cache: false,
			success: function(data){
				$('#'+_self.properties.mapBlockId).append(data);
			},
			complete: function(){
				$('#search_load_content'+randNumber).remove();
			}
		});
	}
	
	this.loading_post_map = function(post_data, url){
		if(!url){
			url = _self.properties.siteUrl + _self.properties.viewAjaxUrl;
		}
		
		var randNumber = Math.round(Math.random(1000)*1000);
		
		$('body').append('<div id="search_load_content'+randNumber+'" style="position: absolute; top: 0; left: 0; bottom: 0; right: 0; text-align: center; background: rgba(0,0,0,0.2); z-index: 10;"><!--div class="ajax_notice" style="display: inline-block;"><div class="loading">Loading...</div></div--></div>').css({'position': 'relative'});
		
		$.ajax({
			url: url, 
			type: 'POST',
			data: post_data,
			cache: false,
			success: function(data){			
				$('#'+_self.properties.mapBlockId).append(data);
			},
			complete: function(){
				$('#search_load_content'+randNumber).remove();
			}
		});
	}
		
	_self.Init(optionArr);
}

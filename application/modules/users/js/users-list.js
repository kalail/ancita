function usersList(optionArr){
	this.properties = {
		siteUrl: '',
		listAjaxUrl: 'users/ajax_users',
		
		userType: null,
		order: null,
		orderDirection: null,
		page: 1,
		
		listBlockId: 'users_block',
		sectionId: 'users_sections',
		errorObj: new Errors(),
		tIds: []
	}

	var _self = this;


	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.init_links();
		_self.init_form();
	}
	
	this.init_links = function(){
		if(_self.properties.userType){
			$('#' + _self.properties.sectionId + ' li').bind('click', function(){
				var id = $(this).attr('id');
				$('#' + _self.properties.sectionId + ' li').removeClass('active');
				$('#'+id).addClass('active');
				_self.properties.userType = $('#'+id).attr('sgid');
				_self.properties.page = 1;
				_self.loading_block();
				return false;
			});
		}
		
		if(_self.properties.tIds.length){
			for(var index in _self.properties.tIds){
				var id = _self.properties.tIds[index];
				$(document).on('change', '#'+id+' select', function(){
					_self.properties.order = $(this).val();
					_self.loading_block();
					return false;
				});
				$(document).on('click', '#'+id+' [name=sorter_btn]', function(){
					if(_self.properties.orderDirection == 'ASC'){
						_self.properties.orderDirection = 'DESC';
					}else{
						_self.properties.orderDirection = 'ASC';
					}
					_self.loading_block();
					return false;
				});
				$(document).on('click', '#'+id+'>.pages a[data-page]', function(){
					_self.properties.page = $(this).attr('data-page');
					var url = _self.properties.siteUrl + _self.properties.listAjaxUrl + '/' +
					_self.loading_block();
					return false;
				});				
			}
		}
	}
	
	this.init_form = function(){
		$('#agent_search').bind('keyup', function(){
			var data = $('#agent_search_form').serialize();
			_self.search(data);
			return false;
		});
	}

	this.loading_block = function(url){
		if(!url){
			url = _self.properties.siteUrl + _self.properties.listAjaxUrl + '/';
			if(_self.properties.userType) url += _self.properties.userType + '/';
			if(_self.properties.order) url += _self.properties.order + '/';
			if(_self.properties.orderDirection) url += _self.properties.orderDirection + '/';
			url +=  _self.properties.page;
		}
		
		var randNumber = Math.round(Math.random(1000)*1000);
		
		$('body').append('<div id="search_load_content'+randNumber+'" class="ajax_search"></div>');
		
		$.ajax({
			url: url, 
			type: 'GET',
			cache: false,
			success: function(data){
				$('#'+_self.properties.listBlockId).html(data);
			},
			complete: function(){
				$('#search_load_content'+randNumber).remove();
			}
		});
	}
	
	this.loading_post_block = function(post_data, url){
		if(!url){
			url = _self.properties.siteUrl + _self.properties.viewAjaxUrl;
		}
		
		$('body').append('<div id="search_load_content'+randNumber+'" class="ajax_search"></div>');
		
		$.ajax({
			url: url, 
			type: 'POST',
			data: post_data,
			cache: false,
			success: function(data){
			
				$('#'+_self.properties.listBlockId).html(data);
			},
			complete: function(){
				$('#search_load_content'+randNumber).remove();
			}
		});
	}
		
	_self.Init(optionArr);
}

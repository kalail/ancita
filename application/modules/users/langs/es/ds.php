<?php

$install_lang["user_type"]["header"] = "Tipo de usuario";
$install_lang["user_type"]["option"]["private"] = "Particular";
$install_lang["user_type"]["option"]["company"] = "Agencia";
$install_lang["user_type"]["option"]["agent"] = "Agente";

$install_lang["professionals"]["header"] = "Profesionales";
$install_lang["professionals"]["option"]["company"] = "Agencia";
$install_lang["professionals"]["option"]["agent"] = "Agente";
$install_lang["professionals"]["option"]["private"] = "Particular";

$install_lang["deactivated_reason_object"]["header"] = "Razón desactivado";
$install_lang["deactivated_reason_object"]["option"]["1"] = "Tengo problemas de privacidad";
$install_lang["deactivated_reason_object"]["option"]["2"] = "No me parece útil";
$install_lang["deactivated_reason_object"]["option"]["3"] = "No entiendo cómo usarlo";
$install_lang["deactivated_reason_object"]["option"]["4"] = "Es temporal, voy a estar de vuelta";
$install_lang["deactivated_reason_object"]["option"]["5"] = "Otro";



<?php

$install_lang["admin_menu_main_items_users_menu_item"] = "Usuarios";
$install_lang["admin_menu_main_items_users_menu_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_feedbacks-items_users_deact_alers_menu_item"] = "Desactivación de perfil";
$install_lang["admin_menu_settings_items_feedbacks-items_users_deact_alers_menu_item_tooltip"] = "Establecer razones y ver perfil alertas de desactivación";
$install_lang["admin_menu_settings_items_system-items_users_deact_sett_menu_item"] = "Desactivación de perfil";
$install_lang["admin_menu_settings_items_system-items_users_deact_sett_menu_item_tooltip"] = "Establecer razones y ver perfil alertas de desactivación";
$install_lang["admin_users_menu_groups_list_item"] = "Grupos de usuarios";
$install_lang["admin_users_menu_groups_list_item_tooltip"] = "";
$install_lang["admin_users_menu_users_list_item"] = "Usuarios";
$install_lang["admin_users_menu_users_list_item_tooltip"] = "";
$install_lang["admin_users_settings_menu_users_deactivated_alerts_item"] = "Desactivaciones";
$install_lang["admin_users_settings_menu_users_deactivated_alerts_item_tooltip"] = "";
$install_lang["admin_users_settings_menu_users_deactivated_reasons_item"] = "Razones para la desactivación";
$install_lang["admin_users_settings_menu_users_deactivated_reasons_item_tooltip"] = "";
$install_lang["admin_users_settings_menu_users_deactivated_settings_item"] = "Configuración";
$install_lang["admin_users_settings_menu_users_deactivated_settings_item_tooltip"] = "";
$install_lang["admin_users_types_menu_users_types_agent_item"] = "Agentes";
$install_lang["admin_users_types_menu_users_types_agent_item_tooltip"] = "";
$install_lang["admin_users_types_menu_users_types_company_item"] = "Agencias";
$install_lang["admin_users_types_menu_users_types_company_item_tooltip"] = "";
$install_lang["admin_users_types_menu_users_types_private_item"] = "Particulares";
$install_lang["admin_users_types_menu_users_types_private_item_tooltip"] = "";
$install_lang["admin_users_types_menu_users_types_settings_item"] = "Configuración";
$install_lang["admin_users_types_menu_users_types_settings_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_account_item"] = "Mi cuenta";
$install_lang["agent_account_menu_agent_my_account_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_profile_item"] = "Mi perfil";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_change_regdata_item"] = "Datos de registro";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_change_regdata_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_deactivate_item"] = "Desactivar perfil";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_deactivate_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_edit_item"] = "Editar perfil";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_edit_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_services_item"] = "Los servicios de pago";
$install_lang["agent_account_menu_agent_my_profile_item_agent_my_profile_services_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_profile_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_overview_item"] = "Visión general";
$install_lang["agent_account_menu_agent_overview_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_payments_item"] = "Pagos";
$install_lang["agent_account_menu_agent_payments_item_agent_my_account_balance_item"] = "Balance de la cuenta";
$install_lang["agent_account_menu_agent_payments_item_agent_my_account_balance_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_payments_item_tooltip"] = "";
$install_lang["agent_main_menu_agent-main-menu-pro-item"] = "Profesionales";
$install_lang["agent_main_menu_agent-main-menu-pro-item_agent-main-menu-pro-agent-item"] = "Encontrar un agente";
$install_lang["agent_main_menu_agent-main-menu-pro-item_agent-main-menu-pro-agent-item_tooltip"] = "";
$install_lang["agent_main_menu_agent-main-menu-pro-item_agent-main-menu-pro-company-item"] = "Encontrar una agencia";
$install_lang["agent_main_menu_agent-main-menu-pro-item_agent-main-menu-pro-company-item_tooltip"] = "";
$install_lang["agent_main_menu_agent-main-menu-pro-item_agent-main-menu-pro-private-item"] = "Encontrar a un particular";
$install_lang["agent_main_menu_agent-main-menu-pro-item_agent-main-menu-pro-private-item_tooltip"] = "";
$install_lang["agent_main_menu_agent-main-menu-pro-item_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-logout-item"] = "Finalizar la sesión";
$install_lang["agent_top_menu_agent-main-logout-item_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-my-account-item"] = "Opciones de cuenta";
$install_lang["agent_top_menu_agent-main-my-account-item_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-my-profile-item"] = "Mi perfil";
$install_lang["agent_top_menu_agent-main-my-profile-item_tooltip"] = "";
$install_lang["company_account_menu_company_my_account_item"] = "Mi cuenta";
$install_lang["company_account_menu_company_my_account_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_agents_item"] = "Mis agentes";
$install_lang["company_account_menu_company_my_agents_item_company_my_agents_list_item"] = "Lista de agentes";
$install_lang["company_account_menu_company_my_agents_item_company_my_agents_list_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_agents_item_company_my_agents_requests_item"] = "Consultas de agente";
$install_lang["company_account_menu_company_my_agents_item_company_my_agents_requests_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_agents_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_profile_item"] = "Mi perfil";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_change_regdata_item"] = "Los datos de registro";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_change_regdata_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_deactivate_item"] = "Perfil desactivar";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_deactivate_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_edit_item"] = "Editar perfil";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_edit_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_services_item"] = "Servicios pagados";
$install_lang["company_account_menu_company_my_profile_item_company_my_profile_services_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_profile_item_tooltip"] = "";
$install_lang["company_account_menu_company_overview_item"] = "Visión general";
$install_lang["company_account_menu_company_overview_item_tooltip"] = "";
$install_lang["company_account_menu_company_payments_item"] = "Pagos";
$install_lang["company_account_menu_company_payments_item_company_my_account_balance_item"] = "Balance de la cuenta";
$install_lang["company_account_menu_company_payments_item_company_my_account_balance_item_tooltip"] = "";
$install_lang["company_account_menu_company_payments_item_tooltip"] = "";
$install_lang["company_main_menu_company-main-menu-pro-item"] = "Profesionales";
$install_lang["company_main_menu_company-main-menu-pro-item_company-main-menu-pro-agent-item"] = "Encontrar un agente";
$install_lang["company_main_menu_company-main-menu-pro-item_company-main-menu-pro-agent-item_tooltip"] = "";
$install_lang["company_main_menu_company-main-menu-pro-item_company-main-menu-pro-company-item"] = "Encontrar una agencia";
$install_lang["company_main_menu_company-main-menu-pro-item_company-main-menu-pro-company-item_tooltip"] = "";
$install_lang["company_main_menu_company-main-menu-pro-item_company-main-menu-pro-private-item"] = "Encontrar a una persona privada";
$install_lang["company_main_menu_company-main-menu-pro-item_company-main-menu-pro-private-item_tooltip"] = "";
$install_lang["company_main_menu_company-main-menu-pro-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-logout-item"] = "Finalizar la sesión";
$install_lang["company_top_menu_company-main-logout-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-my-account-item"] = "Opciones de cuenta";
$install_lang["company_top_menu_company-main-my-account-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-my-agents-item"] = "Mis agentes";
$install_lang["company_top_menu_company-main-my-agents-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-my-profile-item"] = "Mi perfil";
$install_lang["company_top_menu_company-main-my-profile-item_tooltip"] = "";
$install_lang["guest_main_menu_main-menu-pro-item"] = "Profesionales";
$install_lang["guest_main_menu_main-menu-pro-item_main-menu-pro-agent-item"] = "Encontrar un agente";
$install_lang["guest_main_menu_main-menu-pro-item_main-menu-pro-agent-item_tooltip"] = "";
$install_lang["guest_main_menu_main-menu-pro-item_main-menu-pro-company-item"] = "Encontrar una agencia";
$install_lang["guest_main_menu_main-menu-pro-item_main-menu-pro-company-item_tooltip"] = "";
$install_lang["guest_main_menu_main-menu-pro-item_main-menu-pro-private-item"] = "Encontrar a una persona privada";
$install_lang["guest_main_menu_main-menu-pro-item_main-menu-pro-private-item_tooltip"] = "";
$install_lang["guest_main_menu_main-menu-pro-item_tooltip"] = "";
$install_lang["private_account_menu_private_my_account_item"] = "Mi cuenta";
$install_lang["private_account_menu_private_my_account_item_tooltip"] = "";
$install_lang["private_account_menu_private_my_profile_item"] = "Mi perfil";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_change_regdata_item"] = "Los datos de registro";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_change_regdata_item_tooltip"] = "";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_deactivate_item"] = "Desactivar perfil";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_deactivate_item_tooltip"] = "";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_edit_item"] = "Editar perfil";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_edit_item_tooltip"] = "";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_services_item"] = "Servicios pagados";
$install_lang["private_account_menu_private_my_profile_item_private_my_profile_services_item_tooltip"] = "";
$install_lang["private_account_menu_private_my_profile_item_tooltip"] = "";
$install_lang["private_account_menu_private_overview_item"] = "Visión general";
$install_lang["private_account_menu_private_overview_item_tooltip"] = "";
$install_lang["private_account_menu_private_payments_item"] = "Pagos";
$install_lang["private_account_menu_private_payments_item_private_my_account_balance_item"] = "Balance de la cuenta";
$install_lang["private_account_menu_private_payments_item_private_my_account_balance_item_tooltip"] = "";
$install_lang["private_account_menu_private_payments_item_tooltip"] = "";
$install_lang["private_main_menu_private-main-menu-pro-item"] = "Profesionales";
$install_lang["private_main_menu_private-main-menu-pro-item_private-main-menu-pro-agent-item"] = "Encontrar un agente";
$install_lang["private_main_menu_private-main-menu-pro-item_private-main-menu-pro-agent-item_tooltip"] = "";
$install_lang["private_main_menu_private-main-menu-pro-item_private-main-menu-pro-company-item"] = "Encontrar una agencia";
$install_lang["private_main_menu_private-main-menu-pro-item_private-main-menu-pro-company-item_tooltip"] = "";
$install_lang["private_main_menu_private-main-menu-pro-item_private-main-menu-pro-private-item"] = "Encontrar a una persona privada";
$install_lang["private_main_menu_private-main-menu-pro-item_private-main-menu-pro-private-item_tooltip"] = "";
$install_lang["private_main_menu_private-main-menu-pro-item_tooltip"] = "";
$install_lang["private_top_menu_private-main-logout-item"] = "Salir";
$install_lang["private_top_menu_private-main-logout-item_tooltip"] = "";
$install_lang["private_top_menu_private-main-my-account-item"] = "Opciones de cuenta";
$install_lang["private_top_menu_private-main-my-account-item_tooltip"] = "";
$install_lang["private_top_menu_private-main-my-profile-item"] = "Mi perfil";
$install_lang["private_top_menu_private-main-my-profile-item_tooltip"] = "";


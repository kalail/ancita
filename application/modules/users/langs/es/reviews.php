<?php

$install_lang["hands_dop1_header"] = "Conocimiento local";
$install_lang["hands_dop1_votes_1"] = "1.0";
$install_lang["hands_dop1_votes_5"] = "5.0";
$install_lang["hands_dop2_header"] = "Experiencia en procesos";
$install_lang["hands_dop2_votes_1"] = "1.0";
$install_lang["hands_dop2_votes_5"] = "5.0";
$install_lang["hands_main_header"] = "¿Cómo encontrar a los servicios de la profesional?";
$install_lang["hands_main_votes_1"] = "1.0";
$install_lang["hands_main_votes_5"] = "5.0";
$install_lang["stars_dop1_header"] = "Conocimiento local";
$install_lang["stars_dop1_votes_1"] = "1.0";
$install_lang["stars_dop1_votes_2"] = "2.0";
$install_lang["stars_dop1_votes_3"] = "3.0";
$install_lang["stars_dop1_votes_4"] = "4.0";
$install_lang["stars_dop1_votes_5"] = "5.0";
$install_lang["stars_dop2_header"] = "Experiencia en procesos";
$install_lang["stars_dop2_votes_1"] = "1.0";
$install_lang["stars_dop2_votes_2"] = "2.0";
$install_lang["stars_dop2_votes_3"] = "3.0";
$install_lang["stars_dop2_votes_4"] = "4.0";
$install_lang["stars_dop2_votes_5"] = "5.0";
$install_lang["stars_main_header"] = "¿Cómo encontrar a los servicios de la profesional?";
$install_lang["stars_main_votes_1"] = "1.0";
$install_lang["stars_main_votes_2"] = "2.0";
$install_lang["stars_main_votes_3"] = "3.0";
$install_lang["stars_main_votes_4"] = "4.0";
$install_lang["stars_main_votes_5"] = "5.0";
$install_lang["stat_header_reviews_users_object"] = "Usuarios";
$install_lang["stat_reviews_visitors_users_object"] = "Opiniones sobre mi perfil";
$install_lang["stat_reviews_visits_users_object"] = "Reviso los perfiles de usuario";


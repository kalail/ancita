<?php

$install_lang["block_featured_agents_block"] = "Agentes destacados";
$install_lang["block_featured_companies_block"] = "Empresas destacadas";
$install_lang["param_featured_agents_block_count"] = "Conteo";
$install_lang["param_featured_companies_block_count"] = "Conteo";
$install_lang["view_featured_agents_block_gallery_big"] = "Galería 200x200";
$install_lang["view_featured_agents_block_gallery_middle"] = "Galería 100x100";
$install_lang["view_featured_companies_block_gallery_big"] = "Galería 200x200";
$install_lang["view_featured_companies_block_gallery_middle"] = "Galería 100x100";


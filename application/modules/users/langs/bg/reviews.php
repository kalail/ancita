<?php

$install_lang["hands_dop1_header"] = "Познаване на местните особености";
$install_lang["hands_dop1_votes_1"] = "1.0";
$install_lang["hands_dop1_votes_5"] = "5.0";
$install_lang["hands_dop2_header"] = "Експертни знания";
$install_lang["hands_dop2_votes_1"] = "1.0";
$install_lang["hands_dop2_votes_5"] = "5.0";
$install_lang["hands_main_header"] = "Оценете услугите на професионалиста";
$install_lang["hands_main_votes_1"] = "1.0";
$install_lang["hands_main_votes_5"] = "5.0";
$install_lang["stars_dop1_header"] = "Познаване на местните особености";
$install_lang["stars_dop1_votes_1"] = "1.0";
$install_lang["stars_dop1_votes_2"] = "2.0";
$install_lang["stars_dop1_votes_3"] = "3.0";
$install_lang["stars_dop1_votes_4"] = "4.0";
$install_lang["stars_dop1_votes_5"] = "5.0";
$install_lang["stars_dop2_header"] = "Еспертни знания";
$install_lang["stars_dop2_votes_1"] = "1.0";
$install_lang["stars_dop2_votes_2"] = "2.0";
$install_lang["stars_dop2_votes_3"] = "3.0";
$install_lang["stars_dop2_votes_4"] = "4.0";
$install_lang["stars_dop2_votes_5"] = "5.0";
$install_lang["stars_main_header"] = "Оценете услугите професионалиста";
$install_lang["stars_main_votes_1"] = "1.0";
$install_lang["stars_main_votes_2"] = "2.0";
$install_lang["stars_main_votes_3"] = "3.0";
$install_lang["stars_main_votes_4"] = "4.0";
$install_lang["stars_main_votes_5"] = "5.0";
$install_lang["stat_header_reviews_users_object"] = "Потребители";
$install_lang["stat_reviews_visitors_users_object"] = "Отзиви за моя профил";
$install_lang["stat_reviews_visits_users_object"] = "Мои отзиви за чужди профили";


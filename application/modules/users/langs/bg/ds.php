<?php

$install_lang["user_type"]["header"] = "Тип потребител";
$install_lang["user_type"]["option"]["private"] = "Частно лице";
$install_lang["user_type"]["option"]["company"] = "Агенция";
$install_lang["user_type"]["option"]["agent"] = "Брокер";

$install_lang["professionals"]["header"] = "Професионалисти";
$install_lang["professionals"]["option"]["company"] = "Агенция";
$install_lang["professionals"]["option"]["agent"] = "Брокер";
$install_lang["professionals"]["option"]["private"] = "Частно лице";

$install_lang["deactivated_reason_object"]["header"] = "Причина за деактивиране на профила";
$install_lang["deactivated_reason_object"]["option"]["1"] = "Имам опасения за поверителност";
$install_lang["deactivated_reason_object"]["option"]["2"] = "Не намирам, че е полезно";
$install_lang["deactivated_reason_object"]["option"]["3"] = "Не разбирам как да го използвам";
$install_lang["deactivated_reason_object"]["option"]["4"] = "Това е временно, ще се върна";
$install_lang["deactivated_reason_object"]["option"]["5"] = "Друга";



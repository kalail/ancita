<?php

$install_lang["notification_agent_cancel_request"] = "Фирмено уведомление - потребителят не иска да стане техен брокер";
$install_lang["notification_agent_deleted"] = "Уведомление до потребителя - фирмата го е изключила от списъка на техните брокери";
$install_lang["notification_agent_request_accepted"] = "Уведомление до потребителя - фирмата го е включила в списъка на техните брокери";
$install_lang["notification_agent_request_declined"] = "Уведомление до потребителя - фирмата отказва да бъде техен брокер";
$install_lang["notification_agent_send_request"] = "Фирмено уведомление - потребителят иска да стане техен брокер";
$install_lang["notification_agent_unjoin"] = "Фирмено уведомление - потребителят се е отписал от листата на техните брокери";
$install_lang["notification_ausers_deactivated"] = "Деактивиране на профила";
$install_lang["notification_user_account_create_by_admin"] = "Профилът е създаден от администратора";
$install_lang["notification_users_account_create"] = "Профилът е създаден";
$install_lang["notification_users_change_email"] = "Email е променен";
$install_lang["notification_users_change_password"] = "Паролата е изменена";
$install_lang["notification_users_deactivated"] = "Профилът е деактивиран";
$install_lang["notification_users_email_restore"] = "Email е променен (уведомление до стария email)";
$install_lang["notification_users_fogot_password"] = "Възстановяване на парола";
$install_lang["notification_users_registration"] = "Регистрация на потребител";
$install_lang["tpl_agent_cancel_request_content"] = "Здравейте, [fname]!\n\n[user_name] е отписал(а) своята заявка за брокер във Вашата агенция.\n\nС уважение,\n[name_from]";
$install_lang["tpl_agent_cancel_request_subject"] = "[domain] | Брокерът се е отписал от брокерите на вашата фирма";
$install_lang["tpl_agent_deleted_content"] = "Здравейте, [fname]!\n\nАгенция [company_name] Ви е премахнала от списъка на техните брокери.\n\nС уважение,\n[name_from]";
$install_lang["tpl_agent_deleted_subject"] = "[domain] | Вие сте премахнат от списъка на брокерите";
$install_lang["tpl_agent_request_accepted_content"] = "Здравейте, [fname]!\n\nАгенция[company_name] приема Вашата заявка за брокер. Заповядайте в офиса на фирмата за сключване на споразумение.\n\nС уважение,\n[name_from]";
$install_lang["tpl_agent_request_accepted_subject"] = "[domain] | Фирмата приема вашата заявка за брокер";
$install_lang["tpl_agent_request_declined_content"] = "Здравейте, [fname]!\n\nАгенция [company_name] отхвърля Вашата заявка за регистриране като брокер на фирмата.\n\nС уважение,\n[name_from]";
$install_lang["tpl_agent_request_declined_subject"] = "[domain] | Отхвърля Вашата заявка за брокер";
$install_lang["tpl_agent_send_request_content"] = "Здравейте, [fname]!\n\n[user_name] желае да бъде брокер във Вашата агенция.\nМожете да приемете или да отхвърлите заявката в раздела заявки от брокери.\n\nС уважение,\n[name_from]";
$install_lang["tpl_agent_send_request_subject"] = "[domain] | Нова заявка от брокер";
$install_lang["tpl_agent_unjoin_content"] = "Здравейте, [fname]!\n\n[user_name] се е изтрил(а) от списъка на брокерите на Вашата агенция.\n\nС уважение,\n[name_from]";
$install_lang["tpl_agent_unjoin_subject"] = "[domain] |Брокер се отписъл от вашите брокери";
$install_lang["tpl_ausers_deactivated_content"] = "Здравейте, администратор!\n\nЕдин от потребителите е деактивирал своя профил в сайта [domain]. За преглед влезте в панела на администратора > Обратна връзка > Деактивация на профил.\n\nС уважение,\n[name_from]";
$install_lang["tpl_ausers_deactivated_subject"] = "[domain] | Деактивация на профил";
$install_lang["tpl_user_account_create_by_admin_content"] = "Здравейте, [fname]!\n\nДобре дошли в [domain]!\n\nЗа вас е създаден профил в сайта [domain].\nДанни за вход: [email]\nПарола: [password]\n\nС уважение,\n[name_from]";
$install_lang["tpl_user_account_create_by_admin_subject"] = "Добре дошли на [domain]";
$install_lang["tpl_users_account_create_content"] = "Здравейте, [fname]!\n\nВие сте регистриран в сайта [domain]. Данни за вход:\n__________________________\n\nEmail: [email]\nПарола: [password]\n__________________________\n\nС уважение,\n[name_from]";
$install_lang["tpl_users_account_create_subject"] = "[domain] | Създадена регистрация";
$install_lang["tpl_users_change_email_content"] = "Здравейте, [fname]!\n\nДанните за вход на сайта [domain]:\n__________________________\n\nEmail: [email]\n__________________________\n\nС уважение,\n[name_from]";
$install_lang["tpl_users_change_email_subject"] = "[domain] | Email променен";
$install_lang["tpl_users_change_password_content"] = "Здравейте, [fname]!\n\nНовата парола за вход на сайт [domain]:\n__________________________\n\n[password]\n__________________________\n\nС уважение,\n[name_from]";
$install_lang["tpl_users_change_password_subject"] = "[domain] | Парола променен";
$install_lang["tpl_users_deactivated_content"] = "Здравейте, [fname]!\n\nВашият профил на сайта [domain] е деактивиран.\n\nС уважение,\n[name_from]";
$install_lang["tpl_users_deactivated_subject"] = "[domain] |Деактивиране профил";
$install_lang["tpl_users_email_restore_content"] = "Здравейте, [fname]!\n\nПроменени са данните за вход в сайта.\n\nНовият адрес на електронната поща: [email]\n\nАко Вие не сте поискали тази промяна и желаете да си възстановите стария адрес, използвайте връзката [restore_link].\n\nС уважение,\n[name_from]";
$install_lang["tpl_users_email_restore_subject"] = "[domain] | Email  ви е променен";
$install_lang["tpl_users_fogot_password_content"] = "Здравейте, [fname]!\n\nВашата нова парола за сайта [domain]:\n__________________________\n\n[password]\n__________________________\n\nС уважение,\n[name_from]";
$install_lang["tpl_users_fogot_password_subject"] = "[domain] | Нова парола";
$install_lang["tpl_users_registration_content"] = "Здравейте, [fname]!\n\nВашите данни за вход в сайта [domain]:\n__________________________\n\nEmail: [email]\nПарола: [password]\n[confirm_block]\n__________________________\n\nС уважение,\n[name_from]";
$install_lang["tpl_users_registration_subject"] = "[domain] | Регистрация";


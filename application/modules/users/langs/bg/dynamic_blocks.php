<?php

$install_lang["block_featured_agents_block"] = "Специални брокери";
$install_lang["block_featured_companies_block"] = "Специални агенции";
$install_lang["param_featured_agents_block_count"] = "Количество";
$install_lang["param_featured_companies_block_count"] = "Количество";
$install_lang["view_featured_agents_block_gallery_big"] = "Галерия 200x200";
$install_lang["view_featured_agents_block_gallery_middle"] = "Галерия 100x100";
$install_lang["view_featured_companies_block_gallery_big"] = "Галерия 200x200";
$install_lang["view_featured_companies_block_gallery_middle"] = "Галерия 100x100";


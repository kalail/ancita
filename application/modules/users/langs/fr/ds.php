<?php

$install_lang["user_type"]["header"] = "Type d'utilisateur";
$install_lang["user_type"]["option"]["private"] = "Personne privée";
$install_lang["user_type"]["option"]["company"] = "Agence";
$install_lang["user_type"]["option"]["agent"] = "Agent";

$install_lang["professionals"]["header"] = "Professionel";
$install_lang["professionals"]["option"]["company"] = "Agence";
$install_lang["professionals"]["option"]["agent"] = "Agent";
$install_lang["professionals"]["option"]["private"] = "Personne privée";

$install_lang["deactivated_reason_object"]["header"] = "Raison déactivée";
$install_lang["deactivated_reason_object"]["option"]["1"] = "J'ai des préoccupations d'intimité";
$install_lang["deactivated_reason_object"]["option"]["2"] = "Je le trouve inutile";
$install_lang["deactivated_reason_object"]["option"]["3"] = "Je ne comprend pas comment l'utiliser";
$install_lang["deactivated_reason_object"]["option"]["4"] = "C'est temporaire, je reviens";
$install_lang["deactivated_reason_object"]["option"]["5"] = "Autre";



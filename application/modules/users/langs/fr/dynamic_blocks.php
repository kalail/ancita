<?php

$install_lang["block_featured_agents_block"] = "Agents en vedette";
$install_lang["block_featured_companies_block"] = "Agences en vedette";
$install_lang["param_featured_agents_block_count"] = "Compte";
$install_lang["param_featured_companies_block_count"] = "Compte";
$install_lang["view_featured_agents_block_gallery_big"] = "Galerie 200x200";
$install_lang["view_featured_agents_block_gallery_middle"] = "Galerie 100x100";
$install_lang["view_featured_companies_block_gallery_big"] = "Galerie 200x200";
$install_lang["view_featured_companies_block_gallery_middle"] = "Galerie 100x100";


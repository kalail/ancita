<?php

$install_lang["notification_agent_cancel_request"] = "Notification d'entreprise - membre a cancelé sa demande pour devenir agent de cette compagnie";
$install_lang["notification_agent_deleted"] = "Notification de membre - compagnie l'a supprimé de leur liste d'agents";
$install_lang["notification_agent_request_accepted"] = "Notification de membre - compagnie a accepté sa demande pour devenir agent";
$install_lang["notification_agent_request_declined"] = "Notification de membre - compagnie a rejeté sa demande pour devenir agent";
$install_lang["notification_agent_send_request"] = "Notification d'entreprise - Un membre veut devenir leur agent";
$install_lang["notification_agent_unjoin"] = "Notification d'entreprise - membre s'est supprimé de leur liste d'agents";
$install_lang["notification_ausers_deactivated"] = "Deactiver profil";
$install_lang["notification_user_account_create_by_admin"] = "Compte crée par admin";
$install_lang["notification_users_account_create"] = "Compte crée";
$install_lang["notification_users_change_email"] = "Courriel changé";
$install_lang["notification_users_change_password"] = "Mot de passe changé";
$install_lang["notification_users_deactivated"] = "Compte déactivé";
$install_lang["notification_users_email_restore"] = "Courriel changé (notification à ancien courriel)";
$install_lang["notification_users_fogot_password"] = "Retrouver mon mot de passe";
$install_lang["notification_users_registration"] = "Enregistrement d'utilisateur";
$install_lang["tpl_agent_cancel_request_content"] = "Bonjour [fname],\n\n[user_name] s'est supprimé de la liste de demandes de vos agents.\n\nCordialement,\n[name_from]";
$install_lang["tpl_agent_cancel_request_subject"] = "[domain] | Agent supprimé de la liste d'agents de compagnie";
$install_lang["tpl_agent_deleted_content"] = "Bonjour [fname],\n\nL'entreprise [company_name] vous a supprimé de leur liste d'agents.\n\nCordialement,\n[name_from]";
$install_lang["tpl_agent_deleted_subject"] = "[domain] | Vous êtes supprimés de la liste d'agents";
$install_lang["tpl_agent_request_accepted_content"] = "Bonjour [fname],\n\nL'entreprise [company_name] a accepté votre demande et vous êtes agent de compagnie maintenant.\n\nCordialement,\n[name_from]";
$install_lang["tpl_agent_request_accepted_subject"] = "[domain] | Compagnie a accepté votre demande";
$install_lang["tpl_agent_request_declined_content"] = "Bonjour [fname],\n\nL'entreprise [company_name] a refusée votre demande d'agent.\n\nCordialement,\n[name_from]";
$install_lang["tpl_agent_request_declined_subject"] = "[domain] | Compagnie a refusé votre demande";
$install_lang["tpl_agent_send_request_content"] = "Bonjour [fname],\n\n[user_name] veut devenir votre agent de compagnie. Approuvez ou refusez leur demande dans la section de demandes d'agents.\n\nCordialement,\n[name_from]";
$install_lang["tpl_agent_send_request_subject"] = "[domain] | Nouvelle demande d'agent";
$install_lang["tpl_agent_unjoin_content"] = "Bonjour [fname],\n\n[user_name] s'est supprimé de la liste de vos agents.\n\nCordialement,\n[name_from]";
$install_lang["tpl_agent_unjoin_subject"] = "[domain] | Agent supprimé de liste d'agents";
$install_lang["tpl_ausers_deactivated_content"] = "Bonjour admin,\n\nIl y a une nouvelle désactivation de profil sur [domain]. Accédez au panneau admin > Commentaires > Profils déactivés pour le voir.\n\nCordialement,\n[name_from]";
$install_lang["tpl_ausers_deactivated_subject"] = "[domain] | Profile déactivé";
$install_lang["tpl_user_account_create_by_admin_content"] = "Bonjour [fname],\n\Bienvenu [domain]!\n\nUn compte a été crée sur [domain].\nCourriel: [email]\nMot de passe: [password]\n\nCordialement,\n[name_from]";
$install_lang["tpl_user_account_create_by_admin_subject"] = "Bienvenu à [domain]";
$install_lang["tpl_users_account_create_content"] = "Bonjour [fname],\n\nVos données de compte sur [domain]:\n__________________________\n\nCourriel: [email]\nMot de passe: [password]\n__________________________\n\nCordialement,\n[name_from]";
$install_lang["tpl_users_account_create_subject"] = "[domain] | Compte crée";
$install_lang["tpl_users_change_email_content"] = "Bonjour [fname],\n\nVos donées de compte sur [domain]:\n__________________________\n\nCourriel: [email]\n__________________________\n\nCordialement,\n[name_from]";
$install_lang["tpl_users_change_email_subject"] = "[domain] | Courriel changé";
$install_lang["tpl_users_change_password_content"] = "Bonjour [fname],\n\nVos donées de compte sur [domain]:\n__________________________\n\n[password]\n__________________________\n\nCordialement,\n[name_from]";
$install_lang["tpl_users_change_password_subject"] = "[domain] | Mot de passe changé";
$install_lang["tpl_users_deactivated_content"] = "Bonjour [fname],\n\nVotre profil sur [domain] est désactivé.\n\nCordialement,\n[name_from]";
$install_lang["tpl_users_deactivated_subject"] = "[domain] | Compte déactivé";
$install_lang["tpl_users_email_restore_content"] = "Bonjour [fname],\n\nVos paramètres de compte ont changé.\n\nNouveau courriel: [email]\n\nSi cette lettre est une surprise pour vous et vous voulez changer votre courriel/données SVP visitez [restore_link].\n\nCordialement,\n[name_from]";
$install_lang["tpl_users_email_restore_subject"] = "[domain] | Courriel changé";
$install_lang["tpl_users_fogot_password_content"] = "Bonjour [fname],\n\nVotre nouveau mot de passe sur [domain]:\n__________________________\n\n[password]\n__________________________\n\nCordialement,\n[name_from]";
$install_lang["tpl_users_fogot_password_subject"] = "[domain] | Nouveau mot de passe";
$install_lang["tpl_users_registration_content"] = "Bonjour [fname],\n\nVotre paramètres de compte sur [domain]:\n__________________________\n\nCourriel: [email]\nMot de passe: [password]\n[confirm_block]\n__________________________\n\nCordialement,\n[name_from]";
$install_lang["tpl_users_registration_subject"] = "[domain] | Enregistrement";


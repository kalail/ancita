<?php

$install_lang["hands_dop1_header"] = "Conaissances locales";
$install_lang["hands_dop1_votes_1"] = "1.0";
$install_lang["hands_dop1_votes_5"] = "5.0";
$install_lang["hands_dop2_header"] = "Expertise de processus";
$install_lang["hands_dop2_votes_1"] = "1.0";
$install_lang["hands_dop2_votes_5"] = "5.0";
$install_lang["hands_main_header"] = "Que pensez vous des services de cet agent?";
$install_lang["hands_main_votes_1"] = "1.0";
$install_lang["hands_main_votes_5"] = "5.0";
$install_lang["stars_dop1_header"] = "Conaissances locales";
$install_lang["stars_dop1_votes_1"] = "1.0";
$install_lang["stars_dop1_votes_2"] = "2.0";
$install_lang["stars_dop1_votes_3"] = "3.0";
$install_lang["stars_dop1_votes_4"] = "4.0";
$install_lang["stars_dop1_votes_5"] = "5.0";
$install_lang["stars_dop2_header"] = "Expertise de processus";
$install_lang["stars_dop2_votes_1"] = "1.0";
$install_lang["stars_dop2_votes_2"] = "2.0";
$install_lang["stars_dop2_votes_3"] = "3.0";
$install_lang["stars_dop2_votes_4"] = "4.0";
$install_lang["stars_dop2_votes_5"] = "5.0";
$install_lang["stars_main_header"] = "Que pensez vous des services de cet agent?";
$install_lang["stars_main_votes_1"] = "1.0";
$install_lang["stars_main_votes_2"] = "2.0";
$install_lang["stars_main_votes_3"] = "3.0";
$install_lang["stars_main_votes_4"] = "4.0";
$install_lang["stars_main_votes_5"] = "5.0";
$install_lang["stat_header_reviews_users_object"] = "Utilisateurs";
$install_lang["stat_reviews_visitors_users_object"] = "Commentaires sur mon profil";
$install_lang["stat_reviews_visits_users_object"] = "Mes commentaires sur autres profils";


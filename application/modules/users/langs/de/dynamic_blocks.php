<?php

$install_lang["block_featured_agents_block"] = "Ausgewählte Makler";
$install_lang["block_featured_companies_block"] = "Ausgewählte Unternehmen";
$install_lang["param_featured_agents_block_count"] = "zählen";
$install_lang["param_featured_companies_block_count"] = "zählen";
$install_lang["view_featured_agents_block_gallery_big"] = "Gallerie 200x200";
$install_lang["view_featured_agents_block_gallery_middle"] = "Gallerie 100x100";
$install_lang["view_featured_companies_block_gallery_big"] = "Gallerie 200x200";
$install_lang["view_featured_companies_block_gallery_middle"] = "Gallerie 100x100";


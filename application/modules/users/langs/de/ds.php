<?php

$install_lang["user_type"]["header"] = "Benutzertyp";
$install_lang["user_type"]["option"]["private"] = "Privatpersonen";
$install_lang["user_type"]["option"]["company"] = "Agentur";
$install_lang["user_type"]["option"]["agent"] = "Makler";

$install_lang["professionals"]["header"] = "Experte";
$install_lang["professionals"]["option"]["company"] = "Agentur";
$install_lang["professionals"]["option"]["agent"] = "Makler";
$install_lang["professionals"]["option"]["private"] = "Privatpersonen";

$install_lang["deactivated_reason_object"]["header"] = "Deaktivierungs Grund";
$install_lang["deactivated_reason_object"]["option"]["1"] = "Ich habe private Bedenken";
$install_lang["deactivated_reason_object"]["option"]["2"] = "Ich finde es nicht nützlich";
$install_lang["deactivated_reason_object"]["option"]["3"] = "Ich verstehen nicht, wie man es benutzt";
$install_lang["deactivated_reason_object"]["option"]["4"] = "Es ist nur vorübergehend, ich werde wiederkommen";
$install_lang["deactivated_reason_object"]["option"]["5"] = "andere";



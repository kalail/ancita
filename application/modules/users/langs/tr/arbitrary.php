<?php

$install_lang["groups_demo_default"] = "Varsayılan";
$install_lang["groups_demo_simple-users"] = "Basit kullanıcılar";
$install_lang["groups_demo_vip-users"] = "VIP kullanıcılar";

$install_lang["seo_tags_account_description"] = "My account. Add funds to virtual account.";
$install_lang["seo_tags_account_header"] = "Hesabım";
$install_lang["seo_tags_account_keyword"] = "my account, add funds";
$install_lang["seo_tags_account_og_description"] = "My account. Add funds to virtual account.";
$install_lang["seo_tags_account_og_title"] = "Hesabım";
$install_lang["seo_tags_account_og_type"] = "article";
$install_lang["seo_tags_account_title"] = "Pilot Group : Kullanıcı hesabı: [unique_name|Demo kullanıcı]";
$install_lang["seo_tags_index_description"] = "Catalogue of real estate agents, agencies, brokers, property owners. Information about their experience, listings, location. Location on map.";
$install_lang["seo_tags_index_header"] = "Profil : [user_type_str|All]";
$install_lang["seo_tags_index_keyword"] = "real estate agent, real estate agency, real estate broker, property owner, location on map, reviews, experience, listings";
$install_lang["seo_tags_index_og_description"] = "Catalogue of real estate agents, agencies, brokers, property owners. Information about their experience, listings, location. Location on map.";
$install_lang["seo_tags_index_og_title"] = "Catalogue of agencies : [user_type_str|All]";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Pilot Group : Profil kataloğu: [user_type_str|All]";
$install_lang["seo_tags_login_form_description"] = "Login form.";
$install_lang["seo_tags_login_form_header"] = "Giriş yap";
$install_lang["seo_tags_login_form_keyword"] = "login form, user login";
$install_lang["seo_tags_login_form_og_description"] = "Giriş formu.";
$install_lang["seo_tags_login_form_og_title"] = "Giriş yap";
$install_lang["seo_tags_login_form_og_type"] = "profile";
$install_lang["seo_tags_login_form_title"] = "Pilot Group : Giriş formu";
$install_lang["seo_tags_profile_description"] = "User profile. Edit profile data.";
$install_lang["seo_tags_profile_header"] = "Profil";
$install_lang["seo_tags_profile_keyword"] = "user profile, edit profile data";
$install_lang["seo_tags_profile_og_description"] = "User profile. Edit profile data.";
$install_lang["seo_tags_profile_og_title"] = "Profile of [fname|Demo] [sname|user]";
$install_lang["seo_tags_profile_og_type"] = "profile";
$install_lang["seo_tags_profile_title"] = "Pilot Group : Profil : [fname|Demo] [sname|user]";
$install_lang["seo_tags_register_agent_description"] = "Registration of a real estate agent.";
$install_lang["seo_tags_register_agent_header"] = "Acente kaydı";
$install_lang["seo_tags_register_agent_keyword"] = "agent registration";
$install_lang["seo_tags_register_agent_og_description"] = "Registration of a real estate agent.";
$install_lang["seo_tags_register_agent_og_title"] = "Acente kaydı";
$install_lang["seo_tags_register_agent_og_type"] = "profile";
$install_lang["seo_tags_register_agent_title"] = "Pilot Group : Acente kaydı";
$install_lang["seo_tags_register_company_description"] = "Registration of a real estate agency.";
$install_lang["seo_tags_register_company_header"] = "Şirket kaydı";
$install_lang["seo_tags_register_company_keyword"] = "agency registration";
$install_lang["seo_tags_register_company_og_description"] = "Registration of a real estate agency.";
$install_lang["seo_tags_register_company_og_title"] = "Şirket kaydı";
$install_lang["seo_tags_register_company_og_type"] = "profile";
$install_lang["seo_tags_register_company_title"] = "Pilot Group : Şirket kaydı";
$install_lang["seo_tags_register_private_description"] = "Registration of a property owner.";
$install_lang["seo_tags_register_private_header"] = "Özel kişi kaydı";
$install_lang["seo_tags_register_private_keyword"] = "property owner registration";
$install_lang["seo_tags_register_private_og_description"] = "Registration of a property owner.";
$install_lang["seo_tags_register_private_og_title"] = "Özel kişi kaydı";
$install_lang["seo_tags_register_private_og_type"] = "profile";
$install_lang["seo_tags_register_private_title"] = "Pilot Group : Özel kişi kaydı";
$install_lang["seo_tags_restore_description"] = "You can restore password to your profile here if you lost or forgot it.";
$install_lang["seo_tags_restore_header"] = "Şifre kurtarma";
$install_lang["seo_tags_restore_keyword"] = "restore password";
$install_lang["seo_tags_restore_og_description"] = "You can restore password to your profile here if you lost or forgot it.";
$install_lang["seo_tags_restore_og_title"] = "Şifre kurtarma";
$install_lang["seo_tags_restore_og_type"] = "profile";
$install_lang["seo_tags_restore_title"] = "Pilot Group : Şifre kurtarma";
$install_lang["seo_tags_search_description"] = "Find a real estate agent with the help of the search form. Learn more about agent rating, experience, location, listings. Filter agents by location.";
$install_lang["seo_tags_search_header"] = "Profil : [user_type_str|All]";
$install_lang["seo_tags_search_keyword"] = "real estate agent, find an agent, real estate professional, reviews about an agent";
$install_lang["seo_tags_search_og_description"] = "Find a real estate agent with the help of the search form. Learn more about agent rating, experience, location, listings. Filter agents by location.";
$install_lang["seo_tags_search_og_title"] = "Arama dizesiyle acentleri bul";
$install_lang["seo_tags_search_og_type"] = "profile";
$install_lang["seo_tags_search_title"] = "Pilot Group : Profil arama : [user_type_str|All]";
$install_lang["seo_tags_view_description"] = "View information about real estate agent or property owner and their listings. Contact, write a review, find location on map.";
$install_lang["seo_tags_view_header"] = "[name|Demo], [user_type_str]";
$install_lang["seo_tags_view_keyword"] = "real estate agent, agent contact information, agent listings, agent reviews, agent location on map";
$install_lang["seo_tags_view_og_description"] = "View information about real estate agent or property owner and their listings. Contact, write a review, find location on map.";
$install_lang["seo_tags_view_og_title"] = "Profil görüntüle [name|Demo], [user_type_str]";
$install_lang["seo_tags_view_og_type"] = "profile";
$install_lang["seo_tags_view_title"] = "Pilot Group : Profil görüntüle : [name|Demo], [user_type_str]";


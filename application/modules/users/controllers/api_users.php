<?php

/**
 * Users api side controller
 *
 * @package PG_RealEstate
 * @subpackage Users
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Mikhail Chernov <mchernov@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: mchernov $
 **/
class Api_Users extends Controller{
	
	/**
	 * Use email confirmation
	 * 
	 * @var boolean
	 */
	public $use_email_confirmation = true;
	
	/**
	 * Constructor
	 * 
	 * @return Api_Users
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model("Users_model");
	}
	
	/**
	 * Confirm account by email code
	 * 
	 * @return void
	 */
	public function confirm(){
		$code = trim(strip_tags($this->input->get_post("code", true)));
		$this->Users_model->set_format_settings('get_safe', false);
		$user = $this->Users_model->get_user_by_confirm_code($code);
		$this->Users_model->set_format_settings('get_safe', true);
		if(empty($user)){
			$this->set_api_content("errors", l("error_user_no_exists", "users"));
			return;
		}elseif($user["confirm"]){
			$this->set_api_content("errors", l("error_user_already_confirm", "users"));
			return;
		}else{
			$data["confirm"] = 1;
			$this->Users_model->save_user($user["id"], $data);
			$this->set_api_content("messages", l("success_confirm", "users"));

			$this->load->model("users/models/Auth_model");
			$auth_data = $this->Auth_model->login($user["id"]);
			if(!empty($auth_data["errors"])){
				$this->set_api_content("errors", $auth_data["errors"]);
				return;
			}else{
				$token = $this->session->sess_create_token();
				$this->set_api_content("data", array("token" => $token));
			}
		}
	}
	
	/**
	 * Restore user password
	 * 
	 * @return void
	 */
	public function restore(){
		$email = strip_tags($this->input->get_post("email", true));
		$this->Users_model->set_format_settings('get_safe', false);
		$user_data = $this->Users_model->get_user_by_email($email);
		$this->Users_model->set_format_settings('get_safe', true);
		$error = "";
		if(empty($user_data) || !$user_data["id"]){
			$this->set_api_content("errors", l("error_user_no_exists", "users"));
			return;
		}elseif(!$user_data["confirm"]){
			$this->set_api_content("errors", l("error_unconfirmed_user", "users"));
			return;
		}elseif(!$user_data["status"]){
			$this->set_api_content("errors", l("error_user_is_blocked", "users"));
			return;
		}else{
			$user_id = $user_data["id"];
			$save_data["password"] = $save_data["repassword"] = substr(md5(date("Y-m-d H:i:s") . $user_data["email"]), 0, 6);
			$validate_data = $this->Users_model->validate_user($user_data["id"], $save_data);
			if(!empty($validate_data['errors'])){
				$this->set_api_content("messages", implode('<br>', $validate_data['errors']));
			}else{
				$this->Users_model->save_user($user_data["id"], $validate_data['data']);

				$this->load->model("Notifications_model");
				$return = $this->Notifications_model->send_notification($user_data["email"], "users_fogot_password", $save_data, '', $user_data["lang_id"]);
				$this->set_api_content("messages", l("success_restore_mail_sent", "users"));
			}
		}
	}
	
	/**
	 * Change user password
	 * 
	 * @return void
	 */
	public function save_password(){
		$user_id = $this->session->userdata("user_id");
		
		$this->Users_model->set_format_settings('get_safe', false);
		$user = $this->Users_model->get_user_by_id($user_id);
		$this->Users_model->set_format_settings('get_safe', true);
		
		$this->load->model("users/models/Auth_model");
		$this->Auth_model->update_user_session_data($user_id);
		
		$post_data = array(
			"password" => $this->input->get_post("password", true),
			"repassword" => $this->input->get_post("password", true),
		);
		$validate_data = $this->Users_model->validate_user($user_id, $post_data);

		$password_old = $this->input->post('password_old', true);
		$password_old = $this->Users_model->encode_password($password_old);
		if($password_old != $user['password']){
			$validate_data["errors"][] = l('error_password_old_invalid', 'users');
		}

		if(!empty($validate_data["errors"])){
			$this->set_api_content("errors", $validate_data["errors"]);
		}else{
			$save_data = $validate_data["data"];
			$user_id = $this->Users_model->save_user($user_id, $save_data);

			///// send notification
			$this->load->model("Notifications_model");
			$this->Users_model->set_format_settings('get_safe', false);
			$user_data = $this->Users_model->get_user_by_id($user_id);
			$this->Users_model->set_format_settings('get_safe', true);
			$user_data["password"] = $post_data['password'];
			$return = $this->Notifications_model->send_notification($user_data["email"], "users_change_password", $user_data, '', $user_data["lang_id"]);

			$this->set_api_content("messages", l("success_user_updated", "users"));
		}
	}
	
	/**
	 * Change registered email
	 * 
	 * @return void
	 */
	public function save_email(){
		$user_id = $this->session->userdata("user_id");
		
		$post_data = array(
			"email" => $this->input->get_post("email", true),
		);
		
		$validate_data = $this->Users_model->validate_user($user_id, $post_data);
		
		$this->Users_model->set_format_settings('get_safe', false);
		$user_data = $this->Users_model->get_user_by_id($user_id);
		$this->Users_model->set_format_settings('get_safe', true);
	
		if(!empty($validate_data['errors'])){
			$this->set_api_content('errors', $validate_data['errors']);
		}elseif($user_data['email'] == $validate_data['data']['email']){
			$this->set_api_content('errors', l('error_emails_are_equal', 'users'));
		}else{
			$validate_data['data']['email_restore'] = $validate_data['data']['email'];
			$validate_data['data']['email_restore_code'] = substr($this->Users_model->encode_confirm(date("Y-m-d H:i:s") . $validate_data['data']['email']), 0, 10);
			
			$this->Users_model->save_user($user_id, $validate_data['data']);
			$user_data['new_email'] = $validate_data['data']['email'];
			
			///// send notification
			$this->load->model("Notifications_model");
			$this->Notifications_model->send_notification($user_data["email"], "users_change_email", $user_data, '', $user_data["lang_id"]);
			$this->Notifications_model->send_notification($user_data["new_email"], "users_change_email", $user_data, '', $user_data["lang_id"]);

			$this->load->model("users/models/Auth_model");
			$this->Auth_model->update_user_session_data($user_id);

			$this->set_api_content("messages", l("success_user_updated", "users"));
		}
	}
	
	/**
	 * Ghange registered phone
	 * 
	 * @return void
	 */
	public function save_phone(){
		$user_id = $this->session->userdata("user_id");
		
		$post_data = array(
			'phone' => $this->input->get_post("phone", true),
		);
		
		$validate_data = $this->Users_model->validate_user($user_id, $post_data);
		
		$this->Users_model->set_format_settings('get_safe', false);
		$user_data = $this->Users_model->get_user_by_id($user_id);
		$this->Users_model->set_format_settings('get_safe', true);
	
		if(!empty($validate_data['errors'])){
			$this->set_api_content('errors', $validate_data['errors']);
		}elseif($user_data['phone'] == $validate_data['data']['phone']){
			$this->set_api_content('errors', l('error_phones_are_equal', 'users'));
		}else{
			$validate_data['data']['phone_restore'] = $validate_data['data']['phone'];
			$validate_data['data']['phone_restore_code'] = substr($this->Users_model->encode_confirm(date("Y-m-d H:i:s") . $validate_data['data']['phone']), 0, 10);
			
			$this->Users_model->save_user($user_id, $validate_data['data']);
			$user_data['new_phone'] = $validate_data['data']['phone'];
			
			///// send notification
			$this->load->model("Notifications_model");
			$this->Notifications_model->send_notification($user_data["email"], "users_change_phone", $user_data, '', $user_data["lang_id"]);

			$this->load->model("users/models/Auth_model");
			$this->Auth_model->update_user_session_data($user_id);

			$this->set_api_content("messages", l("success_user_updated", "users"));
		}
	}
	
	/**
	 * Get data of my profile
	 * 
	 * @return void
	 */
	public function profile(){
		$user_id = $this->session->userdata("user_id");
		
		$this->Users_model->set_format_settings(array('get_safe'=>false, 'get_company'=>true));
		$data = $this->Users_model->get_user_by_id($user_id);
		$this->Users_model->set_format_settings(array('get_safe'=>true, 'get_company'=>false));

		if(!$data){
			$this->set_api_content('error', l('api_error_not_access', 'users'));
			return;
		}

		$date_formats = $this->pg_date->get_format('date_literal', 'st');
		
		$this->load->helper('date_format');
		
		$data['date_created_output'] = tpl_date_format($data['date_created'], $date_formats);
		$data['agent_date_output'] = tpl_date_format($data['agent_date'], $date_formats);
		
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Change data of user profile
	 * 
	 * @return void
	 */
	public function save(){
		$user_id = $this->session->userdata("user_id");
		
		$this->Users_model->set_format_settings(array('get_safe'=>false, 'get_company'=>true));
		$data = $this->Users_model->get_user_by_id($user_id);
		$this->Users_model->set_format_settings(array('get_safe'=>true, 'get_company'=>false));

		if(!$data){
			$this->set_api_content('error', l('api_error_not_access', 'users'));
			return;
		}
	
		$post_data = $this->input->get_post("data", true);
		
		$action = $this->input->get_post("action", true);
	
		$validate_data = $this->Users_model->validate_user($user_id, $post_data, "user_icon");
		if(isset($_FILES["user_icon"]) && is_array($_FILES["user_icon"]) && is_uploaded_file($_FILES["user_icon"]["tmp_name"])){
			$this->load->model("Uploads_model");
			$validate_image = $this->Uploads_model->validate_upload($this->upload_config_id, "user_icon");
			$validate_date["errors"] = array_merge($validate_data["errors"], $validate_image["error"]);
		}
			
		if(!empty($validate_data["errors"])){
			$this->set_api_content("errors", $validate_data["errors"]);
		}else{
			switch($action){
				case 'subscriptions':
					if($this->pg_module->is_module_installed("subscriptions")){
						// Save user subscribers
						$user_subscriptions_list = $this->input->post("user_subscriptions_list", true);
						$this->load->model("subscriptions/models/Subscriptions_users_model");
						$this->Subscriptions_users_model->save_user_subscriptions($user_id, $user_subscriptions_list);
					}
					$this->set_api_content("messages", l("success_update_user", "users"));
				break;
				case 'company':
					$save_company_status = $this->Users_model->save_agent_company($user_id, $validate_data['data']["agent_company"]);
					if($save_company_status){
						$this->set_api_content("messages", l("success_update_agent_company", "users"));
					}else{
						$this->set_api_content("messages", l("success_update_user", "users"));
					}
				break;
				case 'location':
					$this->Users_model->save_user($user_id, $validate_data["data"], 'user_icon', true);
						
					$this->load->model('geomap/models/Geomap_model');
					$this->load->model('geomap/models/Geomap_settings_model');
					$map_gid = $this->Geomap_model->get_default_driver_gid();
					$post_settings = $this->input->post('map', true);
					$post_settings['lat'] = $validate_data['data']['lat'];
					$post_settings['lon'] = $validate_data['data']['lon'];
					$validate_data = $this->Geomap_settings_model->validate_settings($post_settings);
					if(!empty($validate_data["errors"])) break;
					$this->Geomap_settings_model->save_settings($map_gid, 0, $user_id, 'user_profile', $validate_data["data"]);
					
					$this->set_api_content("messages", l("success_update_user", "users"));
				break;
				default:				
					if($this->input->post("user_icon_delete")){
						$this->load->model("Uploads_model");
						if($data["user_logo_moderation"]){
							$this->Uploads_model->delete_upload($this->Users_model->upload_config_id, $user_id."/", $data["user_logo_moderation"]);
							$validate_data["data"]["user_logo_moderation"] = "";
							$this->load->model("Moderation_model");
							$this->Moderation_model->delete_moderation_item_by_obj($this->Users_model->moderation_type, $user_id);
						}elseif($data["user_logo"]){
							$this->Uploads_model->delete_upload($this->Users_model->upload_config_id, $user_id."/", $data["user_logo"]);
							$validate_data["data"]["user_logo"] = "";
						}
					}
					$this->Users_model->save_user($user_id, $validate_data["data"], "user_icon");
						
					$this->load->model("users/models/Auth_model");
					$this->Auth_model->update_user_session_data($user_id);	
			
					$this->set_api_content("messages", l("success_update_user", "users"));
				break;
			}
		}
	}
	
	/**
	 * Change list of user subscriptions
	 * 
	 * @return void
	 */
	public function save_subscriptions(){
		////save user subscribers
		$user_subscriptions_list = $this->input->post("user_subscriptions_list", true);
		$this->load->model("subscriptions/models/Subscriptions_users_model");
		$this->Subscriptions_users_model->save_user_subscriptions($user_id, $user_subscriptions_list);
		$this->set_api_content("messages", l("success_update_user", "users"));
	}
	
	/**
	 * Register new user
	 * 
	 * @param string $user_type user type
	 * @return void
	 */
	public function register($user_type=null){
		if(!$user_type){
			$user_types = $this->Users_model->get_user_types();
			$user_type = current($user_types);
		}
		$post_data = $this->input->post("data", true);
		$post_data["user_type"] = $user_type;

		$validate_data = $this->Users_model->validate_user(null, $post_data, 'user_icon');
		if (!empty($validate_data["errors"])) {
			$this->set_api_content('errors', $validate_data["errors"]);
			$this->session->sess_destroy();
			return;
		}
		$data = $validate_data["data"];
		$data["status"] = 1;
		if ($this->use_email_confirmation) {
			$data["confirm"] = 0;
			$data["confirm_code"] = substr($this->Users_model->encode_confirm(date("Y-m-d H:i:s") . $data["nickname"]), 0, 10);
		} else {
			$data["confirm"] = 1;
			$data["confirm_code"] = "";
		}
		$saved_password = $post_data["password"];
		
		$data["lang_id"] = $this->pg_language->get_default_lang_id();
		
		$user_id = $this->Users_model->save_user(null, $data, 'user_icon');
		$this->set_api_content('messages', l('success_user_updated', 'users'));
		
		////save user subscribers
		if($this->pg_module->is_module_installed("subscriptions")){
			$this->load->model("subscriptions/models/Subscriptions_users_model");
			$this->Subscriptions_users_model->save_user_subscriptions($user_id, $_REQUEST['user_subscriptions_list']);
		}

		$data["password"] = $saved_password;
		if ($this->use_email_confirmation) {
			$link = site_url() . "users/confirm/" . $data["confirm_code"];
			$data["confirm_block"] = str_replace("[link]", $link, l('confirm_block_text', 'users'));
		}
		
		$this->load->model('Notifications_model');
		$this->Notifications_model->send_notification($data["email"], 'users_registration', $data, '', $data["lang_id"]);

		if (!$this->use_email_confirmation) {
			$this->load->model("users/models/Auth_model");
			$auth_data = $this->Auth_model->login($user_id);
			if (!empty($auth_data["errors"])) {
				$this->set_api_content('errors', $auth_data["errors"]);
				$this->session->sess_destroy();
			}  else {
				$token = $this->session->sess_create_token();
				$this->set_api_content('data', array('token' => $token));
			}
		} else {
			$this->set_api_content('success', l('info_please_checkout_mailbox', 'users'));
		}
	}
	
	/**
	 * Get data of user profile
	 * 
	 * @param integer $user_id user identifier
	 * @return void
	 */
	public function view($user_id){
		if(!$user_id){
			$this->set_api_content('error', l('api_error_not_access', 'users')); 
			return;
		}
	
		$this->Users_model->set_format_settings('get_contact', true);
		$data['user'] = $this->Users_model->get_user_by_id($user_id);
		$this->Users_model->set_format_settings('get_contact', false);
	
		if(!$data['user']){
			$this->set_api_content('error', l('api_error_not_access', 'users'));
			return;
		}
		
		if($this->session->userdata('auth_type') == 'user'){
			$auth_id = $this->session->userdata('user_id');
		}else{
			$auth_id = 0;
		}
	
		if(!$data['user']['status'] && $auth_id != $user_id){
			$this->set_api_content('error', l('api_error_not_access', 'users'));
			return;
		}
		
		if($this->session->userdata('auth_type') == 'user' && $this->session->userdata('user_id') == $data['user']['id']){
			$data['user']['is_owner'] = 1;
		}else{
			$data['user']['is_owner'] = 0;
		}
		
		$data['user']['logo_image'] = $data['user']['media']['user_logo']['thumbs'];
		
		$this->Users_model->check_views_count($user_id);
		
		$this->set_api_content('data', (array)$data);
	}
	
	/**
	 * Approve/decline requests from my agent(s)
	 * 
	 * @param string $action action name
	 * @param integer $agent_id	 agent identifier
	 * @return void
	 */
	public function my_agents_request($action="approve", $agent_id=null){
		$errors = false;
		$messages = array();
		if(!$agent_id){
			$agent_id = $this->input->post("ids");
		}		
		if(!empty($agent_id)){
			$company_id = $this->session->userdata("user_id");
			foreach((array)$agent_id as $i){
				$error = $this->Users_model->{$action."_agent"}($company_id, $i);
				if($error){
					$errors = true;
					$messages[] = $error;
				}else{
					$messages[] = l("success_".$action."_agent", "users");
				}
			}
			if($errors){
				$this->set_api_content("errors", implode("<br>", $messages));
			}else{
				$this->set_api_content("messages", implode("<br>", $messages));
			}
		}
	}

	/**
	 * Remove agent from my agents
	 * 
	 * @param integer $agent_id agent identifier
	 * @return void
	 */
	public function my_agents_delete($agent_id=null){
		$errors = false;
		$messages = array();
		if(!$agent_id){
			$agent_id = $this->input->post("ids");
		}		
		if(!empty($agent_id)){
			$company_id = $this->session->userdata("user_id");
			foreach((array)$agent_id as $i){
				$error = $this->Users_model->delete_agent($company_id, $i);
				if($error){
					$errors = true;
					$messages[] = $error;
				}else{
					$messages[] = l("success_deleted_agent", "users");
				}
			}
			if($error){
				$this->set_api_content("errors", implode("<br>", $messages));
			}else{
				$this->set_api_content("messages", implode("<br>", $messages));
			}
		}
	}
	
	/**
	 * I get out from company
	 * 
	 * @return void
	 */
	public function agent_get_out(){
		$user_id = $this->session->userdata("user_id");
		$user = $this->Users_model->get_user_by_id($user_id);
		$error = $this->Users_model->delete_agent($user["agent_company"], $user_id);
		if($error){
			$this->set_api_content("errors", $error);
		}else{
			$this->set_api_content("messages", l("success_agent_get_out", "users"));
		}
	}
	
	/**
	 * My account
	 * 
	 * @return void
	 */
	public function account(){
		if($this->session->userdata("auth_type") != "user"){
			$this->set_api_content('error', l('api_error_not_access', 'users'));
			return;
		}
		
		$user_id = $this->session->userdata("user_id");

		$this->load->model("users/models/Auth_model");
		$this->Auth_model->update_user_session_data($user_id);

		$data = $this->Users_model->get_user_by_id($user_id);
		$this->set_api_content('account', $data['account']);
	}
	
	/**
	 * Catalogue of users
	 * 
	 * @param string $user_type user type
	 * @param string $order sorting order
	 * @param string $order_direction sorting direction
	 * @param integer $page page of results
	 * @return void
	 */
	public function index($user_type=null, $order='date_created', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION["users_list"])?$_SESSION["users_list"]:array();
		if(!isset($current_settings["order"]))
			$current_settings["order"] = "date_created";
		if(!isset($current_settings["order_direction"]))
			$current_settings["order_direction"] = "DESC";
		if(!isset($current_settings["page"]))
			$current_settings["page"] = 1;
		if(!isset($current_settings["data"]))
			$current_settings["data"] = array();

		if($current_settings['data']['type'] != $user_type){
			$current_settings['data']['type'] = $user_type;
			$current_settings['page'] = 1;
		}
		$this->template_lite->assign('user_type', $user_type);
	
		$filters = $current_settings['data'];
		$filters['active'] = 1;
		
		if($this->session->userdata("auth_type") == "user")
			$filters['not_user'] = $this->session->userdata("user_id");

		$order = trim(strip_tags($order));
		if(!empty($order)) $current_settings['order'] = $order;
		$order = $current_settings['order'];

		$order_direction = strtoupper(trim(strip_tags($order_direction)));
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		
		if($order_direction != 'ASC') $order_direction = 'DESC';
		
		$_SESSION['users_pagination'] = array(
			'order' => $order,
			'order_direction' => $order_direction,
			'page' => $page,
			'data' => $filters,
		);
		
		$view_mode = isset($_SESSION['users_view_mode']) ? $_SESSION['users_view_mode'] : 'list';
		
		$items_on_page = $this->pg_module->get_module_config("start", "index_items_per_page");
		
		$use_map_in_search = true;
		if($user_type == 'company'){
		    if($view_mode == 'list'){
				$use_map_in_search = $this->pg_module->get_module_config('users', 'use_map_in_search');
			}
		}
	 
		$users = array();
		$users_count = $this->Users_model->get_users_count_by_filters($filters);
		if($users_count > 0){
			$order_array = array();
			if($order == 'name'){
				if($user_type == 'company'){
					$order_array = array('company_name' => $order_direction);
				}else{
					$order_array = array("CONCAT(fname, ' ', sname)" => $order_direction);
				}
			}else{
				$order_array = array($order => $order_direction);
			}
		
			$this->Users_model->set_format_settings('get_contact', true);
			$users = $this->Users_model->get_users_list_by_filters($filters, $page, $items_on_page, $order_array);
			$this->Users_model->set_format_settings('get_contact', false);
		}
	
		$_SESSION['users_list'] = $current_settings;
		
		$this->set_api_content('users', $users);
		$this->set_api_content('users_count', $users_count);
		$this->set_api_content('settings', $current_settings);
	}
	
	/**
	 * Found users by searching
	 * 
	 * @param integer $page page of results
	 * @param string $order sorting order
	 * @param string $order_direction sorting direction
	 * @return void
	 */
	public function search($page=1, $order='default', $order_direction='DESC'){
		$filters = $this->input->post('criteria', true);
	
		$filters['active'] = '1';
		
		$data['order'] = trim(strip_tags($order));
		if(!$data['order']) $data['order'] = 'date_created';
		
		$data['order_direction'] = strtoupper(trim(strip_tags($order_direction)));
		if($data['order_direction']!='DESC') $data['order_direction'] = 'ASC';

		$data['page'] = $page;
		if(!$data['page']) $data['page'] = 1;
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
		
		$data['users_count'] = $this->Users_model->get_users_count_by_filters($filters);
		
		$use_leader = false;
		if($data['users_count'] > 0){
			$order_array = array();

			if($order == 'default'){
				$order_array['id'] = 'DESC';
			}else{
				$order_array[$data['order']] = $data['order_direction'];
			}
			
			$format_settings = array('get_contact'=>true);
	
			if(isset($filters['type']) && $filters['type'] == 'agent'){
				$format_settings['get_company'] = true;
			}
	
			$this->Users_model->set_format_settings($format_settings);
			$data['users'] = $this->Users_model->get_users_list_by_filters($filters, $data['page'], $items_on_page, $order_array);
			$this->Users_model->set_format_settings(array('get_company'=>false, 'get_contact'=>false));
		
			foreach($data['users'] as $key=>$user){
				$user['logo_image'] = $user['media']['user_logo']['thumbs'];
				$user['listings_count'] = $user['listings_for_sale_count'] + $user['listings_for_buy_count'] +
										  $user['listings_for_rent_count'] + $user['listings_for_lease_count'];
				$data['users'][$key] = $user;
			}
		}else{
			$this->set_api_content('messages', l('api_error_users_not_found', 'users'));
		}

		$data['date_format'] = $this->pg_date->get_format('date_literal', 'st');
		$data['date_time_format'] = $this->pg_date->get_format('date_time_literal', 'st');
		
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Change user currency
	 * 
	 * @param integer $currency_id currency identifier
	 * @return void
	 */
	public function change_currency($currency_id){
		$currency_id = intval($currency_id);
		$this->session->set_userdata("currency_id", $currency_id);

		if($this->session->userdata("auth_type") == "user"){
			$user_id = $this->session->userdata("user_id");
			$save_data["id_currency"] = $currency_id;
			$this->Users_model->save_user($user_id, $save_data);
		}
		
		$this->set_api_content('result', 'ok');
	}
	
	/**
	 * Change user language 
	 * 
	 * @param integer $lang_id language identifier
	 * @return void
	 */
	public function change_language($lang_id){
		$lang_id = intval($lang_id);
		
		$old_code = $this->pg_language->languages[$this->pg_language->current_lang_id]["code"];
		$this->pg_language->current_lang_id = $lang_id;
		$code = $this->pg_language->languages[$lang_id]["code"];
		$_SERVER["HTTP_REFERER"] = str_replace("/".$old_code."/", "/".$code."/", $_SERVER["HTTP_REFERER"]);
		$site_url = str_replace("/".$code."/", "", site_url());
		
		$this->session->set_userdata("lang_id", $lang_id);

		if($this->session->userdata("auth_type") == "user"){
			$user_id = $this->session->userdata("user_id");
			$save_data["lang_id"] = $lang_id;
			$this->Users_model->save_user($user_id, $save_data);
		}
		
		$this->set_api_content('result', 'ok');
	}
	
	/**
	 * Get agents of user
	 * 
	 * @param integer $user_id user identifier
	 * @return void
	 */
	public function agents($user_id){
		$order = $this->input->post('order', true);
		$data['order'] = trim(strip_tags($order));
		if(!$data['order']) $data['order'] = 'date_created';
		
		$order_direction = $this->input->post('order_direction', true);
		$data['order_direction'] = strtoupper(trim(strip_tags($data['order_direction'])));
		if($data['order_direction']!='DESC') $data['order_direction'] = 'ASC';

		$data['page'] = $page;
		if(!$data['page']) $data['page'] = 1;
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
		
		$data['users_count'] = $this->Users_model->get_agents_count($user_id, 1, null, 1);
		
		$use_leader = false;
		if($data['users_count'] > 0){
			$order_array = array();

			if($order == 'default'){
				$order_array['id'] = 'DESC';
			}else{
				$order_array[$data['order']] = $data['order_direction'];
			}
	
			$this->Users_model->set_format_settings('get_contact', true);
			$data['users'] = $this->Users_model->get_agents_list($user_id, 1, $data['page'], $items_on_page, $order_array, true, null, 1);
			$this->Users_model->set_format_settings('get_contact', false);
			foreach($data['users'] as $key=>$user){
				$user['logo_image'] = $user['media']['user_logo']['thumbs'];
				$user['listings_count'] = $user['listings_for_sale_count'] + $user['listings_for_buy_count'] +
										  $user['listings_for_rent_count'] + $user['listings_for_lease_count'];
				$data['users'][$key] = $user;
			}
		}else{
			$this->set_api_content('messages', l('api_error_users_not_found', 'users'));
		}

		$data['date_format'] = $this->pg_date->get_format('date_literal', 'st');
		$data['date_time_format'] = $this->pg_date->get_format('date_time_literal', 'st');
		
		$this->set_api_content('data', $data);
	}
}

<div class="view-user">
{l i='no_information' gid='users' assign='no_info_str'}
{l i='not_access' gid='users' assign='not_access_str'}
{l i='please_buy' gid='users' assign='buy_str'}
{if $user_data.user_type == 'agent' && $user_data.agent_status}
    <div class="r">
		<div class="f">{l i='company' gid='users'}: </div>
		<div class="v"><a href="{seolink module='users' method='view' data=$user_data.company}">{$user_data.company.output_name}</a></div>
    </div>
{/if}
{if $user_data.contact_info  ne ''}
<div class="r">
    <div class="f">{l i='field_description' gid='users'}: </div>
    <div class="v">
		{if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		{elseif !$user_data.contact_info}<font class="gray_italic">{$no_info_str}</font>
		{elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		{elseif $template eq 'small'}{$user_data.contact_info|truncate:55}
		{else}{$user_data.contact_info}
		{/if}
    </div>
</div>
{/if}
{if $user_data.facebook  ne ''}
<div class="r">
    <div class="f">{l i='field_facebook' gid='users'}: </div>
    <div class="v">
		{if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		{elseif !$user_data.facebook}<font class="gray_italic">{$no_info_str}</font>
		{elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		{else}<a href="{$user_data.facebook|escape}" target="_blank">{$user_data.facebook}</a>
		{/if}
    </div>
</div>
{/if}
<!--<div class="r">
    <div class="f">{l i='field_twitter' gid='users'}: </div>
    <div class="v">
		{if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		{elseif !$user_data.twitter}<font class="gray_italic">{$no_info_str}</font>
		{elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		{else}<a href="{$user_data.twitter|escape}" target="_blank">{$user_data.twitter}</a>
		{/if}
    </div>
</div>
<div class="r">
    <div class="f">{l i='field_vkontakte' gid='users'}: </div>
    <div class="v">
		{if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		{elseif !$user_data.vkontakte}<font class="gray_italic">{$no_info_str}</font>
		{elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		{else}<a href="{$user_data.vkontakte|escape}" target="_blank">{$user_data.vkontakte}</a>
		{/if}
    </div>
</div>-->

{switch from=$user_data.user_type}
	{case value='private'}
    {if $user_data.contact_phone  ne ''}
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:</div>
		<div class="v">
		    {if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user_data.contact_phone}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user_data.contact_phone}
		    {/if}
		</div>
	</div>
    {/if}
    {if $user_data.contact_email  ne ''}
	<div class="r">
		<div class="f">{l i='field_contact_email' gid='users'}:</div>
		<div class="v">
		    {if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user_data.contact_email}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user_data.contact_email}
		    {/if}
		</div>
	</div>
     {/if}

	{case value='company'}
    {if $user_data.location  ne ''}
	<div class="r">
		<div class="f">{l i='field_region' gid='users'}:</div>
		<div class="v">
		    {if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user_data.location}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user_data.location}
		    {/if}
		</div>
	</div>
    {/if}
    {if $user_data.contact_phone ne ''}
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:</div>
		<div class="v">
		    {if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user_data.contact_phone}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}<span itemprop="tel">{$user_data.contact_phone}</span>
		    {/if}
		</div>
	</div>
     {/if}
     {if $user_data.contact_email ne ''}
	<div class="r">
		<div class="f">{l i='field_contact_email' gid='users'}:</div>
		<div class="v">
		    {if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user_data.contact_email}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user_data.contact_email}
		    {/if}
		</div>
	</div>
    {/if}
    {if $user_data.web_url ne ''}
	<div class="r">
		<div class="f">{l i='field_web_url' gid='users'}:</div>
		<div class="v">
		    {if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user_data.web_url}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}<a href="http://{$user_data.web_url}" target="_blank" itemprop="url">{$user_data.web_url}</a>
		    {/if}
		</div>
	</div>
    {/if}
    {if $user_data.working_days_str ne ''}
	<div class="r">
	    <div class="f">{l i='field_working_days' gid='users'}: </div>
	    <div class="v">
			{if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user_data.working_days_str}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user_data.working_days_str}
		    {/if}
		</div>
	</div>
    {/if}
    {if $user_data.working_hours_str ne ''}
	<div class="r">
	    <div class="f">{l i='field_working_hours' gid='users'}: </div>
	    <div class="v">
			{if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user_data.working_hours_str}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user_data.working_hours_str}
		    {/if}
		</div>
	</div>
    {/if}
	<!--<div class="r">
	    <div class="f">{l i='field_lunch_time' gid='users'}: </div>
	    <div class="v">
			{if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user_data.lunch_time_str}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user_data.lunch_time_str}
		    {/if}
		</div>
	</div>-->
	
	{case value='agent'}
    {if !$user_data.contact_phone ne ''}
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:</div>
		<div class="v">
		    {if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user_data.contact_phone}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user_data.contact_phone}
		    {/if}
		</div>
	</div>
    {/if}
    {if !$user_data.contact_email ne ''}
	<div class="r">
		<div class="f">{l i='field_contact_email' gid='users'}:</div>
		<div class="v">
		    {if $user_data.no_access_contact}<font class="gray_italic">{$not_access_str}</font>
		    {elseif !$user_data.contact_email}<font class="gray_italic">{$no_info_str}</font>
		    {elseif !$user_data.is_contact}<font class="gray_italic">{$buy_str}</font>
		    {else}{$user_data.contact_email}
		    {/if}
		</div>
	</div>
    {/if}
{/switch}

{if !$user_data.is_contact && !$user_data.no_access_contact}
<div class="buy-box"><input type="button" value="{l i='btn_activate_contact' gid='services' type='button'}" name="contacts_btn"></div>
{/if}

</div>

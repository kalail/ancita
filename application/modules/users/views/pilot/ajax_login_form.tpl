<div class="content-block load_content">
	<h1>{l i='header_login' gid='users'}</h1>

	<div class="inside logform">
		<form action="{$site_url}users/login" method="post">
		<div class="r">
			<div class="f">{l i='field_email' gid='users'}:&nbsp;*</div>
			<div class="v"><input type="text" name="email" {if $DEMO_MODE}value="{$demo_user_type_login_settings.login|escape}"{/if} onfocus="this.removeAttribute('readonly');" readonly></div>
		</div>
		<div class="r">
			<div class="f">{l i='field_password' gid='users'}:&nbsp;*</div>
			<div class="v">
				<input type="password" name="password" {if $DEMO_MODE}value="{$demo_user_type_login_settings.password|escape}"{/if} onfocus="this.removeAttribute('readonly');" readonly>
				<span class="v-link"><a href="{$site_url}users/restore">{l i='link_restore' gid='users'}</a></span>
			</div>
		</div>
		{* Don't delete (openid) *}
		{*<h3>{l i='field_open_id' gid='users'}</h3>
		<div class="r">
			<div class="v"><input type="text" name="user_open_id" class="openid"></div>
		</div>
		<br>*}
		<div class="r">
			<input type="submit" value="{l i='btn_login' gid='start' type='button'}" name="logbtn">
		</div>
		</form>
		{helper func_name=show_social_networking_login module=users_connections}
		{if $user_types|count}
		<div class="line top">
			<p class="header-comment">{l i='text_register_comment' gid='users'}</p>
            {*<span class="btn-link"><ins class="fa fa-arrow-right fa-lg edge hover no-hover"></ins></span>
             <a href="" onclick="javascript: window.open('{$site_url}registration-as-guest', 'blank_', 'resizable=yes, scrollbars=yes, location=no, directories=no, status=no, width=650, height=768, toolbar=no, menubar=no, left=0,top=0');" id="guestlink" class="btn-link">Guest User</a>*}
             <div class="clr"></div>
			<span class="btn-link"><ins class="fa fa-arrow-right fa-lg edge hover no-hover"></ins>{l i='text_register_as' gid='users'}:</span>
			<div class="clr"></div>
			{foreach item=item from=$user_types}
			<a href="{seolink module='users' method='reg_'+$item}" class="btn-link btn-margin">{l i='link_'+$item gid='users' }</a>
			{/foreach}
		</div>
		{/if}
	</div>
	<div class="clr"></div>
</div>
<script>{literal}
	$(function(){
	var id = $(".guestlink").attr('data');
	$('#guestlink').attr( 'data',id);
	});
{/literal}</script>



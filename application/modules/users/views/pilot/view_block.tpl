		{capture assign="review_callback"}{literal}
			function(data){
				window.location.href = '{/literal}{seolink module='users' method='view' data=$user section='reviews'}#m_reviews{literal}';
			}
		{/literal}{/capture}
		
		{if $user_type eq 'company'}<div itemscope itemtype="http://data-vocabulary.org/Organization">{/if}
		
		<h1>{seotag tag='header_text'}</h1>
		<div class="actions noPrint">
			{block name='button_contact' module='mailbox' user_id=$user.id user_type=$user_type}
			{block name='send_review_block' module='reviews' object_id=$user.id type_gid='users_object' responder_id=$user.id success=$review_callback is_owner=$is_user_owner}
			{block name='user_listings_button' module='listings' user=$user}
			<a href="{$site_url}users/pdf/{$user.id}" id="pdf_btn" class="btn-link link-r-margin" rel="nofollow" title="{l i='link_pdf' gid='listings' type='button'}"><ins class="fa fa-pdf fa-lg edge hover"></ins></a>
			<a href="javascript:void(0);" id="print_btn" class="btn-link link-r-margin" rel="nofollow" title="{l i='link_print' gid='listings' type='button'}" onclick="javascript: window.print(); return false;"><ins class="fa fa-print fa-lg edge hover"></ins></a>
			{*{block name='mark_as_spam_block' module='spam' object_id=$user.id type_gid='users_object' is_owner=$is_user_owner}*}
		</div>
		<div class="clr"></div>
		<div class="view_user">
			<div class="image">
				{l i='text_user_logo' gid='users' type='button' assign='text_user_logo'}
				<a href="{seolink module='users' method='view' data=$user}">
					<img src="{$user.media.user_logo.thumbs.big}" alt="{$text_user_logo|replace:'[user_name]':$user.output_name|escape}" title="{$user.output_name|escape}">
				</a>
			</div>
			<div class="body">
				{l i='no_information' gid='start' assign='no_info_str'}
				<div class="t-1">
					{depends module=reviews}
					{block name=get_rate_block module=reviews rating_data_main=$user.review_value type_gid='users_object' template='normal' read_only='true'}<br>
					<span>{l i='field_rate' gid='users'}:</span> {$user.review_value}<br>
					<span>{l i='field_reviews_count' gid='users'}:</span> {$user.review_count}<br>
					{if $user.review_count eq 0}{block name='send_review_block' module='reviews' object_id=$user.id type_gid='users_object' responder_id=$user.id success=$review_callback is_owner=$is_user_owner template='first'}{/if}<br>
					{/depends}
					{if $user.user_type eq "company" && $user.agent_count}<span>{l i='field_agent_count' gid='users'}:</span> {$user.agent_count}<br>{/if}
					{*{if $user.listings_for_sale_count}<span>{l i='field_listings_for_sale' gid='users'}:</span> <a href="{seolink module='listings' method='user' id_user=$user.id user=$user.output_name operation_type='sale'}">{$user.listings_for_sale_count}</a><br>{/if}
					{if $user.listings_for_buy_count}<span>{l i='field_listings_for_buy' gid='users'}:</span> <a href="{seolink module='listings' method='user' id_user=$user.id user=$user.output_name operation_type='buy'}">{$user.listings_for_buy_count}</a><br>{/if}
					{if $user.listings_for_rent_count}<span>{l i='field_listings_for_rent' gid='users'}:</span> <a href="{seolink module='listings' method='user' id_user=$user.id user=$user.output_name operation_type='rent'}">{$user.listings_for_rent_count}</a><br>{/if}
					{if $user.listings_for_lease_count}<span>{l i='field_listings_for_lease' gid='users'}:</span> <a href="{seolink module='listings' method='user' id_user=$user.id user=$user.output_name operation_type='lease'}">{$user.listings_for_lease_count}</a><br>{/if}*}
				</div>
				<div class="t-2">
					<span>{l i='field_register' gid='users'}:</span> {$user.date_created|date_format:$page_data.date_format}<br>
					<span>{l i='field_views' gid='users'}:</span> {$user.views}<br>
					<span class="status_text">{if $user.is_featured}{l i='status_featured' gid='users'}{/if}</span>
				</div>
			</div>
			<div class="clr"></div>
		</div>
		<div class="edit_block">
			{include file="view_menu.tpl" module=users theme=user}
			<div id="user_block">
		{*if $user.user_type eq 'agent'}<section id="content_m_company" class="view-section{if $section_gid ne 'company'} hide{/if} noPrint">{$user_content.company}</section>{/if*}
				<section id="content_m_reviews" class="view-section{if $section_gid ne 'reviews'} hide{/if} noPrint">{$user_content.reviews}</section>
				{if $user.user_type eq 'company'}<section id="content_m_map" class="view-section{if $section_gid ne 'map'} hide{/if} noPrint">
                <iframe width="630" height="500" frameborder="0" style="border:0;margin-top:-130px;" src = "https://maps.google.com/maps?q={$user.lat},{$user.lon}&hl=es;z=5&amp;output=embed">
               </iframe>
                </section>{/if}
				<section id="content_m_contacts" class="view-section{if $section_gid ne 'contacts'} hide{/if} print_block">
                {$user_content.contacts}
                </section>
                {if $user.office_image ne ""}
                <img id="content_m_cimg" src="{$site_root}uploads/photo/{$user.office_image}" height="200" width="250"/>
                {/if}
                <section id="content_m_xxxx" class="view-section{if $section_gid ne 'services'} hide{/if} noPrint">
        	    <div class="av-rt_2 skinned-form-controls skinned-form-controls-mac" id="mainpartser">
<div class="services-part">
<h4>{l i='agent_lbl_services_offered' gid='content'}</h4>
<div class="mainone real{$current_lang}"><input type="radio" {if $banner_de.real_estate == 1}checked {else} disabled{/if}><span></span><p>{l i='agent_lbl_1' gid='content'}</p></div>
<div class="maintwo rent{$current_lang}"><input type="radio" {if $banner_de.rental_service == 1}checked {else} disabled{/if}><span></span><p>{l i='agent_lbl_2' gid='content'}</p></div>
<div class="mainthree faci{$current_lang}"><input type="radio" {if $banner_de.facility_service == 1}checked {else} disabled{/if}><span></span><p>{l i='agent_lbl_3' gid='content'}</p></div>
<div class="mainfour insu{$current_lang}"><input type="radio" {if $banner_de.insurance_service == 1}checked {else} disabled{/if}><span></span><p>{l i='agent_lbl_4' gid='content'}</p></div>
<div class="mainfive bank{$current_lang}"><input type="radio" {if $banner_de.banking_service == 1}checked {else} disabled{/if}><span></span><p>{l i='agent_lbl_5' gid='content'}</p></div>
</div>
{if $banner_de.operations ne ""}
<div class="operates-part">
<h4>{l i='agent_lbl_operates_in' gid='content'}</h4>
<div class="operates-content">
{$banner_de.operations|replace:",":"<br />"}
</div>
</div>
 {/if}
 <div class="lang-part">
 <h4>{l i='agent_lbl_languages' gid='content'}</h4>
 <table cellpadding="12" style="border-collapse:collapse;" cellspacing="11" border="1" bordercolor="#a9a9a9" class="infotable">
                            <tr>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/no-no.png" alt="NO" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/sv-se.png" alt="SE" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/fi-fi.png" alt="FI" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/dk-dk.png" alt="DK" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/ru-ru.png" alt="RU" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/pl-pl.png" alt="PL" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/nl-nl.png" alt="NL" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/de-de.png" alt="DE" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/fr-fr.png" alt="FR" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/en-gb2.png" alt="EN" /></th>
                                <th class="last smallcell"><img src="{$site_root}{$img_folder}/flags/es-es.png" alt="ES" /></th>					
                            </tr>
                            <tr>
                                <td class="smallcell"><input type="radio" {if $banner_de.lang_no == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $banner_de.lang_se == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $banner_de.lang_fi == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $banner_de.lang_dk == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $banner_de.lang_ru == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $banner_de.lang_pl == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $banner_de.lang_ne == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $banner_de.lang_de == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $banner_de.lang_fr == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $banner_de.lang_en == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="last smallcell"><input type="radio" {if $banner_de.lang_es == 1}checked {else} disabled{/if}><span></span></td>					
                            </tr>				
                        </table>
 </div>
</div>
                </section>
				<section id="content_m_listings" class="view-section{if $section_gid ne 'listings'} hide{/if} noPrint">{$user_content.listings}</section>
			</div>
		</div>
		{if $user_type eq 'company'}</div>{/if}
    



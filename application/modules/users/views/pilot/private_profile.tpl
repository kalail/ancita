<p class="header-comment">
{l i="text_register_as" gid='users'}: <b>{$data.user_type_str}</b><br>
{l i="field_date_created" gid='users'}: <b>{$data.date_created|date_format:$page_data.date_format}</b>
</p>

<h2 class="line top bottom linked">
	{l i='table_header_personal' gid='users'}
	<a class="fright" href="{$site_url}users/profile/personal/"><ins class="fa fa-pencil fa-lg edge hover"></ins></a>
</h2>
<div class="view-section">
	<div class="r">
		<div class="f">{l i='field_fname' gid='users'}:</div>
		<div class="v">{$data.fname}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_sname' gid='users'}:</div>
		<div class="v">{$data.sname}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:</div>
		<div class="v">{if $data.phone}{$data.phone}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_icon' gid='users'}:</div>
		<div class="v">
		{if $data.user_logo_moderation}
		<img src="{$data.media.user_logo_moderation.thumbs.middle}" alt="{$data.output_name|truncate:30|escape}" />{else}
		<img src="{$data.media.user_logo.thumbs.middle}" alt="{$data.output_name|escape}" />
		{/if}
		</div>
	</div>	
</div>

<h2 class="line top bottom linked">
	{l i='table_header_contact' gid='users'}
	<a class="fright" href="{$site_url}users/profile/contact/"><ins class="fa fa-pencil fa-lg edge hover"></ins></a>
</h2>
<div class="view-section">
	<div class="r">
		<div class="f">{l i='field_contact_email' gid='users'}:</div>
		<div class="v">{if $data.contact_email}{$data.contact_email}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_contact_phone' gid='users'}:</div>
		<div class="v">{if $data.contact_phone}{$data.contact_phone}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_contact_info' gid='users'}:</div>
		<div class="v">{if $data.contact_info}{$data.contact_info|nl2br}{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_facebook' gid='users'}:</div>
		<div class="v">{if $data.facebook}<a href="{$data.facebook|escape}" target="_blank">{$data.facebook}</a>{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r" style="display:none;">
		<div class="f">{l i='field_twitter' gid='users'}:</div>
		<div class="v">{if $data.twitter}<a href="{$data.twitter|escape}" target="_blank">{$data.twitter}</a>{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
	<div class="r" style="display:none;">
		<div class="f">{l i='field_vkontakte' gid='users'}:</div>
		<div class="v">{if $data.vkontakte}<a href="{$data.vkontakte|escape}" target="_blank">{$data.vkontakte}</a>{else}{l i='no_information' gid='users'}{/if}</div>
	</div>
</div>

{helper func_name=get_user_subscriptions_list module=subscriptions}

	<div class="profile-info">
		<a href="{seolink module='users' method='view' data=$user}"><img src="{$user.media.user_logo.thumbs.big}" id="bigimg" alt="{$user.output_name|truncate:30|escape}" /></a>
		<div class="user-profile-info">
			<h3 class="s-title">{$user.output_name|truncate:100}</h3>
			{if $user.phone}{$user.phone}{/if}
			<p>{l i='text_profile_complete' gid='users'}:<p>
			<p class="filling-profile">
				<span class="filling-box"><spam class="filling-bar" style="width:{$complete}%"></span></span>
				<span class="percent">{$complete}%</span>
			</p>
			<a href="{$site_url}users/profile" class="btn-link"><ins class="fa fa-pencil fa-lg edge hover"></ins>{l i='link_edit_profile' gid='users'}</a>
		</div>
		<div class="clr"></div>
	</div>

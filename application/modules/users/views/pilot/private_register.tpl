{if $phone_format}
{js file='jquery.maskedinput.min.js'}
<script>{literal}
$(function(){
	$('#phone').mask('{/literal}{$phone_format}{literal}');
});
{/literal}
</script>
{/if}
<form action="" method="post" enctype="multipart/form-data">
	<div class="r">
		<div class="f">{l i='field_fname' gid='users'}:&nbsp;*</div>
		<div class="v"><input type="text" name="data[fname]" value="{$data.fname|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_sname' gid='users'}:&nbsp;*</div>
		<div class="v"><input type="text" name="data[sname]" value="{$data.sname|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_email' gid='users'}:&nbsp;*</div>
		<div class="v"><input type="text" name="data[email]" value="{$data.email|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:</div>
		<div class="v"><input type="text" name="data[phone]" value="{$data.phone|escape}" id="phone"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_password' gid='users'}:&nbsp;(minimum 6 characters)*</div>
		<div class="v"><input type="password" name="data[password]"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_repassword' gid='users'}:&nbsp;*</div>
		<div class="v"><input type="password" name="data[repassword]"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_captcha' gid='users'}:&nbsp;*</div>
		<div class="v captcha">{$data.captcha_image} <input type="text" name="captcha_confirmation" value="" maxlength="{$data.captcha_word_length}" /></div>
	</div>
	<div class="r">
		<div class="f"><input type="checkbox" name="show_my_contact_info" value="1" id="show_my_contact_info" /> <label for="show_my_contact_info">Show my contact info to other members</label></div>
		<div class="v"></div>
	</div>
	<div class="r">
		<div class="f"><input type="checkbox" name="license_agreement" value="1" maxlength="{$data.show_contact_info}" id="license_agreement" /> <label for="license_agreement">{l i='field_license_agreement' gid='users'}</label></div>
		<div class="v"></div>
	</div>
	<h1>I want to receive a notification when</h1>
    <div class="r">
    <div class="f"><input type="checkbox" name="Possible_matches" value="1" id="Possible_matches" /> <label for="Possible_matches">Possible matches found</label></div>
		<div class="v"></div>
	</div>
    <div class="r">
    <div class="f"><input type="checkbox" name="New_message_mailbox" value="1" id="New_message_mailbox" /> <label for="New_message_mailbox">There is a new message in mailbox</label></div>
		<div class="v"></div>
	</div>
    <div class="r">
    <div class="f"><input type="checkbox" name="Someone_viewed_profile" value="1" id="Someone_viewed_profile" /> <label for="Someone_viewed_profile">Someone viewed my profile</label></div>
		<div class="v"></div>
	</div>
    <div class="r">
    <div class="f"><input type="checkbox" name="Someone_interested_listing" value="1" id="Someone_interested_listing" /> <label for="Someone_interested_listing">Someone interested in my listing</label></div>
		<div class="v"></div>
	</div>
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v"><input type="submit" value="{l i='btn_register' gid='start' type='button'}" name="btn_register"></div>
	</div>
</form>

	<div class="user_info">
		<h2>{l i='header_provided_by' gid='listings'}</h2>
		{if $user.status}
		{capture assign='user_actions'}{strip}
			<!--{block name='button_contact' module='mailbox' user_id=$user.id user_type=$user.user_type}-->
			{if $show_all_listings}{block name='user_listings_button' module='listings' user=$user}{/if}
			{block name='send_review_block' module='reviews' object_id=$user.id type_gid='users_object' responder_id=$user.id is_owner=$is_user_owner}
            <!--<a href="{$site_url}feedback" target="_blank" class="btn-link link-r-margin" title="{l i='link_send_review' gid='reviews' type='button'}"><ins class="fa fa-edit fa-lg edge"></ins></a>-->
			<!--{block name='mark_as_spam_block' module='spam' object_id=$user.id type_gid='users_object' is_owner=$is_user_owner}-->
		{/strip}{/capture}
		{if $user_actions}
		<div class="actions noPrint">
			{$user_actions}
		</div>
		{/if}
		{/if}
		
		<div class="clr"></div>	
		<div class="image">
			{l i='text_user_logo' gid='users' type='button' assign='text_user_logo' user_name=$user.output_name}
			{if $user.status}
			<a href="{seolink module='users' method='view' data=$user}" title="{l i='link_user_view' gid='users' type='button'}"><img src="{$user.media.user_logo.thumbs.big}" alt="{$text_user_logo}"></a>
			{else}
			<img src="{$user.media.user_logo.thumbs.big}" alt="{$text_user_logo}" title="{l i='field_user_logo' gid='users' type='button'}">
			{/if}
		</div>
		
		{if $user.user_type eq 'company'}<div itemscope itemtype="http://data-vocabulary.org/Organization">{/if}
		
		<h3>{if $user.status}<a href="{seolink module='users' method='view' data=$user}" title="{l i='link_user_view' gid='users' type='button'}" {if $user.user_type eq 'company'}itemprop="name"{/if}>{$user.output_name|truncate:30}</a>{else}<span {if $user.user_type eq 'company'}itemprop="name"{/if}>{$user.output_name|truncate:30}</span>{/if}, {ld_option i='user_type' gid='users' option=$user.user_type}</h3>
		{if $user.status}
		<div id="user_block">
			<div class="tabs tab-size-15 noPrint">
				<ul id="user_sections">
					<li id="ui_contacts" sgid="contacts" class="active">
                    <a href="{$site_url}listings/user/{$user.id}/{$sgid}">{l i='filter_section_info' gid='listings'}</a></li>
					{if $user.user_type eq 'company'}<li id="ui_map_info" sgid="map_info" class="{if $section_gid eq 'map_info'}active{/if}"><a href="{$site_url}listings/user/{$user.id}/{$sgid}">{l i='filter_section_map' gid='listings'}</a></li>{/if}
					{depends module=contact}<li id="ui_contact" sgid="contact" class="{if $section_gid eq 'contact'}active{/if}"><a href="{$site_url}listings/user/{$user.id}/{$sgid}">{l i='filter_section_contact' gid='listings'}</a></li>{/depends}
				</ul>
			</div>
			<div id="content_ui_contacts" class="view-section print_block">
			    {block name=view_user_block module=users user=$user template='small'}
			</div>
			<div id="content_ui_contact" class="view-section ui_contact">
             <div id="con-result"></div>
        <form id="contacts_form" name="contacts_form" onsubmit="checkRegistration()" action="" method="POST">
		<div class="r">
			<div class="f" id="capone">{l i='field_contact_sender' gid='contact'}&nbsp;*</div>
			<div class="v">
            	<input type="text" name="sender" id="sender" class="sender" onfocus="this.removeAttribute('readonly');" readonly/></div>
		</div>
		<div class="r">
			<div class="f" id="capone">{l i='field_contact_phone' gid='contact'}&nbsp;*</div>
			<div class="v">
           	<input type="text" name="con-phone" id="con-phone" class="con-phone" onfocus="this.removeAttribute('readonly');" readonly/>
            </div>
		</div>
		<div class="r">
			<div class="f" id="capone">{l i='field_contact_email' gid='contact'}&nbsp;*</div>
			<div class="v">	<input type="text" name="con-mail" id="con-mail" class="con-mail"  onfocus="this.removeAttribute('readonly');" readonly/></div>
		</div>
		<div class="r">
			<div class="f" id="capone">{l i='field_contact_message' gid='contact'}&nbsp;</div>
			<div class="v"><textarea name="message" id="message" rows="5" cols="23">{$message|escape}</textarea></div>
		</div>
        <div class="r">
			<div class="f" id="capone">{l i='field_contact_captcha' gid='contact'}&nbsp;*</div>
			<div class="v captcha">
				{$data.captcha_image}
				<input type="text" size="12" name="code" id="code" value="" maxlength="{$data.captcha_word_length}" onfocus="this.removeAttribute('readonly');" readonly>	
                <input type="hidden" name="captcha_word" id="captcha_word" value="{$captcha_word}">			
			</div>
		</div>
		<div class="r">
			<input type="submit" id="con-submit" value="{l i='btn_send' gid='start' type='button'}" />
		</div>
		<input type="hidden" id="user_id" name="user_id" value="{$user.id}" /> 
        <input type="hidden" id="listing_id" name="listing_id" value="" /> 
	    </form>
            </div>
			{if $user.user_type eq 'company'}
			    <div id="content_ui_map_info" class="view-section ui_map_info">
				<iframe width="300" height="360" frameborder="0" style="border:0;margin-top:-100px;" src = "https://maps.google.com/maps?q={$user.lat},{$user.lon}&hl=es;z=5&amp;output=embed">
               </iframe>
			    </div>
			{/if}
		</div>
		
		{if $user.user_type eq 'company'}</div>{/if}
		
		{js module=users file='users-menu.js'}
		{js module=users_services file='available_view.js'}
        <script>{literal}
	   function checkRegistration(){
		var user_id = $.trim($("#user_id").val());
		var sender = $.trim($("#sender").val());
		var phone = $.trim($("#con-phone").val());
		var email = $.trim($("#con-mail").val());
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var capRegex = /[0-9 -()+]+$/;
		var message = $.trim($("#message").val());
	    var code = $.trim($("#code").val());
	    var captcha_word = $.trim($("#captcha_word").val());
	    var id = document.URL.match(/\d+/g);
	    document.getElementById("listing_id").value = id;
		var listing_id = $.trim($("#listing_id").val());
		if(sender == '') {
			$("#sender").css('border-color','red');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#sender").focus();	
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand('Stop');
               }																						 
			});
			return false;
		}
		if(phone == '') {
			$("#sender").css("border-color", "");
			$("#con-phone").css('border-color','red');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#con-phone").focus();
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand('Stop');
               }																								 
			});
			return false;
		}else if(phone != '' && !capRegex.test(phone)) {
			$("#con-phone").css('border-color','red');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#con-phone").focus();
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand('Stop');
               }
			});
			return false;
		}
		if(email == '') {
			$("#con-phone").css('border-color','');
			$("#con-mail").css('border-color','red');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#con-mail").focus();	
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand('Stop');
               }																							 
			});
			return false;
		}else if(email != '' && !emailReg.test(email)) {
			$("#con-mail").css('border-color','red');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#con-mail").focus();
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand('Stop');
               }
			});
			return false;
		}
		if(code == '') {
			$("#con-mail").css('border-color','');
			$("#code").css('border-color','red');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#code").focus();
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand('Stop');
               }																								 
			});
			return false;
		}else if(code != '' && !capRegex.test(code)) {
			$("#code").css('border-color','red');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#code").focus();
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand('Stop');
               }
			});
			return false;
		}
	    var url = '{/literal}{$site_root}{literal}' + 'contact/ajax_consend_message';
		$.ajax({
        type: 'POST',
        url: url,
		async: false,
        data: { user_id:user_id, sender: sender, phone:phone, email:email, message:message,code:code,captcha_word:captcha_word, listing_id:listing_id},
        success: function(response) {
            if(response == 1){
				$("#con-result").html('<p class="success-message">Mail sent successfully.</p>');
				$('html, body').animate({scrollTop: $("#con-result").offset().top-200},3000,function(){
					$('.success-message').delay(3000).fadeOut();
				});
			}else{
				$("#con-result").html('<p class="error-message">Invalid code.</p>');
				$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
					$('.error-message').delay(1000).stop();
					try {
                     window.stop();
                    } catch (exception) {
                    document.execCommand('Stop');
                   }
				});
			}
        }
       });
	   }
	    {/literal}</script>
		<script>{literal}
			var rMenu;
			$(function(){
				rMenu = new usersMenu({
					siteUrl: '{/literal}{$site_root}{literal}',
					idUser: '{/literal}{$user.id}{literal}',
					tabPrefix: 'ui',
					CurrentSection: 'ui_contacts',
					template: 'small',
					{/literal}{depends module=users_services}available_view: new available_view(),{/depends}{literal}
				});
			});
		{/literal}</script>		
		{/if}
	</div>	

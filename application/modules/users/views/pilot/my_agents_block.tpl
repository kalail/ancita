	<div>
		<table class="list">
		<tr id="sorter_block">
			<th><a href="{$sort_links.name}" class="link-sorter">{l i='field_name' gid='users'}{if $page_data.order eq 'name'}<ins class="fa fa-arrow-{if $page_data.order_direction|lower eq 'asc'}up{else}down{/if}"></ins>{/if}</a></th>
			<th><a href="{$sort_links.email}" class="link-sorter">{l i='field_email' gid='users'}{if $page_data.order eq 'email'}<ins class="fa fa-arrow-{if $page_data.order_direction|lower eq 'asc'}up{else}down{/if}"></ins>{/if}</a></th>
			<th><a href="{$sort_links.phone}" class="link-sorter">{l i='field_phone' gid='users'}{if $page_data.order eq 'phone'}<ins class="fa fa-arrow-{if $page_data.order_direction|lower eq 'asc'}up{else}down{/if}"></ins>{/if}</a></th>
			<th><a href="{$sort_links.agent_date}" class="link-sorter">{l i='field_agent_date' gid='users'}{if $page_data.order eq 'agent_date'}<ins class="fa fa-arrow-{if $page_data.order_direction|lower eq 'asc'}up{else}down{/if}"></ins>{/if}</a></th>		
			<th class="w100">&nbsp;</th>		
		</tr>
		{foreach item=item from=$agents}
		<tr>
			<td><a href="{seolink module='users' method='view' data=$item}">{$item.output_name|truncate:100}</a></td>
			<td>{$item.contact_email|truncate:50}</td>
			<td>{$item.contact_phone}</td>
			<td>{if $item.agent_date|strtotime>0}{$item.agent_date|date_format:$page_data.date_format}{else}-{/if}</td>
			<td>
				{if $item.agent_status}
				<a href="{$site_url}users/my_agents_delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_agent_delete' gid='users' type='js'}')) return false;" class="btn-link fright" alt="{l i='btn_delete' gid='start' type='button'}" title="{l i='btn_delete' gid='start' type='button'}"><ins class="fa fa-trash-o fa-lg edge hover"></ins></a>
				{else}
				<a href="{$site_url}users/my_agents_request/decline/{$item.id}" class="btn-link fright" alt="{l i='btn_decline' gid='start' type='button'}" title="{l i='btn_decline' gid='start' type='button'}"><ins class="fa fa-minus fa-lg edge hover"></ins></a>
				<a href="{$site_url}users/my_agents_request/approve/{$item.id}" class="btn-link fright" alt="{l i='btn_approve' gid='start' type='button'}" title="{l i='btn_approve' gid='start' type='button'}"><ins class="fa fa-check fa-lg edge hover"></ins></a>				
				{/if}
			</td>
		</tr>
		{/foreach}
		</table>	
	</div>
	<div id="pages_block_2">{include file="pagination.tpl"}</div>


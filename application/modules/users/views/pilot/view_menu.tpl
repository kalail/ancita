<div class="tabs tab-size-15 noPrint">
	<ul id="user_sections">
		{foreach item=item key=sgid from=$display_sections}
		{assign var=sheadline value='filter_section_'`$sgid}
		{if $item}<li id="m_{$sgid}" sgid="{$sgid}" class="{if $section_gid eq $sgid}my{$sgid} active{/if}">
        <a id="{$sgid}" href="{$site_url}users/view/{$user.id}/{$sgid}">&nbsp;{l i=$sheadline gid='users'} {if $sgid eq 'reviews'}({$user.review_count}){/if}</a></li>{/if}
		{/foreach}
	</ul>
</div>
 <script>{literal}
	$(function(){ 
    $("#contacts").before("<i class='fa fa-user whitefa'></i>");
    $("#map").before("<i class='fa fa-map-marker'></i>");
	$("#services").before("<i class='fa fa-cog'></i>");
	});
	{/literal}</script>
<script>{literal}
	$(function(){ 
	 if ( $( "#m_map" ).hasClass( "mymap" ) ) {
	 $("#content_m_cimg").hide();
	 $(".fa-user").removeClass("whitefa"); 
	 }
	 if ( $( "#m_services" ).hasClass( "myservices" ) ) {
	 $("#content_m_cimg").hide();
	 $(".fa-user").removeClass("whitefa");
	 }
	});
{/literal}</script>	

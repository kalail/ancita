<div class="tabs tab-size-15 noPrint">
	<ul id="user_sections">
		{foreach item=item key=sgid from=$display_sections}
		{assign var=sheadline value='filter_section_'`$sgid}
		{if $item}<li id="m_{$sgid}" sgid="{$sgid}" class="{if $section_gid eq $sgid}active{/if}"><a href="{$site_url}users/view/{$user.id}/{$sgid}">{l i=$sheadline gid='users'} {if $sgid eq 'reviews'}({$user.review_count}){/if}</a></li>{/if}
		{/foreach}
	</ul>
</div>

{if $users}
<h2>
    {if !$header_featured_users || $header_featured_users eq ''}
	{l i='header_featured_users' gid='users'}
    {else}
	{$header_featured_users}
    {/if}
</h2>
<div class="{$type}_users_block">
	{foreach item=item key=key from=$users}
	<div class="user {$photo_size}">
		{if $item.media.user_logo.thumbs[$photo_size]}
		{l i='text_user_logo' gid='users' type='button' assign='text_user_logo' user_name=$item.output_name}
		<a href="{seolink module='users' method='view' data=$item}">
			<img src="{$item.media.user_logo.thumbs[$photo_size]}" alt="{$text_user_logo}" title="{$item.output_name|escape}">
		</a>
		{/if}
		<a href="{seolink module='users' method='view' data=$item}">{$item.output_name|truncate:30}</a>
		<span>{l i=$item.user_type gid='users'}</span>
	</div>
	{/foreach}
</div>
{/if}

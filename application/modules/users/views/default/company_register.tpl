{if $phone_format}
{js file='jquery.maskedinput.min.js'}
<script>{literal}
$(function(){
	$('#phone{/literal}{$rand}{literal}').mask('{/literal}{$phone_format}{literal}');
});
{/literal}</script>
{/if}
<form action="" method="post" enctype="multipart/form-data">
	<div class="r">
		<div class="f">{l i='field_fname' gid='users'}:&nbsp;*</div>
		<div class="v"><input type="text" name="data[fname]" value="{$data.fname|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_sname' gid='users'}:&nbsp;*</div>
		<div class="v"><input type="text" name="data[sname]" value="{$data.sname|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_company' gid='users'}:&nbsp;*</div>
		<div class="v"><input type="text" name="data[company_name]" value="{$data.company_name|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_email' gid='users'}:&nbsp;*</div>
		<div class="v"><input type="text" name="data[email]" value="{$data.email|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_phone' gid='users'}:&nbsp;*</div>
		<div class="v"><input type="text" name="data[phone]" value="{$data.phone|escape}" id="phone{$rand}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_password' gid='users'}:&nbsp;*</div>
		<div class="v"><input type="password" name="data[password]"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_repassword' gid='users'}:&nbsp;*</div>
		<div class="v"><input type="password" name="data[repassword]"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_region' gid='users'}: </div>
		<div class="v">{country_select select_type='city' id_country=$data.id_country id_region=$data.id_region id_city=$data.id_city id_district=$data.id_district var_country='data[id_country]' var_region='data[id_region]' var_city='data[id_city]' var_district='data[id_district]'}</div>
	</div>
	<div class="r">
		<div class="f">{l i='field_address' gid='users'}: </div>
		<div class="v"><input type="text" name="data[address]" value="{$data.address|escape}"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_postal_code' gid='users'}: </div>
		<div class="v"><input type="text" name="data[postal_code]" value="{$data.postal_code|escape}"></div>
	</div>
	<div class="r">
		<div class="f"><input type="checkbox" name="license_agreement" value="1" maxlength="{$data.show_contact_info}" id="license_agreement" /> <label for="license_agreement">{l i='field_license_agreement' gid='users'}</label></div>
		<div class="v"></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_captcha' gid='users'}:&nbsp;*</div>
		<div class="v captcha">{$data.captcha_image} <input type="text" name="captcha_confirmation" value="" maxlength="{$data.captcha_word_length}" /></div>
	</div>
	<div class="r">
		<div class="f">&nbsp;</div>
		<div class="v"><input type="submit" value="{l i='btn_register' gid='start' type='button'}" name="btn_register"></div>
	</div>
</form>

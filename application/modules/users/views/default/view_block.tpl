		{capture assign="review_callback"}{literal}
			function(data){
				window.location.href = '{/literal}{seolink module='users' method='view' data=$user section='reviews'}#m_reviews{literal}';
			}
		{/literal}{/capture}
		
		{if $user_type eq 'company'}<div itemscope itemtype="http://data-vocabulary.org/Organization">{/if}
		
		<h1>{seotag tag='header_text'}</h1>
		<div class="actions noPrint">
			{block name='button_contact' module='mailbox' user_id=$user.id user_type=$user_type}
			{block name='send_review_block' module='reviews' object_id=$user.id type_gid='users_object' responder_id=$user.id success=$review_callback is_owner=$is_user_owner}
			{block name='user_listings_button' module='listings' user=$user}
			<a href="{$site_url}users/pdf/{$user.id}" id="pdf_btn" class="btn-link link-r-margin" rel="nofollow" title="{l i='link_pdf' gid='listings' type='button'}"><ins class="with-icon i-pdf"></ins></a>
			<a href="javascript:void(0);" id="print_btn" class="btn-link link-r-margin" rel="nofollow" title="{l i='link_print' gid='listings' type='button'}" onclick="javascript: window.print(); return false;"><ins class="with-icon i-print"></ins></a>
			{block name='mark_as_spam_block' module='spam' object_id=$user.id type_gid='users_object' is_owner=$is_user_owner}
		</div>
		<div class="clr"></div>
		<div class="view_user">
			<div class="image">
				{l i='text_user_logo' gid='users' type='button' assign='text_user_logo' user_name=$user.output_name}
				<a href="{seolink module='users' method='view' data=$user}">
					<img src="{$user.media.user_logo.thumbs.big}" alt="{$text_user_logo}" title="{$user.output_name|escape}">
				</a>
			</div>
			<div class="body">
				{l i='no_information' gid='start' assign='no_info_str'}
				<div class="t-1">
					{depends module=reviews}
					{block name=get_rate_block module=reviews rating_data_main=$user.review_value type_gid='users_object' template='normal' read_only='true'}<br>
					<span>{l i='field_rate' gid='users'}:</span> {$user.review_value}<br>
					<span>{l i='field_reviews_count' gid='users'}:</span> {$user.review_count}<br>
					{if $user.review_count eq 0}{block name='send_review_block' module='reviews' object_id=$user.id type_gid='users_object' responder_id=$user.id success=$review_callback is_owner=$is_user_owner template='first'}{/if}<br>
					{/depends}
					{if $user.user_type eq "company" && $user.agent_count}<span>{l i='field_agent_count' gid='users'}:</span> {$user.agent_count}<br>{/if}
					{if $user.listings_for_sale_count}<span>{l i='field_listings_for_sale' gid='users'}:</span> <a href="{seolink module='listings' method='user' id_user=$user.id user=$user.output_name operation_type='sale'}">{$user.listings_for_sale_count}</a><br>{/if}
					{if $user.listings_for_buy_count}<span>{l i='field_listings_for_buy' gid='users'}:</span> <a href="{seolink module='listings' method='user' id_user=$user.id user=$user.output_name operation_type='buy'}">{$user.listings_for_buy_count}</a><br>{/if}
					{if $user.listings_for_rent_count}<span>{l i='field_listings_for_rent' gid='users'}:</span> <a href="{seolink module='listings' method='user' id_user=$user.id user=$user.output_name operation_type='rent'}">{$user.listings_for_rent_count}</a><br>{/if}
					{if $user.listings_for_lease_count}<span>{l i='field_listings_for_lease' gid='users'}:</span> <a href="{seolink module='listings' method='user' id_user=$user.id user=$user.output_name operation_type='lease'}">{$user.listings_for_lease_count}</a><br>{/if}
				</div>
				<div class="t-2">
					<span>{l i='field_register' gid='users'}:</span> {$user.date_created|date_format:$page_data.date_format}<br>
					<span>{l i='field_views' gid='users'}:</span> {$user.views}<br>
					<span class="status_text">{if $user.is_featured}{l i='status_featured' gid='users'}{/if}</span>
				</div>
			</div>
			<div class="clr"></div>
		</div>
		<div class="edit_block">
			{include file="view_menu.tpl" module=users theme=user}
			<div id="user_block">
				{*if $user.user_type eq 'agent'}<div id="content_m_company" class="view-section{if $section_gid ne 'company'} hide{/if} noPrint">{$user_content.company}</div>{/if*}
				<div id="content_m_reviews" class="view-section{if $section_gid ne 'reviews'} hide{/if} noPrint">{$user_content.reviews}</div>
				{if $user.user_type eq 'company'}<div id="content_m_map" class="view-section{if $section_gid ne 'map'} hide{/if} noPrint">{$user_content.map}</div>{/if}
				<div id="content_m_contacts" class="view-section{if $section_gid ne 'contacts'} hide{/if} print_block">{$user_content.contacts}</div>
				<div id="content_m_listings" class="view-section{if $section_gid ne 'listings'} hide{/if} noPrint">{$user_content.listings}</div>
			</div>
		</div>
		{if $user_type eq 'company'}</div>{/if}

<div class="view-moderate" id="user_logo_{$data.id}">
	<div class="moderate-big"></div>
	<div class="moderate-img"><img src="{$data.media.user_logo.thumbs.small}" width="50"></div>
	<div class="moderate-cont">{$data.fname} {$data.sname}<br></div>
</div>
<script>{literal}
	$(function(){
		$('#user_logo_{/literal}{$data.id}{literal}').bind('mouseenter', function(){
			$(this).find('.moderate-big').html('<img src="{/literal}{$data.media.user_logo.file_url}{literal}" width="300">')
		}).bind('mouseleave', function(){
			$(this).find('.moderate-big').html('');
		});
	});
{/literal}</script>

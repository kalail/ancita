{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_users_types_menu'}

<div class="actions">
	<ul>
		<li><div class="l"><a href="{$site_url}admin/users/add/{$user_type}">{l i='link_add_user' gid='users'}</a></div></li>
		{helper func_name=button_add_funds module=users_payments}
	</ul>
	&nbsp;
</div>

<div class="menu-level3">
	<ul>
		{foreach item=item from=$filters}
		{assign var='l' value='filter_'`$item`'_users'}
		<li class="{if $filter eq $item}active{/if}{if !$filter_data[$item]} hide{/if}"><a href="{$site_url}admin/users/index/{$user_type}/{$item}">{l i=$l gid='users'} ({$filter_data[$item]})</a></li>
		{/foreach}
	</ul>
	&nbsp;
</div>

<form method="post" action="" name="save_form" enctype="multipart/form-data">
<div class="filter-form">
	<div class="form">
		<div class="row">
			<div class="h">{l i='field_keyword' gid='users'}:</div>
			<div class="v">
				<input type="text" name="keyword" value="{$page_data.filter.keyword_all|escape}" />
			</div>
		</div>
		<div class="row">
			<div class="h">
				<input type="submit" name="filter-submit" value="{l i='header_filters' gid='users' type='button'}">
				<input type="submit" name="filter-reset" value="{l i='header_reset' gid='users' type='button'}">
			</div>
		</div>
	</div>
</div>
</form>

<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first"><input type="checkbox" id="grouping_all"></th>
	<th class="w150"><a href="{$sort_links.name'}"{if $order eq 'name'} class="{$order_direction|lower}"{/if}>{l i='field_name' gid='users'}</a></th>
	<th class="w150"><a href="{$sort_links.email}"{if $order eq 'email'} class="{$order_direction|lower}"{/if}>{l i='field_email' gid='users'}</a></th>
	<th><a href="{$sort_links.account}"{if $order eq 'account'} class="{$order_direction|lower}"{/if}>{l i='field_account' gid='users'}</a></th>
	{depends module=listings}
	<th class="w30"><a href="{$sort_links.listings}"{if $order eq 'listings'} class="{$order_direction|lower}"{/if}>{l i='field_listings_actived' gid='users'}</a></th>
	{/depends}
	{depends module=reviews}
	<th class="w30"><a href="{$sort_links.reviews}"{if $order eq 'reviews'} class="{$order_direction|lower}"{/if}>{l i='field_reviews_count' gid='users'}</a></th>
	{/depends}
	<th class="w100"><a href="{$sort_links.date_created}"{if $order eq 'date_created'} class="{$order_direction|lower}"{/if}>{l i='field_date_created' gid='users'}</a></th>
    <th class="w100"><a href="{$sort_links.date_login}"{if $order eq 'date_login'} class="{$order_direction|lower}"{/if}>{l i='field_last_login' gid='users'}</a></th>
    <th class="w30">{l i='field_days_since_login' gid='users'}</th>
    <th class="w30">{l i='field_days_sice_member' gid='users'}</th>
	<th class="w150">&nbsp;</th>
</tr>
{foreach item=item from=$users}
{counter print=false assign=counter}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td class="first w20 center"><input type="checkbox" class="grouping" value="{$item.id}"></td>
	<td>{$item.output_name|truncate:100}</td>
	<td>{$item.email|truncate:50}</td>
	<td class="center">{block name=currency_format_output module=start value=$item.account cur_gid='USD'}</td>
	{depends module=listings}
	<td class="center"><a href="{$site_url}admin/listings/user/{$item.id}">{math equation="sale+buy+rent+lease" sale=$item.listings_for_sale_count buy=$item.listings_for_buy_count rent=$item.listings_for_rent_count lease=$item.listings_for_lease_count}</a></td>
	{/depends}
	{depends module=reviews}
	<td class="center"><a href="{$site_url}admin/reviews/index/users_object/{$item.id}">{$item.review_count}</a></td>
	{/depends}
	<td class="center">{$item.date_created|date_format:$page_data.date_format}</td>
    <td class="center">{$item.date_login|date_format:$page_data.date_format}</td>
    <td class="center">{$item.since_login}</td>
    <td class="center">{$item.sice_member}</td>
	<td class="icons">
		{if $item.status}
		<a href="{$site_url}admin/users/activate/{$item.id}/0"><img src="{$site_root}{$img_folder}icon-full.png" width="16" height="16" border="0" alt="{l i='link_deactivate_user' gid='users' type='button'}" title="{l i='link_deactivate_user' gid='users' type='button'}"></a>
		{else}
		<a href="{$site_url}admin/users/activate/{$item.id}/1"><img src="{$site_root}{$img_folder}icon-empty.png" width="16" height="16" border="0" alt="{l i='link_activate_user' gid='users' type='button'}" title="{l i='link_activate_user' gid='users' type='button'}"></a>
		{/if}
		<a href="{$site_url}admin/users/edit/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_user' gid='users' type='button'}" title="{l i='link_edit_user' gid='users' type='button'}"></a>
		<a href="{$site_url}admin/users/delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_user' gid='users' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_user' gid='users' type='button'}" title="{l i='link_delete_user' gid='users' type='button'}"></a>
	</td>
</tr>
{foreachelse}
{assign var='colspan' value=8}
{depends module=listings}{assign var='colspan' value=$colspan+1}{/depends}
{depends module=reviews}{assign var='colspan' value=$colspan+1}{/depends}
<tr><td colspan="{$colspan}" class="center">{l i='no_users' gid='users'}</td></tr>
{/foreach}
</table>
{include file="pagination.tpl"}

<script type="text/javascript">{literal}
var reload_link = "{/literal}{$site_url}admin/users/index/{literal}";
var filter = '{/literal}{$filter}{literal}';
var order = '{/literal}{$order}{literal}';
var loading_content;
var order_direction = '{/literal}{$order_direction}{literal}';
$(function(){
	$('#grouping_all').bind('click', function(){
		var checked = $(this).is(':checked');
		if(checked){
			$('input[type=checkbox].grouping').prop('checked', true);
		}else{
			$('input[type=checkbox].grouping').prop('checked', false);
		}
	});
});

function reload_this_page(value){
	var link = reload_link + value + '/' + filter + '/' + order + '/' + order_direction;
	location.href=link;
}
{/literal}</script>

{include file="footer.tpl"}

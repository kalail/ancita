{include file="header.tpl"}

{include file="edit_users_menu.tpl" module=users}

{if $section_gid eq 'contacts'}
	{include file=`$user_type`"_admin_form.tpl" module=users theme=admin}
{elseif $section_gid eq 'seo'}
	{depends module=seo}
	{foreach item=section key=key from=$seo_fields}
	<form method="post" action="{$data.action|escape}" name="seo_{$section.gid}_form">
	<div class="edit-form n150">
		<div class="row header">{$section.name}</div>
		{if $section.tooltip}
		<div class="row">
			<div class="h">&nbsp;</div>
			<div class="v">{$section.tooltip}</div>
		</div>
		{/if}		
		{foreach item=field from=$section.fields}
		<div class="row">
			<div class="h">{$field.name}: </div>
			<div class="v">
				{assign var='field_gid' value=$field.gid}
				{switch from=$field.type}
					{case value='checkbox'}
						<input type="hidden" name="{$section.gid}[{$field_gid}]" value="0">
						<input type="checkbox" name="{$section.gid}[{$field_gid}]" value="1" {if $seo_settings[$field_gid]}checked{/if}>
					{case value='text'}
						{foreach item=lang_item key=lang_id from=$languages}
						{assign var='section_gid' value=$section.gid+'_'+$lang_id}
						<input type="{if $lang_id eq $current_lang_id}text{else}hidden{/if}" name="{$section.gid}[{$field_gid}][{$lang_id}]" value="{$seo_settings[$section_gid][$field_gid]|escape}" class="long" lang-editor="value" lang-editor-type="{$section.gid}_{$field_gid}" lang-editor-lid="{$lang_id}">
						{/foreach}
						<a href="#" lang-editor="button" lang-editor-type="{$section.gid}_{$field_gid}"><img src="{$site_root}{$img_folder}icon-translate.png" width="16" height="16" alt="{l i='note_types_translate' gid='reviews' type='button'}" title="{l i='note_types_translate' gid='reviews' type='button'}"></a>
					{case value='textarea'}
						{foreach item=lang_item key=lang_id from=$languages}
							{assign var='section_gid' value=$section.gid+'_'+$lang_id}
							{if $lang_id eq $current_lang_id}
							<textarea name="{$section.gid}[{$field_gid}][{$lang_id}]" rows="5" cols="80" class="long" lang-editor="value" lang-editor-type="{$section.gid}_{$field_gid}" lang-editor-lid="{$lang_id}">{$seo_settings[$section_gid][$field_gid]|escape}</textarea>
							{else}
							<input type="hidden" name="{$section.gid}[{$field_gid}][{$lang_id}]" value="{$seo_settings[$section_gid][$field_gid][$lang_id]|escape}">
							{/if}
						{/foreach}
						<a href="#" lang-editor="button" lang-editor-type="{$section.gid}_{$field.gid}" lang-field-type="textarea"><img src="{$site_root}{$img_folder}icon-translate.png" width="16" height="16" alt="{l i='note_types_translate' gid='reviews' type='button'}" title="{l i='note_types_translate' gid='reviews' type='button'}"></a>					
				{/switch}<br>{$field.tooltip}
			</div>
		</div>
		{/foreach}	
	</div>	
	<div class="btn"><div class="l"><input type="submit" name="btn_save_{$section.gid}" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$back_url}">{l i='btn_cancel' gid='start'}</a>	
	<input type="hidden" name="btn_save" value="1">
	</form>
	<div class="clr"></div>
	{/foreach}
	{block name=lang_inline_editor module=start}
	{/depends}
{/if}

{include file="footer.tpl"}

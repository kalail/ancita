{if $data.id}
{depends module=seo}
<div class="menu-level3">
	<ul>
		<li class="{if $section_gid eq 'contacts'}active{/if}"><a href="{$site_url}admin/users/edit/{$data.id}/contacts">{l i='filter_section_contacts' gid='users'}</a></li>
		<li class="{if $section_gid eq 'seo'}active{/if}"><a href="{$site_url}admin/users/edit/{$data.id}/seo">{l i='filter_section_seo' gid='seo'}</a></li>
	</ul>
	&nbsp;
</div>
{/depends}
{/if}

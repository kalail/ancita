<div class="row">
	<div class="h">{l i='field_user_type' gid='users'}</div>
	<div class="v">
		<select name="filters[type]">
			<option value="">...</option>
			{foreach item=item from=$user_types}
			<option value="{$item|escape}" {if $item eq $data.type}selected{/if}>{l i=$item gid='users'}</option>
			{/foreach}
		</select>
	</div>
</div>
<div class="row">
	<div class="h">{l i='field_keyword' gid='users'}:</div>
	<div class="v"><input type="text" name="filters[keyword]" value="{$data.keyword|escape}"></div>
</div>
<input type="hidden" name="form" value="admin_export_form">
<script>{literal}
	$(function(){
		$('div.row:odd').addClass('zebra');
	});
{/literal}</script>

{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_users_types_menu'}
<div class="actions">&nbsp;</div>

<form action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%" id="login_settings">
<tr>
	<th class="first">{l i='field_user_type' gid='users'}</th>
	<th class="w100">{l i='field_login_enabled' gid='users'}</th>
	<th class="w100">{l i='field_register_enabled' gid='users'}</th>
	<th class="w100">{l i='field_register_mail_confirm' gid='users'}</th>
	<th class="w100">{l i='field_search_enabled' gid='users'}</th>
</tr>
{foreach item=item key=key from=$data}
{counter print=false assign=counter}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td>{ld_option i='user_type' gid='users' option=$key}: </td>
	<td align="center">
		<input type="hidden" name="data[{$key}_login_enabled]" value="0" />
		<input type="checkbox" name="data[{$key}_login_enabled]" utype="{$key}" atype="login" id="login_{$key}" value="1" autocomplete="off" {if $item.login_enabled}checked{/if} />
	</td>	
	<td align="center">
		<input type="hidden" name="data[{$key}_register_enabled]" value="0" />
		<input type="checkbox" name="data[{$key}_register_enabled]" utype="{$key}" atype="register" id="register_{$key}" value="1" autocomplete="off" {if $item.register_enabled}checked{/if} {if !$item.login_enabled}disabled{/if} />
	</td>
	<td align="center">
		<input type="hidden" name="data[{$key}_register_mail_confirm]" value="0" />
		<input type="checkbox" name="data[{$key}_register_mail_confirm]" utype="{$key}" atype="mail_confirm" id="mail_confirm_{$key}" value="1" autocomplete="off" {if $item.register_mail_confirm}checked{/if} {if !$item.register_enabled || !$item.login_enabled}disabled{/if} />
	</td>
	<td align="center">
		<input type="hidden" name="data[{$key}_search_enabled]" value="0" />
		<input type="checkbox" name="data[{$key}_search_enabled]" utype="{$key}" atype="search_enabled" id="search_enabled_{$key}" value="1" autocomplete="off" {if $item.search_enabled}checked{/if} />
	</td>
</tr>			
{/foreach}
</table>

<div class="btn"><div class="l"><input type="submit" name="btn_auth_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	
</form>
<div class="clr"></div>

<script type="text/javascript">{literal}
$(function(){
	$('#login_settings').find('input[type=checkbox][atype=login]').bind('click', function(){
		var user_type = $(this).attr('utype');
		var checkbox = $('#register_'+user_type);
		if($(this).is(':checked')){
			checkbox.removeAttr('disabled');
		}else{
			checkbox.removeAttr('checked');
			checkbox.attr('disabled', 'disabled');
		}
		checkbox.trigger('click');
	});
	
	$('#login_settings').find('input[type=checkbox][atype=register]').bind('click', function(){
		var user_type = $(this).attr('utype');
		var checkbox = $('#mail_confirm_'+user_type);
		if($(this).is(':checked')){
			checkbox.removeAttr('disabled');
		}else{
			checkbox.removeAttr('checked');
			checkbox.attr('disabled', 'disabled');
		}
	});
});
{/literal}</script>


{include file="footer.tpl"}
<?php

$install_lang["admin_menu_settings_items_uploads-items_video_menu_item"] = "Video";
$install_lang["admin_menu_settings_items_uploads-items_video_menu_item_tooltip"] = "Video files size and storage ";
$install_lang["admin_video_menu_configs_list_item"] = "Upload settings";
$install_lang["admin_video_menu_configs_list_item_tooltip"] = "";
$install_lang["admin_video_menu_system_list_item"] = "System settings";
$install_lang["admin_video_menu_system_list_item_tooltip"] = "";
$install_lang["admin_video_menu_youtube_settings_item"] = "Youtube settings";
$install_lang["admin_video_menu_youtube_settings_item_tooltip"] = "";


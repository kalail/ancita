<?php

$install_lang["admin_menu_settings_items_uploads-items_video_menu_item"] = "Vídeo";
$install_lang["admin_menu_settings_items_uploads-items_video_menu_item_tooltip"] = "Tamaño y almacenamiento de archivos de vídeo";
$install_lang["admin_video_menu_configs_list_item"] = "Configurar carga";
$install_lang["admin_video_menu_configs_list_item_tooltip"] = "";
$install_lang["admin_video_menu_system_list_item"] = "Configuración del sistema";
$install_lang["admin_video_menu_system_list_item_tooltip"] = "";
$install_lang["admin_video_menu_youtube_settings_item"] = "Configuración de Youtube";
$install_lang["admin_video_menu_youtube_settings_item_tooltip"] = "";


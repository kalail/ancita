<?php

$install_lang["admin_menu_settings_items_uploads-items_video_menu_item"] = "Vidéo";
$install_lang["admin_menu_settings_items_uploads-items_video_menu_item_tooltip"] = "Taille et rangement de filières vidéo ";
$install_lang["admin_video_menu_configs_list_item"] = "Paramètres de téléchargement";
$install_lang["admin_video_menu_configs_list_item_tooltip"] = "";
$install_lang["admin_video_menu_system_list_item"] = "Paramètres de système";
$install_lang["admin_video_menu_system_list_item_tooltip"] = "";
$install_lang["admin_video_menu_youtube_settings_item"] = "Paramètres de Youtube";
$install_lang["admin_video_menu_youtube_settings_item_tooltip"] = "";


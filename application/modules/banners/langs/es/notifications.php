<?php

$install_lang["notification_banner_need_moderate"] = "Nuevo banner en espera de moderación";
$install_lang["notification_banner_status_expired"] = "Banner status expired";
$install_lang["notification_banner_status_updated"] = "Estado Banner actualizado";
$install_lang["tpl_banner_need_moderate_content"] = "Hola admin,\n\nHay un nuevo banner esperando moderación en [domain]. Para verlo,vaya a panel de administración > Banners > banners de usuario.\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_banner_need_moderate_subject"] = "[domain] | Nuevo banner esperando moderación";
$install_lang["tpl_banner_status_expired_content"] = "Hola [user],\n\nYour banner status is expired.\n[banner]\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_banner_status_expired_subject"] = "[domain] | Banner status expired";
$install_lang["tpl_banner_status_updated_content"] = "Hola [user],\n\nEstado actualziado de su banner.\n[banner] - [status]\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_banner_status_updated_subject"] = "[domain] | estado de banner actualizado";

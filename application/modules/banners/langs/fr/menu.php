<?php

$install_lang["admin_banners_menu_banners_list_item"] = "Bannières";
$install_lang["admin_banners_menu_banners_list_item_tooltip"] = "";
$install_lang["admin_banners_menu_banners_settings_item"] = "Paramètres";
$install_lang["admin_banners_menu_banners_settings_item_tooltip"] = "";
$install_lang["admin_banners_menu_groups_list_item"] = "Groupes de pages";
$install_lang["admin_banners_menu_groups_list_item_tooltip"] = "";
$install_lang["admin_banners_menu_places_list_item"] = "Positions";
$install_lang["admin_banners_menu_places_list_item_tooltip"] = "";
$install_lang["admin_menu_other_items_banners_menu_item"] = "Annonces style bannière";
$install_lang["admin_menu_other_items_banners_menu_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_my_banners_item"] = "Bannières";
$install_lang["agent_account_menu_agent_my_banners_item_tooltip"] = "";
$install_lang["company_account_menu_company_my_banners_item"] = "Bannières";
$install_lang["company_account_menu_company_my_banners_item_tooltip"] = "";
$install_lang["private_account_menu_private_my_banners_item"] = "Bannières";
$install_lang["private_account_menu_private_my_banners_item_tooltip"] = "";
$install_lang["agent_top_menu_agent-main-banners-item"] = "Bannières";
$install_lang["agent_top_menu_agent-main-banners-item_tooltip"] = "";
$install_lang["company_top_menu_company-main-banners-item"] = "Bannières";
$install_lang["company_top_menu_company-main-banners-item_tooltip"] = "";
$install_lang["private_top_menu_private-main-banners-item"] = "Bannières";
$install_lang["private_top_menu_private-main-banners-item_tooltip"] = "";


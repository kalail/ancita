<?php

$install_lang["notification_banner_need_moderate"] = "Nouvelle bannière en attente d'approbation";
$install_lang["notification_banner_status_expired"] = "Banner status expired";
$install_lang["notification_banner_status_updated"] = "Statut de bannière mis à jour";
$install_lang["tpl_banner_need_moderate_content"] = "Bonjour admin,\n\nIl a une nouvelle bannière en attente d'approbation sur [domain]. Pour le voir, visitez le panneau administratif et vérifiez Bannières > Bannières d'utilisateur.\n\nCordialement,\n[name_from]";
$install_lang["tpl_banner_need_moderate_subject"] = "[domain] | Nouvelle bannière en attente d'approbation";
$install_lang["tpl_banner_status_expired_content"] = "Bonjour [user],\n\nYour banner status is expired.\n[banner]\n\nCordialement,\n[name_from]";
$install_lang["tpl_banner_status_expired_subject"] = "[domain] | Banner status expired";
$install_lang["tpl_banner_status_updated_content"] = "Bonjour [user],\n\nLe statut de votre bannière est mis à jour.\n[banner] - [status]\n\nCordialement,\n[name_from]";
$install_lang["tpl_banner_status_updated_subject"] = "[domain] | Statut de bannière mis à jour";

<?php

$install_lang["active"] = "Actif";
$install_lang["admin_header_banner_change"] = "Éditer bannière";
$install_lang["admin_header_banner_view"] = "Banner settings";
$install_lang["admin_header_banners_add"] = "Ajouter bannière";
$install_lang["admin_header_banners_list"] = "Bannières";
$install_lang["admin_header_free_pages"] = "Choisir pages";
$install_lang["admin_header_group_add"] = "Ajouter nouveau groupe";
$install_lang["admin_header_group_change"] = "Éditer groupe";
$install_lang["admin_header_groups_list"] = "Bannières";
$install_lang["admin_header_pages_in_group"] = "Pages de groupes";
$install_lang["admin_header_place_add"] = "Ajouter nouvelle position";
$install_lang["admin_header_place_change"] = "Éditer position de bannière";
$install_lang["admin_header_places_list"] = "Bannières";
$install_lang["admin_header_settings"] = "Banners : Settings";
$install_lang["admin_header_stat_list"] = "Bannières : statistiques : ";
$install_lang["api_error_empty_banner_id"] = "ID de bannière vide";
$install_lang["api_error_empty_list"] = "Aucune bannière";
$install_lang["api_error_empty_user_id"] = "ID d'utilisateur vide";
$install_lang["api_error_not_owner"] = "Vous n'êtes pas le propriétaire de la bannière";
$install_lang["api_error_wrong_auth_type"] = "Type d'authorization invalide";
$install_lang["api_error_wrong_banner_id"] = "ID de bannière invalide";
$install_lang["approved"] = "Approuvé";
$install_lang["banner_edit_error_alt_text_empty"] = "Text alt vide";
$install_lang["banner_edit_error_filename_empty"] = "Attachez une image SVP";
$install_lang["banner_edit_error_html_empty"] = "Champ HTML est requis";
$install_lang["banner_edit_error_link_empty"] = "Lien vide";
$install_lang["banner_edit_error_name_empty"] = "Nom vide";
$install_lang["banner_edit_error_place_empty"] = "Position est un champ requis";
$install_lang["banner_edit_error_type_empty"] = "Type de bannière est vide";
$install_lang["banner_place"] = "Position";
$install_lang["banner_statistic_update_success"] = "Statistiques de bannière mises à jour";
$install_lang["banners_group_content_groups"] = "Pages d'info";
$install_lang["banners_group_listings_groups"] = "Pages d'annonces";
$install_lang["banners_group_start_groups"] = "Page d'accueil";
$install_lang["banners_group_users_groups"] = "Page d'utilisateurs";
$install_lang["clicks"] = "Clics";
$install_lang["clicks_left"] = "Clics restants";
$install_lang["declined"] = "Décliné";
$install_lang["error_empty_activate_banner_sum"] = "SVP choisir une position";
$install_lang["error_empty_email"] = "Courriel vide";
$install_lang["error_empty_period"] = "Période d'activité vide";
$install_lang["error_empty_service_activate_service"] = "Activation de bannière temporairement indisponible";
$install_lang["error_group_gid_empty"] = "GID de groupe vide";
$install_lang["error_group_gid_exists"] = "Groupe avec ce GID existe déja";
$install_lang["error_group_name_empty"] = "Nom de groupe vide";
$install_lang["error_group_name_exists"] = "Groupe avec ce nom existe déja";
$install_lang["error_group_price_empty"] = "Prix de groupe vide";
$install_lang["error_invalid_email"] = "Courriel invalide";
$install_lang["field_access"] = "Qui peut ajouter des bannières";
$install_lang["field_actions"] = "Actions";
$install_lang["field_admin_moderation_emails"] = "Courriels";
$install_lang["field_alt_text"] = "Texte alternatif";
$install_lang["field_approve"] = "Statut de modération";
$install_lang["field_expiration_date"] = "Terminera le";
$install_lang["field_free_places"] = "Positions disponibles";
$install_lang["field_group_gid"] = "GID";
$install_lang["field_group_id"] = "ID";
$install_lang["field_group_name"] = "Nom de groupe";
$install_lang["field_group_price"] = "Prix";
$install_lang["field_groups"] = "Groupes";
$install_lang["field_html"] = "HTML";
$install_lang["field_image"] = "Image";
$install_lang["field_image_delete"] = "Supprimer filière";
$install_lang["field_limitations"] = "Limites";
$install_lang["field_link"] = "Lien";
$install_lang["field_location"] = "Position";
$install_lang["field_moderation_send_mail"] = "Envoyer une notification au courriel";
$install_lang["field_name"] = "Nom";
$install_lang["field_new_window"] = "Ouvrir dans une nouvelle fenêtre";
$install_lang["field_not_in_ratation"] = "Aucune rotation";
$install_lang["field_number"] = "N/N";
$install_lang["field_number_of_clicks"] = "Nombre de clics";
$install_lang["field_number_of_views"] = "Nombre de vues";
$install_lang["field_page"] = "Position";
$install_lang["field_period"] = "Période, jours";
$install_lang["field_place_height"] = "Hauteur";
$install_lang["field_place_id"] = "ID";
$install_lang["field_place_in_rotation"] = "Bannières en rotation";
$install_lang["field_place_keyword"] = "Mot clé";
$install_lang["field_place_name"] = "Nom";
$install_lang["field_place_numbers"] = "Choisir position";
$install_lang["field_place_price"] = "Prix pour une position";
$install_lang["field_place_rotate_time"] = "Période de rotation";
$install_lang["field_place_sizes"] = "Taille";
$install_lang["field_place_width"] = "Largeur";
$install_lang["field_status"] = "Activité";
$install_lang["field_total_price"] = "Total";
$install_lang["field_type"] = "Type de bannière";
$install_lang["field_user"] = "User";
$install_lang["filter_admin_banners"] = "Bannières d'admin";
$install_lang["filter_users_banners"] = "Bannières d'utilisateur";
$install_lang["header_banner_form"] = "Ajouter bannière";
$install_lang["header_my_banner_activate"] = "Activer bannière";
$install_lang["header_my_banner_statistic"] = "Statistiques de bannières";
$install_lang["header_my_banners"] = "Mes bannières";
$install_lang["header_select_module"] = "Module";
$install_lang["link_activate_banner"] = "Activer bannière";
$install_lang["link_add_banner"] = "Ajouter bannière";
$install_lang["link_add_group"] = "Ajouter nouveau groupe";
$install_lang["link_add_place"] = "Ajouter nouvelle position";
$install_lang["link_back_to_my_banners"] = "Retour à mes bannières";
$install_lang["link_banner_activate"] = "Activer";
$install_lang["link_banner_approve"] = "Approuver bannière";
$install_lang["link_banner_decline"] = "Décliner bannière";
$install_lang["link_banner_delete"] = "Supprimer";
$install_lang["link_banner_stat"] = "Statistiques";
$install_lang["link_deactivate_banner"] = "Désactiver bannières";
$install_lang["link_delete_banner"] = "Supprimer bannière";
$install_lang["link_delete_group"] = "Supprimer groupe";
$install_lang["link_delete_place"] = "Supprimer position";
$install_lang["link_edit_banner"] = "Éditer bannière";
$install_lang["link_edit_group"] = "Éditer groupe";
$install_lang["link_edit_place"] = "Éditer position";
$install_lang["link_view_banner"] = "Voir";
$install_lang["link_view_statistic"] = "Voir statistiques";
$install_lang["never_stop"] = "Jamais arrêter";
$install_lang["no_banners"] = "Aucune bannière";
$install_lang["no_groups"] = "Aucun groupe";
$install_lang["no_place"] = "Aucune positions";
$install_lang["no_rotation"] = "Aucune rotation";
$install_lang["not_active"] = "Inactif";
$install_lang["not_approved"] = "En attente d'approbation";
$install_lang["note_delete_banner"] = "Êtes vous sur de vouloir supprimer cette bannière?";
$install_lang["note_delete_group"] = "Êtes vous sur de vouloir supprimer ce groupe de pages?";
$install_lang["note_delete_place"] = "Êtes vous sur de vouloir supprimer cette position?";
$install_lang["page_used_in_group"] = "Page utilisé sur";
$install_lang["pages_list"] = "Groupe de pages";
$install_lang["place_edit_error_height_empty"] = "Hauteur vide";
$install_lang["place_edit_error_keyword_empty"] = "Mot clé vide";
$install_lang["place_edit_error_keyword_exists"] = "Position avec ce mot clé existe déja";
$install_lang["place_edit_error_name_empty"] = "Nom vide";
$install_lang["place_edit_error_places_in_rotation_empty"] = "Places en rotation devrait être plus que 0";
$install_lang["place_edit_error_rotate_time_empty"] = "Période de rotation devrait être plus que 0";
$install_lang["place_edit_error_width_empty"] = "Largeur vide";
$install_lang["show_statistic"] = "Statistiques pour ";
$install_lang["shows"] = "Hits";
$install_lang["shows_left"] = "Hits restants";
$install_lang["stat_by_days"] = "Par jours";
$install_lang["stat_by_hours"] = "Par heures";
$install_lang["stat_by_month"] = "Par mois";
$install_lang["stat_by_weeks"] = "Par semaines";
$install_lang["stat_clicks"] = "Clics";
$install_lang["stat_day"] = "Jour";
$install_lang["stat_header_banners"] = "Bannières";
$install_lang["stat_header_moderation_banners"] = "Bannières d'utilisateurs en attente d'approbation";
$install_lang["stat_hour"] = "Heure";
$install_lang["stat_month"] = "Mois";
$install_lang["stat_overall_clicks"] = "Tous clics";
$install_lang["stat_overall_views"] = "Toutes vues";
$install_lang["stat_view_type"] = "Type de vue";
$install_lang["stat_views"] = "Vues";
$install_lang["stat_week"] = "Semaine";
$install_lang["stat_year"] = "Année";
$install_lang["success_banner_approved"] = "Banner is successfully approved";
$install_lang["success_banner_declined"] = "Banner is successfully declined";
$install_lang["success_delete_banner"] = "Bannière supprimée";
$install_lang["success_settings_saved"] = "Paramètres sauvés";
$install_lang["success_update_banner_data"] = "Bannières sauvée";
$install_lang["success_update_group_data"] = "Groupe sauvé";
$install_lang["success_update_place_data"] = "Position mise à jour";
$install_lang["text_banner_activated"] = "Activated";
$install_lang["text_banner_form"] = "Ajouter nouvelle bannière";
$install_lang["text_banner_inactivated"] = "Inactivated";
$install_lang["text_my_banner_activate"] = "Choisissez des pages sur lesquelles les bannières devraient être montrées et les positions que vous aimerez acheter sur cette page";
$install_lang["text_my_banner_statistic"] = "Statistiques de vues";
$install_lang["text_my_banners"] = "Gérer bannières";
$install_lang["till"] = "jusqu'à";
$install_lang["update_statistic_manually"] = "Mettre statistiques à jour";


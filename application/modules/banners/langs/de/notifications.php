<?php

$install_lang["notification_banner_need_moderate"] = "Neue Banner warten auf Moderation";
$install_lang["notification_banner_status_expired"] = "Banner status expired";
$install_lang["notification_banner_status_updated"] = "Banner Status aktualisert";
$install_lang["tpl_banner_need_moderate_content"] = "Hallo admin,\n\nEs gibt einen neuen Banner der auf Moderation wartet auf [domain]. Um ihn anzusehen, gehen Sie zu Administrationsbereich> Banner> User Banner.\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_banner_need_moderate_subject"] = "[domain] | Neue Banner wartet auf Moderation";
$install_lang["tpl_banner_status_expired_content"] = "Hello [user],\n\nYour banner status is expired.\n[banner]\n\nBest regards,\n[name_from]";
$install_lang["tpl_banner_status_expired_subject"] = "[domain] | Banner status expired";
$install_lang["tpl_banner_status_updated_content"] = "Hallo [user],\n\nIhr Banner Status wird aktualisiert.\n[banner] - [status]\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_banner_status_updated_subject"] = "[domain] | Banner Status akutalisiert";

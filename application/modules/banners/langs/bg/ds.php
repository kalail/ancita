<?php

$install_lang["place_access"]["header"] = "Позицията е достъпна от";
$install_lang["place_access"]["option"]["1"] = "Всички потребители";
$install_lang["place_access"]["option"]["2"] = "Само от администратора";

$install_lang["banner_type"]["header"] = "Тип на банера";
$install_lang["banner_type"]["option"]["1"] = "Изображение";
$install_lang["banner_type"]["option"]["2"] = "HTML";



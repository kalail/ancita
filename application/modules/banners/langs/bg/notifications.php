<?php

$install_lang["notification_banner_need_moderate"] = "Новият банер очаква проверка";
$install_lang["notification_banner_status_expired"] = "Баннер деактивирован";
$install_lang["notification_banner_status_updated"] = "Статусът на банера е обновен";
$install_lang["tpl_banner_need_moderate_content"] = "Здравствуйте, администратор!\n\nИма нов банер за одобряване на сайта [domain]. За преглед влезте в панела на администратора > Банери > Банери на потребители.\n\nС уважение,\n[name_from]";
$install_lang["tpl_banner_need_moderate_subject"] = "[domain] |Нов банер за одобряване";
$install_lang["tpl_banner_status_expired_content"] = "Здравствуйте, [user],\n\nВаш баннер был деактивирован.\n[banner]\n\nС уважением,\n[name_from]";
$install_lang["tpl_banner_status_expired_subject"] = "[domain] | Баннер декативирован";
$install_lang["tpl_banner_status_updated_content"] = "Здравейте, [user],\n\nСтатусът на вашия банер е обновен.\n[banner] - [status]\n\nС уважение,\n[name_from]";
$install_lang["tpl_banner_status_updated_subject"] = "[domain] | Обновен статус на банера";

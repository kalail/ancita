{include file="header.tpl"}


<form method="post" action="{$data.action}" name="save_form">
<div class="edit-form n150">
	<div class="row header">{l i='admin_header_template_edit' gid='upload_gallery'}</div>
	<div class="row">
		<div class="h">{l i='field_gid' gid='upload_gallery'}: </div>
		<div class="v">{if $allow_add}<input type="text" value="{$data.gid}" name="data[gid]">{else}{$data.gid}{/if}</div>
	</div>
	<div class="row zebra">
		<div class="h">{l i='field_name' gid='upload_gallery'}:&nbsp;* </div>
		<div class="v"><input type="text" value="{$data.name|escape}" name="data[name]"></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_upload_config' gid='upload_gallery'}:&nbsp;* </div>
		<div class="v">
			<select name="data[gid_upload_config]">
			{foreach item=item from=$upload_config}<option value="{$item.gid}" {if $data.gid_upload_config eq $item.gid}selected{/if}>{$item.name}</option>{/foreach}
			</select>
		</div>
	</div>
	<div class="row zebra">
		<div class="h">{l i='field_max_items' gid='upload_gallery'}: </div>
		<div class="v"><input type="text" value="{$data.max_items_count}" name="data[max_items_count]" class="short"> <i>{l i='field_max_items_comment' gid='upload_gallery'}</i></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_use_moderation' gid='upload_gallery'}: </div>
		<div class="v"><input type="checkbox" {if $data.use_moderation}{/if} name="data[use_moderation]" value="1"></div>
	</div>

	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/upload_gallery/index">{l i='btn_cancel' gid='start'}</a>
</div>
</form>

<div class="clr"></div>
{include file="footer.tpl"}

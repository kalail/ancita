<div class="view-moderate" id="upload_{$data.id}">
<div class="moderate-big"></div>
<div class="moderate-img"><img src="{$data.media.thumbs.small}" align="left" hspace="3" width="50" /></div>
<div class="moderate-cont"><b>{$data.type_data.name}</b><br>{$data.comment}</div>
</div>
<script>{literal}
	$(function(){
		$('#upload_{/literal}{$data.id}{literal}').bind('mouseenter', function(){
			$(this).find('.moderate-big').html('<img src="{/literal}{$data.media.file_url}{literal}" width="300">')
		}).bind('mouseleave', function(){
			$(this).find('.moderate-big').html('');
		});
	});
{/literal}</script>

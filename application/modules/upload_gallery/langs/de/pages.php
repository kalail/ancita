<?php

$install_lang["admin_header_edit"] = "Fotoalbum";
$install_lang["admin_header_list"] = "Fotoalbum";
$install_lang["admin_header_template_edit"] = "Gallerietyp bearbeiten";
$install_lang["error_badwords_comment"] = "Schimpfwörter Fehler";
$install_lang["error_gid_empty"] = "Keywort ist ein erforderliches Feld";
$install_lang["error_max_items_count"] = "Sie haben die max Anzahl von Dateien erreicht";
$install_lang["error_name_empty"] = "Name ist ein erforderliches Feld";
$install_lang["field_date_add"] = "Datum hinzugefügt";
$install_lang["field_gid"] = "Keywort";
$install_lang["field_max_items"] = "Fotos max. Anzahl";
$install_lang["field_max_items_comment"] = "(0 – unbegrenzt)";
$install_lang["field_name"] = "Name";
$install_lang["field_upload_config"] = "Konfiguration hochladen";
$install_lang["field_use_moderation"] = "mäßig";
$install_lang["items_count_unlimit"] = "unbegrenzt";
$install_lang["link_add_gallery"] = "Gallerietyp hinzufügen";
$install_lang["link_delete_type"] = "löschen";
$install_lang["link_edit_type"] = "bearbeiten";
$install_lang["moderation_off"] = "aus";
$install_lang["moderation_on"] = "an";
$install_lang["no_upload_gallery"] = "Noch keine Gallerietypen";
$install_lang["note_delete_type"] = "Sind Sie sicher, dass Sie diesen Eintrag löschen wollen?";
$install_lang["success_add_type"] = "Typ ist erfolgreich hinzugefügt";
$install_lang["success_update_type"] = "Typ ist erfolgreich aktualisiert";


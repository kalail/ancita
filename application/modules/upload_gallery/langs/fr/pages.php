<?php

$install_lang["admin_header_edit"] = "Album de photos";
$install_lang["admin_header_list"] = "Album de photos";
$install_lang["admin_header_template_edit"] = "Éditer type de galerie";
$install_lang["error_badwords_comment"] = "Erreur: jurons";
$install_lang["error_gid_empty"] = "Mot clé est un champ requis";
$install_lang["error_max_items_count"] = "Vous avez le nombre maximum de filières";
$install_lang["error_name_empty"] = "Nom est un champ requis";
$install_lang["field_date_add"] = "Ajouter une date";
$install_lang["field_gid"] = "Mot clé";
$install_lang["field_max_items"] = "Photos maximum";
$install_lang["field_max_items_comment"] = "(0-illimité)";
$install_lang["field_name"] = "Nom";
$install_lang["field_upload_config"] = "Télécharger config";
$install_lang["field_use_moderation"] = "Modérer";
$install_lang["items_count_unlimit"] = "Illimité";
$install_lang["link_add_gallery"] = "Ajouter type de galerie";
$install_lang["link_delete_type"] = "Détruire";
$install_lang["link_edit_type"] = "Éditer";
$install_lang["moderation_off"] = "Off";
$install_lang["moderation_on"] = "On";
$install_lang["no_upload_gallery"] = "Aucun types de galerie";
$install_lang["note_delete_type"] = "Êtes vous sur de vouloir détruire cette entrée?";
$install_lang["success_add_type"] = "Type ajouté";
$install_lang["success_update_type"] = "Type mis à jour";


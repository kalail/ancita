<?php

$install_lang["admin_header_edit"] = "Фотоальбом";
$install_lang["admin_header_list"] = "Фотоальбом";
$install_lang["admin_header_template_edit"] = "Редактировать тип галереи";
$install_lang["error_badwords_comment"] = "Найдены запрещенные слова";
$install_lang["error_gid_empty"] = "Укажите системное имя GID";
$install_lang["error_max_items_count"] = "Вы загрузили максимальное количество файлов";
$install_lang["error_name_empty"] = "Название является обязательным полем";
$install_lang["field_date_add"] = "Дата создания";
$install_lang["field_gid"] = "Системное имя GID";
$install_lang["field_max_items"] = "Максимальное количество фотографий";
$install_lang["field_max_items_comment"] = "(0 - без ограничений)";
$install_lang["field_name"] = "Название";
$install_lang["field_upload_config"] = "Конфигурация загрузок";
$install_lang["field_use_moderation"] = "Модерация";
$install_lang["items_count_unlimit"] = "без ограничений";
$install_lang["link_add_gallery"] = "Добавить тип галереи";
$install_lang["link_delete_type"] = "Удалить";
$install_lang["link_edit_type"] = "Редактировать";
$install_lang["moderation_off"] = "Отключить";
$install_lang["moderation_on"] = "Включить";
$install_lang["no_upload_gallery"] = "Типы галереи отсутствуют";
$install_lang["note_delete_type"] = "Вы действительно хотите удалить эту запись?";
$install_lang["success_add_type"] = "Тип успешно добавлен";
$install_lang["success_update_type"] = "Тип успешно обновлен";


<?php

$install_lang["admin_header_edit"] = "Album de fotos";
$install_lang["admin_header_list"] = "Album de fotos";
$install_lang["admin_header_template_edit"] = "Editar tipo de galería";
$install_lang["error_badwords_comment"] = "Error malas palabras";
$install_lang["error_gid_empty"] = "La palabra clave es un campo obligatorio";
$install_lang["error_max_items_count"] = "Ha alcanzado el número máximo de archivos";
$install_lang["error_name_empty"] = "Nombre es un campo obligatorio";
$install_lang["field_date_add"] = "Fecha de agregación";
$install_lang["field_gid"] = "Palabra clave";
$install_lang["field_max_items"] = "Conteo Fotos max";
$install_lang["field_max_items_comment"] = "(0-ilimitado)";
$install_lang["field_name"] = "Nombre";
$install_lang["field_upload_config"] = "Cargar config";
$install_lang["field_use_moderation"] = "Moderado";
$install_lang["items_count_unlimit"] = "Ilimitado";
$install_lang["link_add_gallery"] = "Añadir tipo de galería";
$install_lang["link_delete_type"] = "Borrar";
$install_lang["link_edit_type"] = "editar";
$install_lang["moderation_off"] = "Desactivar";
$install_lang["moderation_on"] = "Activar";
$install_lang["no_upload_gallery"] = "No hay tipos de galerías aún";
$install_lang["note_delete_type"] = "¿Está seguro que desea eliminar la entrada?";
$install_lang["success_add_type"] = "Tipo agregado correctamente";
$install_lang["success_update_type"] = "Tipo actualizado correctamente";


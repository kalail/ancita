<?php

$install_lang["admin_header_edit"] = "Фото албум";
$install_lang["admin_header_list"] = "Фото албум";
$install_lang["admin_header_template_edit"] = "Редактиране типа на галерията";
$install_lang["error_badwords_comment"] = "Намерени са неприлични думи";
$install_lang["error_gid_empty"] = "Посочете системно име на GID";
$install_lang["error_max_items_count"] = "Заредили сте макс. количество файлове";
$install_lang["error_name_empty"] = "Названието е задължително поле";
$install_lang["field_date_add"] = "Дата на добавяне";
$install_lang["field_gid"] = "Keyword";
$install_lang["field_max_items"] = "Максимално количество фотографии";
$install_lang["field_max_items_comment"] = "(0 - безкрайност)";
$install_lang["field_name"] = "Название";
$install_lang["field_upload_config"] = "Конфигурация на изтеглянето";
$install_lang["field_use_moderation"] = "Одобряване след проверка";
$install_lang["items_count_unlimit"] = "без ограничения";
$install_lang["link_add_gallery"] = "Добавяне тип на галерията";
$install_lang["link_delete_type"] = "Изтрий";
$install_lang["link_edit_type"] = "Реадактирай";
$install_lang["moderation_off"] = "Изключено";
$install_lang["moderation_on"] = "Включено";
$install_lang["no_upload_gallery"] = "Липсва тип на галерията";
$install_lang["note_delete_type"] = "Действително ли искате да изтриете записа?";
$install_lang["success_add_type"] = "Типът е добавен";
$install_lang["success_update_type"] = "Tипът е обновен";


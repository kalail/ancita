<?php

$install_lang["field_type"]["header"] = "Tipo de campo";
$install_lang["field_type"]["option"]["checkbox"] = "Caja";
$install_lang["field_type"]["option"]["select"] = "Desplegable";
$install_lang["field_type"]["option"]["text"] = "Campo de texto";
$install_lang["field_type"]["option"]["textarea"] = "Area de texto";
$install_lang["field_type"]["option"]["multiselect"] = "Selección múltiple";



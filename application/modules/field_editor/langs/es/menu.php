<?php

$install_lang["admin_fields_menu_fields_list_item"] = "Campos";
$install_lang["admin_fields_menu_fields_list_item_tooltip"] = "";
$install_lang["admin_fields_menu_forms_list_item"] = "Formas";
$install_lang["admin_fields_menu_forms_list_item_tooltip"] = "";
$install_lang["admin_fields_menu_sections_list_item"] = "Secciones";
$install_lang["admin_fields_menu_sections_list_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_system-items_field_editor_menu_item"] = "Editor de campo";
$install_lang["admin_menu_settings_items_system-items_field_editor_menu_item_tooltip"] = "Administrar formularios de búsqueda , campos y bloques de información en una lista";


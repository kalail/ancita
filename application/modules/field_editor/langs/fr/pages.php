<?php

$install_lang["admin_header_field_add"] = "Ajouter nouveau champ";
$install_lang["admin_header_field_change"] = "Changer données de champ";
$install_lang["admin_header_fields_list"] = "Gèrer références";
$install_lang["admin_header_form_add"] = "Ajouter nouvelle formule";
$install_lang["admin_header_form_change"] = "Changer données de formule";
$install_lang["admin_header_forms_list"] = "Gèrer références";
$install_lang["admin_header_section_add"] = "Ajouter nouvelle section";
$install_lang["admin_header_section_change"] = "Changer références de section";
$install_lang["admin_header_section_list"] = "Gèrer références";
$install_lang["error_editor_type_empty"] = "Type d'éditeur vide";
$install_lang["error_empty_search_name"] = "Nom de recherche vide";
$install_lang["error_field_code_exists"] = "Un champ avec un tel code existe déja";
$install_lang["error_field_code_incorrect"] = "Code de champ invalide";
$install_lang["error_field_length_less_than"] = "Longueur de champ moins de [length]";
$install_lang["error_field_length_more_than"] = "Longueur de champ plus que [length]";
$install_lang["error_field_name_empty"] = "Nom de champ vide";
$install_lang["error_field_option_name_empty"] = "Nom d'option vide";
$install_lang["error_field_type_empty"] = "Type de champ est vide";
$install_lang["error_form_code_incorrect"] = "Mot clé est vide";
$install_lang["error_form_editor_type_incorrect"] = "Contenu de formule est requis";
$install_lang["error_form_name_incorrect"] = "Nom est requis";
$install_lang["error_section_code_exists"] = "Une section avec un tel code existe déja";
$install_lang["error_section_code_incorrect"] = "Code de section invalide";
$install_lang["error_section_empty"] = "Mot clé pour cette section est vide";
$install_lang["error_section_name_empty"] = "Nom de section est vide";
$install_lang["field_checkbox_by_default"] = "Par défaut";
$install_lang["field_comment"] = "Commentaires";
$install_lang["field_field_name"] = "Champ";
$install_lang["field_field_type"] = "Type de champ";
$install_lang["field_form_type"] = "Type de contenu de formule";
$install_lang["field_fts"] = "Utiliser en recherche 'fulltext'";
$install_lang["field_gid"] = "Mot clé";
$install_lang["field_name"] = "Nom";
$install_lang["field_section_data"] = "Section";
$install_lang["field_section_name"] = "Section";
$install_lang["field_select_empty_option"] = "Ajouter option '...'";
$install_lang["field_select_options"] = "Options";
$install_lang["field_select_search_type"] = "Choisir type";
$install_lang["field_select_search_type_many"] = "Choix multiple";
$install_lang["field_select_search_type_one"] = "Choix simple";
$install_lang["field_select_view_type"] = "Type de résultat de champ";
$install_lang["field_select_view_type_checkbox"] = "Case";
$install_lang["field_select_view_type_header"] = "Voire type";
$install_lang["field_select_view_type_multi"] = "Multi-sélection";
$install_lang["field_select_view_type_radio"] = "Boutons de radio";
$install_lang["field_select_view_type_select"] = "Sélectionner";
$install_lang["field_select_view_type_slider"] = "Bouton glissant";
$install_lang["field_text_by_default"] = "Valeur par défaut";
$install_lang["field_text_format"] = "Format de 'output' de valeur";
$install_lang["field_text_max_char"] = "Longueur maximale";
$install_lang["field_text_min_char"] = "Longueur minimale";
$install_lang["field_text_search_type"] = "Type de recherche";
$install_lang["field_text_template"] = "Modèle de valeur";
$install_lang["field_text_view_type_exact"] = "Recherche exacte";
$install_lang["field_text_view_type_header"] = "Paramètres de recherche";
$install_lang["field_text_view_type_like"] = "Occurrence de ce texte";
$install_lang["field_text_view_type_range"] = "Gamme de nombres";
$install_lang["field_textarea_by_default"] = "Valeur par défaut";
$install_lang["field_textarea_max_char"] = "Longueur maximale (0-illimité)";
$install_lang["field_textarea_min_char"] = "Longueur minimale";
$install_lang["filter_section"] = "Choisir une catégorie";
$install_lang["form_name"] = "Formule";
$install_lang["header_add_form_field"] = "Ajouter une section en cette formule";
$install_lang["header_add_form_section"] = "Introduire section dans cette formule";
$install_lang["header_add_select_option"] = "Ajouter une option";
$install_lang["header_change_field_settings"] = "Changer paramètres de champ";
$install_lang["header_change_select_option"] = "Changer option";
$install_lang["header_edit_form_section"] = "Éditer section";
$install_lang["link_add_field"] = "Ajouter nouveau champ";
$install_lang["link_add_form"] = "Ajouter nouvelle formule";
$install_lang["link_add_form_field"] = "Ajouter un champ dans cette formule";
$install_lang["link_add_form_section"] = "Ajouter une section en cette formule";
$install_lang["link_add_new_option"] = "Ajouter nouvelle option";
$install_lang["link_add_section"] = "Ajouter nouvelle section";
$install_lang["link_default_option"] = "Choisir option par défaut";
$install_lang["link_delete_field"] = "Détruire champ";
$install_lang["link_delete_form"] = "Détruire formule";
$install_lang["link_delete_option"] = "Détruire option";
$install_lang["link_delete_section"] = "Détruire section";
$install_lang["link_edit_field"] = "Éditer champ";
$install_lang["link_edit_form"] = "Éditer formule";
$install_lang["link_edit_form_fields"] = "Éditer champs de formule";
$install_lang["link_edit_option"] = "Éditer option";
$install_lang["link_edit_section"] = "Éditer section";
$install_lang["link_save_sorting"] = "Sauvegarder une séquence";
$install_lang["link_sorting_mode"] = "Mode de triage";
$install_lang["link_view_mode"] = "Mode de vue";
$install_lang["no_fields"] = "Aucun champs";
$install_lang["no_forms"] = "Aucune formules";
$install_lang["no_sections"] = "Aucune sections";
$install_lang["note_delete_form"] = "Êtes vous sur de vouloir détruire cette formule?";
$install_lang["note_delete_section"] = "Êtes vous sur de vouloir détruire cette section? Tout les champs seront détruits aussi.";
$install_lang["others_languages"] = "Autres langues.";
$install_lang["select_view_type_checkbox"] = "Cases";
$install_lang["select_view_type_mselect"] = "Multi-sélection";
$install_lang["select_view_type_radio"] = "Boutons de radio";
$install_lang["select_view_type_select"] = "Menu déroulant";
$install_lang["success_delete_field"] = "Champ détruit";
$install_lang["success_delete_form"] = "Formule détruite";
$install_lang["success_delete_section"] = "Section détruite";
$install_lang["success_update_form_data"] = "Formule mise à jour";
$install_lang["success_update_option_data"] = "Option sauvegardée";
$install_lang["success_update_section_data"] = "Section sauvegardée";
$install_lang["text_add_form_or"] = "ou";
$install_lang["text_add_form_select_another_s"] = "Choisir une autre section";
$install_lang["text_add_form_select_field"] = "Choisir un champ";
$install_lang["text_add_form_select_section"] = "Choisir une section";
$install_lang["text_choose_block"] = "Choisir une section";
$install_lang["text_format_none"] = "Aucun";
$install_lang["text_format_price"] = "Prix";
$install_lang["text_format_year"] = "Année";
$install_lang["text_template_email"] = "Courriel";
$install_lang["text_template_floatval"] = "Flotteur";
$install_lang["text_template_intval"] = "Int";
$install_lang["text_template_price"] = "Prix";
$install_lang["text_template_string"] = "Littéral";
$install_lang["text_template_url"] = "URL";


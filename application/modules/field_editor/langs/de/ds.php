<?php

$install_lang["field_type"]["header"] = "Feldtyp";
$install_lang["field_type"]["option"]["checkbox"] = "Ankreuzfeld";
$install_lang["field_type"]["option"]["select"] = "Drop-down";
$install_lang["field_type"]["option"]["text"] = "Textfeld";
$install_lang["field_type"]["option"]["textarea"] = "Textbereich";
$install_lang["field_type"]["option"]["multiselect"] = "Mehrfachauswahl";



<?php

$install_lang["admin_fields_menu_fields_list_item"] = "Felder";
$install_lang["admin_fields_menu_fields_list_item_tooltip"] = "";
$install_lang["admin_fields_menu_forms_list_item"] = "Formulare";
$install_lang["admin_fields_menu_forms_list_item_tooltip"] = "";
$install_lang["admin_fields_menu_sections_list_item"] = "Abschnitte";
$install_lang["admin_fields_menu_sections_list_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_system-items_field_editor_menu_item"] = "Feldeditor";
$install_lang["admin_menu_settings_items_system-items_field_editor_menu_item_tooltip"] = "Suchformulare, Felder & Blöcke in einem Angebot  verwalten";


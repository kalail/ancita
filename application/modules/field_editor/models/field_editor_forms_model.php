<?php

if (!defined('FIELD_EDITOR_FORMS')) define('FIELD_EDITOR_FORMS', DB_PREFIX.'field_editor_forms');

/**
 * Field Editor Forms Model
 * 
 * @package PG_RealEstate
 * @subpackage Field editor
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Field_editor_forms_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Prefix of form section
	 * 
	 * @var string
	 */
	private $form_section_lang_prefix = 'fs_';
	
	/**
	 * Prefix of form section module
	 * 
	 * @var string
	 */
	private $form_section_module_prefix = 'fslng_';
	
	/**
	 * Set of fields
	 * 
	 * @var array
	 */
	private $fields;
	
	/**
	 * Constructor
	 * 
	 * @return Field_editor_model
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
		
		include_once(MODULEPATH . "field_editor/models/field_type_loader_model".EXT);
		$this->fields = new Field_types_loader_model();
	}

	/**
	 * Return field editor form object by identifier
	 * 
	 * @param integer $form_id form identifier
	 * @return array
	 */
	public function get_form_by_id($form_id){
		$result = $this->DB->select("id, gid, editor_type_gid, name, field_data")->from(FIELD_EDITOR_FORMS)->where("id", $form_id)->get()->result_array();
		$return = (!empty($result))?$result[0]:array();
		if(!empty($return["field_data"])){
			$return["field_data"] = unserialize($return["field_data"]);
		}
		return $return;		
	}
	
	/**
	 * Return field editor form object by guid
	 * 
	 * @param string $form_gid form guid
	 * @return array
	 */
	public function get_form_by_gid($form_gid, $editor_type_gid=""){
		$this->DB->select("id, gid, editor_type_gid, name, field_data")->from(FIELD_EDITOR_FORMS)->where("gid", $form_gid);
		if(!empty($editor_type_gid)){
			$this->DB->where("editor_type_gid", $editor_type_gid);
		}
		$result = $this->DB->get()->result_array();
		$return = (!empty($result))?$result[0]:array();
		if(!empty($return["field_data"])){
			$return["field_data"] = unserialize($return["field_data"]);
		}
		return $return;		
	}
	
	/**
	 * Format form data
	 * 
	 * @param array $data form data
	 * @return array
	 */
	public function format_form($data){
		list($section_gids, $field_gids) = $this->get_form_field_gids($data["field_data"]);

		if(!empty($field_gids)){
			$this->CI->load->model('Field_editor_model');
			$this->CI->Field_editor_model->initialize($data["editor_type_gid"]);
			$params["where"]["editor_type_gid"] = $data["editor_type_gid"];
			$params["where_in"]["gid"] = $field_gids;
			$fields = $this->CI->Field_editor_model->get_fields_list($params);

			foreach($data["field_data"] as $key => $item){
				if($item["type"] == 'section'){
					foreach($item["section"]["fields"] as $skey => $sitem){
						if(!isset($fields[$sitem["field"]["gid"]])){
							unset($data["field_data"][$key]["section"]["fields"][$skey]);
						}
					}
				}else{
					if(!isset($fields[$item["field"]["gid"]])){
						unset($data["field_data"][$key]);
					}
				}
			}
		}

		return $data;
	}
	
	/**
	 * Format form for used in output
	 * 
	 * @param array $data form data
	 * @return array
	 */
	public function format_output_form($data){

		list($section_gids, $field_gids) = $this->get_form_field_gids($data["field_data"]);

		if(!empty($section_gids)){
			$sections = $this->get_form_sections($data["editor_type_gid"]);

			foreach($data["field_data"] as $key => $item){
				if($item["type"] == 'section'){
					$data["field_data"][$key]["section"]["name"] = $sections[$item["section"]["gid"]];
				}
			}
		}
		
		if(!empty($field_gids)){
			$this->CI->load->model('Field_editor_model');
			$this->CI->Field_editor_model->initialize($data["editor_type_gid"]);
			$params["where"]["editor_type_gid"] = $data["editor_type_gid"];
			$params["where_in"]["gid"] = $field_gids;
			$fields = $this->CI->Field_editor_model->get_fields_list($params);

			foreach($data["field_data"] as $key => $item){
				if($item["type"] == 'section'){
					foreach($item["section"]["fields"] as $skey => $sitem){
						if(isset($fields[$sitem["field"]["gid"]])){
							$data["field_data"][$key]["section"]["fields"][$skey]["field_content"] = $fields[$sitem["field"]["gid"]];
						}else{
							unset($data["field_data"][$key]["section"]["fields"][$skey]);
						}
					}
				}else{
					if(isset($fields[$item["field"]["gid"]])){
						$data["field_data"][$key]["field_content"] = $fields[$item["field"]["gid"]];
					}else{
						unset($data["field_data"][$key]);
					}
				}
			}
		}
		return $data;
	}
	
	/**
	 * Return field editor form objects as array
	 * 
	 * @param array $param selection parameters
	 * @param array $order_by sorting data
	 * @return array
	 */
	public function get_forms_list($params=array(), $order_by=array()){
		$this->DB->select("id, gid, editor_type_gid, name, field_data")->from(FIELD_EDITOR_FORMS);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(is_array($order_by) && count($order_by)>0){
			foreach($order_by as $field => $dir){
				$this->DB->order_by($field." ".$dir);
			}
		}
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $k=>$r){
				$return[$r["gid"]] = $r;
			}
			return $return;
		}
		return array();
		
	}
	
	/**
	 * Return number of form objects 
	 * 
	 * @param array $params selection parameters
	 * @return integer
	 */
	public function get_forms_count($params=array()){
		$this->DB->select("COUNT(*) AS cnt")->from(FIELD_EDITOR_FORMS);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}
	
	/**
	 * Validate form data
	 * 
	 * @param integer $form_id form identifier
	 * @param array $data form data
	 * @return array
	 */
	public function validate_form($form_id, $data){
		$return = array("errors"=> array(), "data" => array());
	
		if(isset($data["gid"])){
			$data["gid"] = strtolower(strip_tags($data["gid"]));
			$data["gid"] = preg_replace("/[\n\s\t]+/i", '-', $data["gid"]);
			$data["gid"] = preg_replace("/[^a-z0-9\-_]+/i", '', $data["gid"]);
			$data["gid"] = preg_replace("/[\-]{2,}/i", '-', $data["gid"]);

			$return["data"]["gid"] = $data["gid"];
			
			if(empty($return["data"]["gid"]) ){
				$return["errors"][] = l('error_form_code_incorrect', 'field_editor');
			}else{
				$param["where"]["gid"] = $return["data"]["gid"];
				if($form_id) $param["where"]["id <>"] = $form_id;
				$gid_counts = $this->get_forms_count($param);
				if($gid_counts > 0){
					$return["errors"][] = l('error_form_code_exists', 'field_editor');
				}
			}
		}

		if(isset($data["editor_type_gid"])){
			$return["data"]["editor_type_gid"] = strval($data["editor_type_gid"]);
			if(empty($return["data"]["editor_type_gid"]) ){
				$return["errors"][] = l('error_form_editor_type_incorrect', 'field_editor');
			}
		}

		if(isset($data["name"])){
			$return["data"]["name"] = trim(strip_tags($data["name"]));
			if(empty($return["data"]["name"]) ){
				$return["errors"][] = l('error_form_name_incorrect', 'field_editor');
			}
		}
		
		if(isset($data["field_data"]) && is_array($data["field_data"])){
			foreach($data["field_data"] as $field){
				if(is_array($field)){
					if($field["type"] == 'section'){
						$section_fields = $field["section"]['fields'];
						$field["section"]['fields'] = array();
						if(is_array($section_fields) && !empty($section_fields)){
							foreach($section_fields as $sfield){
								if(is_array($sfield)){
									$field["section"]['fields'][] = $sfield;
								}
							}
						}
					}
					$return["data"]["field_data"][] = $field;
				}
			}
			$return["data"]["field_data"] = serialize($return["data"]["field_data"]);
		}elseif(isset($data["field_data"])){
			$return["data"]["field_data"] = serialize(array());
		}
		return $return;
	}
	
	/**
	 * Save form object to data source
	 * 
	 * @param integer $form_id form identifier
	 * @param array $data form data
	 * @return integer
	 */
	public function save_form($form_id, $data){
		if (empty($form_id)){
			$this->DB->insert(FIELD_EDITOR_FORMS, $data);
			$form_id = $this->DB->insert_id();
		}else{
			$this->DB->where('id', $form_id);
			$this->DB->update(FIELD_EDITOR_FORMS, $data);
		}
		return $form_id;
	}
	
	/**
	 * Remove form object from data source by identifier
	 * 
	 * @param integer $form_id form identifier
	 * @return void
	 */
	public function delete_form_by_id($form_id){
		$this->DB->where('id', $form_id);
		$this->DB->delete(FIELD_EDITOR_FORMS);
		return;
	}
	
	/**
	 * Remove form object from data source by guid
	 * 
	 * @param string $form_gid form guid
	 * @return void
	 */
	public function delete_form_by_gid($form_gid){
		$this->DB->where('gid', $form_gid);
		$this->DB->delete(FIELD_EDITOR_FORMS);
		return;
	}
	
	/**
	 * Save form section data
	 * 
	 * @param string $form_editor_type_gid field editor type guid
	 * @param string $section_gid field editor section guid
	 * @param array $name form section name 
	 * @return string
	 */
	public function form_section_save($form_editor_type_gid, $section_gid, $name){
		if(!$section_gid){
			$section_gid = substr(md5(date('Y-m-d H:i:s')), 0, 6);	
		}
		$module_gid = $this->form_section_module_prefix.$form_editor_type_gid;
		$section = $this->form_section_lang_prefix.$section_gid;
		if(!empty($name)){
			$languages = $this->CI->pg_language->languages;
			if(!empty($languages)){
				$lang_ids = array_keys($languages);
				$this->CI->pg_language->pages->set_string_langs($module_gid, $section, $name, $lang_ids);
			}
		}
		return $section_gid;
	}
	
	/**
	 * Remove form section object
	 * 
	 * @param string $form_editor_type_gid field editor type guid
	 * @param string $section_gid field editor section guid
	 * @return void
	 */
	public function form_section_delete($form_editor_type_gid, $section_gid){
		$module_gid = $this->form_section_module_prefix.$form_editor_type_gid;
		$section = $this->form_section_lang_prefix.$section_gid;

		$this->CI->pg_language->pages->delete_string($module_gid, $section);		
	}
	
	/**
	 * Return form section objects as array
	 * 
	 * @param string $form_editor_type_gid field editor type guid
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function get_form_sections($form_editor_type_gid, $lang_id=''){
		$module_gid = $this->form_section_module_prefix.$form_editor_type_gid;
		$raw_sections = $this->CI->pg_language->pages->return_module($module_gid, $lang_id);
		if(!empty($raw_sections)){
			foreach($raw_sections as $gid => $value){
				$sections[str_replace($this->form_section_lang_prefix, '', $gid)] = $value;
			}
			return $sections;
		}
		return false;
	}
	
	/**
	 * Return form section object
	 * 
	 * @param string $form_editor_type_gid field editor type guid
	 * @param string $section_gid field editor section guid
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function get_form_section($form_editor_type_gid, $section_gid, $lang_id=''){
		$module_gid = $this->form_section_module_prefix.$form_editor_type_gid;
		$section = $this->form_section_lang_prefix.$section_gid;

		if($lang_id == 'all'){
			$languages = $this->CI->pg_language->languages;
			if(!empty($languages)){
				foreach($languages as $lang_id => $lang){
					$data[$lang_id] = l($section, $module_gid, $lang_id);
				}
			}
		}else{
			$data = l($section, $module_gid, $lang_id);
		}
		return $data;
	}
	
	/**
	 * Return field settings in form
	 * 
	 * @param string $field_type field type
	 * @return array
	 */
	public function get_form_field_settings($field_type){
		return $this->fields->$field_type->form_field_settings;
	}
	
	/**
	 * Return fields guids in form
	 * 
	 * @param array $field_data fields data
	 * @return array
	 */
	public function get_form_field_gids($field_data){
		$disallowed_fields = array();
		$disallowed_sections = array();
		if(!empty($field_data)){
			foreach($field_data as $item){
				if($item["type"] == 'section'){
					$disallowed_sections[] = $item["section"]['gid'];
					
					if(!empty($item["section"]["fields"])){
						foreach($item["section"]["fields"] as $field){
							if(is_array($field)) $disallowed_fields[] = $field["field"]['gid'];
						}
					}
				}elseif($item["type"] == 'field'){
					$disallowed_fields[] = $item["field"]['gid'];
				}
			}		
		}
		return array($disallowed_sections, $disallowed_fields);
	}
	
	/**
	 * Return criteria for used in search
	 * 
	 * @param string $form_gid form guid
	 * @param array $data form data
	 * @param array $editor_type_gid field editor type guid
	 * @param boolean $use_fe_prefix used field name prefix
	 * @return array
	 */
	public function get_search_criteria($form_gid, $data, $editor_type_gid="", $use_fe_prefix = true){
		$criteria = array();
		if(empty($data)){
			return $criteria;
		}
		$form = $this->get_form_by_gid($form_gid, $editor_type_gid);

		if(empty($form["field_data"])){
			return $criteria;
		}
		
		$this->CI->load->model('Field_editor_model');
		$this->CI->Field_editor_model->initialize($form["editor_type_gid"]);
		$field_gid_prefix = $use_fe_prefix ? '' : $this->CI->Field_editor_model->settings['field_prefix'];
		$prefix = $use_fe_prefix ? $this->CI->Field_editor_model->settings['field_prefix'] : '';

		foreach($form["field_data"] as $item){
			if($item["type"] == 'section'){
				if(!empty($item["section"]["fields"])){
					foreach($item["section"]["fields"] as $field){
						$field['field']['gid'] = $field_gid_prefix.$field['field']['gid'];
						$field_criteria = $this->fields->$field["field"]["type"]->get_search_field_criteria($field["field"], $field["settings"], $data, $prefix);
						if(!empty($field_criteria)) $criteria = array_merge_recursive($criteria, $field_criteria);
					}
				}
			}elseif($item["type"] == 'field'){
				$item['field']['gid'] = $field_gid_prefix.$item['field']['gid'];
				$field_criteria = $this->fields->$item["field"]["type"]->get_search_field_criteria($item["field"], $item["settings"], $data, $prefix);
				if(!empty($field_criteria)) $criteria = array_merge_recursive($criteria, $field_criteria);
			}
		}	
		return $criteria;	
	}
}

<?php

if(!defined('FIELD_EDITOR_SECTIONS')) define('FIELD_EDITOR_SECTIONS', DB_PREFIX.'field_editor_sections');
if(!defined('FIELD_EDITOR_FIELDS')) define('FIELD_EDITOR_FIELDS', DB_PREFIX.'field_editor_fields');

/**
 * Field Editor Model
 *
 * @package PG_RealEstate
 * @subpackage Field editor
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Field_editor_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @return object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * 
	 * @var object
	 */
	private $DB;
	
	/**
	 * Field editor type by default
	 * 
	 * @var string
	 */
	private $default_editor_type = '';
	
	/**
	 * Field editor settings
	 * 
	 * @var array
	 */
	public $settings = array();

	/**
	 * Module language guid
	 * 
	 * @var string
	 */
	private $lang_module_gid = "";

	/**
	 * Cache field editor sections by id
	 * 
	 * @var array
	 */
	private $cache_section_by_id;
	
	/**
	 * Cache field editor sections by guid
	 * 
	 * @var array
	 */
	private $cache_section_by_gid;

	/**
	 * Field editor fields
	 * 
	 * @var array
	 */
	private $fields;
	
	/**
	 * Attributes of field object
	 * 
	 * @var array
	 */
	private $attrs = array(
		'id', 
		'gid', 
		'section_gid', 
		'editor_type_gid', 
		'field_type', 
		'fts', 
		'settings_data', 
		'sorter',
	);

	/**
	 * Constructor
	 * 
	 * @return Field_editor_model
	 */
	function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
		
		include_once(MODULEPATH . "field_editor/models/field_type_loader_model".EXT);
		$this->fields = new Field_types_loader_model();
		
		foreach($this->CI->pg_language->languages as $id=>$value){
			$this->attrs[] = 'comment_'.$value['id'];
		}
		
		$this->get_default_editor_type(true);
		$this->initialize($this->default_editor_type);
	}

	/**
	 * Initialize model object
	 * 
	 * @param string $type field editor type
	 */
	public function initialize($type=null){
		$this->config->load('field_editor', TRUE);
		$settings = $this->config->item('editor_type', 'field_editor');
		if(!$type) $type = $this->get_default_editor_type(true);
		$this->settings = $settings[$type];
	}
	
	/**
	 * Return field editor settings
	 * 
	 * @return array
	 */
	public function get_settings(){
		return $this->settings;
	}

	/**
	 * Return field editor type objects as array
	 * 
	 * @param boolean $installed_only only installed types
	 * @return array
	 */
	public function get_editor_types($installed_only = false){
		$this->config->load('field_editor', TRUE);
		$settings = $this->config->item('editor_type', 'field_editor');

		$return = array();
		if ($installed_only){
			foreach ($settings as $type => $sett) {
				if ($this->CI->pg_module->is_module_installed($sett['module'])){
					$return[$type] = $sett;
				}
			}
		} else {
			$return = $settings;
		}
		return $return;
	}

	/**
	 * Return field editor type by default
	 * 
	 * @param boolean $installed_only only installed types
	 */
	public function get_default_editor_type($installed_only = false){
		if ($installed_only){
			$editor_types = $this->get_editor_types($installed_only);
			if (!isset($editor_types[$this->default_editor_type])){
				$this->default_editor_type = array_shift(array_keys($editor_types));
			}
		}
		return $this->default_editor_type;
	}

	/**
	 * Return field settings as array
	 * 
	 * @param string $field_type field type
	 * @return array
	 */
	public function get_field_settings($field_type){
		return $this->fields->$field_type->manage_field_param;
	}

	/*
	* Sections methods
	*/

	/**
	 * Return section object by identifier
	 * 
	 * @param integer $id section identifier
	 * @return array
	 */
	public function get_section_by_id($id){
		if(empty($this->cache_section_by_id[$id])){
			$result = $this->DB->select("id, gid, editor_type_gid, sorter")->from(FIELD_EDITOR_SECTIONS)->where("id", $id)->get()->result_array();
			$return = (!empty($result))?$this->format_section($result[0]):array();
			$this->cache_section_by_id[$id] = $this->cache_section_by_gid[$return["gid"]] = $return;
		}
		return $this->cache_section_by_id[$id];
	}

	/**
	 * Return section object by guid
	 * 
	 * @param string $gid section guid
	 * @return array
	 */
	public function get_section_by_gid($gid){
		if(empty($this->cache_section_by_gid[$gid])){
			$result = $this->DB->select("id, gid, editor_type_gid, sorter")->from(FIELD_EDITOR_SECTIONS)->where("gid", $gid)->where('editor_type_gid', $this->settings['gid'])->get()->result_array();
			$return = (!empty($result))?$this->format_section($result[0]):array();
			$this->cache_section_by_gid[$gid] = $this->cache_section_by_id[$return["id"]] = $return;
		}
		return $this->cache_section_by_gid[$gid];
	}

	/**
	 * Format section object
	 * 
	 * @param array $data section data
	 * @return array
	 */
	public function format_section($data){
		$data["name"] = l('section_'.$data["id"], 'field_editor_sections');
		return $data;
	}

	/**
	 * Return filtered section objects as array
	 * 
	 * @param array $params filters data
	 * @param array $order_by sorting data
	 * @return array
	 */
	public function get_section_list($params=array(), $order_by=array()){
		$params["where"]["editor_type_gid"] = $this->settings["gid"];

		$this->DB->select("id, gid, editor_type_gid, sorter")->from(FIELD_EDITOR_SECTIONS);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(is_array($order_by) && count($order_by)>0){
			foreach($order_by as $field => $dir){
				$this->DB->order_by($field." ".$dir);
			}
		}else{
			$this->DB->order_by("sorter ASC");
		}
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $k=>$r){
				$return[$r["gid"]] = $this->format_section($r);
			}
			return $return;
		}
		return array();

	}

	/**
	 * Return number of filtered section objects
	 * 
	 * @param array $params filters data
	 * @return integer
	 */
	public function get_section_count($params=array()){
		$params["where"]["editor_type_gid"] = $this->settings["gid"];

		$this->DB->select("COUNT(*) AS cnt")->from(FIELD_EDITOR_SECTIONS);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	/**
	 * Save section object to data source
	 * 
	 * @param integer $id section identifier
	 * @param array $data section data
	 * @param array $name section name
	 * @return integer
	 */
	public function save_section($id, $data, $name=null){
		if (is_null($id)){
			$this->DB->insert(FIELD_EDITOR_SECTIONS, $data);
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where('id', $id);
			$this->DB->update(FIELD_EDITOR_SECTIONS, $data);
		}

		if(!empty($name)){
			$this->CI->pg_language->pages->set_string_langs('field_editor_sections', "section_".$id, $name, array_keys($name));
		}
		unset($this->cache_section_by_id[$id]);
		if(!empty($data["gid"])) unset($this->cache_section_by_gid[$data["gid"]]);
		return $id;
	}

	/**
	 * Set section sorting value
	 * 
	 * @param integer $id section identifier
	 * @param integer $sorter sorting value
	 * @return void
	 */
	public function set_section_sorter($id, $sorter){
		$data["sorter"] = intval($sorter);
		$this->DB->where("id", $id);
		$this->DB->update(FIELD_EDITOR_SECTIONS, $data);
	}

	/**
	 * Validate section data
	 * 
	 * @param integer $id section identifier
	 * @param array $data section data
	 * @param $lang_data languages data
	 * @return array
	 */
	public function validate_section($id, $data, $lang_data=array()){
		$return = array("errors"=> array(), "data" => array(), "lang" => array());

		if(isset($data["gid"])){
			$data["gid"] = strtolower(strip_tags($data["gid"]));
			$data["gid"] = preg_replace("/[\n\s\t]+/i", '-', $data["gid"]);
			$data["gid"] = preg_replace("/[^a-z0-9\-_]+/i", '', $data["gid"]);
			$data["gid"] = preg_replace("/[\-]{2,}/i", '-', $data["gid"]);

			$return["data"]["gid"] = $data["gid"];

			if(empty($return["data"]["gid"]) ){
				$return["errors"][] = l('error_section_code_incorrect', 'field_editor');
			}else{
				$param["where"]["gid"] = $return["data"]["gid"];
				if($id) $param["where"]["id <>"] = $id;
				$gid_counts = $this->get_section_count($param);
				if($gid_counts > 0){
					$return["errors"][] = l('error_section_code_exists', 'field_editor');
				}
			}
		}

		if(isset($data["editor_type_gid"])){
			$return["data"]["editor_type_gid"] = strval($data["editor_type_gid"]);
		}
		
		if(!empty($lang_data)){
			/// $lang_data is a array(lang_id => value)
			$languages = $this->CI->pg_language->languages;
			$cur_lang_id = $this->CI->pg_language->current_lang_id;
			$default_lang = isset($lang_data[$cur_lang_id]) ? (trim(strip_tags($lang_data[$cur_lang_id]))) : '';
			if($default_lang == ''){
				$return["errors"][] = l('error_section_name_empty', 'field_editor');
			}
			
			foreach($languages as $id_lang => $lang_settings){
				$return["lang"][$id_lang] = trim(strip_tags($lang_data[$id_lang]));
				if(empty($return["lang"][$id_lang])) $return["lang"][$id_lang] = $default_lang;
			}
			
		}
		
		if(isset($data["sorter"])){
			$return["data"]["sorter"] = strval($data["sorter"]);
		}
		
		return $return;
	}

	/**
	 * Remove section object from data source by identifier
	 * 
	 * @param integer $id section identifier
	 * @return void
	 */
	public function delete_section($id){
		$data = $this->get_section_by_id($id);
		$params["where"]["section_gid"] = $data["gid"];
		$fields = $this->get_fields_list($params);

		foreach($fields as $field){
			$this->delete_field($field["id"]);
		}

		$this->DB->where('id', $id);
		$this->DB->delete(FIELD_EDITOR_SECTIONS);

		$this->CI->pg_language->pages->delete_string('field_editor_sections', "section_".$id);
		return;
	}

	/**
	 * Remove section object from data source by guid
	 * 
	 * @param string $gid section guid
	 * @return void
	 */
	public function delete_section_by_gid($gid){
		$data = $this->get_section_by_gid($gid);
		$params["where"]["section_gid"] = $data["gid"];
		$params["where"]["editor_type_gid"] = $data["editor_type_gid"];
		$fields = $this->get_fields_list($params);

		foreach($fields as $field){
			$this->delete_field($field["id"]);
		}

		$this->DB->where('id', $data['id']);
		$this->DB->delete(FIELD_EDITOR_SECTIONS);

		$this->CI->pg_language->pages->delete_string('field_editor_sections', "section_".$data["id"]);
		return;
	}

	/*
	 * BASE Fields methods
	 */
	
	/**
	 * Create section field
	 * 
	 * @param string $table_name table name
	 * @param string $field_name field name
	 * @param array $field_settings field definition
	 * @return void
	 */
	public function base_field_create($table_name, $field_name, $field_settings){
		$this->CI->load->dbforge();
		$fields[$field_name] = $field_settings;
		$this->CI->dbforge->add_column($table_name, $fields);
		return;
	}

	/**
	 * Update section field
	 * 
	 * @param string $table_name table name
	 * @param string $field_name field name
	 * @param array $field_settings field definition
	 * @return void
	 */
	public function base_field_update($table_name, $field_name, $field_settings){
		$this->CI->load->dbforge();
		$fields[$field_name] = $field_settings;
		$fields[$field_name]["name"] = $field_name;
		$this->CI->dbforge->modify_column($table_name, $fields);
		return;
	}

	/**
	 * Remove section field
	 * 
	 * @param string $table_name table name
	 * @param string $field_name field name
	 */
	public function base_field_delete($table_name, $field_name){
		$this->CI->load->dbforge();
		$fields = $this->DB->list_fields($table_name);
		if(in_array($field_name, $fields)){
			$this->CI->dbforge->drop_column($table_name, $field_name);
		}
		return;
	}

	/**
	 * Set value of fulltext search field
	 * 
	 * @param integer $id field identifier
	 * @param string $content field value
	 * @return void
	 */
	public function base_update_fulltext_field($id, $content){
		$field_id = isset($this->settings['fulltext_foreign_key']) ? $this->settings['fulltext_foreign_key'] : 'id';
		$fields = $this->settings['fulltext_field'];
		$tables = $this->settings['tables'];
		foreach($fields as $table => $field_name){
			$data[$field_name] = trim($content);
			$this->DB->where($field_id, $id);
			$this->DB->update($tables[$table], $data);
		}
		return;
	}

	/*
	 * Manage Fields methods
	 */
	 
	/**
	 * Return field object by identifier
	 * 
	 * @param integer $id field identifier
	 * @param integer $lang_id language identifier
	 * @return array
	 */ 
	public function get_field_by_id($id, $lang_id=''){
		if(empty($this->cache_field_by_id[$id])){
			$result = $this->DB->select(implode(',', $this->attrs))->from(FIELD_EDITOR_FIELDS)->where("id", $id)->get()->result_array();
			$return = (!empty($result))?$this->format_field($result[0], $lang_id):array();
			$this->cache_field_by_id[$id] = $this->cache_field_by_gid[$return["gid"]] = $return;
		}
		return $this->cache_field_by_id[$id];
	}

	/**
	 * Return field object by guid
	 * 
	 * @param string $gid field guid
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function get_field_by_gid($gid, $lang_id=''){
		if(empty($this->cache_field_by_gid[$gid])){
			$result = $this->DB->select(implode(',', $this->attrs))->from(FIELD_EDITOR_FIELDS)->where("gid", $gid)->get()->result_array();
			$return = (!empty($result))?$this->format_field($result[0], $lang_id):array();
			$this->cache_field_by_gid[$gid] = $this->cache_field_by_id[$return["id"]] = $return;
		}
		return $this->cache_field_by_gid[$gid];
	}

	/**
	 * Format field object
	 * 
	 * @param array $data field data
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function format_field($data, $lang_id=''){
		$data["field_name"] = $this->get_field_select_name($data["gid"]);
		$data["name"] = $this->fields->$data["field_type"]->format_field_name($data, $lang_id);
		$data["settings_data_array"] = $data["settings_data"] ? (array)unserialize($data["settings_data"]) : array();
		$data["comment"] = $data["comment_".$this->pg_language->current_lang_id];
		$data = $this->fields->$data["field_type"]->format_field($data, $lang_id);
		return $data;
	}
	
	/**
	 * Format field name
	 * 
	 * @param array $data field data
	 * @param integer $lang_id language identifier
	 * @return string
	 */
	public function format_field_name($data, $lang_id=''){
		return $this->fields->$data["field_type"]->format_field_name($data, $lang_id);
	}

	/**
	 * Return field name in data source
	 * 
	 * @param string $gid field guid
	 * @return string
	 */
	public function get_field_select_name($gid){
		return $this->settings['field_prefix'].$gid;
	}

	/**
	 * Return filtered field objects as array
	 * 
	 * @param array $params filters data
	 * @param array $filter_object_ids field identifiers
	 * @param array $order_by sorting data
	 * @param boolean $format format results
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function get_fields_list($params=array(), $filter_object_ids = null, $order_by=array(), $format=true, $lang_id=''){
		$this->DB->select(implode(',', $this->attrs))->from(FIELD_EDITOR_FIELDS);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		if(is_array($order_by) && count($order_by)>0){
			foreach($order_by as $field => $dir){
				$this->DB->order_by($field." ".$dir);
			}
		}else{
			$this->DB->order_by("section_gid ASC");
			$this->DB->order_by("sorter ASC");
		}
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $k=>$r){
				$return[$r["gid"]] = ($format)?$this->format_field($r, $lang_id):$r;
			}
			return $return;
		}
		return array();

	}

	/**
	 * Return number of filtered field objects 
	 * 
	 * @param array $params filters data
	 * @param array $filter_object_ids fields identifiers
	 * @return integer
	 */
	public function get_fields_count($params=array(), $filter_object_ids = null){
		$this->DB->select("COUNT(*) AS cnt")->from(FIELD_EDITOR_FIELDS);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	/**
	 * Save field object to data source
	 * 
	 * @param integer $id field identifier
	 * @param string $type field type
	 * @param string $section field section
	 * @param array $data field data
	 * @param string $name field name
	 * @return integer
	 */
	public function save_field($id, $type, $section, $data, $name = null){
		if (is_null($id)){
			$this->DB->insert(FIELD_EDITOR_FIELDS, $data);
			$id = $this->DB->insert_id();

			foreach($this->settings["tables"] as $table_gid => $table_name){
				$this->base_field_create($table_name, $this->get_field_select_name($data["gid"]), $this->fields->$data["field_type"]->base_field_param);
			}
			$this->inc_field_index();
		}else{
			$this->DB->where('id', $id);
			$this->DB->update(FIELD_EDITOR_FIELDS, $data);
		}

		if(!empty($name)){
			$field = $this->get_field_by_id($id);
			$this->fields->$data["field_type"]->update_field_name($field, $name);
		}

		unset($this->cache_field_by_id[$id]);
		if(!empty($data["gid"])) unset($this->cache_field_by_gid[$data["gid"]]);
		return $id;
	}

	/**
	 * Set field sorting value
	 * 
	 * @param integer $id field identifier
	 * @param integer $sorter sorting value
	 * @return void
	 */
	public function set_field_sorter($id, $sorter){
		$data["sorter"] = intval($sorter);
		$this->DB->where("id", $id);
		$this->DB->update(FIELD_EDITOR_FIELDS, $data);
	}
	
	/**
	 * Validate field options data
	 * 
	 * @param integer $id_field field identifier
	 * @param string $option_gid option guid
	 * @param array $data field data
	 * @return array
	 */
	public function validate_field_option($id_field, $option_gid, $data){
		$field_data = $this->get_field_by_id($id_field);
		return $this->fields->$field_data["field_type"]->validate_field_option($data);
	}
	
	/**
	 * Set field option data
	 * 
	 * @param integer $id_field field identifier
	 * @param string $option_gid option guid
	 * @param array $data field data
	 * @return void
	 */
	public function set_field_option($id_field, $option_gid, $data){
		$field_data = $this->get_field_by_id($id_field);
		$this->fields->$field_data["field_type"]->set_field_option($field_data, $option_gid, $data);
	}
	
	/**
	 * Remove field option data
	 * 
	 * @param integer $id_field field identifier
	 * @param string $option_gid option guid
	 * @return void
	 */
	public function delete_field_option($id_field, $option_gid){
		$field_data = $this->get_field_by_id($id_field);
		$this->fields->$field_data["field_type"]->delete_field_option($field_data, $option_gid);
	}
	
	/**
	 * Resort field options data
	 * 
	 * @param integer $id_field field identifier
	 * @param array $sorter_data sorting data
	 * @return void
	 */
	public function sorter_field_option($id_field, $sorter_data){
		$field_data = $this->get_field_by_id($id_field);
		$this->fields->$field_data["field_type"]->sorter_field_option($field_data, $sorter_data);
	}

	/**
	 * Validate field object for saving to data source
	 * 
	 * @param integer $id field identifier
	 * @param string $field_type field type
	 * @param array $data field data
	 * @param array $langs languages data
	 * @return array
	 */
	public function validate_field($id, $field_type, $data, $langs=null){
		$return = array("errors"=> array(), "data" => array());

		if(empty($id)){
			if(isset($data["section_gid"])){
				$return["data"]["section_gid"] = strval($data["section_gid"]);
			}else{
				$return["errors"][] = l('error_section_empty', 'field_editor');
				$return["data"]["section_gid"] = "";
			}

			if(isset($data["editor_type_gid"])){
				$return["data"]["editor_type_gid"] = strval($data["editor_type_gid"]);
			}else{
				$return["errors"][] = l('error_editor_type_empty', 'field_editor');
				$return["data"]["editor_type_gid"] = "";
			}

			$gid_exists = true;
			while($gid_exists){
				$data['gid'] = $this->get_field_gid();
				$param["where"]["gid"] = $data["gid"];
				$gid_counts = $this->get_fields_count($param);
				if($gid_counts > 0){
					$this->inc_field_index();
				}else{
					$gid_exists = false;
				}
			}
			$return["data"]["gid"] = $data["gid"];

			if(isset($data["fts"])){
				$return["data"]["fts"] = intval($data["fts"]);
			}

			if(isset($data["field_type"])){
				$return["data"]["field_type"] = strval($data["field_type"]);
			}else{
				$return["errors"][] = l('error_field_type_empty', 'field_editor');
				$return["data"]["field_type"] = "text";
			}
			
			if(isset($data["sorter"])){
				$return["data"]["sorter"] = intval($data["sorter"]);
			}
			
			$temp = $this->fields->$return["data"]["field_type"]->manage_field_param;
			foreach($temp as $param => $param_data){
				$return["data"]["settings_data"][$param] = $param_data["default"];
			}
			$return["data"]["settings_data"] = serialize($return["data"]["settings_data"]);
		}else{

			if(isset($data["fts"])){
				$return["data"]["fts"] = intval($data["fts"]);
			}

			if(isset($data["settings_data"])){
				$validate_types = $this->fields->$field_type->validate_field_type($data["settings_data"]);
				$return["data"]["settings_data"] = $validate_types["data"];
				if(!empty($validate_types["errors"])){
					foreach($validate_types["errors"] as $err) $return["errors"][] = $err;
				}
				$return["data"]["settings_data"] = serialize($return["data"]["settings_data"]);
			}
		}
		
		$default_lang_id = $this->CI->pg_language->current_lang_id;
		if(isset($data['comment_'.$default_lang_id])){
			$return['data']['comment_'.$default_lang_id] = trim(strip_tags($data['comment_'.$default_lang_id]));
			foreach($this->pg_language->languages as $lid=>$lang_data){
				if($lid == $default_lang_id) continue;
				if(!isset($data['comment_'.$lid]) || empty($data['comment_'.$lid])){
					$return['data']['comment_'.$lid] = $return['data']['comment_'.$default_lang_id];
				}else{
					$return['data']['comment_'.$lid] = trim(strip_tags($data['comment_'.$lid]));
				}
			}
		}
		
		if(!empty($langs)){
			$validate_lang = $this->fields->$field_type->validate_field_name($langs);
			$return = array_merge_recursive($return, $validate_lang);
		}
		return $return;
	}

	/**
	 * Remove field object from data source by identifier
	 * 
	 * @param integer $id field identifier
	 * @return void
	 */
	public function delete_field($id){
		$data = $this->get_field_by_id($id);

		$this->DB->where('id', $id);
		$this->DB->delete(FIELD_EDITOR_FIELDS);

		unset($this->cache_field_by_gid[$data["gid"]]);
		
		foreach($this->settings["tables"] as $table_gid => $table_name){
			$this->base_field_delete($table_name, $this->get_field_select_name($data["gid"]));
		}
		$this->fields->$data['field_type']->delete_field_lang($data["editor_type_gid"], $data["section_gid"], $data["gid"]);
		return;
	}

	/**
	 * Remove field object from data source by guid
	 * 
	 * @param string $gid field guid
	 * @return void
	 */
	public function delete_field_by_gid($gid){
		$data = $this->get_field_by_gid($gid);
		if(empty($data)) return;
		$this->DB->where('id', $data["id"]);
		$this->DB->delete(FIELD_EDITOR_FIELDS);

		foreach($this->settings["tables"] as $table_gid => $table_name){
			$this->base_field_delete($table_name, $this->get_field_select_name($data["gid"]));
		}

		$this->fields->$data['field_type']->delete_field_lang($data["editor_type_gid"], $data["section_gid"], $data["gid"]);
		return;
	}

	/*
	 * Data methods
	 */

	/**
	 * Return system names of field objects as array
	 * 
	 * @param array $section_gids set of section guid
	 * @param array $field_gids set of field guid
	 * @return array
	 */
	public function get_fields_for_select($section_gids=array(), $field_gids=array()){
		$this->DB->select("gid")->from(FIELD_EDITOR_FIELDS);

		if(!empty($field_gids)){
			if(!is_array($field_gids)) $field_gids = array(0=>$field_gids);
			$this->DB->or_where_in('gid', $field_gids);
		}

		if(!empty($section_gids)){
			if(!is_array($section_gids)) $section_gids = array(0=>$section_gids);
			$this->DB->or_where_in('section_gid', $section_gids);
		}		

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $k=>$r){
				$return[] = $this->get_field_select_name($r['gid']);
			}
			return $return;
		}
		return array();
	}

	/**
	 * Return field objects of form as array
	 * 
	 * @param array $content form data
	 * @param array $params filters data
	 * @param array $filter_object_ids field identifiers
	 * @param array $order_by sorting data 
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function get_form_fields_list($content, $params=array(), $filter_object_ids = null, $order_by=array(), $lang_id=''){
		$fields = $this->get_fields_list($params, $filter_object_ids, $order_by, true, $lang_id);

		foreach($fields as $key=>$field){
			$fields[$key] = $this->fields->$field["field_type"]->format_form_fields($field, $content[$field["field_name"]]);
		}

		return $fields;
	}

	/**
	 * Format form field object for used in view
	 * 
	 * @param array $param filters data
	 * @param array $data fields data
	 * @param integer $lang_id language identifer
	 * @return array
	 */
	public function format_item_fields_for_view($params, $data, $lang_id=''){
		$temp = $this->format_list_fields_for_view($params, array(0=>$data), $lang_id);
		return $temp[0];
	}

	/**
	 * Format form field objects for used in view
	 * 
	 * @param array $param filters data
	 * @param array $data fields data
	 * @param integer $lang_id language identifer
	 * @return array
	 */
	public function format_list_fields_for_view($params, $data, $lang_id=''){
		$return = array();
		$fields = $this->get_fields_list($params, null, array(), true, $lang_id);
		if(empty($fields)) return $return;
		foreach($data as $key => $item){
			foreach($fields as $field){
				$temp = array(
					"name" => $field["name"],
					"field_type" => $field["field_type"],
					'section_gid' => $field['section_gid'],
				);
				$return[$key][$field["gid"]] = $this->fields->$field["field_type"]->format_view_fields($field, $temp, $item[$field["field_name"]]);
			}
		}
		return $return;
	}

	/**
	 * Validate field objects for saving to data source
	 * 
	 * @param array $params filters data
	 * @param array $data fields data
	 * @return array
	 */
	public function validate_fields_for_save($params=array(), $data=array()){
		$return = array("errors"=> array(), "data" => array());
		$fields = $this->get_fields_list($params);
		foreach($fields as $field){
			if(isset($data[$field["field_name"]])){
				$temp = $this->fields->$field["field_type"]->validate_field($field, $data[$field["field_name"]]);
				$return["data"][$field["field_name"]] = $temp["data"];
				if(!empty($temp["errors"])){
					$return["errors"] = array_merge($return["errors"], $temp["errors"]);
				}
			}
		}
		return $return;
	}

	/**
	 * Updates languages data
	 *
	 * @param array $sections_data sections data
	 * @param array $langs_data languages data
	 * @return boolean
	 */
	public function update_sections_langs($sections_data, $langs_data) {

		foreach($sections_data as $section){
			if($section["data"]["editor_type_gid"] != $this->settings['gid']) continue;
			$s = $this->get_section_by_gid($section["data"]["gid"]);
			if(!$s) continue;
			$lang_data = $langs_data[$section["data"]["gid"]];
			$this->CI->pg_language->pages->set_string_langs('field_editor_sections', 'section_'.$s['id'], $lang_data, array_keys($lang_data));
		}
		return true;
	}

	/**
	 * Import field editor type structure
	 * 
	 * @param string $editor_type_gid field editor type guid
	 * @param array $sections_data sections data
	 * @param array $fields_data fields data
	 * @param array $forms_data forms data
	 * @return void
	 */
	public function import_type_structure($editor_type_gid, $sections_data, $fields_data, $forms_data){
		$this->initialize($editor_type_gid);

		foreach((array)$sections_data as $section){
			if($section["data"]["editor_type_gid"] != $editor_type_gid) continue;
			$this->CI->Field_editor_model->save_section(null, $section["data"]);
		}
		
		foreach((array)$fields_data as $field){
			if($field["data"]["editor_type_gid"] != $editor_type_gid) continue;
			unset($field["data"]['options']);
			$this->CI->Field_editor_model->save_field(NULL, $editor_type_gid, '', $field["data"]);
		}
		
		$this->CI->load->model('field_editor/models/Field_editor_forms_model');
		foreach((array)$forms_data as $form){
			if($form["data"]["editor_type_gid"] != $editor_type_gid) continue;
			$this->CI->Field_editor_forms_model->save_form(NULL, $form["data"]);
		}
	
		return;
	}

	/**
	 * Export field editor type structure
	 * 
	 * @param string $editor_type_gid field editor type guid
	 * @param string $file_path export file name
	 * @return array
	 */
	public function export_type_structure($editor_type_gid, $file_path=''){
		$content = "<?php\n\n";
		$this->initialize($editor_type_gid);
		
		// sections
		$sections = $this->get_section_list();
		
		$content .= '$fe_sections = array('."\n";
		foreach($sections as $section){
			$fe_sections[] = array(
				'data' => array(
					'gid' => $section['gid'],		
					'editor_type_gid' => $section['editor_type_gid'],
					'sorter' => $section['sorter'],
				),			
			);
			$content .= "\t".'array("data" => array( "gid" => "'.$section['gid'].'", "editor_type_gid" => "'.$section['editor_type_gid'].'")),'."\n";
		}
		$content .= ');'."\n\n";
		
		// fields
		$params["where"]['editor_type_gid'] = $editor_type_gid;
		$params["where_in"]['section_gid'] = array_keys($sections);
		
		if(!empty($sections)){
			$fields = $this->get_fields_list($params);
		}else{
			$fields = array();
		}
		
		$content .= '$fe_fields = array('."\n";
		foreach($fields as $field){

			$options = (!empty($field['options']['option'])) ? array_keys($field['options']['option']) : '';	
			$fe_fields[] = array(
				'data' => array(
					'gid' => $field['gid'],		
					'section_gid' => $field['section_gid'],
					'editor_type_gid' => $field['editor_type_gid'],
					'field_type' => $field['field_type'],
					'fts' => $field['fts'],
					'settings_data' => $field['settings_data'],
					'sorter' => $field['sorter'],
					'options' => $options,
				),			
			);
			
			$options_str = '""';
			if(!empty($options)){
				$options_str = "array('".implode("', '", $options)."')";
			}
			$content .= "\t".'array("data" => array( "gid" => "'.$field['gid'].'", "section_gid" => "'.$field['section_gid'].'", "editor_type_gid" => "'.$field['editor_type_gid'].'", "field_type" => "'.$field['field_type'].'", "fts" => "'.$field['fts'].'", "settings_data" => \''.$field['settings_data'].'\', "sorter" => "'.$field['sorter'].'", "options" => '.$options_str.')),'."\n";
		}
		$content .= ');'."\n\n";

		// forms
		$this->CI->load->model('field_editor/models/Field_editor_forms_model');
		$fparams["where"]["editor_type_gid"] = $editor_type_gid;
		$forms = $this->CI->Field_editor_forms_model->get_forms_list($fparams);

		$content .= '$fe_forms = array('."\n";
		foreach($forms as $form){
			$fe_forms[] = array(
				'data' => array(
					'gid' => $form['gid'],		
					'editor_type_gid' => $form['editor_type_gid'],
					'name' => htmlspecialchars($form['name']),
					'field_data' => $form['field_data'],
				),			
			);

			$content .= "\t".'array("data" => array( "gid" => "'.$form['gid'].'", "editor_type_gid" => "'.$form['editor_type_gid'].'", "name" => "'.htmlspecialchars($form['name']).'", "field_data" => \''.$form['field_data'].'\')),'."\n";
		}
		$content .= ');'."\n\n";
		
		if($file_path && isset($this->CI->zip) && !in_array($file_path, $this->CI->zip->data_arr)){
			$this->CI->zip->add_data($file_path, $content);
		}
 
		return array($fe_sections, $fe_fields, $fe_forms);
	}

	/**
	 * Export sections languages
	 * 
	 * @param array $sections_data sections data
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function export_sections_langs($sections_data, $langs_ids = null) {

		$strings = array();
		foreach($sections_data as $section){
			$s = $this->get_section_by_gid($section["data"]["gid"]);
			$strings['section_'.$s['id']] = $section["data"]["gid"];
		}

		$langs_db = $this->CI->pg_language->export_langs('field_editor_sections', array_keys($strings), $langs_ids);
		$lang_codes = array_keys($langs_db);
		foreach($lang_codes as $lang_code) {
			$lang_data[$strings[$lang_code]] = $langs_db[$lang_code];
		}
		return $lang_data;
	}

	/**
	 * Update fields languages
	 * 
	 * @param string $editor_type_gid field editor type guid
	 * @param array $fields_data fields data
	 * @param array $langs_data languages data
	 * @return array
	 */
	public function update_fields_langs($editor_type_gid, $fields_data, $langs_data) {
		$field_gids = array();
		$this->initialize($editor_type_gid);
		$module_gid = $this->settings['module'];	
		foreach($fields_data as $field){
			if($field['data']['editor_type_gid'] != $editor_type_gid) continue;
			$field_gids[] = $field['data']['gid'];
			$field_options[$field['data']['gid']] = $field['data']['options'];
		}

		$params["where_in"]['gid'] = $field_gids;
		$fields = $this->get_fields_list($params);
		foreach($fields as $field){
			$index = 'fields_'.$module_gid.'_'.$field['section_gid'].'_'.$field['gid'];
			$this->fields->$field["field_type"]->update_field_name($field, $langs_data[$index]);
			if(!empty($langs_data[$index.'-tooltip'])){
				$post_data = array();
				foreach($langs_data[$index.'-tooltip'] as $lang_id=>$comment){
					$post_data['comment_'.$lang_id] = $comment;
				}
				$validate_data = $this->validate_field($field['id'], $field['field_type'], $post_data);
				if(empty($validate_data["errors"]) && !empty($validate_data["data"])){
					$this->save_field($field['id'], $field['field_type'], $field['section_gid'], $validate_data["data"]);
				}
			}
			$options = $field_options[$field['gid']];
			if(!empty($options)){
				$value = array();
				foreach($options as $option_gid){
					$value[$option_gid] = $langs_data[$index.'_'.$option_gid];
				}
				$this->fields->$field["field_type"]->set_field_option($field, $options, $value);
			}
		}
		return true;
	}

	/**
	 * Export fields languages
	 * 
	 * @param string $editor_type_gid field editor type guid
	 * @param array $fields_data fields data
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function export_fields_langs($editor_type_gid, $fields_data, $langs_ids = null) {
		if(!$langs_ids) $langs_ids = array_keys($this->CI->pg_language->languages);
		$field_gids = array();
		$this->initialize($editor_type_gid);
		$module_gid = $this->settings['module'];

		foreach($fields_data as $field){
			$field_gids[] = $field['data']['gid'];
		}
		
		$params["where_in"]['gid'] = $field_gids;
		$fields = $this->get_fields_list($params, null, array(), false);
		foreach($fields as $field){
			$index = 'fields_'.$module_gid.'_'.$field['section_gid'].'_'.$field['gid'];
			foreach($langs_ids as $lid){
				$fdata = $this->format_field($field, $lid);
				$lang_data[$index][$lid] = $fdata["name"];
				$lang_data[$index.'-tooltip'][$lid] = $fdata["comment_".$lid];
				if(!isset($fdata['options'])) continue;
				foreach($fdata['options']['option'] as $ogid => $ovalue){
					$lang_data[$index.'_'.$ogid][$lid] = $ovalue;
				}
			}
		}
		return $lang_data;
	}
	
	/*
	 * Update fulltext indexed field in data table
	 * 
	 * callback have to return array in format:
	 *	main_fields => array (field=>text_value, field=>text_value)
	 *	fe_fields => array (field=>value_for_format, field=>value_for_format)
	 *	default_lang_id => int
	 *	object_lang_id => int
	 * 
	 * @param integer $id object identifier
	 * @return void
	 */
	public function update_fulltext_field($id){
		$sections = $this->get_section_list();
		$fields_for_select = empty($sections) ? array() : $this->get_fields_for_select(array_keys($sections));
		$model_name = ucfirst($this->settings["fulltext_model"]);
		$model_path = strtolower($this->settings["module"]."/models/").$model_name;
		$this->CI->load->model(strtolower($this->settings["module"]).'_model' == strtolower($model_name) ? $model_name : $model_path);
		
		$callback = $this->settings["fulltext_callback"];
		$model_data = $this->CI->$model_name->$callback($id, $fields_for_select);
		if(empty($model_data["fe_fields"])){
			$model_data["fe_fields"] = array();
		}
		$content = "";
		if(!empty($model_data["main_fields"])){
			$content = implode(" ", $model_data["main_fields"]);
		}
		$params = array();
		if(!empty($sections)){
			$params["where_in"]["section_gid"] = array_keys($sections);
		}
		$object_lang_data = $this->format_item_fields_for_fulltext($params, $model_data['fe_fields'], $model_data['object_lang_id']);
		$default_lang_data = $this->format_item_fields_for_fulltext($params, $model_data['fe_fields'], $model_data['default_lang_id']);
		foreach($object_lang_data as $field_gid => $value){
			$content .= " ".$value;
			if($value != $default_lang_data[$field_gid]) $content .= " ".$default_lang_data[$field_gid];
		}
		$this->base_update_fulltext_field($id, $content);
		return;
	}

	/**
	 * Format form field objects for used in fulltext search
	 * 
	 * @param array $params filters data
	 * @param array $data fields data
	 * @param array $lang_id language identifier
	 * @return array
	 */
	public function format_item_fields_for_fulltext($params, $data, $lang_id=''){
		$temp = $this->format_list_fields_for_fulltext($params, array(0=>$data), $lang_id);
		return $temp[0];
	}

	/**
	 * Format form field objects for used in fulltext search
	 * 
	 * @param array $params filters data
	 * @param array $data fields data
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function format_list_fields_for_fulltext($params, $data, $lang_id=''){
		$return = array();
		$fields = $this->get_fields_list($params, null, array(), true, $lang_id);
		if(empty($fields)) return $return;

		foreach($data as $key => $item){
			foreach($fields as $field){
				$temp = array(
					"name" => $field["name"],
					"field_type" => $field["field_type"],
				);
				$value = $item[$field["field_name"]];
				$return[$key][$field["gid"]] = $this->fields->$field["field_type"]->format_fulltext_fields($field, $temp, $value);
			}
		}
		return $return;
	}
	
	/**
	 * Return criteria data for used in fulltext search
	 * 
	 * @param string $text search text
	 * @param boolean $mode search mode
	 * @return array
	 */
	public function return_fulltext_criteria($text, $boolean_mode=false){
		$text = explode(' ', $text);
        $text = implode(' ', $text);
		$text = '%'.$text.'%';
		//$boolean_mode = $boolean_mode || count($text) < 2;
		//$text = ($boolean_mode ? '+('.implode('*) ', $text).'*)' : implode(' ', $text));
		//$mode = ($boolean_mode ? " IN BOOLEAN MODE" : "");
		$text = $this->DB->escape($text);
		$fields = $this->settings['fulltext_field'];
		foreach($fields as $table => $field){
			$return[$table] = array(
				'field' => "(search_field) like (".$text.") AS score",
				'where_sql' => "(search_field) like (".$text.")"
			);
		}
		return $return;
	}
	
	/**
	 * Return index of field object
	 * 
	 * @return integer
	 */
	public function get_field_index(){
		return intval($this->CI->pg_module->get_module_config('field_editor', 'field_counter'));
	}
	
	/**
	 * Increment index of field object
	 * 
	 * @return void
	 */
	public function inc_field_index(){
		$index = $this->get_field_index();
		$index++;
		$this->CI->pg_module->set_module_config('field_editor', 'field_counter', $index);
	}
	
	/**
	 * Return guid for new field
	 * 
	 * @return string
	 */
	public function get_field_gid(){
		$index = $this->get_field_index();
		return "field".$index;
	}
	
	/**
	 * Add language fields
	 * 
	 * field comment
	 * 
	 * @param integer $lang_id language identifier
	 * @return void
	 */
	public function lang_dedicate_module_callback_add($lang_id=false){
		if(!$lang_id) return;
		
		$this->CI->load->dbforge();
		
		$fields['comment_'.$lang_id] = array('type'=>'TEXT', 'null'=>TRUE);
		$this->CI->dbforge->add_column(FIELD_EDITOR_FIELDS, $fields);
		
		$default_lang_id = $this->CI->pg_language->get_default_lang_id();
		if($lang_id != $default_lang_id){
			$this->CI->db->set('comment_'.$lang_id, 'comment_'.$default_lang_id, false);
			$this->CI->db->update(FIELD_EDITOR_FIELDS);
		}
	}
	
	/**
	 * Remove language fields
	 * 
	 * field comment
	 * 
	 * @param integer $lang_id language identifier
	 * @return void
	 */
	public function lang_dedicate_module_callback_delete($lang_id=false){
		if(!$lang_id) return;
		
		$this->CI->load->dbforge();
		
		$table_query = $this->CI->db->get(FIELD_EDITOR_FIELDS);
		$fields_exists = $table_query->list_fields();
		
		$fields = array('comment_'.$lang_id);
		foreach($fields as $field_name){
			if(!in_array($field_name, $fields_exists)) continue;
			$this->CI->dbforge->drop_column(FIELD_EDITOR_FIELDS, $field_name);
		}
	}
}

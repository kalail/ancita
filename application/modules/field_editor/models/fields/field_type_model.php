<?php  

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Field type model
 * 
 * @package PG_RealEstate
 * @subpackage Field editor
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Field_type_model {
	/**
	 * Field definition
	 * 
	 * @var array
	 */
	public $base_field_param = array();

	/**
	 * Default value configuration
	 * 
	 * @var array
	 */
	public $manage_field_param = array();

	/**
	 * Field settings of form
	 * 
	 * @var array
	 */
	public $form_field_settings = array();
	
	/**
	 * Constructor
	 * 
	 * @return Field_type_model
	 */
	function __construct(){
		$this->CI = & get_instance();
	}
	
	/**
	 * Format field data
	 * 
	 * @param array $data field data
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function format_field($data, $lang_id=''){
		if(isset($data['settings_data_array'])){
			foreach($data['settings_data_array'] as $param => $value){
				$param_type = $this->manage_field_param[$param]['type'];
				switch($param_type){
					case 'array': 
						$data['settings_data_array'][$param] = unserialize($value); 
						if(empty($data['settings_data_array'][$param])) $data['settings_data_array'][$param] = array();
					break;
					case 'bool': $data['settings_data_array'][$param] = (bool)$value; break;
					case 'int': $data['settings_data_array'][$param] = (int)$value; break;
					case 'string': $data['settings_data_array'][$param] = trim(strip_tags($value)); break;
					case 'text': break;
				}
			}
		}
		return $data;
	}
	
	/**
	 * Format field name
	 * 
	 * @param array $field field data
	 * @param integer $lang_id language identifier
	 * @return string
	 */
	public function format_field_name($field, $lang_id){
		return l('field_'.$field['gid'], $field['editor_type_gid'].'_'.$field['section_gid'].'_lang', $lang_id);
	}

	/**
	 * Validate field name
	 * 
	 * @param array $name field name
	 * @return array
	 */
	public function validate_field_name($name){
		$return = array('errors' => array(), 'lang' => array());
		$languages = $this->CI->pg_language->languages;
		$cur_lang_id = $this->CI->pg_language->current_lang_id;
		
		$default_lang = isset($name[$cur_lang_id]) ? (trim(strip_tags($name[$cur_lang_id]))) : '';
		if($default_lang == ''){
			$return["errors"][] = l('error_field_name_empty', 'field_editor');
		}
		
		foreach($languages as $id_lang => $lang_settings){
			$return["lang"][$id_lang] = trim(strip_tags($name[$id_lang]));
			if(empty($return["lang"][$id_lang])) $return["lang"][$id_lang] = $default_lang;
		}
		return $return;
	}
	
	/**
	 * Update field name
	 * 
	 * @param array $field field data
	 * @param array $name field name
	 * @return void
	 */
	public function update_field_name($field, $name){
		$lang_ids = array_keys($name);
		if(empty($lang_ids)) return;
		$this->CI->pg_language->pages->set_string_langs($field['editor_type_gid'].'_'.$field['section_gid'].'_lang', 'field_'.$field['gid'], $name, array_keys($name));
	}
	
	/**
	 * Remove field language
	 * 
	 * @param string $type field type
	 * @param string $section field editor section guid
	 * @param string $gid field guid
	 * @return void
	 */
	public function delete_field_lang($type, $section, $gid){
		$this->CI->pg_language->pages->delete_string($type.'_'.$section.'_lang', 'field_'.$gid);
	}
	
	/**
	 * Validate field type
	 * 
	 * @param array $settings_data settiongs options
	 * @return array
	 */
	public function validate_field_type($settings_data){
		$return = array("errors"=> array(), "data" => array());

		$settings = $this->manage_field_param;
		foreach($settings as $param_gid => $param_data){
			if(isset($settings_data[$param_gid])){
				$return["data"][$param_gid] = $settings_data[$param_gid];
				switch($param_data["type"]){
					case 'array': $return["data"][$param_gid] = serialize($return["data"][$param_gid]); break;
					case 'bool': $return["data"][$param_gid] = (bool)$return["data"][$param_gid]; break;
					case 'int': $return["data"][$param_gid] = (int)$return["data"][$param_gid]; break;
					case 'string': $return["data"][$param_gid] = trim(strip_tags($return["data"][$param_gid])); break;
					case 'text': break;
				}
			}else{
				$return["data"][$param_gid] = $param_data["default"];
			}
		}
		
		return $return;
	}

	/**
	 * Format field for form
	 * 
	 * @param array $field field data
	 * @param mixed $content field content
	 */
	public function format_form_fields($field, $content){
		$field["value"] = isset($content)?$content:"";
		if(empty($field["value"])){
			$field["value"] = $field["settings_data_array"]["default_value"];
		}
		return $field;
	}
	
	/**
	 * Format field for view
	 * 
	 * @param array $settings settings parameters
	 * @param array $field field data
	 * @param mixed field value
	 * @return array
	 */
	public function format_view_fields($settings, $field, $value){
		$field["value"] = $value;
		return $field;
	}
	
	/**
	 * Format field for fulltext search
	 * 
	 * @param array $settings settings parameters
	 * @param array $field field data
	 * @param mixed field value
	 * @return string
	 */
	public function format_fulltext_fields($settings, $field, $value){
		return $value."; ";
	}
	
	/**
	 * Validate field data
	 * 
	 * @param array $settings field settings
	 * @param mixed $value field value
	 * @return array
	 */
	public function validate_field($settings, $value){
		$return = array("errors"=> array(), "data" => $value);
		return $return;
	}
	
	/**
	 * Return search criteria for field
	 * 
	 * @param array $field field data
	 * @param array $settings field settings
	 * @param array $data field data
	 * @param string $prefix field name prefix
	 * @return array
	 */
	public function get_search_field_criteria($field, $settings, $data, $prefix){
		$criteria = array();
		$gid = $field['gid'];
		if(!empty($data[$gid])) $criteria["where"][$prefix.$gid] = trim(strip_tags($data[$gid]));
		return $criteria;
	}
	
	/**
	 * Validate field option
	 * 
	 * @param array $data field option data
	 * @return array
	 */
	public function validate_field_option($data){
		$return = array('errors' => array(), 'lang' => array());
		$languages = $this->CI->pg_language->languages;
		$cur_lang_id = $this->CI->pg_language->current_lang_id;
		
		$default_lang = isset($data[$cur_lang_id]) ? (trim(strip_tags($data[$cur_lang_id]))) : '';
		if($default_lang == ''){
			$return["errors"][] = l('error_field_option_name_empty', 'field_editor');
		}
		
		foreach($languages as $id_lang => $lang_settings){
			$return["lang"][$id_lang] = trim(strip_tags($data[$id_lang]));
			if(empty($return["lang"][$id_lang])) $return["lang"][$id_lang] = $default_lang;
		}
		return $return;
	}
	
	/**
	 * Save field option to data source
	 * 
	 * @param array $field field data
	 * @param string $option_gid option guid 
	 * @param array $data option data
	 * @return void
	 */
	public function set_field_option($field, $option_gid, $data){
		if(!$option_gid){
			//// add new option
			$option_gid = 0;
			if(!empty($field["options"]["option"])){
				foreach($field["options"]["option"] as $gid => $value) if(intval($gid) > $option_gid) $option_gid = $gid;
			}
			$option_gid++;			
		}
		if(!is_array($option_gid)){
			$lang_data[$option_gid] = $data;
			$option_gid = (array)$option_gid;
		}else{
			$lang_data = $data;
		}
		
		foreach($lang_data as $option_gid => $option_data){
			foreach($option_data as $lid => $string){
				$options[$lid][$option_gid] = $string;
			}
		}

		foreach($options as $lid => $options_data){
			$reference = $this->CI->pg_language->get_reference($field["option_module"], $field["option_gid"], $lid);
			foreach($options_data as $gid=>$val) $reference["option"][$gid] = $val;
			$this->CI->pg_language->ds->set_module_reference($field["option_module"], $field["option_gid"], $reference, $lid);
		}
		return;
	}
	
	/**
	 * Remove field option
	 * 
	 * @param array $field field data
	 * @param string $option_gid option guid
	 * @return void
	 */
	public function delete_field_option($field, $option_gid){
		foreach($this->CI->pg_language->languages as $lid => $lang){
			$reference = $this->CI->pg_language->get_reference($field["option_module"], $field["option_gid"], $lid);
			if(isset($reference["option"][$option_gid])){
				unset($reference["option"][$option_gid]);
				$this->CI->pg_language->ds->set_module_reference($field["option_module"], $field["option_gid"], $reference, $lid);
			}
		}
		return;
	}
	
	/**
	 * Resort field option
	 * 
	 * @param array $field field data
	 * @param array $sorter_data sorting data
	 * @return void
	 */
	public function sorter_field_option($field, $sorter_data){
		$this->CI->pg_language->ds->set_reference_sorter($field["option_module"], $field["option_gid"], $sorter_data);
		return;
	}

}

<?php  

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Textarea field model
 * 
 * @package PG_RealEstate
 * @subpackage Field editor
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Textarea_field_model extends Field_type_model{
	/**
	 * Field definition
	 * 
	 * @var array
	 */
	public $base_field_param = array(
		'type' => 'TEXT',
		'null' => FALSE,
		'default' => '',
	);

	/**
	 * Default value configuration
	 * 
	 * @var array
	 */
	public $manage_field_param = array(
		'default_value' => array( 'type'=>'text', "default"=>'' ),
		'min_char' => array( 'type'=>'int', 'min' => 0, "default"=>0 ),
		'max_char' => array( 'type'=>'int', 'min' => 0, "default"=>'' ),
	);
	
	/**
	 * Field settings of form
	 * 
	 * @var array
	 */
	public $form_field_settings = array();
	
	/**
	 * Validate field type
	 * 
	 * @param array $settings_data settings data
	 * @return array
	 */
	public function validate_field_type($settings_data){
		$return = parent::validate_field_type($settings_data);

		$settings = $this->manage_field_param;

		if($return["data"]["min_char"] < $settings["min_char"]["min"]){
			$return["data"]["min_char"] = $settings["min_char"]["min"];
		}
		if($return["data"]["max_char"] < $settings["max_char"]["min"]){
			$return["data"]["max_char"] = $settings["max_char"]["min"];
		}

		return $return;
	}
	
	/**
	 * Validate field data
	 * 
	 * @param array $settings settings data
	 * @param string $value field value
	 * @return array
	 */
	public function validate_field($settings, $value){
		$return = array("errors"=> array(), "data" => $value);
		$return["data"] = trim(strip_tags($return["data"]));
		$string_length = strlen($return["data"]);
		if($settings["settings_data_array"]["min_char"] > $string_length){
			$return["errors"][] = $settings['name'].': '.str_replace("[length]", $settings["settings_data_array"]["min_char"], l('error_field_length_less_than', 'field_editor'));
		}
		if($settings["settings_data_array"]["max_char"] && $settings["settings_data_array"]["max_char"] < $string_length){
			$return["errors"][] = $settings['name'].': '.str_replace("[length]", $settings["settings_data_array"]["max_char"], l('error_field_length_more_than', 'field_editor'));
		}
		return $return;
	}

	/**
	 * Return criteria for used in search
	 * 
	 * @param array $field field data
	 * @param array $settings settings data
	 * @param array $data search data
	 * @param string $prefix field name prefix
	 * @return array
	 */
	public function get_search_field_criteria($field, $settings, $data, $prefix){
		$criteria = array();
		$gid = $field['gid'];
		if(!empty($data[$gid])){
			$criteria["where"][$prefix.$gid." LIKE"] = "%".trim(strip_tags($data[$gid]))."%";
		}			
		return $criteria;
	}

	
	/**
	 * Save field options (not available)
	 * 
	 * @return void
	 */
	public function set_field_option($field, $option_gid, $data){
		return;
	}

	/**
	 * Remove field options (not available)
	 * 
	 * @return void
	 */
	public function delete_field_option($field, $option_gid){
		return;
	}	

	/**
	 * Sorter field options (not available)
	 * 
	 * @return void
	 */
	public function sorter_field_option($field, $sorter_data){
		return;
	}
}

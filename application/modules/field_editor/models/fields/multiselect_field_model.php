<?php  

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Multiselect field model
 * 
 * @package PG_RealEstate
 * @subpackage Field editor
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Multiselect_field_model extends Field_type_model{
	/**
	 * Field definition
	 * 
	 * @var array
	 */
	public $base_field_param = array(
		'type' => 'BIGINT',
		'constraint' => 3,
		'null' => FALSE,
		'default' => 0,
	);

	/**
	 * Default value configuration
	 * 
	 * @var array
	 */
	public $manage_field_param = array(
		'default_value' => array( 'type'=>'array', "default"=>'' ),
		'view_type' => array( 'type'=>'string', 'options' => array('mselect', 'checkbox'), "default"=>"checkbox" ),
	);
	
	/**
	 * Field settings of form
	 * 
	 * @var array
	 */
	public $form_field_settings = array(
		"search_type" => array("values"=>array('one', 'many'), "default"=>'one'),
		"view_type" => array("values"=>array('mselect', 'radio'), "default"=>'select')
	);
	
	/**
	 * Format field data
	 * 
	 * @param array $data field data
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function format_field($data, $lang_id=''){
		$data = parent::format_field($data);
		$data["option_module"] = $data["section_gid"].'_lang';
		$data["option_gid"] = 'field_'.$data["gid"].'_opt';
		$data["options"] = ld($data["option_gid"], $data["option_module"], $lang_id);
		return $data;
	}

	/**
	 * Save field name
	 * 
	 * @param array $data field data
	 * @param array $name field name
	 * @return void
	 */
	public function update_field_name($field, $name){
		parent::update_field_name($field, $name);

		$languages = $this->CI->pg_language->languages;
		$cur_lang_id = $this->CI->pg_language->current_lang_id;
		$default_lang = isset($name[$cur_lang_id]) ? (trim(strip_tags($name[$cur_lang_id]))) : '';
		
		foreach($languages as $lid => $lang_settings){
			$name[$lid] = trim(strip_tags($name[$lid]));
			if(empty($name[$lid])) $name[$lid] = $default_lang;

			$reference = $this->CI->pg_language->get_reference($field['section_gid'].'_lang', 'field_'.$field['gid'].'_opt', $lid);
			$reference["header"] = $name[$lid];
			$this->CI->pg_language->ds->set_module_reference($field['section_gid'].'_lang', 'field_'.$field['gid'].'_opt', $reference, $lid);
		}
		return;
	}
	
	/**
	 * Remove field languages
	 * 
	 * @param string $type field type
	 * @param string $section field editor section
	 * @param string $gid field guid
	 * @return void
	 */
	public function delete_field_lang($type, $section, $gid){
		parent::delete_field_lang($type, $section, $gid);
		$this->CI->pg_language->ds->delete_reference($section.'_lang', 'field_'.$gid.'_opt');
	}
	
	/**
	 * Validate field type 
	 * 
	 * @param array $settings_data settings data
	 * @return array
	 */
	public function validate_field_type($settings_data){
		$return = parent::validate_field_type($settings_data);

		$settings = $this->manage_field_param;
		if(!in_array($return["data"]["view_type"], $settings["view_type"]["options"])){
			$return["data"]["view_type"] = $settings["view_type"]["default"];
		}

		return $return;
	}

	/**
	 * Format field data for used in form
	 * 
	 * @param array $field field data
	 * @param string $content field value
	 */
	public function format_form_fields($field, $content){
		$field["value"] = isset($content)?$content:"";
		if(empty($field["value"])){
			$field["value"] = $field["settings_data_array"]["default_value"];
		}else{
			$field["value"] = $this->dec_to_arr($field["value"]);
		}
		return $field;
	}
	
	/**
	 * Format field value for used in view
	 * 
	 * @param array $settings settings data
	 * @param array $field field data
	 * @param mixed $value field value 
	 * @return array
	 */
	public function format_view_fields($settings, $field, $value){
		if(is_array($value)){
			$field["value_dec"] = $this->arr_to_dec($value);
			$field["value_array"] = $value;
		}else{
			$field["value_dec"] = $value;
			$field["value_array"] = $this->dec_to_arr($value);
		}
		$field["value"] = array();
		foreach($field["value_array"] as $v){
			if(isset($settings["options"]["option"][$v])){
				$field["value"][$v] = $settings["options"]["option"][$v];
			}			
		}
		$field["value_str"] = implode(', ', $field["value"]);
		return $field;
	}

	/**
	 * Format field value for used in fulltext search
	 * 
	 * @param array $settings settings data
	 * @param array $field field data
	 * @param mixed $value field value
	 * @return array
	 */
	public function format_fulltext_fields($settings, $field, $value){
		$use_multilang_search = $this->CI->pg_module->get_module_config('field_editor', 'use_multilang_search');
		
		$field_array = $this->dec_to_arr($value);
		if($use_multilang_search){
			foreach($this->CI->pg_language->languages as $lang_id=>$lang_data){
				$options = ld($settings["option_gid"], $settings["option_module"], $lang_id);
				foreach($field_array as $v){
					if(isset($options["option"][$v])){
						$values[] = $options["option"][$v];
					}
				}
			}
		}else{		
			foreach($field_array as $v){
				if(isset($settings["options"]["option"][$v])){
					$values[] = $settings["options"]["option"][$v];
				}		
			}
		}

		$return = (!empty($values))?($field["name"]." ".implode(', ', $values)."; "):'';
		return $return;
	}

	/**
	 * Validate field data
	 * 
	 * @param array $settings settings data
	 * @param mixed $value field data
	 * @return array
	 */
	public function validate_field($settings, $value){
		$value = (array)$value;
		$return = array("errors"=> array(), "data" => $this->arr_to_dec($value));
		return $return;
	}

	/**
	 * Return criteria for used in search
	 * 
	 * @param array $field field data
	 * @param array $settings settings data
	 * @param array $data field data
	 * @param string $prefix field name prefix
	 * @return array
	 */
	public function get_search_field_criteria($field, $settings, $data, $prefix){
		$criteria = array();
		$gid = $field['gid'];
		if(!empty($data[$gid])){
			if($settings["search_type"] == "one"){
				$temp = (array)$data[$gid];
			}elseif(is_array($data[$gid])){
				$temp = $data[$gid];
			}
			$temp = $this->arr_to_dec($temp);
			$criteria["where_sql"][] = $prefix.$gid."&".$temp."=".$this->CI->db->escape($temp);
		}		
		return $criteria;
	}

	/**
	 * Convert data from array to numeric
	 * 
	 * @param array $data data
	 * @return integer
	 */
	private function arr_to_dec($data){
		$data = (array)$data;
		if(empty($data)) return 0;
		$binary_string = "";
		$max = max($data);
		for($i=0; $i<=$max; $i++) $binary_string = ((in_array($i, $data))?"1":"0").$binary_string;
		return bindec($binary_string);
	}
	
	/**
	 * Convert data from numeric to array
	 * 
	 * @param integer $data data
	 * @return array
	 */
	private function dec_to_arr($dec){
		$data = array();
		$binary_string = decbin($dec);
		$arr = str_split($binary_string);
		$max = count($arr)-1;
		for($i=0; $i<=$max; $i++) if($arr[$max-$i] == 1) $data[] = $i;
		return $data;
	}	
}

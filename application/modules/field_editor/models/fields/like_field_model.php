<?php  

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Like field model
 * 
 * @package PG_RealEstate
 * @subpackage FieldEditor
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Like_field_model {
	/**
	 * Field definition
	 * 
	 * @var array
	 */
	public $base_field_param = array(
		'type' => 'TINYINT',
		'constraint' => 3,
		'null' => FALSE,
		'default' => 0,
	);

	/**
	 * Default value configuration
	 * 
	 * @var array
	 */
	public $manage_field_param = array(
		'default_value' => array( 'type'=>'int', "default"=>0 ),
	);

	/**
	 * Field settings of form
	 * 
	 * @var array
	 */
	public $form_field_settings = array();
	
	/**
	 * Save field options (not available)
	 * 
	 * @return void
	 */
	public function set_field_option($field, $option_gid, $data){
		return;
	}

	/**
	 * Remove field options (not available)
	 * 
	 * @return void
	 */
	public function delete_field_option($field, $option_gid){
		return;
	}	

	/**
	 * Sorter field options (not available)
	 * 
	 * @return void
	 */
	public function sorter_field_option($field, $sorter_data){
		return;
	}
}

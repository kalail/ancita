<?php  

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Select field model
 * 
 * @package PG_RealEstate
 * @subpackage Field editor
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Select_field_model extends Field_type_model{
	/**
	 * Field definition
	 * 
	 * @var array
	 */
	public $base_field_param = array(
		'type' => 'INT',
		'constraint' => 3,
		'null' => FALSE,
		'default' => 0,
	);

	/**
	 * Default value configuration
	 * 
	 * @var array
	 */
	public $manage_field_param = array(
		'default_value' => array( 'type'=>'int', 'min' => 0, "default"=>0 ),
		'view_type' => array( 'type'=>'string', 'options' => array('select', 'radio'), "default"=>"select" ),
		'empty_option' => array( 'type'=>'bool', "default"=>true ),
	);
	
	/**
	 * Field settings of form
	 * 
	 * @var array
	 */
	public $form_field_settings = array(
		"search_type" => array("values"=>array('one', 'many'), "default"=>'one'),
		"view_type" => array("values"=>array('select', 'radio'), "default"=>'select')
	);
	
	/**
	 * Format field data
	 * 
	 * @param array $data field data
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function format_field($data, $lang_id=''){
		$data = parent::format_field($data, $lang_id);
		$data["option_module"] = $data["section_gid"].'_lang';
		$data["option_gid"] = 'field_'.$data["gid"].'_opt';
		$data["options"] = ld($data["option_gid"], $data["option_module"], $lang_id);
		return $data;
	}

	/**
	 * Save field name
	 * 
	 * @param array $field field data
	 * @param array $name field name
	 * @return void
	 */
	public function update_field_name($field, $name){
		parent::update_field_name($field, $name);

		$languages = $this->CI->pg_language->languages;
		$cur_lang_id = $this->CI->pg_language->current_lang_id;
		$default_lang = isset($name[$cur_lang_id]) ? (trim(strip_tags($name[$cur_lang_id]))) : '';
		
		foreach($languages as $lid => $lang_settings){
			$name[$lid] = trim(strip_tags($name[$lid]));
			if(empty($name[$lid])) $name[$lid] = $default_lang;

			$reference = $this->CI->pg_language->get_reference($field['section_gid'].'_lang', 'field_'.$field['gid'].'_opt', $lid);
			$reference["header"] = $name[$lid];
			$this->CI->pg_language->ds->set_module_reference($field['section_gid'].'_lang', 'field_'.$field['gid'].'_opt', $reference, $lid);
		}
		return;
	}

	/**
	 * Remove field languages
	 * 
	 * @param string $type field type
	 * @param string $section field editor section
	 * @param string $gid field guid
	 * @return void
	 */
	public function delete_field_lang($type, $section, $gid){
		parent::delete_field_lang($type, $section, $gid);
		$this->CI->pg_language->ds->delete_reference($section.'_lang', 'field_'.$gid.'_opt');
	}
	
	/**
	 * Validate field type
	 * 
	 * @param array $settings field settings
	 * @return array
	 */
	public function validate_field_type($settings_data){
		$return = parent::validate_field_type($settings_data);

		$settings = $this->manage_field_param;
		if(!in_array($return["data"]["view_type"], $settings["view_type"]["options"])){
			$return["data"]["view_type"] = $settings["view_type"]["default"];
		}

		return $return;
	}
	
	/**
	 * Format field value for used in view
	 * 
	 * @param array $settings field settings
	 * @param array $field field data
	 * @param mixed $value field value
	 * @return array
	 */
	public function format_view_fields($settings, $field, $value){
		if(is_array($value)){
			$field["value_arr"] = $value;
		}else{
			$field["value_int"] = $value;
		}
		if(!is_array($value) && isset($settings["options"]["option"][$value])){
			$field["value"] = $settings["options"]["option"][$value];
			
		}elseif(is_array($value)){
			$values = array();
			foreach($value as $v){
				if(!empty($settings["options"]["option"][$v])) $values[] = $settings["options"]["option"][$v];
			}
			$field["value"] = implode(', ', $values);
			
		}elseif(isset($settings["options"]["option"][$settings["settings_data_array"]["default_value"]])){
			$field["value"] = $settings["options"]["option"][$settings["settings_data_array"]["default_value"]];
			
		}else{
			$field["value"] = '';
			
		}
		return $field;
	}
	
	/**
	 * Format field value for used in search
	 * 
	 * @param array $settings settings data
	 * @param array $field field data
	 * @param string $value field value
	 * @return string
	 */
	public function format_fulltext_fields($settings, $field, $value){
		$use_multilang_search = $this->CI->pg_module->get_module_config('field_editor', 'use_multilang_search');
		if(isset($settings["options"]["option"][$value])){
			if($use_multilang_search){
				$return = '';
				foreach($this->CI->pg_language->languages as $lang_id=>$lang_data){
					$return .= l('field_'.$settings['gid'], $settings['editor_type_gid'].'_'.$settings['section_gid'].'_lang', $lang_id).' '.ld_option($settings["option_gid"], $settings["option_module"], $value, $lang_id)."; ";
				}
			}else{
				$return = $field["name"]." ".$settings["options"]["option"][$value]."; ";
			}
		}elseif(isset($settings["options"]["option"][$settings["settings_data_array"]["default_value"]])){
			if($use_multilang_search){
				$return = '';
				foreach($this->CI->pg_language->languages as $lang_id=>$lang_data){
					$return .= l('field_'.$settings['gid'], $settings['editor_type_gid'].'_'.$settings['section_gid'].'_lang', $lang_id).' '.ld_option($settings["option_gid"], $settings["option_module"], $settings["settings_data_array"]["default_value"], $lang_id)."; ";
				}
			}else{
				$return = $field["name"]." ".$settings["options"]["option"][$settings["settings_data_array"]["default_value"]]."; ";
			}
		}else{
			$return = '';
		}
		return $return;
	}
	
	/**
	 * Validate field data
	 * 
	 * @param array $settings settings data
	 * @param string $value field value
	 * @return array
	 */
	public function validate_field($settings, $value){
		$return = array("errors"=> array(), "data" => strval($value));
		return $return;
	}

	/**
	 * Return criteria for used in search
	 * 
	 * @param array $field field data
	 * @param array $settings settings data
	 * @param array $data search data
	 * @param string $prefix field name prefix
	 * @return array
	 */
	public function get_search_field_criteria($field, $settings, $data, $prefix){
		$criteria = array();
		$gid = $field['gid'];
		
		if($settings["search_type"] == "one"){
			if(!empty($data[$gid]))	$criteria["where_in"][$prefix.$gid] = array(intval($data[$gid]));
		}elseif($settings["view_type"] == 'slider'){
			$where = '';
			if(!empty($data[$gid.'_min'])) $where .= " AND ".$prefix.$gid." >= ".intval($data[$gid.'_min']); 
			if(!empty($data[$gid.'_max'])) $where .= " AND ".$prefix.$gid." <= ".intval($data[$gid.'_max']); 
			if(!empty($where)) $criteria["where_sql"][] = "((".substr($where, 4)."))";
		}elseif(!empty($data[$gid]) && is_array($data[$gid])){
			 array_unshift($data[$gid], 0);
			 $criteria["where_in"][$prefix.$gid] = $data[$gid];
		}
		return $criteria;
	}
}

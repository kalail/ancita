<?php  

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Checkbox field model
 * 
 * @package PG_RealEstate
 * @subpackage Field editor
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Checkbox_field_model extends Field_type_model{
	/**
	 * Field definition
	 * 
	 * @var array
	 */
	public $base_field_param = array(
		'type' => 'TINYINT',
		'constraint' => 3,
		'null' => FALSE,
		'default' => 0,
	);

	/**
	 * Default value configuration
	 * 
	 * @var array
	 */
	public $manage_field_param = array(
		'default_value' => array( 'type'=>'bool', "default"=>false ),
	);

	/**
	 * Field settings of form
	 * 
	 * @var array
	 */
	public $form_field_settings = array();

	/**
	 * Format field of form
	 * 
	 * @param array $field field data
	 * @param boolean $content field value
	 * @return array
	 */
	public function format_form_fields($field, $content){
		$field["value"] = $content;
		return $field;
	}

	/**
	 * Format field value for fulltext search
	 * 
	 * @param array $settings filed settings
	 * @param array $field field data
	 * @param boolean $value field value
	 */
	public function format_fulltext_fields($settings, $field, $value){
		$return = (!empty($value))?$field["name"]."; ":'';
		return $return;
	}
	
	/**
	 * Validate field data
	 * 
	 * @param array $settings field settings
	 * @param boolean $value field value
	 */
	public function validate_field($settings, $value){
		$return = array("errors"=> array(), "data" => $value);
		$return["data"] = ($return["data"])?1:0;
		return $return;
	}

	/**
	 * Return field criteria for fulltext search
	 * 
	 * @param array $field field data
	 * @param array $settings field settings
	 * @param array $data search data
	 * @param string $prefix field prefix
	 * @return array
	 */
	public function get_search_field_criteria($field, $settings, $data, $prefix){
		$criteria = array();
		$gid = $field['gid'];
		if(!empty($data[$gid])  && $data[$gid] == 1){
			$criteria["where"][$prefix.$gid] = 1;
		}			
		return $criteria;
	}
	
	/**
	 * Save field options (not available)
	 * 
	 * @return void
	 */
	public function set_field_option($field, $option_gid, $data){
		return;
	}

	/**
	 * Remove field options (not available)
	 * 
	 * @return void
	 */
	public function delete_field_option($field, $option_gid){
		return;
	}	

	/**
	 * Sorter field options (not available)
	 * 
	 * @return void
	 */
	public function sorter_field_option($field, $sorter_data){
		return;
	}
}

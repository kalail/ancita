<?php

/**
 * Field types loader Model
 *
 * @package PG_RealEstate
 * @subpackage Field editor
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Field_types_loader_model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;

	/**
	 * Constructor
	 * 
	 * @return Field_types_loader_model
	 */
	public function __construct(){
		$this->CI = & get_instance();
	}

	/**
	 * Return type object
	 * 
	 * @param string $var type name
	 * @return object
	 */
	public function __get($var){
		if(!$var) return '';
		include_once(MODULEPATH . "field_editor/models/fields/field_type_model".EXT);
		$driver = strtolower($var)."_field_model";
		$driver_file = MODULEPATH . "field_editor/models/fields/".$driver.EXT;
		if(file_exists($driver_file)){
			$model_name = ucfirst($var)."_field_model";
			include_once($driver_file);
			$this->$var = new $model_name();
		}else{
			$this->$var = new Field_type_model();
		}
		return $this->$var;
	}
}


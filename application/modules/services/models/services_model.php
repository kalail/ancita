<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

define('SERVICES_TEMPLATES_TABLE', DB_PREFIX.'services_templates');
define('SERVICES_TABLE', DB_PREFIX.'services');
define('SERVICES_LOG_TABLE', DB_PREFIX.'services_log');

/**
 * Services main model
 *
 * @package PG_RealEstate
 * @subpackage Services
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Services_model extends Model
{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * 
	 * @var object
	 */
	private $DB;

	/*
	 * Template properties in data source
	 * 
	 * data_admin = array("key" => "type") type = text/string/int/price/checkbox
	 * data_user = array("key" => "type") type = text/string/int/price/checkbox/hidden (hidden - user form controller will be wait get or post data)
	 *
	 * @var array
	 */
	private $template_fields = array(
		'id',
		'gid',
		'callback_module',
		'callback_model',
		'callback_method',
		'callback_validate_method',
		'price_type',
		'data_admin',
		'data_user',
		'date_add',
		'moveable',
	);

	/**
	 * Service properties in data source
	 * 
	 * @var array
	 */
	private $service_fields = array(
		"id",
		"gid",
		"template_gid",
		"pay_type",
		"status",
		"price",
		'is_trial',
		'trial_count',
		"data_admin",
		"date_add"
	);

	/**
	 * Cache of templates by identifiers
	 * 
	 * @var array
	 */
	private $cache_template_by_id = array();
	
	/**
	 * Cache of templates by GUIDs
	 * 
	 * @var array
	 */
	private $cache_template_by_gid = array();
	
	/**
	 * Cache services by identifiers
	 * 
	 * @var array
	 */
	private $cache_service_by_id = array();
	
	/**
	 * Cache service by guids
	 * 
	 * @var array
	 */
	private $cache_service_by_gid = array();


	/**
	 * Class contructor
	 * 
	 * @return Service_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return template object by identifier
	 * 
	 * @param integer $id template identifier
	 * @return array
	 */
	public function get_template_by_id($id){
		if(empty($this->cache_template_by_id[$id])){
			$result = $this->DB->select(implode(", ", $this->template_fields))->from(SERVICES_TEMPLATES_TABLE)->where("id", $id)->get()->result_array();
			$return = (!empty($result))?$this->format_template($result[0]):array();
			$this->cache_template_by_id[$id] = $this->cache_template_by_gid[$return["gid"]] = $return;
		}
		return $this->cache_template_by_id[$id];
	}

	/**
	 * Return template object by GUID
	 * 
	 * @param string $gid template guid
	 * @return array
	 */
	public function get_template_by_gid($gid){
		if(empty($this->cache_template_by_gid[$gid])){
			$result = $this->DB->select(implode(", ", $this->template_fields))->from(SERVICES_TEMPLATES_TABLE)->where("gid", $gid)->get()->result_array();
			$return = (!empty($result))?$this->format_template($result[0]):array();
			$this->cache_template_by_gid[$gid] = $this->cache_template_by_id[$return["id"]] = $return;
		}
		return $this->cache_template_by_gid[$gid];
	}

	/**
	 * Format template object
	 * 
	 * @param array $data template data
	 * @return array
	 */
	public function format_template($data){
		if(!empty($data["data_admin"])){
			$temp = unserialize($data["data_admin"]);
			if(!empty($temp)){
				foreach($temp as $param => $type){
					$data["data_admin_array"][$param] = array(
						"gid" => $param,
						"type" => $type,
						"name" => l('admin_param_name_'.$data["id"]."_".$param, 'services')
					);
				}
			}
		}
		if(!empty($data["data_user"])){
			$temp = unserialize($data["data_user"]);
			if(!empty($temp)){
				foreach($temp as $param=>$type){
					$data["data_user_array"][$param] = array(
						"gid" => $param,
						"type" => $type,
						"name" => l('user_param_name_'.$data["id"]."_".$param, 'services')
					);
				}
			}
		}
		$data["name"] = l('template_name_'.$data["id"], 'services');
		return $data;
	}

	/**
	 * Return templates objects as array
	 * 
	 * @param array $param filter parameters
	 * @param array $filter_objetc_ids filter identifiers
	 * @param array $order_by sorting data
	 * @return array
	 */
	public function get_template_list($params=array(), $filter_object_ids=null, $order_by=null){
		$this->DB->select(implode(", ", $this->template_fields))->from(SERVICES_TEMPLATES_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		if(is_array($order_by) && count($order_by)>0){
			foreach($order_by as $field => $dir){
				$this->DB->order_by($field." ".$dir);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $k=>$r){
				$return[$r["gid"]] = $r;
				$return[$r["gid"]]["name"] = l('template_name_'.$r["id"], 'services');
			}
			return $return;
		}
		return array();

	}

	/**
	 * Return number of templates objects
	 * 
	 * @param array $params filter parameters
	 * @param array $filter_object_ids filter identifiers
	 * @return integer
	 */
	public function get_template_count($params=array(), $filter_object_ids=null){
		$this->DB->select("COUNT(*) AS cnt")->from(SERVICES_TEMPLATES_TABLE);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	/**
	 * Validate template object for storing in data source
	 * 
	 * @param integer $id template identifier
	 * @param array $data template data
	 * @param array $langs_data languages data
	 * @return array
	 */
	public function validate_template($id, $data, $langs_data=null){
		$return = array("errors"=> array(), "data" => array(), 'langs_data'=>array());

		if(isset($data["gid"])){
			$data["gid"] = strip_tags($data["gid"]);
			$data["gid"] = preg_replace("/[^a-z0-9\-_]+/i", '', $data["gid"]);

			$return["data"]["gid"] = $data["gid"];

			if(empty($return["data"]["gid"]) ){
				$return["errors"][] = l('error_template_code_incorrect', 'services');
			}
		}

		if(isset($data["callback_module"])){
			$return["data"]["callback_module"] = $data["callback_module"];
		}

		if(isset($data["callback_model"])){
			$return["data"]["callback_model"] = $data["callback_model"];
		}

		if(isset($data["callback_method"])){
			$return["data"]["callback_method"] = $data["callback_method"];
		}

		if(isset($data["callback_validate_method"])){
			$return["data"]["callback_validate_method"] = $data["callback_validate_method"];
		}

		if(isset($data["price_type"])){
			$return["data"]["price_type"] = intval($data["price_type"]);
		}

		if(isset($data["moveable"])){
			$return["data"]["moveable"] = intval($data["moveable"]);
		}

		if(isset($data["data_admin"])){
			$return["data"]["data_admin"] = serialize($data["data_admin"]);
		}

		if(isset($data["data_user"])){
			$return["data"]["data_user"] = serialize($data["data_user"]);
		}
		
		if(!empty($langs_data)){
			foreach($this->pg_language->languages as $lid => $lang_data){
				if(!isset($langs_data[$lid])){
					$return['errors'][] = l('error_empty_template_name', "services");
					break;
				}else{
					$return["langs_data"][$lid] = trim(strip_tags($langs_data[$lid]));
					if(empty($return["langs_data"][$lid])){
						$return['errors'][] = l('error_empty_template_name', "services");
						break;
					}
				}
			}
		}

		return $return;
	}

	/**
	 * Save template object to data source
	 * 
	 * @param integer $id template identifier
	 * @param array $data template data
	 * @param array $name template name by languages
	 * @return integer
	 */
	public function save_template($id, $data, $name = null){
		if (is_null($id)){
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(SERVICES_TEMPLATES_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where('id', $id);
			$this->DB->update(SERVICES_TEMPLATES_TABLE, $data);
		}

		if(!empty($name)){
			$languages = $this->CI->pg_language->languages;
			if(!empty($languages)){
				$lang_ids = array_keys($languages);
				$this->CI->pg_language->pages->set_string_langs('services', "template_name_".$id, $name, $lang_ids);
			}
		}
		unset($this->cache_template_by_id[$id]);
		if(!empty($data["gid"])) unset($this->cache_template_by_gid[$data["gid"]]);
		return $id;
	}

	/**
	 * Remove template object from data source by identifier
	 * 
	 * @param integer $id template identifier
	 * @return void
	 */
	public function delete_template($id){
		$this->DB->where("id", $id);
		$this->DB->delete(SERVICES_TEMPLATES_TABLE);
		$this->CI->pg_language->pages->delete_string("services", "template_name_".$id);
		return;
	}

	/**
	 * Remove template object from data source by guid
	 * 
	 * @param string $gid template guid
	 * @return void
	 */
	public function delete_template_by_gid($gid){
		$template_data = $this->get_template_by_gid($gid);
		$this->delete_template($template_data["id"]);
		return;
	}

	/**
	 * Return service object by identifier
	 * 
	 * @param integer $id service identifier
	 * @return array
	 */
	public function get_service_by_id($id){
		if(empty($this->cache_service_by_id[$id])){
			$result = $this->DB->select(implode(", ", $this->service_fields))->from(SERVICES_TABLE)->where("id", $id)->get()->result_array();
			$return = (!empty($result))?$result[0]:array();
			if(!empty($return["data_admin"])){
				$return["data_admin_array"] = unserialize($return["data_admin"]);
			}
			$this->cache_service_by_id[$id] = $this->cache_service_by_gid[$return["gid"]] = $return;
		}
		return $this->cache_service_by_id[$id];
	}

	/**
	 * Return service object by guid
	 * 
	 * @param string $id service guid
	 * @return array
	 */
	public function get_service_by_gid($gid){
		if(empty($this->cache_service_by_gid[$gid])){
			$result = $this->DB->select(implode(", ", $this->service_fields))->from(SERVICES_TABLE)->where("gid", $gid)->get()->result_array();
			$return = (!empty($result))?$result[0]:array();
			if(!empty($return["data_admin"])){
				$return["data_admin_array"] = unserialize($return["data_admin"]);
			}
			$this->cache_service_by_gid[$gid] = $this->cache_service_by_id[$return["id"]] = $return;
		}
		return $this->cache_service_by_gid[$gid];
	}

	/**
	 * Return services objects from data source as array
	 * 
	 * @param array $params filter criteria
	 * @param array $filter_object_ids filter identifiers
	 * @param array $order_by sorting data
	 * @return array
	 */
	public function get_service_list($params=array(), $filter_object_ids=null, $order_by=null){
		$this->DB->select(SERVICES_TABLE.'.'.implode(", ".SERVICES_TABLE.'.', $this->service_fields));
		$this->DB->from(SERVICES_TABLE);

		if(isset($params['join']) && is_array($params['join']) && count($params['join'])){
			foreach($params['join'] as $join){
				$this->DB->join($join['table'], $join['condition'], $join['type']);
			}
		}

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value, null, false);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		if(is_array($order_by) && count($order_by)>0){
			foreach($order_by as $field => $dir){
				$this->DB->order_by($field." ".$dir);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return $this->format_service($results);
		}
		return array();
	}

	/**
	 * Format service object
	 * 
	 * @param array $data service data
	 * @return array
	 */
	public function format_service($data){
		$template_gids = array();
		foreach($data as $service){
			if(empty($template_gids) || !in_array($service["template_gid"], $template_gids)) $template_gids[] = $service["template_gid"];
		}

		$params["where_in"]["gid"] = $template_gids;
		$templates = $this->get_template_list($params);

		foreach($data as $k => $service){
			$data[$k]["template"] = $this->format_template($templates[$service["template_gid"]]);
			$data[$k]["data_admin"] = unserialize($service["data_admin"]);
			if(isset($service["id"])) $data[$k]["name"] = l('service_name_'.$service["id"], 'services');
		}

		return $data;
	}

	/**
	 * Return number of service objects from data source
	 * 
	 * @param array $params filter criteria
	 * @param array $filter_object_ids filter identifiers
	 * @return integer
	 */
	public function get_service_count($params=array(), $filter_object_ids=null){
		$this->DB->select("COUNT(*) AS cnt")->from(SERVICES_TABLE);

		if(isset($params['join']) && is_array($params['join']) && count($params['join'])){
			foreach($params['join'] as $join){
				$this->DB->join($join['table'], $join['condition'], $join['type']);
			}
		}

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value, null, false);
			}
		}

		if(isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)){
			$this->DB->where_in("id", $filter_object_ids);
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	/**
	 * Validate service object for storing in data source
	 * 
	 * @param integer $id service identifier
	 * @param array $data service data
	 * @param array $langs_data languages data
	 * @return array
	 */
	public function validate_service($id, $data, $langs_data=null){
		$return = array("errors"=> array(), "data" => array(), 'langs_data'=>array());

		if(isset($data["gid"])){
			$data["gid"] = strip_tags($data["gid"]);
			$data["gid"] = preg_replace("/[^a-z0-9\-_]+/i", '', $data["gid"]);

			$return["data"]["gid"] = $data["gid"];

			if(empty($return["data"]["gid"]) ){
				$return["errors"][] = l('error_service_code_incorrect', 'services');
			}else{
				$param["where"]["gid"] = $return["data"]["gid"];
				if($id) $param["where"]["id <>"] = $id;
				$gid_counts = $this->get_service_count($param);
				if($gid_counts > 0){
					$return["errors"][] = l('error_service_code_exists', 'services');
				}
			}
		}

		if(isset($data["template_gid"])){
			$return["data"]["template_gid"] = $data["template_gid"];
		}

		if(isset($data["pay_type"])){
			$return["data"]["pay_type"] = intval($data["pay_type"]);
		}

		if(isset($data["status"])){
			$return["data"]["status"] = intval($data["status"]);
		}

		if(isset($data["price"])){
			$return["data"]["price"] = floatval($data["price"]);
		}
		
		if(isset($data["is_trial"])){
			$return["data"]["is_trial"] = $data["is_trial"] ? 1 : 0;
		}
		
		if(isset($data["trial_count"])){
			$return["data"]["trial_count"] = intval($data["trial_count"]);
			if($return["data"]["trial_count"] < 1){
				$return["errors"][] = l('error_invalid_trial_count', 'services');
			}
		}

		if(isset($data["data_admin"]) && !empty($data["data_admin"])){
			$template_data = $this->get_template_by_gid($data["template_gid"]);
			foreach($data["data_admin"] as $key => $value){
				switch($template_data["data_admin_array"][$key]["type"]){
					case "string": $value = trim(strip_tags($value)); break;
					case "int": $value = intval($value); break;
					case "price": $value = sprintf("%01.2f", floatval($value)); break;
					case "text": break;
					case "checkbox": $value = (intval($value)>0)?1:0; break;
				}
				$data["data_admin"][$key] = $value;
			}
			$return["data"]["data_admin"] = serialize($data["data_admin"]);
		}
		
		if(!empty($langs_data)){
			$default_lang_id = $this->CI->pg_language->current_lang_id;
			if(!isset($langs_data[$default_lang_id])){
				$return['errors'][] = l('error_empty_service_name', "services");
			}else{
				$return["langs_data"][$default_lang_id] = trim(strip_tags($langs_data[$default_lang_id]));
				if(empty($return["langs_data"][$default_lang_id])){
					$return['errors'][] = l('error_empty_service_name', "services");
				}else{
					foreach($this->CI->pg_language->languages as $lid => $lang_data){
						if($lid == $default_lang_id) continue;
						if(!isset($langs_data[$lid])){
							$return["langs_data"][$lid] = $return["langs_data"][$default_lang_id];
						}else{
							$return["langs_data"][$lid] = trim(strip_tags($langs_data[$lid]));
							if(empty($return["langs_data"][$lid])){
								$return["langs_data"][$lid] = $return["langs_data"][$default_lang_id];
							}	
						}
					}
				}
			}
		}

		return $return;
	}

	/**
	 * Save service object to data source
	 * 
	 * @param integer $id service identifier
	 * @param array $data service data
	 * @param array $name service name by languages
	 * @return integer
	 */
	public function save_service($id, $data, $name=array()){
		if (is_null($id)){
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(SERVICES_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where('id', $id);
			$this->DB->update(SERVICES_TABLE, $data);
		}

		if(!empty($name)){
			$languages = $this->CI->pg_language->languages;
			if(!empty($languages)){
				$lang_ids = array_keys($languages);
				$this->CI->pg_language->pages->set_string_langs('services', "service_name_".$id, $name, $lang_ids);
			}
		}

		unset($this->cache_service_by_id[$id]);
		if(!empty($data["gid"])) unset($this->cache_service_by_gid[$data["gid"]]);

		return $id;
	}

	/**
	 * Remove service object from data source by identifier
	 * 
	 * @param integer $id service identifier
	 * @return void
	 */
	public function delete_service($id){
		$this->DB->where("id", $id);
		$this->DB->delete(SERVICES_TABLE);
		$this->CI->pg_language->pages->delete_string("services", "service_name_".$id);
		return;
	}

	/**
	 * Remove service object from data source by guid
	 * 
	 * @param string $gid service guid
	 * @return void
	 */
	public function delete_service_by_gid($gid){
		$template_data = $this->get_service_by_gid($gid);
		$this->delete_service($template_data["id"]);
		return;
	}

	/**
	 * Logging of service used
	 * 
	 * @param integer $id_user user identifier
	 * @param integer $id_service service identifier
	 * @param array $user_data user data
	 * @return void
	 */
	public function add_service_log($id_user, $id_service, $user_data){
		$data = array(
			"id_user" => $id_user,
			"id_service" => $id_service,
			"user_data" => serialize($user_data),
			"date_add" => date("Y-m-d H:i:s"),
		);
		$this->DB->insert(SERVICES_LOG_TABLE, $data);
	}

	/**
	 * Validate service payment object
	 * 
	 * @param integer $id_service service identifier
	 * @param array $user_data user data
	 * @param float $price service price
	 * @return array
	 */
	public function validate_service_payment($id_service, $user_data, $price){
		$return = array("errors"=> array(), "data" => array());
		$service_data = $this->get_service_by_id($id_service);
		$template_data = $this->get_template_by_gid($service_data["template_gid"]);

		$return["data"]["price"] = $price = floatval($price);

		if(!empty($template_data["data_user_array"])){
			foreach($template_data["data_user_array"] as $gid => $param){
				$value = isset($user_data[$gid])?$user_data[$gid]:"";
				switch($param["type"]){
					case "string": $value = trim(strip_tags($value)); break;
					case "int": $value = intval($value); break;
					case "price": $value = sprintf("%01.2f", floatval($value)); break;
					case "text": break;
					case "checkbox": $value = (intval($value)>0)?1:0; break;
					case "hidden":
						if(empty($value)){
							$return["errors"][] = l('error_parametr_incorrect', 'services').$param["name"];
						}
					break;
				}
				$return["data"]["data_user"][$gid] = $value;
			}
		}
		return $return;
	}

	/**
	 * Validate user data by module callback
	 * 
	 * @param integer $id_service service identifier
	 * @param array $user_data user data
	 * @param integer $id_user user identifier
	 * @param float $price service price
	 * @return array
	 */
	public function validate_service_original_model($id_service, $user_data, $id_user, $price){
		$return = array("errors"=> array(), "data" => array());
		$service_data = $this->get_service_by_id($id_service);
		$template_data = $this->get_template_by_gid($service_data["template_gid"]);

		$model_name = ucfirst($template_data["callback_model"]);
		$model_path = strtolower($template_data["callback_module"]."/models/").$model_name;
		$this->CI->load->model($model_path);
		$return = $this->CI->$model_name->$template_data["callback_validate_method"]($id_user, $user_data, $service_data, $price);
		return $return;
	}

	/**
	 * Service payment by write off from account
	 * 
	 * @param integer $id_service service identifier
	 * @param integer $id_user user identifier
	 * @param array $user_data user data
	 * @param float $price service price
	 * @return boolean
	 */
	public function account_payment($id_service, $id_user, $user_data, $price){
		if($this->pg_module->is_module_installed('users_payments')) {
			$this->CI->load->model("Users_payments_model");

			$service_data = $this->get_service_by_id($id_service);
			$t = $this->format_service(array($service_data)); $service_data = $t[0];
			$message = l('service_payment', 'services').$service_data["name"];

			$return = $this->CI->Users_payments_model->write_off_user_account($id_user, $price, $message);
			if($return === true){
				//// log info
				//$this->add_service_log($id_user, $id_service, $user_data);
				$payment_data = array(
					"id_user" => $id_user,
					"amount" => $price,
					"payment_data" => array("id_service" => $id_service, "user_data" => $user_data),
				);
				$this->payment_service_status($payment_data, 1);
				return true;
			}
			else
			return $return;
		} return false;
	}

	/**
	 * Service payment by payment system
	 * 
	 * @param integer $system_id payment system identifer
	 * @param integer $id_user user identifier
	 * @param integer $id_service service identifier
	 * @param array $user_data user data
	 * @param float $price service price
	 * @return boolean
	 */
	public function system_payment($system_gid, $id_user, $id_service, $user_data, $price){
		//// log info
		//$this->add_service_log($id_user, $id_service, $user_data);

		$service_data = $this->get_service_by_id($id_service);
		$t = $this->format_service(array($service_data)); $service_data = $t[0];

		$this->CI->load->model("payments/models/Payment_currency_model");
		$base_currency = $this->CI->Payment_currency_model->get_currency_default(true);
		
		$payment_data["name"] = l('service_payment', 'services').$service_data["name"];
		$payment_data["id_service"] = $id_service;
		$payment_data["user_data"] = $user_data;

		$this->CI->load->helper('payments');
		send_payment('services', $id_user, $price, $base_currency['gid'], $system_gid, $payment_data, true);
	}

	/**
	 * Set status of payment service
	 * 
	 * This method is callback for payment module
	 * 
	 * @param array $payment_data payment data
	 * @param integer $payment_status payment status
	 * @return void
	 */
	public function payment_service_status($payment_data, $payment_status){
		if($payment_status == 1){
			$user_id = $payment_data["id_user"];
			$service_id = $payment_data["payment_data"]["id_service"];
			$service_user_data = $payment_data["payment_data"]["user_data"];
			$price = $payment_data["amount"];

			$service_data = $this->get_service_by_id($service_id);
			$service_admin_data = isset($service_data["data_admin_array"])?$service_data["data_admin_array"]:array();
			$template_data = $this->get_template_by_gid($service_data["template_gid"]);

			$model_name = ucfirst($template_data["callback_model"]);
			$model_path = strtolower($template_data["callback_module"]."/models/").$model_name;
			$this->CI->load->model($model_path);
			$this->CI->$model_name->$template_data["callback_method"]($user_id, $price, $service_admin_data, $service_user_data);
			
			$this->add_service_log($user_id, $service_id, $service_user_data);
		}
		return;
	}

	/**
	 * Check service is used
	 * 
	 * @param string $gid service guid
	 * @return integer
	 */
	public function is_service_active($gid = '') {
		$s = $this->get_service_by_gid($gid);
		return intval($s['status']);
	}

	/**
	 * Returns langs data
	 *
	 * @param array $items data guids
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function export_langs($items, $langs_ids = null) {

		foreach($items as $type => $gids) {
			if ($type != 'param'){
				$method = 'get_' . $type . '_by_gid';
				foreach($gids as $gid) {
					$element = $this->$method($gid);
					$services[$gid] = $type . '_name_'. $element['id'];
				}
			} else {
				$method = 'get_template_by_gid';
				foreach($gids as $template => $param_gids) {
					$element = $this->$method($template);
					if (is_array($param_gids)){
						foreach ($param_gids as $k => $v) {
							$services[$template."_".$v] = 'admin_param_name_'.$element['id'].'_'.$v;
						}
					} else {
						$services[$template."_".$param_gids] = 'admin_param_name_'.$element['id'].'_'.$param_gids;
					}
				}
			}
		}
		$langs_db = $this->CI->pg_language->export_langs('services', $services, $langs_ids);
		$lang_codes = array_keys($langs_db);
		foreach($langs_ids as $lang_code) {
			//$lang_data[$lang_code] = array_combine(array_keys($services), $langs_db[$lang_code]);
			foreach ($services as $key => $value) {
				$lang_data[$key][$lang_code] = $langs_db[$value][$lang_code];
			}
		}
		return $lang_data;
	}

	/**
	 * Updates langs data
	 *
	 * @param array $services data guids 
	 * @param array $langs_data languages data
	 * @return boolean
	 */
	public function update_langs($services, $langs_data) {
		foreach($services as $type => $gids) {
			if ($type != 'param'){
				$method = 'get_' . $type . '_by_gid';
				foreach($gids as $gid) {
					$element = $this->$method($gid);
					$lang_data = $langs_data[$gid];
					$this->CI->pg_language->pages->set_string_langs('services',
																	$type . '_name_' . $element['id'],
																	$lang_data,
																	array_keys($lang_data));
				}
			} else {
				$method = 'get_template_by_gid';
				foreach($gids as $template => $param_gids) {
					$element = $this->$method($template);
					if (is_array($param_gids)){
						foreach ($param_gids as $k => $v) {
							$lang_data = $langs_data[$template."_".$v];
							$this->CI->pg_language->pages->set_string_langs('services', 
																			'admin_param_name_'.$element['id'].'_'.$v, 
																			$lang_data, 
																			array_keys($lang_data));
						}
					} else {
						$lang_data = $langs_data[$template."_".$param_gids];
						$this->CI->pg_language->pages->set_string_langs('services', 
																		'admin_param_name_'.$element['id'].'_'.$param_gids, 
																		$lang_data, 
																		array_keys($lang_data));
					}
				}
			}
		}
		return true;
	}
	
	/**
	 * Return active services by templates guids
	 * 
	 * @param array $templates_gids templates guids
	 */
	public function get_active_services($user_id, $templates_gids){
		$services_params["where_in"]["template_gid"] = (array)$templates_gids;
		$services_params["where"]["status"] = 1;
	    $services = $this->get_service_list($services_params);
	    foreach($services as $key=>$service){
			if($service['is_trial']){
				$is_available = $this->is_trial_available($user_id, $service['id'], $service['trial_count']);
				if(!$is_available) unset($service[$key]);
			}
		}
		return $services;
	}
	
	/**
	 * Check trial services is available
	 * 
	 * @param integer $user_id user identifier
	 * @param string $service_id service identifier
	 * @param integer $trial_count number of trial used
	 * @return boolean
	 */
	public function is_trial_available($user_id, $service_id, $trial_count){
		$this->DB->select('COUNT(*) AS cnt');
		$this->DB->from(SERVICES_LOG_TABLE);
		$this->DB->where('id_service', $service_id);
		$this->DB->where('id_user', $user_id);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]['cnt']) < $trial_count;
		}
		return true;
	}
}

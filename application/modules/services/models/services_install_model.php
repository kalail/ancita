<?php

/**
 * Services install model
 *
 * @package PG_RealEstate
 * @subpackage Services
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Services_install_model extends Model
{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	protected $CI;
	
	/**
	 * Menu configuration
	 * 
	 * @var array
	 */
	protected $menu = array(
		'admin_payments_menu' => array(
			'action' => 'none',
			'items' => array(
				'services_menu_item' => array('action' => 'create', 'link' => 'admin/services', 'status' => 1)
			)
		)
	);
	
	/**
	 * Payments configuration
	 * 
	 * @var array
	 */
	protected $payment_types = array(
		array(
			'gid' => 'services', 
			'callback_module' => 'services', 
			'callback_model' => 'Services_model', 
			'callback_method' => 'payment_service_status',
		),
	);
	
	/**
	 * Constructor
	 *
	 * @return Services_install object
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->CI->load->model('Install_model');
	}

	/**
	 * Install data of menu module
	 * 
	 * @return void
	 */
	public function install_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]['id'] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, 'create', $gid, 0, $this->menu[$gid]["items"]);
		}
	}

	/**
	 * Import languages of menu module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_menu_lang_update($langs_ids = null) {
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read('services', 'menu', $langs_ids);

		if(!$langs_file) { log_message('info', 'Empty menu langs data'); return false; }

		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, 'update', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages of menu module
	 * 
	 * @return array
	 */
	public function install_menu_lang_export($langs_ids) {
		if(empty($langs_ids)) return false;
		$this->CI->load->helper('menu');

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, 'export', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array( "menu" => $return );
	}

	/**
	 * Uninstall data of menu module
	 * 
	 * @return void
	 */
	public function deinstall_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			if($menu_data['action'] == 'create'){
				linked_install_set_menu($gid, 'delete');
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]['items']);
			}
		}
	}

	/**
	 * Install data of payments module
	 * 
	 * @return void
	 */
	public function install_payments() {
		// add account payment type
		$this->CI->load->model("Payments_model");
		foreach($this->payment_types as $payment_type) {
			$data = array(
				'gid' => $payment_type['gid'],
				'callback_module' => $payment_type['callback_module'],
				'callback_model' => $payment_type['callback_model'],
				'callback_method' => $payment_type['callback_method'],
			);
			$this->CI->Payments_model->save_payment_type(null, $data);
		}
	}

	/**
	 * Import languages of payments module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_payments_lang_update($langs_ids = null) {
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read('services', 'payments', $langs_ids);
		if(!$langs_file) { log_message('info', 'Empty payments langs data'); return false; }
		$this->CI->load->model('Payments_model');
		$this->CI->Payments_model->update_langs($this->payment_types, $langs_file, $langs_ids);
		return true;
	}

	/**
	 * Export languages of payments module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_payments_lang_export($langs_ids = null) {
		$this->CI->load->model('Payments_model');
		$return = $this->CI->Payments_model->export_langs($this->payment_types, $langs_ids);
		return array( "payments" => $return );
	}

	/**
	 * Uninstall data of payments module
	 * 
	 * @return void
	 */
	public function deinstall_payments() {
		$this->CI->load->model('Payments_model');
		foreach($this->payment_types as $payment_type) {
			$this->CI->Payments_model->delete_payment_type_by_gid($payment_type['gid']);
		}
	}
	
	/**
	 * Install module data
	 * 
	 * @return void
	 */
	public function _arbitrary_installing() {
		
	}

	/**
	 * Uninstall module data
	 * 
	 * @return void
	 */
	public function _arbitrary_deinstalling(){
		
	}
}

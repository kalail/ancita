<?php

$install_lang["pay_type"]["header"] = "Tipo de pago";
$install_lang["pay_type"]["option"]["1"] = "Cuenta solamente";
$install_lang["pay_type"]["option"]["2"] = "Cuenta + pago directo";
$install_lang["pay_type"]["option"]["3"] = "Pago directo";

$install_lang["price_type"]["header"] = "Tipo de precio";
$install_lang["price_type"]["option"]["1"] = "Precio definido por admin";
$install_lang["price_type"]["option"]["2"] = "Precio definido por el usuario";
$install_lang["price_type"]["option"]["3"] = "Precio definido por el script";



<?php

$install_lang["pay_type"]["header"] = "Тип платежа";
$install_lang["pay_type"]["option"]["1"] = "Списание со счета";
$install_lang["pay_type"]["option"]["2"] = "Списание + Прямой платеж";
$install_lang["pay_type"]["option"]["3"] = "Прямой платеж";

$install_lang["price_type"]["header"] = "Способ формирования цены";
$install_lang["price_type"]["option"]["1"] = "Цена определяется администратором";
$install_lang["price_type"]["option"]["2"] = "Цена определяется пользователем";
$install_lang["price_type"]["option"]["3"] = "Цена определяется скриптом";



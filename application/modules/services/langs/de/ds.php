<?php

$install_lang["pay_type"]["header"] = "Zahlungsart";
$install_lang["pay_type"]["option"]["1"] = "Nur Konto";
$install_lang["pay_type"]["option"]["2"] = "Konto + Direktzahlung";
$install_lang["pay_type"]["option"]["3"] = "Direktzahlung";

$install_lang["price_type"]["header"] = "Preisart";
$install_lang["price_type"]["option"]["1"] = "Preis ist vom Admin eingestellt";
$install_lang["price_type"]["option"]["2"] = "Preis ist vom Benutzer eingestellt";
$install_lang["price_type"]["option"]["3"] = "Preis ist vom Skript eingestellt";



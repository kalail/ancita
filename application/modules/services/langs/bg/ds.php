<?php

$install_lang["pay_type"]["header"] = "Тип плащане";
$install_lang["pay_type"]["option"]["1"] = "От сметка";
$install_lang["pay_type"]["option"]["2"] = "От сметка + Директно плащане";
$install_lang["pay_type"]["option"]["3"] = "Директно плащане";

$install_lang["price_type"]["header"] = "Начин за оформяне на цена";
$install_lang["price_type"]["option"]["1"] = "Цената е определена от администратора";
$install_lang["price_type"]["option"]["2"] = "Цената е определена от потребителя";
$install_lang["price_type"]["option"]["3"] = "Цената се определя от скрипта";



<?php

$install_lang["pay_type"]["header"] = "Type de paiement";
$install_lang["pay_type"]["option"]["1"] = "Compte seulement";
$install_lang["pay_type"]["option"]["2"] = "Compte + paiement direct";
$install_lang["pay_type"]["option"]["3"] = "Paiement direct";

$install_lang["price_type"]["header"] = "Type de prix";
$install_lang["price_type"]["option"]["1"] = "Prix décidé par l'administrateur";
$install_lang["price_type"]["option"]["2"] = "Prix décidé par l'utilisateur";
$install_lang["price_type"]["option"]["3"] = "Prix décidé par le script";



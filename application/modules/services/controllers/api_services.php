<?php

/**
 * Services api controller
 *
 * @package PG_RealEstate
 * @subpackage Services
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Alexander Batukhtin <abatukhtin@pilotgroup.net>
 **/
class Api_Services extends Controller
{
	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::Controller();
		$this->load->model("Services_model");
	}

	/**
	 * Service form
	 *
	 * @param string $service_gid
	 * @param array $user_form_data
	 */
	public function get_form(){

		$service_gid = $this->input->post('service_gid', true);
		if(!$service_gid) {
			log_message('error', 'services API: Empty service gid');
			$this->set_api_content('errors', l('error_service_code_incorrect', 'services'));
			return false;
		}
		$user_form_data = $this->input->post('user_form_data', true);

		$user_id = $this->session->userdata('user_id');
		$this->load->model('users/models/Auth_model');
		$this->Auth_model->update_user_session_data($user_id);

		$service = $this->Services_model->get_service_by_gid($service_gid);
		if(!$service['status']) {
			log_message('error', 'services API: Wrong service status');
			$this->set_api_content('errors', l('error_service_code_incorrect', 'services'));
			return false;
		}

		$t = $this->Services_model->format_service(array($service));
		$service = $t[0];
		$service['template'] = $this->Services_model->format_template($service['template']);
		if(!empty($service['data_admin_array'])){
			foreach($service['template']['data_admin_array'] as $gid => $temp){
				if(!empty($service['data_admin_array'][$gid])){
					$service['template']['data_admin_array'][$gid]['value'] = $service['data_admin_array'][$gid];
				}
			}
		}

		if($service['template']['price_type'] == '2' || $service['template']['price_type'] == '3'){
			$service['price'] = $this->input->post('price', true);
		}

		if(!empty($service['template']['data_user_array'])){
			foreach($service['template']['data_user_array'] as $gid => $temp){
				$value = '';
				if($temp['type'] == 'hidden'){
					$value = $this->input->get_post($gid, true);
				}
				if(isset($user_form_data[$gid])){
					$value = $user_form_data[$gid];
				}
				$service['template']['data_user_array'][$gid]['value'] = $value;
			}
		}

		// get payments types
		$service['free_activate'] = false;
		if($service['price'] <= 0){
			$service['free_activate'] = true;
		}
		if($service['pay_type'] == 1 || $service['pay_type'] == 2){
			$this->load->model('Users_payments_model');
			$service['user_account'] = $this->Users_payments_model->get_user_account($user_id);
			if($service["user_account"] <= 0 && $service['price'] > 0){
				$service['disable_account_pay'] = true;
			}elseif(($service['template']['price_type'] == 1 || $service['template']['price_type'] == 3) && $service['price'] > $service['user_account']){
				$service['disable_account_pay'] = true;
			}
		}

		if($service['pay_type'] == 2 || $service['pay_type'] == 3){
			$this->load->model('payments/models/Payment_systems_model');
			$data['billing_systems'] = $this->Payment_systems_model->get_active_system_list();
		}
		$data['service'] = $service;
		$data['users_payments_module_installed'] = $this->pg_module->is_module_installed('users_payments');
		$this->set_api_content('data', $data);
	}

	/**
	 * Send service form
	 *
	 * @param string $service_gid
	 * @param array $user_data
	 * @param string $payment_type account|system
	 * @param string $system_gid If $payment_type === 'system'
	 */
	public function send_form() {
		$service_gid = $this->input->post('service_gid', true);
		if(!$service_gid) {
			log_message('error', 'services API: Empty service gid');
			$this->set_api_content('errors', l('error_service_code_incorrect', 'services'));
			return false;
		}

		$service = $this->Services_model->get_service_by_gid($service_gid);
		$user_data = $this->input->post('user_data', true);

		$user_id = $this->session->userdata('user_id');
		$this->load->model('users/models/Auth_model');
		$this->Auth_model->update_user_session_data($user_id);

		$service_return = $this->Services_model->validate_service_payment($service['id'], $user_data, $service['price']);
		if(!empty($service_return['errors'])){
			$this->set_api_content('errors', $service_return['errors']);
			return false;
		}
		$origin_return = $this->Services_model->validate_service_original_model($service['id'], $user_data, $user_id, $service['price']);
		if(!empty($origin_return['errors'])){
			$this->set_api_content('errors', $origin_return['errors']);
			return false;
		}
		$payment_type = $this->input->post('payment_type');

		if('account' === $payment_type){
			$return = $this->Services_model->account_payment($service['id'], $user_id, $user_data, $service['price']);
			if($return !== true){
				$this->set_api_content('errors', $return);
			}else{
				$this->set_api_content('messages', l('success_services_apply', 'services'));
				/*$redirect = $this->session->userdata('service_redirect');
				$this->session->set_userdata(array('service_redirect'=>''));
				$this->load->model('users/models/Auth_model');
				$this->Auth_model->update_user_session_data($user_id);
				redirect($redirect);*/
			}
		}elseif('system' === $payment_type){
			$system_gid = $this->input->post('system_gid', true);
			$data['system_gid'] = $system_gid;
			if(empty($system_gid)){
				$this->set_api_content('messages', l('error_select_payment_system', 'services'));
			}else{
				$this->Services_model->system_payment($system_gid, $user_id, $service['id'], $user_data, $service['price']);
				/*$redirect = $this->session->userdata('service_redirect');
				$this->session->set_userdata(array('service_redirect'=>''));
				$this->load->model('users/models/Auth_model');
				$this->Auth_model->update_user_session_data($user_id);
				redirect($redirect);*/
			}
		}
		$data['service_gid'] = $service_gid;
		$data['user_data'] = $user_data;
		$data['payment_type'] = $payment_type;
		$data['service_gid'] = $service_gid;
		$this->set_api_content('data', $data);
	}

}

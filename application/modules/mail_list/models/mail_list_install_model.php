<?php

/**
 * Mail List Install Model
 *
 * @package PG_RealEstate
 * @subpackage Mail list
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Mail_list_install_model extends Model
{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	protected $CI;
	
	/**
	 * Menu configuration
	 * 
	 * @var array
	 */
	protected $menu = array(
		'admin_menu' => array(
			'action' => 'none',
			'items' => array(
				'other_items' => array(
					'action' => 'none',
					'items' => array(
						'newsletters-items' => array(
							'action'=>'none',
							'items' => array(
								'mail_list_menu_item' => array('action' => 'create', 'link' => 'admin/mail_list/users', 'status' => 1, 'sorter' => 2)
							)
						)
					)
				)
			)
		),
		'admin_mail_list_menu' => array(
			'action' => 'create',
			'name' => 'Admin mode - Content - Mailing lists',
			'items' => array(
				'mail_list_users_item' => array('action' => 'create', 'link' => 'admin/mail_list/users', 'status' => 1),
				'mail_list_filters_item' => array('action' => 'create', 'link' => 'admin/mail_list/filters', 'status' => 1)
			)
		)
	);
	
	/**
	 * Ausers configuration
	 * 
	 * @var array
	 */
	protected $ausers_methods = array(
		array('module' => 'mail_list', 'method' => 'users', 'is_default' => 1),
		array('module' => 'mail_list', 'method' => 'filters', 'is_default' => 0),
	);

	/**
	 * Constructor
	 *
	 * @return Mail_list_install object
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->CI->load->model('Install_model');
	}

	/**
	 * Install data of menu module
	 * 
	 * @return void
	 */
	public function install_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]['id'] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, 'create', $gid, 0, $this->menu[$gid]["items"]);
		}
	}

	/**
	 * Install languages of menu module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_menu_lang_update($langs_ids = null) {
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read('mail_list', 'menu', $langs_ids);

		if(!$langs_file) { log_message('info', 'Empty menu langs data'); return false; }

		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, 'update', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages of menu module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_menu_lang_export($langs_ids) {
		if(empty($langs_ids)) return false;
		$this->CI->load->helper('menu');

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, 'export', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array( "menu" => $return );
	}
	
	/**
	 * Uninstall data of menu module
	 * 
	 * @return void
	 */
	public function deinstall_menu() {
		$this->CI->load->model('Menu_model');

		$menu = $this->CI->Menu_model->get_menu_by_gid('admin_menu');
		$item = $this->CI->Menu_model->get_menu_item_by_gid('mail_list_menu_item', $menu['id']);
		$this->CI->Menu_model->delete_menu_item($item['id']);

		$menu = $this->CI->Menu_model->get_menu_by_gid('admin_mail_list_menu');
		$this->CI->Menu_model->delete_menu($menu['id']);
	}

	/**
	 * Install data of ausers module
	 * 
	 * @return void
	 */
	public function install_ausers() {
		// install ausers permissions
		$this->CI->load->model('Ausers_model');

		foreach($this->ausers_methods as $method){
			$this->CI->Ausers_model->save_method(null, $method);
		}
	}

	/**
	 * Import languages of ausers module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_ausers_lang_update($langs_ids = null) {
		$langs_file = $this->CI->Install_model->language_file_read('mail_list', 'ausers', $langs_ids);

		// install ausers permissions
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'mail_list';
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method['method']])){
				$this->CI->Ausers_model->save_method($method['id'], array(), $langs_file[$method['method']]);
			}
		}
		
		return true;
	}

	/**
	 * Export languages of ausers module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_ausers_lang_export($langs_ids) {
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'mail_list';
		$methods =  $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method['method']] = $method['langs'];
		}
		return array('ausers' => $return);
	}

	/**
	 * Uninstall data of ausers module
	 * 
	 * @return void
	 */
	public function deinstall_ausers() {
		// delete moderation methods in ausers
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'mail_list';
		$this->CI->Ausers_model->delete_methods($params);
	}

	/**
	 * Install module data
	 * 
	 * @return void
	 */
	public function _arbitrary_installing(){

	}

	/**
	 * Uninstall module data
	 * 
	 * @return void
	 */
	public function _arbitrary_deinstalling(){

	}
}

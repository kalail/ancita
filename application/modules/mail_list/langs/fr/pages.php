<?php

$install_lang["admin"] = "Paramètres de liste de courrier";
$install_lang["admin_header_mail_list"] = "Paramètres de liste de courrier";
$install_lang["admin_header_mail_list_edit"] = "Éditer liste de courrier";
$install_lang["admin_header_mail_list_filters"] = "Filtres";
$install_lang["admin_header_mail_list_users"] = "Utilisateurs";
$install_lang["admin_header_mail_lists_list"] = "Paramètres de liste de courrier";
$install_lang["admin_header_mail_lists_settings"] = "Paramètres de liste de courrier";
$install_lang["admin_header_subscription"] = "Abonnement";
$install_lang["admin_header_users_data"] = "Données d'utilisateurs";
$install_lang["btn_save_filter"] = "Sauver filtre";
$install_lang["field_date"] = "Enregistrement";
$install_lang["field_email"] = "Courriel";
$install_lang["field_filter_criteria"] = "Critère de filtre";
$install_lang["field_filter_date"] = "Date de création";
$install_lang["field_id_city"] = "Ville";
$install_lang["field_id_country"] = "Pays";
$install_lang["field_id_district"] = "District";
$install_lang["field_id_region"] = "Région";
$install_lang["field_location"] = "Lieu";
$install_lang["field_nickname"] = "Nom";
$install_lang["field_registration_date"] = "Enregistrement";
$install_lang["field_subscription"] = "Abonnement";
$install_lang["field_user_type"] = "Type d'utilisateur";
$install_lang["filter_all"] = "Tous";
$install_lang["filter_not_subscribed"] = "Pas abonné";
$install_lang["filter_subscribed"] = "Abonné";
$install_lang["id_field_subscription"] = "Liste de courier";
$install_lang["link_delete"] = "Supprimer";
$install_lang["link_search"] = "Recherche";
$install_lang["link_subscribe_all"] = "Abonner à tous";
$install_lang["link_subscribe_selected"] = "Abonner à sélection";
$install_lang["link_subscribe_user"] = "Abonner utilisateur";
$install_lang["link_unsubscribe_all"] = "Désabonner tous";
$install_lang["link_unsubscribe_selected"] = "Désabonner sélection";
$install_lang["link_unsubscribe_user"] = "Désabonner utilisateur";
$install_lang["mailing_lists"] = "Liste de courier";
$install_lang["no_searches"] = "Aucune recherche";
$install_lang["users"] = "Utilisateurs";


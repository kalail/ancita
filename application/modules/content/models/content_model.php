<?php

require_once SITE_PHYSICAL_PATH . 'system/libraries/PHPExcel.php';

if (!defined('CONTENT_TABLE')) define('CONTENT_TABLE', DB_PREFIX.'content');
if (!defined('CITIES_TABLE')) define('CITIES_TABLE', DB_PREFIX.'cnt_cities');
if (!defined('REGIONS_TABLE')) define('REGIONS_TABLE', DB_PREFIX.'cnt_regions');
if (!defined('COUNTRIES_TABLE')) define('COUNTRIES_TABLE', DB_PREFIX.'cnt_countries');
if (!defined('DISTRICTS_TABLE')) define('DISTRICTS_TABLE', DB_PREFIX.'cnt_districts');
if (!defined('CITY_PRICES_TABLE')) define('CITY_PRICES_TABLE', DB_PREFIX.'market_city_price');
if (!defined('DISTRICTS_ADJUSTMENT_TABLE')) define('DISTRICTS_ADJUSTMENT_TABLE', DB_PREFIX.'market_district_price');
if (!defined('FACTORS_TABLE')) define('FACTORS_TABLE', DB_PREFIX.'market_factors');
if (!defined('VERSION_TABLE')) define('VERSION_TABLE', DB_PREFIX.'market_version');
if (!defined('CITY_APARTMENTS_PRICES_GRAPH')) define('CITY_APARTMENTS_PRICES_GRAPH', DB_PREFIX.'market_city_apartments_prices_graph');
if (!defined('CITY_HOUSES_PRICES_GRAPH')) define('CITY_HOUSES_PRICES_GRAPH', DB_PREFIX.'market_city_houses_prices_graph');
if (!defined('PROVINCE_APARTMENTS_PRICES_GRAPH')) define('PROVINCE_APARTMENTS_PRICES_GRAPH', DB_PREFIX.'market_province_apartments_prices_graph');
if (!defined('PROVINCE_HOUSES_PRICES_GRAPH')) define('PROVINCE_HOUSES_PRICES_GRAPH', DB_PREFIX.'market_province_houses_prices_graph');
if (!defined('FACTORS_GRAPH_TABLE')) define('FACTORS_GRAPH_TABLE', DB_PREFIX.'market_factors_graph');
if (!defined('USERS_EXTEND_TABLE')) define('USERS_EXTEND_TABLE', DB_PREFIX.'users_extend_data');
if (!defined('USERS_EXTEND_TEXT_TABLE')) define('USERS_EXTEND_TEXT_TABLE', DB_PREFIX.'users_extend_agent_text');

/**
 * Content main model
 *
 * @package PG_RealEstate
 * @subpackage Content
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Content_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	public $CI;
	
	/**
	 * Link to database object
	 * 
	 * @var object
	 */
	public $DB;

	/**
	 * All attributes of content object
	 * 
	 * @var array
	 */
	public $fields_all = array(
		"id",
		"lang_id",
		"parent_id",
		"gid",
		"title",
		"content",
		"sorter",
		"status",
		'date_created',
		'date_modified',
		'id_seo_settings',
	);

	/**
	 * Listing attributes of content object
	 * 
	 * @var array
	 */
	public $fields_list = array(
		"id",
		"lang_id",
		"parent_id",
		"gid",
		"title",
		"sorter",
		"status",
		'date_created',
		'date_modified',
	);

	/**
	 * Current active item data by language
	 * 
	 * @var array
	 */
	public $curent_active_item_id = array();

	/**
	 * Generate tree buffer
	 * 
	 * @var array
	 */
	public $temp_generate_raw_tree = array();
	
	/**
	 * Generate tree item buffer
	 * 
	 * @var array
	 */
	public $temp_generate_raw_items = array();

	/**
	 * Constructor
	 * 
	 * @return Content_model
	 */
	public function Content_model(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Return information pages as array
	 * 
	 * @param integer $lang_id language identifier
	 * @param integer $parent_id parent page identifier
	 * @param array $params filters parameters
	 * @return array
	 */
	public function get_pages_list($lang_id, $parent_id=0, $params=array()){
		$this->DB->select(implode(", ", $this->fields_list));
		$this->DB->from(CONTENT_TABLE);
		$this->DB->where("lang_id", $lang_id);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$this->DB->order_by("parent_id ASC");
		$this->DB->order_by("sorter ASC");

		$this->temp_generate_raw_items = $this->temp_generate_raw_tree = array();
		$results = $this->DB->get()->result_array();

		if(!empty($results) && is_array($results)){
			$active_parent_id = array();
			foreach($results as $r){
				$r["active"] = $this->_is_active_item($r);
				if($r["active"]){
					$active_parent_id[] = $r["parent_id"];
				}
				$this->temp_generate_raw_items[$r["id"]] = $r;
			}

			if(!empty($active_parent_id)){
				$this->_set_active_chain($active_parent_id);
			}

			foreach($this->temp_generate_raw_items as $r){
				$this->temp_generate_raw_tree[$r["parent_id"]][] = $r;
			}

			$tree = $this->_generate_tree($parent_id);
			return $tree;
		}

		return false;

	}

	/**
	 * Return active information pages as array
	 * 
	  * @param integer $lang_id language identifier
	 * @param integer $parent_id parent page identifier
	 * @param array $params filters parameters
	 * @return array
	 */
	public function get_active_pages_list($lang_id, $parent_id=0, $params=array()){
		$params["where"]["status"] = 1;
		return $this->get_pages_list($lang_id, $parent_id, $params);
	}

	/**
	 * Return number of information pages
	 * 
	 * @param integer $lang_id language identifier
	 * @param array $params filters parameters
	 * @return integer
	 */
	public function get_pages_count($lang_id, $params=array()){
		$this->DB->select("COUNT(*) AS cnt");
		$this->DB->from(CONTENT_TABLE);
		$this->DB->where("lang_id", $lang_id);

		if(isset($params["where"]) && is_array($params["where"]) && count($params["where"])){
			foreach($params["where"] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])){
			foreach($params["where_in"] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])){
			foreach($params["where_sql"] as $value){
				$this->DB->where($value);
			}
		}

		$result = $this->DB->get()->result();
		if(!empty($result)){
			return intval($result[0]->cnt);
		}else{
			return 0;
		}
	}

	/**
	 * Return number of active information pages
	 * 
	 * @param integer $lang_id language identifier
	 * @param array $params filters parameters
	 * @return integer
	 */
	public function get_active_pages_count($lang_id, $params=array()){
		$params["where"]["status"] = 1;
		return $this->get_pages_count($lang_id, $params);
	}

	/**
	 * Return page object by identifier
	 * 
	 * @param integer $page_id page identufier
	 * @return array
	 */
	public function get_page_by_id($page_id){
		$page_data = array();
		$result = $this->DB->select(implode(", ", $this->fields_all))->from(CONTENT_TABLE)->where("id", $page_id)->get()->result_array();
		if(!empty($result)){
			$page_data = $result[0];
		}
		return $page_data;
	}

	/**
	 * Return page object by GUID
	 * 
	 * @param integer $lang_id language identifier
	 * @param string $gid page GUID
	 * @return array
	 */
	public function get_page_by_gid($lang_id, $gid){
		$page_data_arr = $page_data = array();
		$default_lang_id = $this->CI->pg_language->get_default_lang_id();
		$result = $this->DB->select(implode(", ", $this->fields_all))->from(CONTENT_TABLE)->where("gid", $gid)->get()->result_array();
		if(!empty($result)){
			foreach($result as $r){
				$page_data_arr[$r["lang_id"]] = $r;
			}

			if(isset($page_data_arr[$lang_id])){
				$page_data = $page_data_arr[$lang_id];
			}elseif(isset($page_data_arr[$default_lang_id])){
				$page_data = $page_data_arr[$default_lang_id];
			}elseif(!empty($page_data_arr)){
				$page_data = current($page_data_arr);
			}
		}
		return $page_data;
	}

	/**
	 * Save page object to data source
	 * 
	 * @param integer $page_id page identifier
	 * @param array $atttrs page data
	 * @return integer
	 */
	public function save_page($page_id, $attrs){
		if (is_null($page_id)){
			$attrs["date_created"] = $attrs["date_modified"] = date("Y-m-d H:i:s");

			if(!isset($attrs["status"])) $attrs["status"] = 1;
			if(!isset($attrs["sorter"]) && isset($attrs["lang_id"])){
				$sorter_params["where"]["parent_id"] = isset($attrs["parent_id"])?$attrs["parent_id"]:0;
				$attrs["sorter"] = $this->get_pages_count($attrs["lang_id"], $sorter_params)+1;
			}
			$this->DB->insert(CONTENT_TABLE, $attrs);
			$page_id = $this->DB->insert_id();
		}else{
			$attrs["date_modified"] = date("Y-m-d H:i:s");
			$this->DB->where('id', $page_id);
			$this->DB->update(CONTENT_TABLE, $attrs);
		}
		return $page_id;
	}

	/**
	 * Validate page data
	 * 
	 * @param integer $page_id page identifier
	 * @param array $data page data
	 * @return array
	 */
	public function validate_page($page_id, $data){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data["title"])){
			$return["data"]["title"] = strip_tags($data["title"]);
			if(empty($return["data"]["title"]) ){
				$return["errors"][] = l('error_content_title_invalid', 'content');
			}
		}

		if(isset($data["gid"])){
			$this->CI->config->load("reg_exps", TRUE);
			$reg_exp = $this->CI->config->item("not_literal", "reg_exps");
			$temp_gid = $return["data"]["gid"] = strtolower(trim(strip_tags($data["gid"])));
			if(!empty($temp_gid)){
				$return["data"]["gid"] = preg_replace($reg_exp, '-', $return["data"]["gid"]);
				$return["data"]["gid"] = preg_replace("/[\-]{2,}/i", '-', $return["data"]["gid"]);
				$return["data"]["gid"] = trim($return["data"]["gid"], '-');
				if(empty($return["data"]["gid"])){
					$return["data"]["gid"] = md5($temp_gid);
				}
			}else{
				$return["errors"][] = l('error_content_gid_invalid', 'content');
			}
		}

		if(isset($data["content"])){
			$return["data"]["content"] = $data["content"];
		}

		if(isset($data["lang_id"])){
			$return["data"]["lang_id"] = intval($data["lang_id"]);
		}

		if(isset($data["parent_id"])){
			$return["data"]["parent_id"] = intval($data["parent_id"]);
		}

		if(isset($data["sorter"])){
			$return["data"]["sorter"] = intval($data["sorter"]);
		}

		if(isset($data["status"])){
			$return["data"]["status"] = intval($data["status"]);
		}

		if(isset($data["id_seo_settings"])){
			$return["data"]["id_seo_settings"] = intval($data["id_seo_settings"]);
		}

		return $return;
	}

	/**
	 * Remove page object from data source
	 * 
	 * @param integer $page_id page identifier
	 * @return void
	 */
	public function delete_page($page_id){
		$page_data = $this->get_page_by_id($page_id);
		if(!empty($page_data)){
			$this->DB->where('id', $page_id);
			$this->DB->delete(CONTENT_TABLE);
			$this->resort_pages($page_data["lang_id"], $page_data["parent_id"]);
		}
		return;
	}

	/**
	 * Activate/de-activate page object
	 * 
	 * @param integer $page_id page identifier
	 * @param integer $status page status
	 * @return void
	 */
	public function activate_page($page_id, $status=1){
		$attrs["status"] = intval($status);
		$this->DB->where('id', $page_id);
		$this->DB->update(CONTENT_TABLE, $attrs);
	}

	/**
	 * Resort pages order
	 * 
	 * @param integer $lang_id language identifier
	 * @param integer $parent_id parent page identifier
	 * @return void
	 */
	public function resort_pages($lang_id, $parent_id=0){
		$results = $this->DB->select("id, sorter")->from(CONTENT_TABLE)->where("lang_id", $lang_id)->where("parent_id", $parent_id)->order_by('sorter ASC')->get()->result_array();
		if(!empty($results)){
			$i = 1;
			foreach($results as $r){
				$data["sorter"] = $i;
				$this->DB->where('id', $r["id"]);
				$this->DB->update(CONTENT_TABLE, $data);
				$i++;
			}
		}
	}

	/**
	 * Make page current
	 * 
	 * @param integer $lang_id language identifier
	 * @param integer $page_id page identifier
	 * @return boolean
	 */
	public function set_page_active($lang_id, $page_id){
		if(!$lang_id){
			return false;
		}
		if(!is_numeric($page_id)){
			$item = $this->get_page_by_gid($lang_id, $page_id);
			$page_id = $item["id"];
		}
		if(!$page_id){
			return false;
		}
		$this->curent_active_item_id[$lang_id] = $page_id;
		return;
	}

	///// inner functions
	
	/**
	 * Check page is current
	 * 
	 * @param array $item page data
	 * @return boolean
	 */
	public function _is_active_item($item){
		if(!empty($this->curent_active_item_id[$item["lang_id"]])){
			if($this->curent_active_item_id[$item["lang_id"]] == $item["id"]){
				return true;
			}
		}
		return false;
	}

	/**
	 * Generate chain to current page object
	 * 
	 * @param array $parent_ids parent page identifiers
	 * @return void
	 */
	public function _set_active_chain($parent_ids){
		foreach($parent_ids as $id){
			$parent_id = $id;
			do{
				$this->temp_generate_raw_items[$parent_id]["in_chain"] = true;
				$parent_id = $this->temp_generate_raw_items[$parent_id]["parent_id"];
			}while($parent_id > 0);
		}
	}

	/**
	 * Generate page tree
	 * 
	 * @param integer $parent_id root page identifier
	 * @return array
	 */
	public function _generate_tree($parent_id){

		if(empty($this->temp_generate_raw_tree) || empty($this->temp_generate_raw_tree[$parent_id])){
			return array();
		}

		$tree = array();
		foreach($this->temp_generate_raw_tree[$parent_id] as $subitem){
			if(isset($this->temp_generate_raw_tree[$subitem["id"]]) && !empty($this->temp_generate_raw_tree[$subitem["id"]])){
				$subitem["sub"] = $this->_generate_tree($subitem["id"]);
			}
			$tree[] = $subitem;
		}

		return $tree;
	}
	
	////// ------------------------------------- Start Marketing --------------------------------------------
	public function get_city_list($month, $year){
		$city_data = array();
		$query = "SELECT 
					city.id AS city_id, city.name AS city_name
					,reg.id AS region_id, reg.name AS region_name
					,cn.id AS country_id, cn.code AS country_code, cn.name AS country_name
					FROM ".CITIES_TABLE." city
					LEFT JOIN ".REGIONS_TABLE." reg ON reg.id = city.id_region
					LEFT JOIN ".COUNTRIES_TABLE." cn ON cn.code = reg.country_code
					WHERE
					cn.code = 'ES'
					ORDER BY reg.name DESC, city.name ASC";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$city_data[] = $rt;
			}
		}
		return $city_data;
	}
	
	public function get_city_price($month, $year, $cityId)
	{
		$factors_graph = $this->get_factor_graph_setting();
		
		$city_price = array();
		if($factors_graph['functionality'] != 'on') {
			$query = "SELECT * FROM ".CITY_PRICES_TABLE." WHERE Month = '".$month."' AND Year = '".$year."' AND id='".$cityId."'";
		}
		else
		{
			$query = "SELECT 
						cit.id AS city_id, cit.name AS city_name, capg.apartment_price AS appartment_price, chpg.house_price AS house_price 
						FROM ".CITIES_TABLE." cit
						LEFT JOIN ".CITY_APARTMENTS_PRICES_GRAPH." capg ON cit.id = capg.id_city
						LEFT JOIN ".CITY_HOUSES_PRICES_GRAPH." chpg ON cit.id = chpg.id_city
						AND capg.date = chpg.date
						WHERE capg.date = '" . $factors_graph['date'] . "' AND cit.id='".$cityId."' 
						ORDER BY cit.name ASC";
		}
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$city_price[] = $rt;
			}
		}
		return $city_price[0];
	}
	
	public function get_district_list_bycity($month, $year, $cityId)
	{
		$data = array();
		$query = "SELECT
					dist.id AS id,dist.name AS name
					FROM
					".DISTRICTS_TABLE." dist
					WHERE
					dist.id_city = ".$cityId; 
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data;
	}
	
	public function get_district_price($month, $year, $cityId, $districtId)
	{
		$data = array();
		$query = "SELECT * FROM ".DISTRICTS_ADJUSTMENT_TABLE." WHERE Month='".$month."' AND Year='".$year."' AND id_city = '".$cityId."' AND id_district='".$districtId."'";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data[0];
	}
	
	public function get_factors($month,$year)
	{
		$data = array();
		$query = "SELECT * FROM " . FACTORS_TABLE . " WHERE Month='".$month."' AND Year='".$year."'";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data[0];
	}
	
	public function save_custom_query($strSQL)
	{
		$this->DB->query($strSQL);
	}
	
	public function get_factor_graph_setting()
	{
		$data = array();
		$query = "SELECT functionality, date FROM " .FACTORS_GRAPH_TABLE. " WHERE id = 1";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data[0];
	}
	
	public function get_version_setting($tab_id)
	{
		$data = array();
		$query = "SELECT * FROM " . VERSION_TABLE . " WHERE VersionType='".$tab_id."'";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data[0];
	}
	
	public function get_all_cityname()
	{
		$data = array();
		$query = "SELECT name FROM " . CITIES_TABLE;
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data;
	}
	
	public function get_city_name_id()
	{
		$data = array();
		$query = "SELECT name,id FROM " . CITIES_TABLE;
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data;
	}
	
	public function get_region_name_id()
	{
		$data = array();
		$query = "SELECT name,id FROM " . REGIONS_TABLE;
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data;
	}
	
	public function delete_graph_appratment_house()
	{
		$strSQL = "DELETE FROM " . CITY_APARTMENTS_PRICES_GRAPH;
		$this->DB->query($strSQL);
		$strSQL = "DELETE FROM " . CITY_HOUSES_PRICES_GRAPH;
		$this->DB->query($strSQL);
		$strSQL = "DELETE FROM " . PROVINCE_APARTMENTS_PRICES_GRAPH;
		$this->DB->query($strSQL);
		$strSQL = "DELETE FROM " . PROVINCE_HOUSES_PRICES_GRAPH;
		$this->DB->query($strSQL);
	}
	
	public function save_graph_table_custom($strSQL)
	{
		$this->DB->query($strSQL);
	}
	
	public function save_city_price_appartment($month, $year, $id, $value)
	{
		$strSQL = " REPLACE INTO " . CITY_PRICES_TABLE . " (Month, Year, id, appartment_price) VALUES ('" . $month . "', '" . $year . "', '" . $id . "', '" . $value . "')";
		$this->DB->query($strSQL);
	}
	
	public function save_city_price_house($month, $year, $id, $value)
	{
		$strSQL = " UPDATE " . CITY_PRICES_TABLE . " SET house_price = '".$value."' WHERE  Month='".$month."' AND Year='".$year."' AND id='".$id."'";
		$this->DB->query($strSQL);
	}
	
	public function save_district_price($month, $year, $area_id, $city_id, $value)
	{
		$strSQL = " REPLACE INTO " . DISTRICTS_ADJUSTMENT_TABLE . " (Month, Year, id_district, id_city, adjustment) VALUES ('" . $month . "', '" . $year . "', '" . $area_id . "', '" . $city_id . "', '" . $value . "')";
		$this->DB->query($strSQL);
	}
	
	public function save_factor($strSQL)
	{
		$this->DB->query($strSQL);
	}
	
	public function save_version_setting($month, $year, $tab_id)
	{
		$strSQL = " UPDATE " . VERSION_TABLE . " SET Month = '".$month."', Year = '".$year."' WHERE VersionType='".$tab_id."'";
		$this->DB->query($strSQL);
	}
	
	////// ------------------------------------- End Marketing --------------------------------------------
	
	
	/////--------------------------------------- Start Agent view ------------------------------------------
	public function get_user_extend_data($userId){
		$query = "SELECT * FROM ".USERS_EXTEND_TABLE." WHERE id_user='".$userId."'";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		
		$user_text = $this->get_extend_text($userId);
		$data[0]['agent_text'] = $user_text['agent_text'];
		$data[0]['agent_text_additional'] = $user_text['agent_text_additional'];
		 
		return $data[0];
	}
	
	public function get_extend_text($userId){
		$id_lang = $this->CI->pg_language->current_lang_id;
		$query = "SELECT * FROM ".USERS_EXTEND_TEXT_TABLE." WHERE extend_user_id='".$userId."' and id_lang='".$id_lang."'";
		$result = $this->DB->query($query)->result();
		if(!empty($result)){
			foreach($result as $res_obj){
				$rt = get_object_vars($res_obj);
				$data[] = $rt;
			}
		}
		return $data[0];
	}
	/////----------------------------------------- End Agent view ------------------------------------------

	////// seo
	
	/**
	 * Return seo settings of content module
	 * 
	 * @param string $method method name
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function get_seo_settings($method='', $lang_id=''){
		if(!empty($method)){
			return $this->_get_seo_settings($method, $lang_id);
		}else{
			$actions = array('index', 'view');
			$return = array();
			foreach($actions as $action){
				$return[$action] = $this->_get_seo_settings($action, $lang_id);
			}
			return $return;
		}
	}

	/**
	 * Return seo settings of content module
	 * 
	 * @param string $method method name
	 * @param integer $lang_id language identifier
	 * @return array
	 */
	public function _get_seo_settings($method, $lang_id=''){
		if($method == "index"){
			return array(
				"templates" => array(),
				"url_vars" => array()
			);
		}elseif($method == "view"){
			return array(
				"templates" => array('title', 'gid'),
				"url_vars" => array(
					"gid" => array('gid'=>'literal')
				)
			);
		}
	}

	/**
	 * Replace seo parameters of request query
	 * 
	 * @param string $var_name_from variable from name
	 * @param string $var_name_to variable to name
	 * @param mixed $value parameter value
	 * @return mixed
	 */
	public function request_seo_rewrite($var_name_from, $var_name_to, $value){
		$user_data = array();

		if($var_name_from == $var_name_to){
			return $value;
		}

		if($var_name_from == "gid"){
			$lang_id = $this->CI->pg_language->current_lang_id;
			$page_data = $this->get_page_by_gid($lang_id, $value);
		}

		if($var_name_to == "id"){
			return $page_data["id"];
		}
	}

	/**
	 * Return data for xml sitemap
	 * 
	 * @return array
	 */
	public function get_sitemap_xml_urls(){
		$this->CI->load->helper('seo');
		
		$return = array(
			array(
				"url" => rewrite_link('content', 'index', array(), false, null, true),
				"priority" => 0.1
			),
		);

		$this->DB->select(implode(", ", $this->fields_list))->from(CONTENT_TABLE)->where('status', '1');
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$return[] = array(
					"url" => rewrite_link('content', 'view', $r, false, null, true),
					"priority" => 0.5
				);
			}
		}
		return $return;
	}

	/**
	 * Return data for sitemap page
	 * 
	 * @return array
	 */
	public function get_sitemap_urls(){
		$this->CI->load->helper('seo');

		$lang_id = $this->CI->pg_language->current_lang_id;
		$pages = $this->get_active_pages_list($lang_id, 0);
		$block = array();

		foreach($pages as $page){
			$sub = array();
			if(!empty($page["sub"])){
				foreach($page["sub"] as $sub_page){
					$sub[] = array(
						"name" => $sub_page["title"],
						"link" => rewrite_link('content', 'view', $sub_page),
						"clickable" => true
					);
				}
			}
			$block[] = array(
				"name" => $page["title"],
				"link" => rewrite_link('content', 'view', $page),
				"clickable" => true,
				"items" => $sub
			);
		}
		return $block;
	}

	////// banners callback method
	
	/**
	 * Return data of banner place
	 * 
	 * @return array
	 */
	public function _banner_available_pages(){
		//$return[] = array("link"=>"content/index", "name"=> l('header_index_content', 'content'));
		$return[] = array("link"=>"content/view", "name"=> l('header_view_content', 'content'));
		return $return;
	}

	/**
	 * Dynamic block callback method for returning info pages
	 * 
	 * @param array $params dynamic block parameter
	 * @param string $view dynamic block view
	 * @return string
	 */
	public function _dynamic_block_get_info_pages($params, $view=''){
		$this->CI->load->helper('content');
		return content_info_pages($params['keyword'], $view);
	}
}

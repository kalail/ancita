<!--
    Task    : CR-2015-01 -2.3 - Make Monthly offer as it is in existing Ancita version
	Date    : 06-Nov-2015
Modified By : Mahi-->
<?php

/**
 * Content management
 * 
 * @package PG_RealEstate
 * @subpackage Content
 * @category	helpers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Mikhail Makeev <mmakeev@pilotgroup.net>
 * @version $Revision: 68 $ $Date: 2010-01-11 16:02:23 +0300 (Пн, 11 янв 2010) $ $Author: irina $
 **/

if ( ! function_exists('get_content_tree'))
{
	function get_content_tree($parent_id=0){
		$CI = & get_instance();
		$CI->load->model('Content_model');
		$lang_id = $CI->pg_language->current_lang_id;

		if($parent_id){
			$params["where"]["parent_id"] = $parent_id;
			$sub_count = $CI->Content_model->get_active_pages_count($lang_id, $params);
			if(empty($sub_count)){
				$CI->Content_model->set_page_active($lang_id, $parent_id);
				$parent_data = $CI->Content_model->get_page_by_id($parent_id);
				$parent_id = isset($parent_data["parent_id"])?$parent_data["parent_id"]:0;
			}
		}

		$pages = $CI->Content_model->get_active_pages_list($lang_id, $parent_id);

		$CI->template_lite->assign("content_tree", $pages);
		$html = $CI->template_lite->fetch("tree", 'user', 'content');
		echo $html;
	}
}

if ( ! function_exists('get_content_page'))
{
	function get_content_page($gid){
		$CI = & get_instance();
		$CI->load->model('Content_model');

		$lang_id = $CI->pg_language->current_lang_id;
		$page_data = $CI->Content_model->get_page_by_gid($lang_id, $gid);
		$CI->template_lite->assign("page", $page_data);
		$html = $CI->template_lite->fetch("show_block", 'user', 'content');
		echo $html;
	}
}

if ( ! function_exists('get_content_promo'))
{
	function get_content_promo($view=''){
		
		$CI = & get_instance();
		$CI->load->model('content/models/Content_promo_model');

		$lang_id = $CI->pg_language->current_lang_id;
		$promo_data = $CI->Content_promo_model->get_promo($lang_id);
		
		$promo_data_slide = $CI->Content_promo_model->get_promo_slide($lang_id);
		$CI->template_lite->assign("promo_data_slide", $promo_data_slide);
		
		$CI->template_lite->assign("promo", $promo_data);
		$CI->template_lite->assign("view", $view);
		$html = $CI->template_lite->fetch("show_promo_block", 'user', 'content');
		return $html;
	}
}

if ( ! function_exists('get_estate_agents'))
{
	function get_estate_agents($view=''){
		
		$CI = & get_instance();
		$CI->load->model('content/models/Content_promo_model');

		$lang_id = $CI->pg_language->current_lang_id;
	
		$citylist1 = $CI->Content_promo_model->getCity_byuser_extenddata('1');
		$citylist2 = $CI->Content_promo_model->getCity_byuser_extenddata('2');
		$CI->template_lite->assign("city1", $citylist1);
		$CI->template_lite->assign("city2", $citylist2);

		$html = $CI->template_lite->fetch("estate_agent_box", 'user', 'content');
		return $html;
	}
}

if ( ! function_exists('content_info_pages'))
{
	/**
	 * Return widget of info pages
	 * 
	 * @param string $keyword page guid
	 * @param string $view page view
	 * @return void
	 */
	function content_info_pages($keyword='', $view=''){
		$CI = & get_instance();
		$CI->load->model('Content_model');
		
		$parent_id = 0;
		
		if($keyword){
			$section = $this->Content_model->get_page_by_gid($CI->pg_language->current_lang_id, $keyword);
			if($section){
				$parent_id = $section["id"];
				$CI->template_lite->assign("section", $section);
			}
		}

		$pages = $CI->Content_model->get_active_pages_list($CI->pg_language->current_lang_id, $parent_id, array('where_sql'=>array('(parent_id="'.$parent_id.'")')));
		
		foreach($pages as $i => $page){
			$page = $CI->Content_model->get_page_by_id($page['id']);
			$pages[$i]['content'] = strip_tags($page['content']);
		}
		
		$CI->template_lite->assign("pages", $pages);
	
		return $CI->template_lite->fetch("helper_info_pages", "user", "content");
	}
}


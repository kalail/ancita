<?php

/**
 * Content user side controller
 * 
 * @package PG_RealEstate
 * @subpackage Content
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Content extends Controller
{
	/**
	 * link to CodeIgniter object
	 * @var object
	 */

	/**
	 * Constructor
	 */
	function Content()
	{
		parent::Controller();
		$this->load->model("Content_model");
	}
	
	function index($lang_id=null){
		if(!is_null($gid)) {
			$this->load->helper('seo');
			redirect(rewrite_link('content', 'view', $gid));
		};
		$this->load->model('Menu_model');
		$this->Menu_model->breadcrumbs_set_active(l('seo_tags_index_title', 'content'), site_url().'content/');

		$params = array();
		$params['where']['parent_id'] = '0';
		$params['where']['status'] = '1';
		$pages_list = $this->Content_model->get_pages_list($this->pg_language->current_lang_id, 0, $params);

		$this->template_lite->assign('pages', $pages_list);
		$this->template_lite->assign('date_format', $this->pg_date->get_format('date_time_literal', 'st'));
		$this->template_lite->view('list');
	}

	function view($gid){
	 	$gid = trim(strip_tags($gid));
	 	if(!$gid) show_404();
		$lang_id = $this->pg_language->current_lang_id;
		$page_data = $this->Content_model->get_page_by_gid($lang_id, $gid);
		if(!$page_data) show_404();
		$this->template_lite->assign("page", $page_data);

		$this->load->model('Menu_model');
		$this->Menu_model->breadcrumbs_set_active($page_data["title"]);
		
		if($page_data['id_seo_settings']){
			$this->load->model('Seo_model');
			$seo_settings = $this->Seo_model->parse_seo_tags($page_data['id_seo_settings']);
			$this->pg_seo->set_seo_tags($seo_settings);
		}else{
			$this->load->helper('seo');
			$seo_data = $page_data;
			$seo_data['canonical'] = rewrite_link('content', 'view', $page_data, false, null, true);
			$this->pg_seo->set_seo_data($seo_data);
		}
		
		$this->template_lite->view('view');
	}
	
	public function ajax_get_extenduser($CityId, $Acitve)
	{
		$this->load->model("content/models/Content_promo_model");
		$userlist = $this->Content_promo_model->getextenddata_userlist($CityId,$Acitve);
		echo json_encode($userlist);
		return;
	}
	private function _user_listings_block($user, $operation_type, $order=null, $order_direction=null, $page=null){
		$this->load->model("listings/models/Listings_model");
		$current_settings = isset($_SESSION['user_listings'])?$_SESSION['user_listings']:array();
		if(!isset($current_settings['order']))
			$current_settings['order'] = 'modified';
		if(!isset($current_settings['order_direction']))
			$current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page']))
			$current_settings['page'] = 1;
		$view_mode = isset($_SESSION['listings_view_mode']) ? $_SESSION['listings_view_mode'] : 'list';

		 $filters['type'] = $operation_type;
		 $filters['user'] = $user['id'];
		 $filters['active'] = 1;
		
		if(!$order) $order = $current_settings['order'];
		$this->template_lite->assign('order', $order);
	 	$current_settings['order'] = $order;
		if (!$order_direction) $order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		$current_settings['order_direction'] = $order_direction;
		$page = intval($page);
		if(!$page) $page = $current_settings['page'];
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
		// save criteria for pagination
		$_SESSION['listings_pagination'] = array(
			'order' => $current_settings['order'],
			'order_direction' => $current_settings['order_direction'],
			'page' => $current_settings['page'],
			'data' => $filters,
		);
		
		$this->load->helper('seo');
		$action_data = array('id_user'=>$user['id'], 'user'=>$user['name'], 'operation_type'=>$operation_type);
		$url = rewrite_link('listings', 'user', $action_data);
		// sorting
		$sort_data = array(
			'url' => $url,
			'order' => $order,
			'direction' => $order_direction,
			'links' => array(
				'views' => l('field_views', 'listings'),
				'price' => l('field_price', 'listings'),
				'open' => l('field_date_open', 'listings'),
				'modified' => l('field_date_modified', 'listings'),
			),
		);
		if($this->pg_module->is_module_installed('reviews')){
			$this->load->helper('reviews');
			$review_link = get_reviews_sorter();
			if(is_array($review_link)){
				$first = array_splice($sort_data['links'], 0, 1);
				$sort_data['links'] = array_merge($first, $review_link, $sort_data['links']);
				if (($key = array_search('Rating', $sort_data['links'])) !== false) {
                unset($sort_data['links'][$key]);
                }
				if (($key = array_search('Reviews', $sort_data['links'])) !== false) {
                unset($sort_data['links'][$key]);
                }
				if (($key = array_search('Open house', $sort_data['links'])) !== false) {
                unset($sort_data['links'][$key]);
                }
				if (($key = array_search('Updated', $sort_data['links'])) !== false) {
                unset($sort_data['links'][$key]);
                }
			}
		}
		$this->template_lite->assign('sort_data', $sort_data);
		$use_map_in_search = $view_mode == 'map';
		$this->template_lite->assign('use_map_in_search', $use_map_in_search);
	
		$listings_count = $this->Listings_model->get_listings_count($filters);
		if($listings_count > 0){
			$order_array = array();
			if($order == 'price'){
				$order_array['price_sorting'] = $order_direction;
			}elseif($order == 'open'){
				$order_array['date_open'] = $order_direction;
			}elseif($order == 'modified'){
				$order_array['date_modified'] = $order_direction;
			}else{
				$order_array[$order] = $order_direction;
			}
			$this->Listings_model->set_format_settings('get_saved_listings', true);
			$listings = $this->Listings_model->get_listings_list($filters, $page, $items_on_page, $order_array);
			$this->Listings_model->set_format_settings('get_saved_listings', false);
			$this->template_lite->assign('listings', $listings);		
			
			if($use_map_in_search){
				$markers = array();
				foreach($listings as $listing){
					if((float)$listing['lat'] == 0 && (float)$listing['lon'] == 0) continue;
					$this->template_lite->assign('view_mode', $view_mode);
					$this->template_lite->assign('listing', $listing);
					$info = $this->template_lite->fetch('listing_map_block', 'user', 'listings');
					$markers[] = array( 
						'gid' => $listing['id'],
						'country' => $listing['country'], 
						'region' => $listing['region'], 
						'city' => $listing['city'], 
						'address' => $listing['address'], 
						'postal_code' => $listing['zip'], 
						'lat' => (float)$listing['lat'], 
						'lon' => (float)$listing['lon'], 
						'info' => $info,
					);
				}
				$this->template_lite->assign('markers', $markers);
			}
		}
		
		if($use_map_in_search){
			$current_language = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
			$view_settings = array(
				'lang' => $current_language['code'],
			);			
			$this->template_lite->assign('map_settings', $view_settings);
		}
	
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $listings_count, $items_on_page);
		$current_settings['page'] = $page;
		
		$_SESSION['user_listings'] = $current_settings;
		
		$this->load->helper('navigation');
		
		$page_data = get_user_pages_data($url.'/'.$order.'/'.$order_direction.'/', $listings_count, $items_on_page, $page, 'briefPage');
		$page_data['date_format'] = $this->pg_date->get_format('date_time_literal', 'st');
		$this->template_lite->assign('page_data', $page_data);
		$is_owner = 0;
		if($this->session->userdata('auth_type') == 'user'){
			if($this->session->userdata('user_id') == $user['id'])	$is_owner = 1;
		}else{
			$this->template_lite->assign('is_guest', 1);
		}
		$this->template_lite->assign('is_listing_owner', $is_owner);
		
		$this->template_lite->assign('tstamp', time());
		
		return $this->template_lite->fetch('user_block_lists', 'user', 'listings');
	}
	
	public function ajax_get_extendusersearch($str)
	{
		$this->load->model("content/models/Content_promo_model");
		$userlist = $this->Content_promo_model->getextenduser_byserach($str);
		echo json_encode($userlist);
		return;
	}
	
	public function ajax_user_listings($user_id, $operation_type, $order='modified', $order_direction='DESC', $page=1){
		$this->load->model("users/models/Users_model");	
		$user = $this->Users_model->get_user_by_id($user_id, true);
		if(!$user) exit;
		if(!$operation_type) $operation_type = $this->input->post('type');
		echo $this->_user_listings_block($user, $operation_type, $order, $order_direction, $page);
		exit;
	}
	
	public function estate_agents($Id,$option=''){
		$this->load->model("content/models/Content_promo_model");
		$Ids = explode(':',$Id);
		$userId = $Ids[1];
		
		$txtOption = '0';
		if($option != ''){
			$oId = explode(':',$option);
			if($oId[1] == '1'){$txtOption = '1';}
		}

		$result = $this->Content_model->get_user_extend_data($userId);
		
		if (strpos($result['website'],'http://') === false){
			$result['website'] = 'http://'.$result['website'];
		}
		 
		$this->load->model("users/models/Users_model");
		
		if($result['pg_user_id'] != ''){
			$result_user = $this->Users_model->get_user_by_id($result['pg_user_id']);
			$this->template_lite->assign("user", $result_user);
		}
		
		$user = $this->Users_model->get_user_by_id($result['pg_user_id'], true);
		
		$this->load->model("listings/models/Listings_model");
		
		
		$result['agent_text'] =  html_entity_decode($result['agent_text']);
		$result['agent_text_additional'] =  html_entity_decode($result['agent_text_additional']);
		$operation_type=null;
		$order='modified';
	    $order_direction='DESC';
		$page=1;
		$block_content = $this->_user_listings_block($user, $operation_type, $order, $order_direction, $page);
		$this->template_lite->assign('block_listing', $block_content);
		
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;
		$this->template_lite->assign('page', $current_settings['page']);
		
		$citylist1 = $this->Content_promo_model->getCity_byuser_extenddata('1');
		$citylist2 = $this->Content_promo_model->getCity_byuser_extenddata('2');
		$this->template_lite->assign("city1", $citylist1);
		$this->template_lite->assign("city2", $citylist2);
		$this->template_lite->assign('extend', $result);
		$this->template_lite->assign("vOption", $txtOption);
		$this->load->model('Menu_model');
		$this->Menu_model->breadcrumbs_set_active('Agent');
		$this->template_lite->view('estate_agent_view');
	}
	
}

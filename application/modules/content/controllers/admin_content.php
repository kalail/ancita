<!--
    Task    : CR-2015-01 -2.3 - Make Monthly offer as it is in existing Ancita version
	Date    : 06-Nov-2015
Modified By : Mahi-->
<?php

/**
 * Content us admin side controller
 *
 * @package PG_RealEstate
 * @subpackage Content
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Admin_Content extends Controller
{
	/**
	 * link to CodeIgniter object
	 * @var object
	 */

	/**
	 * Constructor
	 */
	function Admin_Content()
	{
		parent::Controller();
		$this->load->model("Content_model");
		$this->load->model('Menu_model');
		$this->Menu_model->set_menu_active_item('admin_menu', 'content_items');
	}

	function index($lang_id=null){
		if(!$lang_id) $lang_id = $this->pg_language->get_default_lang_id();

		$languages = $this->pg_language->languages;
		$this->template_lite->assign("languages", $languages);
		$this->template_lite->assign("current_lang", $lang_id);

		$pages = $this->Content_model->get_pages_list($lang_id);
		$this->template_lite->assign("pages", $pages);

		$this->pg_theme->add_js('admin-multilevel-sorter.js');
		$this->system_messages->set_data('header', l('admin_header_page_list', 'content'));
		$this->template_lite->view('list');
	}

	/**
	 * Edit info page content
	 * 
	 * @param integer $lang_id language identifier
	 * @param integer $parent_id parent page identifier
	 * @param integer $page_id page identifier
	 * @param string $section_gid section guid
	 * @return void
	 */
	public function edit($lang_id, $parent_id=0, $page_id=null, $section_gid='text'){
		if($page_id){
			$data = $this->Content_model->get_page_by_id($page_id);
		}else{
			$data = array();
		}

		if($this->input->post('btn_save')){
			switch($section_gid){
				case 'text':
					$post_data = array(
						"title" => $this->input->post('title', true),
						"content" => $this->input->post('content'),
						"gid" => $this->input->post('gid', true),
						"lang_id" => $lang_id,
						"parent_id" => $parent_id
					);
					$validate_data = $this->Content_model->validate_page($page_id, $post_data);
					if(!empty($validate_data["errors"])){
						$this->system_messages->add_message('error', $validate_data["errors"]);
					}else{
						$data = $validate_data["data"];
						$this->Content_model->save_page($page_id, $data);
						$this->system_messages->add_message('success', ($page_id)?l('success_update_page', 'content'):l('success_add_page', 'content'));
						//$url = site_url()."admin/content/index/".$lang_id;
						//redirect($url);
					}
					$data = array_merge($data, $post_data);
				break;
				case 'seo':
					$this->load->model('Seo_model');
					$seo_fields = $this->Seo_model->get_seo_fields();
					foreach($seo_fields as $key=>$section_data){
						if($this->input->post('btn_save_'.$section_data['gid'])){
							$post_data = array();
							$post_data[$section_data['gid']] = $this->input->post($section_data['gid'], true);
							$validate_data = $this->Seo_model->validate_seo_tags($page_id, $post_data);
							if(empty($validate_data['errors'])){
								$page_data['id_seo_settings'] = $this->Seo_model->save_seo_tags($data['id_seo_settings'], $validate_data['data']);
								if(!$data['id_seo_settings']){
									$data['id_seo_settings'] = $page_data['id_seo_settings'];
									$this->Content_model->save_page($page_id, $page_data);
								}
								$this->system_messages->add_message('success', l('success_settings_updated', 'seo'));
							}
							$data = array_merge($data, $post_data);
							break;
						}
					}
				break;
			}
		}
		
		switch($section_gid){
			case 'text':
				$this->load->plugin('fckeditor');
				$data["content_fck"] = create_editor("content", isset($data["content"]) ? $data["content"] : "", 700, 400, 'Middle');
			break;
			case 'seo':
				$this->load->model('Seo_model');
				$seo_fields = $this->Seo_model->get_seo_fields();
				$this->template_lite->assign('seo_fields', $seo_fields);
			
				$languages = $this->pg_language->languages;
				$this->template_lite->assign('languages', $languages);
				
				$current_lang_id = $this->pg_language->current_lang_id;
				$this->template_lite->assign('current_lang_id', $current_lang_id);

				$seo_settings = $this->Seo_model->get_seo_tags($data['id_seo_settings']);
				$this->template_lite->assign('seo_settings', $seo_settings);
			break;
		}
		
		$this->template_lite->assign('data', $data);
		$this->template_lite->assign('section_gid', $section_gid);

		$this->template_lite->assign('languages', $this->pg_language->languages);
		$this->template_lite->assign("current_lang", $lang_id);
		$this->template_lite->assign('parent_id', $parent_id);
		$this->system_messages->set_data('header', l('admin_header_page_edit', 'content'));
		$this->template_lite->view('edit_form');
	}

	function ajax_save_sorter(){
		$sorter = $this->input->post("sorter");
		foreach($sorter as $parent_str => $items_data){
			$parent_id = intval(str_replace("parent_", "", $parent_str));
			foreach($items_data as $item_str =>$sort_index){
				$page_id = intval(str_replace("item_", "", $item_str));
				$data = array(
					"parent_id" => $parent_id,
					"sorter" => $sort_index
				);
				$this->Content_model->save_page($page_id, $data);
			}
		}
	}

	function ajax_activate($status, $page_id){
		$this->Content_model->activate_page($page_id, $status);
	}

	function ajax_delete($page_id){
		$this->Content_model->delete_page($page_id);
	}
	
	function delete_slide_photo($slide_id,$lang_id,$slide_image){
		$this->load->model("content/models/Content_promo_model");
		$this->load->model("Uploads_model");
		$this->Uploads_model->delete_upload($this->Content_promo_model->upload_gid, $lang_id."/", $slide_image);
		$this->Content_promo_model->delete_promo_slide($slide_id);
		$url = site_url()."admin/content/promo/".$lang_id;
				redirect($url);
	}

	function promo($lang_id=0, $content_type=''){
		if(!$lang_id) $lang_id = $this->pg_language->get_default_lang_id();
		$this->load->model("content/models/Content_promo_model");

		$promo_data = $this->Content_promo_model->get_promo($lang_id);
		$promo_data_slide = $this->Content_promo_model->get_promo_slide($lang_id);
		$this->template_lite->assign("promo_data_slide", $promo_data_slide);

		if($this->input->post('btn_Add_Slide')){
			
			$post_promo_data_slides = array(
				"Lang_id" => $lang_id,
				"promo_each_image_seconds" => $this->input->post('block_each_image_seconds', true),
				"promo_url" => $this->input->post('promo_url', true),
				"feature1" => $this->input->post('feature1', true),
				"SortingOrder"=>$this->input->post('slider_image_order', true),
			);
			
				$return = $this->Content_promo_model->save_promo_slide($post_promo_data_slides,'promo_slide_image');
				if( $return == 'sucess')
				{
					$this->system_messages->add_message('success', 'Promo Slide Successfully Added');
					$url = site_url()."admin/content/promo/".$lang_id."/".$content_type;
					redirect($url);
				}
				else if( $return == 'maxcount')
				{
					$this->system_messages->add_message('error', 'Already added maximum number of slides.');
				}
				else if( $return == 'exists')
				{
					$this->system_messages->add_message('error', 'Slider order already exists');
				}
				else
				{
					$this->system_messages->add_message('error', 'Please select the image.');
				}
	
		}

		if($this->input->post('btn_Update_Slide')){
			
				$id = $this->input->post('block_slide_id', true);
			    $hidden_order = $this->input->post('hidden_order', true);
				$SortingOrder = $this->input->post('slider_image_order', true);
				if($hidden_order == $SortingOrder){
				$post_promo_data_slides1 = array(
					"promo_each_image_seconds" => $this->input->post('block_each_image_seconds', true),
					"promo_url" => $this->input->post('promo_url', true),
					"feature1" => $this->input->post('feature1', true),
					"Lang_id"=>$this->input->post('Lang_id', true),
				);
			   $return = $this->Content_promo_model->update_promo_slide1($id,$post_promo_data_slides1);
				}else{
					$post_promo_data_slides2 = array(
					"promo_each_image_seconds" => $this->input->post('block_each_image_seconds', true),
					"promo_url" => $this->input->post('promo_url', true),
					"feature1" => $this->input->post('feature1', true),
					"SortingOrder"=>$this->input->post('slider_image_order', true),
					"Lang_id"=>$this->input->post('Lang_id', true),
				);
				$return = $this->Content_promo_model->update_promo_slide2($id,$post_promo_data_slides2);
				}
				if( $return == 'sucess')
				{
					$this->system_messages->add_message('success', 'Promo Slide Successfully Updated');
					$url = site_url()."admin/content/promo/".$lang_id."/".$content_type;
					redirect($url);
				}
				else if( $return == 'exists')
				{
					$this->system_messages->add_message('error', 'Slider order already exists');
				}
				else
				{
					$this->system_messages->add_message('error', 'Error in slider update');
				}
	
		}


		if($this->input->post('btn_save_settings')){
			$post_data = array(
				"content_type" => $this->input->post('content_type', true),
				"block_width" => $this->input->post('block_width', true),
				"block_width_unit" => $this->input->post('block_width_unit', true),
				"block_height" => $this->input->post('block_height', true),
				"block_height_unit" => $this->input->post('block_height_unit', true),
			);
			$validate_data = $this->Content_promo_model->validate_promo($post_data);

			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message('error', $validate_data["errors"]);
				$promo_data = array_merge($promo_data, $validate_data["data"]);
			}else{
				$this->Content_promo_model->save_promo($lang_id, $validate_data["data"]);
				$this->system_messages->add_message('success', l('success_update_promo_block', 'content'));
				$url = site_url()."admin/content/promo/".$lang_id;
				redirect($url);
			}
		}

		if($this->input->post('btn_save_content')){
			if($content_type == 't'){
				$post_data = array(
					"promo_text" => $this->input->post('promo_text', true),
					"block_align_hor" => $this->input->post('block_align_hor', true),
					"block_align_ver" => $this->input->post('block_align_ver', true),
					"block_image_repeat" => $this->input->post('block_image_repeat', true),
					"promo_text_color" => $this->input->post('promo_text_color', true),
				);
				
				//print_r($post_data);die;
				$validate_data = $this->Content_promo_model->validate_promo($post_data, 'promo_image');
			}elseif($content_type == 'f'){
				$validate_data = $this->Content_promo_model->validate_promo(array(), '', 'promo_flash');
			}
			
//			if($this->input->post('promo_image_delete')){
//				$this->load->model("Uploads_model");
//				$this->Uploads_model->delete_upload($this->Content_promo_model->upload_gid, $lang_id."/", $promo_data['promo_image']);
//				$validate_data["data"]["promo_image"] = '';
//			}
//			
		
			if($this->input->post('promo_flash_delete')){
				$this->load->model("File_uploads_model");
				$this->File_uploads_model->delete_upload($this->Content_promo_model->file_upload_gid, $lang_id."/", $promo_data['promo_flash']);
				$validate_data["data"]["promo_flash"] = '';
			}

			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message('error', $validate_data["errors"]);
				$promo_data = array_merge($promo_data, $validate_data["data"]);
			}else{
				$this->Content_promo_model->save_promo($lang_id, $validate_data["data"], '', 'promo_flash');
				$this->system_messages->add_message('success', l('success_update_promo_block', 'content'));
				$url = site_url()."admin/content/promo/".$lang_id."/".$content_type;
				redirect($url);
			}

		}

		if(!$content_type) $content_type = $promo_data["content_type"];

		$this->load->plugin('fckeditor');
		$promo_data["promo_text_fck"] = create_editor("promo_text", isset($promo_data["promo_text"]) ? $promo_data["promo_text"] : "", 570, 300, 'Middle');

		$this->template_lite->assign('languages', $this->pg_language->languages);
		$this->template_lite->assign("current_lang", $lang_id);
		$this->template_lite->assign("content_type", $content_type);
		$this->template_lite->assign("promo_data", $promo_data);
		$this->system_messages->set_data('header', l('admin_header_promo_edit', 'content'));
		$this->Menu_model->set_menu_active_item('admin_menu', 'interface-items');
		$this->template_lite->view('promo_form');
	}
	
	function marketing($tab_id='',$changeVersion='',$nVersionMonth='',$nVersionYear='',$oVersionMonth='',$oVersionYear='')
	{
		if($tab_id == '')
			$tab_id = "Placefactors";
		$IsActiveVersion = 0;
		if($changeVersion == '')
		{
			$version = $this->Content_model->get_version_setting($tab_id);
			$currentMonth = $version['Month'];
			$currentYear = $version['Year'];
			$IsActiveVersion = 1;
		}
		
		if($this->input->post('btn_trunon_new_functionality')){
			if($tab_id == 'Placefactors')
			{
				if(isset($_POST["check_functionality"])) {
					$strSQL = "UPDATE " .FACTORS_GRAPH_TABLE. " SET date = (SELECT max(date) FROM " .CITY_APARTMENTS_PRICES_GRAPH. ") WHERE id = 1";
					$this->Content_model->save_custom_query($strSQL);
					$strSQL = "UPDATE " .FACTORS_GRAPH_TABLE. " SET functionality = 'on' WHERE id = 1";
					$this->Content_model->save_custom_query($strSQL);
				} else {
					$strSQL = "UPDATE " .FACTORS_GRAPH_TABLE. " SET date = '' WHERE id = 1";
					$this->Content_model->save_custom_query($strSQL);
					$strSQL = "UPDATE " .FACTORS_GRAPH_TABLE. " SET functionality = '' WHERE id = 1";
					$this->Content_model->save_custom_query($strSQL);
				}
			}
		}
			
		if($this->input->post('btn_save_Placefactors')){
			if($tab_id == 'Placefactors')
			{
				$currentMonth = $_POST['versionMonth'];
				$currentYear = $_POST['versionYear'];
				if($_POST['IsActiveVersion'] == '1')
				{
					$this->Content_model->save_version_setting($_POST['versionMonth'],$_POST['versionYear'],$tab_id);
				}
				foreach ($_POST as $field_name => $value) {
					$id = (int) str_replace(array('appartment_', 'house_'), '', $field_name);
					
					if (substr_count($field_name, 'appartment') == 1) {
						$this->Content_model->save_city_price_appartment($currentMonth,$currentYear,$id,$value);
					}
		
					if (substr_count($field_name, 'house') == 1) {
						$this->Content_model->save_city_price_house($currentMonth,$currentYear,$id,$value);
					}
					
					if (substr_count($field_name, 'adjustment') == 1) {
						$firstdashpos = strpos($field_name, '_')+1;
						$seconddashpos = strrpos($field_name,'_');
		
						$area_id =  (int) substr($field_name, $firstdashpos, $seconddashpos - $firstdashpos);
						$city_id =  (int) substr($field_name, $seconddashpos+1);
             
						$this->Content_model->save_district_price($currentMonth,$currentYear,$area_id,$city_id,$value);
					}  
				}
				
				$this->system_messages->add_message('success', 'Place factors Saved Successfully');
			}
		}
		
		if($this->input->post('btn_save_Commonfactors')){
			if($tab_id == 'Commonfactors')
			{
				$currentMonth = $_POST['versionMonth'];
				$currentYear = $_POST['versionYear'];
				if($_POST['IsActiveVersion'] == '1')
				{
					$this->Content_model->save_version_setting($_POST['versionMonth'],$_POST['versionYear'],$tab_id);
				}
				$aa_1 = $this->GetVarFloat('aa_1');
				$aa_2 = $this->GetVarFloat('aa_2');
				$aa_3 = $this->GetVarFloat('aa_3');
				$bb_1 = $this->GetVarFloat('bb_1');
				$bb_2 = $this->GetVarFloat('bb_2');
				$bb_3 = $this->GetVarFloat('bb_3');
				$cc = $this->GetVarFloat('cc');
				$dd = $this->GetVarFloat('dd');
				$ee = $this->GetVarFloat('ee');
				$ff = $this->GetVarFloat('ff');
				$gg = $this->GetVarFloat('gg');
				$hh = $this->GetVarFloat('hh');
				$ii_1 = $this->GetVarFloat('ii_1');
				$ii_2 = $this->GetVarFloat('ii_2');
				$ii_3 = $this->GetVarFloat('ii_3');
				$ba_1 = $this->GetVarFloat('ba_1');
				$ba_2 = $this->GetVarFloat('ba_2');
				$ba_3 = $this->GetVarFloat('ba_3');
				$ba_4 = $this->GetVarFloat('ba_4');
				$ba_5 = $this->GetVarFloat('ba_5');
				$ba_6 = $this->GetVarFloat('ba_6');
				$bb = $this->GetVarFloat('bb');
				$bc_1 = $this->GetVarFloat('bc_1');
				$bc_2 = $this->GetVarFloat('bc_2');
				$bd_1 = $this->GetVarFloat('bd_1');
				$bd_2 = $this->GetVarFloat('bd_2');
				$be = $this->GetVarFloat('be');
				$bf = $this->GetVarFloat('bf');
				$bg = $this->GetVarFloat('bg');
				$bh_1 = $this->GetVarFloat('bh_1');
				$bh_2 = $this->GetVarFloat('bh_2');
				$bi_1 = $this->GetVarFloat('bi_1');
				$bi_2 = $this->GetVarFloat('bi_2');
				$bi_3 = $this->GetVarFloat('bi_3');
				$bj_1 = $this->GetVarFloat('bj_1');
				$bj_2 = $this->GetVarFloat('bj_2');
				$bj_3 = $this->GetVarFloat('bj_3');
				$bk_1 = $this->GetVarFloat('bk_1');
				$bk_2 = $this->GetVarFloat('bk_2');
				$bk_3 = $this->GetVarFloat('bk_3');
				$bk_4 = $this->GetVarFloat('bk_4');
				$bl_1 = $this->GetVarFloat('bl_1');
				$bl_2 = $this->GetVarFloat('bl_2');
				$bl_3 = $this->GetVarFloat('bl_3');
				$bl_4 = $this->GetVarFloat('bl_4');
				$bm_1 = $this->GetVarFloat('bm_1');
				$bm_2 = $this->GetVarFloat('bm_2');
				$a_1 = $this->GetVarFloat('a_1');
				$a_2 = $this->GetVarFloat('a_2');
				$a_3 = $this->GetVarFloat('a_3');
				$b_1 = $this->GetVarFloat('b_1');
				$b_2 = $this->GetVarFloat('b_2');
				$c_1 = $this->GetVarFloat('c_1');
				$c_2 = $this->GetVarFloat('c_2');
				$c_3 = $this->GetVarFloat('c_3');
				$d_1 = $this->GetVarFloat('d_1');
				$d_2 = $this->GetVarFloat('d_2');
				$e_1 = $this->GetVarFloat('e_1');
				$e_2 = $this->GetVarFloat('e_2');
				$f_1 = $this->GetVarFloat('f_1');
				$f_2 = $this->GetVarFloat('f_2');
				$g_1 = $this->GetVarFloat('g_1');
				$g_2 = $this->GetVarFloat('g_2');
				$g_3 = $this->GetVarFloat('g_3');
				$h_1 = $this->GetVarFloat('h_1');
				$h_2 = $this->GetVarFloat('h_2');
				$h_3 = $this->GetVarFloat('h_3');
				$i_1 = $this->GetVarFloat('i_1');
				$i_2 = $this->GetVarFloat('i_2');
				$i_3 = $this->GetVarFloat('i_3');
				$j_1 = $this->GetVarFloat('j_1');
				$j_2 = $this->GetVarFloat('j_2');
				$j_3 = $this->GetVarFloat('j_3');
				$j_4 = $this->GetVarFloat('j_4');
				$k_1 = $this->GetVarFloat('k_1');
				$k_2 = $this->GetVarFloat('k_2');
				$k_3 = $this->GetVarFloat('k_3');
				$k_4 = $this->GetVarFloat('k_4');
				$price_min = $this->GetVarFloat('price_min');
				$price_max = $this->GetVarFloat('price_max');
				$gliding = $this->GetVarFloat('gliding');
				
				$strSQL1 = " REPLACE INTO " . FACTORS_TABLE . " (Month, Year) VALUES ('" . $currentMonth . "', '" . $currentYear . "')";
				
				$strSQL2 = " UPDATE " . FACTORS_TABLE . " SET aa_1='" . $aa_1 . "', aa_2='" . $aa_2 . "', aa_3='" . $aa_3 . "', bb_1=' " . $bb_1 . "',
				bb_2='" . $bb_2 . "', bb_3='" . $bb_3 . "', cc='" . $cc . "', dd='" . $dd . "', ee='" . $ee . "', ff='" . $ff . "', 
				gg='" . $gg . "', hh='" . $hh . "', ii_1='" . $ii_1 . "', ii_2='" . $ii_2 . "', ii_3='" . $ii_3 . "', ba_1='" . $ba_1 . "', 
				ba_2='" . $ba_2 . "', ba_3='" . $ba_3 . "', ba_4='" . $ba_4 . "', ba_5='" . $ba_5 . "', ba_6='" . $ba_6 . "', bb='" . $bb . "',
				bc_1='" . $bc_1 . "', bc_2='" . $bc_2 . "', bd_1='" . $bd_1 . "', bd_2='" . $bd_2 . "', be='" . $be . "', bf='" . $bf . "',
				bg='" . $bg . "', bh_1='" . $bh_1 . "', bh_2='" . $bh_2 . "', bi_1='" . $bi_1 . "', bi_2='" . $bi_2 . "', bi_3='" . $bi_3 . "',
				bj_1='" . $bj_1 . "', bj_2='" . $bj_2 . "', bj_3='" . $bj_3 . "', bk_1='" . $bk_1 . "', bk_2='" . $bk_2 . "', bk_3='" . $bk_3 . "',
				bk_4='" . $bk_4 . "', bl_1='" . $bl_1 . "', bl_2='" . $bl_2 . "', bl_3='" . $bl_3 . "', bl_4='" . $bl_4 . "', bm_1='" . $bm_1 . "',
				bm_2='" . $bm_2 . "', 
				a_1='" . $a_1 . "', a_2='" . $a_2 . "', a_3='" . $a_3 . "', b_1='" . $b_1 . "', b_2='" . $b_2 . "', c_1='" . $c_1 . "', 
				c_2='" . $c_2 . "', c_3='" . $c_3 . "', d_1='" . $d_1 . "', d_2='" . $d_2 . "', e_1='" . $e_1 . "', e_2='" . $e_2 . "', 
				f_1='" . $f_1 . "', f_2='" . $f_2 . "', g_1='" . $g_1 . "', g_2='" . $g_2 . "', g_3='" . $g_3 . "', h_1='" . $h_1 . "', 
				h_2='" . $h_2 . "', h_3='" . $h_3 . "', i_1='" . $i_1 . "', i_2='" . $i_2 . "', i_3='" . $i_3 . "', j_1='" . $j_1 . "', 
				j_2='" . $j_2 . "', j_3='" . $j_3 . "', j_4='" . $j_4 . "', k_1='" . $k_1 . "', k_2='" . $k_2 . "', k_3='" . $k_3 . "', 
				k_4='" . $k_4 . "', price_min='" . $price_min . "', price_max='" . $price_max . "', gliding='" . $gliding ."' 
				WHERE Month='".$currentMonth."' AND Year='".$currentYear."'";
				
				$this->Content_model->save_factor($strSQL1);
				$this->Content_model->save_factor($strSQL2);
				
				$this->system_messages->add_message('success', 'Common factors Saved Successfully');
			}
		}
		
		$tempresult ='';
		if($this->input->post('btn_load_excel')){
			if($tab_id == 'Excelimport')
			{
				$tempresult = $this->checkCitylist($_FILES['db_file']);
				if($tempresult == "")
				{
					$this->system_messages->add_message('error', "Select excel 2007 file");
				}
			}
		}
		
		$arrMonth = array('Select Month','JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC');
		$arrYear = array();
		for($y = 2014; $y <= 2025; $y++)
		{
			array_push($arrYear,$y);
		}
		
		// load factor
		if($changeVersion == 'Change')
		{
			$currentMonth = $nVersionMonth;
			$currentYear = $nVersionYear;
			$factors = $this->Content_model->get_factors($currentMonth,$currentYear);
			if(!$factors)
			{
				$factors = $this->Content_model->get_factors($oVersionMonth,$oVersionYear);
			}
		}
		else
		{
			$factors = $this->Content_model->get_factors($currentMonth,$currentYear);
		}
		
		// load city list and value
		$city_list = $this->Content_model->get_city_list($currentMonth,$currentYear);
		for($row = 0; $row < count($city_list); $row++)
		{
			$city = $city_list[$row];
			if($changeVersion == 'Change')
			{
				$currentMonth = $nVersionMonth;
				$currentYear = $nVersionYear;
				$city_price = $this->Content_model->get_city_price($currentMonth,$currentYear,$city['city_id']);
				if(!$city_price)
				{
					$city_price = $this->Content_model->get_city_price($oVersionMonth,$oVersionYear,$city['city_id']);
				}
			}
			else
			{
				$city_price = $this->Content_model->get_city_price($currentMonth,$currentYear,$city['city_id']);
			}
			
			$district_list = $this->Content_model->get_district_list_bycity($currentMonth,$currentYear,$city['city_id']);
			for($dist = 0; $dist < count($district_list); $dist++)
			{
				$district = $district_list[$dist];
				if($changeVersion == 'Change')
				{
					$currentMonth = $nVersionMonth;
					$currentYear = $nVersionYear;
					$dist_price = $this->Content_model->get_district_price($currentMonth,$currentYear,$city['city_id'],$district['id']);
					if(!$dist_price)
					{
						$dist_price = $this->Content_model->get_district_price($oVersionMonth,$oVersionYear,$city['city_id'],$district['id']);
					}
				}
				else
				{
					$dist_price = $this->Content_model->get_district_price($currentMonth,$currentYear,$city['city_id'],$district['id']);
				}
				
				$urbanication[$dist]["row"] = $dist;
				$urbanication[$dist]["id"] = $district['id'];
				$urbanication[$dist]["name"] = $district['name'];
				$urbanication[$dist]["adjustment"] = $dist_price['adjustment'];
			}
			
			$city_list[$row]["appartment_price"] = $city_price['appartment_price'];
			$city_list[$row]["house_price"] = $city_price['house_price'];
			$city_list[$row]["urbanication"] = $urbanication;
			$urbanication = null;
		}
		
		
		$version2 = $this->Content_model->get_version_setting($tab_id);
		if($version2['Month'] == $currentMonth && $version2['Year'] == $currentYear)
		{
			$IsActiveVersion = 1;
		}
		else
		{
			$IsActiveVersion = 0;
		}
		
		$version['Month'] = $currentMonth;
		$version['Year'] = $currentYear;
		
		if($changeVersion == 'Change')
		{
			$crntversion['Month'] = $oVersionMonth;
			$crntversion['Year'] = $oVersionYear;
		}
		else
		{
			$crntversion['Month'] = $currentMonth;
			$crntversion['Year'] = $currentYear;
		}
		
		$factors_graph = $this->Content_model->get_factor_graph_setting();
		
		$this->template_lite->assign("factors_graph", $factors_graph);
		$this->template_lite->assign("arrMonth", $arrMonth);
		$this->template_lite->assign("arrYear", $arrYear);
		$this->template_lite->assign("version", $version);
		$this->template_lite->assign("crntversion", $crntversion);
		$this->template_lite->assign("IsActiveVersion", $IsActiveVersion);
		$this->template_lite->assign("ExcelCityValidation", $tempresult);
		
		$this->template_lite->assign("tab_id", $tab_id);
		$this->template_lite->assign("cities", $city_list);
		$this->template_lite->assign("factors", $factors);
		$this->system_messages->set_data('header', l('calc_admin_marketing', 'content'));
		$this->template_lite->view('marketing');
	}
	
	function GetVarFloat($name, $default = 0) {

		if ( !(isset($_REQUEST[$name]) && !empty($_REQUEST[$name])) )
		return $default;
	
		$value = $_REQUEST[$name];
		$value = trim($value);
		$value = floatval($value);
	
		if ($value == 0) return $default;
	
		return $value;
	}
	
	function checkCitylist($upload) {
	
		if($upload["name"] == "")
		{
			return "";
		}

		$tempPath = SITE_PATH.SITE_SUBFOLDER.'temp/import';
		$userId = intval($this->session->userdata("user_id", true));
		
		//$fname = $upload["tmp_name"].$upload["name"];
		$handle = fopen($upload["tmp_name"], "r");
		$info_file = explode(".", $upload["name"]);    
		$file_name = $userId."_import_excel.".$info_file[1];    
		$file_path = $tempPath."/".$file_name; 
		
		if($info_file[1] != "xlsx")
		{
			return "";
		}
		
		copy($upload["tmp_name"],$file_path);	
		//copy
		$inputFileType = 'Excel2007';
		//$inputFileName = $_FILES['q']['tmp_name'];
		$inputFileName = $file_path;
		
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setLoadAllSheets();
		$objPHPExcel = $objReader->load($inputFileName);
		
		$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		$loadedSheetNames = $objPHPExcel->getSheetNames();
		
		$sheet1CityArray = array();
		$sheet2CityArray = array();	
		$databaseCityArray = array();
		$return = array();
		
		foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) {

			$sheetData = $objPHPExcel->getSheet($sheetIndex)->toArray(null,true,true,true);
	
			foreach ($sheetData as $val) {
				if($loadedSheetName == 'Apartments Cities') {
					if ($val["D"] !== "") array_push($sheet1CityArray, $val["D"]);
				}
				if($loadedSheetName == 'Villas Cities') {
					if ($val["D"] !== "") array_push($sheet2CityArray, $val["D"]);
				}			
			}
		}
		//print_r($sheet1CityArray);die;
		$arrcityName = $this->Content_model->get_all_cityname();
		for($city = 0; $city < count($arrcityName); $city++)
		{
			array_push($databaseCityArray, $arrcityName[$city]["name"]);
		}
		
		$uniqueSheet1Cities = array_diff($sheet1CityArray, $databaseCityArray);
		$uniqueSheet2Cities = array_diff($sheet2CityArray, $databaseCityArray);	
		$uniqueDb1Cities = array_diff($databaseCityArray, $sheet1CityArray);
		$uniqueDb2Cities = array_diff($databaseCityArray, $sheet2CityArray);	
		sort($uniqueSheetCities);
		//print_r($uniqueSheet1Cities);die;
		$return[0] = $uniqueSheet1Cities;
		$return[1] = $uniqueDb1Cities;
		$return[2] = $uniqueSheet2Cities;
		$return[3] = $uniqueDb2Cities;
		
		$strTable = '<table><tr>';
		
		//Cities in sheet1 Excel file, not in Ancita
		$strTable .= '<td valign="top"><table class="table_main" cellpadding="3" cellspacing="1">';
		$strTable .= "<tr style='background-color:#FFFFCC;'><th>No</th><th align='center'>Cities in sheet1 Excel <br>file, not in Ancita</th></tr>";
		for($city1 = 0; $city1 < count($uniqueSheet1Cities); $city1++)
		{
			$strTable .= "<tr style='background-color:#FFFFCC;'><td align='center'>" . ($city1 + 1) . "</td><td align='center'>" . $uniqueSheet1Cities[$city1] . "</td></tr>";
		}
		$strTable .= "</table></td>";
		
		//Cities in Ancita, but not in sheet1 Excel file
		$strTable .= '<td valign="top"><table class="table_main" cellpadding="3" cellspacing="1">';
		$strTable .= "<tr style='background-color:#EBFDF3;'><th>No</th><th align='center'>Cities in Ancita, but <br> not in sheet1 Excel file</th></tr>";
		for($city2 = 0; $city2 < count($uniqueDb1Cities); $city2++)
		{
			$strTable .= "<tr style='background-color:#EBFDF3;'><td align='center'>" . ($city2 + 1) . "</td><td align='center'>" . $uniqueDb1Cities[$city2] . "</td></tr>";
		}
		$strTable .= "</table></td>";
		
		//Cities in sheet2 Excel file, not in Ancita
		$strTable .= '<td valign="top"><table class="table_main" cellpadding="3" cellspacing="1">';
		$strTable .= "<tr style='background-color:#FFFFCC;'><th>No</th><th align='center'>Cities in sheet2 Excel <br>file, not in Ancita</th></tr>";
		for($city3 = 0; $city3 < count($uniqueSheet2Cities); $city3++)
		{
			$strTable .= "<tr style='background-color:#FFFFCC;'><td align='center'>" . ($city3 + 1) . "</td><td align='center'>" . $uniqueSheet2Cities[$city3] . "</td></tr>";
		}
		$strTable .= "</table></td>";
		
		//Cities in Ancita, but not in sheet2 Excel file
		$strTable .= '<td valign="top"><table class="table_main" cellpadding="3" cellspacing="1">';
		$strTable .= "<tr style='background-color:#EBFDF3;'><th>No</th><th align='center'>Cities in Ancita, but <br> not in sheet2 Excel file</th></tr>";
		for($city4 = 0; $city4 < count($uniqueDb2Cities); $city4++)
		{
			$strTable .= "<tr style='background-color:#EBFDF3;'><td align='center'>" . ($city4 + 1) . "</td><td align='center'>" . $uniqueDb2Cities[$city4] . "</td></tr>";
		}
		$strTable .= "</table></td>";
		
		$strTable .= '</tr><tr><td style="maxwidth: 400px;"><div class="btn"><div class="l"><input type="button" value="Import" onclick="importExcelData();"></div></div><br><br><span id="import_status"></span></td></tr></table>';
		
		return $strTable;
	}
	
	public function ajax_importExcelDataPrice()
	{
		$count = 0;
		$userId = intval($this->session->userdata("user_id", true));
		$file_name = $userId."_import_excel.xlsx";
		$tempPath = SITE_PATH.SITE_SUBFOLDER.'temp/import';
		$file_path = $tempPath."/".$file_name; 
		
		$inputFileType = 'Excel2007';
		$inputFileName = $file_path;
	
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objReader->setLoadAllSheets();
		$objPHPExcel = $objReader->load($inputFileName);
		$loadedSheetNames = $objPHPExcel->getSheetNames();
		
		$arrregionlist = $this->Content_model->get_region_name_id();
		$provinces = array();
		for($reg = 0; $reg < count($arrregionlist); $reg++)
		{
			$provinces = $provinces + array($arrregionlist[$reg]['name']=>$arrregionlist[$reg]['id']);
		}
		$provinces['Espana'] = '1';
		$provinces['Costa Blanca'] = '2';
		$provinces['España'] = '1';
		$provinces['Spain'] = '1';
		
		$arrcitylist = $this->Content_model->get_city_name_id();
		$cities = array();
		for($c = 0; $c < count($arrcitylist); $c++)
		{
			$cities = $cities + array($arrcitylist[$c]['name']=>$arrcitylist[$c]['id']);
		}
		
		$response = array();

		//delete old records
		$this->Content_model->delete_graph_appratment_house();
		
		foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) {
	
			$sheetData = $objPHPExcel->getSheet($sheetIndex)->toArray(null,true,true,true);
			
			if($loadedSheetName == 'Apartments Cities') {
				$unique = array();
				$unique2 = array();
				foreach ($sheetData as $key => $val) {
					if($val["D"] != "") {
						$city = $cities[$val["D"]];
						if(!in_array($city, $unique)) {
							array_push($unique, $city);
						} else {
							array_push($unique2, $city);					
						}
					}
					if ($key > 2) {
						$count = 0;				
						foreach ($val as $key2 => $val2) {
							$price = (int) str_replace(',','',$val2);
							$date = $sheetData[2][$key2];
	
							if ($price !== 0 ) {
								$strSQL = "INSERT INTO " . CITY_APARTMENTS_PRICES_GRAPH . " (id_city, apartment_price, date ) VALUES('{$city}', '{$price}', '{$date}' );";
								$this->Content_model->save_graph_table_custom($strSQL);
								$count++;							
							}
						}
					}
				}
				$response[0] = count($unique);
				$response[4] = count($unique2);			
			}		
	
			if($loadedSheetName == 'Villas Cities') {
				$unique = array();
				$unique2 = array();			
				foreach ($sheetData as $key => $val) {
					if($val["D"] != "") {
						$city = $cities[$val["D"]];
						if(!in_array($city, $unique)) {
							array_push($unique, $city);
						} else {
							array_push($unique2, $city);					
						}
					}
					if ($key > 2) {
						foreach ($val as $key2 => $val2) {
							$price = (int) str_replace(',','',$val2);
							$date = $sheetData[2][$key2];
	
							if ($price !== 0 ) {
								$strSQL = "INSERT INTO " . CITY_HOUSES_PRICES_GRAPH . " (id_city, house_price, date ) VALUES('{$city}', '{$price}', '{$date}' );";
								$this->Content_model->save_graph_table_custom($strSQL);
							}
						}
					}
				}
				$response[1] = count($unique);
				$response[5] = count($unique2);				
			}		
	
			if($loadedSheetName == 'Apartments Provinces') {
				$unique = array();
				foreach ($sheetData as $key => $val) {
					if($val["D"] != "") {
						$province = $provinces[$val["D"]];
						if(!in_array($province, $unique)) array_push($unique, $province);
					}
					if ($key > 2) {
						foreach ($val as $key2 => $val2) {
							$price = (int) str_replace(',','',$val2);
							$date = $sheetData[2][$key2];
	
							if ($price !== 0 ) {
								$strSQL = "INSERT INTO " . PROVINCE_APARTMENTS_PRICES_GRAPH . " (id_province, apartment_price, date ) VALUES('{$province}', '{$price}', '{$date}' );";
								$this->Content_model->save_graph_table_custom($strSQL);
							}
						}
					}
				}
				$response[2] = count($unique);
			}		
	
			if($loadedSheetName == 'Villas Provinces') {
				$unique = array();
				foreach ($sheetData as $key => $val) {
					if($val["D"] != "") {
						$province = $provinces[$val["D"]];
						if(!in_array($province, $unique)) array_push($unique, $province);
					}
					if ($key > 2) {
						foreach ($val as $key2 => $val2) {
							$price = (int) str_replace(',','',$val2);
							$date = $sheetData[2][$key2];
	
							if ($price !== 0 ) {
								$strSQL = "INSERT INTO " . PROVINCE_HOUSES_PRICES_GRAPH . " (id_province, house_price, date ) VALUES('{$province}', '{$price}', '{$date}' );";
								$this->Content_model->save_graph_table_custom($strSQL);
							}
						}
					}
				}
				$response[3] = count($unique);
			}
		}
		
	 if($response[4] > 0 || $response[5] > 0) {
		$warning = " <br>WARNING! <br>Not imported records: <br>  " . $response[4] . " - for Apartment cities <br>" . $response[5] . " - for Villas Cities ";
		} else {
			$warning = "";
		}
	
	echo "Imported Excel records: <br> " . $response[0] . " - for Apartment Cities, <br>" . $response[1] . " - for Villas Cities, <br>" . $response[2] . " - for Apartment Provinces, <br>" . $response[3] . " - for Villas Provinces <br>" . $warning;

	}
}

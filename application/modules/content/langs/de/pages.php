<?php

$install_lang["admin_header_page_add"] = "Seite hinzufügen";
$install_lang["admin_header_page_change"] = "Seite bearbeiten";
$install_lang["admin_header_page_edit"] = "Seite bearbeiten";
$install_lang["admin_header_page_list"] = "Seiten";
$install_lang["admin_header_promo_block_main"] = "Haupteinstellungen";
$install_lang["admin_header_promo_edit"] = "Promo Block Einstellungen";
$install_lang["error_content_gid_invalid"] = "Seiten Keywort ist ungültig";
$install_lang["error_content_title_invalid"] = "Seitentitel ist ungültig";
$install_lang["field_block_height"] = "Blockhöhe";
$install_lang["field_block_img_align_bottom"] = "Boden";
$install_lang["field_block_img_align_center"] = "In der Mitte";
$install_lang["field_block_img_align_hor"] = "Horizontale Bildausrichtung";
$install_lang["field_block_img_align_left"] = "Zur Linken";
$install_lang["field_block_img_align_right"] = "Zur Rechten";
$install_lang["field_block_img_align_top"] = "Top";
$install_lang["field_block_img_align_ver"] = "Vertikale Bildausrichtung";
$install_lang["field_block_img_no_repeat"] = "Nicht wiederholen";
$install_lang["field_block_img_repeat"] = "Wiederholung";
$install_lang["field_block_img_repeating"] = "Wiederholen";
$install_lang["field_block_unit_auto"] = "auto";
$install_lang["field_block_unit_percent"] = "%";
$install_lang["field_block_unit_px"] = "px";
$install_lang["field_block_width"] = "Blockbreite";
$install_lang["field_content"] = "Inhalt";
$install_lang["field_date_created"] = "erstellt";
$install_lang["field_date_modified"] = "modifiziert";
$install_lang["field_gid"] = "Keywort";
$install_lang["field_lang"] = "Sprache";
$install_lang["field_promo_flash"] = "Blitz";
$install_lang["field_promo_flash_delete"] = "Blitz löschen";
$install_lang["field_promo_flash_uploaded"] = "Datei ist hochgeladen";
$install_lang["field_promo_image_delete"] = "Bild löschen";
$install_lang["field_promo_img"] = "Bild";
$install_lang["field_promo_text"] = "Text übersetzen";
$install_lang["field_promo_type"] = "Typ des Promo Inhaltes";
$install_lang["field_promo_type_flash"] = "Blitz";
$install_lang["field_promo_type_text"] = "Text & Bild";
$install_lang["field_title"] = "Titel";
$install_lang["field_view_link"] = "Link";
$install_lang["filter_section_text"] = "Text";
$install_lang["header_index_content"] = "Informationsseiten";
$install_lang["header_info_pages"] = "Informationsseiten";
$install_lang["header_view_content"] = "Inhalt anzeigen";
$install_lang["link_add_page"] = "Seite hinzufügen";
$install_lang["link_create_subitem"] = "Unterseite hinzufügen";
$install_lang["link_delete_page"] = "Seite löschen";
$install_lang["link_edit_page"] = "Seite bearbeiten";
$install_lang["link_save_sorter"] = "Folge speichern";
$install_lang["note_delete_page"] = "Sind Sie sicher, dass Sie die Seite löschen wollen?";
$install_lang["success_add_content"] = "Seite ist erfolgreich hinzugefügt";
$install_lang["success_add_page"] = "Seite ist erfolgreich hinzugefügt";
$install_lang["success_delete_content"] = "Seite ist gelöscht";
$install_lang["success_update_content"] = "Seite ist erfolgreich aktualisiert";
$install_lang["success_update_page"] = "Seite ist erfolgreich aktualisiert";
$install_lang["success_update_promo_block"] = "Promo Block ist erfolgreich aktualisiert";


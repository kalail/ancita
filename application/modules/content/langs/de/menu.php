<?php

$install_lang["admin_menu_settings_items_content_items_content_menu_item"] = "Informationsseiten";
$install_lang["admin_menu_settings_items_content_items_content_menu_item_tooltip"] = "Hinzufügen, übersetzen & sortieren von Informationsseiten";
$install_lang["admin_menu_settings_items_interface-items_promo_menu_item"] = "Promo Block";
$install_lang["admin_menu_settings_items_interface-items_promo_menu_item_tooltip"] = "Promo Bild, Text, Blitz auf der Startseite";
$install_lang["agent_main_menu_agent-main-help-item"] = "Hilfe";
$install_lang["agent_main_menu_agent-main-help-item_tooltip"] = "";
$install_lang["company_main_menu_company-main-help-item"] = "Hilfe";
$install_lang["company_main_menu_company-main-help-item_tooltip"] = "";
$install_lang["guest_main_menu_guest-main-help-item"] = "Hilfe";
$install_lang["guest_main_menu_guest-main-help-item_tooltip"] = "";
$install_lang["private_main_menu_private-main-help-item"] = "Hilfe";
$install_lang["private_main_menu_private-main-help-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-about-us-item"] = "Über uns";
$install_lang["user_footer_menu_footer-menu-about-us-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-advertise-item"] = "Werbung";
$install_lang["user_footer_menu_footer-menu-advertise-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-terms-item"] = "Nutzungsbedingungen";
$install_lang["user_footer_menu_footer-menu-terms-item_tooltip"] = "";


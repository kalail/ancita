<?php

$install_lang["admin_menu_settings_items_content_items_content_menu_item"] = "Инфо страницы";
$install_lang["admin_menu_settings_items_content_items_content_menu_item_tooltip"] = "Добавление, перевод и сортировка инфо страниц";
$install_lang["admin_menu_settings_items_interface-items_promo_menu_item"] = "Промо блок";
$install_lang["admin_menu_settings_items_interface-items_promo_menu_item_tooltip"] = "Промо изображение, текст, флеш на индексной странице";
$install_lang["agent_main_menu_agent-main-help-item"] = "Помощь";
$install_lang["agent_main_menu_agent-main-help-item_tooltip"] = "";
$install_lang["company_main_menu_company-main-help-item"] = "Помощь";
$install_lang["company_main_menu_company-main-help-item_tooltip"] = "";
$install_lang["guest_main_menu_guest-main-help-item"] = "Помощь";
$install_lang["guest_main_menu_guest-main-help-item_tooltip"] = "";
$install_lang["private_main_menu_private-main-help-item"] = "Помощь";
$install_lang["private_main_menu_private-main-help-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-about-us-item"] = "О нас";
$install_lang["user_footer_menu_footer-menu-about-us-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-advertise-item"] = "Реклама";
$install_lang["user_footer_menu_footer-menu-advertise-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-terms-item"] = "Условия";
$install_lang["user_footer_menu_footer-menu-terms-item_tooltip"] = "";


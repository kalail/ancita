<?php

$install_lang["seo_tags_index_description"] = "Информационные страницы сайта.";
$install_lang["seo_tags_index_header"] = "Содержание страниц";
$install_lang["seo_tags_index_keyword"] = "статьи сайта, рубрики сайта";
$install_lang["seo_tags_index_og_description"] = "Информационные страницы сайта.";
$install_lang["seo_tags_index_og_title"] = "Pages content";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Содержание страниц";
$install_lang["seo_tags_view_description"] = "Информация о нас. Условия размещения рекламы на сайте. Полезная информация посетителю сайта";
$install_lang["seo_tags_view_header"] = "[title|Информация]";
$install_lang["seo_tags_view_keyword"] = "о нас, реклама на сайте, условия сайта, полезная информация";
$install_lang["seo_tags_view_og_description"] = "Информация о нас. Условия размещения рекламы на сайте. Полезная информация посетителю сайта";
$install_lang["seo_tags_view_og_title"] = "[title|Инфо страница]";
$install_lang["seo_tags_view_og_type"] = "article";
$install_lang["seo_tags_view_title"] = "Pilot Group : Просмотр : [title|Default page]";


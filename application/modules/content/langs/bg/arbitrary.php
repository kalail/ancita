<?php

$install_lang["seo_tags_index_description"] = "Информационни страници на сайта.";
$install_lang["seo_tags_index_header"] = "Съдържание на страниците";
$install_lang["seo_tags_index_keyword"] = "статьи сайта, рубрики сайта";
$install_lang["seo_tags_index_og_description"] = "Информационни страници на сайта.";
$install_lang["seo_tags_index_og_title"] = "Съдържание на страниците";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Съдържание на страниците";
$install_lang["seo_tags_view_description"] = "Информация за нас. Условия за разполагане реклама на сайта. Полезна информация за посетители на сайта";
$install_lang["seo_tags_view_header"] = "[title|Инфо страница]";
$install_lang["seo_tags_view_keyword"] = "за нас, реклама на сайта, условия на сайта, полезна информация";
$install_lang["seo_tags_view_og_description"] = "Информация за нас. Условия за публикуване реклама на сайта. Полезна информация за посетители на сайта";
$install_lang["seo_tags_view_og_title"] = "[title|Инфо страница]";
$install_lang["seo_tags_view_og_type"] = "article";
$install_lang["seo_tags_view_title"] = "Pilot Group  : Преглед: [title|Инфо страница]";


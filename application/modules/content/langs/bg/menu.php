<?php

$install_lang["admin_menu_settings_items_content_items_content_menu_item"] = "Инфо страници";
$install_lang["admin_menu_settings_items_content_items_content_menu_item_tooltip"] = "Добавяне, разполагане и сортиране на инфо страниците";
$install_lang["admin_menu_settings_items_interface-items_promo_menu_item"] = "Промо блок";
$install_lang["admin_menu_settings_items_interface-items_promo_menu_item_tooltip"] = "Промо снимка, текст, флаш на първа страница";
$install_lang["agent_main_menu_agent-main-help-item"] = "Помощ";
$install_lang["agent_main_menu_agent-main-help-item_tooltip"] = "";
$install_lang["company_main_menu_company-main-help-item"] = "Помощ";
$install_lang["company_main_menu_company-main-help-item_tooltip"] = "";
$install_lang["guest_main_menu_guest-main-help-item"] = "Помощ";
$install_lang["guest_main_menu_guest-main-help-item_tooltip"] = "";
$install_lang["private_main_menu_private-main-help-item"] = "Помощ";
$install_lang["private_main_menu_private-main-help-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-about-us-item"] = "За нас";
$install_lang["user_footer_menu_footer-menu-about-us-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-advertise-item"] = "Реклама";
$install_lang["user_footer_menu_footer-menu-advertise-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-terms-item"] = "Условия за ползване";
$install_lang["user_footer_menu_footer-menu-terms-item_tooltip"] = "";


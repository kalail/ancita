<?php

$install_lang["admin_menu_settings_items_content_items_content_menu_item"] = "Información de páginas";
$install_lang["admin_menu_settings_items_content_items_content_menu_item_tooltip"] = "Añadir, traducir y ordenar la información de páginas";
$install_lang["admin_menu_settings_items_interface-items_promo_menu_item"] = "Bloque de promoción";
$install_lang["admin_menu_settings_items_interface-items_promo_menu_item_tooltip"] = "Imagen de promo, texto, flash en la portada del sitio";
$install_lang["agent_main_menu_agent-main-help-item"] = "Ayuda";
$install_lang["agent_main_menu_agent-main-help-item_tooltip"] = "";
$install_lang["company_main_menu_company-main-help-item"] = "Ayuda";
$install_lang["company_main_menu_company-main-help-item_tooltip"] = "";
$install_lang["guest_main_menu_guest-main-help-item"] = "Ayuda";
$install_lang["guest_main_menu_guest-main-help-item_tooltip"] = "";
$install_lang["private_main_menu_private-main-help-item"] = "Ayuda";
$install_lang["private_main_menu_private-main-help-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-about-us-item"] = "¿Quiénes somos?";
$install_lang["user_footer_menu_footer-menu-about-us-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-advertise-item"] = "Anunciar";
$install_lang["user_footer_menu_footer-menu-advertise-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-terms-item"] = "Condiciones de uso";
$install_lang["user_footer_menu_footer-menu-terms-item_tooltip"] = "";


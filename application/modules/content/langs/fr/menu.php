<?php

$install_lang["admin_menu_settings_items_content_items_content_menu_item"] = "Pages d'info";
$install_lang["admin_menu_settings_items_content_items_content_menu_item_tooltip"] = "Ajouter, traduire et trier pages d'info";
$install_lang["admin_menu_settings_items_interface-items_promo_menu_item"] = "Bloc de promotion";
$install_lang["admin_menu_settings_items_interface-items_promo_menu_item_tooltip"] = "Texte/image/flash de promo sur page d'acceuil";
$install_lang["agent_main_menu_agent-main-help-item"] = "Information";
$install_lang["agent_main_menu_agent-main-help-item_tooltip"] = "";
$install_lang["company_main_menu_company-main-help-item"] = "Information";
$install_lang["company_main_menu_company-main-help-item_tooltip"] = "";
$install_lang["guest_main_menu_guest-main-help-item"] = "Information";
$install_lang["guest_main_menu_guest-main-help-item_tooltip"] = "";
$install_lang["private_main_menu_private-main-help-item"] = "Information";
$install_lang["private_main_menu_private-main-help-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-about-us-item"] = "Notre info";
$install_lang["user_footer_menu_footer-menu-about-us-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-advertise-item"] = "Annonces";
$install_lang["user_footer_menu_footer-menu-advertise-item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-terms-item"] = "Termes d'usage";
$install_lang["user_footer_menu_footer-menu-terms-item_tooltip"] = "";


<?php

$install_lang["seo_tags_index_description"] = "Information pages.";
$install_lang["seo_tags_index_header"] = "Contenu des pages";
$install_lang["seo_tags_index_keyword"] = "articles, help, advice, blog, news";
$install_lang["seo_tags_index_og_description"] = "Information pages.";
$install_lang["seo_tags_index_og_title"] = "Contenu des pages";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Contenu des pages";
$install_lang["seo_tags_view_description"] = "About us. Terms of advertising. Information useful for site members.";
$install_lang["seo_tags_view_header"] = "[title|Information]";
$install_lang["seo_tags_view_keyword"] = "about us, advertisement, terms of use, helpful information";
$install_lang["seo_tags_view_og_description"] = "About us. Terms of advertising. Information useful for site members.";
$install_lang["seo_tags_view_og_title"] = "[title|Information]";
$install_lang["seo_tags_view_og_type"] = "article";
$install_lang["seo_tags_view_title"] = "Pilot Group : Voir page : [title|Information]";


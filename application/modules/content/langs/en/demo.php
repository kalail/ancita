<?php

$install_lang["content"] = "Search multiple real estate listings of residential, commercial and vacation getaway properties, find homes for sale and apartments for rent. Add a property for sale or lease. Thousands of buyers and tenants are matched with sellers and landlords daily.";


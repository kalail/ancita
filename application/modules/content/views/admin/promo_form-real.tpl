{include file="header.tpl"}
<div class="menu-level2">
	<ul>
		{foreach item=item key=lang_id from=$languages}
		<li{if $lang_id eq $current_lang} class="active"{/if}><div class="l"><a href="{$site_url}admin/content/promo/{$lang_id}">{$item.name}</a></div></li>
		{/foreach}
	</ul>
	&nbsp;
</div>

<div class="actions">&nbsp;</div>

<form method="post" action="" name="save_form">
	<div class="edit-form n150">
		<div class="row header">{l i='admin_header_promo_block_main' gid='content'}</div>
		<div class="row">
			<div class="h">{l i='field_promo_type' gid='content'}: </div>
			<div class="v">
				<select name="content_type">
				<option value="t"{if $promo_data.content_type eq 't'} selected{/if}>{l i='field_promo_type_text' gid='content'}</option>	
				<option value="f"{if $promo_data.content_type eq 'f'} selected{/if}>{l i='field_promo_type_flash' gid='content'}</option>	
				</select>
			</div>
		</div>
		<div class="row zebra">
			<div class="h">{l i='field_block_width' gid='content'}: </div>
			<div class="v">
				<select name="block_width_unit" class="units">
				<option value="auto"{if $promo_data.block_width_unit eq 'auto'} selected{/if}>{l i='field_block_unit_auto' gid='content'}</option>	
				<option value="px"{if $promo_data.block_width_unit eq 'px'} selected{/if}>{l i='field_block_unit_px' gid='content'}</option>	
				<option value="%"{if $promo_data.block_width_unit eq '%'} selected{/if}>{l i='field_block_unit_percent' gid='content'}</option>	
				</select>
				<input type="text" class="short unit_val" name="block_width" value="{$promo_data.block_width}" {if $promo_data.block_width_unit eq 'auto'} disabled{/if}>
			</div>
		</div>
		<div class="row">
			<div class="h">{l i='field_block_height' gid='content'}: </div>
			<div class="v">
				<select name="block_height_unit" class="units">
				<option value="auto"{if $promo_data.block_height_unit eq 'auto'} selected{/if}>{l i='field_block_unit_auto' gid='content'}</option>	
				<option value="px"{if $promo_data.block_height_unit eq 'px'} selected{/if}>{l i='field_block_unit_px' gid='content'}</option>	
				</select>
				<input type="text" class="short unit_val" name="block_height" value="{$promo_data.block_height}" {if $promo_data.block_height_unit eq 'auto'} disabled{/if}>
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save_settings" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<div class="clr"></div>
</form>

<div class="menu-level3">
	<ul>
		<li{if $content_type eq 't'} class="active"{/if}><div class="l"><a href="{$site_url}admin/content/promo/{$current_lang}/t">{l i='field_promo_type_text' gid='content'}</a></div></li>
		<li{if $content_type eq 'f'} class="active"{/if}><div class="l"><a href="{$site_url}admin/content/promo/{$current_lang}/f">{l i='field_promo_type_flash' gid='content'}</a></div></li>
	</ul>
	&nbsp;
</div>

<form method="post" action="{$site_url}admin/content/promo/{$current_lang}/{$content_type}" name="save_form"  enctype="multipart/form-data">
{if $content_type eq 't'}
	<div class="edit-form n150">
		<div class="row header">&nbsp;</div>
		<div class="row">
			<div class="h">{l i='field_promo_text' gid='content'}: </div>
			<div class="v">
				{$promo_data.promo_text_fck}
			</div>
		</div>
		<div class="row zebra">
			<div class="h">{l i='field_block_img_align_hor' gid='content'}: </div>
			<div class="v">
				<select name="block_align_hor">
				<option value="center"{if $promo_data.block_align_hor eq 'center'} selected{/if}>{l i='field_block_img_align_center' gid='content'}</option>
				<option value="left"{if $promo_data.block_align_hor eq 'left'} selected{/if}>{l i='field_block_img_align_left' gid='content'}</option>
				<option value="right"{if $promo_data.block_align_hor eq 'right'} selected{/if}>{l i='field_block_img_align_right' gid='content'}</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="h">{l i='field_block_img_align_ver' gid='content'}: </div>
			<div class="v">
				<select name="block_align_ver">
				<option value="center"{if $promo_data.block_align_ver eq 'center'} selected{/if}>{l i='field_block_img_align_center' gid='content'}</option>
				<option value="top"{if $promo_data.block_align_ver eq 'top'} selected{/if}>{l i='field_block_img_align_top' gid='content'}</option>
				<option value="bottom"{if $promo_data.block_align_ver eq 'bottom'} selected{/if}>{l i='field_block_img_align_bottom' gid='content'}</option>
				</select>
			</div>
		</div>
		<div class="row zebra">
			<div class="h">{l i='field_block_img_repeating' gid='content'}: </div>
			<div class="v">
				<select name="block_image_repeat">
				<option value="repeat"{if $promo_data.block_image_repeat eq 'repeat'} selected{/if}>{l i='field_block_img_repeat' gid='content'}</option>
				<option value="no-repeat"{if $promo_data.block_image_repeat eq 'no-repeat'} selected{/if}>{l i='field_block_img_no_repeat' gid='content'}</option>
				</select>
			</div>
		</div>
        <div class="row">
			<div class="h">{l i='field_block_img_text_color' gid='content'}: </div>
			<div class="v">
                <select name="promo_text_color">
                	<option value="black" {if $promo_data.promo_text_color eq 'black'} selected{/if}>{l i='field_block_img_black' gid='content'}</option>
                    <option value="white" {if $promo_data.promo_text_color eq 'white'} selected{/if}>{l i='field_block_img_White' gid='content'}</option>
                </select>
			</div>
		</div>
        <div class="btn"><div class="l"><input type="submit" name="btn_save_content" value="{l i='btn_save' gid='start' type='button'}"></div></div>
        <div class="clr"></div>
        
        
         {js file='ajaxfileupload.min.js'}
	{js file='gallery-uploads.js'}
    
    <script>{literal}
        var gUpload;
        $(function(){
            gUpload = new galleryUploads({
                siteUrl: '{/literal}{$site_root}{literal}',
        });
    });
    {/literal}
	</script>
        
        <div class="row header">{l i='field_block_slider_tit' gid='content'}</div>
        <div class="row zebra">
        	<div class="h">{l i='field_block_slider_sel' gid='content'}:</div>
			<div class="v">
				<input type="file" name="promo_slide_image">
			</div>
		</div>
        <div class="row zebra">
			<div class="h">{l i='field_block_slider_tim' gid='content'}:</div>
			<div class="v">
                <input type="text" id="block_each_image_seconds" name="block_each_image_seconds" value="10" style="width:100px !important;" />
			</div>
		</div>
        <div class="row zebra">
			<div class="h">{l i='field_block_slider_order' gid='content'}:</div>
			<div class="v">
                <input type="text" id="slider_image_order" name="slider_image_order" value="" style="width:100px !important;" />
			</div>
		</div>
        <div class="row zebra">
			<div class="h">{l i='field_block_slider_adnum' gid='content'}:</div>
			<div class="v">
                <input type="text" id="promo_url" name="promo_url" value="" style="width:400px !important;" />
			</div>
		</div>
        <div class="row" id="rwnewslide">
        	<div class="btn"><div class="l"><input type="submit" name="btn_Add_Slide" value="{l i='btn_add' gid='start' type='button'}"></div></div>
        </div>
        
        <div class="row" id="rwupdateslide" style="display:none;">
        	<div class="btn"><div class="l"><input type="submit" name="btn_Update_Slide" value="Update"></div></div>
            <div class="btn"><div class="l"><input type="button" name="btn_Reset_Slide" onclick="javascript: Reset();return false;" value="Reset"></div></div>
        </div>
	        <input type="hidden" id="block_slide_id" name="block_slide_id" value="" />
            <input type="hidden" id="Lang_id" name="Lang_id" value="{$current_lang}" />
             <input type="hidden" id="hidden_order" name="hidden_order" value="" />
		<div class="clr"></div>
        
        
        <div class="row">
        
        <ol class="blocks" id="sortList">
        
        {foreach item=item from=$promo_data_slide}
<li id="pItem{$item.Id}">
            
            <div class="photo-info-area">
                <div class="photo-area">
                <img src="{$item.Promo_slide.file_url}" hspace="3" onclick="javascript: gUpload.full_view('{$item.Promo_slide.file_url}');" style="width:200px;height:200px;" />
                    
		<br>
        <b> 
        	Order: 
        	<font class="stat-active">
        		{$item.SortingOrder}
           	</font>
		</b>
        <b> 
        	Seconds: 
        	<font class="stat-active">
        		{$item.promo_each_image_seconds}
           	</font>
		</b>
		<br />
        <b> 
        	URL: 
        	<font class="stat-active">
        		{$item.promo_url}
           	</font>
		</b>  
                    <script>{literal}
						function Edit(editId,url,seconds,order){
							$("#block_slide_id").val(editId);
							$("#block_each_image_seconds").val(seconds);
							$("#promo_url").val(url);
							$("#hidden_order").val(order);
							$("#slider_image_order").val(order);
							$("#rwnewslide").attr("style","display:none");
							$("#rwupdateslide").attr("style","display:block");
							$("#block_each_image_seconds").focus();
						}
						
						function Reset(){
							
							$("#block_slide_id").val('');
							$("#block_each_image_seconds").val('');
							$("#promo_url").val('');
							$("#slider_image_order").val('');
							$("#rwnewslide").attr("style","display:block");
							$("#rwupdateslide").attr("style","display:none");
							}
						
						{/literal}
					</script>
                    
                    <a href="#" onclick="javascript: Edit({$item.Id},'{$item.promo_url}',{$item.promo_each_image_seconds},{$item.SortingOrder}); return false;">
                    <img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='btn_edit' gid='start' type='button'}" title="{l i='btn_edit' gid='start' type='button'}">
                    </a>

                    <a href="{$site_root}admin/content/delete_slide_photo/{$item.Id}/{$current_lang}/{$item.Promo_slide.file_name}" >
                         <img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='btn_delete' gid='start' type='button'}" title="{l i='btn_delete' gid='start' type='button'}">
                    </a>
                    
                	<div class="clr"></div>
            
				</div>
                </div>
            </li>
            
		{/foreach}
        </ol>
		<div class="clr"></div>
        </div>
        
        
		<!--	<div class="row">
			<div class="h">{l i='field_promo_img' gid='content'} 1: </div>
			<div class="v">
				<input type="file" name="promo_image">
				{if $promo_data.promo_image}<br><img src="{$promo_data.media.promo_image.file_url}" width="500">{/if}
			</div>
		</div>
		{if $promo_data.promo_image}
		<div class="row zebra">
			<div class="h">{l i='field_promo_image_delete' gid='content'}: </div>
			<div class="v"><input type="checkbox" name="promo_image_delete" value="1"></div>
		</div>
		{/if}-->
      
	</div>
{/if}
{if $content_type eq 'f'}
	<div class="edit-form n150">
		<div class="row header">&nbsp;</div>
		<div class="row">
			<div class="h">{l i='field_promo_flash' gid='content'}: </div>
			<div class="v">
				<input type="file" name="promo_flash"><br>
				{if $promo_data.promo_flash}<i>{l i='field_promo_flash_uploaded' gid='content'}</i>{/if}
			</div>
		</div>
		{if $promo_data.promo_flash}
		<div class="row zebra">
			<div class="h">{l i='field_promo_flash_delete' gid='content'}: </div>
			<div class="v"><input type="checkbox" name="promo_flash_delete" value="1"></div>
		</div>
		{/if}
	</div>
    
    <div class="btn"><div class="l"><input type="submit" name="btn_save_content" value="{l i='btn_save' gid='start' type='button'}"></div></div>

{/if}
	<div class="clr"></div>
</form>
<script type="text/javascript">{literal}
$(function(){
	$('.units').bind('change', function(){
		if($(this).val() == 'auto'){
			$(this).parent().find('input.unit_val').attr('disabled', 'disabled');
		}else{
			$(this).parent().find('input.unit_val').removeAttr('disabled');
		}	
	});
});
{/literal}</script>
<div class="clr"></div>
{include file="footer.tpl"}

{include file="header.tpl"}
<div class="menu-level2">
	<ul>
		<li{if $tab_id eq 'Placefactors'} class="active"{/if}><div class="l"><a href="{$site_url}admin/content/marketing/Placefactors">{l i='calc_admin_heading1' gid='content'}</a></div></li>
        <li{if $tab_id eq 'Commonfactors'} class="active"{/if}><div class="l"><a href="{$site_url}admin/content/marketing/Commonfactors">{l i='calc_admin_heading2' gid='content'}</a></div></li>
        <li{if $tab_id eq 'Excelimport'} class="active"{/if}><div class="l"><a href="{$site_url}admin/content/marketing/Excelimport">{l i='calc_admin_heading3' gid='content'}</a></div></li>
	</ul>
	&nbsp;
</div>
<div class="actions">&nbsp;</div>
<form method="post" name="save_form" action="{$site_url}admin/content/marketing/{$tab_id}" enctype="multipart/form-data">
{if $tab_id eq 'Placefactors'}
<div class="edit-form edit-form-marketing">
<div class="row header">{l i='calc_admin_lbl_new_functionality' gid='content'}</div>
<div class="row zebra">
<table>
	<tr>
    	<td width="160px">{l i='calc_admin_lbl_new_functionality_on' gid='content'}</td>
        <td>
        	<input type="checkbox" name="check_functionality" value="{$factors_graph.functionality}"  {if $factors_graph.functionality eq 'on'}checked {/if}  />
        </td>
        <td>
        	<div class="btn"><div class="l"><input type="submit" name="btn_trunon_new_functionality" value="{l i='btn_save' gid='start' type='button'}"></div></div>
        </td>
    </tr>
</table>
</div>
</div>
{/if}
{if $tab_id ne 'Excelimport'}
{if $factors_graph.functionality ne 'on' || $tab_id eq 'Commonfactors'}
<div class="edit-form edit-form-marketing">
<div class="row header">{l i='calc_admin_version_setting' gid='content'}</div>
<div class="row zebra">
<table>
	<tr>
    	<td width="100px">{l i='calc_admin_select_version' gid='content'}</td>
        <td>
        	<select name="versionMonth" id="versionMonth" onchange="javascript:changeVersion();">
                {foreach from=$arrMonth key="key" item="month"}
                	<option value="{if $key ne 0}{$key}{/if}" {if $version.Month eq $key} selected {/if} >{$month}</option>
                {/foreach}
            </select>
            <input type="hidden" name="crntVersionMonth" id="crntVersionMonth" value="{$crntversion.Month}" />
        </td>
        <td>
        	<select name="versionYear" id="versionYear" onchange="javascript:changeVersion();">
            	<option value="">{l i='calc_admin_option_select_year' gid='content'}</option>
                {foreach from=$arrYear key="key" item="year"}
                	<option value="{$year}" {if $version.Year eq $year} selected {/if} >{$year}</option>
                {/foreach}
            </select>
            <input type="hidden" name="crntVersionYear" id="crntVersionYear" value="{$crntversion.Year}" />
        </td>
        <td>
        	<input type="checkbox" value="1" name="IsActiveVersion" {if $IsActiveVersion eq '1'} checked {/if} /> {l i='calc_admin_is_active_version' gid='content'}
        </td>
    </tr>
</table>
</div>
{literal}
<script>
	function changeVersion()
	{
		var nvMonth = $('#versionMonth option:selected').val();
		var nvYear = $('#versionYear option:selected').val();
		var ovMonth = $('#crntVersionMonth').val();
		var ovYear = $('#crntVersionYear').val();
		
		location.href = '{/literal}{$site_root}{literal}admin/content/marketing/{/literal}{$tab_id}{literal}/Change/'+nvMonth+'/'+nvYear+'/'+ovMonth+'/'+ovYear;
	}
</script>
{/literal}
</div>
{/if}
{/if}
{if $tab_id eq 'Placefactors'}
<div class="edit-form edit-form-marketing">
<div class="row header">{l i='calc_admin_lbl_price_factor' gid='content'}</div>
<div class="row">
	<table style="width:100%;">
    <tr>
    	<th>{l i='calc_admin_region' gid='content'}</th>
        <th>{l i='calc_admin_city' gid='content'}</th>
        <th>{l i='calc_admin_appartment_price' gid='content'}</th>
        <th>{l i='calc_admin_house_price' gid='content'}</th>
        <th>{l i='calc_admin_urbanicasion' gid='content'}</th>
        <th>{l i='calc_admin_area_adjustment' gid='content'}</th>
    </tr>
    <tr height="5px;"></tr>
    {foreach from=$cities item=item name=it}
    <tr height="5px"></tr>
    <tr>
    	<td>{$item.region_name}</td>
        <td style="padding-left: 20px;">{$item.city_name}</td>
        <td><input size="8" type="text" name="appartment_{$item.city_id}" value="{$item.appartment_price}"></td>
        <td><input size="8" type="text" name="house_{$item.city_id}" value="{$item.house_price}" style="width:auto;"></td>
        {if $item.urbanication ne NULL}
            {foreach from=$item.urbanication item=item_u name=it_u}
            	{if $item_u.row > 0}
                	</tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                {/if}
                <td>
                    {$item_u.name}
                </td>												
                <td>
                    <input size="8" type="text" name="adjustment_{$item_u.id}_{$item.city_id}" value="{$item_u.adjustment}">
                </td>
            {/foreach}
        {/if}
    </tr>
    {/foreach}
    </table>
    {if $factors_graph.functionality ne 'on'}
   <div class="btn"><div class="l"><input type="submit" name="btn_save_Placefactors" value="{l i='btn_save' gid='start' type='button'}"></div></div>
   {/if}
</div>
</div>
<div class="clr"></div>
{/if}
{if $tab_id eq 'Commonfactors'}
<div class="edit-form edit-form-marketing">
	<div class="row header">{l i='calc_admin_lbl_price_presentations' gid='content'}</div>
    <div class="row">
    	<table class="form_table2">
        	<tr>
            	<td width="230px">
               		<h4 align="center">{l i='calc_admin_lbl_price_adjustment' gid='content'}</h4>
                	<table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column"><div class="fr tr_height">{l i='calc_admin_lowest_price_presented' gid='content'}</div></td>
                            <td><input size="8" type="text" name="price_min" value="{$factors.price_min}"></td>
                        </tr>
                        <tr>
                            <td class="left_column"><div class="fr tr_height">{l i='calc_admin_highest_price_presented' gid='content'}</div></td>
                            <td><input size="8" type="text" name="price_max" value="{$factors.price_max}"></td>
                        </tr>
                    </table>
                </td>
                <td width="230px">
                	<h4 align="center">{l i='calc_admin_gliding_factor' gid='content'}</h4>																		
                    <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column"><div class="fr tr_height">{l i='calc_admin_factor' gid='content'}</div></td>
                            <td>
                            <select class="sel-cat" name="gliding" id="area_select"  style="width:120px;">
                            <option value="">{l i='calc_admin_choose_factor' gid='content'}</option>
                                <option value="1" {if $factors.gliding == 1} selected {/if}>1</option>
                                <option value="2" {if $factors.gliding == 2} selected {/if}>2</option>
                                <option value="3" {if $factors.gliding == 3} selected {/if}>3</option>
                                <option value="4" {if $factors.gliding == 4} selected {/if}>4</option>
                                <option value="5" {if $factors.gliding == 5} selected {/if}>5</option>
                                <option value="6" {if $factors.gliding == 6} selected {/if}>6</option>
                                <option value="7" {if $factors.gliding == 7} selected {/if}>7</option>														
                            </select>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    <div class="row header">{l i='calc_admin_house_apartments' gid='content'}</div>
    <div class="row">
    	<table class="form_table2">
        	<tr>
            	<td width="230px">
                    <h4 align="center">{l i='calc_admin_Beach' gid='content'} (AA)</h4>																		
                    <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column">{l i='calc_option_1st_line' gid='content'}</td>
                            <td><input size="8" type="text" name="aa_1" value="{$factors.aa_1}"></td>
                        </tr>
                        <tr>
                            <td class="left_column">{l i='calc_option_lessthen_15min_walk' gid='content'}</td>
                            <td><input size="8" type="text" name="aa_2" value="{$factors.aa_2}"></td>
                        </tr>
                        <tr>
                            <td class="left_column">{l i='calc_option_morethan_15minutes_walk' gid='content'}</td>
                            <td><input size="8" type="text" name="aa_3" value="{$factors.aa_3}"></td>
                        </tr>												
                    </table>
            	</td>
                <td width="230px">
                    <h4 align="center">{l i='calc_admin_View' gid='content'} (BB)</h4>												
                    <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column">{l i='calc_option_sea' gid='content'}</td>
                            <td><input size="8" type="text" name="bb_1" value="{$factors.bb_1}"></td>
                        </tr>
                        <tr>
                            <td class="left_column">{l i='calc_option_mountain_garden' gid='content'}</td>
                            <td><input size="8" type="text" name="bb_2" value="{$factors.bb_2}"></td>
                        </tr>
                        <tr>
                            <td class="left_column">{l i='calc_option_city_street' gid='content'}</td>
                            <td><input size="8" type="text" name="bb_3" value="{$factors.bb_3}"></td>
                        </tr>												
                    </table>										
                </td>
                <td width="230px">
                    <h4 align="center">{l i='calc_lbl_access_benefits' gid='content'}</h4>												
                    <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column">{l i='calc_option_tennis' gid='content'} <b>(CC)</b></td>
                            <td><input size="8" type="text" name="cc" value="{$factors.cc}"></td>
                        </tr>
                        <tr>
                            <td class="left_column">{l i='calc_option_football' gid='content'} <b>(DD)</b></td>
                            <td><input size="8" type="text" name="dd" value="{$factors.dd}"></td>
                        </tr>
                        <tr>
                            <td class="left_column">{l i='calc_option_golf' gid='content'} <b>(EE)</b></td>
                            <td><input size="8" type="text" name="ee" value="{$factors.ee}"></td>
                        </tr>												
                    </table>										
                </td>
                <td width="230px">
                    <h4 align="center">{l i='calc_lbl_walking_distance_10min' gid='content'}</h4>																		
                    <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column">{l i='calc_option_buss' gid='content'} <b>(FF)</b></td>
                            <td><input size="8" type="text" name="ff" value="{$factors.ff}"></td>
                        </tr>
                        <tr>
                            <td class="left_column">{l i='calc_option_supermarket' gid='content'} <b>(GG)</b></td>
                            <td><input size="8" type="text" name="gg" value="{$factors.gg}"></td>
                        </tr>
                        <tr>
                            <td class="left_column">{l i='calc_option_restaurants' gid='content'} <b>(HH)</b></td>
                            <td><input size="8" type="text" name="hh" value="{$factors.hh}"></td>
                        </tr>												
                    </table>
                </td>
            <tr>
            <tr>
            	<td width="230px">
                    <h4 align="center">{l i='calc_admin_bathroom_factor' gid='content'} (II)</h4>												
                    <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column">({l i='calc_admin_Bath' gid='content'}+{l i='calc_admin_wc' gid='content'})/{l i='calc_admin_bed' gid='content'} < 0.49</td>
                            <td><input size="8" type="text" name="ii_1" value="{$factors.ii_1}"></td>
                        </tr>
                        <tr>
                            <td class="left_column">({l i='calc_admin_Bath' gid='content'}+{l i='calc_admin_wc' gid='content'})/{l i='calc_admin_bed' gid='content'})0.49-0.69</td>
                            <td><input size="8" type="text" name="ii_2" value="{$factors.ii_2}"></td>
                        </tr>
                        <tr>
                            <td class="left_column">({l i='calc_admin_Bath' gid='content'}+{l i='calc_admin_wc' gid='content'})/{l i='calc_admin_bed' gid='content'} > 0.69</td>
                            <td><input size="8" type="text" name="ii_3" value="{$factors.ii_3}"></td>
                        </tr>												
                    </table>										
                </td>
            </tr>
        </table>
    </div>
    <div class="row header">{l i='calc_admin_apartment' gid='content'}</div>
    <div class="row">
    	<table class="form_table2">
        <tr>
        	<td width="230px">
                <h4 align="center">{l i='calc_admin_type' gid='content'} (BA)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_ground_floor' gid='content'}</td>
                        <td><input size="8" type="text" name="ba_1" value="{$factors.ba_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_low_floor' gid='content'}</td>
                        <td><input size="8" type="text" name="ba_2" value="{$factors.ba_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_middle' gid='content'}</td>
                        <td><input size="8" type="text" name="ba_3" value="{$factors.ba_3}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_upper' gid='content'}</td>
                        <td><input size="8" type="text" name="ba_4" value="{$factors.ba_4}"></td>
                    </tr>												
                    <tr>
                        <td class="left_column">{l i='calc_option_duplex' gid='content'}</td>
                        <td><input size="8" type="text" name="ba_5" value="{$factors.ba_5}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_penthouse' gid='content'}</td>
                        <td><input size="8" type="text" name="ba_6" value="{$factors.ba_6}"></td>
                    </tr>													
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_lbl_elevator' gid='content'} (BB)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="bb" value="{$factors.bb}"></td>
                    </tr>
                </table>										
            </td>
            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_admin_terrasse' gid='content'} (BC)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="bc_1" value="{$factors.bc_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="bc_2" value="{$factors.bc_2}"></td>
                    </tr>
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_admin_terrasse' gid='content'} - {l i='calc_admin_barbeque' gid='content'} (BD)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="bd_1" value="{$factors.bd_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="bd_2" value="{$factors.bd_2}"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        	<td width="230px" valign="top">
                <h4 align="center">{l i='calc_admin_other_terrasse' gid='content'}</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">{l i='calc_admin_solarium' gid='content'} <b>(BE)</b></td>
                        <td><input size="8" type="text" name="be" value="{$factors.be}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_admin_south_faced' gid='content'} <b>(BF)</b></td>
                        <td><input size="8" type="text" name="bf" value="{$factors.bf}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_admin_south_west_faced' gid='content'} <b>(BG)</b></td>
                        <td><input size="8" type="text" name="bg" value="{$factors.bg}"></td>
                    </tr>												
                </table>										
            </td>

            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_lbl_garage' gid='content'} (BH)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="bh_1" value="{$factors.bh_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="bh_2" value="{$factors.bh_2}"></td>
                    </tr>
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_lbl_swimmingpool' gid='content'} (BI)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_common' gid='content'}</td>
                        <td><input size="8" type="text" name="bi_1" value="{$factors.bi_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_private' gid='content'}</td>
                        <td><input size="8" type="text" name="bi_2" value="{$factors.bi_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_none' gid='content'}</td>
                        <td><input size="8" type="text" name="bi_3" value="{$factors.bi_3}"></td>
                    </tr>												
                </table>										
            </td>
            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_lbl_quality_standard' gid='content'} (BJ)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_low' gid='content'}</td>
                        <td><input size="8" type="text" name="bj_1" value="{$factors.bj_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_medium' gid='content'}</td>
                        <td><input size="8" type="text" name="bj_2" value="{$factors.bj_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_high' gid='content'}</td>
                        <td><input size="8" type="text" name="bj_3" value="{$factors.bj_3}"></td>
                    </tr>												
                </table>										
            </td>
        </tr>
        <tr>
        	<td width="230px" valign="top">
                <h4 align="center">{l i='calc_admin_years_since_finished' gid='content'} (BK)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">> 20</td>
                        <td><input size="8" type="text" name="bk_1" value="{$factors.bk_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">20 - 10</td>
                        <td><input size="8" type="text" name="bk_2" value="{$factors.bk_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">9 - 5</td>
                        <td><input size="8" type="text" name="bk_3" value="{$factors.bk_3}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">< 5</td>
                        <td><input size="8" type="text" name="bk_4" value="{$factors.bk_4}"></td>
                    </tr>                                                
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_admin_years_since_renovated' gid='content'} (BL)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">> 20</td>
                        <td><input size="8" type="text" name="bl_1" value="{$factors.bl_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">20 - 10</td>
                        <td><input size="8" type="text" name="bl_2" value="{$factors.bl_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">9 - 5</td>
                        <td><input size="8" type="text" name="bl_3" value="{$factors.bl_3}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">< 5</td>
                        <td><input size="8" type="text" name="bl_4" value="{$factors.bl_4}"></td>
                    </tr>		                                                
                </table>										
            </td>
            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_admin_turistico_type' gid='content'} (BM)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="bm_1" value="{$factors.bm_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="bm_2" value="{$factors.bm_2}"></td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
     </div>
     <div class="row header">{l i='calc_admin_house' gid='content'}</div>
     <div class="row">
    	<table class="form_table2">
        <tr>
        	<td width="230px">
                <h4 align="center">{l i='calc_lbl_plot' gid='content'} (A)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">< 500 sqm</td>
                        <td><input size="8" type="text" name="a_1" value="{$factors.a_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">500 - 1500 sqm</td>
                        <td><input size="8" type="text" name="a_2" value="{$factors.a_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">> 1500 sqm</td>
                        <td><input size="8" type="text" name="a_3" value="{$factors.a_3}"></td>
                    </tr>												
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_lbl__self_owned_plot' gid='content'} (B)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="b_2" value="{$factors.b_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="b_1" value="{$factors.b_1}"></td>
                    </tr>
                </table>
            </td>
            <td width="230px">
                <h4 align="center">{l i='calc_admin_type' gid='content'} (C)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_villa' gid='content'}</td>
                        <td><input size="8" type="text" name="c_1" value="{$factors.c_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_Semidetached_villa' gid='content'}</td>
                        <td><input size="8" type="text" name="c_2" value="{$factors.c_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_finca' gid='content'}</td>
                        <td><input size="8" type="text" name="c_3" value="{$factors.c_3}"></td>
                    </tr>												
                </table>										
            </td>
            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_admin_floors' gid='content'} (D)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">> 1 fl</td>
                        <td><input size="8" type="text" name="d_1" value="{$factors.d_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_admin_allon_one_floor' gid='content'}</td>
                        <td><input size="8" type="text" name="d_2" value="{$factors.d_2}"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        	<td width="230px" valign="top">
                <h4 align="center">{l i='calc_lbl_garden' gid='content'} (E)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="e_1" value="{$factors.e_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="e_2" value="{$factors.e_2}"></td>
                    </tr>
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_lbl_garden' gid='content'} - {l i='calc_admin_barbeque' gid='content'} (F)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="f_1" value="{$factors.f_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="f_2" value="{$factors.f_2}"></td>
                    </tr>
                </table>
            </td>
            <td width="230px">
                <h4 align="center">{l i='calc_lbl_garage' gid='content'} (G)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_car_port' gid='content'}</td>
                        <td><input size="8" type="text" name="g_1" value="{$factors.g_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_garage' gid='content'}</td>
                        <td><input size="8" type="text" name="g_2" value="{$factors.g_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_no_car' gid='content'}</td>
                        <td><input size="8" type="text" name="g_3" value="{$factors.g_3}"></td>
                    </tr>                                                
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_lbl_swimmingpool' gid='content'} (H)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_common' gid='content'}</td>
                        <td><input size="8" type="text" name="h_1" value="{$factors.h_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_private' gid='content'}</td>
                        <td><input size="8" type="text" name="h_2" value="{$factors.h_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_none' gid='content'}</td>
                        <td><input size="8" type="text" name="h_3" value="{$factors.h_3}"></td>
                    </tr>												
                </table>										
            </td>
        </tr>
        <tr>
        	<td width="230px" valign="top">
                <h4 align="center">{l i='calc_lbl_quality_standard' gid='content'} (I)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_low' gid='content'}</td>
                        <td><input size="8" type="text" name="i_1" value="{$factors.i_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_medium' gid='content'}</td>
                        <td><input size="8" type="text" name="i_2" value="{$factors.i_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">{l i='calc_option_high' gid='content'}</td>
                        <td><input size="8" type="text" name="i_3" value="{$factors.i_3}"></td>
                    </tr>												
                </table>										
            </td>
            <td width="230px" valign="top">                                        
                <h4 align="center">{l i='calc_admin_years_since_finished' gid='content'} (J)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">> 20</td>
                        <td><input size="8" type="text" name="j_1" value="{$factors.j_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">20 - 10</td>
                        <td><input size="8" type="text" name="j_2" value="{$factors.j_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">9 - 5</td>
                        <td><input size="8" type="text" name="j_3" value="{$factors.j_3}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">< 5</td>
                        <td><input size="8" type="text" name="j_4" value="{$factors.j_4}"></td>
                    </tr>                                                
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center">{l i='calc_admin_years_since_renovated' gid='content'} (K)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">> 20</td>
                        <td><input size="8" type="text" name="k_1" value="{$factors.k_1}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">20 - 10</td>
                        <td><input size="8" type="text" name="k_2" value="{$factors.k_2}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">9 - 5</td>
                        <td><input size="8" type="text" name="k_3" value="{$factors.k_3}"></td>
                    </tr>
                    <tr>
                        <td class="left_column">< 5</td>
                        <td><input size="8" type="text" name="k_4" value="{$factors.k_4}"></td>
                    </tr>		                                                
                </table>										
            </td>
        </tr>
        </table>
     </div>
     <div class="row">
     <div class="btn"><div class="l"><input type="submit" name="btn_save_Commonfactors" value="{l i='btn_save' gid='start' type='button'}"></div>
     </div>
 </div>
 </div>
 <div class="clr"></div>
{/if}
{if $tab_id eq 'Excelimport'}
<div class="edit-form edit-form-marketing">
<div class="row header">{l i='calc_admin_import_excel' gid='content'}</div>
<div class="row zebra">
<table style="margin-left:15px;">
	<tr>
    	<td width="100px">{l i='calc_admin_import_file' gid='content'}</td>
        <td>
        	<div class="fileinputs" style="display: inline;">
        	<input type="file" name="db_file" id="db_file" style="cursor: pointer;" class="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" onchange="document.getElementById('file_text_db_file').innerHTML = this.value.replace(/.*\\(.*)/, '$1');document.getElementById('file_img_db_file').innerHTML = ChangeIcon(this.value.replace(/.*\.(.*)/, '$1'));"/>
            <div class="fakefile" style="cursor: pointer">
                <table cellpadding="0" cellspacing="0">
                <tr>
                	<td>	
                        <a class="admin_button_major" style="cursor: pointer"><span>&nbsp;</span>{l i='calc_admin_choose' gid='content'}</a>
                    </td>												
                    <td style="padding-left:10px;">
                        <span id='file_img_db_file'></span>
                    </td>								
                    <td style="padding-left:4px;">
                        <span id='file_text_db_file'></span>	
                    </td>
                </tr>
                </table>
            </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="2">
        	<!--<div class="btn"><div class="l"><a style="cursor:pointer;" >Load</a></div></div>-->
            <div class="btn"><div class="l"><input type="submit" name="btn_load_excel" id="btn_load_excel" value="{l i='calc_admin_load' gid='content'}" /></div></div>
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2"><div id="loading_div" align="justify">{$ExcelCityValidation}</div></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
</table>
</div>
{/if}
</form>
<div class="clr"></div>
{literal}
<script>

/*$('form#save_form #btn_load_excel').click(function(event) { // <- goes here !
event.preventDefault();
    /*if ( parseInt($j("#zip_field").val(), 10) > 1000){
        
        $j('form#userForm .button').attr('onclick','').unbind('click');
        alert('Sorry we leveren alleen inomstreken hijen!');
    }  
});*/
function importExcelData(){	
	$.post(
		'{/literal}{$site_root}admin/content/ajax_importExcelDataPrice{literal}', 
		{},
		function(data){
			$('#import_status').html(data);
		}
	);
}

function ChangeIcon(type){	
    switch (type.toLowerCase())
    {
        case 'bmp':         
        case 'jpg':  
        case 'png':
        case 'gif':
        case 'tiff':
        type = type.toLowerCase();
        break;
        case 'jpeg':
        	type = 'jpg';
        	break;
        case 'tif':
        	type = 'tiff';
        	break;
        case 'mp3':
        case 'wav':
        case 'ogg':
        	type = 'mp3';
         break;
        case 'avi':
        case 'wmv':
        case 'flv':
        	type = 'avi';
        	 break;
       	case 'csv':
        	type = 'csv';
        	 break;
        case 'zip':
        	type = 'zip';
        	 break; 
        default: type = 'other'; break;
    };   
    return "<img src='{/literal}{$site_root}{$img_folder}{literal}" + type +".png'>";
}
function checkCities(fn, result_id) {		
    destination = document.getElementById(result_id);
	//destination.innerHTML = '<img height="16px" style="vertical-align:middle;" src="{/literal}{$site_root}{$img_folder}{literal}indicator.gif">';
	 //var file = value.files[1];
   	//alert(file.tmp_name);
	/*$.ajax({
		url: '{/literal}{$site_root}{literal}admin/content/ajax_get_checkCitylist',
		dataType: 'json',
		type: 'POST',
		data: {datas : '1'},
		cache: false,
		success: function(data){
			if(data){
				alert(data);
			}else{
				alert('');
			}
		}
	});*/
	//alert(fn.files[0].tmp_name)
	
}

</script>
<style>
div.fileinputs {
	position: relative;
}
div.fakefile {
	position: absolute;
	top: 0;
	left: 0;
	z-index: 1;

}
input.file {
	position: relative;
	text-align: left;
	-moz-opacity:0 ;
	filter:alpha(opacity: 0);
	opacity: 0;
	z-index: 2;
	margin-left: -115px;
	margin-top: 10px;
	cursor: pointer;
}
.admin_button_major {
    font: 13px Trebuchet MS, Arial;
	text-decoration: none;
	color: #ffffff !important;
	position: relative;
	background: url('{/literal}{$site_root}{$img_folder}{literal}btn_green.png') 0 -31px no-repeat;
	padding: 7px 17px 7px 15px;
	height: 17px;
	margin: -6px 10px 5px 0;
	/*display:inline-block;*/
}
</style>
{/literal}
{include file="footer.tpl"}

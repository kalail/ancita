{include file="header.tpl"}
<aside class="lc">
	<div class="inside account_menu">
		{helper func_name=get_content_tree helper_name=content func_param=$page.id}
	</div>
	{helper func_name=show_banner_place module=banners func_param='left-banner'}
</aside>

<section class="rc">
    <article class="content-block">
		<h1>{seotag tag='header_text'}</h1>
		{$page.content}
	</article>

	{block name=show_social_networks_like module=social_networking}
	{block name=show_social_networks_share module=social_networking}
	{block name=show_social_networks_comments module=social_networking}
</section>
<div class="clr"></div>
{include file="footer.tpl"}

{include file="header.tpl"}
{js file='jquery-ui.js'}
<div class="content-block">
<div class="divAgentView">
<script>{literal}
$(function(){
	$("#selCity2").children("[value='0']").remove();
});
{/literal}</script>
{literal}
<script>
	function onChangeUser(userId,type)
	{
		if(type == 1){
		window.location.href = '{/literal}{$site_root}{literal}users/view/'+userId;
		}else{
		window.location.href = '{/literal}{$site_root}{literal}agents/'+userId;
		}
	}
	
	function onChangeCity(cityId, type)
	{
		if(cityId != '')
		{
			$.ajax({
				url: '{/literal}{$site_root}{literal}content/ajax_get_extenduser/' + cityId + '/' + type,
				dataType: 'json',
				type: 'GET',
				data: {},
				cache: false,
				success: function(data){
					$('#selCity' + type).css('display','none');
					$('#selUser' + type).css('display','block');
					$("#selUser1").attr('size',30).css('height','100px');
					 $("#selUser2").attr('size',30).css('height','100px');
					$('#selUser' + type).find("option:gt(0)").remove();
					if(type == 1){
					for(var id in data){
						$('#selUser' + type).append('<option value="'+data[id].pg_user_id+'">'+data[id].name+'</option>');
					}
					}else{
						for(var id in data){
						$('#selUser' + type).append('<option value="'+data[id].id_user
						+'">'+data[id].name+'</option>');
					}
					}
				}
			});
		}
		else
		{
			$('#selUser' + type).css('display','none');
		}
	}
</script>
{/literal}
{if $extend.active eq '1' && $vOption ne '1'}
	<div class="av-lt">
    	<table class="tbllt_1">
        	<tr><td>{l i='agent_lbl_company' gid='content'}</td><td><b>{$extend.name}</b></td></tr>
            {if $extend.phone1 ne ''}<tr><td>{l i='agent_lbl_phone' gid='content'}1:</td><td>{$extend.phone1}</td></tr>{/if}
            {if $extend.phone2 ne ''}<tr><td>{l i='agent_lbl_phone' gid='content'}2:</td><td>{$extend.phone2}</td></tr>{/if}
            {if $extend.phone3 ne ''}<tr><td>{l i='agent_lbl_phone' gid='content'}3:</td><td>{$extend.phone3}</td></tr>{/if}
            {if $extend.addresse ne ''}<tr><td>{l i='agent_lbl_address' gid='content'}</td><td>{$extend.addresse}</td></tr>{/if}
            {if $extend.contact1 ne '' || $extend.contact2 ne '' || $extend.contact3 ne ''}
            <tr>
            	<td>{l i='agent_lbl_contacts' gid='content'}</td>
                <td>
                	{if $extend.contact1 ne ''}{$extend.contact1}{/if}
                    {if $extend.contact2 ne ''}<br>{$extend.contact2}{/if}
                    {if $extend.contact3 ne ''}<br>{$extend.contact3}{/if}
                </td>
            </tr>
            {/if}
        </table> 
        <a class="avlnk" href="{$extend.website}" target="_blank">{l i='agent_lbl_link1' gid='content'}</a><br>
        <a class="avlnk" href="{$extend.website}" target="_blank">{l i='agent_lbl_link2' gid='content'}</a>
        {if $extend.pg_user_id ne ''}{block name='user_listings_button' module='listings' user=$user custom='yes' custtext='<span class="avlnk"> View all listings</span>'}{/if}
        <br />
        {if $extend.banner_2 ne ''}
        	<div class="ltbanner">
            	<img src="{$site_root}uploads/photo/{$extend.banner_2}" style="height:300px !important;"/>
        	</div>
        {/if}
        
        <div class="ltmap">
        	 <script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=AIzaSyDyLQinxzTYt0r_WvJSa26Ruj_dVXgtBEY" type="text/javascript"></script>		
            <div id="map_container"></div> 
        </div>
    </div>
    <div class="av-rt">
    	<div class="divEstateAgents">
            <table class="tblEstateAgents">
                <tr>
                    <td>
                        {l i='dyn_estate_agent_box_lbl_1' gid='dynamic_blocks'}<br />
                        <select name="selCity1" id="selCity1" onchange="onChangeCity(this.value,1)">
                            <option value="">{l i='dyn_estate_agent_box_choose_city' gid='dynamic_blocks'}</option>
                            {foreach from=$city1 key="key" item="city"}
                                <option value="{$city.city}">{$city.name}</option>
                            {/foreach}
                        </select>
                        <select name="selUser1" id="selUser1" style="display:none;" onchange="onChangeUser(this.value,1)">
                            <option value="">{l i='dyn_estate_agent_box_choose_agent' gid='dynamic_blocks'}</option>
                        </select>
                    </td>
                    <td>
                        {l i='dyn_estate_agent_box_lbl_2' gid='dynamic_blocks'}<br />
                        <select name="selCity2" id="selCity2" onchange="onChangeCity(this.value,2)">
                            <option value="">{l i='dyn_estate_agent_box_choose_city' gid='dynamic_blocks'}</option>
                            {foreach from=$city2 key="key" item="citys"}
                                <option value="{$citys.city}">{$citys.name}</option>
                            {/foreach}
                        </select>
                         <select name="selUser2" id="selUser2" style="display:none;" onchange="onChangeUser(this.value,2)">
                            <option value="">{l i='dyn_estate_agent_box_choose_agent' gid='dynamic_blocks'}</option>
                        </select>
                    </td>
                    <td>
                        {literal}
                        <script type="text/javascript">
                            $(function() {
                                var getData = function (request, response) {
                                    $.ajax({
                                        url: '{/literal}{$site_root}{literal}content/ajax_get_extendusersearch/' + request.term,
                                        data: {},
                                        dataType: "json",
                                        type: "POST",
                                        success: function (data) {
                                            response($.map(data, function (item) {
                                                return {
                                                    label: item.name,
                                                    val: item.id,
													user: item.usr
                                                }
                                            }))
                                        }
                                    });
                                };
                                
                                $("#agents_available").autocomplete({
                                    source: getData,
                                    select: function(event, ui) {
								if(ui.item.user == null){
								window.location.href = '{/literal}{$site_root}{literal}agents/'+ui.item.val;
								}else{
								window.location.href = '{/literal}{$site_root}{literal}users/view/'+ui.item.user;
								}
							},
							minLength: 1
                                });
                            });
                    </script>
                    {/literal}
                        {l i='dyn_estate_agent_box_searchagent' gid='dynamic_blocks'} <br />
                        <input type="text" class="txtboxs" name="location" id="agents_available" value="" /> 
                    </td>
                </tr>
            </table>
        </div>
        <div class="rtbanner">
        	{if $extend.banner_1 ne ''}<a href="{$extend.website}" target="_blank"><img src="{$site_root}uploads/photo/{$extend.banner_1}" /></a>{/if}
        </div>
        <div style="width:100%; margin:2px;">&nbsp;</div>
        <div class="av-rt_2 skinned-form-controls skinned-form-controls-mac">
        	<table cellpadding="0" cellspacing="0" border="0" style="width:100%;" class="">
            	<tr><td class="headline2">{l i='agent_lbl_services_offered' gid='content'}</td></tr>
                <tr>
                	<td>
                    	<table cellpadding="2" cellspacing="2" border="3" class="infotable">
                            <tr>
                                <th>{l i='agent_lbl_1' gid='content'}</th>
                                <th>{l i='agent_lbl_2' gid='content'}</th>
                                <th>{l i='agent_lbl_3' gid='content'}</th>
                                <th>{l i='agent_lbl_4' gid='content'}</th>
                                <th class="last">{l i='agent_lbl_5' gid='content'}</th>
                            </tr>
                            <tr>
                                <td><input type="radio" {if $extend.real_estate == 1}checked {else} disabled{/if}><span></span></td>
                                <td><input type="radio" {if $extend.rental_service == 1}checked {else} disabled{/if}><span></span></td>
                                <td><input type="radio" {if $extend.facility_service == 1}checked {else} disabled{/if}><span></span></td>
                                <td><input type="radio" {if $extend.insurance_service == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="last"><input type="radio" {if $extend.banking_service == 1}checked {else} disabled{/if}><span></span></td>					
                            </tr>				
                        </table>
                    </td>
                </tr>
                <tr><td class="headline2">{l i='agent_lbl_operates_in' gid='content'}</td></tr>
                <tr>
                	<td>
                    	<table cellpadding="2" cellspacing="2" border="3" class="infotable">
                            <tr>
                                <td class="last" width="100%">{$extend.operations|replace:",":"<br />"}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td class="headline2">{l i='agent_lbl_languages' gid='content'}</td></tr>
                <tr>
                	<td>
                    	<table cellpadding="2" cellspacing="2" border="3" class="infotable">
                            <tr>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/no-no.png" alt="NO" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/sv-se.png" alt="SE" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/fi-fi.png" alt="FI" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/dk-dk.png" alt="DK" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/ru-ru.png" alt="RU" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/pl-pl.png" alt="PL" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/nl-nl.png" alt="NL" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/de-de.png" alt="DE" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/fr-fr.png" alt="FR" /></th>
                                <th class="smallcell"><img src="{$site_root}{$img_folder}/flags/en-gb2.png" alt="EN" /></th>
                                <th class="last smallcell"><img src="{$site_root}{$img_folder}/flags/es-es.png" alt="ES" /></th>					
                            </tr>
                            <tr>
                                <td class="smallcell"><input type="radio" {if $extend.lang_no == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $extend.lang_se == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $extend.lang_fi == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $extend.lang_dk == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $extend.lang_ru == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $extend.lang_pl == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $extend.lang_ne == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $extend.lang_de == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $extend.lang_fr == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="smallcell"><input type="radio" {if $extend.lang_en == 1}checked {else} disabled{/if}><span></span></td>
                                <td class="last smallcell"><input type="radio" {if $extend.lang_es == 1}checked {else} disabled{/if}><span></span></td>					
                            </tr>				
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div class="text">{$extend.agent_text}</div>
        {$extend.agent_text_additional}
    </div>
     <div style="clear:both;"></div>
    <div id="listings_block">
     {$block_listing}
     </div>
     {js module=listings file='listings-list.js'}
     <script>{literal}
		$(function(){
			new listingsList({
				siteUrl: '{/literal}{$site_root}{literal}',
				listAjaxUrl: '{/literal}content/ajax_user_listings/{$extend.pg_user_id}{literal}',
				sectionId: 'user_listing_sections',
				operationType: 'sale',
				order: 'modified',
				orderDirection: 'DESC',
				page: {/literal}{$page}{literal},
				tIds: ['pages_block_1', 'pages_block_2', 'sorter_block'],
			});
		});
		{/literal}</script>
{else}
    <div style="clear:both;"></div>
    <div style="width:100%;">
    	<div class="divEstateAgents">
            <table class="tblEstateAgents">
                <tr>
                    <td>
                        {l i='dyn_estate_agent_box_lbl_1' gid='dynamic_blocks'}<br />
                        <select name="selCity1" id="selCity1" onchange="onChangeCity(this.value,1)">
                            <option value="">{l i='dyn_estate_agent_box_choose_city' gid='dynamic_blocks'}</option>
                            {foreach from=$city1 key="key" item="city"}
                                <option value="{$city.city}">{$city.name}</option>
                            {/foreach}
                        </select>
                        <select name="selUser1" id="selUser1" style="display:none;" onchange="onChangeUser(this.value,1)">
                            <option value="">{l i='dyn_estate_agent_box_choose_agent' gid='dynamic_blocks'}</option>
                        </select>
                    </td>
                    <td>
                        {l i='dyn_estate_agent_box_lbl_2' gid='dynamic_blocks'}<br />
                        <select name="selCity2" id="selCity2" onchange="onChangeCity(this.value,2)">
                            <option value="">{l i='dyn_estate_agent_box_choose_city' gid='dynamic_blocks'}</option>
                            {foreach from=$city2 key="key" item="citys"}
                                <option value="{$citys.city}">{$citys.name}</option>
                            {/foreach}
                        </select>
                         <select name="selUser2" id="selUser2" style="display:none;" onchange="onChangeUser(this.value,2)">
                            <option value="">{l i='dyn_estate_agent_box_choose_agent' gid='dynamic_blocks'}</option>
                        </select>
                    </td>
                    <td>
                        {literal}
                        <script type="text/javascript">
                            $(function() {
                                var getData = function (request, response) {
                                    $.ajax({
                                        url: '{/literal}{$site_root}{literal}content/ajax_get_extendusersearch/' + request.term,
                                        data: {},
                                        dataType: "json",
                                        type: "POST",
                                        success: function (data) {
                                            response($.map(data, function (item) {
                                                return {
                                                    label: item.name,
                                                    val: item.id,
													user: item.usr
                                                }
                                            }))
                                        }
                                    });
                                };
                                
                                $("#agents_available").autocomplete({
                                    source: getData,
                                   select: function(event, ui) {
								if(ui.item.user == null){
								window.location.href = '{/literal}{$site_root}{literal}agents/'+ui.item.val;
								}else{
								window.location.href = '{/literal}{$site_root}{literal}users/view/'+ui.item.user;
								}
							},
							minLength: 1
                                });
                            });
                    </script>
                    {/literal}
                        {l i='dyn_estate_agent_box_searchagent' gid='dynamic_blocks'} <br />
                        <input type="text" class="txtboxs" name="location" id="agents_available" value="" /> 
                    </td>
                </tr>
            </table>
        </div>
        <div class="div_non_info">
        <table class="tbllt_3">
        	<tr><td colspan="2" class="headerInfo"><b>{if $vOption eq '1'}{l i='agent_lbl_title_2' gid='content'}{else}{l i='agent_lbl_title_1' gid='content'}{/if}</b></td></tr>
        	<tr><td>{l i='agent_lbl_company' gid='content'}</td><td><b>{$extend.name}</b></td></tr>
            {if $extend.addresse ne ''}<tr><td>{l i='agent_lbl_address' gid='content'}</td><td>{$extend.addresse}</td></tr>{/if}
            {if $extend.website ne ''}<tr><td>{l i='agent_lbl_website' gid='content'}</td><td>{$extend.website}</td></tr>{/if}
        </table>
        </div>
        {if $vOption eq '1'}<div class="div_non_info_title">{$extend.agent_text}</div>{/if}
        {if $extend.website ne ''}
             <div id="iframewrap_non_member">
            	<iframe id="frame_non_member" src="{$extend.website}" />
            </div>
        {/if}
    </div>
{/if}
</div>
{literal}
<script>
	function onChangeUser(userId,type)
	{
		if(type == 1){
		window.location.href = '{/literal}{$site_root}{literal}users/view/'+userId;
		}else{
		window.location.href = '{/literal}{$site_root}{literal}agents/'+userId;
		}
	}
	
	function onChangeCity(cityId, type)
	{
		if(cityId != '')
		{
			$.ajax({
				url: '{/literal}{$site_root}{literal}content/ajax_get_extenduser/' + cityId + '/' + type,
				dataType: 'json',
				type: 'GET',
				data: {},
				cache: false,
				success: function(data){
					$('#selCity' + type).css('display','none');
					$('#selUser' + type).css('display','block');
					$("#selUser1").attr('size',30).css('height','100px');
					 $("#selUser2").attr('size',30).css('height','100px');
					$('#selUser' + type).find("option:gt(0)").remove();
					if(type == 1){
					for(var id in data){
						$('#selUser' + type).append('<option value="'+data[id].pg_user_id+'">'+data[id].name+'</option>');
					}
					}else{
						for(var id in data){
						$('#selUser' + type).append('<option value="'+data[id].id_user
						+'">'+data[id].name+'</option>');
					}
					}
				}
			});
		}
		else
		{
			$('#selUser' + type).css('display','none');
		}
	}
</script>
<script type="text/javascript">    
	var map = null;
	var geocoder = null;
	var marker = [];
	var map_size_x = 280;
	var map_size_y = 280;		

	try {
		var markerImage = [G_DEFAULT_ICON.image,"http://www.google.com/uds/samples/places/temp_marker.png"];
	} 
	catch(e) {}
	
	var use_smart_zoom = 1 ;        
	var radius = '' ;
	var limit_points = new Array();
	
	if (radius != ''){
		limit_points[0] = new GLatLng('','');
		limit_points[1] = new GLatLng('','');            
	}

	var default_zoom = 5;
	var point_array = new Array();

	function initialize() {
		if (GBrowserIsCompatible()) {
			map = new GMap2(document.getElementById("map_container"), { size: new GSize(map_size_x,map_size_y) });
			var mapControl = new GMapTypeControl();
			map.addControl(mapControl);
			map.addControl(new GSmallMapControl());
			geocoder = new GClientGeocoder();
			default_map_type = G_NORMAL_MAP ;        
			map.setMapType(default_map_type);        
		}
	}

	//index - номер объявления порядковый из отображаемых total_index
	function showAddress(address,id,image,type,cost,date, lat, lon, index, total_index, link) {
		var point_g;
		if (lat!=0){
			try {
				point_g = new GLatLng(lat, lon);
			}
		catch(e) {}
		}else{
			point_g = 0;
			if (geocoder) {
				geocoder.getLatLng(
				address,
					function(point) {
						map.setCenter(point, 15);
						marker[id] = new GMarker(point);
						point_array[index] = point;
						map.addOverlay(marker[id]);
						marker[id].openInfoWindowHtml(address);
					}
				);
			}
			return;
		}

	if (geocoder) {
		geocoder.getLatLng(
			address,
			function(point) {              
				point = point_g;                
				if (!point){                    
				point = new GLatLng(0, 0);
				point_g = 0;                                
			}
			marker[id] = new GMarker(point);
			point_array[index] = point;              
			map.addOverlay(marker[id]);
			GEvent.addListener(marker[id], "mouseover", function() {
				marker[id].setImage(markerImage[1]);
				if(!link){
					link = "";
				}
			marker[id].openInfoWindowHtml('<table width="250"><tr><td rowspan="4"><a href="'+link+'"><img src='+image+' height=70px style="border: 1px solid #cccccc; cursor: pointer;"></a></td><td  align="left"><a href="'+link+'">'+address+'</a></td><tr><td  align="left">'+type+'</td></tr><tr><td  align="left">'+cost+'</td></tr></table>');});     
			GEvent.addListener(marker[id], "mouseout", function() {
			marker[id].setImage(markerImage[0]);});
			
			if (!use_smart_zoom && ((index+1) == total_index)){                  
			map.setCenter(SmartCenterPoint(total_index), default_zoom);
			}              
			if (use_smart_zoom && ((index+1) == total_index)){
			setTimeout("SmartZoom("+total_index+")",1000);                      
			}          

		}
	);
	}
}

function ShowResultMap(){ 
	if(typeof map != "undefined") {
		if (map)
			map.clearOverlays();
			point_array[0] = showAddress("{/literal}{$extend.addresse}{literal}","","","<b>  </b>","Price: ","Available from ", "", "", 0, 1);
			if (radius != ''){
			var polyOptions    = {geodesic: true};
			var polygon = new GPolygon([
			
			], "#f33f00", 1, 0.8, "#ff0000", 0.15, polyOptions);
			
			map.addOverlay(polygon);
			
			limit_points[0] = new GLatLng('','');
			limit_points[1] = new GLatLng('','');
		}
	}        
}

function GetMinMax(size){        
	var min_lat = point_array[0].lat();        
	var max_lat = min_lat;
	var min_lng = point_array[0].lng();        
	var max_lng = min_lng;
	var lat;
	var lng;        
	for (var i=1; i < size; i++){            
		lat = point_array[i].lat();
		lng = point_array[i].lng();
		if (lat < min_lat) min_lat = lat;
		if (lat > max_lat) max_lat = lat;
		if (lng < min_lng) min_lng = lng;
		if (lng > max_lng) max_lng = lng;
	}
	if (radius != ''){
		for (var i=0; i < 2; i++){            
			lat = limit_points[i].lat();
			lng = limit_points[i].lng();
			if (lat < min_lat) min_lat = lat;
			if (lat > max_lat) max_lat = lat;
			if (lng < min_lng) min_lng = lng;
			if (lng > max_lng) max_lng = lng;
		}
	}
	return {min_lat:min_lat, min_lng:min_lng, max_lat:max_lat, max_lng:max_lng}
}

function SmartCenterPoint(size){
	if (size == 1){
		map.setCenter(point_array[0], default_zoom);
		return;
	}        
	if ((point_array[0]=='undefined') || (point_array[0]=='null')){
		return;
	}    
	var rectangle = GetMinMax(size);                
	var center_lat = (rectangle.max_lat + rectangle.min_lat)/2;        
	var center_lng = (rectangle.max_lng + rectangle.min_lng)/2;
	var center_point = new GLatLng(center_lat,center_lng);        
	return center_point;
}

function GetSmartZoom(size){        
	var rectangle = GetMinMax(size);
	var ne = new google.maps.LatLng(rectangle.max_lat,
		rectangle.max_lng);                                    
	var sw = new google.maps.LatLng(rectangle.min_lat,
			rectangle.min_lng);                                                      
	var bounds = new google.maps.LatLngBounds(sw, ne);
	var zoom = map.getBoundsZoomLevel(bounds, new google.maps.Size(280, 400));        
	return zoom;
}


function SmartZoom(size){            
	var smart_center_point = SmartCenterPoint(size);        
	var smart_zoom = GetSmartZoom(size);    
	map.setCenter(smart_center_point, smart_zoom);        
}

initialize();    
ShowResultMap();    
    
    </script>  
{/literal}
</div>	
	
{include file="footer.tpl"}
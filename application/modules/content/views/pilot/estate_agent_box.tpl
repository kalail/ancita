{js file='jquery-ui.js'}
<div class="divEstateAgents">
	<table class="tblEstateAgents"> 
    	<tr>
        	<td>
            	<div class="divTitle">
            		<div class="locCalcimg"><p class="agentTitle">{l i='dyn_estate_agent_box_title' gid='dynamic_blocks'}</p></div>
                </div>
            </td>
        </tr>
    	<tr>
        	<td class="tdlf">
            	{l i='dyn_estate_agent_box_lbl_1' gid='dynamic_blocks'}<br />
                <select name="selCity1" id="selCity1" onchange="onChangeCity(this.value,1)">
                	<option value="">{l i='dyn_estate_agent_box_choose_city' gid='dynamic_blocks'}</option>
                    {foreach from=$city1 key="key" item="city"}
                        <option value="{$city.city}">{$city.name}</option>
                    {/foreach}
                </select>
                <select name="selUser1" id="selUser1" style="display:none;" onchange="onChangeUser(this.value,1)">
                	<option value="">{l i='dyn_estate_agent_box_choose_agent' gid='dynamic_blocks'}</option>
                </select>
            </td>
        </tr>
        <tr>
        	<td class="tdlf">
            	{l i='dyn_estate_agent_box_lbl_2' gid='dynamic_blocks'}<br />
                <select name="selCity2" id="selCity2" onchange="onChangeCity(this.value,2)">
                	<option value="">{l i='dyn_estate_agent_box_choose_city' gid='dynamic_blocks'}</option>
                    {foreach from=$city2 key="key" item="citys"}
                        <option value="{$citys.city}">{$citys.name}</option>
                    {/foreach}
                </select>
                 <select name="selUser2" id="selUser2" style="display:none;" onchange="onChangeUser(this.value,2)">
                	<option value="">{l i='dyn_estate_agent_box_choose_agent' gid='dynamic_blocks'}</option>
                </select>
            </td>
        </tr>
        <tr>
        	<td class="tdlf">
            	{literal}
            	<script type="text/javascript">
					$(function() {
						var getData = function (request, response) {
							$.ajax({
								url: '{/literal}{$site_root}{literal}content/ajax_get_extendusersearch/' + request.term,
								data: {},
								dataType: "json",
								type: "POST",
								success: function (data) {
									response($.map(data, function (item) {
										return {
											label: item.name,
											val: item.id,
											user: item.usr
										}
									}))
								}
							});
						};
						
						$("#agents_available").autocomplete({
							source: getData,
							select: function(event, ui) {
								if(ui.item.user == null){
								window.location.href = '{/literal}{$site_root}{literal}agents/'+ui.item.val;
								}else{
								window.location.href = '{/literal}{$site_root}{literal}users/view/'+ui.item.user;
								}
							},
							minLength: 1
						});
					});
            </script>
            {/literal}
            	{l i='dyn_estate_agent_box_searchagent' gid='dynamic_blocks'} <br />
                <input type="text" class="txtboxs" name="location" id="agents_available" value="" /> 
            </td>
        </tr>
    </table>
</div>
{literal}
<script>
	function onChangeUser(userId,type)
	{
		if(type == 1){
		window.location.href = '{/literal}{$site_root}{literal}users/view/'+userId;
		}else{
		window.location.href = '{/literal}{$site_root}{literal}agents/'+userId;
		}
	}
	
	function onChangeCity(cityId, type)
	{
		if(cityId != '')
		{
			$.ajax({
				url: '{/literal}{$site_root}{literal}content/ajax_get_extenduser/' + cityId + '/' + type,
				dataType: 'json',
				type: 'GET',
				data: {},
				cache: false,
				success: function(data){
					$('#selCity' + type).css('display','none');
					$('#selUser' + type).css('display','block');
					 $("#selUser1").attr('size',30).css('height','100px');
					 $("#selUser2").attr('size',30).css('height','100px');
					 
					$('#selUser' + type).find("option:gt(0)").remove();
					if(type == 1){
					for(var id in data){
						$('#selUser' + type).append('<option value="'+data[id].pg_user_id+'">'+data[id].name+'</option>');
					}
					}else{
						for(var id in data){
						$('#selUser' + type).append('<option value="'+data[id].id_user
						+'">'+data[id].name+'</option>');
					}
					}
				}
			});
		}
		else
		{
			$('#selUser' + type).css('display','none');
		}
	}
</script>
{/literal}
<script>{literal}
$(function(){
	$("#selCity2").children("[value='0']").remove();
});
{/literal}</script>

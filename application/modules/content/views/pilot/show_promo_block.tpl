<!--
    Task    : CR-2015-01 -2.3 - Make Monthly offer as it is in existing Ancita version
	Date    : 06-Nov-2015
Modified By : Mahi-->

{if ($promo.content_type eq 't' && ($promo.promo_image || $promo.promo_text) ) || ($promo.content_type eq 'f' && $promo.promo_flash)}
{if $promo.block_width > 980 && $promo.block_width_unit eq 'px' && $promo.block_height eq 440 && $promo.block_height_unit eq 'px'}
<div class="promo-block-wrapper">
<div class="promo-block-wrapper2">
<div class="promo-block-wrapper3">
<div class="promo-block-wrapper4">
{/if}


{js file='jquery.bxslider'}

<script>
	{literal}
  	$(document).ready(function () {
            var slider = $('#slider1').bxSlider();
            modifyDelay(0);
            $(".feature1").show();
            function modifyDelay(startSlide) {
                var duration = $('#slider1 li:nth-child(' + (startSlide + 2) + ')').attr('duration')*1000;
                if (duration == "") {
                    duration = 10000;
                } else {
                    duration = parseInt(duration);
                }

                slider.reloadSlider({
                    mode: 'horizontal',
                    auto: true,
                    pause: duration,
                    autoControls: true,
                    speed: 1000,
                    startSlide: startSlide,
                    onSlideAfter: function ($el, oldIndex, newIndex) {
                        modifyDelay(newIndex);
                    }
                });
            }

        });
		
		
	{/literal}
</script>



<div class="promo-block" style="width: {$promo.block_width}{$promo.block_width_unit}; height: {$promo.block_height}{$promo.block_height_unit};">

 	<div class="slider"  style="width: {$promo.block_width}{$promo.block_width_unit}; height: {$promo.block_height}{$promo.block_height_unit};">
    
        <ul id="slider1" style="width: {$promo.block_width}{$promo.block_width_unit}; height: {$promo.block_height}{$promo.block_height_unit};margin: 0px; padding: 0px;">
        
        	{foreach item=item from=$promo_data_slide}

       		<li duration="{if $item.promo_each_image_seconds eq '0'}10000{else}{$item.promo_each_image_seconds}{/if}">
            <a href="{$item.promo_url}">
             	<img src="{$item.Promo_slide.file_url}" width="{$promo.block_width}{$promo.block_width_unit}" height="{$promo.block_height}{$promo.block_height_unit}" />
            </a>
              {if $item.feature1 eq 1}
              {if $promo.content_type eq 't'}
              {if $promo.block_width > 980 && $promo.block_width_unit eq 'px' && $promo.block_height eq 440 && $promo.block_height_unit eq 'px'}
             <div class="inside-wrapper">
              {/if}
              <div class="inside feature{$item.feature1}" style="display:none;">
              <div class="panel" style="color:{$promo.promo_text_color} !important">{$promo.promo_text}</div>
              <div class="background"></div>
              </div>
             {if $promo.block_width > 980 && $promo.block_width_unit eq 'px' && $promo.block_height eq 440 && $promo.block_height_unit eq 'px'}
              </div>
              {/if}
              {/if}
              {/if}
        	</li>
           {/foreach}

        </ul>
    </div>

{if $promo.content_type eq 'f'}
<object width="100%" height="100%" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">
<param value="Always" name="allowScriptAccess">
<param value="{$promo.media.promo_flash.file_url}" name="movie">
<param value="false" name="menu">
<param value="high" name="quality">
<param value="opaque" name="wmode">
<param value="" name="flashvars">
<embed width="100%" height="100%" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" swliveconnect="FALSE" menu="false" wmode="opaque" allowscriptaccess="Always" quality="high" flashvars="" src="{$promo.media.promo_flash.file_url}"> 
</object>
{/if}
</div>

{if $promo.block_width > 980 && $promo.block_width_unit eq 'px' && $promo.block_height eq 440 && $promo.block_height_unit eq 'px'}
<div class="gradient_wrapper">
	<div class="gradient_wrapper2">
		<div class="gradient">
			<div class="gradient-l"></div>
			<div class="gradient-r"></div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
{/if}
{/if}

/*  Task 			: CR-2015-03-2.1.1
	Modified Date	: 11-Nov-2015 */
function countryInput(optionArr){
	this.properties = {
		siteUrl: '',
		rand: '',
		id_country: '',
		id_region: '',
		id_city: '',
		id_district: '',
		load_country_link: 'countries/ajax_get_countries',
		load_region_link: 'countries/ajax_get_regions/',
		load_city_link: 'countries/ajax_get_cities/',
		load_district_link: 'countries/ajax_get_districts/',
		load_location_link: 'countries/ajax_get_locations/',
		load_form: 'countries/ajax_get_form/',
		load_data: 'countries/ajax_get_data/',
		select_type: 'city',
		id_main: '',
		id_text: '',
		id_open: '',
		id_hidden_country: '',
		id_hidden_region: '',
		id_hidden_city: '',
		id_hidden_district: '',
		id_bg: '',
		id_select: '',
		id_items: 'country_select_items',
		id_back: 'country_select_back',
		id_clear: 'country_select_clear',
		id_close: 'country_select_close',
		id_search: 'city_search',
		id_district_search: 'district_search',
		id_city_page: 'city_page',
		id_district_page: 'district_page',
		timeout_obj: null,
		timeout: 500,
		
		dropdownClass: 'dropdown',
		contentObj: new loadingContent({
			loadBlockWidth: '680px', closeBtnClass: 'load_content_controller_close', closeBtnPadding: 15
		})
	}
	var _self = this;
	var show_locations = false;

	this.errors = {
	}

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.properties.id_main = 'country_select_'+_self.properties.rand;
		_self.properties.id_text = 'country_text_'+_self.properties.rand;
		_self.properties.id_open = 'country_open_'+_self.properties.rand;
		_self.properties.id_hidden_country = 'country_hidden_'+_self.properties.rand;
		_self.properties.id_hidden_region = 'region_hidden_'+_self.properties.rand;
		_self.properties.id_hidden_city = 'city_hidden_'+_self.properties.rand;
		_self.properties.id_hidden_district = 'district_hidden_'+_self.properties.rand;
		_self.properties.id_bg = 'country_input_bg_'+_self.properties.rand;
		_self.properties.id_select = 'region_select_'+_self.properties.rand;

		$('#'+_self.properties.id_open).bind('click', function(){
			switch(_self.properties.select_type){
				case 'district':
					if(_self.properties.id_city){
						_self.open_form('district', _self.properties.id_city, true);
					}else if(_self.properties.id_region){
						_self.open_form('city', _self.properties.id_region);
					}else if(_self.properties.id_country){
						_self.open_form('region', _self.properties.id_country);
					}else{
						_self.open_form('country');
					}
				break;
				case 'city':
					if(_self.properties.id_region){
						_self.open_form('city', _self.properties.id_region);
					}else if(_self.properties.id_country){
						_self.open_form('region', _self.properties.id_country);
					}else{
						_self.open_form('country');
					}
				break;
				case 'region':
					if(_self.properties.id_country){
						_self.open_form('region', _self.properties.id_country);
					}else{
						_self.open_form('country');
					}
				break;
				case 'country':
					_self.open_form('country');
				break;
			}
			return false;
		});
		
		$('#'+_self.properties.id_text).bind('keyup', function(){
			if(_self.properties.timeout_obj){
				clearTimeout(_self.properties.timeout_obj);
			}
			_self.properties.timeout_obj = setTimeout(function(){
				var name = $('#'+_self.properties.id_text).val();
				_self.emptyValues();
				if(name){
					_self.load_locations(name);
				}else{
					_self.closeBox();
				}
			}, _self.properties.timeout)
			return true;
		});
		
		_self.initBg();
		_self.initBox();
	}

	this.open_form = function(type, variable, init){
		var url =  _self.properties.siteUrl+_self.properties.load_form+type;
		if(variable){
			url += '/'+variable;
		}
		$.ajax({
			url: url,
			cache: false,
			success: function(data){
				_self.properties.contentObj.show_load_block(data);
				$('#'+_self.properties.id_clear).unbind().bind('click', function(){
					_self.clearBox();
				});
				switch(type){
					case 'country':
						$('#'+_self.properties.id_back).unbind().bind('click', function(){
							_self.set_values();
							$(this).blur();
							return false;
						});
						_self.load_countries();
					break;
					case 'region':
						_self.load_regions(variable);
						$('#'+_self.properties.id_back).unbind().bind('click', function(){
							_self.open_form('country', 0);
							return false;
						});
					break;
					case 'city':
						_self.load_cities(variable, '', 1);
						$('#'+_self.properties.id_back).unbind().bind('click', function(){
							_self.open_form('region', _self.properties.id_country);
							return false;
						});
						$('#'+_self.properties.id_search).unbind().bind('keyup', function(){
							_self.load_cities(variable, $(this).val(), 1);
						});
					break;
					case 'district':
						_self.open_form('city', _self.properties.id_region);
						return false;
						/*_self.load_districts(variable, '', 1, init || false);
						$('#'+_self.properties.id_back).unbind().bind('click', function(){
							_self.open_form('city', _self.properties.id_region);
							return false;
						});
						$('#'+_self.properties.id_district_search).unbind().bind('keyup', function(){
							_self.load_districts(variable, $(this).val(), 1);
						});*/
					break;
				}
				$('#' + _self.properties.id_close).bind('click', function() {
					_self.properties.contentObj.hide_load_block();
				});
			}
		});
	}

	this.load_countries = function(){
		$.ajax({
			url: _self.properties.siteUrl+_self.properties.load_country_link,
			dataType: 'json',
			cache: false,
			success: function(data){
				$('#'+_self.properties.id_items).unbind();
				$('#'+_self.properties.id_items).empty();
				for(var id in data.items ){
					$('#'+_self.properties.id_items).append('<li index="'+data.items[id].code+'">'+data.items[id].name+'</li>');
				}
				$('#'+_self.properties.id_items+' li').bind('click', function(){
					
					_self.set_values('country', $(this).attr('index'), $(this).text(), data);
					if(_self.properties.select_type == 'country'){
						_self.properties.contentObj.hide_load_block();
					}else{
						_self.open_form('region', $(this).attr('index'));
					}
				});
			}
		});
	}

	this.load_regions = function(id_country){
		$.ajax({
			url: _self.properties.siteUrl+_self.properties.load_region_link + id_country,
			dataType: 'json',
			cache: false,
			success: function(data){
				$('#'+_self.properties.id_items).unbind();
				$('#'+_self.properties.id_items).empty();
				for(var id in data.items ){
					$('#'+_self.properties.id_items).append('<li index="'+data.items[id].id+'">'+data.items[id].name+'</li>');
				}
				$('#'+_self.properties.id_items+' li').bind('click', function(){
					_self.set_values('region', $(this).attr('index'), $(this).text(), data);
					if(_self.properties.select_type == 'region'){
						_self.properties.contentObj.hide_load_block();
					}else{
						_self.open_form('city', $(this).attr('index'));
					}
				});
			}
		});
	}

	this.load_cities = function(id_region, search, page){
		if(search != ''){
			var ajax_type = 'POST';
			var send_data = {
				search: search
			};
		}else{
			var ajax_type = 'GET';
			var send_data = {};
		}

		$.ajax({
			url: _self.properties.siteUrl+_self.properties.load_city_link + id_region + '/' + page,
			dataType: 'json',
			type: ajax_type,
			data: send_data,
			cache: false,
			success: function(data){
				$('#'+_self.properties.id_items).unbind();
				$('#'+_self.properties.id_items).empty();
			
				for(var id in data.items){
					$('#'+_self.properties.id_items).append('<li index="'+data.items[id].id+'"><input type="checkbox" name="selCity[]" value="'+data.items[id].id+'_'+data.items[id].name+'" />'+data.items[id].name+'</li>');
				}
				

				_self.generate_city_pages(data.pages, data.current_page, search);
				$('#'+_self.properties.id_items+' li').bind('click', function(){
					//_self.set_values('city', $(this).attr('index'), $(this).text(), data);
					var arrCityvalues = new Array();
					$.each($("input[name='selCity[]']:checked"), function() {
					  arrCityvalues.push($(this).val());
					});
					
					var strCity = "";
					var strCityId = "";
					for (var c = 0; c < arrCityvalues.length; c++)
					{
						if(c > 0)
						{
							strCity += ',';
							strCityId += ',';
						}
						
						var arrSplitCity = 	arrCityvalues[c].split('_');
						strCity += arrSplitCity[1];
						strCityId += arrSplitCity[0];
					}
					
					_self.set_values('city', strCityId, strCity, data);
					if(_self.properties.select_type == 'city'){
						_self.properties.contentObj.hide_load_block();
					}else{
						//_self.open_form('district', $(this).attr('index'));
					}
				});
			}
		});
	}
	
	this.load_districts = function(id_city, search, page, init){
		if(search != ''){
			var ajax_type = 'POST';
			var send_data = {
				search: search
			};
		}else{
			var ajax_type = 'GET';
			var send_data = {};
		}

		$.ajax({
			url: _self.properties.siteUrl+_self.properties.load_district_link + id_city + '/' + page,
			dataType: 'json',
			type: ajax_type,
			data: send_data,
			cache: false,
			success: function(data){
				$('#'+_self.properties.id_items).unbind();
				$('#'+_self.properties.id_items).empty();
				
				if(!data.items.length && search == '' && page == 1){
					if(init){
						_self.open_form('city', _self.properties.id_region);
					}else{
						_self.properties.contentObj.hide_load_block();
					}
				}	
				
				for(var id in data.items){
					$('#'+_self.properties.id_items).append('<li index="'+data.items[id].id+'">'+data.items[id].name+'</li>');
				}

				_self.generate_district_pages(data.pages, data.current_page, search);
				$('#'+_self.properties.id_items+' li').bind('click', function(){
					_self.set_values('district', $(this).attr('index'), $(this).text(), data);
					_self.properties.contentObj.hide_load_block();
				});
			}
		});
	}

	this.load_locations = function(name){
		$.ajax({
			url: _self.properties.siteUrl+_self.properties.load_location_link + encodeURIComponent(name),
			dataType: 'json',
			type: 'GET',
			cache: false,
			success: function(data){
				if (data.all > 0) {
					$('#'+_self.properties.id_select+' ul').empty();
					for(var id in data.items.districts ){
						$('#'+_self.properties.id_select+' ul').append('<li gid="rs_'+id+'" district="'+data.items.districts[id].id+'" city="'+data.items.districts[id].id_city+'" region="'+data.items.districts[id].id_region+'" country="'+data.items.districts[id].country_code+'">'+data.items.districts[id].name+'</li>');
					}
					for(var id in data.items.cities ){
						$('#'+_self.properties.id_select+' ul').append('<li gid="rs_'+id+'" city="'+data.items.cities[id].id+'" region="'+data.items.cities[id].id_region+'" country="'+data.items.cities[id].country_code+'">'+data.items.cities[id].name+'</li>');
					}
					for(var id in data.items.regions ){
						$('#'+_self.properties.id_select+' ul').append('<li gid="rs_'+id+'" city="" region="'+data.items.regions[id].id+'" country="'+data.items.regions[id].country_code+'">'+data.items.regions[id].name+'</li>');
					}
					for(var id in data.items.countries ){
						$('#'+_self.properties.id_select+' ul').append('<li gid="rs_'+id+'" city="" region="" country="'+data.items.countries[id].code+'">'+data.items.countries[id].name+'</li>');
					}
					_self.openBox();
				} else {
					_self.closeBox();
				}
			}
		});
	}

	this.set_values = function(type, variable, value, data){
		var string_value = "";
		switch(type){
			case 'country':
				$('#'+_self.properties.id_hidden_country).val(variable.toString()).change();
				_self.properties.id_country = variable.toString();

				$('#'+_self.properties.id_hidden_region).val(0).change();
				_self.properties.id_region = 0;

				$('#'+_self.properties.id_hidden_city).val(0).change();
				_self.properties.id_city = 0;
				
				$('#'+_self.properties.id_hidden_district).val(0).change();
				_self.properties.id_district = 0;

				string_value = value;
			break;
			case 'region':
				$('#'+_self.properties.id_hidden_region).val(variable).change();
				_self.properties.id_region = variable;

				$('#'+_self.properties.id_hidden_city).val(0).change();
				_self.properties.id_city = 0;

				$('#'+_self.properties.id_hidden_district).val(0).change();
				_self.properties.id_district = 0;

				//string_value = data.country.name+', '+value;
				string_value = value;
			break;
			case 'city':
				$('#'+_self.properties.id_hidden_city).val(variable).change();
				_self.properties.id_city = variable;

				$('#'+_self.properties.id_hidden_district).val(0).change();
				_self.properties.id_district = 0;

				//string_value = data.country.name+', '+data.region.name+', '+value;
				string_value = data.region.name+', '+value;
			break;
			case 'district':
				$('#'+_self.properties.id_hidden_district).val(variable).change();
				_self.properties.id_district = variable;

				//string_value = data.country.name + ', ' + data.region.name + ', ' + data.city.name + ', ' + value;
				string_value =  data.region.name + ', ' + data.city.name + ', ' + value;
			break;
			default:
				$('#'+_self.properties.id_hidden_country).val(0).attr('data-name', '').change();
				_self.properties.id_country = 0;

				$('#'+_self.properties.id_hidden_region).val(0).attr('data-name', '').change();
				_self.properties.id_region = 0;

				$('#'+_self.properties.id_hidden_city).val(0).attr('data-name', '').change();
				_self.properties.id_city = 0;
			
				$('#'+_self.properties.id_hidden_district).val(0).attr('data-name', '').change();
				_self.properties.id_district = 0;
			break;
		}

		//if(string_value == '') string_value = '...';
		$('#'+_self.properties.id_text).val(string_value);
	}
	
	this.set_values_text = function(country, region, city, district, value){
		$('#'+_self.properties.id_text).val(value);
		_self.properties.id_country = country;
		_self.properties.id_region = region;
		_self.properties.id_city = city;
		_self.properties.id_district = district;
		$('#'+_self.properties.id_hidden_country).val(_self.properties.id_country).change();
		$('#'+_self.properties.id_hidden_region).val(_self.properties.id_region).change();
		$('#'+_self.properties.id_hidden_city).val(_self.properties.id_city).change();
		$('#'+_self.properties.id_hidden_district).val(_self.properties.id_district).change();
		_self.closeBox();
	}

	this.emptyValues = function(){
		_self.properties.id_country = 'ES';
		_self.properties.id_region = '';
		_self.properties.id_city = '';
		_self.properties.id_district = '';
		$('#'+_self.properties.id_hidden_country).val(_self.properties.id_country).change();
		$('#'+_self.properties.id_hidden_region).val(_self.properties.id_region).change();
		$('#'+_self.properties.id_hidden_city).val(_self.properties.id_city).change();
		$('#'+_self.properties.id_hidden_district).val(_self.properties.id_district).change();
	}
	
	this.set_values_external = function(type, variable){
		$.ajax({
			url: _self.properties.siteUrl+_self.properties.load_data + type + '/' + variable,
			dataType: 'json',
			cache: false,
			success: function(data){
				switch(type){
					case 'country':
						_self.set_values(type, variable, data.country.name, data);
					break;
					case 'region':
						_self.set_values(type, variable, data.region.name, data);
					break;
					case 'city':
						_self.set_values(type, variable, data.city.name, data);
					break;
					case 'district':
						_self.set_values(type, variable, data.district.name, data);
					break;
				}
			}
		});

	}

	this.generate_city_pages = function(pages, current_page, search){
		$('#'+_self.properties.id_city_page+' a').unbind();
		$('#'+_self.properties.id_city_page).empty();
		if(pages > 1){
			for(var i=1; i<=pages; i++){
				if(i == current_page){
					$('#'+_self.properties.id_city_page).append('<strong>'+i+'</strong>');
				}else{
					$('#'+_self.properties.id_city_page).append('<a href="#">'+i+'</a>');
				}
			}
			$('#'+_self.properties.id_city_page+' a').bind('click', function(){
				_self.load_cities(_self.properties.id_region, search, $(this).text());
				return false;
			});
		}
	}
	
	this.generate_district_pages = function(pages, current_page, search){
		$('#'+_self.properties.id_district_page+' a').unbind();
		$('#'+_self.properties.id_district_page).empty();
		if(pages > 1){
			for(var i=1; i<=pages; i++){
				if(i == current_page){
					$('#'+_self.properties.id_district_page).append('<strong>'+i+'</strong>');
				}else{
					$('#'+_self.properties.id_district_page).append('<a href="#">'+i+'</a>');
				}
			}
			$('#'+_self.properties.id_district_page+' a').bind('click', function(){
				_self.load_districts(_self.properties.id_city, search, $(this).text());
				return false;
			});
		}
	}

	this.initBg = function(){
		$('body').append('<div id="'+_self.properties.id_bg+'"></div>');
		$('#'+_self.properties.id_bg).css({
			'display': 'none',
			'position': 'fixed',
			'z-index': '98999',
			'width': '1px',
			'height': '1px',
			'left': '1px',
			'top': '1px'
		});
	}
	
	this.expandBg = function(){

		$('#'+_self.properties.id_bg).css({
			'width': $(window).width()+'px',
			'height': $(window).height()+'px',
			'display': 'block'
		}).bind('click', function(){
			_self.closeBox();
		});
		
	}
	
	this.collapseBg = function(){
		$('#'+_self.properties.id_bg).css({
			'width': '1px',
			'height': '1px',
			'display': 'none'
		}).unbind();
	}
	
	this.initBox = function(){
		_self.createDropDown();

		$('#'+_self.properties.id_select).off('click', 'li').on('click', 'li', function(){
			_self.set_values_text($(this).attr('country'), $(this).attr('region'), $(this).attr('city'), $(this).attr('district'), $(this).text());
		});
	}
	
	this.unsetBox = function(){
		$('#'+_self.properties.id_select).unbind().remove();
	}
	
	this.openBox = function(){
		_self.expandBg();
		_self.resetDropDown();
		$('#'+_self.properties.id_select).slideDown();
	}
	
	this.createDropDown = function(){
		$('body').append('<div class="'+_self.properties.dropdownClass+'" id="'+_self.properties.id_select+'"><ul></ul></div>');
		_self.resetDropDown();
	}
	
	this.resetDropDown = function(){
		var top = $('#'+_self.properties.id_text).offset().top + $('#'+_self.properties.id_text).outerHeight();

		$('#'+_self.properties.id_select).css({
			width: $('#'+_self.properties.id_text).width()+'px',
			left: $('#'+_self.properties.id_text).offset().left+'px',
			top: top +'px'
		});
	}
	
	this.closeBox = function(){
		_self.collapseBg();
		$('#'+_self.properties.id_select).slideUp();
	}
	
	this.clearBox = function(){
		_self.set_values_text('', 0, 0, 0, '');
		//_self.closeBox();
		_self.properties.contentObj.hide_load_block();
	}

	_self.Init(optionArr);
}

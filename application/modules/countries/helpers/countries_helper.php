<?php  
/*  Task 			: CR-2015-03-2.1.1
	Modified Date	: 11-Nov-2015 */
	
/**
 * Countries management
 * 
 * @package PG_RealEstate
 * @subpackage Countries
 * @category	helpers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Mikhail Makeev <mmakeev@pilotgroup.net>
 * @version $Revision: 68 $ $Date: 2010-01-11 16:02:23 +0300 (Пн, 11 янв 2010) $ $Author: irina $
 **/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('country_select'))
{
	function country_select($select_type='city', $id_country='', $id_region='', $id_city='', $var_country_name='id_country', $var_region_name='id_region', $var_city_name='id_city', $var_js_name='', $id_district='', $var_district_name=''){
		$CI = & get_instance();
		$CI->load->model("Countries_model");

		if(!empty($id_country)){
			$data["country"] = $CI->Countries_model->get_country($id_country);
		}
		if(!empty($id_region)){
			$data["region"] = $CI->Countries_model->get_region($id_region);
		}
		if(!empty($id_city)){
			$data["city"] = $CI->Countries_model->get_city($id_city);
		}
		if(!empty($id_district)){
			$data["district"] = $CI->Countries_model->get_district($id_district);
		}

		$data["var_country_name"] = $var_country_name?$var_country_name:"id_country";
		$data["var_region_name"] = $var_region_name?$var_region_name:"id_region";
		$data["var_city_name"] = $var_city_name?$var_city_name:"id_city";
		$data["var_district_name"] = $var_district_name?$var_district_name:"id_district";
		$data["select_type"] = $select_type?$select_type:"city";
		$data["var_js_name"] = $var_js_name;
		
		if($data['select_type'] == 'city'){
			$use_district = $CI->pg_module->get_module_config('countries', 'use_district');
			if($use_district){
				$data['select_type'] = 'district';
			}
		}
		
		$data["rand"] = rand(100000, 999999);

		$CI->template_lite->assign('country_helper_data', $data);
		return $CI->template_lite->fetch('helper_country_select', 'user', 'countries');
	}
}

if ( ! function_exists('country_input'))
{
	function country_input($select_type='city', $id_country='', $id_region='', $id_city='', $var_country_name='id_country', $var_region_name='id_region', $var_city_name='id_city', $var_js_name='', $placeholder = '', $id_district='', $var_district_name=''){
		$CI = & get_instance();
		$CI->load->model("Countries_model");

		/*if(!empty($id_country)){
			$data["country"] = $CI->Countries_model->get_country($id_country);
		}
		if(!empty($id_region)){
			$data["region"] = $CI->Countries_model->get_region($id_region);
		}
		if(!empty($id_city)){
			$data["city"] = $CI->Countries_model->get_city($id_city);
		}
		if(!empty($id_district)){
			$data["district"] = $CI->Countries_model->get_district($id_district);
		}*/
		
		$data["country"] = $CI->Countries_model->get_country('ES');

		$data["var_country_name"] = $var_country_name?$var_country_name:"id_country";
		$data["var_region_name"] = $var_region_name?$var_region_name:"id_region";
		$data["var_city_name"] = $var_city_name?$var_city_name:"id_city";
		$data["var_district_name"] = $var_district_name?$var_district_name:"id_district";
		$data["select_type"] = $select_type?$select_type:"city";
		$data["var_js_name"] = $var_js_name;
		$data["placeholder"] = $placeholder;
		
		/*if ($data["district"]["name"]) {
			$data["location_text"] = $data["district"]["name"];
		} elseif ($data["city"]["name"]) {
			$data["location_text"] = $data["city"]["name"];
		} elseif ($data["region"]["name"]) {
			$data["location_text"] = $data["region"]["name"];
		} elseif ($data["country"]["name"]) {
			$data["location_text"] = $data["country"]["name"];
		}*/
		
		if($data['select_type'] == 'city'){
			$use_district = $CI->pg_module->get_module_config('countries', 'use_district');
			if($use_district){
				$data['select_type'] = 'district';
			}
		}

		$data["rand"] = rand(100000, 999999);

		$CI->template_lite->assign('country_helper_data', $data);
		return $CI->template_lite->fetch('helper_country_input', 'user', 'countries');
	}
}

if ( ! function_exists('country'))
{
	function country($id_country='', $id_region='', $id_city='', $id_district=''){
		$CI = & get_instance();
		$CI->load->model("Countries_model");
		if(!empty($id_country)){
			$data["country"] = $CI->Countries_model->get_country($id_country);
			$return_array[] = $data["country"]["name"];
		}
		if(!empty($id_region)){
			$data["region"] = $CI->Countries_model->get_region($id_region);
			$return_array[] = $data["region"]["name"];
		}
		if(!empty($id_city)){
			$data["city"] = $CI->Countries_model->get_city($id_city);
			$return_array[] = $data["city"]["name"];
		}
		if(!empty($id_district)){
			$data["district"] = $CI->Countries_model->get_district($id_district);
			$return_array[] = $data["district"]["name"];
		}
		$return = (is_array($return_array))?implode(', ', $return_array):'';
		return $return;
	}
}

/*
 * Data is array( id => array(id_country, id_region, id_city), id => array(id_country, id_region, id_city), .....)
 * return array(id => str, id => str, id => str...);
 */
if ( ! function_exists('countries_output_format')){
	function countries_output_format($data){
		if(empty($data)){
			return array();
		}
		$CI = & get_instance();
		$location_data = get_location_data($data, 'country');
		$country_template = $CI->pg_module->get_module_config('countries', 'output_country_format');
		
		$return = array();
		foreach($data as $id => $location){
			if(isset($location['country']) && isset($location_data["country"][$location['country']])){
				$str = str_replace("[country]", $location_data["country"][$location['country']]["name"], $country_template);
				$str = str_replace('[country_code]', $location['country'], $str);
			}else{
				$str = "";
			}
			$return[$id] = $str;
		} 
		return $return;
	}
}	

if ( ! function_exists('regions_output_format')){
	function regions_output_format($data){
		if(empty($data)){
			return array();
		}
		$CI = & get_instance();
		$location_data = get_location_data($data, 'region');
		$country_template = $CI->pg_module->get_module_config('countries', 'output_country_format');
		$region_template = $CI->pg_module->get_module_config('countries', 'output_region_format');
		
		$return = array();
		foreach($data as $id => $location){
			$template = $country = $country_code = $region = '';
			if(isset($location['country']) && !empty($location_data["country"][$location['country']])){
				$country = $location_data["country"][$location['country']]["name"];
				$country_code = $location['country'];
				$template = $country_template;
			}
			
			if(isset($location['region']) && !empty($location_data["region"][$location['region']])){
				$region = $location_data["region"][$location['region']]["name"];
				$template = $region_template;
			}
			
			if($template){
				$template = str_replace("[country]", $country, $template);
				$template = str_replace("[country_code]", $country_code, $template);
				$template = str_replace("[region]", $region, $template);
			}
			$return[$id] = $template;
		} 
		return $return;
	}
}	

if ( ! function_exists('cities_output_format')){
	function cities_output_format($data){
		if(empty($data)){
			return array();
		}
		$CI = & get_instance();
		$location_data = get_location_data($data, 'city');
		$country_template = $CI->pg_module->get_module_config('countries', 'output_country_format');
		$region_template = $CI->pg_module->get_module_config('countries', 'output_region_format');
		$city_template = $CI->pg_module->get_module_config('countries', 'output_city_format');

		$return = array();
		foreach($data as $id => $location){
			$template = $country = $country_code = $region = $city = '';
			if(isset($location['country']) && !empty($location_data["country"][$location['country']])){
				$country = $location_data["country"][$location['country']]["name"];
				$country_code = $location['country'];
				$template = $country_template;
			}
			
			if(isset($location['region']) && !empty($location_data["region"][$location['region']])){
				$region = $location_data["region"][$location['region']]["name"];
				$template = $region_template;
			}
			
			if(isset($location['city']) && !empty($location_data["city"][$location['city']])){
				$city = $location_data["city"][$location['city']]["name"];
				$template = $city_template;
			}
			
			if($template){
				$template = str_replace("[country]", $country, $template);
				$template = str_replace("[country_code]", $country_code, $template);
				$template = str_replace("[region]", $region, $template);
				$template = str_replace("[city]", $city, $template);
			}
			$return[$id] = $template;
		} 
		return $return;	
	}
}

if ( ! function_exists('districts_output_format')){
	function districts_output_format($data){
		if(empty($data)){
			return array();
		}
		
		$CI = & get_instance();
		
		$use_district = $CI->pg_module->get_module_config('countries', 'use_district');
		if(!$use_district){
			return cities_output_format($data);
		}
		
		$location_data = get_location_data($data, 'district');
		$country_template = $CI->pg_module->get_module_config('countries', 'output_country_format');
		$region_template = $CI->pg_module->get_module_config('countries', 'output_region_format');
		$city_template = $CI->pg_module->get_module_config('countries', 'output_city_format');
		$districts_template = $CI->pg_module->get_module_config('countries', 'output_district_format');

		$return = array();
		foreach($data as $id => $location){
			$template = $country = $country_code = $region = $city = '';
			if(isset($location['country']) && !empty($location_data["country"][$location['country']])){
				$country = $location_data["country"][$location['country']]["name"];
				$country_code = $location['country'];
				$template = $country_template;
			}
			
			if(isset($location['region']) && !empty($location_data["region"][$location['region']])){
				$region = $location_data["region"][$location['region']]["name"];
				$template = $region_template;
			}
			
			if(isset($location['city']) && !empty($location_data["city"][$location['city']])){
				$city = $location_data["city"][$location['city']]["name"];
				$template = $city_template;
			}
			
			if(isset($location['district']) && !empty($location_data["district"][$location['district']])){
				$district = $location_data["district"][$location['district']]["name"];
				$template = $districts_template;
			}
			
			if($template){
				$template = str_replace("[country]", $country, $template);
				$template = str_replace("[country_code]", $country_code, $template);
				$template = str_replace("[region]", $region, $template);
				$template = str_replace("[city]", $city, $template);
				$template = str_replace("[district]", $district, $template);
			}
			$return[$id] = $template;
		} 
		return $return;	
	}
}


if ( ! function_exists('address_output_format')){
	function address_output_format($data, $microdata=false){
		if(empty($data)){
			return array();
		}
		$CI = & get_instance();
		$location_data = get_location_data($data, 'city');
		$country_template = $CI->pg_module->get_module_config('countries', 'output_country_format');
		$region_template = $CI->pg_module->get_module_config('countries', 'output_region_format');
		$city_template = $CI->pg_module->get_module_config('countries', 'output_city_format');
		$district_template = $CI->pg_module->get_module_config('countries', 'output_district_format');
		$address_template = $CI->pg_module->get_module_config('countries', 'output_address_format');

		$return = array();
		foreach($data as $id => $location){
			$template = $country = $country_code = $region = $city = '';
			if(isset($location['country']) && !empty($location_data["country"][$location['country']])){
				$country = $location_data["country"][$location['country']]["name"];
				$country_code = $location['country'];
				$template = $country_template;
			}
			
			if(isset($location['region']) && !empty($location_data["region"][$location['region']])){
				$region = $location_data["region"][$location['region']]["name"];
				$template = $region_template;
			}
			
			if(isset($location['city']) && !empty($location_data["city"][$location['city']])){
				$city = $location_data["city"][$location['city']]["name"];
				$template = $city_template;
			}
			
			if(isset($location['district']) && !empty($location_data["district"][$location['district']])){
				$use_district = $CI->pg_module->get_module_config('countries', 'use_district');
				if($use_district){
					$district = $location_data["district"][$location['district']]["name"];
					$template = $district_template;
				}
			}
			
			if((isset($location['address']) && !empty($location['address'])) || (isset($location['zip_code']) && !empty($location['zip_code']))){
				$template = $address_template;
			}
			
			if($microdata){
				$country = '<span itemprop="country-name">'.$country.'</span>';
				$region = '<span itemprop="region">'.$region.'</span>';
				$city = '<span itemprop="locality">'.$city.'</span>';
				$location['address'] = '<span itemprop="street-address">'.$location['address'].'</span>';
				$location['zip_code'] = '<span itemprop="postal-code">'.$location['zip_code'].'</span>';
			}

			if($template){
				$template = str_replace("[country]", $country, $template);
				$template = str_replace("[country_code]", $country_code, $template);
				$template = str_replace("[region]", $region, $template);
				$template = str_replace("[city]", $city, $template);
				$template = str_replace("[district]", $district, $template);
				$template = str_replace("[address]", $location['address'], $template);
				$template = str_replace("[zip_code]", $location['zip_code'], $template);
			}
			$return[$id] = preg_replace('/^(\s)*,(\s)*|(\s)*,(\s)*$/', '', preg_replace('/,(\s)*,/', ',', $template));
		} 
		return $return;	
	}
}


if ( ! function_exists('get_location_data')){
	function get_location_data($data, $type = 'city'){
		$CI = & get_instance();
		$CI->load->model("Countries_model");

		if(empty($data)){
			return array();
		}
		
		$use_district = $CI->pg_module->get_module_config('countries', 'use_district');
		
		$return = $country_ids = $region_ids = $city_ids = $district_ids = array();
		foreach($data as $set){
			// country
			if(isset($set['country']) && !empty($set['country']) && !in_array($set['country'], $country_ids)){
				$country_ids[] = $set['country'];
			}
			
			if($type != 'country'){
				// region
				if(isset($set['region']) && !empty($set['region']) && !in_array($set['region'], $region_ids)){
					$region_ids[] = $set['region'];
				}
				
				if($type != 'region'){
					// city
					if(isset($set['city']) && !empty($set['city']) && !in_array($set['city'], $city_ids)){
						$city_ids[] = $set['city'];
					}
			
					if($type != 'city'){
						// district
						if(isset($set['district']) && !empty($set['district']) && !in_array($set['district'], $district_ids)){
							$district_ids[] = $set['district'];
						}
					}
				}
			}
		}
		
		if(!empty($country_ids)){
			$return["country"] = $CI->Countries_model->get_countries_by_code($country_ids);
		}
		
		if(!empty($region_ids)){
			$return["region"] = $CI->Countries_model->get_regions_by_id($region_ids);
		}
		
		if(!empty($city_ids)){
			$return["city"] = $CI->Countries_model->get_cities_by_id($city_ids);
		}

		if(!empty($district_ids)){
			$return["district"] = $CI->Countries_model->get_districts_by_id($district_ids);
		}

		return $return;
	}
}

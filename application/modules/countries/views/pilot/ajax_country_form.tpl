<div class="load_content_controller">
	{switch from=$type}
		{case value='country'}
			<h1>{l i='header_country_select' gid='countries'}</h1>
			{capture assign='form_back_link'}<a href="#" id="country_select_back" class="btn-link" style="display:none;"><ins class="fa fa-arrow-left fa-lg edge"></ins>{l i='link_reset_country' gid='countries'}</a>{/capture}
		{case value='region'}
			<h1>{l i='header_region_select' gid='countries'}</h1>
			{capture assign='form_crumb'}<div class="crumb">{$data.country.name}</div>{/capture}
			{capture assign='form_back_link'}<a href="#" id="country_select_back" class="btn-link" style="display:none;"><ins class="fa fa-arrow-left fa-lg edge"></ins>{l i='link_select_another_country' gid='countries'}</a>{/capture}
		{case value='city'}
			<h1>{l i='header_city_select' gid='countries'}</h1>
			{capture assign='form_crumb'}<div class="crumb">{$data.country.name} > {$data.region.name}</div>{/capture}
			{capture assign='form_search'}<input type="text" id="city_search" class="controller-search">{/capture}
			{capture assign='form_back_link'}<a href="#" id="country_select_back" class="btn-link"><ins class="fa fa-arrow-left fa-lg edge"></ins>{l i='link_select_another_region' gid='countries'}</a>{/capture}
			{capture assign='form_pages'}<div id="city_page" class="fright"></div>{/capture}
		{case value='district'}
			<h1>{l i='header_district_select' gid='countries'}</h1>
			{capture assign='form_crumb'}<div class="crumb">{$data.country.name} > {$data.region.name} > {$data.city.name}</div>{/capture}
			{capture assign='form_search'}<input type="text" id="district_search" class="controller-search">{/capture}
			{capture assign='form_back_link'}<a href="#" id="country_select_back" class="btn-link"><ins class="fa fa-arrow-left fa-lg edge"></ins>{l i='link_select_another_city' gid='countries'}{/capture}
			{capture assign='form_pages'}<div id="district_page" class="fright"></div>{/capture}
		{case}
			{capture assign='form_back_link'}<a href="#" id="country_select_back" class="btn-link"><ins class="fa fa-arrow-left fa-lg edge"></ins>{l i='link_reset_country' gid='countries'}</a>{/capture}
	{/switch}
	<div class="inside">
		{$form_crumb}{$form_search}
		<ul class="controller-items" id="country_select_items"></ul>
		<div class="controller-actions">{$form_pages}<div>
			<div>{$form_back_link}</div>
			<div class="fright">
				<a href="javascript:void(0);" id="country_select_close" class="btn-link"><ins class="fa fa-arrow-left fa-lg edge"></ins>{l i='link_close' gid='countries'}</a>
			</div>
		</div>
	</div>
</div>

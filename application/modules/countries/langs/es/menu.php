<?php

$install_lang["admin_countries_menu_countries_install_item"] = "Instalación";
$install_lang["admin_countries_menu_countries_install_item_tooltip"] = "";
$install_lang["admin_countries_menu_countries_list_item"] = "Países";
$install_lang["admin_countries_menu_countries_list_item_tooltip"] = "";
$install_lang["admin_menu_settings_items_content_items_countries_menu_item"] = "Países";
$install_lang["admin_menu_settings_items_content_items_countries_menu_item_tooltip"] = "Instalar y editar la base de datos los países";


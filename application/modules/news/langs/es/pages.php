<?php

$install_lang["admin_header_feed_add"] = "Añadir canal";
$install_lang["admin_header_feed_change"] = "Editar Canal";
$install_lang["admin_header_feeds_list"] = "Noticias del sitio";
$install_lang["admin_header_news_add"] = "Añadir noticia";
$install_lang["admin_header_news_change"] = "Editar noticia";
$install_lang["admin_header_news_list"] = "Noticias del sitio";
$install_lang["admin_header_news_settings"] = "Configuración de Noticias";
$install_lang["admin_header_settings_list"] = "Noticias del sitio";
$install_lang["api_error_empty_news_id"] = "Código de Noticia vacío";
$install_lang["api_error_news_not_found"] = "Noticias no encontradas";
$install_lang["error_feed_link_incorrect"] = "El enlace al canal está vacío";
$install_lang["error_feed_max_news_incorrect"] = "La cantidad max  de noticias debe ser mayor que 0";
$install_lang["error_gid_already_exists"] = "Noticia con esa palabra clave ya existe";
$install_lang["error_gid_incorrect"] = "Palabra clave incorrecta o está vacía";
$install_lang["error_name_incorrect"] = "Asunto está vacía";
$install_lang["error_sett_feed_channel_description_incorrect"] = "Descripción del canal está vacío";
$install_lang["error_sett_feed_channel_title_incorrect"] = "Título de canal está vacío";
$install_lang["error_sett_feed_image_title_incorrect"] = "Se requiere imagen del título RSS";
$install_lang["error_sett_rss_news_count_incorrect"] = "El número máximo de noticias RSS debe ser superior a 0";
$install_lang["error_sett_userhelper_page_incorrect"] = "Artículos por página deben ser más de 0";
$install_lang["error_sett_userside_page_incorrect"] = "Artículos por página deben ser más de 0";
$install_lang["feed_source"] = "Fuente";
$install_lang["field_annotation"] = "Anotación";
$install_lang["field_annotation_text"] = "La anotación se muestra en una página de noticias";
$install_lang["field_content"] = "Contenido";
$install_lang["field_date_add"] = "Adicional";
$install_lang["field_feed_link"] = "El enlace al canal";
$install_lang["field_feed_max_news"] = "Número máximo de noticias importadas";
$install_lang["field_feed_max_news_text"] = "Número máximo de noticias importadas a la vez";
$install_lang["field_feed_title"] = "Título de RSS";
$install_lang["field_gid"] = "Palabra clave";
$install_lang["field_icon"] = "Imagen";
$install_lang["field_icon_delete"] = "Borrar la imagen";
$install_lang["field_name"] = "Sujeto";
$install_lang["field_news_lang"] = "Idioma";
$install_lang["field_news_type"] = "Fuente";
$install_lang["field_settings_rss_channel_description"] = "Descripción del canal RSS";
$install_lang["field_settings_rss_channel_title"] = "Título del canal RSS";
$install_lang["field_settings_rss_image_title"] = "Título del logo RSS";
$install_lang["field_settings_rss_image_url"] = "Logo RSS";
$install_lang["field_settings_rss_news_max_count"] = "Número máximo de noticias en RSS";
$install_lang["field_settings_rss_use_feeds_news"] = "Usar canales de noticias en RSS";
$install_lang["field_settings_rss_userhelper_items"] = "Noticias por página en el bloque";
$install_lang["field_settings_rss_userside_items"] = "Noticias por página en el listado";
$install_lang["field_status"] = "Estado";
$install_lang["field_video"] = "vídeo";
$install_lang["field_video_delete"] = "Eliminar vídeo";
$install_lang["field_video_status"] = "Estado de procesamiento de vídeo";
$install_lang["field_video_status_end"] = "Convertido";
$install_lang["field_video_status_images"] = "Crear imágenes";
$install_lang["field_video_status_start"] = "Procesando...";
$install_lang["field_video_status_waiting"] = "A la espera de la conversión de youtube";
$install_lang["filter_all_feeds"] = "Todos los canales";
$install_lang["filter_section_text"] = "Text";
$install_lang["header_index_news"] = "Noticias";
$install_lang["header_latest_added_news"] = "Últimas noticias";
$install_lang["header_news"] = "Noticias";
$install_lang["header_view_news"] = "Ver noticias";
$install_lang["link_activate_news"] = "Activar noticias";
$install_lang["link_add_feeds"] = "Añadir canal";
$install_lang["link_add_news"] = "Añadir noticia";
$install_lang["link_back_to_news"] = "Volver a las noticias";
$install_lang["link_deactivate_feed"] = "Desactivar canal";
$install_lang["link_deactivate_news"] = "Desactivar";
$install_lang["link_delete_feed"] = "Eliminar canal";
$install_lang["link_delete_news"] = "Eliminar noticias";
$install_lang["link_edit_feed"] = "Editar canal";
$install_lang["link_edit_news"] = "Editar noticias";
$install_lang["link_parse_feed"] = "Importar noticias";
$install_lang["link_read_more"] = "Leer más";
$install_lang["link_view_more"] = "Ver detalles";
$install_lang["no_feeds"] = "No hay canales todavía";
$install_lang["no_news"] = "No hay noticias todavía";
$install_lang["no_news_yet_header"] = "No hay noticias todavía";
$install_lang["note_delete_feed"] = "¿Está seguro que desea eliminar el canal?";
$install_lang["note_delete_news"] = "¿Está seguro de que desea eliminar la noticia?";
$install_lang["subscriptions_type_last_news"] = "Últimas noticias";
$install_lang["success_activate_feed"] = "Canal activado con éxito";
$install_lang["success_activate_news"] = "Noticias activadas con éxito";
$install_lang["success_add_feed"] = "Canal guardado correctamente";
$install_lang["success_add_news"] = "Noticias agregadas correctamente";
$install_lang["success_deactivate_feed"] = "El Canal está desactivado";
$install_lang["success_deactivate_news"] = "Noticia desactivada";
$install_lang["success_delete_feed"] = "Canal eliminado";
$install_lang["success_delete_news"] = "Noticia eliminada";
$install_lang["success_no_feed_news"] = "No hay canales de noticias que añadir";
$install_lang["success_parse_feed"] = "Canales de noticias añadidos con éxito ([count])";
$install_lang["success_settings_save"] = "Los ajustes se han guardado correctamente";
$install_lang["success_update_feed"] = "El canal se ha guardado correctamente";
$install_lang["success_update_news"] = "Noticias actualizadas correctamente";


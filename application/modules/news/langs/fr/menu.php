<?php

$install_lang["admin_menu_settings_items_content_items_news_menu_item"] = "Nouvelles";
$install_lang["admin_menu_settings_items_content_items_news_menu_item_tooltip"] = "Ajouter nouvelles et listes de nouvelles";
$install_lang["admin_news_menu_feeds_list_item"] = "Listes";
$install_lang["admin_news_menu_feeds_list_item_tooltip"] = "";
$install_lang["admin_news_menu_news_list_item"] = "Nouvelles";
$install_lang["admin_news_menu_news_list_item_tooltip"] = "";
$install_lang["admin_news_menu_settings_list_item"] = "Paramètres";
$install_lang["admin_news_menu_settings_list_item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-news-item"] = "Nouvelles";
$install_lang["user_footer_menu_footer-menu-news-item_tooltip"] = "";


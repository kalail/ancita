<?php

$install_lang["seo_tags_index_description"] = "Новости сайта. Список последних новостей.";
$install_lang["seo_tags_index_header"] = "Новини";
$install_lang["seo_tags_index_keyword"] = "новини, списък с новини";
$install_lang["seo_tags_index_og_description"] = "Новини на сайта. Списък с последни новини.";
$install_lang["seo_tags_index_og_title"] = "Новини";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Недвижими имоти";
$install_lang["seo_tags_view_description"] = "Преглед на новина [name].";
$install_lang["seo_tags_view_header"] = "[name]";
$install_lang["seo_tags_view_keyword"] = "новина, преглед на новини";
$install_lang["seo_tags_view_og_description"] = "Преглед на новина [name].";
$install_lang["seo_tags_view_og_title"] = "[name]";
$install_lang["seo_tags_view_og_type"] = "article";
$install_lang["seo_tags_view_title"] = "Недвижими имоти : Новини : [name|default]";


<?php

$install_lang["admin_menu_settings_items_content_items_news_menu_item"] = "Nachrichten";
$install_lang["admin_menu_settings_items_content_items_news_menu_item_tooltip"] = "Nachrichten und News Feed hinzufügen";
$install_lang["admin_news_menu_feeds_list_item"] = "Feeds";
$install_lang["admin_news_menu_feeds_list_item_tooltip"] = "";
$install_lang["admin_news_menu_news_list_item"] = "Nachrichten";
$install_lang["admin_news_menu_news_list_item_tooltip"] = "";
$install_lang["admin_news_menu_settings_list_item"] = "Einstellungen";
$install_lang["admin_news_menu_settings_list_item_tooltip"] = "";
$install_lang["user_footer_menu_footer-menu-news-item"] = "Nachrichten";
$install_lang["user_footer_menu_footer-menu-news-item_tooltip"] = "";


<?php

$install_lang["seo_tags_index_description"] = "Site news. Latest news.";
$install_lang["seo_tags_index_header"] = "Nachrichten";
$install_lang["seo_tags_index_keyword"] = "news, news items";
$install_lang["seo_tags_index_og_description"] = "Site news. Latest news.";
$install_lang["seo_tags_index_og_title"] = "Nachrichten";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Pilot Group : Nachrichten";
$install_lang["seo_tags_view_description"] = "Read news item [name]";
$install_lang["seo_tags_view_header"] = "[name]";
$install_lang["seo_tags_view_keyword"] = "news, read news";
$install_lang["seo_tags_view_og_description"] = "Read news item [name]";
$install_lang["seo_tags_view_og_title"] = "[name]";
$install_lang["seo_tags_view_og_type"] = "article";
$install_lang["seo_tags_view_title"] = "Pilot Group : Nachrichten : [name|default]";


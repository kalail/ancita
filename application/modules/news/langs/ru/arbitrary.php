<?php

$install_lang["seo_tags_index_description"] = "Новости сайта. Список последних новостей.";
$install_lang["seo_tags_index_header"] = "Новости";
$install_lang["seo_tags_index_keyword"] = "новости, список новостей";
$install_lang["seo_tags_index_og_description"] = "Новости сайта. Список последних новостей.";
$install_lang["seo_tags_index_og_title"] = "Новости";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Pilot Group : Новости";
$install_lang["seo_tags_view_description"] = "Читать новость [name].";
$install_lang["seo_tags_view_header"] = "[name]";
$install_lang["seo_tags_view_keyword"] = "новость, читать новость";
$install_lang["seo_tags_view_og_description"] = "Читать новость [name].";
$install_lang["seo_tags_view_og_title"] = "[name]";
$install_lang["seo_tags_view_og_type"] = "article";
$install_lang["seo_tags_view_title"] = "Pilot Group : Новости : [name|default]";


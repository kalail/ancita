<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * News management
 * 
 * @package PG_RealEstate
 * @subpackage News
 * @category	helpers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Mikhail Makeev <mmakeev@pilotgroup.net>
 * @version $Revision: 68 $ $Date: 2010-01-11 16:02:23 +0300 (Пн, 11 янв 2010) $ $Author: irina $
 **/

if ( ! function_exists('news_last_added')) {
	/**
	 * Return last added news
	 * 
	 * @param integer $count max news count
	 * @return string
	 */
	function news_last_added($count){
		$CI = & get_instance();
		$CI->load->model('News_model');
		
		$count = intval($count);
		if(!$count) $count = 8;
		
		$news = $CI->News_model->get_news_list(1, $count, array("date_add" => "DESC"), array("where"=>array("status"=>1, "id_lang"=>$CI->pg_language->current_lang_id)));
		$CI->template_lite->assign("news", $news);
		
		return $CI->template_lite->fetch("helper_latest_added_news", "user", "news");
	}
}

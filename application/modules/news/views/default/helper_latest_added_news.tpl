{if $news}
<h2>{l i='header_latest_added_news' gid='news'}</h2>
<div class="latest_added_news_block">
	{foreach item=item key=key from=$news}
	<div class="news">
		{if $item.img}<div class="image"><a href="{seolink module='news' method='view' data=$item}"><img src="{$item.media.img.thumbs.small}" align="left" /></a></div>
		<div class="body">{/if}
			<b>{$item.name|truncate:100}</b>
			{$item.annotation|truncate:180}<br>
			<a href="{seolink module='news' method='view' data=$item}">{l i='link_view_more' gid='news'}</a>
		{if $item.img}</div>{/if}		
	</div>
	{/foreach}
	<div class="clr"></div>
	<p><a href="{seolink module='news' method='index'}">{l i='link_read_more' gid='news'}</a></p>
</div>
{/if}

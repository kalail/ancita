{include file="header.tpl"}
{include file="left_panel.tpl" module=start}
<div class="rc">
	<div class="content-block">
		<h1>{seotag tag='header_text'}</h1>
		<div class="news-view">
			{*<span class="date">{$data.date_add|date_format:$page_data.date_format}</span>*}
			{if $data.img}
			<img src="{$data.media.img.thumbs.big}" align="left" />
			<div class="clr"></div>
			{/if}
			{if !$data.content}{$data.annotation}{else}{$data.content}{/if}
			{if $data.video_content.embed}
				<p>{$data.video_content.embed}</p>
			{/if}

			{if $data.feed_link}{l i='feed_source' gid='news'}: <a href="{$data.feed_link}">{$data.feed.title}</a>{/if}
			<div class="clr"></div>
			<br><a href="{seolink module='news' method='index'}">{l i='link_back_to_news' gid='news'}</a>
		</div>
	</div>

	{block name=show_social_networks_like module=social_networking}
	{block name=show_social_networks_share module=social_networking}
	{block name=show_social_networks_comments module=social_networking}
</div>
<div class="clr"></div>
{include file="footer.tpl"}

<?php

$install_lang["admin_menu_main_items_payments_menu_item"] = "Платежи";
$install_lang["admin_menu_main_items_payments_menu_item_tooltip"] = "";
$install_lang["admin_payments_menu_payments_list_item"] = "Платежи пользователей";
$install_lang["admin_payments_menu_payments_list_item_tooltip"] = "";
$install_lang["admin_payments_menu_settings_list_item"] = "Настройки валюты";
$install_lang["admin_payments_menu_settings_list_item_tooltip"] = "";
$install_lang["admin_payments_menu_systems_list_item"] = "Платежные системы";
$install_lang["admin_payments_menu_systems_list_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_payments_item_agent_my_payments_item"] = "Мои платежи";
$install_lang["agent_account_menu_agent_payments_item_agent_my_payments_item_tooltip"] = "";
$install_lang["company_account_menu_company_payments_item_company_my_payments_item"] = "Мои платежи";
$install_lang["company_account_menu_company_payments_item_company_my_payments_item_tooltip"] = "";
$install_lang["private_account_menu_private_payments_item_private_my_payments_item"] = "Мои платежи";
$install_lang["private_account_menu_private_payments_item_private_my_payments_item_tooltip"] = "";



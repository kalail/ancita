<?php

$install_lang["notification_payment_status_updated"] = "Estado del pago actualizado";
$install_lang["tpl_payment_status_updated_content"] = "Hola [user],\n\nEstado de su pago actualizado:\n[payment] – [status]\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_payment_status_updated_subject"] = "[domain] | Estado del pago actualizado";


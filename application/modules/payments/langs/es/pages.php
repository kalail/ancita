<?php

$install_lang["admin_header_currency_add"] = "Añadir moneda";
$install_lang["admin_header_currency_change"] = "Cambiar moneda";
$install_lang["admin_header_currency_list"] = "Pagos";
$install_lang["admin_header_payment_system_change"] = "Editar la configuración del sistema de pago";
$install_lang["admin_header_payments_list"] = "Pagos";
$install_lang["admin_header_systems_list"] = "Pagos";
$install_lang["api_error_wrong_auth_type"] = "Tipo de autenticación incorrecto";
$install_lang["currency_updater_xe"] = "Xe.com";
$install_lang["currency_updater_yahoo"] = "Yahoo Finanzas";
$install_lang["error_abbr_required"] = "Se requiere abreviatura moneda";
$install_lang["error_currency_code_already_exists"] = "Moneda con dicho código ya existe";
$install_lang["error_currency_code_incorrect"] = "Código de la moneda es incorrecto";
$install_lang["error_currency_name_required"] = "El nombre es necesario";
$install_lang["error_deactivate_is_default_currency"] = "Default currency can't be de-activated";
$install_lang["error_delete_is_default_currency"] = "Moneda Predeterminado no se puede eliminar";
$install_lang["error_empty_currencies"] = "Monedas vacíos";
$install_lang["error_format_required"] = "Se requiere formato de moneda";
$install_lang["error_incorrect_rates_driver"] = "Tasas incorrectas de driver";
$install_lang["error_system_gid_already_exists"] = "Sistema con tal palabra clave ya existe";
$install_lang["error_system_gid_incorrect"] = "Palabra clave está vacía o es incorrecta";
$install_lang["error_system_name_incorrect"] = "Nombre incorrecto";
$install_lang["field_amount"] = "Cantidad";
$install_lang["field_billing_type"] = "Sistema de pago";
$install_lang["field_currency_abbr"] = "Abreviatura";
$install_lang["field_currency_default"] = "Moneda predeterminada";
$install_lang["field_currency_format"] = "Formato de moneda";
$install_lang["field_currency_gid"] = "Moneda";
$install_lang["field_currency_name"] = "Nombre";
$install_lang["field_currency_status"] = "Status";
$install_lang["field_date_add"] = "Fecha";
$install_lang["field_decimal_part"] = "Decimales";
$install_lang["field_decimal_part_dash"] = "Guión";
$install_lang["field_decimal_part_none"] = "Ninguno";
$install_lang["field_decimal_part_one"] = "Uno";
$install_lang["field_decimal_part_two"] = "Dos";
$install_lang["field_decimal_separator"] = "Separador decimal";
$install_lang["field_decimal_separator_comma"] = "Coma";
$install_lang["field_decimal_separator_period"] = "Punto";
$install_lang["field_groups_separator"] = "Separador de grupos";
$install_lang["field_groups_separator_comma"] = "Coma";
$install_lang["field_groups_separator_empty"] = "Vacío";
$install_lang["field_groups_separator_period"] = "Punto";
$install_lang["field_groups_separator_space"] = "Espacio";
$install_lang["field_info_data"] = "Detalles";
$install_lang["field_number_format"] = "Detalles";
$install_lang["field_payment_amount"] = "Cantidad";
$install_lang["field_payment_billing_system"] = "Sistema de pago";
$install_lang["field_payment_date"] = "Fecha de pago";
$install_lang["field_payment_name"] = "Descripción";
$install_lang["field_payment_type"] = "Tipo";
$install_lang["field_payment_user"] = "Usuario";
$install_lang["field_per_base"] = "Relación de moneda";
$install_lang["field_status"] = "Estado";
$install_lang["field_system_name"] = "Nombre";
$install_lang["field_system_use"] = "Usar este sistema";
$install_lang["field_system_used"] = "Activo";
$install_lang["filter_all_systems"] = "Todo";
$install_lang["filter_billing_type"] = "Sistema de pago";
$install_lang["filter_payment_type"] = "Tipo de pago";
$install_lang["filter_payments_all"] = "Todo";
$install_lang["filter_payments_approve"] = "Aprobado";
$install_lang["filter_payments_decline"] = "Rechazado";
$install_lang["filter_payments_wait"] = "En espera de aprobación";
$install_lang["filter_used_systems"] = "Activo";
$install_lang["header_currency_rates_update_auto"] = "Tipos de cambio actualizados automáticamente";
$install_lang["header_currency_rates_update_manual"] = "Tipos de cambio actualizados manualmente";
$install_lang["header_my_payments_statistic"] = "Historial de pagos";
$install_lang["header_payment_form"] = "Pago";
$install_lang["header_payment_return"] = "Resultado de pago";
$install_lang["html_field_comment"] = "Detalles del pago";
$install_lang["html_field_name"] = "Tipo de pago";
$install_lang["info_field_comment"] = "Descripción";
$install_lang["link_activate_system"] = "Activar el sistema";
$install_lang["link_add_currency"] = "Añadir nueva moneda";
$install_lang["link_currency_activate"] = "Activate currency";
$install_lang["link_currency_deactivate"] = "De-activate currency";
$install_lang["link_currency_rates_update_auto"] = "Guardar driver";
$install_lang["link_currency_rates_update_manual"] = "Actualizar tasas";
$install_lang["link_currency_rates_update_off"] = "Apagar";
$install_lang["link_currency_rates_update_on"] = "Encender";
$install_lang["link_deactivate_system"] = "Desactivar sistema";
$install_lang["link_default_currency"] = "Utilizar como moneda predeterminada";
$install_lang["link_delete_currency"] = "Eliminar moneda";
$install_lang["link_edit_currency"] = "Editar moneda";
$install_lang["link_edit_cyrrency"] = "Editar moneda";
$install_lang["link_edit_payments"] = "Editar";
$install_lang["link_payment_approve"] = "Aprobar";
$install_lang["link_payment_decline"] = "Rechazar";
$install_lang["no_payment_currencies"] = "Ninguna moneda";
$install_lang["no_payment_systems"] = "No hay sistemas de facturación aún";
$install_lang["no_payments"] = "No hay pagos aún";
$install_lang["note_delete_currency"] = "¿Está seguro que desea eliminar la moneda?";
$install_lang["payment_status_approved"] = "Aprobado";
$install_lang["payment_status_declined"] = "Rechazado";
$install_lang["payment_status_wait"] = "Pendiente";
$install_lang["payment_type_name_1"] = "Agregar fondos a la cuenta";
$install_lang["payment_type_name_2"] = "Pago por servicios";
$install_lang["payment_type_name_account"] = "Agregar fondos a la cuenta";
$install_lang["payment_type_name_services"] = "Pago por servicios";
$install_lang["stat_header_all"] = "Todo";
$install_lang["stat_header_approved"] = "Aprobado";
$install_lang["stat_header_awaiting"] = "En espera de la aprobación";
$install_lang["stat_header_declined"] = "Rechazado";
$install_lang["stat_header_payments"] = "Pagos";
$install_lang["success_activate_currency"] = "Los cambios se han guardado correctamente";
$install_lang["success_activate_system"] = "El sistema se activó con éxito";
$install_lang["success_add_currency"] = "La moneda se agregó correctamente";
$install_lang["success_approve_payment"] = "El pago fue aprobado con éxito";
$install_lang["success_currency_status_changed"] = "Currency status is successfully changed";
$install_lang["success_deactivate_system"] = "El sistema desactivó";
$install_lang["success_decline_payment"] = "El pago es rechazado";
$install_lang["success_delete_currency"] = "Moneda borrada";
$install_lang["success_payment_send"] = "Pago enviado correctamente";
$install_lang["success_rates_driver_updated"] = "Tasas de driver actualizada correctamente";
$install_lang["success_rates_update_turn_off"] = "Actualización de tasas de monedas desactivada";
$install_lang["success_rates_update_turn_on"] = "Actualización de tasas de monedas activada";
$install_lang["success_rates_updated"] = "Tipos de cambio actualizado";
$install_lang["success_update_currency"] = "Moneda actualizada correctamente";
$install_lang["success_update_system_data"] = "Los datos del sistema de facturación se ha actualizado correctamente";
$install_lang["system_field_amount_rate"] = "Tasa de monto";
$install_lang["system_field_appcode"] = "Código de la aplicación Daopay";
$install_lang["system_field_merchant_login"] = "Sesipon de comerciante";
$install_lang["system_field_merchant_pass1"] = "Contraseña 1";
$install_lang["system_field_merchant_pass2"] = "Contraseña 2";
$install_lang["system_field_purse_id"] = "ID Purse";
$install_lang["system_field_secret_word"] = "Palabra secreta";
$install_lang["system_field_seller_id"] = "ID del vendedor";
$install_lang["system_field_seller_name"] = "Nombre de la cuenta";
$install_lang["system_field_seller_secret"] = "secreto Vendedor";
$install_lang["system_field_serviceid"] = "ID de servicio PayGol";
$install_lang["system_field_smscoin_rate"] = "Tasa de conversión";
$install_lang["system_field_transaction_key"] = "Clave de transacción";
$install_lang["system_in_use"] = "Usado";
$install_lang["system_not_in_use"] = "No utilizado";
$install_lang["text_currency_rates_update"] = "Permitir las tasas de cambio actualizadas automáticamente (las tasas de cambio se toman a partir del recurso especificado)";
$install_lang["text_my_payments_statistic"] = "Mis estadísticas sobre pagos";
$install_lang["text_payment_form"] = "Cambiar los datos de pago";
$install_lang["text_payment_return"] = "Estadísticas sobre el retorno de Pago";


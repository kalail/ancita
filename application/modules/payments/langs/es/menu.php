<?php

$install_lang["admin_menu_main_items_payments_menu_item"] = "Pagos";
$install_lang["admin_menu_main_items_payments_menu_item_tooltip"] = "";
$install_lang["admin_payments_menu_payments_list_item"] = "Pagos de los usuarios";
$install_lang["admin_payments_menu_payments_list_item_tooltip"] = "";
$install_lang["admin_payments_menu_settings_list_item"] = "Configuración de moneda";
$install_lang["admin_payments_menu_settings_list_item_tooltip"] = "";
$install_lang["admin_payments_menu_systems_list_item"] = "Sistemas de pago";
$install_lang["admin_payments_menu_systems_list_item_tooltip"] = "";
$install_lang["agent_account_menu_agent_payments_item_agent_my_payments_item"] = "Historial de pagos";
$install_lang["agent_account_menu_agent_payments_item_agent_my_payments_item_tooltip"] = "";
$install_lang["company_account_menu_company_payments_item_company_my_payments_item"] = "Historial de pagos";
$install_lang["company_account_menu_company_payments_item_company_my_payments_item_tooltip"] = "";
$install_lang["private_account_menu_private_payments_item_private_my_payments_item"] = "Historial de pagos";
$install_lang["private_account_menu_private_payments_item_private_my_payments_item_tooltip"] = "";



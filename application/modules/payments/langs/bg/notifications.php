<?php

$install_lang["notification_payment_status_updated"] = "Статуса на плащанията е обновен";
$install_lang["tpl_payment_status_updated_content"] = "Здравейте, [user]!\n\nСтатусът на Вашето плащане е обновен:\n[payment] – [status]\n\nС уважение,\n[name_from]";
$install_lang["tpl_payment_status_updated_subject"] = "[domain] | Статусът на плащане е обновен";


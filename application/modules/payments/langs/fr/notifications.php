<?php

$install_lang["notification_payment_status_updated"] = "Statut de paiement mis à jour";
$install_lang["tpl_payment_status_updated_content"] = "Bonjour [user],\n\nVotre statut de paiement est mis à jour:\n[payment] – [status]\n\Cordialement,\n[name_from]";
$install_lang["tpl_payment_status_updated_subject"] = "[domain] | Statut de paiement mis à jour";


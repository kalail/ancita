<?php

$install_lang["notification_payment_status_updated"] = "Zahlungsstatus aktualisiert";
$install_lang["tpl_payment_status_updated_content"] = "Hallo [user],\n\nIhre Zahlungsstatus wurde aktualisiert:\n[payment] – [status]\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_payment_status_updated_subject"] = "[domain] | Zahlungsstatus aktualisiert";

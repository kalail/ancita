{counter print=false start=0 assign=counter}
{capture assign='currency_options'}
	{foreach item=item from=$currencies}{if $item.status}{counter}<option value="{$item.id}" {if $item.is_default} selected{/if}>{$item.gid}</option>{/if}{/foreach}	
{/capture}
{if $counter > 1}
<li>
	<select onchange="javascript: load_currency(this.value);">{$currency_options}</select>
	<script>{literal}
		var currency_url = site_url + 'users/change_currency/';
		function load_currency(value){
			location.href = currency_url + value;
		}
	{/literal}</script>
</li>
{/if}

{literal}
function(value){
	var custVal = '{/literal}{$template}{literal}'.replace({/literal}{$pattern_value|replace:'^]':'^\\]'}{literal}g, {/literal}{$value}{literal})
    var custformat = '{/literal}{$custmFormat}{literal}';
    if(custformat == 'yes'){
    	custVal = custVal.replace("USD","").replace("RUB","").replace("EUR","");
    }
	return custVal;
}
{/literal}

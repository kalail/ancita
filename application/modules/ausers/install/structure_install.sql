DROP TABLE IF EXISTS `[prefix]ausers`;
CREATE TABLE IF NOT EXISTS `[prefix]ausers` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(3) NOT NULL,
  `lang_id` int(3) NOT NULL,
  `user_type` varchar(10) NOT NULL,
  `permission_data` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nickname` (`nickname`),
  KEY `password` (`password`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `[prefix]ausers`  VALUES (NULL, 'support@pilotgroup.net', 'Support', '96e79218965eb72c92a549dd5a330112', 'Support', '', 0, 1, 'admin', '', '2012-05-21 13:57:31', '2012-05-21 13:57:31');
INSERT INTO `[prefix]ausers`  VALUES (NULL, 'webmaster@pilotgroup.net', 'Webmaster', '96e79218965eb72c92a549dd5a330112', 'Webmaster', 'look&feel;of the site', 1, 1, 'moderator', 'a:5:{s:7:"banners";a:3:{s:5:"index";s:1:"1";s:11:"groups_list";s:1:"1";s:11:"places_list";s:1:"1";}s:7:"content";a:2:{s:5:"index";s:1:"1";s:5:"promo";s:1:"1";}s:14:"dynamic_blocks";a:2:{s:5:"index";s:1:"1";s:11:"area_blocks";s:1:"1";}s:4:"menu";a:1:{s:5:"index";s:1:"1";}s:6:"themes";a:3:{s:5:"index";s:1:"1";s:15:"installed_theme";s:1:"1";s:13:"enable_themes";s:1:"1";}}', '2012-05-21 13:57:31', '2012-05-21 13:57:31');
INSERT INTO `[prefix]ausers`  VALUES (NULL, 'seo@pilotgroup.net', 'Seomanager', '96e79218965eb72c92a549dd5a330112', 'Seomanager', 'SEO', 1, 1, 'moderator', 'a:1:{s:3:"seo";a:6:{s:5:"index";s:1:"1";s:15:"default_listing";s:1:"1";s:7:"listing";s:1:"1";s:9:"analytics";s:1:"1";s:7:"tracker";s:1:"1";s:6:"robots";s:1:"1";}}', '2012-05-21 14:00:23', '2012-05-21 14:13:07');

DROP TABLE IF EXISTS `[prefix]ausers_moderate_methods`;
CREATE TABLE `[prefix]ausers_moderate_methods` (
`id` INT( 3 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`module` VARCHAR( 25 ) NOT NULL ,
`method` VARCHAR( 100 ) NOT NULL ,
`is_default` TINYINT( 3 ) NOT NULL
) ENGINE = MYISAM DEFAULT CHARSET=utf8;

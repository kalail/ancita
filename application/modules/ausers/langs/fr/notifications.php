<?php

$install_lang["notification_auser_account_create_by_admin"] = "Compte admin crée par un admin";
$install_lang["tpl_auser_account_create_by_admin_content"] = "Bienvenu à [domain]\n\nBonjour [user],\n\nUn compte a été crée pour vous sur [domain].\nCourriel de connexion: [email]\nMot de passe: [password]\n\nCordialement,\n[name_from]";
$install_lang["tpl_auser_account_create_by_admin_subject"] = "[user_type] compte est crée sur [domain]";


<?php

$install_lang["admin_header_ausers_add"] = "Добавить пользователя";
$install_lang["admin_header_ausers_change"] = "Редактирование данных";
$install_lang["admin_header_ausers_edit"] = "Редактирование данных";
$install_lang["admin_header_ausers_list"] = "Администраторы";
$install_lang["admin_header_login"] = "Административный раздел | Логин";
$install_lang["error_email_already_exists"] = "Пользователь с указанным email уже существует";
$install_lang["error_email_incorrect"] = "Неверный email";
$install_lang["error_login_password_incorrect"] = "Неверный логин или пароль";
$install_lang["error_name_incorrect"] = "Неверное имя";
$install_lang["error_nickname_already_exists"] = "Пользователь с указанным ником уже существует";
$install_lang["error_nickname_incorrect"] = "Неверный ник";
$install_lang["error_pass_repass_not_equal"] = "Пароль и повторный пароль должны быть одинаковыми";
$install_lang["error_password_empty"] = "Не указан пароль";
$install_lang["error_password_incorrect"] = "Неверный пароль";
$install_lang["field_change_password"] = "Изменить пароль";
$install_lang["field_date_created"] = "Создан";
$install_lang["field_date_modified"] = "Изменён";
$install_lang["field_description"] = "Описание";
$install_lang["field_email"] = "Email";
$install_lang["field_lang"] = "Язык по умолчанию";
$install_lang["field_name"] = "Имя";
$install_lang["field_nickname"] = "Ник";
$install_lang["field_password"] = "Пароль";
$install_lang["field_permissions"] = "Права доступа";
$install_lang["field_repassword"] = "Повторите пароль";
$install_lang["field_status"] = "Статус";
$install_lang["field_user_type"] = "Тип пользователя";
$install_lang["field_user_type_admin"] = "Администратор";
$install_lang["field_user_type_moderator"] = "Модератор";
$install_lang["filter_active_users"] = "Активные";
$install_lang["filter_admin_users"] = "Администраторы";
$install_lang["filter_all_users"] = "Все пользователи";
$install_lang["filter_moderator_users"] = "Модераторы";
$install_lang["filter_not_active_users"] = "Неактивные";
$install_lang["link_activate_user"] = "Активировать";
$install_lang["link_add_user"] = "Добавить пользователя";
$install_lang["link_deactivate_user"] = "Деактивировать";
$install_lang["link_delete_user"] = "Удалить";
$install_lang["link_edit_user"] = "Редактировать";
$install_lang["no_users"] = "Нет пользователей";
$install_lang["note_delete_user"] = "Вы уверены, что хотите удалить пользователя?";
$install_lang["success_activate_user"] = "Пользователь активирован";
$install_lang["success_add_user"] = "Пользователь добавлен";
$install_lang["success_deactivate_user"] = "Пользователь деактивирован";
$install_lang["success_delete_user"] = "Пользователь удалён";
$install_lang["success_update_user"] = "Изменения сохранены";


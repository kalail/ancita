<?php

$install_lang["notification_auser_account_create_by_admin"] = "Cuenta de administrador es creado por admin";
$install_lang["tpl_auser_account_create_by_admin_content"] = "Bienvenido a [domain]\n\nHola [user],\n\nUna cuenta fue creada en [domain].\nIniciar sesión correo: [email]\nConstraseña: [password]\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_auser_account_create_by_admin_subject"] = "[user_type] cuenta se crea en [domain]";


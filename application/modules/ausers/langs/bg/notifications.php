<?php

$install_lang["notification_auser_account_create_by_admin"] = "Създаден е профил на администратор";
$install_lang["tpl_auser_account_create_by_admin_content"] = "Добрe дошли на [domain]\n\nЗдравейте, [user]!\n\nЗа вас е създаден профил на сайта [domain].\nЛогин: [email]\nПарола: [password]\n\nС най-добри пожелания,\n[name_from]";
$install_lang["tpl_auser_account_create_by_admin_subject"] = "[user_type] account is created on [domain]";


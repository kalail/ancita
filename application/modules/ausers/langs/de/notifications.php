<?php

$install_lang["notification_auser_account_create_by_admin"] = "Administratorkonto ist erstellt durch Admin";
$install_lang["tpl_auser_account_create_by_admin_content"] = "Willkommen auf [domain]\n\nHallo [user],\n\nEin Konto wurde für Sie auf [domain] erstellt.\nAnmeldung E-Mail: [email]\nKennwort: [password]\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_auser_account_create_by_admin_subject"] = "[user_type] Konto ist erstellt auf [domain]";


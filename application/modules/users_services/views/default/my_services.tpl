{include file="header.tpl"}
{include file="left_panel.tpl" module=start}
<div class="rc">
	<div class="content-block">
		<h1>{l i='header_services' gid='users_services'}</h1>
		<p class="header-comment">{l i='text_services' gid='users_services'}</p>
	</div>
	<div class="content-block">
		<h2 class="line top bottom">{l i='header_contact_service' gid='users_services'}</h2>
		<p class="header-comment">{l i='text_contact_service' gid='users_services'}</p>
		{if $current_services.contact}
			<table class="list">
			<tr>
				<th>{l i='field_date_add' gid='users_services'}</th>
				<th>{l i='field_service_name' gid='users_services'}</th>
				<th>{l i='field_service_contacts_left' gid='users_services'}</th>
				<th>{l i='field_service_exp_date' gid='users_services'}</th>
			</tr>
			{foreach item=item from=$current_services.contact}
				{assign var='service_name' value=$item.service_name}
				<tr>
					<td>{$item.date_created|date_format:$page_data.date_format}</td>
					<td>
						{l i="service_string_name_$service_name" gid='users_services'}
						({if $item.service_data.contact_count > 0}{$item.service_data.contact_count}{else}{l i='unlim' gid='users_services'}{/if} {l i='contact_count' gid='users_services'} /
						{if $item.service_data.service_period > 0}{$item.service_data.service_period} {l i='service_expiration_period' gid='users_services'}{else}{l i='unlim' gid='users_services'}{/if})
					</td>
					<td>{if $item.contact_count > 0}{$item.contact_count}{else}{l i='unlim' gid='users_services'}{/if}</td>
					<td>{if $item.service_data.service_period > 0}{$item.contact_service_end_date|date_format:$page_data.date_format}{else}{l i='unlim' gid='users_services'}{/if}</td>
				</tr>
			{/foreach}
			</table><br>
		{/if}
		<a class="btn-link" href="{seolink module='users_services' method='index' service_gid='contact'}">
			<ins class="with-icon i-rarr"></ins>
			{l i='btn_pay' gid='users_services'}
		</a>
		<br>
	</div>
	<div class="content-block">
		<h2 class="line top bottom">{l i='header_post_service_'+$usertype gid='users_services'}</h2>
		<p class="header-comment">{l i='text_post_service_'+$usertype gid='users_services'}</p>
		{if $current_services.post}
			<table class="list">
			<tr>
				<th>{l i='field_date_add' gid='users_services'}</th>
				<th>{l i='field_service_name' gid='users_services'}</th>
				<th>{l i='field_service_post_left' gid='users_services'}</th>
			</tr>
			{foreach item=item from=$current_services.post}
				{assign var='service_name' value=$item.service_name}
				<tr>
					<td>{$item.date_created|date_format:$page_data.date_format}</td>
					<td>
						<b>{l i="service_string_name_$service_name" gid='users_services'}</b>
						{if $item.service_data.post_count > 0}{$item.service_data.post_count}{else}{l i='unlim' gid='users_services'}{/if} {l i='post_count' gid='users_services'} /
						{if $item.service_data.period > 0}{$item.service_data.period}{else}{l i='unlim' gid='users_services'}{/if} {l i='service_expiration_period' gid='users_services'}
					</td>
					<td>
						{if $item.post_count > 0}{$item.post_count}{else}{l i='unlim' gid='users_services'}{/if}
					</td>
				</tr>
			{/foreach}
			</table><br>
		{/if}
		<a class="btn-link" href="{seolink module='users_services' method='index' service_gid='post'}">
			<ins class="with-icon i-rarr"></ins>
			{l i='btn_pay' gid='users_services'}
		</a>
		<br>
	</div>
	<div class="content-block">
		<h2 class="line top bottom">{l i='header_combined_service' gid='users_services'}</h2>
		<p class="header-comment">{l i='text_combined_service' gid='users_services'}</p>
		{if $current_services.combined}
			<table class="list">
			<tr>
				<th>{l i='field_date_add' gid='users_services'}</th>
				<th>{l i='field_service_name' gid='users_services'}</th>
				<th>{l i='field_service_contacts_left' gid='users_services'}</th>
				<th>{l i='field_service_post_left' gid='users_services'}</th>
			</tr>
			{foreach item=item from=$current_services.combined}
				{assign var='service_name' value=$item.service_name}
				<tr>
					<td>{$item.date_created|date_format:$page_data.date_format}</td>
					<td>
						<b>{l i="service_string_name_$service_name" gid='users_services'}</b><br>
						<b>{l i='contact_count' gid='users_services'}:</b> {if $item.service_data.contact_count > 0}{$item.service_data.contact_count}{else}{l i='unlim' gid='users_services'}{/if}/{if $item.service_data.service_period > 0}{$item.service_data.service_period}{else}{l i='unlim' gid='users_services'}{/if} {l i='service_expiration_period' gid='users_services'}<br>
						<b>{l i='post_count' gid='users_services'}:</b> {if $item.service_data.post_count > 0}{$item.service_data.post_count}{else}{l i='unlim' gid='users_services'}{/if} /
						{if $item.service_data.period > 0}{$item.service_data.period}{else}{l i='unlim' gid='users_services'}{/if} {l i='service_expiration_period' gid='users_services'}
					</td>
					<td>
					{if $item.contact_status}
						{if $item.contact_count > 0}{$item.contact_count}{else}{l i='unlim' gid='users_services'}{/if} /
						{if $item.service_data.service_period > 0}{$item.contact_service_end_date|date_format:$page_data.date_format}{else}{l i='unlim' gid='users_services'}{/if}
					{else}
						{l i='service_expiried' gid='users_services'}
					{/if}
					</td>
					<td>
					{if $item.post_status}
						{if $item.post_count > 0}{$item.post_count}{else}{l i='unlim' gid='users_services'}{/if}
					{else}
						{l i='service_expiried' gid='users_services'}
					{/if}
					</td>
				</tr>
			{/foreach}
			</table><br>
		{/if}
		<a class="btn-link" href="{seolink module='users_services' method='index' service_gid='combined'}">
			<ins class="with-icon i-rarr"></ins>
			{l i='btn_pay' gid='users_services'}
		</a>
		<br>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}

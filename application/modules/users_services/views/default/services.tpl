{include file="header.tpl"}
{include file="left_panel.tpl" module=start}
<div class="rc">
	<div class="content-block">
		<h1>{l i='header_services' gid='users_services'}</h1>
		<p class="header-comment">{l i='text_services' gid='users_services'}</p>
	</div>
	{if $contact_services}
		<div class="content-block">
			<h2 class="line top bottom">{l i='header_contact_service' gid='users_services'}</h2>
			<p class="header-comment">{l i='text_contact_service' gid='users_services'}</p>
			<table class="list">
				<tr>
					<th>{l i='field_service_name' gid='users_services'}</th>
					<th>{l i='field_service_contacts' gid='users_services'}</th>
					<th>{l i='field_service_exp_period' gid='users_services'}</th>
					<th>{l i='field_amount' gid='users_services'}</th>
					<th class="w50"></th>
				</tr>
				{foreach item=item from=$contact_services}
					<tr>
						<td>{$item.name}</td>
						<td>{if $item.data_admin.contact_count > 0}{$item.data_admin.contact_count}{else}{l i='unlim' gid='users_services'}{/if}</td>
						<td>{if $item.data_admin.service_period > 0}{$item.data_admin.service_period} {l i='service_expiration_period' gid='users_services'}{else}{l i='unlim' gid='users_services'}{/if}</td>
						<td>{block name=currency_format_output module=start value=$item.price cur_gid=$base_currency.gid}</td>
						<td class="center">
							<a href="{$site_url}services/form/{$item.gid}" class="btn-link fright"><ins class="with-icon i-dollar"></ins></a>
						</td>
					</tr>
				{/foreach}
			</table>
		</div>
	{/if}
	{if $post_services}
		<div class="content-block">
			<h2 class="line top bottom">{l i='header_post_service_'+$usertype gid='users_services'}</h2>
			<p class="header-comment">{l i='text_post_service_'+$usertype gid='users_services'}</p>
			<table class="list">
				<tr>
					<th>{l i='field_service_name' gid='users_services'}</td>
					<th>{l i='field_service_post_'+$usertype gid='users_services'}</th>
					<th>{l i='field_service_act_period' gid='users_services'}</th>
					<th>{l i='field_amount' gid='users_services'}</th>
					<th class="w50"></th>
				</tr>
				{foreach item=item from=$post_services}
					<tr>
						<td>{$item.name}</td>
						<td>{if $item.data_admin.post_count > 0}{$item.data_admin.post_count}{else}{l i='unlim' gid='users_services'}{/if}</td>
						<td>{if $item.data_admin.period > 0}{$item.data_admin.period} {l i='service_expiration_period' gid='users_services'}{else}{l i='unlim' gid='users_services'}{/if}</td>
						<td>{block name=currency_format_output module=start value=$item.price cur_gid=$base_currency.gid}</td>
						<td class="center">
							<a href="{$site_url}services/form/{$item.gid}" class="btn-link fright"><ins class="with-icon i-dollar"></ins></a>
						</td>
					</tr>
				{/foreach}
			</table>
		</div>
	{/if}
	{if $combined_services}
		<div class="content-block">
			<h2 class="line top bottom">{l i='header_combined_service' gid='users_services'}</h2>
			<p class="header-comment">{l i='text_combined_service' gid='users_services'}</p>
			<table class="list">
				<tr>
					<th>{l i='field_service_name' gid='users_services'}</th>
					<th>{l i='field_service_contacts' gid='users_services'}</th>
					<th>{l i='field_service_post_'+$usertype gid='users_services'}</th>
					<th>{l i='field_amount' gid='users_services'}</th>
					<th class="w50"></th>
				</tr>
				{foreach item=item from=$combined_services}
					<tr>
						<td>{$item.name}</td>
						<td>
							{if $item.data_admin.contact_count > 0}{$item.data_admin.contact_count}{else}{l i='unlim' gid='users_services'}{/if} {l i='contact_count' gid='users_services'} /
							{if $item.data_admin.service_period > 0}{$item.data_admin.service_period}{else}{l i='unlim' gid='users_services'}{/if}  {l i='service_expiration_period' gid='users_services'}
						</td>
						<td>
							{if $item.data_admin.post_count > 0}{$item.data_admin.post_count}{else}{l i='unlim' gid='users_services'}{/if} {l i='post_count' gid='users_services'} /
							{if $item.data_admin.period > 0}{$item.data_admin.period}{else}{l i='unlim' gid='users_services'}{/if} {l i='service_expiration_period' gid='users_services'}
						</td>
						<td>{block name=currency_format_output module=start value=$item.price cur_gid=$base_currency.gid}</td>
						<td class="center">
							<a href="{$site_url}services/form/{$item.gid}" class="btn-link fright"><ins class="with-icon i-dollar"></ins></a>
						</td>
					</tr>
				{/foreach}
			</table>
		</div>
	{/if}
</div>
<div class="clr"></div>
{include file="footer.tpl"}

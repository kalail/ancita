<div class="content-block">
	<h2>{l i='featured_user_services' gid='users_services'}</h2>
	<p class="header-comment">
	{if $is_user_featured.is_featured}
		{l i='your_featured_period_till' gid='users_services'}: {$is_user_featured.featured_end_date|date_format:$page_data.date_format}<br />
		{l i='add_more_featured_period' gid='users_services'}<br />
	{else}
		{l i='create_featured_period' gid='users_services'}
	{/if}
	</p>
	{foreach item=item from=$services}
	<div class="r">
		<div class="payment_table">
		<table>
			<tr>
				<td colspan="2"><b>{$item.name}</b></td>
			</tr>
			{foreach item=item2 from=$item.template.data_admin_array}
				{assign var='service_parameter_gid' value=$item2.gid}
				<tr>
					<td>{$item2.name}:</td>
					<td class="value">
					{if $item2.type eq 'string' || $item2.type eq 'int' || $item2.type eq 'text'}
						{$item.data_admin[$service_parameter_gid]}
					{elseif $item2.type eq 'price'}
						{block name=currency_format_output module=start value=$item2.value cur_gid=$base_currency.gid}
					{elseif $item2.type eq 'checkbox'}
						{if $item2.value eq '1'}{l i='yes_checkbox_value' gid='services'}{else}{l i='no_checkbox_value' gid='services'}{/if}
					{/if}
					</td>
				</tr>
			{/foreach}
			<tr>
				<td>{l i='field_price' gid='services'}:</td>
				<td class="value"><b>{block name=currency_format_output module=start value=$item.price cur_gid=$base_currency.gid}</b></td>
			</tr>
		</table>
		</div>
	</div>
	<div class="r">
		<div class="b"><input type="button" class='btn' value="{l i='btn_apply' gid='start' type='button'}" onclick="document.location.href=site_url+'users_services/apply_service/{$user_id}/{$item.gid}'"></div>
	</div>
	{/foreach}
</div>


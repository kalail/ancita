<?php

$install_lang["notification_user_package_enabled"] = "Paquet de services payés initalisé";
$install_lang["notification_user_service_enabled"] = "Service payé initialisé";
$install_lang["notification_user_service_expired"] = "Service payé expiré";
$install_lang["tpl_user_package_enabled_content"] = "Bonjour [user],\n\nService payé [name] initialisé.\n\nCordialement,\n[name_from]";
$install_lang["tpl_user_package_enabled_subject"] = "[domain] | Service payé initialisé";
$install_lang["tpl_user_service_enabled_content"] = "Bonjour [user],\n\nService payé [name] initialisé jusqu'à [date].\n\nCordialement,\n[name_from]";
$install_lang["tpl_user_service_enabled_subject"] = "[domain] | Service payé initialisé";
$install_lang["tpl_user_service_expired_content"] = "Bonjour [user],\n\nService payé [name] a expiré. Voulez vous renouveler?\n\nCordialement,\n[name_from]";
$install_lang["tpl_user_service_expired_subject"] = "[domain] | Service payé expiré";


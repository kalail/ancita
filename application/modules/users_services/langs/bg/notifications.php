<?php

$install_lang["notification_user_package_enabled"] = "Пакета за платени услуги е стартиран";
$install_lang["notification_user_service_enabled"] = "Платената услуга е стартирана";
$install_lang["notification_user_service_expired"] = "Платената услуга изтече";
$install_lang["tpl_user_package_enabled_content"] = "Здравейте, [user]!\n\nПлатената услуга [name] е включена.\n\nС уважение,\n[name_from]";
$install_lang["tpl_user_package_enabled_subject"] = "[domain] | Платената услуга е включена";
$install_lang["tpl_user_service_enabled_content"] = "Здравейте, [user]!\n\nПлатената услуга [name] е включена и активана до [date].\n\nС уважение,\n[name_from]";
$install_lang["tpl_user_service_enabled_subject"] = "[domain] | Платената услуга е включена";
$install_lang["tpl_user_service_expired_content"] = "Здравейте, [user]!\n\nСрокът на действие на платената услуга [name] изтече. Желаете ли да удължите срока на активност?\n\nС уважение,\n[name_from]";
$install_lang["tpl_user_service_expired_subject"] = "[domain] | Платената услуга изтече";


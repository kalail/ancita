<?php

$install_lang["seo_tags_index_description"] = "Мои услуги. Списък на закупените ми услуги.";
$install_lang["seo_tags_index_header"] = "Мои услуги";
$install_lang["seo_tags_index_keyword"] = "мои услуги, услуги на сайта";
$install_lang["seo_tags_index_og_description"] = "Мои услуги. Списък на закупените ми услуги.";
$install_lang["seo_tags_index_og_title"] = "Мои услуги.";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Pilot Group : Мои услуги";
$install_lang["seo_tags_my_services_description"] = "Достъпни услуги. Списък на моите достъпни услуги.";
$install_lang["seo_tags_my_services_header"] = "Достъпни услуги";
$install_lang["seo_tags_my_services_keyword"] = "достъпни услуги, услуги на сайта";
$install_lang["seo_tags_my_services_og_description"] = "Достъпни услуги. Списък на моите достъпни услуги.";
$install_lang["seo_tags_my_services_og_title"] = "Достъпни услуги";
$install_lang["seo_tags_my_services_og_type"] = "article";
$install_lang["seo_tags_my_services_title"] = "Pilot Group : Достъпни услуги";


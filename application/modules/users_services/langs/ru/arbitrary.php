<?php

$install_lang["seo_tags_index_description"] = "Мои сервисы. Список купленных мной сервисов.";
$install_lang["seo_tags_index_header"] = "Мои сервисы";
$install_lang["seo_tags_index_keyword"] = "мои сервисы, сервисы сайта";
$install_lang["seo_tags_index_og_description"] = "Мои сервисы. Список купленных мне сервисов.";
$install_lang["seo_tags_index_og_title"] = "Мои сервисы";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Pilot Group : Мои сервисы";
$install_lang["seo_tags_my_services_description"] = "Доступные сервисы. Список доступных мне сервисов.";
$install_lang["seo_tags_my_services_header"] = "Доступные сервисы";
$install_lang["seo_tags_my_services_keyword"] = "доступные сервисы, сервисы сайта";
$install_lang["seo_tags_my_services_og_description"] = "Доступные сервисы. Список доступных мне сервисов.";
$install_lang["seo_tags_my_services_og_title"] = "Доступные сервисы";
$install_lang["seo_tags_my_services_og_type"] = "article";
$install_lang["seo_tags_my_services_title"] = "Pilot Group : Доступные сервисы";


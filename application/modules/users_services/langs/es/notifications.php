<?php

$install_lang["notification_user_package_enabled"] = "Servicio pagado (paquete) habilitado";
$install_lang["notification_user_service_enabled"] = "Servicio pagodo habilitado";
$install_lang["notification_user_service_expired"] = "Servicio pagado caducado";
$install_lang["tpl_user_package_enabled_content"] = "Hola [user],\n\nServicio pagado [name] habilitado.\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_user_package_enabled_subject"] = "[domain] | Servico pagado habilitado";
$install_lang["tpl_user_service_enabled_content"] = "Hola [user],\n\nServicio pagado [name] habilitado hasta [date].\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_user_service_enabled_subject"] = "[domain] | Servicio pagado habilitado";
$install_lang["tpl_user_service_expired_content"] = "Hola [user],\n\nServicio pagado [name] expiró. ¿Desea extender el periodo de actividad?\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_user_service_expired_subject"] = "[domain] | Servicio pagado caducado";


<?php

$install_lang["add_more_featured_period"] = "¿Le gustaría ampliar el período de actividad de servicio?";
$install_lang["add_more_show_logo_period"] = "¿Le gustaría ampliar el período de actividad de servicio?";
$install_lang["btn_pay"] = "continuar";
$install_lang["contact_count"] = "usuarios";
$install_lang["contact_service_buy_text"] = "Destalles de contacto proporcionados en términos de servicio pagado.<br>\n\nUsted no tiene paquetes activos. Encontrar paquetes disponibles a continuación:";
$install_lang["contact_service_link_buy"] = "Servicios pagados";
$install_lang["contact_service_spend_text"] = "Destalles de contacto proporcionados en términos de servicio pagado.<br>\n\nUsted tiene paquetes que pueden ser usadospara accesar a detalles de contacto:";
$install_lang["create_featured_period"] = "Conviértase en miembre destacado";
$install_lang["create_show_logo_period"] = "Visualice su imagen de perfil en los listados de búsqueda";
$install_lang["error_service_activating"] = "No se ha podido activar el servicio";
$install_lang["featured_user_services"] = "Miembros destacados";
$install_lang["field_amount"] = "Cantidad";
$install_lang["field_date_add"] = "Fecha de compra";
$install_lang["field_service_act_period"] = "Período de activación";
$install_lang["field_service_contacts"] = "№ de contactos";
$install_lang["field_service_contacts_left"] = "Contactos restantes";
$install_lang["field_service_exp_date"] = "fecha de expiración";
$install_lang["field_service_exp_period"] = "periodo de actividad";
$install_lang["field_service_name"] = "servicio de pago";
$install_lang["field_service_post_agent"] = "Publicaciones";
$install_lang["field_service_post_company"] = "Publicaciones";
$install_lang["field_service_post_left"] = "Publicaciones izquierda";
$install_lang["field_service_post_private"] = "Publicaciones";
$install_lang["header_combined_service"] = "paquete combinado";
$install_lang["header_contact_service"] = "La información de contacto";
$install_lang["header_get_user_contacts"] = "El acceso a los datos de contacto";
$install_lang["header_post_service_agent"] = "Listados publicación";
$install_lang["header_post_service_company"] = "Listados publicación";
$install_lang["header_post_service_private"] = "Listados publicación";
$install_lang["header_services"] = "servicios pagados";
$install_lang["post_count"] = "publicaciones";
$install_lang["seo_tags_index_description"] = "My services. List of my paid services.";
$install_lang["seo_tags_index_header"] = "My services";
$install_lang["seo_tags_index_keyword"] = "my services, paid services";
$install_lang["seo_tags_index_og_description"] = "My services. List of my paid services.";
$install_lang["seo_tags_index_og_title"] = "My services";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Pilot Group : My services";
$install_lang["seo_tags_my_services_description"] = "Available services. List of available services.";
$install_lang["seo_tags_my_services_header"] = "Available services";
$install_lang["seo_tags_my_services_keyword"] = "available services, site services";
$install_lang["seo_tags_my_services_og_description"] = "Available services. List of available services.";
$install_lang["seo_tags_my_services_og_title"] = "Available services";
$install_lang["seo_tags_my_services_og_type"] = "article";
$install_lang["seo_tags_my_services_title"] = "Pilot Group : Available services";
$install_lang["service_contacts_left"] = "Contactos restantes";
$install_lang["service_expiration_period"] = "día";
$install_lang["service_expiried"] = "expiró";
$install_lang["service_string_name_combined"] = "Servicio combinado";
$install_lang["service_string_name_contact"] = "Datos de contacto";
$install_lang["service_string_name_post"] = "publicación";
$install_lang["show_logo_services"] = "Logotipo de pantalla en las búsquedas";
$install_lang["success_service_activating"] = "Servicio se activado con éxito";
$install_lang["text_combined_service"] = "Paquete combinado incluye 2 servicios en 1 paquete - la información de contacto y publicación de lsitado. Es una gran oportunidad para ahorrar dinero!";
$install_lang["text_contact_service"] = "Usted será capaz de ver la información de contacto del usuario y en contacto con ellos directamente";
$install_lang["text_post_service_agent"] = "Usted será capaz de activar los listados durante un determinado período.";
$install_lang["text_post_service_company"] = "Usted será capaz de activar los listados durante un determinado período.";
$install_lang["text_post_service_private"] = "Usted será capaz de activar los listados durante un determinado período.";
$install_lang["text_service_trial"] = "Trial";
$install_lang["text_services"] = "He aquí una lista de servicios pagados disponibles y recientemente adquiridos.";
$install_lang["unlim"] = "ilimitado";
$install_lang["your_featured_period_till"] = "Usted es un miembro destacado hasta";
$install_lang["your_show_logo_period_till"] = "Mostrar el logotipo hasta";


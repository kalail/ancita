<?php

$install_lang["seo_tags_index_description"] = "My services. List of my paid services.";
$install_lang["seo_tags_index_header"] = "My services";
$install_lang["seo_tags_index_keyword"] = "my services, paid services";
$install_lang["seo_tags_index_og_description"] = "My services. List of my paid services.";
$install_lang["seo_tags_index_og_title"] = "My services";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "Pilot Group : My services";
$install_lang["seo_tags_my_services_description"] = "Available services. List of available services.";
$install_lang["seo_tags_my_services_header"] = "Available services";
$install_lang["seo_tags_my_services_keyword"] = "available services, site services";
$install_lang["seo_tags_my_services_og_description"] = "Available services. List of available services.";
$install_lang["seo_tags_my_services_og_title"] = "Available services";
$install_lang["seo_tags_my_services_og_type"] = "article";
$install_lang["seo_tags_my_services_title"] = "Pilot Group : Available services";


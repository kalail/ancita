<?php

$install_lang["notification_user_package_enabled"] = "Kostenpflichtiger Service (Paket) aktiviert";
$install_lang["notification_user_service_enabled"] = "Kostenpflichtiger Service aktiviert";
$install_lang["notification_user_service_expired"] = "Kostenpflichtiger Service abgelaufen";
$install_lang["tpl_user_package_enabled_content"] = "Hallo [user],\n\nDer kostenpflichtige Service [Name] ist aktiviert.\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_user_package_enabled_subject"] = "[domain] |  kostenpflichtiger Service aktiviert";
$install_lang["tpl_user_service_enabled_content"] = "Hallo [user],\n\nKostenpflichtiger Service [name] aktiviert ist und bis [Datum] aktiv.\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_user_service_enabled_subject"] = "[domain] |  kostenpflichtiger Service aktiviert";
$install_lang["tpl_user_service_expired_content"] = "Hallo [user],\n\nKostenpflichtiger Service [Name] ist abgelaufen. Möchten  Sie, seine Aktivität verlängern?\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_user_service_expired_subject"] = "[domain] | Kostenpflichtiger Service abgelaufen";


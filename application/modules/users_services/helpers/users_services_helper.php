<?php  

/**
 * Users services management
 * 
 * @package PG_RealEstate
 * @subpackage Users services
 * @category	helpers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Mikhail Makeev <mmakeev@pilotgroup.net>
 * @version $Revision: 68 $ $Date: 2010-01-11 16:02:23 +0300 (Пн, 11 янв 2010) $ $Author: irina $
 **/

if(!defined("BASEPATH")) exit("No direct script access allowed");

if(!function_exists("user_featured_services")){	
	/**
	 * Check featured status of user
	 */
	function user_featured_services(){
		$CI = &get_instance();
		$CI->load->model("users/model/users_model", "Users_model", true);		

		$user_type = $CI->session->userdata("user_type");
		$user_id = $CI->session->userdata("user_id");
		$CI->load->model("Services_model");
		$service_template = array($user_type."_featured_template");
		$services = $CI->Services_model->get_active_services($user_id, $service_template);
		//  ...and if the service is activated
		if(count($services) == 0) {
			return false;
		}
		$CI->template_lite->assign('services', $services);
		
		$CI->load->model("Users_services_model");
		$is_featured = $CI->Users_services_model->is_user_featured($user_id);
		$CI->template_lite->assign("is_user_featured",	$is_featured);
		
		$page_data["date_format"] = $CI->pg_date->get_format('date_time_literal', 'st');
		$CI->template_lite->assign("page_data", $page_data);
	
		$CI->load->model('payments/models/Payment_currency_model');
		$base_currency = $CI->Payment_currency_model->get_currency_default(true);
		$CI->template_lite->assign('base_currency', $base_currency);
		
		return $CI->template_lite->fetch("helper_".$user_type."_featured", "user", "users_services");
	}
}

if(!function_exists("user_show_logo_services")){	
	/**
	 * Check featured status of user
	 */
	function user_show_logo_services(){
		$CI = &get_instance();
		$CI->load->model("Users_model");		

		$user_type = $CI->session->userdata("user_type");
		$user_id = $CI->session->userdata("user_id");
		$CI->load->model("Services_model");
		$service_template = array($user_type."_show_logo_template");
		$services = $CI->Services_model->get_active_services($user_id, $service_template);
		//  ...and if the service is activated
		if(count($services) == 0) {
			return false;
		}
		$CI->template_lite->assign('services', $services);
		
		$CI->load->model("Users_services_model");
		$is_show_logo = $CI->Users_services_model->is_user_show_logo($user_id);
		$CI->template_lite->assign("is_show_logo",	$is_show_logo);
		
		$page_data["date_format"] = $CI->pg_date->get_format('date_time_literal', 'st');
		$CI->template_lite->assign("page_data", $page_data);
		
		$CI->load->model('payments/models/Payment_currency_model');
		$base_currency = $CI->Payment_currency_model->get_currency_default(true);
		$CI->template_lite->assign('base_currency', $base_currency);
		
		return $CI->template_lite->fetch("helper_".$user_type."_show_logo", "user", "users_services");
	}
}

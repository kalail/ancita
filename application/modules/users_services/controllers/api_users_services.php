<?php

/**
 * Users services api controller
 *
 * @package PG_RealEstate
 * @subpackage Users services
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Alexander Batukhtin <abatukhtin@pilotgroup.net>
 **/
class Api_Users_services extends Controller
{

	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::Controller();
		$this->load->model('Users_services_model');
	}

	/**
	 * Services list
	 *
	 * @param type $filter contact|post|combined
	 */
	public function list_services() {
		$user_id = $this->session->userdata('user_id');
		$filter = (string)$this->input->post('filter', true);
		$user_type = $this->session->userdata('user_type');
		$data['usertype'] = $user_type;

		$available_filter = array('contact', 'post', 'combined');
		if(!in_array($filter, $available_filter)) {
			$filter = '';
		}

		$this->load->model('Services_model');
		/* Contact services */
		if (!$filter || $filter == 'contact'){
			$service_template = array($user_type.'_contact_template');
			$contact_services = $this->Services_model->get_active_services($user_id, $service_template);
			$data['contact_services'] = $contact_services;
		}

		/* Post services */
		if (!$filter || $filter == 'post'){
			$service_template = array($user_type.'_post_template');
			$post_services = $this->Services_model->get_active_services($user_id, $service_template);
			$data['post_services'] = $post_services;
		}

		/* Combined services */
		if (!$filter || $filter == 'combined'){
			$service_template = array)$user_type.'_combined_template');
			$combined_services = $this->Services_model->get_active_services($user_id, $service_template);
			$data['combined_services'] = $combined_services;
		}

		// seo
		/*$data = $this->Users_model->get_user_by_id($user_id);
		$this->pg_seo->set_seo_data($data);
		$this->session->set_userdata(array('service_redirect' => 'payments/statistic'));*/

		$this->set_api_content('data', $data);
	}

	/**
	 * My services
	 *
	 */
	public function my() {
		$user_id = $this->session->userdata('user_id');
		$user_type = $this->session->userdata('user_type');
		$data['usertype'] = $user_type;
		$order['id'] = 'DESC';

		$params = array();
		$params['where']['id_user'] = $user_id;
		$params['where']['service_name'] = 'post';
		$params['where_sql'][] = "(contact_status='1' OR post_status='1')";
		$current_services['post'] = $this->Users_services_model->get_services_list($order, $params);

		$params['where']['service_name'] = 'combined';
		$current_services['combined'] = $this->Users_services_model->get_services_list($order, $params);

		$params['where']['service_name'] = 'contact';
		$current_services['contact'] = $this->Users_services_model->get_services_list($order, $params);

		$data['current_services'] = $current_services;

		$data['date_format'] = $this->pg_date->get_format('date_time_literal', 'st');

		// seo
		/*$this->load->model('users/models/Users_model');
		$data = $this->Users_model->get_user_by_id($user_id);
		$this->pg_seo->set_seo_data($data);*/
		$this->set_api_content('data', $data);
	}

}

<?php

/**
 * Users payments user side controller
 *
 * @package PG_RealEstate
 * @subpackage Users payments
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Alexander Batukhtin <abatukhtin@pilotgroup.net>
 * @version $Revision: 1 $ $Date: 2012-09-12 10:32:07 +0300 (Ср, 12 сент 2012) $ $Author: abatukhtin $
 **/
class Users_payments extends Controller
{

	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::Controller();
	}

	public function save_payment() {
		if ($this->input->post('btn_payment_save')) {

			$user_id = $this->session->userdata('user_id');
			$amount = floatval($this->input->post('amount', true));
			$system_gid = $this->input->post('system_gid', true);
			$currency_gid = $this->input->post('currency_gid', true);

			if (empty($amount)) {
				$this->system_messages->add_message('error', l('error_empty_amount', 'users_payments'));
			} elseif ($amount < 0) {
				$this->system_messages->add_message('error', l('error_invalid_amount', 'users_payments'));
			} elseif (empty($system_gid)) {
				$this->system_messages->add_message('error', l('error_empty_system_gid', 'users_payments'));
			} else {
				$this->load->model('payments/models/Payment_currency_model');
				$default_currency = $this->Payment_currency_model->default_currency;
				
				$this->load->helper('payments');
				$amount = currency_value(array('value'=>$amount, 'cur_gid'=>$default_currency['gid'], 'to_gid'=>'USD'));
	
				$additional['name'] = l('header_add_funds', 'users_payments');
				$payment_data = send_payment('account',
											$user_id,
											$amount,
											'USD',
											$system_gid,
											$additional,
											'form');
				if (!empty($payment_data['errors'])) {
					$this->system_messages->add_message('error', $payment_data['errors']);
				}
				if (!empty($payment_data['info'])) {
					$this->system_messages->add_message('info', $payment_data['info']);
				}
			}
		}
		redirect(site_url() . 'users/account');
	}
}

<?php

$install_lang["notification_users_update_account"] = "Funds added on account";
$install_lang["tpl_users_update_account_content"] = "Hello [fname],\n\nYour account is [account]\n\nBest regards,\n[name_from]";
$install_lang["tpl_users_update_account_subject"] = "[domain] | Funds added on account";


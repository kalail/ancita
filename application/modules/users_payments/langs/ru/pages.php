<?php

$install_lang["account_funds_add_message"] = "Пополнить счет";
$install_lang["account_funds_spend_message"] = "Списать средства со счета";
$install_lang["added_by_admin"] = "Добавлено администратором";
$install_lang["error_empty_amount"] = "Пустая сумма";
$install_lang["error_empty_billing_system_list"] = "Сервис временно недоступен";
$install_lang["error_empty_system_gid"] = "Выберите платежную систему";
$install_lang["error_empty_users_list"] = "Список пользователей пуст";
$install_lang["error_invalid_amount"] = "Неверное значение суммы";
$install_lang["error_money_not_sufficient"] = "Недостаточно денег на счету";
$install_lang["error_no_users_to_add_funds"] = "Выберите пользователей";
$install_lang["field_amount"] = "Сумма";
$install_lang["field_billing"] = "Тип платежа";
$install_lang["header_add_funds"] = "Пополнить счет";
$install_lang["link_add_funds"] = "+$ Пополнить счет";
$install_lang["on_account_header"] = "Сумма на счете";
$install_lang["success_add_funds"] = "Данные успешно обновлены";


<?php

$install_lang["notification_users_update_account"] = "Допълване на сметката";
$install_lang["tpl_users_update_account_content"] = "Здравейте, [fname]!\n\nСредства по сметка:  [account]\n\nС уважение,\n[name_from]";
$install_lang["tpl_users_update_account_subject"] = "[domain] | Зареждане на сметка";


<?php

$install_lang["account_funds_add_message"] = "Добавяне на средства";
$install_lang["account_funds_spend_message"] = "Дебитни средства от сметката";
$install_lang["added_by_admin"] = "Добавени от администратора";
$install_lang["error_empty_amount"] = "Няма сума";
$install_lang["error_empty_billing_system_list"] = "Услугата е временно недостъпна";
$install_lang["error_empty_system_gid"] = "Изберете платежна система";
$install_lang["error_empty_users_list"] = "Списъкът с потребители е празен";
$install_lang["error_invalid_amount"] = "Грешна сума";
$install_lang["error_money_not_sufficient"] = "Недостатъчно средства по сметката";
$install_lang["error_no_users_to_add_funds"] = "Изберете потребител";
$install_lang["field_amount"] = "Сума за плащане";
$install_lang["field_billing"] = "Метод на плащане";
$install_lang["header_add_funds"] = "Добавете средства";
$install_lang["link_add_funds"] = "Добавете средства +";
$install_lang["on_account_header"] = "Баланс по сметка";
$install_lang["success_add_funds"] = "Данните са обновени";


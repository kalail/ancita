<?php

$install_lang["account_funds_add_message"] = "Kapital hinzufügen";
$install_lang["account_funds_spend_message"] = "Kapital abschreiben";
$install_lang["added_by_admin"] = "von Admin hinzugefügt";
$install_lang["error_empty_amount"] = "Betrag leer";
$install_lang["error_empty_billing_system_list"] = "Service vorübergehend nicht verfügbar";
$install_lang["error_empty_system_gid"] = "Bezahlsystem auswählen";
$install_lang["error_empty_users_list"] = "Benutzer Liste leer";
$install_lang["error_invalid_amount"] = "Ungültiger Betrag";
$install_lang["error_money_not_sufficient"] = "Nicht genügend Guthaben auf dem Account";
$install_lang["error_no_users_to_add_funds"] = "Benutzer wurden nicht ausgewählt";
$install_lang["field_amount"] = "Betrag eingeben";
$install_lang["field_billing"] = "Wählen Sie die Bezahlmethode aus";
$install_lang["header_add_funds"] = "Geld dem Konto hinzufügen";
$install_lang["link_add_funds"] = "Geld hinzufügen +";
$install_lang["on_account_header"] = "Kontostand";
$install_lang["success_add_funds"] = "Konto wurde erfolgreich aktualisiert";


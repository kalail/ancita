<?php

$install_lang["notification_users_update_account"] = "Betrag auf dem Konto hinzugefügt";
$install_lang["tpl_users_update_account_content"] = "Hallo [fname],\n\nIhr Konto ist [account]\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_users_update_account_subject"] = "[domain] | Betrag auf dem Konto hinzugefügt";


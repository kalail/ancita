<?php

$install_lang["account_funds_add_message"] = "Ajouter fonds";
$install_lang["account_funds_spend_message"] = "Oublier fonds";
$install_lang["added_by_admin"] = "Ajouter administrateur";
$install_lang["error_empty_amount"] = "Quantité vide";
$install_lang["error_empty_billing_system_list"] = "Service indisponible";
$install_lang["error_empty_system_gid"] = "Choisir système de paiement";
$install_lang["error_empty_users_list"] = "Liste d'utilisateur vide";
$install_lang["error_invalid_amount"] = "Quantité invalide";
$install_lang["error_money_not_sufficient"] = "Fonds insuffisants";
$install_lang["error_no_users_to_add_funds"] = "Utilisateurs pas sélectionnés";
$install_lang["field_amount"] = "Paiement";
$install_lang["field_billing"] = "Choisir méthode de paiement";
$install_lang["header_add_funds"] = "Ajouter fonds à mon compte";
$install_lang["link_add_funds"] = "Ajouter fonds";
$install_lang["on_account_header"] = "Balanc de compte";
$install_lang["success_add_funds"] = "Compte mis à jour";


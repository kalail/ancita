<?php

$install_lang["account_funds_add_message"] = "Añadir fondos";
$install_lang["account_funds_spend_message"] = "Amortización de fondos";
$install_lang["added_by_admin"] = "Añadido por admin";
$install_lang["error_empty_amount"] = "Cantidad vacío";
$install_lang["error_empty_billing_system_list"] = "El servicio no está disponible temporalmente";
$install_lang["error_empty_system_gid"] = "Elija el sistema de pago";
$install_lang["error_empty_users_list"] = "Lista de usuarios vacía";
$install_lang["error_invalid_amount"] = "Cantidad no válida";
$install_lang["error_money_not_sufficient"] = "Fondos insuficientes en la cuenta";
$install_lang["error_no_users_to_add_funds"] = "Los usuarios no son seleccionados";
$install_lang["field_amount"] = "Introduzca el importe del pago";
$install_lang["field_billing"] = "Elija la forma de pago";
$install_lang["header_add_funds"] = "Agregar fondos a la cuenta";
$install_lang["link_add_funds"] = "Añadir fondos +";
$install_lang["on_account_header"] = "Balance de la cuenta";
$install_lang["success_add_funds"] = "Cuenta actualizada correctamente";


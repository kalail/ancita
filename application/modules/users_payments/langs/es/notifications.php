<?php

$install_lang["notification_users_update_account"] = "Fondos añadidos a la cuenta";
$install_lang["tpl_users_update_account_content"] = "Hola [fname],\n\nSu cuenta está [account]\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_users_update_account_subject"] = "[domain] | Fondos añadidos en cuenta";


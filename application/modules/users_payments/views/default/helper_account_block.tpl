{capture assign='user_account_value'}
	{block name=get_currency_value module=start value=$user_account cur_gid='USD'}
{/capture}
{if $template eq 'text'}
	{block name=currency_format_output module=start value=$user_account_value}
{else}
<div>
	<a href="{$site_url}users/account" class="value fleft">{l i='on_account_header' gid='users_payments'} ({block name=currency_output module=start cur_gid=$base_currency.gid})</a><span class="digit">{block name=currency_format_output module=start value=$user_account_value}</span>	
</div>
{/if}

function ReviewsForm(optionArr){
	this.properties = {
		siteUrl: '',
		sendReviewBtn: 'review_btn',
		cFormId: 'reviews_form',
		urlGetForm: 'reviews/ajax_get_form/',
		urlSendForm: 'reviews/ajax_send_review/',
		id_close: 'close_btn',
		rand: null,
		isPopup: true,
		isOwner: false,
		success: null,
		contentObj: new loadingContent({loadBlockWidth: '544px', closeBtnClass: 'load_content_close', closeBtnPadding: 5}),
		errorObj: new Errors,		
	};

	var _self = this;
	
	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		if(_self.properties.isPopup){
			$('#'+_self.properties.sendReviewBtn+'_'+_self.properties.rand).bind('click', function(){
				var data = {object_id: $(this).attr('data-id'), type_gid: $(this).attr('data-type'), responder_id: $(this).attr('data-responder'), is_owner: $(this).attr('data-owner') !== undefined ? 1 : 0, rand: _self.properties.rand};
				_self.get_form(data);
				return false;
			}).show();
		}else{
			$('#'+_self.properties.cFormId+'_'+_self.properties.rand).bind('submit', function(){
				if(_self.properties.readOnly) return;
				_self.send_form($(this).serialize());
				return false;
			});		
		}
	}
	
	this.get_form = function(data){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlGetForm,
			type: 'post',
			data: data,
			cache: false,
			success: function(data){
				_self.properties.contentObj.show_load_block(data);
				$('#'+_self.properties.id_close).unbind().bind('click', function(){
					_self.clearBox();
					return false;
				});
			}
		});		
		return false;
	}
	
	this.send_form = function(data){
		$.ajax({
			url: _self.properties.siteUrl + _self.properties.urlSendForm,
			type: 'post',
			data: data,
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					if(_self.properties.isPopup){
						var btn = $('#'+_self.properties.sendReviewBtn+'_'+_self.properties.rand);
						btn.attr('href', '').unbind('click');
						btn.find('ins').addClass('g');
						_self.properties.contentObj.hide_load_block();
					}else{
						$('#'+_self.properties.cFormId).find('textarea').val('');
					}
					_self.properties.errorObj.show_error_block(data.success, 'success');
					if(_self.properties.success){
						_self.properties.success(data.id);
					}
				}
			},
		});				
		return false;
	}
	
	this.clearBox = function(){
		var data = $('#'+_self.properties.cFormId+'_'+_self.properties.rand).serialize();
		_self.send_form(data);
	}
	
	_self.Init(optionArr);
}

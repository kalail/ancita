{include file="header.tpl" load_type='ui'}
{include file="left_panel.tpl" module=start}
<div class="rc">
	<div class="content-block">
		<h1>{l i='header_reviews_list' gid='reviews'} - {$page_data.total_rows} {l i='header_reviews_found' gid='reviews'}</h1>
		{$content}
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}

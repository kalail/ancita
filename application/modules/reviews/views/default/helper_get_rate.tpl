<link href="{$site_root}application/modules/reviews/views/default/css/style-{$_LANG.rtl}.css" rel="stylesheet" type="text/css" />
{js module=reviews file='reviews.js'}
{assign var=rate_type value=$type.rate_type}
<!-- div class="rating-info" -->
	<div class="rating {if $template eq 'mini' || $template eq 'mini-extended'}small{else}large{/if}" id="rating-{$rand}">
		{if $template eq 'extended'}<span>{$type.values[$rate_type].main.header}</span>{/if}
		<ul class="{$rate_type}">
			{assign var=count value=$type.values[$rate_type].main.votes|@count}
			{foreach from=$type.values[$rate_type].main.votes key=vote_id item=vote_label}
			<li data-id="{$vote_id}" data-label="{$vote_label}" {if $read_only && $count eq 2 && $vote_id eq 1}class="hide"{/if}></li>
			{/foreach}
		</ul>
		<label class="hide"></label>
		<input type="hidden" name="rating_data[main]" value="{$rating_data.main}" />
		<div class="clr"></div>
	</div>
	{if $template eq 'extended' || $template eq 'mini-extended'}
	<div class="rating_extended {if $template eq 'mini-extended'}mini{/if}" id="extended-rating-{$rand}">
		{foreach item=rate_item key=rate_key from=$type.values[$rate_type]}
			{if $rate_key ne 'main'}
			<span>{$type.values[$rate_type][$rate_key].header}:</span> 
			<div class="rating small">
				<ul class="{$rate_type}">
					{assign var=count value=$type.values[$rate_type].main.votes|@count}
					{foreach from=$type.values[$rate_type][$rate_key].votes key=vote_id item=vote_label}
					<li data-id="{$vote_id}" data-label="{$vote_label}" {if $read_only && $count eq 2 && $vote_id eq 1}class="hide"{/if}></li>
					{/foreach}
				</ul>
				<label></label>
				<input type="hidden" name="rating_data[{$rate_key}]" value="{$rating_data[$rate_key]}" />
				<div class="clr"></div>
			</div>
			<br />				
			{/if}
		{/foreach}
	</div>
	{/if}
<!-- /div -->
<script type="text/javascript">
{literal}
$(function(){
	new Reviews({
		siteUrl: '{/literal}{$site_root}{literal}', 
		ratingId: '{/literal}rating-{$rand}{literal}',
		type: '{/literal}{$symbolType}{literal}',
		{/literal}{if $read_only}readOnly: true,{/if}{literal}
		{/literal}{if $show_label}showLabel: true,{/if}{literal}
		{/literal}{if $_LANG.rtl eq 'rtl'}isRTL: true,{/if}{literal}
	});	
});
{/literal}
</script>

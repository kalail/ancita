{*<h1>{l i='header_reviews_list' gid='reviews'} - {$page_data.total_rows} {l i='header_reviews_found' gid='reviews'}</h1>*}

{if $reviews}
<div class="sorter line" id="sorter_block">
	{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
	<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>
</div>
{/if}

<div>
{foreach item=item from=$reviews}
	{capture assign='poster_view_link'}{strip}
		{seolink module='users' method='view' data=$item.poster}
	{/strip}{/capture}
	{capture assign='responder_view_link'}{strip}
		{seolink module='users' method='view' data=$item.responder}
	{/strip}{/capture}
	{if $item.banned}
	<div class="banned">
		<a class="fright btn-link" id="item-show-banned-{$item.id}" title="{l i='link_content_show' gid='reviews'}"><ins class="with-icon-small g i-expand"></ins></a>
		<a class="fright btn-link hide" id="item-hide-banned-{$item.id}" title="{l i='link_content_hide' gid='reviews'}"><ins class="with-icon-small g i-collapse"></ins></a>
		<p>{l i='text_content_banned' gid='reviews'}</p>
	</div>
	{/if}
	<div id="item-block-{$item.id}" class="item {if $item.banned}hide{/if}">
		<div class="review-actions">
			{if !$item.banned}{block name='mark_as_spam_block' module='spam' object_id=$item.id type_gid='reviews_object' template='link' is_owner=$is_review_owner}{/if}
			{if !$item.answer && $user_id eq $item.id_responder}<a href="{$site_url}reviews/reply/{$type.gid}/{$object_id}" id="reply-btn-{$item.id}" class="btn-link link-r-margin">{l i='btn_send_reply' gid='reviews' type='button'}</a>{/if}
		</div>
		<h3><a href="{$poster_view_link}">{$item.poster.output_name|truncate:50}</a>, {$item.date_add|date_format:$page_data.date_format} ({l i='stat_header_reviews_'+$item.gid_type gid='reviews'})</h3>
		<div class="image">
			<a href="{$poster_view_link}"><img src="{$item.poster.media.user_logo.thumbs.middle}" title="{$item.poster.output_name}"></a>
		</div>
		<div class="body">
			{l i='no_information' gid='start' assign='no_info_str'}
			{assign var=rate_type value=$item.type.rate_type}
			{assign var=rate_score value=$item.rating_data.main}
			<div class="t-1">
				{block name=get_rate_block module=reviews rating_data=$item.rating_data type_gid=$item.gid_type template='mini-extended' read_only="true" show_label="true"}
			</div>
			<div class="t-2">{$item.message}</div>
		</div>
		<div class="clr"></div>
		<div class="{if !$item.answer}hide{/if} reply" id="reply-block-{$item.id}">
			<h3><a href="{$responder_view_link}">{$item.responder.output_name|truncate:50}</a>, {if $item.date_answer|strtotime > 0}{$item.date_answer|date_format:$page_data.date_format}{/if}</h3>
			<div class="image">
				<a href="{$responder_view_link}"><img src="{$item.responder.media.user_logo.thumbs.middle}" title="{$item.responder.output_name}"></a>
			</div>
			<div class="body">
				<div class="t-1"></div>
				<div class="t-2" id="reply-text-{$item.id}">{$item.answer}</div>
			</div>
		</div>
		<div class="clr"></div>
		{if !$item.answer && $user_id eq $item.id_responder}
		<div class="edit_block">
		<form action="" method="post" id="reply-form-{$item.id}" class="hide">
			<div class="r">
				<div class="f">{l i='field_reviews_answer' gid='reviews'}:&nbsp;* </div>
				<div class="v"><textarea name="data[answer]" rows="5" cols="8"></textarea></div>
			</div>
			<div class="r">
				<div class="f">&nbsp;</div>
				<div class="v"><input type="submit" value="{l i='btn_save' gid='start' type='button'}" name="btn_save"></div>
			</div>
		</form>
		</div>
		<script>{literal}
			$(function(){
				var errorObj = new Errors();
				$('#reply-form-{/literal}{$item.id}{literal}').bind('submit', function(){
					var el = $(this);
					var data = $(this).serialize();
					$.ajax({
						url: '{/literal}{$site_root}{literal}' + 'reviews/ajax_reply/{/literal}{$item.id}{literal}', 
						type: 'POST',
						cache: false,
						data: data,
						dataType: 'json',
						success: function(data){
							if(data.success){
								el.hide();
								var block = $('#reply-block-{/literal}{$item.id}{literal}'); 
								var h3 = block.find('h3');
								var html = h3.html() + data.date_answer;
								h3.html(html);
								$('#reply-text-{/literal}{$item.id}{literal}').html(data.answer);
								block.show();
								$('#reply-btn-{/literal}{$item.id}{literal}').hide();
								errorObj.show_error_block(data.success, 'success');
							}else{
								errorObj.show_error_block(data.error, 'error');
							}
							return false;
						}
					});
					return false;
				});
				$('#reply-btn-{/literal}{$item.id}{literal}').bind('click', function(){
					$('#reply-form-{/literal}{$item.id}{literal}').show();
					return false;
				});
			});
		{/literal}</script>
		{/if}
	</div>	
	{if $item.banned}
	<script>{literal}
		$('#item-show-banned-{/literal}{$item.id}{literal}').bind('click', function(){
			$(this).hide();
			$('#item-hide-banned-{/literal}{$item.id}{literal}').css('display', 'block');
			$(this).parent().find('p').hide();
			$('#item-block-{/literal}{$item.id}{literal}').show();
		});
		$('#item-hide-banned-{/literal}{$item.id}{literal}').bind('click', function(){
			$(this).hide();
			$('#item-show-banned-{/literal}{$item.id}{literal}').show();
			$(this).parent().find('p').show();
			$('#item-block-{/literal}{$item.id}{literal}').hide();
		});
	{/literal}</script>
	{/if}
{foreachelse}
	<div class="item empty">{l i='no_reviews' gid='reviews'}</div>
{/foreach}
</div>
{if $reviews}
<div id="pages_block_2">{pagination data=$page_data type='full'}</div>
{/if}

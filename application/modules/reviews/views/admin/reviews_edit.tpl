{include file="header.tpl"}
<form action="" method="post" name="save_form">
<div class="edit-form n150">
	<div class="row header">{l i='admin_header_reviews_edit' gid='reviews'}</div>
	<div class="row">
		<div class="h">{l i='field_reviews_rate' gid='reviews'}:&nbsp;* </div>
		<div class="v">{block name=get_rate_block module=reviews rating_data=$data.rating_data type_gid=$data.gid_type}</div>
	</div>
    <div class="row">
		<div class="h">Name:&nbsp;* </div>
		<div class="v"><input type="text" name="data[rname]" id="rname" value="{$data.rname|escape}" required="required"/></div>
	</div>
     <div class="row">
		<div class="h">Email:&nbsp;* </div>
		<div class="v"><input type="text" name="data[remail]" id="remail" value="{$data.remail|escape}" required="required"/></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_reviews_message' gid='reviews'}:&nbsp;* </div>
		<div class="v"><textarea name="data[message]">{$data.message|escape}</textarea></div>
	</div>
	<div class="row">
		<div class="h">{l i='field_reviews_answer' gid='reviews'}: </div>
		<div class="v"><textarea name="data[answer]">{$data.answer|escape}</textarea></div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/reviews/index/{$data.gid_type}">{l i='btn_cancel' gid='start'}</a>
</div>
</form>
<div class="clr"></div>
<script>{literal}
$(function(){
	$("div.row:odd").addClass("zebra");
});
{/literal}</script>
{include file="footer.tpl"}

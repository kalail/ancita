{include file="header.tpl"}
<div class="actions">
<div class="edit-form n150">
	<div class="row header">{l i='admin_header_reviews_show' gid='reviews'}</div>
	<div class="row">
		<div class="h">{l i='field_reviews_rate' gid='reviews'}: </div>
		<div class="v">{block name=get_rate_block module=reviews rating_data=$data.rating_data type_gid=$data.gid_type read_only="true"}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_reviews_message' gid='reviews'}: </div>
		<div class="v">{$data.message}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_reviews_answer' gid='reviews'}: </div>
		<div class="v">{$data.answer}</div>
	</div>
	<div class="row">
		<div class="h">{l i='field_reviews_date_add' gid='reviews'}: </div>
		<div class="v">{$data.date_add|date_format:$date_format}</div>
	</div>
</div>
<a class="cancel" href="{$site_url}admin/reviews/index/{$data.gid_type}">{l i='btn_cancel' gid='start'}</a>
<div class="clr"></div>
<script>{literal}
	$(function(){
		$("div.row:odd").addClass("zebra");
	});
{/literal}</script>
{include file="footer.tpl"}

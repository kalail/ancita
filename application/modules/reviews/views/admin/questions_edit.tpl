{include file="header.tpl"}
<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header">{l i='admin_add_question' gid='reviews'}</div>
		<div class="row zebra">
			<div class="h">English:&nbsp;* </div>
			<div class="v">
				<textarea name="data[questionen]" cols="60" rows="2" style="margin: 0px; height: 80px; width: 400px;">{$data.questionen}</textarea>
			</div>
		</div>
        <div class="row zebra">
			<div class="h">Norsk:&nbsp;* </div>
			<div class="v">
				<textarea name="data[questionno]" cols="60" rows="2" style="margin: 0px; height: 80px; width: 400px;">{$data.questionno}</textarea>
			</div>
		</div>
        <div class="row zebra">
			<div class="h">Svenska:&nbsp;* </div>
			<div class="v">
				<textarea name="data[questionsw]" cols="60" rows="2" style="margin: 0px; height: 80px; width: 400px;">{$data.questionsw}</textarea>
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/reviews/questions/">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>
{include file="footer.tpl"}

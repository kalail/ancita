{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_reviews_menu'}
<div class="actions">&nbsp;</div>

<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n250">
		<div class="row header">{l i='admin_header_settings_edit' gid='reviews'}</div>
		<div class="row zebra">
			<div class="h">{l i='settings_use_alerts' gid='reviews'}:</div>
			<div class="v">
				<input type="hidden" name="reviews_use_alerts" value="0">
				<input type="checkbox" name="reviews_use_alerts" value="1" {if $settings_data.reviews_use_alerts}checked{/if} class="short" id="reviews_use_alerts">
				{l i='settings_alert_email' gid='reviews'}
				<input type="hidden" name="reviews_alert_email" value="{$settings_data.reviews_alert_email|escape}" id="reviews_alert_email_old">
				<input type="text" name="reviews_alert_email" value="{$settings_data.reviews_alert_email|escape}" class="long" id="reviews_alert_email" {if !$settings_data.reviews_use_alerts}disabled{/if}>
				<script>{literal}
					$(function(){
						$('#reviews_use_alerts').bind('change', function(){
							if(this.checked){
								$('#reviews_alert_email').removeAttr('disabled');
							}else{
								var value = $('#reviews_alert_email').val();
								$('#reviews_alert_email_old').val(value);
								$('#reviews_alert_email').attr('disabled', 'disabled');
							}
						});
					});
				{/literal}</script>
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
</form>

{include file="footer.tpl"}

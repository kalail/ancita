{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_reviews_menu'}
<div class="actions">
	<ul>
		<li><div class="l"><a href="{$site_root}admin/reviews/question_edit">{l i='admin_add_question' gid='reviews'}</a></div></li>
	</ul>
	&nbsp;
</div>


<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first">{l i='admin_questions' gid='reviews'}</th>
	<th class="w100">&nbsp;</th>
</tr>
{foreach item=item from=$questions}
<tr class="zebra">
	<td class="first center">{$item.questionen}</td>
	<td class="icons">
		<a href="{$site_url}admin/reviews/question_edit/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_contact_us' gid='contact_us'}" title="{l i='link_edit_contact_us' gid='contact_us'}"></a>
		<a href="{$site_url}admin/reviews/quesion_delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_contact_us' gid='contact_us' type='js'}')) return false;">
        <img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_contact_us' gid='contact_us'}" title="{l i='link_delete_contact_us' gid='contact_us'}"></a>
	</td>
</tr>
{/foreach}
</table>
{include file="footer.tpl"}

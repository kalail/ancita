	<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first" colspan="2">{l i='stat_header_reviews' gid='reviews'}</th>
	</tr>
	{if $stat_reviews.index_method}
	{foreach item=item from=$stat_reviews.types}
	{counter print=false assign=counter}
	<tr {if !($counter is div by 2)}class="zebra"{/if}>
		<td class="first"><a href="{$site_url}admin/reviews/index/{$item.gid}">{$item.output_name}</a></td>
		<td class="w50 center"><a href="{$site_url}admin/reviews/index/{$item.gid}">{$item.count}</a></td>
	</tr>
	{/foreach}
	{/if}
	</table>


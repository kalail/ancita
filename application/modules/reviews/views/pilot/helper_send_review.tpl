{js module='reviews' file='reviews-form.js'}
{switch from=$template}
	{case value='form'}
		{if !$is_send}
		<h1>{l i='header_reviews_form' gid='reviews'}</h1>
		<div class="edit_block">
			{include file="review_form.tpl" module=reviews theme=user}
		</div>
		{/if}
	{case value='first'}
		<a {if !$is_send}href="{$site_url}reviews/send_review"{/if} data-id="{$object_id}" data-type="{$type.gid}" data-responder="{$responder_id}" id="review_btn_{$rand}" title="{if $is_send}{l i='text_send_review' gid='reviews' type='button'}{else}{l i='link_send_review' gid='reviews' type='button'}{/if}">{l i='link_send_first' gid='reviews'}</a><br>
	{case}
		<a {if !$is_send}href="{$site_url}reviews/send_review"{/if} data-id="{$object_id}" data-type="{$type.gid}" data-responder="{$responder_id}" id="review_btn_{$rand}" class="btn-link link-r-margin" title="{if $is_send}{l i='text_send_review' gid='reviews' type='button'}{else}{l i='link_send_review' gid='reviews' type='button'}{/if}"><ins class="fa fa-edit {if $is_send}g{else}hover{/if} fa-lg edge"></ins></a>
{/switch}
{if !$is_send || $template eq 'form'}
<script>{literal}
$(function(){
	{/literal}{if $is_guest}{literal}
	new ReviewsForm({
		siteUrl: '{/literal}{$site_root}{literal}', 
		{/literal}{if $template eq 'form'}isPopup: false,{/if}{literal}
		{/literal}{if $success}success: {strip}{$success}{/strip},{/if}{literal}
		rand: {/literal}{$rand}{literal},
	});	
	{/literal}{else:}{literal}
	new ReviewsForm({
		siteUrl: '{/literal}{$site_root}{literal}', 
		{/literal}{if $template eq 'form'}isPopup: false,{/if}{literal}
		{/literal}{if $success}success: {strip}{$success}{/strip},{/if}{literal}
		rand: {/literal}{$rand}{literal},
	});	
	{/literal}{/if}{literal}
});
{/literal}</script>
{/if}


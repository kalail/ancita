	<form id="reviews_form_{$rand}" action="" method="POST">
	<div class="r">
		{block name=get_rate_block module=reviews type_gid=$type.gid template='extended' show_label='true'}
	</div>
    <div class="r">
		<div class="f">Name&nbsp;*:</div>
		<div class="v"><input type="text" name="rname" id="rname" onfocus="this.removeAttribute('readonly');" readonly/></div>
	</div>
    <div class="r">
		<div class="f">Email&nbsp;:</div>
		<div class="v"><input type="text" name="remail" id="remail" onfocus="this.removeAttribute('readonly');" readonly/></div>
	</div>
	<div class="r">
		<div class="f">{l i='field_reviews_message' gid='reviews'}&nbsp;*:</div>
		<div class="v"><textarea name="data[message]" rows="5" cols="23"></textarea></div>
	</div>
     <div class="r">
			<div class="f">{l i='field_contact_captcha' gid='contact'}&nbsp;*</div>
			<div class="v captcha">
				{$data.captcha_image}
				<input type="text" size="12" name="code" id="code" value="" maxlength="{$data.captcha_word_length}" onfocus="this.removeAttribute('readonly');" readonly>	
                <input type="hidden" name="captcha_word" id="captcha_word" value="{$captcha_word}">			
			</div>
		   </div>
	{*<div class="r">
		<input type="checkbox" name="agree" value="1" id="review_agree" /> <label for="review_agree">{l i='field_reviews_agree' gid='reviews'}</label>
	</div>*}
	<div class="r"><input type="submit" value="{l i='btn_send' gid='start' type='button'}" id="close_btn" /></div>
	<input type="hidden" name="type_gid" value="{$type.gid}">
	<input type="hidden" name="object_id" value="{$object_id}">
	<input type="hidden" name="responder_id" value="{$responder_id}">
	{if $is_review_owner}<input type="hidden" name="is_owner" value="1">{/if}
	</form>
    <script>{literal}
	$(function(){
    $("#autogen_error_block").css('z-index','999999');
	});
	{/literal}</script>

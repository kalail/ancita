<?php

define("REVIEWS_TYPES_TABLE", DB_PREFIX."reviews_types");
define("QUESTIONS_TABLE", DB_PREFIX."feedback_ques");
/**
 * Review type Model
 *
 * @package PG_RealEstate
 * @subpackage Reviews
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Reviews_type_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	/**
	 * Link to DataBase object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Fields of review type in data source
	 * 
	 * @var array
	 */
	public $fields = array(
		"id",
		"gid",
		"name",
		"rate_type",
		"module",
		"model",
		"callback",
		"date_add",
	);
	public $qfields = array(
		"id",
		"questionen",
		"questionno",
		"questionsw"
	);
	/**
	 * Settings for formatting review type object
	 * 
	 * @var array
	 */
	private $format_settings = array(
		"use_format" => true,
		"get_rate_type" => true,
		"get_rate_type_values" => true,
	);

	/**
	 * Configuration of rating types
	 * 
	 * @var array
	 */
	public $rating_type_settings = array(
		'stars' => array('answers' => array(1, 2, 3, 4, 5) ),
		'hands' => array('answers' => array(1, 5)),
	);
	
	public $rating_type_ld = 'rate_type';
	
	/**
	 * Cache of review types
	 * 
	 * @var array
	 */
	private $type_cache = array();
	
	/**
	 * Cache of formatted review types
	 * 
	 * @var array
	 */
	private $type_format_cache = array();
	
	/**
	 * Constructor
	 *
	 * @return Review_type_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;		
		foreach($this->CI->pg_language->languages as $id=>$value){
			$this->fields[] = "values_".$value["id"];
		}
	}

	/**
	 * Return review type object from data source by ID/GUID
	 * 
	 * @param integer $id type ID/GUID
	 * @param boolean $formatted format results
	 * @return mixed
	 */
	public function get_type($type_id, $formatted=false){
		$field = "id";
		if(!intval($type_id)){
			$field = "gid";
			$type_id= preg_replace("/[^a-z_]/", "", strtolower($type_id));
		}
		if(!$type_id) return false;
		
		if($formatted){
			if(isset($this->type_format_cache[$type_id])) return $this->type_format_cache[$type_id];
		}else{
			if(isset($this->type_cache[$type_id])) return $this->type_cache[$type_id];
		}
		
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(REVIEWS_TYPES_TABLE);
		$this->DB->where($field, $type_id);

		//_compile_select;
		$result = $this->DB->get()->result();
		if(!empty($result)){
			$rt = get_object_vars($result[0]);
			$this->type_cache[$rt['id']] = $this->type_cache[$rt['gid']] = $rt;
			if($formatted){
				$rt = current($this->format_type(array($rt)));
				$this->type_format_cache[$rt['id']] = $this->type_format_cache[$rt['gid']] = $rt;
			}
			return $rt;
		}else
			return false;
	}

	/**
	 * Save review type object to data source
	 * 
	 * @param integer review type identifier
	 * @param array $data review type data
	 * @return boolean
	 */
	public function save_type($id, $data){
		if(empty($data)) return false;
		if(!$id){
			$data["date_add"] = date("Y-m-d H:i:s");
			$this->DB->insert(REVIEWS_TYPES_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where("id", $id);
			$this->DB->update(REVIEWS_TYPES_TABLE, $data);
		}		
		return $id;
	}

	/**
	 * Remove type from data source by ID or GUID
	 * 
	 * @param mixed $id integer ID / string GID
	 * @return void
	 */
	public function delete_type($type_id){
		$type = $this->get_type($type_id);		
		$this->DB->where("id", $type["id"]);
		$this->DB->delete(REVIEWS_TYPES_TABLE); 
		return;
	}
	
	/**
	 * Return all types from data source as array
	 * 
	 * @param array $filter_object_ids filters identifiers
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_types($filter_object_ids=null, $formatted=true){
		$this->DB->select(implode(", ", $this->fields));
		$this->DB->from(REVIEWS_TYPES_TABLE);
		
		if(is_array($filter_object_ids)){
			foreach($filter_object_ids as $value){
				$this->DB->where_in("gid", $value);
			}
		}
		
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data = array();
			foreach($results as $r){
				$this->type_cache[$r['id']] = $this->type_cache[$r['gid']] = $data[$r['id']] = $r;
			}
			if($formatted){
				$data = $this->format_type($data);
				foreach($data as $r){
					$this->type_format_cache[$r['id']] = $this->type_format_cache[$r['gid']] = $r;
				}
			}
			return $data;
		}
		return array();
	}
	
	//list questions
	public function get_questions(){
		$this->DB->select(implode(", ", $this->qfields));
		$this->DB->from(QUESTIONS_TABLE);
		$results = $this->DB->get()->result_array();
		return $results;
	}
	//edit question
	public function get_question($question_id){
		$field = "id";
		if(!$question_id) return false;
		$this->DB->select(implode(", ", $this->qfields));
		$this->DB->from(QUESTIONS_TABLE);
		$this->DB->where($field, $question_id);
		$result = $this->DB->get()->result();
		if(!empty($result)){
			$rt = get_object_vars($result[0]);
			return $rt;
		}else
			return false;
	    }
	//save question after update
	 public function save_question($id, $data){
		if(empty($data)) return false;
		if(!$id){
			$this->DB->insert(QUESTIONS_TABLE, $data);
			$id = $this->DB->insert_id();
		}else{
			$this->DB->where("id", $id);
			$this->DB->update(QUESTIONS_TABLE, $data);
		}		
		return $id;
	}
	//delete question 
	public function delete_question($id){		
		$this->DB->where("id", $id);
		$this->DB->delete(QUESTIONS_TABLE);
		return "";
	}
	/**
	 * Validate review type for saving to data source
	 * 
	 * @param integer $id review type identifier
	 * @param array $data review type data
	 * @return array
	 */
	public function validate_type($id, $data){
		$return = array("errors"=> array(), "data" => array());
		
		if(isset($data['gid'])){
			$return["data"]["gid"] = trim(strip_tags($data['gid']));
			if(empty($return["data"]["gid"])){
				$return["errors"][] = l("error_empty_type_gid", "reviews");
			}
		}
		
		if(isset($data['rate_type'])){
			$return["data"]["rate_type"] = trim(strip_tags($data['rate_type']));
			if(empty($return["data"]["rate_type"])){
				$return["errors"][] = l("error_empty_rate_type", "reviews");
			}
		}
		if(isset($data['name'])){
			$return["data"]["name"] = trim(strip_tags($data['name']));
		}

		if(isset($data['module'])){
			$return["data"]["module"] = trim(strip_tags($data['module']));
		}

		if(isset($data['model'])){
			$return["data"]["model"] = trim(strip_tags($data['model']));
		}

		if(isset($data['callback'])){
			$return['data']["callback"] = trim(strip_tags($data['callback']));
		}
	
		$default_lang_id = $this->CI->pg_language->current_lang_id;
		if(isset($data['values_'.$default_lang_id])){
			$values_errors = false;
			foreach((array)$data['values_'.$default_lang_id] as $rates_key=>$rates_arr){
				foreach((array)$rates_arr as $rate_key=>$rate_arr){
					$return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['header'] = trim(strip_tags($rate_arr['header']));
					if(empty($return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['header'])){
						$return['errors'][] = l('error_empty_review_type_value', 'reviews');
						$values_errors = true;
						break;
					}
					if($values_errors) break;
		
					foreach((array)$rate_arr['votes'] as $vote_key=>$vote_value){			
						$return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['votes'][$vote_key] = trim(strip_tags($vote_value));
						if(empty($return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['votes'][$vote_key])){
							$return['errors'][] = l('error_empty_review_type_value', 'reviews');
							$values_errors = true;
							break;
						}
					}
					if($values_errors) break;
				}
			}
			foreach($this->pg_language->languages as $lid=>$lang){
				if($values_errors) break;
				if($lid == $default_lang_id) continue;
				$return['data']['values_'.$lid] = array();
				foreach((array)$data['values_'.$lid] as $rates_key=>$rates_arr){
					$return['data']['values_'.$lid][$rates_key] = array();
					foreach((array)$rates_arr as $rate_key=>$rate_arr){
						$return['data']['values_'.$lid][$rates_key][$rate_key] = array();
						if(!isset($rate_arr['header']) || empty($rate_arr['header'])){
							$return['data']['values_'.$lid][$rates_key][$rate_key]['header'] = $return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['header'];
						}else{
							$return['data']['values_'.$lid][$rates_key][$rate_key]['header'] = trim(strip_tags($rate_arr['header']));
							if(empty($return['data']['values_'.$lid][$rates_key][$rate_key]['header'])){
								$return['errors'][] = l('error_empty_review_type_value', 'listings');
								$values_errors = true;
								break;
							}
						}
						$return['data']['values_'.$lid][$rates_key][$rate_key]['votes'] = array();
						foreach((array)$return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['votes'] as $vote_key=>$vote_value){
							if(!isset($rate_arr['votes'][$vote_key]) || empty($rate_arr['votes'][$vote_key])){
								$return['data']['values_'.$lid][$rates_key][$rate_key]['votes'][$vote_key] = $return['data']['values_'.$default_lang_id][$rates_key][$rate_key]['votes'][$vote_key];
							}else{
								$return['data']['values_'.$lid][$rates_key][$rate_key]['votes'][$vote_key] = trim(strip_tags($rate_arr['votes'][$vote_key]));
								if(empty($return['data']['values_'.$lid][$rates_key][$rate_key]['votes'][$vote_key])){
									$return['errors'][] = l('error_empty_review_type_value', 'listings');
									$values_errors = true;
									break;
								}
							}
						}
						if($values_errors) break;
					}
					if($values_errors) break;
				}
				if($values_errors) break;
				$return['data']['values_'.$lid] = serialize($return['data']['values_'.$lid]);
			}
			$return['data']['values_'.$default_lang_id] = serialize($return['data']['values_'.$default_lang_id]);
		}

		return $return;
	}
	
	/**
	 * Format review type object
	 * 
	 * @param array $data review type data
	 * @return array
	 */
	public function format_type($data){
		if(!$this->format_settings["use_format"]){
			return $data;
		}
		
		if($this->format_settings["get_rate_type"]){
			$type_settings = $this->format_rating_type_settings();
			$lang_id = $this->CI->pg_language->current_lang_id;
			foreach($data as $key=>$type){
				$data[$key] = $type;
				$data[$key]["values"] = isset($data[$key]["values_".$lang_id]) ? (array)unserialize($data[$key]["values_".$lang_id]) : array();
				$data[$key]["rate_type_output"] = $type_settings[$data[$key]["rate_type"]]['name'];				
			}
		}
		
		if($this->format_settings["get_rate_type_values"]){
			foreach($this->CI->pg_language->languages as $lang_id=>$lang_item){
				foreach($data as $key=>$type){
					$data[$key] = $type;
					$data[$key]["values_".$lang_id] = isset($data[$key]["values_".$lang_id]) ? (array)unserialize($data[$key]["values_".$lang_id]) : array();
				}
			}
		}
		
		foreach($data as $key=>$type){
			$data[$key]["output_name"] = l('stat_header_reviews_'.$data[$key]["gid"], 'reviews');
			if(!$data[$key]["output_name"]) $data[$key]["output_name"] = $data[$key]["gid"];
		}
		
		return $data;
	}
	
	/**
	 * Format data of review type by default 
	 * 
	 * @param integer $type_id type identifier
	 * @return array
	 */
	public function format_default_type($type_id){
		$return = array();
		foreach ($this->fields as $field){
			$return[$field] = "-";
		}
		$return["output_name"] = "-";
		return $return;
	}
	
	/**
	 * Change settings for formatting review type object
	 * 
	 * @param array $data settings data
	 * @return array
	 */
	public function set_format_settings($data){
		foreach($data as $key=>$value){
			if(isset($this->format_settings[$key]))
				$this->format_settings[$key] = $value;
		}
	}
	
	/**
	 * Add review fields dependes on languages
	 * 
	 * @param integer $lang_id language idnetifier
	 * @return void
	 */
	public function lang_dedicate_module_callback_add($lang_id=false){
		if(!$lang_id) return;
		$this->CI->load->dbforge();
		$fields["values_".$lang_id] = array(
			"type" => "TEXT",
			"null" => TRUE,
		);
		$default_lang_id = $this->CI->pg_language->get_default_lang_id();
		$table_query = $this->CI->db->get(REVIEWS_TYPES_TABLE);
		$exists_fields = $table_query->list_fields();
		$this->CI->dbforge->add_column(REVIEWS_TYPES_TABLE, $fields);		
		if(in_array("values_" . $default_lang_id, $exists_fields)){
			$this->CI->db->set('values_' . $lang_id, 'values_' . $default_lang_id, false);
			$this->CI->db->update(REVIEWS_TYPES_TABLE);
		}
		
	}
	
	/**
	 * Remove review fields dependes on languages
	 * 
	 * @param integer $lang_id language identifier
	 * @return void
	 */
	public function lang_dedicate_module_callback_delete($lang_id=false){
		if(!$lang_id) return;
		$field_name = "values_" . $lang_id;
		$this->CI->load->dbforge();
		$table_query = $this->CI->db->get(REVIEWS_TYPES_TABLE);
		if (in_array("values_" . $lang_id, $table_query->list_fields())){
			$this->CI->dbforge->drop_column(REVIEWS_TYPES_TABLE, $field_name);
		}
	}	
	
	/**
	 * Add new rate type
	 * 
	 * @param string $rate_type_gid rate type GUID
	 * @param array $rate_type_data rate type data
	 * @param integer $lang_id language identifier
	 * @return void
	 */
	public function add_rate_type($rate_type_gid, $rate_type_data, $lang_id){
		$types = $this->get_types();
		foreach($types as $type){
			if (!isset($type["values_".$lang_id][$rtgid])){
				$type["values_".$lang_id][$rtgid] = $rate_type_data;
			}
			$this->save_type($type);
		}
	}
	
	/**
	 * Add review fields to data source
	 * 
	 * @param array $fields fields data
	 * @return void
	 */
	public function add_column($fields){
		$this->CI->load->dbforge();
		$table_query = $this->CI->db->get(REVIEWS_TYPES_TABLE);
		$table_fields = $this->CI->db->get(REVIEWS_TYPES_TABLE)->list_fields();
		foreach($fields as $field_name=>$field_data){
			if(!in_array($field_name, $table_fields)){
				$this->CI->dbforge->add_column(REVIEWS_TYPES_TABLE, array($field_name=>$field_data));
			}
		}
	}
	
	/**
	 * Remove review fields from data source
	 * 
	 * @param array $fields fields data
	 * @return void
	 */
	public function delete_column($fields){
		$this->CI->load->dbforge();
		$table_fields = $this->CI->db->get(REVIEWS_TYPES_TABLE)->list_fields();
		foreach($fields as $field_name){
			if(in_array($field_name, $table_fields)){
				$this->CI->dbforge->drop_column(REVIEWS_TYPES_TABLE, $field_name);
			}
		}
	}
	
	/**
	 * Format settings of rating type
	 * 
	 * @return array
	 */
	public function format_rating_type_settings(){
		$langs = ld($this->rating_type_ld, 'reviews');
		foreach($this->rating_type_settings as $rating_gid => $data){
			$this->rating_type_settings[$rating_gid]['name'] = $langs['option'][$rating_gid];
		}
		return $this->rating_type_settings;
	}
	
	/**
	 * Validate settings of reviews data
	 * 
	 * @param array $data settings data
	 * @return array
	 */
	public function validate_settings($data){
		$return = array("errors"=> array(), "data" => array());
		
		if(isset($data["reviews_use_alerts"])){
			$reviews_use_alerts = $return["data"]["reviews_use_alerts"] = $data["reviews_use_alerts"] ? 1 : 0;
		}else{
			$reviews_use_alerts = $this->pg_module->get_module_config('reviews', 'reviews_use_alerts');
		}
		
		if(isset($data["reviews_alert_email"])){
			$this->CI->config->load("reg_exps", TRUE);
			$email_expr = $this->CI->config->item("email", "reg_exps");
			$return["data"]["reviews_alert_email"] = trim(strip_tags($data["reviews_alert_email"]));
			if(!empty($return["data"]["reviews_alert_email"]) && !preg_match($email_expr, $return["data"]["reviews_alert_email"])){
				$return["errors"][] = l("error_email_incorrect", "reviews");
			}
			$reviews_alert_email = $return["data"]["reviews_alert_email"];
		}else{
			$reviews_alert_email = $this->pg_module->get_module_config('reviews', 'reviews_use_alerts');
		}
		
		if($reviews_use_alerts && !$reviews_alert_email){
			$return["errors"][] = l("error_empty_email", "reviews");
		}

		return $return;
	}
}

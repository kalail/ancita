<?php

$install_lang["notification_auser_reviews_object"] = "Нов отзив за сайта (за администратора)";
$install_lang["notification_auser_reviews_reply"] = "Коментар по отзива (за администратора)";
$install_lang["notification_user_reviews_object"] = "Нов отзив за сайта";
$install_lang["notification_user_reviews_reply"] = "Коментар по отзива";
$install_lang["tpl_auser_reviews_object_content"] = "Здравейте, администратор!\n\nПояви се нов отзив на сайта [domain]. За преглед влезте в панела на администратора > Обратна връзка > Отзиви и проверете полетата \"Потребители\" и \"Обявления\".\n\nС уважение,\n[name_from]";
$install_lang["tpl_auser_reviews_object_subject"] = "[domain] |Нов отзив";
$install_lang["tpl_auser_reviews_reply_content"] = "Здравейте, администратор!\n\nИма нов коментар на отзива.\n\nОтзив: [review]\n\nКоментар: [comment]\n\nС уважение,\n[name_from]";
$install_lang["tpl_auser_reviews_reply_subject"] = "[domain] | Коментар на отзив";
$install_lang["tpl_user_reviews_object_content"] = "Здравейте, [user]!\n\nОставен е нов отзив.\n\nАвтор на отзива: [poster]\n\nСъобщение: [review]\n\nС уважение,\n[name_from]";
$install_lang["tpl_user_reviews_object_subject"] = "[domain] | Нов отзив за [type] (ID=[object_id])";
$install_lang["tpl_user_reviews_reply_content"] = "Здравейте, [user]!\n\nПояви се нов коментар на вашия отзив.\n\nВашият отзив: [review]\nКоментар: [comment]\n\nС уважение,\n[name_from]";
$install_lang["tpl_user_reviews_reply_subject"] = "[domain] | Коментар на ваш отзив";


<?php

$install_lang["admin_header_reviews"] = "Évaluations";
$install_lang["admin_header_reviews_edit"] = "Éditer évaluations";
$install_lang["admin_header_reviews_list"] = "Évaluations";
$install_lang["admin_header_reviews_show"] = "Voir évaluations";
$install_lang["admin_header_reviews_types_list"] = "Évaluations";
$install_lang["admin_header_settings_edit"] = "Éditer paramètres d'évaluation";
$install_lang["admin_header_types_edit"] = "Éditer type d'évaluation";
$install_lang["admin_header_types_list"] = "Contenu";
$install_lang["btn_send_reply"] = "Répondre";
$install_lang["btn_send_review"] = "Écrire évaluations";
$install_lang["dop_rate_add_btn"] = "Ajouter nouvelle entrée";
$install_lang["dop_rate_delete_btn"] = "Supprimer entrée";
$install_lang["error_badwords_message"] = "Ce message contient des mauvais mots, essayez d'éviter cela";
$install_lang["error_email_incorrect"] = "Courriel invalide";
$install_lang["error_empty_agree"] = "Confirmation vide";
$install_lang["error_empty_answer"] = "Réponse vide";
$install_lang["error_empty_email"] = "Courriel vide";
$install_lang["error_empty_message"] = "Commentaire vide";
$install_lang["error_empty_object"] = "Objet vide";
$install_lang["error_empty_poster"] = "Propriétaire d'évaluation invalide";
$install_lang["error_empty_rating"] = "Marque vide";
$install_lang["error_empty_review_type_value"] = "Valeur de type d'évaluation vide";
$install_lang["error_empty_type"] = "Type d'objet vide";
$install_lang["error_empty_type_gid"] = "GID de type d'objet vide";
$install_lang["error_guest"] = "Connectez ou enregistrez vous pour écrire une évaluation";
$install_lang["error_invalid_type"] = "Type d'objet invalide";
$install_lang["error_is_owner"] = "Vous êtes le propriétaire de cet objet";
$install_lang["error_reply"] = "Vous ne pouvez pas répondre à cette évaluation";
$install_lang["error_review_exists"] = "Vous avez déja répondu à cette évaluation";
$install_lang["field_reviews_agree"] = "J'assure que l'évaluation est honnête et respectueuse";
$install_lang["field_reviews_answer"] = "Répondre";
$install_lang["field_reviews_count"] = "Évaluations";
$install_lang["field_reviews_date_add"] = "Date";
$install_lang["field_reviews_message"] = "Commentaire";
$install_lang["field_reviews_poster"] = "Propriétaire";
$install_lang["field_reviews_rate"] = "Évaluation";
$install_lang["field_reviews_status"] = "Statut";
$install_lang["field_types_date_add"] = "Date ajoutée";
$install_lang["field_types_dop_rate"] = "Évaluation supplémentaire";
$install_lang["field_types_gid"] = "GID";
$install_lang["field_types_main_rate"] = "Évaluation primaire";
$install_lang["field_types_name"] = "Nom de type";
$install_lang["field_types_rate_header"] = "Nom";
$install_lang["field_types_rate_type"] = "Type d'évaluation";
$install_lang["header_reviews"] = "Évaluations";
$install_lang["header_reviews_form"] = "Écrire une évaluation";
$install_lang["header_reviews_found"] = "Évaluations";
$install_lang["header_reviews_list"] = "Trouvé";
$install_lang["link_content_hide"] = "Cacher contenu";
$install_lang["link_content_show"] = "Montrer contenu";
$install_lang["link_reviews_delete"] = "Supprimer évaluation";
$install_lang["link_reviews_edit"] = "Éditer évaluation";
$install_lang["link_reviews_show"] = "Voir évaluation";
$install_lang["link_send_first"] = "Be the first to write a review!";
$install_lang["link_send_review"] = "Envoyer évaluation";
$install_lang["link_types_dop_rate_delete"] = "Supprimer évaluation";
$install_lang["link_types_edit"] = "Éditer paramètres de notage";
$install_lang["no_reviews"] = "Aucune évaluations";
$install_lang["no_types"] = "Aucun contenu";
$install_lang["note_reviews_delete"] = "Êtes vous sur de vouloir supprimer cette évaluation?";
$install_lang["note_reviews_delete_all"] = "Êtes vous sur de vouloir supprimer ces évaluations?";
$install_lang["note_types_dop_rate_delete"] = "Êtes vous sur de vouloir supprimer cette note?";
$install_lang["note_types_translate"] = "Traduire texte";
$install_lang["settings_alert_email"] = "Courriel";
$install_lang["settings_alert_name"] = "Nom";
$install_lang["settings_use_alerts"] = "Envoyer alertes";
$install_lang["stat_header_reviews"] = "Évaluations";
$install_lang["stat_header_reviews_listings_object"] = "Annonces";
$install_lang["stat_header_reviews_users_object"] = "Utilisateurs";
$install_lang["stat_header_spam_reviews"] = "Évaluations";
$install_lang["stat_reviews_visitors_all"] = "Évaluations de mon contenu";
$install_lang["stat_reviews_visitors_listings_object"] = "Évaluations de mes annonces";
$install_lang["stat_reviews_visitors_users_object"] = "Évaluations de mon profil";
$install_lang["stat_reviews_visits_all"] = "Mes évaluations";
$install_lang["stat_reviews_visits_listings_object"] = "J'évalue les annonces des utilisateurs";
$install_lang["stat_reviews_visits_users_object"] = "J'évalue les profils des utilisateurs";
$install_lang["success_reviews_created"] = "Cette évaluation est ajouté";
$install_lang["success_reviews_deleted"] = "Cette évaluation est supprimée";
$install_lang["success_reviews_replied"] = "Cette évaluations est sauvée";
$install_lang["success_reviews_updated"] = "Cette évaluation est changée";
$install_lang["success_settings_saved"] = "Paramètres sauvés";
$install_lang["success_types_updated"] = "Type de contenu changé";
$install_lang["text_content_banned"] = "Contenu banni";
$install_lang["text_reviews_visitors"] = "Visiter mes évaluations";
$install_lang["text_reviews_visits"] = "Mes évaluations de visite";
$install_lang["text_send_review"] = "Vous avez déja écrit une évaluation";


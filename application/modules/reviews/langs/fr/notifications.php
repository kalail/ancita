<?php

$install_lang["notification_auser_reviews_object"] = "Nouvelle évaluation (pour admins)";
$install_lang["notification_auser_reviews_reply"] = "Commentaires d'évaluation (pour admins)";
$install_lang["notification_user_reviews_object"] = "Nouvelle évaluation";
$install_lang["notification_user_reviews_reply"] = "Commentaires d'évaluation";
$install_lang["tpl_auser_reviews_object_content"] = "Bonjour admin,\n\nIl a une nouvelle évaluation sur [domain]. Accédez au panneau admin > commentares > évaluations et cherchez dans les filières \"Utilisateurs\" et \"Annonces\".\n\nCordialement,\n[name_from]";
$install_lang["tpl_auser_reviews_object_subject"] = "[domain] | Nouvelle évaluation";
$install_lang["tpl_auser_reviews_reply_content"] = "Bonjour admin,\n\nIl a un commentaire qui nécessite une évaluation.\n\nÉvaluer: [review]\n\nCommentaire: [comment]\n\nCordialement,\n[name_from]";
$install_lang["tpl_auser_reviews_reply_subject"] = "[domain] | Commentaire à évaluer";
$install_lang["tpl_user_reviews_object_content"] = "Bonjour [user],\n\nIl a une nouvelle évaluation.\n\Évaluation de: [poster]\n\nMessage: [review]\n\nCordialement,\n[name_from]";
$install_lang["tpl_user_reviews_object_subject"] = "[domain] | Nouvelle évaluation de [type] (ID=[object_id])";
$install_lang["tpl_user_reviews_reply_content"] = "Bonjour [user],\n\nIl a un nouveau commentaire sur votre évaluation.\n\nVotre évaluation: [review]\nCommentaire: [comment]\n\nCordialement,\n[name_from]";
$install_lang["tpl_user_reviews_reply_subject"] = "[domain] | Commentaire sur votre évaluation";


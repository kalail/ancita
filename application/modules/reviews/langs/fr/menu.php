<?php

$install_lang["admin_menu_feedbacks-items_reviews_list_menu_item"] = "Évaluations";
$install_lang["admin_menu_feedbacks-items_reviews_list_menu_item_tooltip"] = "Préparer évaluations d'annonces et profils";
$install_lang["admin_menu_system-items_reviews_sett_menu_item"] = "Évaluations";
$install_lang["admin_menu_system-items_reviews_sett_menu_item_tooltip"] = "Préparer évaluations d'annonces et profils";
$install_lang["admin_reviews_menu_rws_objects_item"] = "Liste d'évaluations";
$install_lang["admin_reviews_menu_rws_objects_item_tooltip"] = "";
$install_lang["admin_reviews_menu_rws_settings_item"] = "Paramètres";
$install_lang["admin_reviews_menu_rws_settings_item_tooltip"] = "";
$install_lang["admin_reviews_menu_rws_types_item"] = "Types";
$install_lang["admin_reviews_menu_rws_types_item_tooltip"] = "";


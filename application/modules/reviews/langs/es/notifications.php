<?php

$install_lang["notification_auser_reviews_object"] = "Nueva revisión (para administradores)";
$install_lang["notification_auser_reviews_reply"] = "Comentario a revisar (para administradores)";
$install_lang["notification_user_reviews_object"] = "Nuevo comentario";
$install_lang["notification_user_reviews_reply"] = "Comentario a revisar";
$install_lang["tpl_auser_reviews_object_content"] = "Hola admin,\n\nHay una nueva reseña en [domain] . Acceso panel de administración > Restroalimentación > Comentarios y navegar pestañas \"Usuarios\" y \"fichas\" anuncios para visualizarla.\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_auser_reviews_object_subject"] = "[domain] | Nueva reseña";
$install_lang["tpl_auser_reviews_reply_content"] = "Hola admin,\n\nHay un comentario a revisar.\n\nReseña: [review]\n\nComentario: [comment]\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_auser_reviews_reply_subject"] = "[domain] | Comentario a revisar";
$install_lang["tpl_user_reviews_object_content"] = "Hola [user],\n\nThere is a new review.\n\nReview from: [poster]\n\nMessage: [review]\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_user_reviews_object_subject"] = "[domain] | Nueva resña de [type] (ID=[object_id])";
$install_lang["tpl_user_reviews_reply_content"] = "Hola [user],\n\nHay un comentario para su revisión.\n\nSu reseña: [review]\nComentario: [comment]\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_user_reviews_reply_subject"] = "[domain] | Comentar a su reseña";


<?php

$install_lang["admin_menu_feedbacks-items_reviews_list_menu_item"] = "Reseñas";
$install_lang["admin_menu_feedbacks-items_reviews_list_menu_item_tooltip"] = "Configuración de las reseñas de los listados y los perfiles";
$install_lang["admin_menu_system-items_reviews_sett_menu_item"] = "Reseñas";
$install_lang["admin_menu_system-items_reviews_sett_menu_item_tooltip"] = "Configuración de las reseñas de los listados y los perfiles";
$install_lang["admin_reviews_menu_rws_objects_item"] = "Lista de reseñas";
$install_lang["admin_reviews_menu_rws_objects_item_tooltip"] = "";
$install_lang["admin_reviews_menu_rws_settings_item"] = "Configuración";
$install_lang["admin_reviews_menu_rws_settings_item_tooltip"] = "";
$install_lang["admin_reviews_menu_rws_types_item"] = "Tipos";
$install_lang["admin_reviews_menu_rws_types_item_tooltip"] = "";


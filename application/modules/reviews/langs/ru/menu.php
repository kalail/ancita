<?php

$install_lang["admin_menu_feedbacks-items_reviews_list_menu_item"] = "Отзывы";
$install_lang["admin_menu_feedbacks-items_reviews_list_menu_item_tooltip"] = "Настройка отзывов для объявлений и пользователей";
$install_lang["admin_menu_system-items_reviews_sett_menu_item"] = "Отзывы";
$install_lang["admin_menu_system-items_reviews_sett_menu_item_tooltip"] = "Настройка отзывов для объявлений и пользователей";
$install_lang["admin_reviews_menu_rws_objects_item"] = "Список отзывов";
$install_lang["admin_reviews_menu_rws_objects_item_tooltip"] = "";
$install_lang["admin_reviews_menu_rws_settings_item"] = "Настройки";
$install_lang["admin_reviews_menu_rws_settings_item_tooltip"] = "";
$install_lang["admin_reviews_menu_rws_types_item"] = "Типы";
$install_lang["admin_reviews_menu_rws_types_item_tooltip"] = "";


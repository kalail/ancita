<?php

$install_lang["notification_auser_reviews_object"] = "Neue Bewertungen (für Administratoren)";
$install_lang["notification_auser_reviews_reply"] = "Kommentar zu Bewertung (für Administratoren)";
$install_lang["notification_user_reviews_object"] = "Neue Bewertung";
$install_lang["notification_user_reviews_reply"] = "Kommentar zu Bewertung";
$install_lang["tpl_auser_reviews_object_content"] = "Hallo admin,\n\nEs gibt eine neue Bewertung zu [domain]. Gehen Sie in den Administrationsbereich> Bewertungen> Bewertungen und durchsuchen \"Benutzer\" und \"Listings\" Registerkarten, um es anzuzeigen.\n\nBest regards,\n[name_from]";
$install_lang["tpl_auser_reviews_object_subject"] = "[domain] | Neue Bewertung";
$install_lang["tpl_auser_reviews_reply_content"] = "Hallo admin,\n\nEs gibt einen Kommentar zu bewerten.\n\nBewertung: [review]\n\nKommentar: [comment]\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_auser_reviews_reply_subject"] = "[domain] | Kommentar überprüfen";
$install_lang["tpl_user_reviews_object_content"] = "Hallo [user],\n\nEs gibt eine neue Bewertung.\n\nBewertung von: [poster]\n\nNachricht: [review]\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_user_reviews_object_subject"] = "[domain] | Neue Bewertung von [type] (ID=[object_id])";
$install_lang["tpl_user_reviews_reply_content"] = "Hallo [user],\n\nEs gibt einen Kommentar zu Ihrer Bewertung.\n\nIhre Beurteilung: [review]\nKommentar: [comment]\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_user_reviews_reply_subject"] = "[domain] | Kommentar zu Ihrer Bewertung";


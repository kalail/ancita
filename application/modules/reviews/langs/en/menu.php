<?php

$install_lang["admin_menu_feedbacks-items_reviews_list_menu_item"] = "Reviews";
$install_lang["admin_menu_feedbacks-items_reviews_list_menu_item_tooltip"] = "Set up reviews of listings and profiles";
$install_lang["admin_menu_system-items_reviews_sett_menu_item"] = "Reviews";
$install_lang["admin_menu_system-items_reviews_sett_menu_item_tooltip"] = "Set up reviews of listings and profiles";
$install_lang["admin_reviews_menu_rws_objects_item"] = "Reviews list";
$install_lang["admin_reviews_menu_rws_objects_item_tooltip"] = "";
$install_lang["admin_reviews_menu_rws_settings_item"] = "Settings";
$install_lang["admin_reviews_menu_rws_settings_item_tooltip"] = "";
$install_lang["admin_reviews_menu_rws_types_item"] = "Types";
$install_lang["admin_reviews_menu_rws_types_item_tooltip"] = "";


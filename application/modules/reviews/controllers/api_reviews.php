<?php 

/**
 * Reviews api side controller
 * 
 * @package PG_RealEstate
 * @subpackage Reviews
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Api_Reviews extends Controller{
	
	/**
	 * Constructor
	 *
	 * @return Api_Reviews
	 */
	public function __construct(){
		parent::Controller();
	}
	
	/**
	 * Send review on object
	 * 
	 * @return void
	 */
	public function send_review(){
		$this->load->model("Reviews_model");
		
		if($this->session->userdata("auth_type") != "user"){
			$this->set_api_content("errors", l('error_guest', 'reviews'));
			return;
		}
				
		$post_data = $this->input->post("data");
		$id = intval($post_data['id']);
		$post_data["id_object"] = intval($this->input->post("object_id"));
		$post_data["gid_type"] = $this->input->post("type_gid");
		$post_data["id_poster"] = intval($this->session->userdata("user_id"));
		$post_data["rating_data"] = $this->input->post("rating_data");

		$validate_data = $this->Reviews_model->validate_review($id, $post_data);		
		if(!empty($validate_data["errors"])){
			$this->set_api_content("errors", implode("<br>", $validate_data["errors"]));
			return;
		}else{
			if(isset($validate_data["id"])){
				$this->Reviews_model->save_review($id, $validate_data["data"]);
				$this->set_api_content("messages", l("success_reviews_updated", "reviews"));
			}else{
				$id = $this->Reviews_model->save_review(null, $validate_data["data"]);
			
				$this->load->model('Users_model');
				$this->Users_model->set_format_settings('get_safe', false);
				$review = $this->Reviews_model->get_review_by_id($id, true);
				$this->Users_model->set_format_settings('get_safe', true);
			
				//send notification
				$this->load->model("Notifications_model");

				$data = array(
					"user" 		=> $review["responder"]["output_name"],
					"poster" 	=> $review["poster"]["output_name"],
					"type" 	 	=> $review["type"]["output_name"],
					"object_id" => $review["id_object"],
					"review" 	=> $review["message"],
				);
				$this->Notifications_model->send_notification($review['responder']['email'], $this->Reviews_model->notification_user_review_gid, $data, '', $review['responder']['lang_id']);
				
				if($this->pg_module->get_module_config('reviews', 'reviews_use_alerts')){
					$email = $this->pg_module->get_module_config('reviews', 'reviews_alert_email');				
					$data = array(
						"user" 		=> $review["responder"]["output_name"],	
						"poster" 	=> $review["poster"]["output_name"],
						"type" 	 	=> $review["type"]["output_name"],
						"object_id" => $review["id_object"],
						"review" 	=> $review["message"],
					);				
					$this->Notifications_model->send_notification($email, $this->Reviews_model->notification_auser_review_gid, $data);
				}
			
				$this->set_api_content("messages", l("success_reviews_created", "reviews"));
			}
			return;
		}
	}
	
	/**
	 * Reply to review
	 * 
	 * @param integer $review_id review identifier
	 * @return void
	 */
	public function reply($review_id){
		$this->load->model("Reviews_model");
		
		$this->Reviews_model->set_format_settings(array('use_format'=>false));
		$review = $this->Reviews_model->get_review_by_id($review_id);
		$this->Reviews_model->set_format_settings(array('use_format'=>true));
		
		$user_id = $this->session->userdata("user_id", true);
		if($review["id_responder"] != $user_id){
			$return["error"] = l('error_reply', 'reviews');
			exit;
		}
	
		$post_data = $this->input->post("data", true);
		$post_data["date_answer"] = date("Y-m-d H:i:s");
		$validate_data = $this->Reviews_model->validate_reply($review_id, $post_data);
		if(!empty($validate_data["errors"])){
			$this->set_api_content("errors", implode("<br>", $validate_data["errors"]));
			exit;
		}else{
			$this->Reviews_model->save_review($review_id, $validate_data["data"]);
			
			$this->load->model('Users_model');
			$this->Users_model->set_format_settings('get_safe', false);
			$review = $this->Reviews_model->get_review_by_id($review_id);
			$this->Users_model->set_format_settings('get_safe', true);
			
			$this->load->model('Notifications_model');
			
			$data = array(
				'user' 		=> $review['poster']['output_name'],
				'responder' => $review['responder']['output_name'],
				'type' 	 	=> $review['type']['output_name'],
				'object_id' => $review['id_object'],
				'review' 	=> $review['message'],
				'comment'	=> $review['answer'],
			);
			$this->Notifications_model->send_notification($review['poster']['email'], $this->Reviews_model->notification_user_reply_gid, $data, '', $review['poster']['lang_id']);
			
			if($this->pg_module->get_module_config('reviews', 'reviews_use_alerts')){
				$email = $this->pg_module->get_module_config('reviews', 'reviews_alert_email');				
				$data = array(
					'responder' => $review['responder']['output_name'],
					'type' 	 	=> $review['type']['output_name'],
					'object_id' => $review['id_object'],
					'review' 	=> $review['message'],
					'comment'	=> $review['answer'],
				);				
				$this->Notifications_model->send_notification($email, $this->Reviews_model->notification_auser_reply_gid, $data);
			}
			
			$this->load->helper('date_format');
			
			$date_format = $this->pg_date->get_format('date_time_literal', 'st');
			$return["answer"] = $validate_data["data"]["answer"];
			$return["date_answer"] = tpl_date_format($post_data["date_answer"], $date_format);
			$this->set_api_content("messages", l("success_reviews_replied", "reviews"));			
			return;
		}
	}
	
	/**
	 * Get reviews data
	 * 
	 * @param string $type_gid type GUID
	 * @param integer $object_id object identifier
	 * @param string $order sorting order
	 * @param string $order_direction sorting direction
	 * @param integer $page page of results
	 * @return void
	 */
	public function get_reviews($type_gid=null, $object_id=null, $order="date_add", $order_direction="DESC", $page=1){
		$return = array("reviews_count"=>0, "reviews"=>null);
		
		if(!$type_gid) $type_gid = $this->input->get("type_gid");
		if(!$object_id) $object_id = intval($this->input->get("object_id"));
		
		$attrs = array();
		
		$current_settings = isset($_SESSION["reviews_list"]) ? $_SESSION["reviews_list"] : array();
				
		if(!isset($current_settings["order"]))
			$current_settings["order"] = "date_add";
		if(!isset($current_settings["order_direction"]))
			$current_settings["order_direction"] = "DESC";
		if (!isset($current_settings["page"]))
			$current_settings["page"] = 1;

		if (!$order) $order = $current_settings["order"];
		$current_settings["order"] = $order;

		if (!$order_direction) $order_direction = $current_settings["order_direction"];
		$current_settings["order_direction"] = $order_direction;

		$this->load->model('Reviews_model');

		$return["reviews_count"] = $this->Reviews_model->get_reviews_count($attrs);
		
		if(!$page) $page = $current_settings["page"];
		$items_on_page = $this->pg_module->get_module_config("start", "index_items_per_page");
	
		$order = isset($data["order"]) ? $data["order"] : "date_add";
		$order_derection = isset($data["order_direction"]) ? $data["order_direction"] : "DESC";
	
		$this->load->helper("sort_order");
		$page = get_exists_page_number($page, $return["reviews_count"], $items_on_page);
		$current_settings["page"] = $page;

		$_SESSION["reviews_list"] = $current_settings;
		
		if($return["reviews_count"] > 0){
			if($order == "poster") $order = "id_poster";
			$return["reviews"] = $this->Reviews_model->get_reviews_list($page, $items_on_page, array($order => $order_direction), $attrs);
		}
		
		$this->set_api_content('data', $return);

		return;
	}
}

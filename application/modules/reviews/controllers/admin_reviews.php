<?php

/**
 * Reviews admin side controller
 * 
 * @package PG_RealEstate
 * @subpackage Reviews
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Admin_Reviews extends Controller{

	/**
	 * Constructor
	 * 
	 * @return Admin_Reviews
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model("Menu_model");
		
		$this->system_messages->set_data("header", l("admin_header_reviews", "reviews"));
		$this->Menu_model->set_menu_active_item("admin_menu", "system-items");
	}

	/**
	 * Reviews on object management
	 * 
	 * @param string $type_gid reviews type GUID
	 * @param string $object_id object identifier
	 * @param string $order sorting order
	 * @param string $order_direction sorting direction
	 * @param integer $page page of results
	 * @return void
	 */
	public function index($type_gid='', $object_id=0, $order=null, $order_direction=null, $page=null){
		$this->load->model("Reviews_model");
		$this->load->model("reviews/models/Reviews_type_model");
		$type_gid = strval($type_gid); $object_id = intval($object_id);
		
		$base_types = $this->Reviews_type_model->get_types();
		foreach($base_types as $type){
			if(!$type_gid) $type_gid = $type['gid'];
			$types[$type['gid']] = $type['output_name'];
		}
		$this->template_lite->assign("types", $types);
		
		$attrs = array("type"=>strval($type_gid), "object"=>intval($object_id));
		
		$current_settings = isset($_SESSION["reviews_list"]) ? $_SESSION["reviews_list"] : array();
				
		$current_settings["type_gid"] = $attrs['type'];
		$current_settings["object_id"] = $attrs['object'];
				
		if(!isset($current_settings["order"])) $current_settings["order"] = "date_add";
		if(!isset($current_settings["order_direction"])) $current_settings["order_direction"] = "DESC";
		if(!isset($current_settings["page"])) $current_settings["page"] = 1;		
		
		if(!$order) $order = $current_settings["order"];
		$this->template_lite->assign("order", $order);
		$current_settings["order"] = $order;

		if (!$order_direction) $order_direction = $current_settings["order_direction"];
		$this->template_lite->assign("order_direction", $order_direction);
		$current_settings["order_direction"] = $order_direction;

		$reviews_count = $this->Reviews_model->get_reviews_count_by_filters($attrs);
		
		if(!$page) $page = $current_settings["page"];
		$items_on_page = $this->pg_module->get_module_config("start", "admin_items_per_page");
			
		$this->load->helper("sort_order");
		$page = get_exists_page_number($page, $reviews_count, $items_on_page);
		$current_settings["page"] = $page;

		$_SESSION["reviews_list"] = $current_settings;

		$sort_link_prefix = site_url()."admin/reviews/index/".$type_gid."/".$object_id;
		$sort_links = array(
			"date_add" => $sort_link_prefix."/date_add/".(($order != "date_add" xor $order_direction == "DESC") ? "ASC" : "DESC"),
		);
		
		$this->template_lite->assign("sort_links", $sort_links);

		if($reviews_count > 0){
			$reviews = $this->Reviews_model->get_reviews_list_by_filters($attrs, $page, $items_on_page, array($order => $order_direction));
			$this->template_lite->assign("reviews", $reviews);
		}
		$this->load->helper("navigation");
		
		$url = site_url()."admin/reviews/index/".$type_gid."/".$object_id."/".$order."/".$order_direction."/";
		$page_data = get_admin_pages_data($url, $reviews_count, $items_on_page, $page, "briefPage");
		$page_data["date_format"] = $this->pg_date->get_format('date_time_literal', 'st');
		$this->template_lite->assign("page_data", $page_data);
		
		$this->template_lite->assign("type_gid", $type_gid);
		$this->template_lite->assign("object_id", $object_id);
		
		$this->Menu_model->set_menu_active_item('admin_reviews_menu', 'rws_objects_item');
		$this->Menu_model->set_menu_active_item("admin_menu", "feedbacks-items");
		$this->template_lite->view("reviews_list");
	}

	/**
	 * View review data
	 * 
	 * @param integer $id review identifier
	 * @return void
	 */
	public function show($id){
		$this->load->model("Reviews_model");
		$data = $this->Reviews_model->get_review_by_id($id, true);
		$this->template_lite->assign("data", $data);
		
		$this->load->helper("navigation");
		
		$date_format = $this->pg_date->get_format('date_time_literal', 'st');
		$this->template_lite->assign("date_format", $date_format);
		
		$this->Menu_model->set_menu_active_item('admin_reviews_menu', 'rws_objects_item');
		$this->Menu_model->set_menu_active_item("admin_menu", "feedbacks_items");
		$this->template_lite->view("reviews_view");
	}
	
	/**
	 * Edit review data
	 * 
	 * @param integer $id review identifier
	 * @return void
	 */
	public function edit($id){
		$this->load->model("Reviews_model");
		$data = $this->Reviews_model->get_review_by_id($id, true);
		
		if($this->input->post("btn_save")){
			$post_data = $this->input->post("data", true);
			$post_data["rating_data"] = $this->input->post("rating_data");			
			$validate_data = $this->Reviews_model->validate_review($id, $post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
			}else{
				$this->Reviews_model->save_review($id, $validate_data["data"]);
				$this->system_messages->add_message("success", l("success_reviews_updated", "reviews"));
				$current_settings = $_SESSION["reviews_list"];
				$url = site_url()."admin/reviews/index/".$current_settings["type_gid"]."/".$current_settings["object_id"];
				redirect($url);
			}
			$data = array_merge($data, $post_data);
		}
		
		$this->template_lite->assign("data", $data);
		
		$this->load->helper("navigation");
		
		$date_format = $this->pg_date->get_format('date_time_literal', 'st');
		$this->template_lite->assign("date_format", $date_format);
		
		$this->Menu_model->set_menu_active_item('admin_reviews_menu', 'rws_objects_item');
		$this->Menu_model->set_menu_active_item("admin_menu", "feedbacks_items");
		$this->template_lite->view("reviews_edit");
	}

	/**
	 * Remove review
	 * 
	 * @param integer/array $ids review identifiers
	 * @return void
	 */
	public function delete($ids=null){
		$errors = false;
		$messages = array();
		if(!$ids) $ids = $this->input->post("ids");
		if(!empty($ids)){
			$this->load->model("Reviews_model");
			foreach((array)$ids as $id){
				$error = $this->Reviews_model->delete_review($id);
				if($error){
					$errors = true;
					$messages[] = $error;
				}else{
					$messages[] = l("success_reviews_deleted", "reviews");
				}
			}
			if($errors){
				$this->system_messages->add_message("error", implode("<br>", $messages));				
			}else{
				$this->system_messages->add_message("success", implode("<br>", $messages));
			}
		}
		$current_settings = $_SESSION["reviews_list"];
		$url = site_url()."admin/reviews/index/".$current_settings["type_gid"]."/".$current_settings["object_id"];
		redirect($url);
	}
		
//approval review
    public function order_approve($order_id){
		$this->load->model("Reviews_model");
		$this->Reviews_model->approve_review($order_id);
		if(!empty($order_id)){
			$this->system_messages->add_message('success', l('success_review_approved', 'reviews'));
		}
		$review = $this->Reviews_model->get_review_by_id($order_id, true);
		//mail admin
		$data = array(
				"user" 		=> $review["responder"]["output_name"],
				"poster" 	=> $review["rname"],
				"type" 	 	=> $review["type"]["output_name"],
				"email" => $review["remail"],
				"review" 	=> $review["message"],
			);
           $adminemail = $this->pg_module->get_module_config('reviews', 'reviews_alert_email');	
           $this->load->model("Notifications_model");
		   $this->Notifications_model->send_notification($adminemail, $this->Reviews_model->notification_auser_review_gid, $data, '', $this->pg_language->current_lang_id);
		   //mail agent
   $this->Notifications_model->send_notification($review['responder']['contact_email'], $this->Reviews_model->notification_user_review_gid, $data, '', $this->pg_language->current_lang_id);
		  if($type == 'Users'){
		  redirect(site_url().'admin/reviews/index/users_object');
		  }else{
			redirect(site_url().'admin/reviews/index/listings_object');
		  }
	}
	/**
	 * Reviews types management
	 * 
	 * @param string $order sorting order
	 * @param string $order_direction sorting direction
	 * @param integer $page page of results
	 * @return void
	 */
	public function types($order="date_add", $order_direction="DESC", $page=1){
		$this->load->model("reviews/models/Reviews_type_model");
		
		$types = $this->Reviews_type_model->get_types();		
		$this->template_lite->assign("types", $types);
	
		$date_format = $this->pg_date->get_format('date_time_literal', 'st');
		$this->template_lite->assign("date_format", $date_format);

		$this->Menu_model->set_menu_active_item('admin_reviews_menu', 'rws_types_item');
		$this->template_lite->view("types_list");
	}
	
	//questions management
	//questions list page
	public function questions(){
		$this->load->model("reviews/models/Reviews_type_model");
		$questions = $this->Reviews_type_model->get_questions();	
		$this->template_lite->assign("questions", $questions);
		$this->Menu_model->set_menu_active_item('admin_reviews_menu', 'rwsquestions_item');
		$this->Menu_model->set_menu_active_item("admin_menu", "feedbacks-items");
		$this->template_lite->view('questions_list');
	}
	//questions edit/update page
    public function question_edit($question_id){
		 $this->load->model("reviews/models/Reviews_type_model");		
		 $data = $this->Reviews_type_model->get_question($question_id, true);
	   	 if($this->input->post("btn_save")){
			$post_data = $this->input->post("data", true);
			$this->Reviews_type_model->save_question($question_id, $post_data );
			if($ajax){
			return;
		    }
		    $this->system_messages->add_message("success", l("success_reviews_updated", "reviews"));
			$url = site_url()."admin/reviews/questions/";
		    redirect($url);
		   }
		$this->template_lite->assign("data", $data);		
		$this->Menu_model->set_menu_active_item('admin_reviews_menu', 'rwsquestions_item');
		$this->Menu_model->set_menu_active_item("admin_menu", "feedbacks-items");
		$this->template_lite->view("questions_edit");
	}
	//questions delete page
	 public function quesion_delete($ids=null){
		if(!$ids) $ids = $this->input->post("ids");
		if(!empty($ids)){
			$this->load->model("reviews/models/Reviews_type_model");
			foreach((array)$ids as $id){
				 $this->Reviews_type_model->delete_question($id);
				}
		$this->system_messages->add_message("success", l("success_reviews_updated", "reviews"));
		$url = site_url()."admin/reviews/questions/";
		redirect($url);
			  }
		}
	/**
	 * Edit data of review type
	 * 
	 * @param integer $type_id review type identifier
	 * @param string $rate_type rate type
	 * @return void
	 */
	public function types_edit($type_id, $rate_type=''){
		$this->load->model("reviews/models/Reviews_type_model");
				
		$data = $this->Reviews_type_model->get_type($type_id, true);
		if($rate_type) $data['rate_type'] = $rate_type;
	
		if($this->input->post("btn_save")){

			$post_data = $this->input->post("data", true);
			$data = array_merge($data, $post_data);
			
			$rate_type = $data["rate_type"];
			$ratings = $this->input->post("ratings", true);
			$rates_data = $this->input->post("rate", true);
	
			if(!empty($rate_type)){
				$rating_type_settings = $this->Reviews_type_model->rating_type_settings[$rate_type];
				foreach($this->pg_language->languages as $lang_id=>$lang){
					foreach($ratings as $rating){			
						unset($data["values_".$lang_id][$rate_type][$rating]);
						$data["values_".$lang_id][$rate_type][$rating]["header"] = $rates_data[$rating]['header'][$lang_id];
						foreach($rating_type_settings['answers'] as $answer_id){
							$data["values_".$lang_id][$rate_type][$rating]["votes"][$answer_id] = $rates_data[$rating]['votes'][$answer_id][$lang_id];
						}
					}
				}
			}

			$validate_data = $this->Reviews_type_model->validate_type($type_id, $data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", implode("<br>", $validate_data["errors"]));
			}else{
				$this->Reviews_type_model->save_type($type_id, $validate_data["data"]);
				if($ajax){
					return;
				}
				$this->system_messages->add_message("success", l("success_types_updated", "reviews"));
				//$url = site_url()."admin/reviews/types";
				//redirect($url);
			}
		}

		$this->template_lite->assign("data", $data);		
		$this->template_lite->assign("rate_types_settings", $this->Reviews_type_model->format_rating_type_settings());
		
		$date_format = $this->pg_date->get_format('date_time_literal', 'st');
		$this->template_lite->assign("date_format", $date_format);
		
		$this->template_lite->assign("current_lang_id", $this->pg_language->current_lang_id);
		$this->template_lite->assign("langs", $this->pg_language->languages);
		
		$this->Menu_model->set_menu_active_item('admin_reviews_menu', 'rws_types_item');
		$this->template_lite->view("types_edit");
	}
	
	/**
	 * Remove custom rates by ajax
	 * 
	 * @param integer $type_id review tyoe identifier
	 * @param string $dop_rate_type custom review type
	 * @return void
	 */
	public function ajax_types_rate_delete($type_id, $dop_rate_type){
		$type_id = intval($type_id);
		$dop_rate_type = trim(strip_tags($dop_rate_type));
		if(empty($type_id) || empty($dop_rate_type)) return false;
		
		$this->load->model("reviews/models/Reviews_type_model");
		$type = $this->Reviews_type_model->get_type($type_id, true);
		$rate_types = array_keys($this->Reviews_type_model->rating_type_settings);

		foreach($this->pg_language->languages as $lang_id=>$lang){
			foreach($rate_types as $rtype){
				if(isset($type["values_".$lang_id][$rtype][$dop_rate_type])){
					unset($type["values_".$lang_id][$rtype][$dop_rate_type]);
				}
			}
		}
		$validate_data = $this->Reviews_type_model->validate_type($type_id, $type);
		if(empty($validate_data['errors'])){
			$this->Reviews_type_model->save_type($type_id, $validate_data['data']);
		}
		return;
	}
	
	/**
	 * Settings of reviews module
	 * 
	 * @return void
	 */
	public function settings(){
		$data = array(
			'reviews_alert_email' => $this->pg_module->get_module_config('reviews', 'reviews_alert_email'),
			'reviews_use_alerts' => intval($this->pg_module->get_module_config('reviews', 'reviews_use_alerts')),
		);
		if ($this->input->post('btn_save')) {
			$post_data = array(
				'reviews_alert_email' => $this->input->post('reviews_alert_email', true),
				'reviews_use_alerts' => intval($this->input->post('reviews_use_alerts', true)),
			);
			$this->load->model("reviews/models/Reviews_type_model");
			$validate_data = $this->Reviews_type_model->validate_settings($post_data);
			if(!empty($validate_data["errors"])){
				$this->system_messages->add_message("error", $validate_data["errors"]);
			}else{
				foreach ($validate_data['data'] as $setting => $value) {
					$this->pg_module->set_module_config('reviews', $setting, $value);
				}
				$this->system_messages->add_message('success', l('success_settings_saved', 'reviews'));
			}
			$data = $validate_data["data"];
		}

		$this->template_lite->assign('settings_data', $data);

		$this->Menu_model->set_menu_active_item('admin_reviews_menu', 'rws_settings_item');
		$this->Menu_model->set_menu_active_item("admin_menu", "feedbacks-items");
		$this->template_lite->view('settings');
	}
}

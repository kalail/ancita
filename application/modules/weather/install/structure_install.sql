DROP TABLE IF EXISTS `[prefix]weather_drivers`;
CREATE TABLE IF NOT EXISTS `[prefix]weather_drivers` (
  `id` int(3) NOT NULL auto_increment,
  `gid` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `regkey` varchar(100) NOT NULL,
  `need_regkey` tinyint(1) NOT NULL,
  `link` varchar(100) NOT NULL,
  `auto_locations` tinyint(1) NOT NULL,
  `editable` tinyint(1) NOT NULL,
  `settings` text NULL,
  `date_add` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gid` (`gid`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `[prefix]weather_drivers` VALUES (1, 'custom_informers', 1, '', 0, '', 0, 0, '', '2011-03-21 15:26:54', '2011-03-21 15:26:54');

DROP TABLE IF EXISTS `[prefix]weather_locations`;
CREATE TABLE IF NOT EXISTS `[prefix]weather_locations` (
	`id` int(3) NOT NULL auto_increment,
	`gid_driver` varchar(30) NOT NULL,
	`id_country` char(2) NOT NULL,
	`id_region` int(3) NOT NULL,
	`id_city` int(3) NOT NULL,
	`lat` decimal(10,7) NOT NULL,
	`lon` decimal(10,7) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE `gid_driver` (`gid_driver`, `id_country`, `id_region`, `id_city`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

<?php
return array(
	array(
		'gid_driver' => 'custom_informers', 
		'id_country' => 'US', 
		'id_region' => 5, 
		'id_city' => 5, 
		'lat' => '40.7142700', 
		'lon' => '-74.0059700', 
		'code' => '<div style="line-height: 10px;background:none;"><div style="max-width: 160px; width: 160px; background:none;"><object style="margin:0;padding:0;" type="application/x-shockwave-flash" data="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" 	id="w4aaa9c4e6c122b4c4af9ecec7aeeb33a" height="272" width="160">	<param value="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" name="movie"/>	<param value="transparent" name="wmode">	<param value="station_id=KNYC&city_name=New York&language=en&use_celsius=Yes&skinName=LightBlue&PID=166707&ts=201404150139&hideChangeSkin=No" name="flashvars">	<param value="all" name="allowNetworking">	<param value="always" name="allowScriptAccess"></object><a alt="Hotels Combined" title="Hotels Combined" style="margin:0; padding:0; text-decoration: none; background: none;" target="_blank" href="http://widgets.hotelscombined.com/City/Weather/New_York.htm?use_celsius=Yes"><div style="background: none; color: white; text-align: center; width: 160px; height: 17px; margin: 0px 0 0 0; padding: 5px 0 0 0; cursor:pointer; background: transparent url(http://static.hotelscombined.com.s3.amazonaws.com/Pages/WeatherWidget/Images/weather_light_blue_bottom.png) no-repeat; font-size: 12px; font-family: Arial,sans-serif; line-height: 12px; font-weight: bold;">See 10-Day Forecast</div></a><div style="text-align: center; width: 160px;"><a alt="Hotels Combined" title="Hotels Combined" style="background:none;font-family:Arial,sans-serif; font-size: 9px; color: #777777;" rel="nofollow" href="http://www.hotelscombined.com">&copy; HotelsCombined.com</a></div></div></div>', 
	),
	array(
		'gid_driver' => 'custom_informers', 
		'id_country' => 'GB', 
		'id_region' => 4, 
		'id_city' => 4, 
		'lat' => '51.5085300', 
		'lon' => '-0.1257400', 
		'code' => '<div style="line-height: 10px;background:none;"><div style="max-width: 160px; width: 160px; background:none;"><object style="margin:0;padding:0;" type="application/x-shockwave-flash" data="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" 	id="w4aaa9c4e6c122b4c4af9ecec7aeeb33a" height="272" width="160">	<param value="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" name="movie"/>	<param value="transparent" name="wmode">	<param value="station_id=EGLC&city_name=London&language=en&use_celsius=Yes&skinName=LightBlue&PID=166708&ts=201404150140&hideChangeSkin=No" name="flashvars">	<param value="all" name="allowNetworking">	<param value="always" name="allowScriptAccess"></object><a alt="Hotels Combined" title="Hotels Combined" style="margin:0; padding:0; text-decoration: none; background: none;" target="_blank" href="http://widgets.hotelscombined.com/City/Weather/London.htm?use_celsius=Yes"><div style="background: none; color: white; text-align: center; width: 160px; height: 17px; margin: 0px 0 0 0; padding: 5px 0 0 0; cursor:pointer; background: transparent url(http://static.hotelscombined.com.s3.amazonaws.com/Pages/WeatherWidget/Images/weather_light_blue_bottom.png) no-repeat; font-size: 12px; font-family: Arial,sans-serif; line-height: 12px; font-weight: bold;">See 10-Day Forecast</div></a><div style="text-align: center; width: 160px;"><a alt="Hotels Combined" title="Hotels Combined" style="background:none;font-family:Arial,sans-serif; font-size: 9px; color: #777777;" rel="nofollow" href="http://www.hotelscombined.com">&copy; HotelsCombined.com</a></div></div></div>', 
	),
	array(
		'gid_driver' => 'custom_informers', 
		'id_country' => 'FR', 
		'id_region' => 1, 
		'id_city' => 1, 
		'lat' => '48.8666670', 
		'lon' => '2.3333330', 
		'code' => '<div style="line-height: 10px;background:none;"><div style="max-width: 160px; width: 160px; background:none;"><object style="margin:0;padding:0;" type="application/x-shockwave-flash" data="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" 	id="w4aaa9c4e6c122b4c4af9ecec7aeeb33a" height="272" width="160">	<param value="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" name="movie"/>	<param value="transparent" name="wmode">	<param value="station_id=LFPO&city_name=Paris&language=en&use_celsius=Yes&skinName=LightBlue&PID=166709&ts=201404150141&hideChangeSkin=No" name="flashvars">	<param value="all" name="allowNetworking">	<param value="always" name="allowScriptAccess"></object><a alt="Hotels Combined" title="Hotels Combined" style="margin:0; padding:0; text-decoration: none; background: none;" target="_blank" href="http://widgets.hotelscombined.com/City/Weather/Paris.htm?use_celsius=Yes"><div style="background: none; color: white; text-align: center; width: 160px; height: 17px; margin: 0px 0 0 0; padding: 5px 0 0 0; cursor:pointer; background: transparent url(http://static.hotelscombined.com.s3.amazonaws.com/Pages/WeatherWidget/Images/weather_light_blue_bottom.png) no-repeat; font-size: 12px; font-family: Arial,sans-serif; line-height: 12px; font-weight: bold;">See 10-Day Forecast</div></a><div style="text-align: center; width: 160px;"><a alt="Hotels Combined" title="Hotels Combined" style="background:none;font-family:Arial,sans-serif; font-size: 9px; color: #777777;" rel="nofollow" href="http://www.hotelscombined.com">&copy; HotelsCombined.com</a></div></div></div>', 
	),
	array(
		'gid_driver' => 'custom_informers', 
		'id_country' => 'DE', 
		'id_region' => 2, 
		'id_city' => 2, 
		'lat' => '52.5166667', 
		'lon' => '13.4000000', 
		'code' => '<div style="line-height: 10px;background:none;"><div style="max-width: 160px; width: 160px; background:none;"><object style="margin:0;padding:0;" type="application/x-shockwave-flash" data="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" 	id="w4aaa9c4e6c122b4c4af9ecec7aeeb33a" height="272" width="160">	<param value="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" name="movie"/>	<param value="transparent" name="wmode">	<param value="station_id=EDDI&city_name=Berlin&language=en&use_celsius=Yes&skinName=LightBlue&PID=166710&ts=201404150141&hideChangeSkin=No" name="flashvars">	<param value="all" name="allowNetworking">	<param value="always" name="allowScriptAccess"></object><a alt="Hotels Combined" title="Hotels Combined" style="margin:0; padding:0; text-decoration: none; background: none;" target="_blank" href="http://widgets.hotelscombined.com/City/Weather/Berlin.htm?use_celsius=Yes"><div style="background: none; color: white; text-align: center; width: 160px; height: 17px; margin: 0px 0 0 0; padding: 5px 0 0 0; cursor:pointer; background: transparent url(http://static.hotelscombined.com.s3.amazonaws.com/Pages/WeatherWidget/Images/weather_light_blue_bottom.png) no-repeat; font-size: 12px; font-family: Arial,sans-serif; line-height: 12px; font-weight: bold;">See 10-Day Forecast</div></a><div style="text-align: center; width: 160px;"><a alt="Hotels Combined" title="Hotels Combined" style="background:none;font-family:Arial,sans-serif; font-size: 9px; color: #777777;" rel="nofollow" href="http://www.hotelscombined.com">&copy; HotelsCombined.com</a></div></div></div>', 
	),
	array(
		'gid_driver' => 'custom_informers', 
		'id_country' => 'ES', 
		'id_region' => 3, 
		'id_city' => 3, 
		'lat' => '40.4165000', 
		'lon' => '-3.7025600', 
		'code' => '<div style="line-height: 10px;background:none;"><div style="max-width: 160px; width: 160px; background:none;"><object style="margin:0;padding:0;" type="application/x-shockwave-flash" data="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" 	id="w4aaa9c4e6c122b4c4af9ecec7aeeb33a" height="272" width="160">	<param value="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" name="movie"/>	<param value="transparent" name="wmode">	<param value="station_id=LEGT&city_name=Madrid&language=en&use_celsius=Yes&skinName=LightBlue&PID=166711&ts=201404150142&hideChangeSkin=No" name="flashvars">	<param value="all" name="allowNetworking">	<param value="always" name="allowScriptAccess"></object><a alt="Hotels Combined" title="Hotels Combined" style="margin:0; padding:0; text-decoration: none; background: none;" target="_blank" href="http://widgets.hotelscombined.com/City/Weather/Madrid.htm?use_celsius=Yes"><div style="background: none; color: white; text-align: center; width: 160px; height: 17px; margin: 0px 0 0 0; padding: 5px 0 0 0; cursor:pointer; background: transparent url(http://static.hotelscombined.com.s3.amazonaws.com/Pages/WeatherWidget/Images/weather_light_blue_bottom.png) no-repeat; font-size: 12px; font-family: Arial,sans-serif; line-height: 12px; font-weight: bold;">See 10-Day Forecast</div></a><div style="text-align: center; width: 160px;"><a alt="Hotels Combined" title="Hotels Combined" style="background:none;font-family:Arial,sans-serif; font-size: 9px; color: #777777;" rel="nofollow" href="http://www.hotelscombined.com">&copy; HotelsCombined.com</a></div></div></div>',
	),
	array(
		'gid_driver' => 'custom_informers', 
		'id_country' => 'CA', 
		'id_region' => 6, 
		'id_city' => 6, 
		'lat' => '43.7001100', 
		'lon' => '-79.4163000', 
		'code' => '<div style="line-height: 10px;background:none;"><div style="max-width: 160px; width: 160px; background:none;"><object style="margin:0;padding:0;" type="application/x-shockwave-flash" data="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" 	id="w4aaa9c4e6c122b4c4af9ecec7aeeb33a" height="272" width="160">	<param value="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" name="movie"/>	<param value="transparent" name="wmode">	<param value="station_id=CYTZ&city_name=Toronto&language=en&use_celsius=Yes&skinName=LightBlue&PID=166712&ts=201404150142&hideChangeSkin=No" name="flashvars">	<param value="all" name="allowNetworking">	<param value="always" name="allowScriptAccess"></object><a alt="Hotels Combined" title="Hotels Combined" style="margin:0; padding:0; text-decoration: none; background: none;" target="_blank" href="http://widgets.hotelscombined.com/City/Weather/Toronto.htm?use_celsius=Yes"><div style="background: none; color: white; text-align: center; width: 160px; height: 17px; margin: 0px 0 0 0; padding: 5px 0 0 0; cursor:pointer; background: transparent url(http://static.hotelscombined.com.s3.amazonaws.com/Pages/WeatherWidget/Images/weather_light_blue_bottom.png) no-repeat; font-size: 12px; font-family: Arial,sans-serif; line-height: 12px; font-weight: bold;">See 10-Day Forecast</div></a><div style="text-align: center; width: 160px;"><a alt="Hotels Combined" title="Hotels Combined" style="background:none;font-family:Arial,sans-serif; font-size: 9px; color: #777777;" rel="nofollow" href="http://www.hotelscombined.com">&copy; HotelsCombined.com</a></div></div></div>',
	),
	array(
		'gid_driver' => 'custom_informers', 
		'id_country' => 'RU', 
		'id_region' => 7, 
		'id_city' => 7, 
		'lat' => '55.7500000', 
		'lon' => '-37.6166667', 
		'code' => '<div style="line-height: 10px;background:none;"><div style="max-width: 160px; width: 160px; background:none;"><object style="margin:0;padding:0;" type="application/x-shockwave-flash" data="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" 	id="w4aaa9c4e6c122b4c4af9ecec7aeeb33a" height="272" width="160">	<param value="http://static.hotelscombined.com.s3.amazonaws.com/swf/weather_widget.swf" name="movie"/>	<param value="transparent" name="wmode">	<param value="station_id=UUEE&city_name=Moscow&language=en&use_celsius=Yes&skinName=LightBlue&PID=166713&ts=201404150142&hideChangeSkin=No" name="flashvars">	<param value="all" name="allowNetworking">	<param value="always" name="allowScriptAccess"></object><a alt="Hotels Combined" title="Hotels Combined" style="margin:0; padding:0; text-decoration: none; background: none;" target="_blank" href="http://widgets.hotelscombined.com/City/Weather/Moscow.htm?use_celsius=Yes"><div style="background: none; color: white; text-align: center; width: 160px; height: 17px; margin: 0px 0 0 0; padding: 5px 0 0 0; cursor:pointer; background: transparent url(http://static.hotelscombined.com.s3.amazonaws.com/Pages/WeatherWidget/Images/weather_light_blue_bottom.png) no-repeat; font-size: 12px; font-family: Arial,sans-serif; line-height: 12px; font-weight: bold;">See 10-Day Forecast</div></a><div style="text-align: center; width: 160px;"><a alt="Hotels Combined" title="Hotels Combined" style="background:none;font-family:Arial,sans-serif; font-size: 9px; color: #777777;" rel="nofollow" href="http://www.hotelscombined.com">&copy; HotelsCombined.com</a></div></div></div>',
	),
);


<?php
$module["module"] = "weather";
$module["install_name"] = "Weather";
$module["install_descr"] = "Weather widgets";
$module["version"] = "1.01";
$module["files"] = array(
	array("file", "read", "application/modules/weather/controllers/admin_weather.php"),
	array("file", "read", "application/modules/weather/helpers/weather_helper.php"),
	array("file", "read", "application/modules/weather/install/demo_content.php"),
	array("file", "read", "application/modules/weather/install/module.php"),
	array("file", "read", "application/modules/weather/install/permissions.php"),
	array("file", "read", "application/modules/weather/install/settings.php"),
	array("file", "read", "application/modules/weather/install/structure_deinstall.sql"),
	array("file", "read", "application/modules/weather/install/structure_install.sql"),
	array("file", "read", "application/modules/weather/models/weather_install_model.php"),
	array("file", "read", "application/modules/weather/models/weather_model.php"),
	array("file", "read", "application/modules/weather/views/admin/edit.tpl"),
	array("file", "read", "application/modules/weather/views/admin/list.tpl"),
	array("file", "read", "application/modules/weather/views/admin/location_form.tpl"),
	array("file", "read", "application/modules/weather/views/admin/locations.tpl"),
	array("file", "read", "application/modules/weather/views/default/helper_weather_block.tpl"),
	
	array("dir", "read", "application/modules/weather/langs"),
);

$module["dependencies"] = array(
	"start" => array("version"=>"1.01"),
	"menu" => array("version"=>"1.01"),
);

$module["linked_modules"] = array(
	"install" => array(
		"menu"			=> "install_menu",
	),
	"deinstall" => array(
		"menu"			=> "deinstall_menu",
	)
);


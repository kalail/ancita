<?php  

if (!defined('BASEPATH')) exit('No direct script access allowed');

define('WEATHER_DRIVERS_TABLE', DB_PREFIX.'weather_drivers');
define('WEATHER_LOCATIONS_TABLE', DB_PREFIX.'weather_locations');

/**
 * Weather main model
 * 
 * @package PG_RealEstate
 * @subpackage Weather
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
**/
class Weather_model extends Model
{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Driver fields in data source
	 * 
	 * @var array
	 */
	private $driver_fields = array(
		'id', 
		'gid', 
		'status',
		'regkey', 
		'need_regkey', 
		'link', 
		'auto_locations',
		'editable',
		'settings',
		'date_add', 
		'date_modified', 
		'status',
	);
	
	/**
	 * Locations fields in data source
	 * 
	 * @var array
	 */
	private $location_fields = array(
		'id',
		'gid_driver',
		'id_country',
		'id_region',
		'id_city',
	);

	/**
	 * Cahce of drivers data 
	 * 
	 * @var array
	 */
	private $driver_cache = array();

	/**
	 * Driver by default
	 * 
	 * @var string
	 */
	private $default_driver_gid = '';
	
	/**
	 * Settings for formatting driver object
	 * 
	 * @var array
	 */
	private $location_format_settings = array(
		'use_format' => true,
		'get_location' => true,
	);

	/**
	 * Constructor
	 *
	 * @return Weather_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
		
		foreach($this->CI->pg_language->languages as $id=>$value){
			$this->location_fields[] = 'code_'.$value['id'];
		}

		$this->get_default_driver();
	}

	/**
	 * Return driver data by guid
	 * 
	 * @param string $gid driver guid
	 * @return array
	 */
	public function get_driver_by_gid($gid){
		if(empty($this->driver_cache[$gid])){
			$this->DB->select(implode(', ', $this->driver_fields))->from(WEATHER_DRIVERS_TABLE)->where('gid', $gid);
			$results = $this->DB->get()->result_array();
			if(!empty($results) && is_array($results)){
				$this->driver_cache[$gid] = $this->format_driver($results[0]);
			}
		}
		return $this->driver_cache[$gid];
	}

	/**
	 * Return data of driver by default
	 * 
	 * @return array
	 */
	public function get_default_driver(){
		if(!empty($this->default_driver_gid)){
			return $this->get_driver_by_gid($this->default_driver_gid);
		}

		$this->DB->select(implode(", ", $this->driver_fields))->from(WEATHER_DRIVERS_TABLE)->where("status", "1")->limit(1);
		$results = $this->DB->get()->result_array();
		
		if(!empty($results) && is_array($results)){
			$this->default_driver_gid = $results[0]["gid"];
			$this->driver_cache[$this->default_driver_gid] = $this->format_driver($results[0]);
			return $this->driver_cache[$this->default_driver_gid];
		}
		return array();
	}
	
	/**
	 * Return guid of driver by default
	 * 
	 * @return string
	 */
	public function get_default_driver_gid(){
		return $this->default_driver_gid;
	}
	
	/**
	 * Format driver data
	 * 
	 * @param array $data driver data
	 * @return array
	 */
	public function format_driver($data){
		$data['name'] = l('driver_name_'.$data['gid'], 'weather');
		$data['settings'] = $data['settings'] ? unserialize($data['settings']) : array();
		return $data;
	}

	/**
	 * Set driver object as default
	 * 
	 * @param string $gid driver guid
	 * @return void
	 */
	public function set_default_driver($gid){
		$this->driver_cache = array();
		$this->default_driver_gid = $gid;

		$data["status"] = 0;
		$this->DB->update(WEATHER_DRIVERS_TABLE, $data);

		$data["status"] = 1;
		$this->DB->where('gid', $gid);
		$this->DB->update(WEATHER_DRIVERS_TABLE, $data);
		return;
	}

	/**
	 * Return all weather drivers
	 * 
	 * @return array
	 */
	public function get_drivers(){
		$data = array();
		$this->DB->select(implode(", ", $this->driver_fields))->from(WEATHER_DRIVERS_TABLE);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $result){
				$data[] = $this->format_driver($result);
			}
		}
		return $data;
	}

	/**
	 * Save driver object to data source
	 * 
	 * @param string $gid driver guid 
	 * @param array $data driver data
	 * @return void
	 */
	public function save_driver($gid, $data){
		if (is_null($gid)){
			$data["date_add"] = $data["date_modified"] = date("Y-m-d H:i:s");
			if(!isset($data["status"])) $data["status"] = 0;
			$this->DB->insert(WEATHER_DRIVERS_TABLE, $data);
		}else{
			$data["date_modified"] = date("Y-m-d H:i:s");
			$this->DB->where('gid', $gid);
			$this->DB->update(WEATHER_DRIVERS_TABLE, $data);
			unset($this->driver_cache[$gid]);
		}
	}

	/**
	 * Validate driver object for saving to data source
	 * 
	 * @param string $gid driver guid 
	 * @param array $data driver data
	 * @return void
	 */
	public function validate_driver($gid, $data){
		$return = array("errors"=> array(), "data" => array());

		if(isset($data["regkey"])){
			$return["data"]["regkey"] = trim(strip_tags($data["regkey"]));
		}
		
		if(isset($data['settings'])){
			$return['data']['settings'] = serialize($data['settings']);
		}

		return $return;
	}

	/**
	 * Remove driver object from data source
	 * 
	 * @return void
	 */
	public function delete_driver($id){
	
	}
	
	/**
	 * Return location object by identifier
	 * 
	 * @param integer $location_id location identifier
	 * @return array
	 */
	public function get_location_by_id($location_id){
		$this->DB->select(implode(', ', $this->location_fields));
		$this->DB->from(WEATHER_LOCATIONS_TABLE);
		$this->DB->where('id', $location_id);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return $this->format_location($results[0]);
		}
		return array();
	}
	
	/**
	 * Return locations objects as array (internal)
	 * 
	 * @param integer $page page of results
	 * @param integer $items_on_page results per page
	 * @param array $order_by sorting data
	 * @param array $params sql criteria
	 * @return array
	 */
	public function _get_locations_list($page=null, $items_on_page=null, $order_by=null, $params=array()){
		$this->DB->select(implode(', ', $this->location_fields));
		$this->DB->from(WEATHER_LOCATIONS_TABLE);

		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}
		
		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value, null, false);
			}
		}
		
		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->location_fields)){
					$this->DB->order_by($field.' '.$dir);
				}
			}
		}elseif($order_by){
			$this->DB->order_by($order_by);
		}
		
		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}
	
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			$data = $this->format_locations($data);
			return $data;
		}
		return array();
	}
	
	/**
	 * Return number of locations in data source
	 * 
	 * @param array $params sql criteria
	 * @return integer
	 */
	public function _get_locations_count($params=array()){
		$this->DB->select('COUNT(*) AS cnt');
		$this->DB->from(WEATHER_LOCATIONS_TABLE);
		
		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}
		
		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value, null, false);
			}
		}
		
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]['cnt']);
		}
		return 0;
	}
	
	/**
	 * Return filtered locations objects as array
	 * 
	 * @param array $filters filters data
	 * @param integer $page page of results
	 * @param integer $items_on_page results per page
	 * @param array $order_by sorting data
	 * @return array
	 */
	public function get_locations_list($filters=array(), $page=null, $items_on_page=null, $order_by=null){
		$params = $this->_get_search_criteria($filters);
		return $this->_get_locations_list($params);
	}
	
	/**
	 * Return number of locations in data source
	 * 
	 * @param array $filters filters data
	 * @return integer
	 */
	public function get_locations_count($filters=array()){
		$params = $this->_get_search_criteria($filters);
		return $this->_get_locations_count($params);
	}
	
	/**
	 * Save location object to data source
	 * 
	 * @param integer $location_id location identifier
	 * @param array $data location data
	 * @return void
	 */
	public function save_location($location_id, $data){
		if(is_null($location_id)){
			$this->DB->insert(WEATHER_LOCATIONS_TABLE, $data);
		}else{
			$this->DB->where('id', $location_id);
			$this->DB->update(WEATHER_LOCATIONS_TABLE, $data);
		}
	}
	
	/**
	 * Remove location object from data source
	 * 
	 * @param integer $location_id location identifier
	 * @return void
	 */
	public function delete_location($location_id){
		$this->DB->where('id', $location_id);
		$this->DB->delete(WEATHER_LOCATIONS_TABLE);
	}
	
	/**
	 * Return sql criteria as array from filters data
	 * 
	 * @param array $filters filters data
	 * @return array
	 */
	private function _get_search_criteria($filters){
		$params = array();
		
		if(empty($filters)) return $params;

		$fields = array_flip($this->location_fields);
		foreach($filters as $filter_name=>$filter_data){
			if(!is_array($filter_data)) $filter_data = trim($filter_data);
			switch($filter_name){
				// By field
				default:
					if(isset($fields[$filter_name])){
						if(is_array($filter_data)){
							$params = array_merge_recursive($params, array('where_in'=>array(WEATHER_LOCATIONS_TABLE.'.'.$filter_name=>$filter_data)));	
						}else{
							$params = array_merge_recursive($params, array('where'=>array(WEATHER_LOCATIONS_TABLE.'.'.$filter_name=>$filter_data)));	
						}
					}
				break;
			}
		}	
	}
	
	/**
	 * Validate location object for saving to data source
	 * 
	 * @param integer $location_id location identifier
	 * @param array $data location data
	 * @return array
	 */
	public function validate_location($location_id, $data){
		$return = array('errors'=>array(), 'data'=>array());
		
		if($location_id){
			$this->set_location_format_settings('use_format', false);
			$location = $this->get_location_by_id($location_id);
			$this->set_location_format_settings('use_format', true);
		}else{
			$location = array();
		}
		
		if(isset($data['gid_driver'])){
			$return['data']['gid_driver'] = trim(strip_tags($data['gid_driver']));
		}
		
		if(isset($data['id_country'])){
			$return['data']['id_country'] = trim(strip_tags($data['id_country']));
			if(empty($return['data']['id_country'])){
				$return['errors'][] = l('error_empty_country', 'weather');
			}
		}elseif(!$location_id){
			$return['errors'][] = l('error_empty_country', 'weather');
		}
		
		if(isset($data['id_region'])){
			$return['data']['id_region'] = intval($data['id_region']);
			if(empty($return['data']['id_region'])){
				$return['errors'][] = l('error_empty_region', 'weather');
			}
		}elseif(!$location_id){
			$return['errors'][] = l('error_empty_region', 'weather');
		}
		
		if(isset($data['id_city'])){
			$return['data']['id_city'] = intval($data['id_city']);
			if(empty($return['data']['id_city'])){
				$return['errors'][] = l('error_empty_city', 'weather');
			}else{
				$this->CI->load->model('Countries_model');
				$city = $this->CI->Countries_model->get_city($data['id_city']);
				if(!empty($city)){
					$return['data']['lat'] = $city['latitude'];
					$return['data']['lon'] = $city['longitude'];
				}
			}
		}elseif(!$location_id){
			$return['errors'][] = l('error_empty_city', 'weather');
		}
		
		foreach($this->CI->pg_language->languages as $lid=>$lang_data){
			if(isset($data['code_'.$lid])){
				$return['data']['code_'.$lid] = $data['code_'.$lid];
			}
		}
		
		return $return;
	}
	
	/**
	 * Format location data
	 * 
	 * @param array $data location data
	 * @return array
	 */
	public function format_location($data){
		return array_shift($this->format_locations(array($data)));
	}
	
	/**
	 * Format locations data
	 * 
	 * @param array $data locations data
	 * @return array
	 */
	public function format_locations($data){
		if(!$this->location_format_settings['use_format']) return $data;
		
		$locations_ids = array();
		
		foreach($data as $key=>$location){
			$location['code'] = $location['code_'.$this->pg_language->current_lang_id];
		
			// get location
			if($this->location_format_settings['get_location']){
				$locations_ids[$location['id']] = array(
					'country' => $location['id_country'], 
					'region' => $location['id_region'], 
					'city' => $location['id_city'], 
				);
			}
	
			$data[$key] = $location;
		}
		
		// get location
		if(!empty($locations_ids)){
			$this->CI->load->helper('countries');
			$locations = address_output_format($locations_ids);
			$locations_data = get_location_data($locations_ids);
			foreach($data as $key=>$location){
				$location['country'] = (isset($locations_data['country'][$location['id_country']])) ? $locations_data['country'][$location['id_country']]['name'] : '';
				$location['region'] = (isset($locations_data['region'][$location['id_region']])) ? $locations_data['region'][$location['id_region']]['name'] : '';
				$location['region_code'] = (isset($locations_data['region'][$location['id_region']])) ? $locations_data['region'][$location['id_region']]['code'] : '';
				$location['city'] = (isset($locations_data['city'][$location['id_city']])) ? $locations_data['city'][$location['id_city']]['name'] : '';
				$location['output_name'] = $locations[$location['id']];
				$data[$key] = $location;
			}
		}
		
		return $data;
	}
	
	/**
	 * Change settings for formatting location object
	 * 
	 * @param string $name parameter name
	 * @param mixed $value parameter value
	 * @return void
	 */
	public function set_location_format_settings($name, $value=false){
		if(!is_array($name)) $name = array($name=>$value);
		foreach($name as $key => $item)	$this->location_format_settings[$key] = $item;
	}
	
	/**
	 * Return widget code of custom informer
	 * 
	 * @param string $country_id country code
	 * @param integer $region_id region identifier
	 * @param integer $city_id city identifier
	 * @return string
	 */
	public function get_weather_code($country_id, $region_id, $city_id){
		$this->DB->select(implode(', ', $this->location_fields));
		$this->DB->from(WEATHER_LOCATIONS_TABLE);
		$this->DB->where('id_country', $country_id);
		$this->DB->where('id_region', $region_id);
		$this->DB->where('id_city', $city_id);
		$this->DB->limit(1);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$result = $this->format_location($results[0]);
			return $result['code'];
		}else{
			$this->CI->load->model('Countries_model');
			$city = $this->CI->Countries_model->get_city($city_id);
			if(!empty($city)){
				$this->DB->select(implode(', ', $this->location_fields));
				$this->DB->from(WEATHER_LOCATIONS_TABLE);
				$this->DB->where('id_country', $country_id);
				$this->DB->where('id_region', $region_id);
				$this->DB->order_by('ABS(lat - '.$this->DB->escape($city['latitude']).')*(lon - '.$this->DB->escape($city['longitude']).') ASC');
				$this->DB->limit(1);
				$results = $this->DB->get()->result_array();
				if(!empty($results) && is_array($results)){
					$result = $this->format_location($results[0]);
					return $result['code'];
				}
			}
		}
		return '';
	}
	
	/**
	 * Add widget field depended on language
	 * 
	 * @param integer $lang_id language identifier
	 * @return void
	 */
	public function lang_dedicate_module_callback_add($lang_id=false){
		if(!$lang_id) return;
		
		$this->CI->load->dbforge();
		
		$fields['code_'.$lang_id] = array('type'=>'TEXT', 'null'=>TRUE);
		$this->CI->dbforge->add_column(WEATHER_LOCATIONS_TABLE, $fields);
	}
	
	/**
	 * Remove widget field depended on language
	 * 
	 * @param integer $lang_id language identifier
	 * @return void
	 */
	public function lang_dedicate_module_callback_delete($lang_id=false){
		if(!$lang_id) return;
		
		$this->CI->load->dbforge();
		
		$table_query = $this->CI->db->get(WEATHER_LOCATIONS_TABLE);
		$fields_exists = $table_query->list_fields();
		
		$fields = array('code_'.$lang_id);
		foreach($fields as $field_name){
			if(!in_array($field_name, $fields_exists)) continue;
			$this->CI->dbforge->drop_column(WEATHER_LOCATIONS_TABLE, $field_name);
		}
	}
}

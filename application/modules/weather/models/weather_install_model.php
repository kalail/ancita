<?php

/**
 * Weather install model
 * 
 * @package PG_RealEstate
 * @subpackage Weather
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Weather_install_model extends Model{
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	protected $CI;
	
	/**
	 * Menu configuration
	 * 
	 * @var array
	 */
	protected $menu = array(
		"admin_menu" => array(
			"action" => "none",
			"items" => array(
				"interface-items" => array(
					"action" => "none",
					"items" => array(
						"weather_menu_item" => array("action" => "create", "link" => "admin/weather", "status" => 1, "sorter" => 7),
					),
				),
			),			
		),
	);
	
	/**
	 * Fields depended on languages
	 * 
	 * @var array
	 */
	protected $lang_dm_data = array(
		array(
			"module" => "weather",
			"model" => "Weather_model",
			"method_add" => "lang_dedicate_module_callback_add",
			"method_delete" => "lang_dedicate_module_callback_delete",
		),
	);
	
	/**
	 * Constructor
	 *
	 * @return Weather_install_model
	 */
	public function Weather_install_model(){
		parent::Model();
		$this->CI = & get_instance();
		//// load langs
		$this->CI->load->model("Install_model");
	}

	/**
	 * Install data of menu module
	 * 
	 * @return void
	 */
	public function install_menu(){
		
		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]["id"] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, "create", $gid, 0, $this->menu[$gid]["items"]);
		}
	}
	
	/**
	 * Import languages of menu module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_menu_lang_update($langs_ids=null){
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read("weather", "menu", $langs_ids);

		if(!$langs_file){log_message("info", "Empty menu langs data"); return false;}

		$this->CI->load->helper("menu");

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, "update", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages of menu module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_menu_lang_export($langs_ids){
		if(empty($langs_ids)) return false;
		$this->CI->load->helper("menu");

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, "export", $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array("menu" => $return);
	}
	
	/**
	 * Uninstall data of menu module
	 * 
	 * @return void
	 */
	public function deinstall_menu(){		
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			if($menu_data['action'] == 'create'){
				linked_install_set_menu($gid, 'delete');
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]['items']);
			}
		}
	}
	
	/**
	 * Install fields depended on linked modules
	 * 
	 * @return void
	 */
	public function _prepare_installing(){
		$this->CI->load->model("Weather_model");
		foreach($this->CI->pg_language->languages as $lang_id => $value){
			$this->CI->Weather_model->lang_dedicate_module_callback_add($lang_id);
		}
	}
	
	/**
	 * Install module data
	 * 
	 * @return void
	 */
	public function _arbitrary_installing(){
		///// add entries for lang data updates
		foreach($this->lang_dm_data as $lang_dm_data){
			$this->CI->pg_language->add_dedicate_modules_entry($lang_dm_data);
		}
		
		$this->add_demo_content();
	}

	/**
	 * Unintall module data
	 * 
	 * @return void
	 */
	public function _arbitrary_deinstalling(){
		/// delete entries in dedicate modules
		foreach($this->lang_dm_data as $lang_dm_data){
			$this->CI->pg_language->delete_dedicate_modules_entry(array('where'=>$lang_dm_data));
		}
	}
	
	/**
	 * Add demo data
	 * 
	 * @return void
	 */
	public function add_demo_content(){
		$this->CI->load->model('Weather_model');
		$demo_content = include MODULEPATH . 'weather/install/demo_content.php';
		
		foreach($demo_content as $data) {
			foreach($this->pg_language->languages as $lid=>$lang_data){
				$data['code_'.$lid] = $data['code'];
			}
			$validate_data = $this->CI->Weather_model->validate_location(null, $data);
			if(!empty($validate_data['errors'])) continue;
			$this->CI->Weather_model->save_location(null, $validate_data['data']);
		}
	}
}

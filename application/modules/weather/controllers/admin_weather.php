<?php

/**
 * Weather admin side controller
 * 
 * @package PG_RealEstate
 * @subpackage Weather
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Admin_Weather extends Controller{
	
	/**
	 * Constructor
	 *
	 * @return Admin_Weather
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model("Menu_model");
		$this->Menu_model->set_menu_active_item("admin_menu", "interface-items");
	}
	
	/**
	 * Weather informers management
	 * 
	 * @return void
	 */
	public function index(){
		$this->load->model('Weather_model');
		
		$drivers = $this->Weather_model->get_drivers();
		$this->template_lite->assign('drivers', $drivers);

		$page_data['date_format'] = $this->pg_date->get_format('date_time_numeric', 'st');
		$this->template_lite->assign('page_data', $page_data);

		$this->system_messages->set_data('header', l('admin_header_driver_list', 'weather'));
		
		$this->template_lite->view('list');
	}
	
	/**
	 * Edit weather driver data
	 * 
	 * @param string $driver_gid driver guid
	 * @return void
	 */
	public function edit($driver_gid){
		$this->load->model('Weather_model');
		
		$data = $this->Weather_model->get_driver_by_gid($driver_gid);
		if(empty($data)) show_404();
	
		if($this->input->post('btn_save')){
			$post_data = $this->input->post('data', true);
			
			$validate_data = $this->Weather_model->validate_driver($driver_gid, $post_data);
			if(!empty($validate_data['errors'])){
				$this->system_messages->add_message('error', $validate_data['errors']);
			}else{
				$this->Weather_model->save_driver($driver_gid, $validate_data['data']);
				$this->system_messages->add_message('success', l('success_driver_updated', 'weather'));
				$url = site_url().'admin/weather/edit/'.$driver_gid;
				redirect($url);
			}
			
			$data = array_merge($data, $post_data);
		}
		
		$this->template_lite->assign('data', $data);
		
		$this->system_messages->set_data('header', l('admin_header_driver_list', 'weather'));
		
		$this->template_lite->view('edit');
	}
	
	/**
	 * Activate weather driver
	 * 
	 * @param string $driver_gid driver guid
	 * @return void
	 */
	public function activate($driver_gid){
		if(!empty($driver_gid)){
			$this->load->model('Weather_model');
			$this->Weather_model->set_default_driver($driver_gid);
		}
		redirect(site_url()."admin/weather");
	}
	
	/**
	 * Driver locations management
	 * 
	 * @param string $driver_gid driver guid
	 * @param string $order sorting order
	 * @param string $order_direction sorting direction
	 * @param integer $page page of results
	 * @return void
	 */
	public function locations($driver_gid, $order=null, $order_direction=null, $page=null){
		$this->load->model('Weather_model');
		
		$current_settings = isset($_SESSION['weather_list']) ? $_SESSION['weather_list'] : array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'date_created';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;
		
		$current_settings['page'] = $page;
		
		if (!$order) $order = $current_settings['order'];
		$this->template_lite->assign('order', $order);
		$current_settings['order'] = $order;

		if (!$order_direction) $order_direction = $current_settings['order_direction'];
		$this->template_lite->assign('order_direction', $order_direction);
		$current_settings['order_direction'] = $order_direction;
		
		$filters['gid_driver'] = $driver_gid;
		
		$locations_count = $this->Weather_model->get_locations_count($filters);

		if(!$page) $page = $current_settings['page'];
		$items_on_page = $this->pg_module->get_module_config('start', 'admin_items_per_page');
			
		$this->load->helper('sort_order');
		$page = get_exists_page_number($page, $listings_count, $items_on_page);
		$current_settings['page'] = $page;

		$_SESSION['weather_list'] = $current_settings;

		if($locations_count > 0){
			$locations = $this->Weather_model->get_locations_list($filters, $page, $items_on_page, array($order=>$order_direction));
			$this->template_lite->assign('locations', $locations);
		}

		$this->template_lite->assign('driver_gid', $driver_gid);
		
		$this->system_messages->set_data('back_link', site_url()."admin/weather/index");
		
		$this->system_messages->set_data('header', l('admin_header_location_list', 'weather'));

		$this->template_lite->view('locations');
	}
	
	/**
	 * Edit location data
	 * 
	 * @param string $driver_gid driver guid
	 * @param integer $location_id location identifier
	 * @param integer $lang_id language identifier
	 * @return void
	 */
	public function location_edit($driver_gid, $location_id=null, $lang_id=null){
		$this->load->model('Weather_model');
		
		$driver = $this->Weather_model->get_driver_by_gid($driver_gid);
		if(empty($driver)) show_404();
	
		$data = $this->Weather_model->get_location_by_id($location_id);

		if(!$lang_id) $lang_id = $this->pg_language->current_lang_id;

		if($this->input->post('btn_save')){
			$post_data = array(
				'gid_driver' => $driver_gid,
				'id_country' => $this->input->post('id_country', true),
				'id_region' => $this->input->post('id_region', true),
				'id_city' => $this->input->post('id_city', true),
				'code_'.$lang_id => $this->input->post('code'),
			);
		
			$validate_data = $this->Weather_model->validate_location($location_id, $post_data);
			if(!empty($validate_data['errors'])){
				$this->system_messages->add_message('error', $validate_data['errors']);
			}else{
				$this->Weather_model->save_location($location_id, $validate_data['data']);
				$this->system_messages->add_message('success', l('success_location_updated', 'weather'));
				$url = site_url().'admin/weather/locations/'.$driver_gid;
				redirect($url);
			}
			
			$data = array_merge($data, $post_data);
		}
		
		$this->template_lite->assign('data', $data);
		
		$this->template_lite->assign('driver_gid', $driver_gid);
		
		$this->system_messages->set_data('header', l('admin_header_location_list', 'weather'));
		
		$this->template_lite->view('location_form');
	}
	
	/**
	 * Remove driver location
	 * 
	 * @param string $driver_gid driver guid
	 * @param integer $location_id location identifiers
	 * @return void
	 */
	public function location_delete($driver_gid, $location_ids){
		if(!$location_ids) $location_ids = $this->input->post('ids', true);
		if(!empty($location_ids)){
			$this->load->model('Weather_model');
			foreach((array)$location_ids as $location_id) $this->Weather_model->delete_location($location_id);
			$this->system_messages->add_message('success', l('success_location_deleted', 'weather'));
		}
		redirect(site_url().'admin/weather/locations/'.$driver_gid);
	}
}

{include file="header.tpl"}
<form method="post" action="{$data.action}" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n150">
		<div class="row header">{l i='admin_header_location_edit' gid='weather'}</div>
		<div class="row">
			<div class="h">{l i='field_location' gid='weather'}: </div>
			<div class="v">{country_select select_type='city' id_country=$data.id_country id_region=$data.id_region id_city=$data.id_city}</div>
		</div>
		<div class="row">
			<div class="h">{l i='field_code' gid='weather'}: </div>
			<div class="v"><textarea name="code" rows="10" cols="80">{$data.code|escape}</textarea></div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
	<a class="cancel" href="{$site_url}admin/weather/locations/{$driver_gid}">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>
<script>{literal}
	$(function(){
		$("div.row:odd").addClass("zebra");
	});
{/literal}</script>
{include file="footer.tpl"}

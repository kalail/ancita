{include file="header.tpl"}
<div class="actions">
	<ul>
		<li><div class="l"><a href="{$site_url}admin/weather/location_edit/{$driver_gid}">{l i='link_location_add' gid='weather'}</a></div></li>
	</ul>
	&nbsp;
</div>
<form id="locations_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th>{l i='field_location' gid='weather'}</th>
	<th class="w100">&nbsp;</th>
</tr>
{foreach item=item from=$locations}
{counter print=false assign=counter}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td>{$item.output_name}</td>
	<td class="icons">
		<a href="{$site_url}admin/weather/location_edit/{$driver_gid}/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_location_edit' gid='weather' type='button'}" title="{l i='link_location_edit' gid='weather' type='button'}"></a>
		<a href="{$site_url}admin/weather/location_delete/{$driver_gid}/{$item.id}" onclick="javascript: if(!confirm('{l i='note_location_delete' gid='weather' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_location_delete' gid='weather' type='button'}" title="{l i='link_weather_delete' gid='weather' type='button'}"></a>
	</td>
</tr>
{foreachelse}
<tr><td colspan="2" class="center">{l i='no_locations' gid='weather'}</td></tr>
{/foreach}
</table>
</form>
{include file="pagination.tpl"}
<script>{literal}
var reload_link = "{/literal}{$site_url}admin/weather/locations/{$driver_gid}{literal}";
var order = '{/literal}{$order}{literal}';
var loading_content;
var order_direction = '{/literal}{$order_direction}{literal}';
function reload_this_page(value){
	var link = reload_link + '/' + value + '/' + order + '/' + order_direction;
	location.href=link;
}
{/literal}</script>
{include file="footer.tpl"}

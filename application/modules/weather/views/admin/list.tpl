{include file="header.tpl"}

<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first w150">{l i='field_title' gid='weather'}</th>
	<th class="w50">{l i='field_status' gid='weather'}</th>
	{*<th>{l i='field_regkey' gid='weather'}</th>*}
	<th class="w50">{l i='field_link' gid='weather'}</th>
	<th class="w70">&nbsp;</th>
</tr>
{foreach item=item from=$drivers}
{counter print=false assign=counter}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td class="first">{$item.name}</td>
	<td class="center">
		{if $item.status}
		<img src="{$site_root}{$img_folder}icon-full.png" width="16" height="16" border="0" alt="{l i='link_driver_active' gid='weather' type='button'}" title="{l i='link_driver_active' gid='weather' type='button'}">
		{else}
		<a href="{$site_url}admin/weather/activate/{$item.gid}"><img src="{$site_root}{$img_folder}icon-empty.png" width="16" height="16" border="0" alt="{l i='link_driver_activate' gid='weather' type='button'}" title="{l i='link_driver_activate' gid='weather' type='button'}"></a>
		{/if}
	</td>
	{*<td>
		{if $item.need_regkey}
			{$item.regkey|truncate:50:"...":true}
		{else}
			{l i='driver_key_notrequired' gid='weather'}
		{/if}
	</td>*}
	<td class="center">
		{if $item.auto_locations}
		<a href="{$item.link}" target="_blank">
			{if $item.need_regkey}
				{l i='driver_registration' gid='weather'}
			{else}
				{l i='driver_info' gid='weather'}
			{/if}
		</a>
		{else}
		&nbsp;
		{/if}
	</td>
	<td class="icons">
		{if !$item.auto_location}<a href="{$site_url}admin/weather/locations/{$item.gid}"><img src="{$site_root}{$img_folder}icon-settings.png" width="16" height="16" border="0" alt="{l i='link_driver_locations' gid='weather' type='button'}" title="{l i='link_driver_locations' gid='weather' type='button'}"></a>{/if}
		{if $item.editable}<a href="{$site_url}admin/weather/edit/{$item.gid}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_driver_edit' gid='weather' type='button'}" title="{l i='link_driver_edit' gid='weather' type='button'}"></a>{/if}
	</td>
</tr>
{foreachelse}
<tr><td colspan="4" class="center">{l i='no_drivers' gid='weather'}</td></tr>
{/foreach}
</table>
{include file="pagination.tpl"}
{include file="footer.tpl"}

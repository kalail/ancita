<?php

/**
 * Weather management
 * 
 * @package PG_RealEstate
 * @subpackage Weather
 * @category	helpers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Mikhail Makeev <mmakeev@pilotgroup.net>
 * @version $Revision: 68 $ $Date: 2010-01-11 16:02:23 +0300 (Пн, 11 янв 2010) $ $Author: irina $
 **/

if(!function_exists('weather_block')){
	/**
	 * Show weather block
	 * 
	 * @return string
	 */
	function weather_block($params){
		$CI = &get_instance();
	
		if(empty($params['country']) || empty($params['region']) || empty($params['city'])) return '';
		
		$CI->load->model('Weather_model');
		$data = $CI->Weather_model->get_default_driver();
		
		if($data['auto_location']){
			$CI->template_lite->assign('weather_data', $weather_data);
		}else{
			$weather_code = $CI->Weather_model->get_weather_code($params['country'], $params['region'], $params['city']);
			if(!$weather_code) return '';
			$CI->template_lite->assign('weather_code', $weather_code);
		}
		
		$html = $CI->template_lite->fetch('helper_weather_block', 'user', 'weather');
		return $html;
	}
}

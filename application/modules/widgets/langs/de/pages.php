<?php

$install_lang["admin_header_configuration"] = "Konfiguration";
$install_lang["admin_header_preview"] = "Vorschau";
$install_lang["admin_header_widget_code"] = "Kopiere und füge den Code auf eine Webseite ein";
$install_lang["admin_header_widget_edit"] = "Widget bearbeiten";
$install_lang["admin_header_widgets_install"] = "Aktiverte Widgets";
$install_lang["admin_header_widgets_list"] = "Installierte Widgets";
$install_lang["btn_get_code"] = "Code holen";
$install_lang["colors_background"] = "Hintergrund";
$install_lang["colors_block"] = "Urheberrecht";
$install_lang["colors_border"] = "Grenze";
$install_lang["colors_header"] = "Kopfzeile";
$install_lang["colors_link"] = "Links";
$install_lang["colors_text"] = "Text";
$install_lang["error_module_install"] = "Das Modul ist nicht installiert";
$install_lang["error_widget_invalid"] = "Der Pfad zum Widget ist ungültig";
$install_lang["field_module"] = "Modul";
$install_lang["field_name"] = "Name";
$install_lang["field_widget_colors"] = "Farben";
$install_lang["field_widget_footer"] = "Fußzeile";
$install_lang["field_widget_size"] = "Größe";
$install_lang["field_widget_title"] = "Titel";
$install_lang["link_delete_widget"] = "Widget entfernen";
$install_lang["link_edit_widget"] = "Widget bearbeiten";
$install_lang["link_install_widget"] = "Installieren";
$install_lang["no_widgets"] = "Keine Widgets";
$install_lang["no_widgets_install"] = "Keine Widgets. <a href=\"http://pilotgroup.zendesk.com/tickets/new/\">Contact us</a> für mehrere Details";
$install_lang["note_delete_widget"] = "Wollen Sie das Widget löschen?";
$install_lang["success_widget_delete"] = "Das Widget wurde erfolgreich entfernt";
$install_lang["success_widget_install"] = "Das Widget wurde erfolgreich installiert";
$install_lang["success_widget_update"] = "Das Widget wurde erfolgreich aktualisiert";

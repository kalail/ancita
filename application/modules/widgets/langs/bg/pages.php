<?php

$install_lang["admin_header_configuration"] = "Настройки";
$install_lang["admin_header_preview"] = "Преглед";
$install_lang["admin_header_widget_code"] = "Копирайте и поставете кода във вебсайта";
$install_lang["admin_header_widget_edit"] = "Редактиране на виджета";
$install_lang["admin_header_widgets_install"] = "Инсталиране виджет";
$install_lang["admin_header_widgets_list"] = "Инсталирани виджети";
$install_lang["btn_get_code"] = "Получаване на код";
$install_lang["colors_background"] = "фон";
$install_lang["colors_block"] = "copyright";
$install_lang["colors_border"] = "рамка";
$install_lang["colors_header"] = "Цвят на главата";
$install_lang["colors_link"] = "линкове";
$install_lang["colors_text"] = "текст";
$install_lang["error_module_install"] = "Модулът не е инсталиран";
$install_lang["error_widget_invalid"] = "Пътят довиджета е грешен";
$install_lang["field_listings_location"] = "Местоположение";
$install_lang["field_listings_max"] = "Макс. количество обявления";
$install_lang["field_listings_operation_types"] = "Тип на операциите";
$install_lang["field_module"] = "Модул";
$install_lang["field_name"] = "Название";
$install_lang["field_widget_colors"] = "Цветове";
$install_lang["field_widget_footer"] = "Отдолу";
$install_lang["field_widget_size"] = "Размер";
$install_lang["field_widget_title"] = "Заглавие";
$install_lang["link_delete_widget"] = "Премахване на виджета";
$install_lang["link_edit_widget"] = "Редактиране виджета";
$install_lang["link_install_widget"] = "Инсталиране";
$install_lang["name_listings"] = "Обяви на сайта";
$install_lang["no_widgets"] = "Липсват виджети";
$install_lang["no_widgets_install"] = "Липсват виджети. <a href=\"http://pilotgroup.zendesk.com/tickets/new/\">Връзка</a> за повече детайли";
$install_lang["note_delete_widget"] = "Искате ли да премахнете виджета?";
$install_lang["success_widget_delete"] = "Виджета е премахнат";
$install_lang["success_widget_install"] = "Виджета е инсталиран";
$install_lang["success_widget_update"] = "Виджета е обновен";


<?php

if (!defined('NF_SENDER_TABLE')) define('NF_SENDER_TABLE', DB_PREFIX . 'notifications_sender');

/**
 * Notifications sender model
 * 
 * @package PG_RealEstate
 * @subpackage Notifications
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Sender_model extends Model {
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;
	
	/**
	 * Link to database object
	 * 
	 * @var object
	 */
	private $DB;
	
	/**
	 * Max notifications send a once
	 * 
	 * @var integer
	 */
	public $max_send_counter = 3;
	
	/**
	 * Timeout of sends
	 * 
	 * @var integer
	 */
	public $send_timeout = 1;
	
	/**
	 * Sender properties in data source
	 * 
	 * @var array
	 */
	private $attrs = array(
		'id', 
		'email', 
		'subject', 
		'message', 
		'content_type', 
		'send_counter',
	);

	/**
	 * Constructor
	 * 
	 * @return Sender_model
	 */
	public function __construct() {
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}

	/**
	 * Send mail
	 * 
	 * send mail in txt/html
	 * 
	 * @param string $email email
	 * @param string $subject subject
	 * @param string $message message
	 * @param string $mail_type mail type
	 * @return boolean/string
	 */
	public function send_letter($email, $subject, $message, $mail_type = "txt") {
		$this->CI->load->library('email');
		$this->CI->email->clear();

		$mailconfig = array(
			'charset' => $this->CI->pg_module->get_module_config('notifications', 'mail_charset'),
			'protocol' => $this->CI->pg_module->get_module_config('notifications', 'mail_protocol'),
			'mailpath' => $this->CI->pg_module->get_module_config('notifications', 'mail_mailpath'),
			'smtp_host' => $this->CI->pg_module->get_module_config('notifications', 'mail_smtp_host'),
			'smtp_user' => $this->CI->pg_module->get_module_config('notifications', 'mail_smtp_user'),
			'smtp_pass' => $this->CI->pg_module->get_module_config('notifications', 'mail_smtp_pass'),
			'smtp_port' => $this->CI->pg_module->get_module_config('notifications', 'mail_smtp_port'),
			'useragent' => $this->CI->pg_module->get_module_config('notifications', 'mail_useragent'),
			'dkim_private_key' => $this->CI->pg_module->get_module_config('notifications', 'dkim_private_key'),
			'dkim_domain_selector' => $this->CI->pg_module->get_module_config('notifications', 'dkim_domain_selector'),
			'mailtype' => $mail_type
		);
		$this->CI->email->initialize($mailconfig);
		$from_email = $this->CI->pg_module->get_module_config('notifications', 'mail_from_email');
		$from_name = $this->CI->pg_module->get_module_config('notifications', 'mail_from_name');
		$this->CI->email->from($from_email, $from_name);
		$this->CI->email->to($email);

		$this->CI->email->subject($subject);
		$this->CI->email->message($message);

		$result = $this->CI->email->send();
		if ($result === true) {
			return true;
		} else {
			return $this->CI->email->_debug_msg;
		}
	}

	/**
	 * Push mail to queue in data source
	 * 
	 * @param string $email email
	 * @param string $subject subject
	 * @param string $message message
	 * @param string $content_type mail type
	 * @return void
	 */
	public function push($email, $subject, $message, $content_type = "text") {
		$data = array(
			"email" => $email,
			"subject" => $subject,
			"message" => $message,
			"content_type" => $content_type,
			"send_counter" => 0,
		);
		$this->DB->insert(NF_SENDER_TABLE, $data);
	}

	/**
	 * Return mail's objects as array
	 * 
	 * @param integer $count max number of mails
	 */
	public function get($count = 10) {
		$this->DB->select('id, email, subject, message, content_type, send_counter')->from(NF_SENDER_TABLE)->order_by('id')->limit($count, 0);
		$results = $this->DB->get()->result_array();
		if (!empty($results) && is_array($results)) {
			return $results;
		}
		return array();
	}

	/**
	 * Update counter of sending attempts
	 * 
	 * @param integer $id mail identifier
	 * @param integer $counter counter value
	 * @return void
	 */
	public function update_counter($id, $counter) {
		$data = array(
			"send_counter" => $counter,
		);
		$this->DB->where('id', $id);
		$this->DB->update(NF_SENDER_TABLE, $data);
	}

	/**
	 * Remove mail object
	 * 
	 * @param integer $id mail identifier
	 * @return void
	 */
	public function delete($id) {
		if (is_array($id)) {
			$this->DB->where_in('id', $id);
		} else {
			$this->DB->where('id', $id);
		}
		$this->DB->delete(NF_SENDER_TABLE);
	}

	/**
	 * One cycle of sending
	 * 
	 * @param integer $count max sent mails
	 * @return array
	 */
	public function sending_session($count = 10) {
		$res = array("sent" => 0, "errors" => 0);

		$letters = $this->get($count);
		if (empty($letters))
			return $res;

		foreach ($letters as $letter) {
			$return = $this->send_letter($letter["email"], $letter["subject"], $letter["message"], $letter["content_type"]);
			if ($return === true || $letter["send_counter"] + 1 >= $this->max_send_counter) {
				$this->delete($letter["id"]);
			} else {
				$this->update_counter($letter["id"], $letter["send_counter"] + 1);
			}

			if ($return === true) {
				$res["sent"]++;
			} else {
				$res["errors"]++;
			}
		}
		return $res;
	}

	/**
	 * Validate mail settings for saving to data source
	 * 
	 * @param array $data settings data
	 * @return array
	 */
	public function validate_mail_config($data) {
		$return = array("data" => array(), "errors" => array());

		if (isset($data["mail_charset"])) {
			$return["data"]["mail_charset"] = strip_tags($data["mail_charset"]);
			if (empty($return["data"]["mail_charset"])) {
				$return["errors"][] = l('error_charset_incorrect', 'notifications');
			}
		}

		if (isset($data["mail_protocol"])) {
			$return["data"]["mail_protocol"] = strip_tags($data["mail_protocol"]);
			if (empty($return["data"]["mail_protocol"]) || !in_array($return["data"]["mail_protocol"], array('mail', 'sendmail', 'smtp'))) {
				$return["errors"][] = l('error_protocol_incorrect', 'notifications');
			}
		}

		if (isset($data["mail_mailpath"])) {
			$return["data"]["mail_mailpath"] = strip_tags($data["mail_mailpath"]);
		}

		if (isset($data["mail_smtp_host"])) {
			$return["data"]["mail_smtp_host"] = strip_tags($data["mail_smtp_host"]);
		}

		if (isset($data["mail_smtp_user"])) {
			$return["data"]["mail_smtp_user"] = strip_tags($data["mail_smtp_user"]);
		}

		if (isset($data["mail_smtp_pass"])) {
			$return["data"]["mail_smtp_pass"] = strip_tags($data["mail_smtp_pass"]);
		}

		if (isset($data["mail_smtp_port"])) {
			$return["data"]["mail_smtp_port"] = strip_tags($data["mail_smtp_port"]);
		}

		if (isset($data["mail_useragent"])) {
			$return["data"]["mail_useragent"] = strip_tags($data["mail_useragent"]);
			if (empty($return["data"]["mail_useragent"])) {
				$return["errors"][] = l('error_useragent_incorrect', 'notifications');
			}
		}

		if (isset($data["mail_from_email"])) {
			$return["data"]["mail_from_email"] = strip_tags($data["mail_from_email"]);
			if (empty($data["mail_from_email"])) {
				$return["errors"][] = l('error_from_email_incorrect', 'notifications');
			}
		}

		if (isset($data["mail_from_name"])) {
			$return["data"]["mail_from_name"] = strip_tags($data["mail_from_name"]);
			if (empty($data["mail_from_name"])) {
				$return["errors"][] = l('error_from_name_incorrect', 'notifications');
			}
		}
		
		if (isset($data["dkim_private_key"])) {
			$return["data"]["dkim_private_key"] = strip_tags($data["dkim_private_key"]);
		}

		if (isset($data["dkim_domain_selector"])) {
			$return["data"]["dkim_domain_selector"] = strip_tags($data["dkim_domain_selector"]);
		}

		return $return;
	}

	/**
	 * Validate test mail object
	 * 
	 * @param array $data mail data
	 * @return array
	 */
	public function validate_test($data) {
		$return = array("data" => array(), "errors" => array());

		if (isset($data["mail_to_email"])) {
			$return["data"]["mail_to_email"] = strip_tags($data["mail_to_email"]);
			if (empty($data["mail_to_email"]) || !preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",
               $data["mail_to_email"])) {
				$return["errors"][] = l('error_to_email_incorrect', 'notifications');
			}
		}

		return $return;
	}

	/**
	 * Send from queue by cron
	 * 
	 * @return void
	 */
	public function cron_que_sender() {
		$data = $this->sending_session(60);
		echo "Letters sent: " . $data["sent"] . "; (" . $data["errors"] . " errors)";
	}

	/**
	 * Return sender's objects as array
	 * 
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param array $order_by sorting data
	 * @param array $params sql criteria of query to data source
	 * @param array $filter_object_ids filters identifiers
	 * @return array
	 */
	public function get_senders_list($page = null, $items_on_page = null, $order_by = null, $params = array(), $filter_object_ids = null) {
		$this->DB->select(implode(", ", $this->attrs))->from(NF_SENDER_TABLE);

		if (isset($params["where"]) && is_array($params["where"]) && count($params["where"])) {
			foreach ($params["where"] as $field => $value)
				$this->DB->where($field, $value);
		}

		if (isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])) {
			foreach ($params["where_in"] as $field => $value)
				$this->DB->where_in($field, $value);
		}

		if (isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])) {
			foreach ($params["where_sql"] as $value)
				$this->DB->where($value);
		}

		if (isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)) {
			$this->DB->where_in("id", $filter_object_ids);
		}

		if (is_array($order_by) && count($order_by) > 0) {
			foreach ($order_by as $field => $dir) {
				if (in_array($field, $this->attrs)) {
					$this->DB->order_by($field . " " . $dir);
				}
			}
		}

		if (!is_null($page)) {
			$page = intval($page) ? intval($page) : 1;
			$this->DB->limit($items_on_page, $items_on_page * ($page - 1));
		}

		$data = array();
		$results = $this->DB->get()->result_array();
		if (!empty($results) && is_array($results)) {
			foreach ($results as $r) {
				$data[] = $this->format_senders($r);
			}
		}
		return $data;
	}

	/**
	 * Return number of sender's objects
	 * 
	 * @param array $params sql criteria of query to data source
	 * @param array $filter_object_ids filters idnetifiers
	 * @return integer
	 */
	public function get_senders_count($params = array(), $filter_object_ids = null) {
		$this->DB->select('COUNT(*) AS cnt')->from(NF_SENDER_TABLE);

		if (isset($params["where"]) && is_array($params["where"]) && count($params["where"])) {
			foreach ($params["where"] as $field => $value)
				$this->DB->where($field, $value);
		}

		if (isset($params["where_in"]) && is_array($params["where_in"]) && count($params["where_in"])) {
			foreach ($params["where_in"] as $field => $value)
				$this->DB->where_in($field, $value);
		}

		if (isset($params["where_sql"]) && is_array($params["where_sql"]) && count($params["where_sql"])) {
			foreach ($params["where_sql"] as $value)
				$this->DB->where($value);
		}

		if (isset($filter_object_ids) && is_array($filter_object_ids) && count($filter_object_ids)) {
			$this->DB->where_in("id", $filter_object_ids);
		}

		$results = $this->DB->get()->result_array();
		if (!empty($results) && is_array($results)) {
			return intval($results[0]["cnt"]);
		}
		return 0;
	}

	/**
	 * Format sender object
	 * 
	 * @param array $data sender data
	 * @param array $get_langs languages identifiers
	 * @return array
	 */
	public function format_senders($data, $get_langs = false) {
		$data["name_i"] = "sender_" . $data["id"];
		return $data;
	}

	/**
	 * Send mail by identifier(s)
	 * 
	 * @param integer/array $id mail's identifiers
	 * @return array
	 */
	public function send($id = 0) {
		$res = array("sent" => 0, "errors" => 0);

		if (is_array($id)) {
			$this->DB->where_in('id', $id);
		} else {
			$this->DB->where('id', $id);
		}
		$this->DB->select('id, email, subject, message, content_type, send_counter')->from(NF_SENDER_TABLE)->order_by('id');
		$letters = $this->DB->get()->result_array();

		if (empty($letters))
			return $res;

		foreach ($letters as $letter) {
			$return = $this->send_letter($letter["email"], $letter["subject"], $letter["message"], $letter["content_type"]);
			if ($return === true || $letter["send_counter"] + 1 >= $this->max_send_counter) {
				$this->delete($letter["id"]);
			} else {
				$this->update_counter($letter["id"], $letter["send_counter"] + 1);
			}

			if ($return === true) {
				$res["sent"]++;
			} else {
				$res["errors"]++;
			}
		}
		return $res;
	}

}


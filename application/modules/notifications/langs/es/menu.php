<?php

$install_lang["admin_menu_other_items_newsletters-items_notifications_menu_item"] = "Notificaciones por correo electrónico";
$install_lang["admin_menu_other_items_newsletters-items_notifications_menu_item_tooltip"] = "Servidor SMTP, texto de las notificaciones, suscripciones";
$install_lang["admin_notifications_menu_nf_items"] = "Alertas";
$install_lang["admin_notifications_menu_nf_items_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_pool_item"] = "El envío de la cola";
$install_lang["admin_notifications_menu_nf_pool_item_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_settings_item"] = "Configuración";
$install_lang["admin_notifications_menu_nf_settings_item_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_templates_item"] = "Plantillas";
$install_lang["admin_notifications_menu_nf_templates_item_tooltip"] = "";


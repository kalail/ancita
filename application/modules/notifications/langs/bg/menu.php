<?php

$install_lang["admin_menu_other_items_newsletters-items_notifications_menu_item"] = "Писмени известия";
$install_lang["admin_menu_other_items_newsletters-items_notifications_menu_item_tooltip"] = "SMTP сървър, текст на известията, абонамент";
$install_lang["admin_notifications_menu_nf_items"] = "Известия";
$install_lang["admin_notifications_menu_nf_items_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_pool_item"] = "Изпращане в чакащи";
$install_lang["admin_notifications_menu_nf_pool_item_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_settings_item"] = "Настройки";
$install_lang["admin_notifications_menu_nf_settings_item_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_templates_item"] = "Шаблони";
$install_lang["admin_notifications_menu_nf_templates_item_tooltip"] = "";


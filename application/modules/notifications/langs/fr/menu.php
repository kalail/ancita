<?php

$install_lang["admin_menu_other_items_newsletters-items_notifications_menu_item"] = "Notifications par courriel";
$install_lang["admin_menu_other_items_newsletters-items_notifications_menu_item_tooltip"] = "Serveur SMTP, notifications, inscriptions";
$install_lang["admin_notifications_menu_nf_items"] = "Alertes";
$install_lang["admin_notifications_menu_nf_items_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_pool_item"] = "Queue d'attente";
$install_lang["admin_notifications_menu_nf_pool_item_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_settings_item"] = "Paramètres";
$install_lang["admin_notifications_menu_nf_settings_item_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_templates_item"] = "Modèles";
$install_lang["admin_notifications_menu_nf_templates_item_tooltip"] = "";


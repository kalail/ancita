<?php

$install_lang["admin_menu_other_items_newsletters-items_notifications_menu_item"] = "E-Mailbenachrichtigungen";
$install_lang["admin_menu_other_items_newsletters-items_notifications_menu_item_tooltip"] = "SMTP Server, Text der Benachrichtigungen, Abonnements";
$install_lang["admin_notifications_menu_nf_items"] = "Warnungen";
$install_lang["admin_notifications_menu_nf_items_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_pool_item"] = "Sende Warteschlange";
$install_lang["admin_notifications_menu_nf_pool_item_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_settings_item"] = "Einstellungen";
$install_lang["admin_notifications_menu_nf_settings_item_tooltip"] = "";
$install_lang["admin_notifications_menu_nf_templates_item"] = "Vorlagen";
$install_lang["admin_notifications_menu_nf_templates_item_tooltip"] = "";


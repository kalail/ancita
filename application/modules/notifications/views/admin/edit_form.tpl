{include file="header.tpl"}


<form method="post" action="{$data.action}" name="save_form">
	<div class="edit-form n150">
		<div class="row header">{l i='admin_header_template_edit' gid='notifications'}</div>
		<div class="row zebra">
			<div class="h">{l i='field_template_gid' gid='notifications'}:&nbsp;* </div>
			<div class="v">{if $allow_edit}<input type="text" value="{$data.gid}" name="gid">{else}{$data.gid}{/if}</div>
		</div>

		<div class="row">
			<div class="h">{l i='field_notification_name' gid='notifications'}:&nbsp;* </div>
			<div class="v" id="field_notification_name">
				{assign var="name_i" value=$data.name_i}
				{foreach item=lang_item key=lang_id from=$languages}										
				<input type="{if $lang_id eq $cur_lang}text{else}hidden{/if}" name="langs[{$lang_id}]" value="{if $validate_lang}{$validate_lang[$lang_id]}{else}{l i=$data.name_i gid='notifications' lang=$lang_id}{/if}" lang-editor="value" lang-editor-type="langs" lang-editor-lid="{$lang_id}">
				{/foreach}
				<a href="#" lang-editor="button" lang-editor-type="langs"><img src="{$site_root}{$img_folder}icon-translate.png" width="16" height="16"></a>
			</div>
		</div>
		<div class="row zebra">
			<div class="h">{l i='field_send_type' gid='notifications'}:&nbsp;* </div>
			<div class="v">{if $allow_edit}
				<select name="send_type">
					<option value="que" {if $data.send_type eq 'que'}selected{/if}>{l i='field_send_type_que' gid='notifications'}</option>
					<option value="simple" {if $data.send_type eq 'simple'}selected{/if}>{l i='field_send_type_simple' gid='notifications'}</option>
				</select>
				{elseif $data.send_type eq 'que'}{l i='field_send_type_que' gid='notifications'}
					{elseif $data.send_type eq 'simple'}{l i='field_send_type_simple' gid='notifications'}
						{/if}
						</div>
					</div>
					<div class="row">
						<div class="h">{l i='field_default_template' gid='notifications'}:&nbsp;* </div>
						<div class="v">
							<select name="id_template_default">
						{foreach item=item from=$templates}<option value="{$item.id}" {if $data.id_template_default eq $item.id}selected{/if}>{$item.name}</option>{/foreach}
					</select>
				</div>
			</div>

			<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
			<a class="cancel" href="{$site_url}admin/notifications/index">{l i='btn_cancel' gid='start'}</a>
		</div>
	</form>
{block name=lang_inline_editor module=start}	
	<script>
		{literal}
function showLangs(divId){
	$('#'+divId).slideToggle();
}

function openTab(id, object){
	$('#edit_divs > div.tab').hide();
	$('#'+id).show();
	$('#edit_tabs > li').removeClass('active');
	$(object).addClass('active');
}

		{/literal}
	</script>
	<div class="clr"></div>
	{include file="footer.tpl"}

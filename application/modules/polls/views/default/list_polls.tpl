{include file="header.tpl"}
{include file="left_panel.tpl" module=start hide_poll="true"}
<div class="rc">
	<div class="content-block">
		<h1>{l i='polls_results' gid='polls'}</h1>
		{foreach item=item key=key from=$polls}
			<div id="poll_{$key}" class="poll_question_link">
				<h2>
					{if $language}
						{if $item.question[$language]}
							{$item.question[$language]}
						{else}
							{$item.question.default}
						{/if}
					{else}
						{if $item.question[$cur_lang]}
							{$item.question[$cur_lang]}
						{else}
							{$item.question.default}
						{/if}
					{/if}
					<div class="fright">
					<a class="btn-link"><ins class="with-icon i-toggle down"></ins></a>
					</div>
				</h2>

			</div>
			<div class="poll_results_content"></div>
		{/foreach}
		{literal}
			<script type="text/javascript">
				$(function() {
					if ('undefined' != typeof(PollsList)) {
						new PollsList({
							siteUrl: '{/literal}{$site_root}{literal}',
						});
					}
				});
			</script>
		{/literal}
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}

<?php

$install_lang["seo_tags_index_description"] = "Анкета на сайта. Възможност за изкажете мнение по зададени въпроси";
$install_lang["seo_tags_index_header"] = "Резултати от анкета";
$install_lang["seo_tags_index_keyword"] = "анкета,отговор на вопрос, изказване на мнение";
$install_lang["seo_tags_index_og_description"] = "Анкета на сайта. Възможност за изкажете мнение по зададени въпроси";
$install_lang["seo_tags_index_og_title"] = "Резултати от анкетата";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "PG Real Estate | Резултати анкета";


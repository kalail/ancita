<?php

$install_lang["seo_tags_index_description"] = "Polls. An opportunity to share your opinion.";
$install_lang["seo_tags_index_header"] = "Polls";
$install_lang["seo_tags_index_keyword"] = "polls, share opinion, answer questions";
$install_lang["seo_tags_index_og_description"] = "Polls. An opportunity to share your opinion.";
$install_lang["seo_tags_index_og_title"] = "Polls";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "PG Real Estate | Poll results";


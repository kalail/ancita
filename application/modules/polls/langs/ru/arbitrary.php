<?php

$install_lang["seo_tags_index_description"] = "Опросы сайта. Возможность высказать свое мнение по заданному вопросу.";
$install_lang["seo_tags_index_header"] = "Результаты опросов";
$install_lang["seo_tags_index_keyword"] = "опросы, ответить на вопрос, высказать мнение";
$install_lang["seo_tags_index_og_description"] = "Опросы сайта. Возможность высказать свое мнение по заданному вопросу.";
$install_lang["seo_tags_index_og_title"] = "Результаты опросов";
$install_lang["seo_tags_index_og_type"] = "article";
$install_lang["seo_tags_index_title"] = "PG Real Estate | Результаты опросов";


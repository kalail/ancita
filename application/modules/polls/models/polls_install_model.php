<?php

/**
 * Poll install model
 *
 * @package PG_RealEstate
 * @subpackage Polls
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Polls_install_model extends Model {
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	protected $CI;
	
	/**
	 * Menu configuration
	 * 
	 * @var array
	 */
	protected $menu = array(
		'admin_menu' => array(
			'action' => 'none',
			'items' => array(
				'other_items' => array(
					'action'=>'none',
					'items' => array(
						'polls_menu_item' => array('action' => 'create', 'link' => 'admin/polls', 'status' => 1, 'sorter' => 2)
					)
				)
			)
		),
		'admin_polls_menu' => array(
			'action' => 'create',
			'name' => 'Admin mode - Polls',
			'items' => array(
				'polls_list_item' => array('action' => 'create', 'link' => 'admin/polls', 'status' => 1)
			)
		),
		'private_main_menu' => array(
			'action' => 'none',
			'items' => array(
				//'private-main-polls-item' => array('action' => 'create', 'link' => 'polls', 'status' => 1, 'sorter' => 10)
			)
		),
		'company_main_menu' => array(
			'action' => 'none',
			'items' => array(
				//'company-main-polls-item' => array('action' => 'create', 'link' => 'polls', 'status' => 1, 'sorter' => 10)
			)
		),
		'agent_main_menu' => array(
			'action' => 'none',
			'items' => array(
				//'agent-main-polls-item' => array('action' => 'create', 'link' => 'polls', 'status' => 1, 'sorter' => 10)
			)
		),
		'guest_main_menu' => array(
			'action' => 'none',
			'items' => array(
				//'guest-main-polls-item' => array('action' => 'create', 'link' => 'polls', 'status' => 1, 'sorter' => 10)
			)
		)
	);

	/**
	 * Ausers configuration
	 * 
	 * @var array
	 */
	private $ausers_methods = array(
		array('module' => 'polls', 'method' => 'index', 'is_default' => 1),
	);
	
	/**
	 * Class constructor
	 * 
	 * @return Polls_install_model
	 */
	public function __construct() {
		parent::Model();
		$this->CI = & get_instance();
		//// load langs
		$this->CI->load->model('Install_model');
	}

	/**
	 * Install data of module menu
	 * 
	 * @return void
	 */
	public function install_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			$this->menu[$gid]['id'] = linked_install_set_menu($gid, $menu_data["action"], $menu_data["name"]);
			linked_install_process_menu_items($this->menu, 'create', $gid, 0, $this->menu[$gid]["items"]);
		}
	}

	/**
	 * Import languages of module menu
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return boolean
	 */
	public function install_menu_lang_update($langs_ids = null) {
		if(empty($langs_ids)) return false;
		$langs_file = $this->CI->Install_model->language_file_read('polls', 'menu', $langs_ids);

		if(!$langs_file) { log_message('info', 'Empty menu langs data'); return false; }

		$this->CI->load->helper('menu');

		foreach($this->menu as $gid => $menu_data){
			linked_install_process_menu_items($this->menu, 'update', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_file);
		}
		return true;
	}

	/**
	 * Export languages of module menu
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function install_menu_lang_export($langs_ids) {
		if(empty($langs_ids)) return false;
		$this->CI->load->helper('menu');

		$return = array();
		foreach($this->menu as $gid => $menu_data){
			$temp = linked_install_process_menu_items($this->menu, 'export', $gid, 0, $this->menu[$gid]["items"], $gid, $langs_ids);
			$return = array_merge($return, $temp);
		}
		return array( "menu" => $return );
	}

	/**
	 * Uninstall data of module menu
	 * 
	 * @return void
	 */
	public function deinstall_menu() {
		$this->CI->load->helper('menu');
		foreach($this->menu as $gid => $menu_data){
			if($menu_data['action'] == 'create'){
				linked_install_set_menu($gid, 'delete');
			}else{
				linked_install_delete_menu_items($gid, $this->menu[$gid]['items']);
			}
		}
	}

	/**
	 * Install data of site_map module 
	 * 
	 * @return void
	 */
	public function install_site_map() {
		//// Site map
		$this->CI->load->model('Site_map_model');
		$site_map_data = array(
			'module_gid' => 'polls',
			'model_name' => 'Polls_model',
			'get_urls_method' => 'get_sitemap_urls',
		);
		$this->CI->Site_map_model->set_sitemap_module('polls', $site_map_data);
	}

	/**
	 * Uninstall data of site_map module
	 * 
	 * @return void
	 */
	public function deinstall_site_map() {
		$this->CI->load->model('Site_map_model');
		$this->CI->Site_map_model->delete_sitemap_module('polls');
	}
	
	/**
	 * Install data of ausers module
	 * 
	 * @return void
	 */
	public function install_ausers() {
		// install ausers permissions
		$this->CI->load->model('Ausers_model');
		foreach($this->ausers_methods as $method){
			$this->CI->Ausers_model->save_method(null, $method);
		}
	}

	/**
	 * Import languages of ausers module
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return void
	 */
	public function install_ausers_lang_update($langs_ids = null) {
		$langs_file = $this->CI->Install_model->language_file_read('polls', 'ausers', $langs_ids);

		// install ausers permissions
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'polls';
		$methods = $this->CI->Ausers_model->get_methods_lang_export($params);

		foreach($methods as $method){
			if(!empty($langs_file[$method['method']])){
				$this->CI->Ausers_model->save_method($method['id'], array(), $langs_file[$method['method']]);
			}
		}
	}

	/**
	 * Export languages of ausers module
	 * 
	 * @param array $langs_ids languages identfiers
	 * @return array
	 */
	public function install_ausers_lang_export($langs_ids) {
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'polls';
		$methods =  $this->CI->Ausers_model->get_methods_lang_export($params, $langs_ids);
		foreach($methods as $method){
			$return[$method['method']] = $method['langs'];
		}
		return array('ausers' => $return);
	}

	/**
	 * Uninstall data of ausers module
	 * 
	 * @return void
	 */
	public function deinstall_ausers() {
		// delete moderation methods in ausers
		$this->CI->load->model('Ausers_model');
		$params['where']['module'] = 'polls';
		$this->CI->Ausers_model->delete_methods($params);
	}

	/**
	 * Install module data
	 * 
	 * @return void
	 */
	public function _arbitrary_installing() {
		///// Seo
		$seo_data = array(
			'module_gid' => 'polls',
			'model_name' => 'Polls_model',
			'get_settings_method' => 'get_seo_settings',
			'get_rewrite_vars_method' => 'request_seo_rewrite',
			'get_sitemap_urls_method' => 'get_sitemap_xml_urls',
		);
		$this->CI->pg_seo->set_seo_module('polls', $seo_data);
		
		// add seo link
		$this->CI->load->model('Seo_model');
		
		$xml_data = $this->CI->Seo_model->get_xml_route_file_content();
		
		$data = array(
			'noindex' => 1,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '[text:polls]',
		);		
		$this->CI->pg_seo->set_settings('user', 'polls', 'index', $data);
		
		$xml_data['polls']['index'] = $this->CI->pg_seo->url_template_transform('polls', 'index', $data["url_template"], 'base', 'xml');
		
		$this->CI->Seo_model->set_xml_route_file_content($xml_data);
		$this->CI->Seo_model->rewrite_route_php_file();
		
		$this->add_demo_content();
	}

	/**
	 * Import module languages
	 * 
	 * @param array $langs_ids array languages identifiers
	 * @return void
	 */
	public function _arbitrary_lang_install($langs_ids=null){
		$langs_file = $this->CI->Install_model->language_file_read("polls", "arbitrary", $langs_ids);
		if(!$langs_file){log_message("info", "Empty polls arbitrary langs data"); return false;}
		
		$post_data = array(
			"title" => $langs_file["seo_tags_index_title"],
			"keyword" => $langs_file["seo_tags_index_keyword"],
			"description" => $langs_file["seo_tags_index_description"],
			"header" => $langs_file["seo_tags_index_header"],
			"og_title" => $langs_file["seo_tags_index_og_title"],
			"og_type" => $langs_file["seo_tags_index_og_type"],
			"og_description" => $langs_file["seo_tags_index_og_description"],
		);
		$this->CI->pg_seo->set_settings("user", "polls", "index", $post_data);
	}

	/**
	 * Export module languages
	 * 
	 * @param array $langs_ids languages identifiers
	 * @return array
	 */
	public function _arbitrary_lang_export($langs_ids=null){
		if(empty($langs_ids)) return false;

		//// arbitrary
		$settings = $this->CI->pg_seo->get_settings("user", "polls", "index", $langs_ids);
		$arbitrary_return["seo_tags_index_title"] = $settings["title"];
		$arbitrary_return["seo_tags_index_keyword"] = $settings["keyword"];
		$arbitrary_return["seo_tags_index_description"] = $settings["description"];
		$arbitrary_return["seo_tags_index_header"] = $settings["header"];
		$arbitrary_return["seo_tags_index_og_title"] = $settings["og_title"];
		$arbitrary_return["seo_tags_index_og_type"] = $settings["og_type"];
		$arbitrary_return["seo_tags_index_og_description"] = $settings["og_description"];

		return array("arbitrary" => $arbitrary_return);
	}

	/**
	 * Uninstall module data
	 * 
	 * @return void
	 */
	public function _arbitrary_deinstalling() {
		// remove seo link
		$this->CI->load->model('Seo_model');
		
		$xml_data = $this->CI->Seo_model->get_xml_route_file_content();
		
		$data = array(
			'noindex' => 0,
			'title' => '',
			'keyword' => '',
			'description' => '',
			'header' => '',
			'og_title' => '',
			'og_type' => '',
			'og_descrtiption' => '',
			'url_template' => '',
		);		
		$this->CI->pg_seo->set_settings('user', 'polls', 'index', $data);
		
		$xml_data['polls']['index'] = $this->CI->pg_seo->url_template_transform('polls', 'index', $data["url_template"], 'base', 'xml');
		
		$this->CI->Seo_model->set_xml_route_file_content($xml_data);
		$this->CI->Seo_model->rewrite_route_php_file();
		
		$this->CI->pg_seo->delete_seo_module('polls');
	}

	/**
	 * Add demo data
	 * 
	 * @return void
	 */
	public function add_demo_content(){
		$this->CI->load->model('Polls_model');
		$demo_content = include MODULEPATH . 'polls/install/demo_content.php';
		// Associating languages id with codes
		foreach($this->CI->pg_language->languages as $l) {
			$lang[$l['code']] = $l['id'];
		}

		foreach($demo_content as $data) {
			$poll_data = array();
			$answer_data = array();
			// Replace language code with ID
			foreach($data['question'] as $l => $question) {
				$poll_data['question'][$lang[$l]] = $question;
				unset($poll_data['question'][$l]);
			}
			// Same for the answers
			foreach($data['answers'] as $number => $answer) {
				foreach($answer as $key => $value) {
					if('color' === $key) {
						$answer_data['answers_colors'][$number] = $value;
					} else {
						$answer_data['answers_languages'][$number . '_' . $lang[$key]] = $value;
					}
				}
			}
			$poll_data = $this->CI->Polls_model->validate_poll($poll_data);
			$answer_data = $this->CI->Polls_model->validate_answers($answer_data);
			$data['question'] = $poll_data['data']['question'];
			$data['answers_languages'] = $answer_data['data']['answers_languages'];
			$data['answers_colors'] = $answer_data['data']['answers_colors'];
			unset($data['answers']);
			$this->CI->Polls_model->save_poll($data);
		}
		return true;
	}
}

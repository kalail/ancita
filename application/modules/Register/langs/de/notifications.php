<?php

$install_lang["notification_contact_us_form"] = "Kontaktformular";
$install_lang["tpl_contact_us_form_content"] = "Kontakt Formular ausgefüllt::\n\n\n\n___________________________________\n\n\n\Grund: [reason];\n\nName: [user_name];\n\nBenutzer E-Mail: [user_email];\n\nBetreff: [subject];\n\nNachricht:\n\n[message] \n\n___________________________________";
$install_lang["tpl_contact_us_form_subject"] = "[domain] | Neues Kontaktformular";


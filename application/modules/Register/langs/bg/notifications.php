<?php

$install_lang["notification_contact_us_form"] = "Свържете се с нас";
$install_lang["tpl_contact_us_form_content"] = "Форма за връзка:\n\n\n\n___________________________________\n\n\n\nПричина: [reason];\n\nИме: [user_name];\n\nEmail: [user_email];\n\nТема: [subject];\n\nСъобщение:\n\n[message] \n\n___________________________________";
$install_lang["tpl_contact_us_form_subject"] = "[domain] | Свържете се с нас";


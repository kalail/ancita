	{if $form_data}
		{foreach item=item from=$form_data}
		{if $item.type eq 'section'}
			<div class="r section">
				<p>{$item.section.name}</p>
				{foreach item=field key=key from=$item.section.fields}
				<div class="r custom {$field.field.type} {$field.settings.search_type}">
					<div class="f">{$field.field_content.name}</div>
					<div class="v">{include file="helper_search_field_block.tpl" module=listings field=$field}</div>
				</div>
				{/foreach}
			</div>
		{else}
			<div class="r custom {$item.field.type} {$item.settings.search_type}">
				<div class="f">{$item.field_content.name}</div>
				<div class="v">{include file="helper_search_field_block.tpl" module=listings field=$item}</div>
			</div>
		{/if}
		{/foreach}
	{/if}


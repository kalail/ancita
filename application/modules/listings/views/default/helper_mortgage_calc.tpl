{js module=listings file='mortgage-calc.js'}
<div class="mortgage_calc_block edit_form">
	<div class="inside">
		<h2>{l i='mortgage_calc_title' gid='listings'}</h2>
		<form method="POST">
			<div class="r">
				<label>1.&nbsp;{l i='mortgage_calc_query1' gid='listings'}&nbsp;*</label>
				<input type="text" name="principal" size="15">
			</div>
			<div class="r">
				<label>2.&nbsp;{l i='mortgage_calc_query2' gid='listings'}&nbsp;*</label>
				<input type="text" name="intRate" size="15">
			</div>
			<div class="r">
				<label>3.&nbsp;{l i='mortgage_calc_query3' gid='listings'}&nbsp;*</label>
				<input type="text" name="numYears" size="15">
			</div>
			<div class="r">
				<input type="button" value="{l i='mortgage_calc_compute' gid='listings' type='button'}" id="calc_btn">
				<input type="button" value="{l i='mortgage_calc_reset' gid='listings' type='button'}" id="empty_btn">
			</div>
			<div class="r">
				<label>4.&nbsp;{l i='mortgage_calc_query4' gid='listings'}&nbsp;*</label>
				<input type="text" name="moPmt" size="15" dir="ltr">
				<input type="hidden" name="HmoPmt" value=0 size="15">
			</div>
			<p>{l i='mortgage_calc_create' gid='listings'}</p>
			<p><input type="button" value="{l i='mortgage_calc_createbutton' gid='listings' type='button'}" id="results_btn"></p>
			<p class="subtext">{l i='mortgage_calc_content2' gid='listings' type='button'}</p>
		</form>
	</div>
</div>
{capture assign='currency'}{strip}
	{block name='currency_output' module='start' value='%s'}
{/strip}{/capture}
<script>{literal}
	$(function(){
		new mortgageCalc({
			siteUrl: '{/literal}{$site_root}{literal}',
			selectedCurrAbbr: '{/literal}{$currency|strip_tags}{literal}',
		});
	});
{/literal}</script>

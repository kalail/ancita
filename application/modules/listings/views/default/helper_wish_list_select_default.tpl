<div id="wish_list_select_{$select_data.rand}" class="wish-list-select">
	<span id="wish_list_text_{$select_data.rand}">
	{foreach item=item from=$select_data.selected}
	{$item.output_name|truncate:50} {if $select_data.max_select ne 1}<br>{/if}<input type="hidden" name="{$select_data.var_name|escape}{if $select_data.max_select ne 1}[]{/if}" value="{$item.id}">
	{/foreach}
	</span>
	<a href="#" id="wish_list_link_{$select_data.rand}">{l i='link_manage_wish_lists' gid='listings'}</a> {if $select_data.max_select > 1}<i>({l i='max_wish_lists_select' gid='users'}: {$select_data.max_select})</i>{/if}<br>
	<div class="clr"></div>
</div>
{js module=listings file='wish-lists-select.js'}
<script>{literal}
$(function(){
	new wishListsSelect({
		siteUrl: '{/literal}{$site_root}{literal}',
		selected_items: [{/literal}{$select_data.selected_str}{literal}],
		max: '{/literal}{$select_data.max_select}{literal}',
		var_name: '{/literal}{$select_data.var_name}{literal}',
		template: '{/literal}{$select_data.template}{literal}',
		params: {{/literal}{foreach item=item key=key from=$select_data.params}{$key}:'{$item}',{/foreach}{literal}},
		rand: '{/literal}{$select_data.rand}{literal}',		
	});
});
{/literal}</script>

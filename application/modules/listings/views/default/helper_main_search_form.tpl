<script> 
	var selects_{$form_settings.rand} = []; 
	var checkboxes_{$form_settings.rand} = []; 
	var form_selects_{$form_settings.rand} = []; 
	var form_checkboxes_{$form_settings.rand} = []; 
	var selectObject_{$form_settings.rand};
	var checkboxObject_{$form_settings.rand};
	var formSelectObject_{$form_settings.rand};
	var formCheckboxObject_{$form_settings.rand};
</script>
<form action="" method="{if $listings_page_data.type eq 'line'}GET{else}POST{/if}" id="main_search_form_{$form_settings.rand}">
<div class="search-form {$listings_page_data.type|escape}">
	<div class="inside">
	{if $data.id_category and $data.property_type}
		{assign var='property_type_value' value=$data.id_category+'_'+$data.property_type}
	{else}
		{assign var='property_type_value' value=$data.id_category}
	{/if}
	{if $listings_page_data.type eq 'line'}
		{l i='field_search_category' gid='listings' assign='default_select_lang'}
		<div id="line-search-form-{$form_settings.rand}">
			{strip}
			<span>
			<input type="text" name="filters[keyword]" value="{$data.keyword|escape}">
			{selectbox input='filters[category]' id='id_category_select_'+$form_settings.rand value=$property_types subvalue=$property_items selected=$property_type_value default=$default_select_lang}
			<script> selectDropdownClass_{$form_settings.rand} = 'dropdown right'; selects_{$form_settings.rand}.push('id_category_select_{$form_settings.rand}'); </script>
			<input type="submit" class="search with-icon-small i-search w no-hover" id="main_search_button_{$form_settings.rand}" value="">
			</span>
			<a href="{$search_action_link}" class="options">{l i='link_more_options' gid='start'}</a>
			{/strip}
		</div>
	{elseif $listings_page_data.type eq 'slider'}
		{l i='select_default' gid='start' assign='default_select_lang'}
		<div class="btn-block">
			<div id="search-preresult-{$form_settings.rand}" class="preload"></div>
			<div class="search-btn">
				<ins class="with-icon-small i-lupe"></ins>
				<input type="submit" id="main_search_button_{$form_settings.rand}" name="search_button" value="{l i='btn_search' gid='start' type='button'}">
			</div>
		</div>
		<div class="fields-block">
			<div id="slider-search-form-{$form_settings.rand}">
				<div class="search-field country">
					{l i='field_search_country' gid='listings' assign='location_default'}
					{country_input select_type='city' id_country=$data.id_country id_region=$data.id_region id_city=$data.id_city id_district=$data.id_district default=$location_default var_country='filters[id_country]' var_region='filters[id_region]' var_city='filters[id_city]' var_district='filters[id_district]'}
				</div>				
				<div class="search-field category">
					{l i='field_search_category' gid='listings' assign='property_type_default'}
					{selectbox input='filters[category]' id='id_category_select_'+$form_settings.rand value=$property_types subvalue=$property_items selected=$property_type_value default=$property_type_default}
					<script> selects_{$form_settings.rand}.push('id_category_select_{$form_settings.rand}'); </script>
				</div>	
			</div>
			<div class="clr"></div>
		</div>
		<input type="hidden" name="filters[type]" value="{$form_settings.object|escape}" id="operation_type{$form_settings.rand}">
		<input type="hidden" name="new" value="1">
	{else}
		{l i='field_search_category' gid='listings' assign='default_select_lang'}
		<div class="btn-block">
			<div id="search-preresult-{$form_settings.rand}" class="preload"></div>
			<div class="search-btn">
				<ins class="with-icon-small i-lupe"></ins>
				<input type="submit" id="main_search_button_{$form_settings.rand}" name="search_button" value="{l i='btn_search' gid='start' type='button'}">
			</div>
			<a href="#" id="more-options-link-{$form_settings.rand}" {if $listings_page_data.type ne 'short'}class="hide"{/if}>{l i='link_more_options' gid='start'}</a>
			<a href="#" id="less-options-link-{$form_settings.rand}" {if $listings_page_data.type eq 'short'}class="hide"{/if}>{l i='link_less_options' gid='start'}</a>
		</div>
		<div class="fields-block">
			<div id="short-search-form-{$form_settings.rand}">
				<div class="search-field country">
					<p>{l i='field_search_country' gid='listings'}</p>
					{country_input select_type='city' id_country=$data.id_country id_region=$data.id_region id_city=$data.id_city id_district=$data.id_district var_country='filters[id_country]' var_region='filters[id_region]' var_city='filters[id_city]' var_district='filters[id_district]'}
				</div>				
				<div class="search-field category">
					<p>{l i='field_search_category' gid='listings'}</p>
					{selectbox input='filters[category]' id='id_category_select_'+$form_settings.rand value=$property_types subvalue=$property_items selected=$property_type_value default=$default_select_lang}
					<script>selects_{$form_settings.rand}.push('id_category_select_{$form_settings.rand}');</script>
					<script>{literal}
						$(function(){
							$('#id_category_select_{/literal}{$form_settings.rand}{literal}').bind('change', function(){
								$.ajax({
									url: '{/literal}{$site_root}{literal}' + '{/literal}listings/ajax_get_main_search_form/{$form_settings.rand}{literal}', 
									type: 'POST',
									data: {type: '{/literal}{$form_settings.object}{literal}', category: $(this).val()},
									cache: false,
									dataType: 'json',
									success: function(data){
										form_selects_{/literal}{$form_settings.rand}{literal} = [];
										form_checkboxes_{/literal}{$form_settings.rand}{literal} = [];
										$('#main_search_form_{/literal}{$form_settings.rand}{literal}').find('.custom').remove();
										if(formSelectObject_{/literal}{$form_settings.rand}{literal}) formSelectObject_{/literal}{$form_settings.rand}{literal}.clear();
										$('#short-search-form-{/literal}{$form_settings.rand}{literal}').append(data.short);	
										$('#full-search-form-{/literal}{$form_settings.rand}{literal}').prepend(data.full);	
										formSelectObject_{/literal}{$form_settings.rand}{literal} = new selectBox({elementsIDs: form_selects_{/literal}{$form_settings.rand}{literal}, selectDropdownClass: 'dropdown'});
										formCheckboxObject_{/literal}{$form_settings.rand}{literal} = new checkBox({elementsIDs: form_checkboxes_{/literal}{$form_settings.rand}{literal}});
									}
								});
							});
						});
					{/literal}</script>
				</div>
				<div class="search-field periodbox {if $form_settings.object ne 'rent' && $form_settings.object ne 'lease'}hide{/if}" id="booking_date_start">
					<p>{l i='field_booking_date_start' gid='listings'}</p>
					<input type="text" name="booking_date_start" value="{if $data.booking_date_start|strtotime>0}{$data.booking_date_start|date_format:$listings_page_data.date_format|escape}{/if}" id="date_start{$form_settings.rand}" class="short">
					<input type="hidden" name="filters[booking_date_start]" value="{if $data.booking_date_start|strtotime>0}{$data.booking_date_start|date_format:'%Y-%m-%d'|escape}{/if}" id="alt_date_start{$form_settings.rand}">
					<script>{literal}
						$(function(){
							$('#date_start{/literal}{$form_settings.rand}{literal}').datepicker({dateFormat: '{/literal}{$listings_page_data.datepicker_date_format}{literal}', altFormat: 'yy-mm-dd', altField: '#alt_date_start{/literal}{$form_settings.rand}{literal}', showOn: 'both'});
						});
					{/literal}</script>
				</div>
				<div class="search-field periodbox {if $form_settings.object ne 'rent' && $form_settings.object ne 'lease'}hide{/if}" id="booking_date_end">
					<p>{l i='field_booking_date_end' gid='listings'}</p>
					<input type="text" name="booking_date_end" value="{if $data.booking_date_end|strtotime>0}{$data.booking_date_end|date_format:$listings_page_data.date_format|escape}{/if}" id="date_end{$form_settings.rand}" class="short">
					<input type="hidden" name="filters[booking_date_end]" value="{if $data.booking_date_end|strtotime>0}{$data.booking_date_end|date_format:'%Y-%m-%d'|escape}{/if}" id="alt_date_end{$form_settings.rand}">
					<script>{literal}
						$(function(){
							$('#date_end{/literal}{$form_settings.rand}{literal}').datepicker({dateFormat: '{/literal}{$listings_page_data.datepicker_date_format}{literal}', altFormat: 'yy-mm-dd', altField: '#alt_date_end{/literal}{$form_settings.rand}{literal}', showOn: 'both'});
						});
					{/literal}</script>
				</div>	
				
				<div class="search-field guests-box {if $form_settings.object ne 'rent' && $form_settings.object ne 'lease'}hide{/if}" id="booking_guests">
					<p>{l i='field_booking_guests' gid='listings'}</p>
					{ld i='booking_guests' gid='listings' assign='booking_guests'}
					{selectbox input='filters[booking_guests]' id='booking_guests_select_'+$form_settings.rand value=$booking_guests.option selected=$data.booking_guests default=$booking_guests.header}
					<script> selects_{$form_settings.rand}.push('booking_guests_select_{$form_settings.rand}'); </script>
				</div>	
				
				<div class="search-field price-range">
					<p>{l i='field_price_range' gid='listings'}</p>
					{capture assign='price_min'} <input type="text" name="filters[price_min]" class="{if $form_settings.object ne 'rent' && $form_settings.object ne 'lease'}short{else}mini{/if}" value="{$data.price_min|escape}"> {/capture}
					{capture assign='price_max'} <input type="text" name="filters[price_max]" class="{if $form_settings.object ne 'rent' && $form_settings.object ne 'lease'}short{else}mini{/if}" value="{$data.price_max|escape}"> {/capture}
					{block name='currency_output' module='start' value=$price_min} &nbsp;{l i='text_to' gid='listings'}&nbsp;
					{block name='currency_output' module='start' value=$price_max}
					<input type="hidden" name="short_name" value="filters[price]">
				</div>
				
				{if $form_settings.object ne 'rent' && $form_settings.object ne 'lease'}
					{$main_search_extend_form}
				{/if}
			</div>
			<div id="full-search-form-{$form_settings.rand}" {if $listings_page_data.type eq 'short'}class="hide"{/if}>
				{if $form_settings.object eq 'rent' || $form_settings.object eq 'lease'}
					{$main_search_extend_form}
				{/if}
				
				{$main_search_full_form}
				
				{if $listings_page_data.use_advanced}
				<div class="search-field">
					<p>{l i='field_postal_code' gid='listings'}:</p>
					<input type="text" name="filters[zip]" value="{$data.zip|escape}">
				</div>
		
				<!-- div class="search-field">
					<p>{l i='field_radius' gid='listings'}:</p>
					{l i='text_radius_select' gid='listings' assign='default_radius_select'}
					{selectbox input='filters[radius]' id='radius_select_'+$form_settings.rand value=$radius_data.option selected=$data.radius default=$default_radius_select}
					<script>selects_{$form_settings.rand}.push('radius_select_{$form_settings.rand}');</script>
				</div -->
				
				<div class="search-field">
					<p>{l i='field_id' gid='listings'}</p>
					<input type="text" name="filters[id]" value="{$data.id|escape}">
				</div>
					
				<div class="search-field checkboxes">
					<p>&nbsp;</p>
					{if $form_settings.object ne 'buy' && $form_settings.object ne 'lease'}
					{l i='field_with_photo' gid='listings' assign='with_photo_value'}
					{checkbox input='filters[with_photo]' id='with_photo_select_'+$form_settings.rand value=$with_photo_value selected=$data.with_photo}
					<script>checkboxes_{$form_settings.rand}.push('with_photo_select_{$form_settings.rand}');</script>
					{/if}
					
					{l i='field_open_house' gid='listings' assign='open_house_value'}
					{checkbox input='filters[by_open_house]' id='open_house_select_'+$form_settings.rand value=$open_house_value selected=$data.open_house}
					<script>checkboxes_{$form_settings.rand}.push('open_house_select_{$form_settings.rand}');</script>
					
					{l i='field_by_private' gid='listings' assign='by_private_value'}
					{checkbox input='filters[by_private]' id='by_private_select_'+$form_settings.rand value=$by_private_value selected=$data.by_private}
					<script> checkboxes_{$form_settings.rand}.push('by_private_select_{$form_settings.rand}'); </script>
				</div>
				{/if}
			</div>
			<input type="hidden" name="filters[type]" value="{$form_settings.object|escape}">
			<input type="hidden" name="form" value="main_search_form">
			<input type="hidden" name="new" value="1">
		</div>
	{/if}
	</div>
</div>
</form>
<script>{literal}
	$(function(){
		if(selectObject_{/literal}{$form_settings.rand}{literal}) selectObject_{/literal}{$form_settings.rand}{literal}.clear();
		selectObject_{/literal}{$form_settings.rand}{literal} = new selectBox({elementsIDs: selects_{/literal}{$form_settings.rand}{literal}, selectDropdownClass: 'dropdown'});
		checkboxObject_{/literal}{$form_settings.rand}{literal} = new checkBox({elementsIDs: checkboxes_{/literal}{$form_settings.rand}{literal}});
		if(formSelectObject_{/literal}{$form_settings.rand}{literal}) formSelectObject_{/literal}{$form_settings.rand}{literal}.clear();
		formSelectObject_{/literal}{$form_settings.rand}{literal} = new selectBox({elementsIDs: form_selects_{/literal}{$form_settings.rand}{literal}, selectDropdownClass: 'dropdown'});
		formCheckboxObject_{/literal}{$form_settings.rand}{literal} = new checkBox({elementsIDs: form_checkboxes_{/literal}{$form_settings.rand}{literal}});
		
		$('#main_search_form_{/literal}{$form_settings.rand}{literal}').bind('submit', function(){
			var url = '{/literal}{$site_root}{literal}' + 'listings/ajax_search';
			$.post(url, $(this).serialize(), function(data){
				window.location.href = data;
			});
			return false;
		});
	});
{/literal}</script>

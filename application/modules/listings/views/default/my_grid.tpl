{include file="header.tpl"}

{include file="left_panel.tpl" module=start}

<div class="rc">
	<div class="content-block">
		<h1>{l i='header_my_listings' gid='listings'}</h1>

		<div class="search-links">
			{l i='link_search_in_my_lsitings' gid='listings'}
			<div class="edit_block">
				<div class="fright"><a class="btn-link" href="{$site_url}listings/edit"><ins class="with-icon i-expand"></ins>{l i='link_add_listing' gid='listings'}</a></div>
				<form action="" method="post" enctype="multipart/form-data" id="listings_search_form">
				<div class="r">
					<input type="text" name="name" value="{$page_data.name|escape}" id="listings_search" autocomplete="off" />
				</div>
				</form>
			</div>
		</div>

		<div id="search_my_listings_form" class="hide"></div>

		<div id="listings_block">{$block}</div>
		{js module=listings file='listings-list.js'}
		<script>{literal}
		$(function(){
			new listingsList({
				siteUrl: '{/literal}{$site_root}{literal}',
				viewUrl: 'listings/my',
				viewAjaxUrl: 'listings/ajax_my',
				tIds: ['pages_block_1', 'pages_block_2', 'sorter_block']
			});
		});
	{/literal}</script>
	</div>
</div>
<div class="clr"></div>
{include file="footer.tpl"}

{l i='no_information' gid='start' assign='no_info_str'}
{if $section_gid == 'overview'}
	<div class="qr-code"><img alt="{l i='text_qrcode' gid='listings' type='button'}" title="{l i='link_qrcode' gid='listings' type='button'}" src="{$data.qr_code}" id="qr_code_img"></div>
	
	{if $data.operation_type eq 'rent' || $data.operation_type eq 'lease'}
	<div class="r">
		<div class="f">{l i='field_price_type' gid='listings'}:</div>
		<div class="v">
			{ld_option i='price_type' gid='listings' option=$data.price_type}
		</div>
	</div>
	{/if}
	
	<div class="r">
		<div class="f">{l i='field_square' gid='listings'}:</div>
		<div class="v">{$data.square_output}</div>
	</div>
	
	{if $data.id_district}
	<div class="r">
		<div class="f">{l i='field_district' gid='listings'}:</div>
		<div class="v">{$data.district}</div>
	</div>
	{/if}
	
	{if $data.operation_type eq 'rent'}
		{if $data.calendar_period_min}
		<div class="r">
			<div class="f">{l i='field_calendar_period_min' gid='listings'}:</div>
			<div class="v">{$data.calendar_period_min} {ld_option i='price_period_unit' gid='listings' option=$data.price_period}</div>
		</div>
		{/if}
		{if $data.calendar_period_max}
		<div class="r">
			<div class="f">{l i='field_calendar_period_max' gid='listings'}:</div>
			<div class="v">{$data.calendar_period_max} {ld_option i='price_period_unit' gid='listings' option=$data.price_period}</div>
		</div>
		{/if}
	{/if}
	
	<div class="r">
		<div class="f">{l i='field_price_auction' gid='listings'}:</div>
		<div class="v">
			{if $data.price_auction}
			{l i='option_checkbox_yes' gid='start'}
			{else}
			{l i='option_checkbox_no' gid='start'}
			{/if}
		</div>
	</div>
	{if $data.date_available|strtotime > 0}
	<div class="r">
		<div class="f">{l i='field_date_available' gid='listings'}:</div>
		<div class="v">{$data.date_available|date_format:$date_format}</div>
	</div>
	{/if}
	{if $data.date_open|strtotime > 0}
	<div class="r">
		<div class="f">{l i='field_date_open' gid='listings'}:</div> 
		<div class="v">
			{$data.date_open|date_format:$date_format} 
			{if $data.date_open_begin}{ld_option i='dayhour-names' gid='start' option=$data.date_open_begin}{/if}
			{if $data.date_open_end}{if $data.date_open_begin} - {/if}{ld_option i='dayhour-names' gid='start' option=$data.date_open_end}{/if}
		</div>
	</div>
	{/if}
	{if $sections_data_count > 2}
	{foreach item=section from=$sections_data}
		{capture assign='section_content'}{strip}
		{foreach item=item key=field_gid from=$data.field_editor}
		{if $item.section_gid eq $section.gid && ($item.value|is_array and $item.value|count or $item.value|trim or $item.value_original|trim)}
		<div class="r">
			<div class="f">{$item.name}: </div>
			<div class="v">
				{if $item.value_str}
					{$item.value_str}
				{elseif $item.value}
					{$item.value}
				{else}
					{$item.value_original}
				{/if}
				{if $field_gid eq 'live_square_1' || $field_gid eq 'live_square_4'}{$data.square_unit_str}{/if}
			</div>
		</div>
		{/if}
		{/foreach}
		{/strip}{/capture}
		{if $section_content}<h2>{$section.name}</h2>{$section_content}{/if}
	{/foreach}
	{else}
	{foreach item=item key=field_gid from=$data.field_editor}
	{if ($item.value|is_array and $item.value|count or $item.value|trim or $item.value_original|trim)}
	<div class="r">
		<div class="f">{$item.name}: </div>
		<div class="v">
			{if $item.value_str}
				{$item.value_str}
			{elseif $item.value}
				{$item.value}
			{else}
				{$item.value_original}
			{/if}
			{if $field_gid eq 'live_square_1' || $field_gid eq 'live_square_4'}{$data.square_unit_str}{/if}
		</div>
	</div>
	{/if}
	{/foreach}	
	{/if}
{/if}
{if $section_gid == 'gallery'}
	{l i='text_listing_photo' gid='listings' assign='text_listing_photo' property_type=$data.property_type_str operation_type=$data.operation_type_str location=$data.location}
	{l i='text_listing_photo_number' gid='listings' assign='text_listing_photo_number' property_type=$data.property_type_str operation_type=$data.operation_type_str location=$data.location}
	<div class="photo-view">
		{if $data.photo_count > 1}
		{js file='dualslider/jquery.dualSlider.0.3.min.js'}
		{js file='dualslider/jquery.timers-1.2.js'}
		<div class="slider">
			<div class="carousel" title="{$text_listing_photo|escape}">	
				<div class="backgrounds">
					{foreach item=item key=key from=$data.photos}
						{math equation="x+1" x=$key assign='photo_number'}
						<div class="item item_{$key}" title="{$text_listing_photo_number|replace:'[number]':$photo_number|escape}" style="background: url('{$item.media.thumbs.620_400}') no-repeat;"></div>
					{/foreach}
				</div>
				
				<div class="paging_wrapper">
					<div class="paging_wrapper2">
						<div class="paging">
							<a id="previous_item" class="previous hide" alt="{l i='text_nav_prev_photo' gid='listings' type='button'}" title="{l i='link_nav_prev_photo' gid='listings' type='button'}">{l i='text_nav_prev_photo' gid='listings'}</a>
							<a id="next_item" class="next hide" alt="{l i='text_nav_next_photo' gid='listings' type='button'}" title="{l i='link_nav_next_photo' gid='listings' type='button'}">{l i='text_nav_next_photo' gid='listings'}</a>
							<span id="numbers" class="hide"><a href="#" rel=""></a></span>
						</div>
					</div>
				</div>	
				
				<div class="panel">
					<div class="details_wrapper">
						<div class="details">
							{foreach item=item key=key from=$data.photos}
							<div class="detail">
								{if $item.comment}
								<div class="photo-comment hide">
									<div class="comment-panel">{$item.comment|truncate:255}</div>
									<div class="background"></div>
								</div>
								{/if}
							</div>
							{/foreach}
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<script>{literal}
			$(function(){
				$(".slider .carousel").dualSlider({
					auto: {/literal}{if $page_data.slider_auto}true{else}false{/if}{literal},
					autoDelay: {/literal}{if $page_data.slider_auto}{$page_data.rotation*1000}{else}false{/if}{literal},
					easingCarousel: "swing",
					easingDetails: "swing",
					durationCarousel: 700,
					durationDetails: 300,
					widthsliderimage: $(".slider.carousel .backgrounds .item").width(),
					{/literal}{if $_LANG.rtl eq 'rtl'}rtl: true,{/if}{literal}
				});
				$('.slider .backgrounds .item').bind('click', function(){
					var next = $('#next_item');
					if(next.css('display') != 'none') next.trigger('click');
				});
				$('#content_m_gallery .slider .carousel .photo-comment').show();
			});
		{/literal}</script>
		{elseif $data.photo_count eq 1}
		<img src="{$data.photos.0.media.thumbs.620_400}" alt="{l i='text_listing_photo' gid='listings'}" title="{l i='text_listing_photo' gid='listings'}">
		{else}
		<img src="{$data.photo_default.media.thumbs.620_400}" alt="{l i='text_no_photo' gid='listings' type='button'}" title="{l i='text_no_photo' gid='listings' type='button'}">
		{/if}
	</div>
	
	{if $data.photo_count > 1}
	{if $data.photo_count > $page_data.visible}
	{js file='jcarousellite.min.js'}
	{js file='init_carousel_controls.js'}
	<script>{literal}
		$(function(){
			var rtl = 'rtl' === '{/literal}{$_LANG.rtl}{literal}';
			var idPrev, idNext;
			if(!rtl) {
				idNext = '#directionright{/literal}{$page_data.rand}{literal}';
				idPrev = '#directionleft{/literal}{$page_data.rand}{literal}';
			} else {
				idNext = '#directionleft{/literal}{$page_data.rand}{literal}';
				idPrev = '#directionright{/literal}{$page_data.rand}{literal}';
			};
			$('#listings_carousel .carousel_block{/literal}{$page_data.rand}{literal}').jCarouselLite({
				rtl: rtl,
				visible: {/literal}{$page_data.visible}{literal},
				btnNext: idNext,
				btnPrev: idPrev,
				circular: false,
				afterEnd: function(a) {
					var index = $(a[0]).index();
					carousel_controls{/literal}{$page_data.rand}{literal}.update_controls(index);
				}
			});
			carousel_controls{/literal}{$page_data.rand}{literal} = new init_carousel_controls({
				rtl: rtl,
				carousel_images_count: {/literal}{$page_data.visible}{literal},
				carousel_total_images: {/literal}{$data.photo_count}{literal},
				btnNext: idNext,
				btnPrev: idPrev
			});
		});
	{/literal}</script>	
	{/if}	
	<script>{literal}
		$(function(){
			$('#previous_item').bind('click', function(){
				$('#listings_carousel li.active').removeClass('active').prev().addClass('active');
			});
			$('#next_item').bind('click', function(){
				$('#listings_carousel li.active').removeClass('active').next().addClass('active');
			});
			$('#listings_carousel img').bind('click', function(){
				var control = $('#numbers a');
				if(control.length){
					control.attr('rel', $(this).attr('rel')); control.trigger('click');
					$('#listings_carousel li').removeClass('active');
					$(this).parent().addClass('active');
				}
			});
		});
	{/literal}</script>	
	<div id="listings_carousel" class="carousel">
		<div id="directionleft{$page_data.rand}" class="directionleft {if $data.photo_count <= $page_data.visible}hide{/if}">
			<div class="with-icon i-larr w" id="l_hover"></div>
		</div>
		<div class="carousel_block carousel_block{$page_data.rand} item_{$page_data.visible}_info">
			<ul>
				{counter print=false assign=counter start=0}
				{foreach item=item key=key from=$data.photos}
				{counter print=false assign=counter}
				{l i='text_listing_photo_number' gid='listings' assign='photo_alt'}
				{l i='link_listing_photo_number' gid='listings' assign='photo_title'}
				<li {if $counter eq 1}class="active"{/if}><img src="{$item.media.thumbs.60_60}" id="listing_photo{$key}" rel="{$counter}" alt="{$photo_alt|replace:'[number]':$counter|escape}" title="{$photo_title|replace:'[number]':$counter|escape}"></li>
				{/foreach}
			</ul>
		</div>
		<div id="directionright{$page_data.rand}" class="directionright">
			<div class="with-icon i-rarr w" id="r_hover"></div>
		</div>
		<div class="clr"></div>		
	</div>
	<div class="clr"></div>
	{/if}
{/if}
{if $section_gid == 'print'}
	{if $data.photos > 0}
	<div class="tabs tab-size-15">
		<ul>
			<li>{l i='filter_section_gallery' gid='listings'}</li>
		</ul>
	</div>
	<div class="photos">
		<table>
			<tr>
			{counter print=false assign=counter start=1}
			{foreach item=item key=key from=$data.photos}
			{if $counter > 0 && $counter is div by 4}</tr><tr>{/if}
			<td><img src="{$item.media.thumbs.200_200}"></td>
			{counter}
			{/foreach}
			</tr>
		</table>
		<div class="clr"></div>
	</div>
	{/if}
{/if}
{if $section_gid == 'virtual_tour'}
	{if $data.virtual_tour_count}
	<div id="panorama_block">{block name=virtual_tour_block module=listings data=$data.virtual_tour.0}</div>
	
	{js file='jcarousellite.min.js'}
	{js file='init_carousel_controls.js'}
	<script>{literal}
		$(function(){
			var rtl = 'rtl' === '{/literal}{$_LANG.rtl}{literal}';
			var idVtourPrev, idVtourNext;
			if(!rtl){
				idVtourNext = '#directionright_vtour_{/literal}{$page_data.rand}{literal}';
				idVtourPrev = '#directionleft_vtour_{/literal}{$page_data.rand}{literal}';
			} else {
				idVtourNext = '#directionleft_vtour_{/literal}{$page_data.rand}{literal}';
				idVtourPrev = '#directionright_vtour_{/literal}{$page_data.rand}{literal}';
			};
			$('#listings_vtour_carousel .carousel_block_vtour_{/literal}{$page_data.rand}{literal}').jCarouselLite({
				rtl: rtl,
				visible: {/literal}{$page_data.visible}{literal},
				btnNext: idVtourNext,
				btnPrev: idVtourPrev,
				circular: false,
				afterEnd: function(a) {
					var index = $(a[0]).index();
					carousel_controls_vtour_{/literal}{$page_data.rand}{literal}.update_controls(index);
				}
			});
			carousel_controls_vtour_{/literal}{$page_data.rand}{literal} = new init_carousel_controls({
				rtl: rtl,
				carousel_images_count: {/literal}{$page_data.visible}{literal},
				carousel_total_images: {/literal}{$data.virtual_tour_count}{literal},
				btnNext: idVtourNext,
				btnPrev: idVtourPrev,
			});
			
			$('#listings_vtour_carousel img').bind('click', function(){
				$('#listings_vtour_carousel li').removeClass('active');
				$(this).parent().addClass('active');
			});
		});
	{/literal}</script>
	<div id="listings_vtour_carousel" class="carousel {if $data.virtual_tour_count <= $page_data.visible}visible{/if}">
		<div id="directionleft_vtour_{$page_data.rand}" class="directionleft">
			<div class="with-icon i-larr w" id="l_hover"></div>
		</div>
		<div class="carousel_block carousel_block_vtour_{$page_data.rand} item_{$page_data.visible}_info">
			<ul>
				{counter print=false assign=counter start=0}
				{foreach item=item key=key from=$data.virtual_tour}
				{counter print=false assign=counter}
				<li {if $counter eq 1}class="active"{/if}><img src="{$item.media.thumbs.60_60}" id="listing_panorama{$key}" rel="{$counter}" class="panorama" data-url="{$item.media.url}" data-file="{$item.media.thumbs.620_400|replace:$item.media.url:''}" data-width="{$item.settings.width}" data-height="{$item.settings.height}" data-comment="{$item.comment|escape}"></li>
				{/foreach}
			</ul>
		</div>
		<div id="directionright_vtour_{$page_data.rand}" class="directionright">
			<div class="with-icon i-rarr w" id="r_hover"></div>
		</div>
		<div class="clr"></div>		
	</div>
	<div class="clr"></div>
	{/if}
{/if}
{if $section_gid == 'map'}
	{block name=show_default_map module=geomap id_user=$user_id object_id=$data.id gid='listing_view' markers=$markers settings=$map_settings width='630' height='400' only_load_scripts=$map_only_load_scripts only_load_content=$map_only_load_content}
{/if}
{if $section_gid == 'panorama'}
	{if !$panorama_only_load_scripts}<div id="pano_container" class="pano_container"></div>{/if}
	{block name=show_default_map module=geomap id_user=$data.id_user gid='listing_view' markers=$markers settings=$map_settings width='630' height='400' only_load_scripts=$panorama_only_load_scripts only_load_content=$panorama_only_load_content}	
{/if}
{if $section_gid == 'reviews'}
	{block name=get_reviews_block module=reviews object_id=$data.id type_gid='listings_object'}
{/if}
{if $section_gid == 'video'}
	<div class="r">{$data.listing_video_content.embed}</div>
{/if}
{if $section_gid == 'file'}
	<h3>{$data.listing_file_name}</h3>
	{if $data.listing_file_comment}{$data.listing_file_comment}<br><br>{/if}
	
	<div class="download-bar">
		<a class="btn-link" title="{l i='field_file_download' gid='listings' type='button'}" href="{$data.listing_file_content.file_url}" target="blank"><ins class="with-icon i-download"></ins></a>
		{$data.listing_file_content.file_name}
	</div>	
{/if}
{if $section_gid == 'calendar'}
	{block name='listings_calendar_block' module='listings' listing=$data template='view' count=2}
	{block name='listings_booking_block' module='listings' listing=$data template='form' no_save=1}
{/if}
{if $field_editor_section}
	{foreach item=section_item  key=section_key from=$sections_data}
	<h2>{$section_item.name}</h2>	
		{foreach item=item from=$data.field_editor}
		{if $item.section_gid eq $section_item.gid}
		<div class="r">
			<div class="f">{$item.name}: </div>
			<div class="v">{$item.value}</div>
		</div>
		{/if}
		{/foreach}
	{/foreach}
{/if}


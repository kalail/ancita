{if $listings}
<h2>{l i='header_featured_listings' gid='listings'}</h2>
<div class="featured_listings_block">
	{foreach item=item key=key from=$listings}
	<div class="listing{if $key is div by 8} first{/if}">
		{l i='link_listing_view' gid='listings' type='button' assign='logo_title' id_ref=$item.id}
		{l i='text_listing_logo' gid='listings' type='button' assign='text_listing_logo' id_ref=$item.id property_type=$item.property_type_str operation_type=$item.operation_type_str location=$item.location}
		<a href="{$site_url}listings/view/{$item.id}"><img src="{$item.media.photo.thumbs.middle}" alt="{$text_listing_logo}" title="{$logo_title}"></a><br>{$item.output_name|truncate:30}
	</div>
	{/foreach}
</div>
{/if}

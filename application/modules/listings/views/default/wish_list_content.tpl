{foreach item=item from=$listings}
{counter print=false assign=counter}
<div id="item-block-{$item.listing.id}" class="item listing {if $counter is div by 4}last{/if}">	
	{l i='link_listing_view' gid='listings' type='button' assign='logo_title' id_ref=$item.listing.id}	
	{l i='text_listing_logo' gid='listings' type='button' assign='text_listing_logo' id_ref=$item.listing.id property_type=$item.listing.property_type_str operation_type=$item.listing.operation_type_str location=$item.listing.location}	
	<a href="{seolink module='listings' method='view' data=$item.listing}">		
		<img src="{$item.listing.media.photo.thumbs.middle}" alt="{$text_listing_logo}" title="{$logo_title}">
		{if $item.listing.photo_count || $item.listing.is_vtour}		
		<div class="photo-info">			
			<div class="panel">				
				{if $item.listing.photo_count}<span class="btn-link"><ins class="with-icon-small w i-photo"></ins> {$item.listing.photo_count}</span>{/if}				
				{if $item.listing.is_vtour}<span class="btn-link"><ins class="with-icon-small w i-vtour"></ins> 360&deg;</span>{/if}			
			</div>			
			<div class="background"></div>		
		</div>		
		{/if}	
	</a>	
	<a href="{seolink module='listings' method='view' data=$item.listing}">{$item.listing.output_name|truncate:30}</a>	
	<span>{block name=listing_price_block module='listings' data=$item.listing template='small'}</span>	
	<span>{$item.listing.property_type_str} {$item.listing.operation_type_str}</span>		
</div>	
{foreachelse}
<div class="item empty">{l i='no_listings' gid='listings'}</div>
{/foreach}<div class="clr"></div>
{if $listings}<div id="pages_block_2">{pagination data=$page_data type='full'}</div>{/if}

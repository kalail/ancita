{include file="header.tpl"}

<div class="rc_wrapper">
<div class="panel">
<div class="inside">

<div class="lc-1">
	<div class="content-block">
		<h1>{l i='header_listings_result' gid='listings'} - <span id="total_rows">{$page_data.total_rows}</span> {l i='header_listings_found' gid='listings'}</h1>

		<div class="tabs tab-size-15 noPrint">
			<ul id="user_listing_sections">
				{foreach item=tgid from=$operation_types}
				<li id="m_{$tgid}" sgid="{$tgid}" class="{if $current_operation_type eq $tgid}active{/if}"><a href="{$site_url}listings/user/{$user.id}/{$tgid}">{l i='operation_search_'+$tgid gid='listings'}</a></li>
				{/foreach}
			</ul>
			<div id="list_link"><a href="{$site_url}listings/set_view_mode/list" class="btn-link fright" title="{l i='link_view_list' gid='listings' type='button'}"><ins class="with-icon i-list"></ins></a></div>
		</div>
		
		<div class="sorter line" id="sorter_block">
			{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
			{if $listings}<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>{/if}
		</div>
		
		{block name=show_default_map module=geomap id_user=$data.id_user gid='listing_search' settings=$map_settings width='630' height='400' map_id='listings_map_full_container'}
		
		<div id="listings_map">{$block}</div>

		<div id="pages_block_2">{if $listings}{pagination data=$page_data type='full'}{/if}</div>
		
		{js module=listings file='listings-map.js'}
		<script>{literal}
			$(function(){
				new listingsMap({
					siteUrl: '{/literal}{$site_root}{literal}',
					mapAjaxUrl: '{/literal}listings/ajax_user/{$user.id}{literal}',
					sectionId: 'user_listing_sections',
					operationType: '{/literal}{$current_operation_type}{literal}',
					order: '{/literal}{$order}{literal}',
					orderDirection: '{/literal}{$order_direction}{literal}',
					page: {/literal}{$page}{literal},
					tIds: ['pages_block_1', 'pages_block_2', 'sorter_block'],
				});
			});
		{/literal}</script>
	</div>
</div>

<div class="rc-2">
	{block name="user_info" module="users" user=$user}
	{helper func_name=show_banner_place module=banners func_param='right-banner'}
	{helper func_name='show_mortgage_calc' module='listings'}
</div>

<div class="clr"></div>

</div>
</div>
</div>

{include file="footer.tpl"}

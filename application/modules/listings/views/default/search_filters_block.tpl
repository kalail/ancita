{if $current_search_data|count}
<ul>
	{foreach item=item key=key from=$current_search_data}
	<li title="{$item.name|escape}: {$item.label|escape}" {if $key eq 'price'}dir="ltr"{/if}>{$item.name|truncate:10:'...':true}: {$item.label|truncate:12:'...':true} <a href="{$site_url}listings/delete_search_criteria/{$key|escape}" class="btn-link small fright"><ins class="with-icon-small i-delete"></ins></a></li>
	{/foreach}
</ul>
{/if}

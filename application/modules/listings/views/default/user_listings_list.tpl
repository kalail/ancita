{include file="header.tpl"}

<div class="rc_wrapper">
<div class="panel">
<div class="inside">

<div class="lc-1">
	
	<div class="content-block">
		
		<h1>{l i='header_listings_result' gid='listings'} - <span id="total_rows">{$page_data.total_rows}</span> {l i='header_listings_found' gid='listings'}</h1>
		
		<div class="tabs tab-size-15 noPrint">
			<ul id="user_listing_sections">
				{foreach item=tgid from=$operation_types}
				<li id="m_{$tgid}" sgid="{$tgid}" class="{if $current_operation_type eq $tgid}active{/if}"><a href="{$menu_action_link|replace:'[operation_type]':$tgid}/modified/DESC/1">{l i='operation_search_'+$tgid gid='listings'}</a></li>
				{/foreach}
			</ul>
			<div id="map_link">{helper func_name=show_map_view module=geomap func_param=$site_url+'listings/set_view_mode/map'}</div>
		</div>
		
		<div id="listings_block">
			{$block}
		</div>
		
		{js module=listings file='listings-list.js'}
		<script>{literal}
		$(function(){
			new listingsList({
				siteUrl: '{/literal}{$site_root}{literal}',
				listAjaxUrl: '{/literal}listings/ajax_user/{$user.id}{literal}',
				sectionId: 'user_listing_sections',
				operationType: '{/literal}{$current_operation_type}{literal}',
				order: '{/literal}{$order}{literal}',
				orderDirection: '{/literal}{$order_direction}{literal}',
				page: {/literal}{$page}{literal},
				tIds: ['pages_block_1', 'pages_block_2', 'sorter_block'],
			});
		});
		{/literal}</script>
	</div>
	
</div>
<div class="rc-2">
	{block name="user_info" module="users" user=$user}
	{helper func_name=show_banner_place module=banners func_param='right-banner'}
	{helper func_name='show_mortgage_calc' module='listings'}
</div>

<div class="clr"></div>

</div>
</div>
</div>

{include file="footer.tpl"}

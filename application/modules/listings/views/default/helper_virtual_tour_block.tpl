{js file='jquery.reel.js'}
<div id="panorama_box_{$virtual_tour_rand}" class="panorama_box">
	{l i='text_listing_vtour' gid='listings' type='button' assign='text_listing_vtour' id_ref=$virtual_tour_data.id property_type=$virtual_tour_data.property_type_str operation_type=$virtual_tour_data.operation_type_str location=$virtual_tour_data.location}
	<img src="{$virtual_tour_data.media.thumbs.620_400}" hspace="3" id="panorama_{$virtual_tour_rand}" width="{$virtual_tour_width}" height="{$virtual_tour_height}" alt="{$text_listing_vtour}">
	<div class="panorama-comment {if !$virtual_tour_data.comment}hide{/if}">
		<div class="comment-panel" title="{$virtual_tour_data.comment}">{$virtual_tour_data.comment|truncate:255}</div>
		<div class="background"></div>
	</div>
</div>		
<script>{literal}
	$(function(){
		var panorama = $('.panorama');
		if(panorama.length){
			panorama.bind('click', function(){
				var vtour_box = $('#panorama_box_{/literal}{$virtual_tour_rand}{literal}');
				var vtour = vtour_box.find('#panorama_{/literal}{$virtual_tour_rand}{literal}');
				vtour.unreel();
				var item = $(this);
				var w = 400*item.attr('data-height')/item.attr('data-height');
				var comment = item.attr('data-comment') || '';
				vtour.reel({
					brake: 0,
					timeout: 0,
					cursor: 'pointer',
					path: item.attr('data-url'), 
					image: item.attr('data-file'), 
					suffix: '',
					indicator: 10,
					stitched: w,
					speed: -0.02,
					steppable: false,
					preloader: 0,
					frames: w,
				});
				if(comment){
					vtour_box.find('.panorama-comment').removeClass('hide');
				}else{
					vtour_box.find('.panorama-comment').addClass('hide');
				}
				vtour_box.find('.comment-panel').html(comment);
				return false;
			});
							
			var item = $(panorama.get(0));
			var w = 400*item.attr('data-height')/item.attr('data-height');
			$('#panorama_{/literal}{$virtual_tour_rand}{literal}').reel({
				brake: 0,
				timeout: 0,
				cursor: 'pointer',
				path: item.attr('data-url'), 
				image: item.attr('data-file'), 
				suffix: '', 
				indicator: 10,
				stitched: w,
				speed: -0.02,
				steppable: false,
				preloader: 0,
				frames: w,
			});
		}
	});
{/literal}</script>

<div class="content-block load_content">
	<h1>{l i='header_share' gid='listings'}</h1>
	<div class="inside edit_block">
		{include file="share_form.tpl" module=listings}
	</div>
</div>
<script>{literal}
	var pMenu;
	$(function(){
		pMenu = new listingsMenu({
			siteUrl: '{/literal}{$site_root}{literal}',
			idListing: '{/literal}{$data.id}{literal}',
			listBlockId: 'share_block',
			sectionId: 'share_sections',
			currentSection: 'm_send_email',
		});
	});
{/literal}</script>


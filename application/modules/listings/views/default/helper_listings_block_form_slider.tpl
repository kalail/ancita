{if $listings}
{js file='dualslider/jquery.dualSlider.0.3.min.js'}
{js file='dualslider/jquery.timers-1.2.js'}

<script>var forms_{$slider_form_settings.rand} = {literal}{}{/literal};</script>
<div class="slider_{$view|escape}">
	<div class="slider_wrapper">
		<div class="slider_wrapper2">
			<div class="slider">	
		
				<div class="carousel">		
					<div class="backgrounds_wrapper">
						<div class="backgrounds_wrapper2">
							<div class="backgrounds_wrapper3">
								<div class="backgrounds">						
									{foreach item=item key=key from=$listings}
									{l i='text_listing_logo' gid='listings' type='button' assign='text_listing_logo' id_ref=$item.id property_type=$item.property_type_str operation_type=$item.operation_type_str location=$item.location}
									<div class="item item_{$key|escape}" style="background: url('{$item.media.slider.thumbs[$view]}') no-repeat;" title="{$text_listing_logo}"></div>							
									{/foreach}
								</div>
							</div>	
						</div>
					</div>
						
					<div class="gradient_wrapper">
						<div class="gradient_wrapper2">
							<div class="gradient">
								<div class="gradient-l"></div>
								<div class="gradient-r"></div>
							</div>
						</div>
					</div>
					
					{if $slider_form_page_data.count > 1}
					<div class="paging_wrapper">
						<div class="paging_wrapper2">
							<div class="paging">
								<a id="previous_item" class="previous" alt="{l i='nav_prev' gid='start' type='button'}" title="{l i='nav_prev' gid='start' type='button'}">{l i='nav_prev' gid='start'}</a>
								<a id="next_item" class="next" alt="{l i='nav_next' gid='start' type='button'}" title="{l i='nav_next' gid='start' type='button'}">{l i='nav_next' gid='start'}</a>
							</div>
						</div>
					</div>	
					{/if}
					
					<div class="panel">
						<div class="details_wrapper">
							<div class="details">
								{foreach item=item key=key from=$listings}
								<div class="listing detail">
									<a href="{seolink module='users' method='view' data=$item.user}"><img src="{$item.user.media.user_logo.thumbs.small}" alt="{$item.user.output_name|escape}"></a>
									<a href="{seolink module='users' method='view' data=$item.user}">{$item.user.output_name|truncate:50}</a>							
									<a href="{seolink module='listings' method='view' data=$item}">{if $view eq '654_395'}{$item.output_name|truncate:50}{else}{$item.output_name|truncate:100}{/if}</a>
									<span>{$item.property_type_str} {$item.operation_type_str}</span>
									<span>{block name=listing_price_block module='listings' data=$item template='small'}</span>
								</div>
								{/foreach}
							
							</div>
							<div class="details_background"></div>
						</div>
					</div>
					
					<div class="slider_search_form">
						{$slider_search_form}
						<div class="background"></div>
					</div>		
				</div>	
				
			</div>
		</div>
	</div>
</div>
<script>{literal}
	$(function(){
		$(".carousel").dualSlider({
			auto: {/literal}{if $slider_form_page_data.slider_auto}true{else}false{/if}{literal},
			autoDelay: {/literal}{if $slider_form_page_data.slider_auto}{$slider_form_page_data.rotation*1000}{else}false{/if}{literal},
			easingCarousel: "swing",
			easingDetails: "swing",
			durationCarousel: 700,
			durationDetails: 300,
			widthsliderimage: $(".carousel .backgrounds .item").width(),
			{/literal}{if $_LANG.rtl eq 'rtl'}rtl: true,{/if}{literal}
		});
		$('.gradient .gradient-l').bind('click', function(){
			{/literal}{if {$_LANG.rtl === 'rtl'}{literal}
			var next = $('#next_item');
			if(next.css('display') != 'none') next.trigger('click');
			{/literal}{else}{literal}
			var previous = $('#previous_item');
			if(previous.css('display') != 'none')  previous.trigger('click');
			{/literal}{/if}{literal}
		});
		$('.gradient .gradient-r').bind('click', function(){
			{/literal}{if {$_LANG.rtl === 'rtl'}{literal}
			var previous = $('#previous_item');
			if(previous.css('display') != 'none')  previous.trigger('click');
			{/literal}{else}{literal}
			var next = $('#next_item');
			if(next.css('display') != 'none') next.trigger('click');
			{/literal}{/if}{literal}
			
		});
		$('.slider .backgrounds .item').bind('click', function(){
			{/literal}{if {$_LANG.rtl === 'rtl'}{literal}
			var previous = $('#previous_item');
			if(previous.css('display') != 'none')  previous.trigger('click');
			{/literal}{else}{literal}
			var next = $('#next_item');
			if(next.css('display') != 'none') next.trigger('click');
			{/literal}{/if}{literal}
		});
	});
{/literal}</script>
{/if}

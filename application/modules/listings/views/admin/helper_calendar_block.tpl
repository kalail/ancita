<div class="edit-form n150">
	<div class="row header">{l i='admin_header_calendar_period_management' gid='listings'}</div>
	<div id="calendar" class="cal-type-{$listing.price_period}">
		<div class="calendar-nav">
			<div class="fleft">
				{switch from=$listing.price_period}
					{case value='1'}
						<a href="#" id="prev_month">{l i='link_month_prev' gid='listings'}</a>
					{case value='2'}
						<a href="#" id="prev_year">{l i='link_year_prev' gid='listings'}</a> 
				{/switch}
			</div>
			<div class="fright">
				{switch from=$listing.price_period}
					{case value='1'}
						<a href="#" id="next_month">{l i='link_month_next' gid='listings'}</a> 
					{case value='2'}
						<a href="#" id="next_year">{l i='link_year_next' gid='listings'}</a>
				{/switch}
			</div>
			<div class="clr"></div>
		</div>
	
		<div id="calendar_block">
			{include file="calendar_block.tpl" module=listings theme=user}
		</div>
	
		<div class="statusbar">
			{ld i='booking_status' gid='listings' assign='booking_status'}
			{foreach item=item2 key=key2 from=$booking_status.option}
			{if $key2 eq 'open' || $key2 eq 'book'}
			<span class="{$key2}"></span> {$item2}
			{/if}
			{/foreach}
		</div>
	</div>
</div>

{js file='listings-calendar.js' module='listings'}
<script>{literal}
	var listings_calendar{/literal}{$rand}{literal};
	$(function(){
		listings_calendar{/literal}{$rand}{literal} = new listingsCalendar({
			siteUrl: '{/literal}{$site_root}{literal}',
			ajaxCalendarUrl: 'admin/listings/ajax_get_calendar',
			listingId: {/literal}{$listing.id}{literal},
			count: {/literal}{$count}{literal},
			month: {/literal}{$m}{literal},
			year: {/literal}{$y}{literal},
		});
	});
{/literal}</script>
<div class="clr"></div>
{js file='booking-form.js' module='listings'}
<table class="data" id="listing_periods">
	<tr>
		<th class="w250">{l i='field_booking_period' gid='listings'}</th>
		{*<th class="w30">{l i='field_booking_guests' gid='listings'}</th>*}
		<th class="w70">{l i='field_booking_price' gid='listings'}, {$current_price_currency.abbr} {ld_option i='price_period' gid='listings' option=$data.price_period}</th>
		<th class="w250">{l i='field_booking_comment' gid='listings'}</th>
		<th class="w70">&nbsp;</th>
	</tr>
	{assign var='is_opened' value=0}
	{foreach item=item from=$data.booking.periods}
	{if $item.status eq 'open'}
	{assign var='is_opened' value=1}
	{assign var='period' value=$item}
	{include file="calendar_period.tpl" module=listings}
	{/if}
	{/foreach}
	{if !$is_opened}<tr><td colspan="5" class="center">{l i='no_periods' gid='listings'}</td></tr>{/if}
</table>
<div class="btn"><div class="l"><input type="submit" name="btn_period" value="{l i='btn_add' gid='start' type='button'}" id="btn_period"></div></div>
<script>{literal}
	$(function(){
		new bookingForm({
			siteUrl: '{/literal}{$site_root}{literal}',
			listingId: '{/literal}{$data.id}{literal}',
			bookingBtn: 'btn_period',
			cFormId: 'period_form',
			isSavePeriod: false,
			urlGetForm: 'admin/listings/ajax_period_form/',
			urlSaveForm: 'admin/listings/ajax_save_period/',
			calendar: listings_calendar{/literal}{$rand}{literal},
			successCallback: function(id, data, calendar){
				var periods = $('#listing_periods tbody');
				var row = periods.find('tr:nth-child(2)').first();
				if(row.children().length == 1) row.remove();
				periods.find('tr:first-child').after(data);
				window['period_'+id].set_calendar(calendar);
			},
		});
	});
{/literal}</script>
<div class="clr"></div>
<table class="data" id="listing_booked">
	<tr>
		<th class="w250">{l i='adfield_booking_period' gid='listings'}</th>
		{*<th class="w30">{l i='field_booking_guests' gid='listings'}</th>*}
		<th class="w30">&nbsp;</th>
		<th class="w250">{l i='field_booking_comment' gid='listings'}</th>
		<th class="w70">&nbsp;</th>
	</tr>
	{assign var='is_booked' value=0}
	{foreach item=item from=$data.booking.periods}
	{if $item.status eq 'book'}
	{assign var='is_booked' value=1}
	{assign var='period' value=$item}
	{include file="calendar_period.tpl" module=listings themes=user}
	{/if}
	{/foreach}
	{if !$is_booked}<tr><td colspan="5" class="center">{l i='no_periods' gid='listings'}</td></tr>{/if}	
</table>
<div class="btn"><div class="l"><input type="submit" value="{l i='btn_add' gid='start' type='button'}" name="btn_order" id="btn_order"></div></div>
<script>{literal}
	$(function(){
		new bookingForm({
			siteUrl: '{/literal}{$site_root}{literal}',
			listingId: '{/literal}{$data.id}{literal}',
			bookingBtn: 'btn_order',
			cFormId: 'order_form',
			isSavePeriod: false,
			urlGetForm: 'admin/listings/ajax_order_form/',
			urlSaveForm: 'admin/listings/ajax_save_order/',
			calendar: listings_calendar{/literal}{$rand}{literal},
			successCallback: function(id, data, calendar){
				var periods = $('#listing_booked tbody');
				var row = periods.find('tr:nth-child(2)').first();
				if(row.children().length == 1) row.remove();
				periods.find('tr:first-child').after(data);
				window['period_'+id].set_calendar(calendar);
			},
		});
	});
{/literal}</script>
<div class="clr"></div>


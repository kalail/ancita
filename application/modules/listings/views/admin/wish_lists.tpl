{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_listings_menu'}
<div class="actions">
	<ul>
		<li><div class="l"><a href="{$site_url}admin/listings/edit_wish_list">{l i='link_add_wish_list' gid='listings'}</a></div></li>
	</ul>
	&nbsp;
</div>

<form id="wish_lists_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="w150">{l i='field_wish_list_name' gid='listings'}</th>
	{*<th class="w150">{l i='field_user_data' gid='listings'}</th>*}
	<th class="w150"><a href="{$sort_links.date_created}"{if $order eq 'date_created'} class="{$order_direction|lower}"{/if}>{l i='field_date_created' gid='listings'}</a></th>
	<th class="w70">&nbsp;</th>
</tr>
{foreach item=item from=$wish_lists}
{counter print=false assign=counter}
<tr{if $counter is div by 2} class="zebra"{/if}>				
	<td>{$item.output_name|truncate:50}</td>
	{*<td>{$item.user.output_name|truncate:50}</td>*}
	<td class="center">{$item.date_created|date_format:$page_data.date_format}</td>
	<td class="icons">
		{if $item.status}
		<a href="{$site_url}admin/listings/activate_wish_list/{$item.id}/0"><img src="{$site_root}{$img_folder}icon-full.png" width="16" height="16" border="0" alt="{l i='link_deactivate_wish_list' gid='listings' type='button'}" title="{l i='link_deactivate_wish_list' gid='listings' type='button'}"></a>
		{else}
		<a href="{$site_url}admin/listings/activate_wish_list/{$item.id}/1"><img src="{$site_root}{$img_folder}icon-empty.png" width="16" height="16" border="0" alt="{l i='link_activate_wish_list' gid='listings' type='button'}" title="{l i='link_activate_wish_list' gid='listings' type='button'}"></a>
		{/if}
		<a href="{$site_url}admin/listings/wish_list_content/{$item.id}"><img src="{$site_root}{$img_folder}icon-settings.png" width="16" height="16" border="0" alt="{l i='link_wish_list_content' gid='listings' type='button'}" title="{l i='link_wish_list_content' gid='listings' type='button'}"></a>
		<a href="{$site_url}admin/listings/edit_wish_list/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_wish_list' gid='listings' type='button'}" title="{l i='link_edit_wish_list' gid='listings' type='button'}"></a>
		<a href="{$site_url}admin/listings/delete_wish_list/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_wish_list' gid='listings' type='js'}')) return false;"><img src="{$site_root}{$img_folder}icon-delete.png" width="16" height="16" border="0" alt="{l i='link_delete_wish_list' gid='listings' type='button'}" title="{l i='link_delete_wish_list' gid='listings' type='button'}"></a>
	</td>
</tr>
{foreachelse}
<tr><td colspan="4" class="center">{l i='no_wish_lists' gid='listings'}</td></tr>
{/foreach}
</table>
</form>
{include file="pagination.tpl"}

{js file='easyTooltip.min.js'}
<script>{literal}
var reload_link = "{/literal}{$site_url}admin/listings/wish_lists{literal}";
var order = '{/literal}{$order}{literal}';
var loading_content;
var order_direction = '{/literal}{$order_direction}{literal}';
$(function(){
	$(".tooltip").each(function(){
		$(this).easyTooltip({
			useElement: 'span_'+$(this).attr('id')
		});
	});
});
function reload_this_page(){
	$('form[name="save_form"]').submit();
}
function reload_this_page(value){
	var link = reload_link + '/' + value + '/' + order + '/' + order_direction;
	location.href=link;
}
{/literal}</script>

{include file="footer.tpl"}

<div class="load_content_controller">
	<h1>{if $data.id}{l i='header_edit_photo' gid='listings'}{else}{l i='header_add_photo' gid='listings'}{/if}</h1>
	<div class="inside">
	<form action="" method="post" enctype="multipart/form-data">
	<div class="edit-form">
		<div class="row">
			<div class="h">{l i='field_photo' gid='listings'}:</div>
			<div class="v"><input type="file" name="photo_file" id="photo_file"><br><i>{$upload_config.requirements_str}</i>{if $data.media.thumbs.60_60}<br><img src="{$data.media.thumbs.60_60}" />{/if}</div>
		</div>
		<div class="row zebra">
			<div class="h">{l i='field_photo_comment' gid='listings'}: </div>
			<div class="v"><textarea name="comment" id="photo_comment">{$data.comment|escape}</textarea></div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="button" name="btn_save" value="{l i='btn_save' gid='start' type='button'}" onclick="javascript: gUpload.upload_photo({if $data.id}{$data.id}{else}0{/if});"></div></div>
	<a class="cancel" href="#" onclick="javascript: gUpload.close_open_form(); return false;">{l i='btn_cancel' gid='start'}</a>
	</form>
	</div>
</div>

{include file="header.tpl" load_type='ui'}

<form method="post" action="{$data.action|escape}" name="save_form" id="save_form" enctype="multipart/form-data">
<div class="edit-form n150">
	<div class="row header">{if $data.id}{l i='admin_header_wish_list_edit' gid='listings'}{else}{l i='admin_header_wish_list_add' gid='listings'}{/if}</div>
	<div class="row">
		<div class="h">{l i='field_wish_list_name' gid='listings'}:&nbsp;* </div>
		<div class="v" id="wish_list_name">
			{foreach item=lang_item key=lang_id from=$langs}
			{assign var='name' value='name_'+$lang_id}
			<input type="{if $lang_id eq $current_lang_id}text{else}hidden{/if}" name="data[name_{$lang_id}]" value="{$data[$name]|escape}" lang-editor="value" lang-editor-type="data-name" lang-editor-lid="{$lang_id}" />
			{/foreach}
			<a href="#" lang-editor="button" lang-editor-type="data-name"><img src="{$site_root}{$img_folder}icon-translate.png" width="16" height="16"></a>
		</div>
	</div>
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}"></div></div>
<a class="cancel" href="{$site_url}admin/listings/wish_lists">{l i='btn_cancel' gid='start'}</a>
</form>
<div class="clr"></div>
{block name=lang_inline_editor module=start}
{include file="footer.tpl"}

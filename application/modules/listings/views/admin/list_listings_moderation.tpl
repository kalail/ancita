{include file="header.tpl"}
{helper func_name=get_admin_level1_menu helper_name=menu func_param='admin_listings_menu'}
<div class="actions">&nbsp;</div>

<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first">{l i='field_user_data' gid='listings'}</th>
	<th class="w150"><a href="{$sort_links.date_created}"{if $order eq 'date_created'} class="{$order_direction|lower}"{/if}>{l i='field_date_created' gid='listings'}</a></th>
	<th class="w50">&nbsp;</th>
</tr>
{foreach item=item from=$listings}
{counter print=false assign=counter}
<tr{if $counter is div by 2} class="zebra"{/if}>
	<td><b>{$item.user.nickname}</b> {$item.user.fname} {$item.user.sname}</td>
	<td class="center">{$item.date_created|date_format:$page_data.date_format}</td>
	<td class="icons">
		<a href="{$site_url}admin/listings/moderation_edit/{$item.id}"><img src="{$site_root}{$img_folder}icon-edit.png" width="16" height="16" border="0" alt="{l i='link_edit_listing_moderation' gid='listings' type='button'}" title="{l i='link_edit_listing_moderation' gid='listings' type='button'}"></a>
	</td>
</tr>
{foreachelse}
<tr><td colspan="4" class="center">{l i='no_listings_moderation' gid='listings'}</td></tr>
{/foreach}
</table>
{include file="pagination.tpl"}

{include file="footer.tpl"}

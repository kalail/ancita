	{if $listings}
	<div class="sorter line" id="sorter_block">
		{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
		<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>
	</div>
	<div>
		<table class="list">
		<tr id="sorter_block">
			<th class="w100">{l i='field_photo' gid='listings'}</th>		
			<th>{l i='field_name' gid='listings'}</th>		
			<th>{l i='field_date_modified' gid='listings'}</th>		
			<th class="w30">{l i='field_views' gid='listings'}</th>
			<th class="w120">{l i='field_search_status' gid='listings'}</th>		
			<th class="w70">&nbsp;</th>		
		</tr>
		{foreach item=item from=$listings}
		{capture assign='property_output'}{$item.property_type_str} {$item.operation_type_str}{/capture}
		{capture assign='price_output'}	
			{block name=listing_price_block module='listings' data=$item template='small'}
		{/capture}
		{l i='link_listing_view' gid='listings' type='button' assign='logo_title' id_ref=$item.id}
		{l i='text_listing_logo' gid='listings' type='button' assign='text_listing_logo' id_ref=$item.id property_type=$item.property_type_str operation_type=$item.operation_type_str location=$item.location}
		<tr>
			<td>
				<a href="{seolink module='listings' method='view' data=$item}" title="{l i='btn_preview' gid='start' type='button'}">
					<img src="{$item.media.photo.thumbs.small}" alt="{$text_listing_logo}" title="{$logo_title}">
				</a>
			</td>
			<td>{$item.output_name|truncate:40}<br>{$property_output|truncate:40}<br>{$price_output|truncate:70}</td>
			<td>{$item.date_modified|date_format:$page_data.date_format}</td>
			<td>{$item.views}</td>
			<td>
			{if $item.status}
		<a href="{$site_url}listings/activate/{$item.id}/0"><img id="frontendimg" src="{$site_root}application/views/admin/img/icon-full.png" width="16" height="16" border="0" alt="{l i='link_deactivate_listing' gid='listings' type='button'}" title="{l i='link_deactivate_listing' gid='listings' type='button'}"></a>
		{else}
		<a href="{$site_url}listings/activate/{$item.id}/1"><img id="frontendimg" src="{$site_root}application/views/admin/img/icon-empty.png" width="16" height="16" border="0" alt="{l i='link_activate_listing' gid='listings' type='button'}" title="{l i='link_activate_listing' gid='listings' type='button'}"></a>
		    {/if}
				{if $item.moderation_status eq 'default' && $item.initial_moderation}<span class="status"><ins class="fa fa-check-circle fa-2x"></ins>{l i='listing_status_default' gid='listings'}</span>
				{elseif $item.moderation_status eq 'decline'}<span class="status decline"><ins class="fa fa-minus-circle e fa-2x"></ins>{l i='listing_status_decline' gid='listings'}</span>
				{elseif $item.moderation_status eq 'approved' || $item.moderation_status eq 'default' && !$item.initial_moderation}<span class="status approved"><ins class="fa fa-check-circle fa-2x"></ins>{l i='listing_status_approved' gid='listings'}</span>
				{elseif $item.moderation_status eq 'wait'}<span class="status wait"><ins class="fa fa-clock-o g fa-2x"></ins>{l i='listing_status_wait' gid='listings'}</span>
				{/if}

			</td>

			   <td>
				<a href="{seolink module='listings' method='view' data=$item}" class="btn-link fright" title="{l i='btn_preview' gid='start' type='button'}"><ins class="fa fa-eye fa-lg edge hover"></ins></a>
				<a href="{$site_url}listings/edit/{$item.id}" class="btn-link fright" title="{l i='btn_edit' gid='start' type='button'}"><ins class="fa fa-pencil fa-lg edge hover"></ins></a>
				{if $item.operation_type eq 'rent'}<a href="{$site_url}listings/edit/{$item.id}/calendar/2" class="btn-link fright" title="{l i='link_calendar_view' gid='listings' type='button'}"><ins class="fa fa-calendar-o fa-lg edge hover"><ins class="fa fa-calendar-number"></ins></ins></a>{/if}
				{depends module=services}{if $item.operation_type ne 'buy' && $item.operation_type ne 'lease'  && $item.status}<a href="{$site_url}listings/services/{$item.id}" class="btn-link fright" title="{l i='link_services' gid='listings' type='button'}"><ins class="fa fa-dollar fa-lg edge hover"></ins></a>{/if}{/depends}
				<a href="{$site_url}listings/delete/{$item.id}" onclick="javascript: if(!confirm('{l i='note_delete_listing' gid='listings' type='js'}')) return false;" class="btn-link fright" title="{l i='btn_delete' gid='start' type='button'}"><ins class="fa fa-trash-o fa-lg edge hover"></ins></a>
			</td>
		</tr>
		{/foreach}
		</table>	

	</div>
	<div id="pages_block_2">{include file="pagination.tpl"}</div>
	{else}
	<div class="item empty">{l i='no_listings' gid='listings'}</div>
	{/if}
	
	<script>{literal}
	$(function(){
		$('#total_rows').html('{/literal}{$page_data.total_rows}{literal}');
	});
	{/literal}</script>

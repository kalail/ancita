{switch from=$template}
	{case value='icon'}
		{*<a href="{$site_url}listings/save/{$listing.id}" id="btn-save-{$listing.id}{$rand}" class="btn-link link-r-margin" title="{if $listing.is_saved}{l i='text_listing_saved' gid='listings' type='button'}{else}{l i='link_add_to_comparison' gid='listings' type='button'}{/if}" rel="nofollow"><ins class="fa fa-star {if $listing.is_saved}g{else}hover{/if} fa-lg edge"></ins></a>*}
	{case value='link'}
		{*<a href="{$site_url}listings/save/{$listing.id}" id="link-save-{$listing.id}{$rand}" {if $listing.is_saved}class="hide"{/if} rel="nofollow">{l i='link_add_to_comparison' gid='listings'}</a><span id="text-save-{$listing.id}{$rand}" {if !$listing.is_saved}class="hide"{/if}>{l i='text_listing_saved' gid='listings'}</span>*}
	{case}
		<a href="{$site_url}listings/save/{$listing.id}" id="link-save-{$listing.id}{$rand}" {if $listing.is_saved}class="hide"{/if} rel="nofollow">{l i='link_add_to_comparison' gid='listings'}</a><span id="text-save-{$listing.id}{$rand}" {if !$listing.is_saved}class="hide"{/if}>{l i='text_listing_saved' gid='listings'}</span>
{/switch}
{js file='listings-saved.js' module='listings'}
<script>{literal}
	$(function(){
		new listingsSaved({
			siteUrl: '{/literal}{$site_root}{literal}',
			saveBtn: '{/literal}btn-save-{$listing.id}{$rand}{literal}',
			saveLink: '{/literal}link-save-{$listing.id}{$rand}{literal}',
			saveText: '{/literal}text-save-{$listing.id}{$rand}{literal}',
			listingId: {/literal}{$listing.id}{literal},
			isSaved: {/literal}{if $listing.is_saved}1{else}0{/if}{literal},
			savedTitle: '{/literal}{l i='text_listing_saved' gid='listings' type='button'}{literal}',
			{/literal}{if $is_guest}displayLogin: true,{/if}{literal}
		});
	});
{/literal}</script>

{include file="header.tpl"}
{js module=listings file='jquery-1.9.1.js'}
{js module=listings file='jquery-ui.js'}
<div class="content-block load_content">
	<h1>{l i='header_order_add' gid='listings'}</h1>
	<div class="inside edit_block">
    <div id="con-result"></div>
		<form method="post" action="" name="save_form" id="order_form" enctype="multipart/form-data">
          <div class="r">
				<div class="f">Name:*</div>
				<div class="v" id="namelabel">
					<input type="text" name="name" id="name" class="name" onfocus="this.removeAttribute('readonly');" readonly/>
				</div>
			</div>
             <div class="r">
				<div class="f">Phone:</div>
				<div class="v" id="phonelabel">
					<input type="text" name="phone" id="phone" class="phone" onfocus="this.removeAttribute('readonly');" readonly/>
				</div>
			</div>
          <div class="r">
				<div class="f">Email:*</div>
				<div class="v" id="period_mail">
					<input type="text" name="con-mail" id="con-mail" class="con-mail" onfocus="this.removeAttribute('readonly');" readonly/>
				</div>
			</div>
            <div class="r">
				<div class="f">From date:*</div>
				<div class="v periodbox">
	           <input type="text" name="datestart" id="datestart" class="middle"/><i class="fa fa-calendar" aria-hidden="true"></i>
					</div>
			        </div>
						<div class="r">
				<div class="f">To date:*</div>
				<div class="v periodbox">
				<input type="text" id="dateend" name="dateend" class="middle"/><i class="fa fa-calendar" aria-hidden="true"></i>
					</div>
    		        </div>
			<div class="r">
				<div class="f">Comment:</div>
				<div class="v" id="period_comment">
					<textarea name="comments" id="comments" rows="10" cols="80"></textarea>
				</div>
			</div>
            <div class="r">
			<div class="f">{l i='field_contact_captcha' gid='contact'}&nbsp;*</div>
			<div class="v captcha">
				{$data.captcha_image}
				<input type="text" size="12" name="code" id="code" value="" maxlength="{$data.captcha_word_length}" onfocus="this.removeAttribute('readonly');" readonly>	
                <input type="hidden" name="captcha_word" id="captcha_word" value="{$captcha_word}">			
			</div>
		   </div>		
			<div class="b">
				<input type="submit" id="con-submit" name="btn_save" value="Save" id="close_btn">
                <input type="hidden" name="data" id="data" value=""/>
			</div>
		</form>
	</div>
</div>
<script>{literal}
	$('#con-submit').click(function(e){
		e.preventDefault();
		var data = $.trim($("#data").val());
	    var name = $.trim($("#name").val());
		var phone = $.trim($("#phone").val());
		var email = $.trim($("#con-mail").val());
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var capRegex = /[0-9 -()+]+$/;
		var smonth = $.trim($("#datestart").val());
		var eyear = $.trim($("#dateend").val());
		var comments = $.trim($("#comments").val());
		var code = $.trim($("#code").val());
	    var captcha_word = $.trim($("#captcha_word").val());
		if(name == '') {
			$("#con-result").html('<p class="error-message">Name required.</p>');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#name").focus();																								 
			});
			return false;
		}
		if(phone == '') {
			$("#con-result").html('<p class="error-message">Phone required.</p>');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#phone").focus();																								 
			});
			return false;
		}
		if(email == '') {
			$("#con-result").html('<p class="error-message">Email required.</p>');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#con-mail").focus();																								 
			});
			return false;
		}else if(email != '' && !emailReg.test(email)) {
			$("#con-result").html('<p class="error-message">Invalid email.</p>');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#con-mail").focus();
			});
			return false;
		}
		if(smonth == '') {
			$("#con-result").html('<p class="error-message">From Date required.</p>');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#datestart").focus();																								 
			});
			return false;
		}
		if(eyear == '') {
			$("#con-result").html('<p class="error-message">To Date required.</p>');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#dateend").focus();																								 
			});
			return false;
		}
		if(code == '') {
			$("#con-result").html('<p class="error-message">Code required.</p>');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#code").focus();
				window.stop();																								 
			});
			return false;
		}else if(code != '' && !capRegex.test(code)) {
			$("#con-result").html('<p class="error-message">Invalid code.</p>');
			$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#code").focus();
				window.stop();
			});
			return false;
		}
		var url = '{/literal}{$site_root}{literal}' + 'listings/guest_req';
		$.ajax({
        type: 'POST',
        url: url,
        data: { data:data, name:name, phone:phone, email: email, smonth:smonth,eyear:eyear,comments:comments,code:code,captcha_word:captcha_word},
        success: function(response) {
            if(response == 1){
				$("#con-result").html('<p class="success-message">Request sent successfully.</p>');
				$('html, body').animate({scrollTop: $("#con-result").offset().top-200},6000,function(){
					$('.success-message').delay(6000).fadeOut();
				});
			}else{
				$("#con-result").html('<p class="error-message">Invalid code.</p>');
				$('html, body').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
					$('.error-message').delay(1000).stop();
				});
			}
        }
    });
	});
	{/literal}</script>
	<script>{literal}
	$(function(){
	 $( "#datestart" ).datepicker({
	  dateFormat: 'dd MM yy',
      changeMonth: true,//this option for allowing user to select month
      changeYear: true //this option for allowing user to select from year range
    });
	 $( "#dateend" ).datepicker({
	  dateFormat: 'dd MM yy',
      changeMonth: true,//this option for allowing user to select month
      changeYear: true //this option for allowing user to select from year range
    });
    var parent = $(window.opener.document).contents();
	var id = parent.find("#guestlink").attr('data');
	$('#data').val(id);
	});
{/literal}</script>

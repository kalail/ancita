{include file="header.tpl" load_type='ui'}
<div class="content-block">
	<h1>{l i='header_share' gid='listings'}</h1>
	<div class="edit_block">
		{include file="share_form.tpl" module=listings}
	</div>
</div>
<div class="clr"></div>
{js module=listings file='listings-menu.js'}
<script>{literal}
	var pMenu;
	$(function(){
		pMenu = new listingsMenu({
			siteUrl: '{/literal}{$site_root}{literal}',
			idListing: '{/literal}{$data.id}{literal}',
			listBlockId: 'share_block',
			sectionId: 'share_sections',
			currentSection: 'm_send_email',
		});
	});
{/literal}</script>
{include file="footer.tpl"}

<div id="listing_select_{$select_data.rand}" class="listing-select">
	{if $select_data.max_select eq 1}{l i='text_filter' gid='listings'}:{/if}
	<a href="#" id="listing_link_{$select_data.rand}">{l i='link_manage_listings' gid='listings'}</a>{if $select_data.max_select > 1} <i>({l i='max_listing_select' gid='listings'}: {$select_data.max_select})</i>{/if}<br>
	{if $select_data.max_select > 1}
	<span id="listing_text_{$select_data.rand}">
	{foreach item=item from=$select_data.selected}
	{$item.output_name|truncate:100} {if $select_data.max_select ne 1}<br>{/if}<input type="hidden" name="{$select_data.var_name}{if $select_data.max_select ne 1}[]{/if}" value="{$item.id}">
	{/foreach}
	</span>
	{else}
	<div id="listing_text_{$select_data.rand}">
	{foreach item=item from=$select_data.selected}
	<div class="item">
		{l i='link_listing_view' gid='listings' type='button' assign='logo_title' id_ref=$item.id}
		{l i='text_listing_logo' gid='listings' type='button' assign='text_listing_logo' id_ref=$item.id property_type=$item.property_type_str operation_type=$item.operation_type_str location=$item.location}
		<a href="{seolink module='listings' method='view' data=$item}" title="{l i='btn_preview' gid='start' type='button'}"><img src="{$item.media.photo.thumbs.small}" alt="{$text_listing_logo}" title="{$logo_title}"></a>
		<span>{$item.output_name|truncate:100}</span>
		<span>{$item.property_type_str} {$item.operation_type_str}</span>
		<span>{block name=listing_price_block module='listings' data=$item template='small'}</span>
		<div class="clr"></div>
		<input type="hidden" name="{$select_data.var_name}{if $select_data.max_select ne 1}[]{/if}" value="{$item.id}">
	</div>
	{/foreach}
	</div>
	{/if}
	<div class="clr"></div>
</div>

{capture assign='append_callback'}{literal}
	function(data){
	{/literal}{if $select_data.max_select > 1}{literal}
	return data.output_name +
	' <br><input type="hidden" name="{/literal}{$select_data.var_name}{literal}[]" value="'+data.id+'">';	
	{/literal}{else}{literal}
	return '' +
	'<div class="item">' +
	'	<a href="'+data.seo_url+'" title="{/literal}{l i='btn_preview' gid='start' type='js'}{literal}"><img src="'+data.media.photo.thumbs.small+'" alt="'+data.output_name+'" title="'+data.output_name+'"></a>' +
	'	<span>'+data.output_name+'</span>' +
	'	<span>'+data.property_type_str+' '+data.operation_type_str+'</span>' +
	'	<span>'+data.price_str+'</span>' +
	'	<div class="clr"></div>' +
	'	<input type="hidden" name="{/literal}{$select_data.var_name}{literal}" value="'+data.id+'">' +
	'</div>';
	{/literal}{/if}{literal}
	}
{/literal}{/capture}
{js module=listings file='listings-select.js'}
<script type='text/javascript'>{literal}
$(function(){
	new listingsSelect({
		siteUrl: '{/literal}{$site_root}{literal}',	
		selected_items: [{/literal}{$select_data.selected_str}{literal}],
		max: '{/literal}{$select_data.max_select}{literal}',
		var_name: '{/literal}{$select_data.var_name}{literal}',
		template: '{/literal}{$select_data.template}{literal}',
		params: {{/literal}{foreach item=item key=key from=$select_data.params}{$key}:'{$item}',{/foreach}{literal}},
		{/literal}{if $select_data.callback}change_callback: {$select_data.callback},{/if}{literal}
		append_callback: {/literal}{$append_callback}{literal},
		rand: '{/literal}{$select_data.rand}{literal}',	
	});
});
{/literal}</script>

{include file="header.tpl"}

<div class="rc">
	<div class="content-block">
		<h1>{l i='header_my_saved_listings' gid='listings'} ({$saved_count|intval})</h1>
		
		<div class="tabs tab-size-15 noPrint">
			<ul id="user_listing_sections">
				{foreach item=tgid from=$operation_types}
				<li id="m_{$tgid}" sgid="{$tgid}" class="{if $current_operation_type eq $tgid}active{/if}"><a href="{$site_url}listings/saved/{$tgid}">{l i='operation_search_'+$tgid gid='listings'} ({$items_count[$tgid]|intval})</a></li>
				{/foreach}
			</ul>
		</div>
		
		<div id="listings_block">{$block}</div>
		{js module=listings file='listings-list.js'}
		<script>{literal}
		$(function(){
			lList = new listingsList({
				siteUrl: '{/literal}{$site_root}{literal}',
				listAjaxUrl: '{/literal}listings/ajax_saved{literal}',
				sectionId: 'user_listing_sections',
				operationType: '{/literal}{$current_operation_type}{literal}',
				order: '{/literal}{$order}{literal}',
				orderDirection: '{/literal}{$order_direction}{literal}',
				page: {/literal}{$page}{literal},
				tIds: ['pages_block_1', 'pages_block_2', 'sorter_block'],
			});
		});
		{/literal}</script>
	</div>
</div>

{include file="left_panel.tpl" module=start}

<div class="clr"></div>
{include file="footer.tpl"}


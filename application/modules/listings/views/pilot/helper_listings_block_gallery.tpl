{if $listings}
<div id="ShowCustomlisting_{$type}" style="display:{if $type eq 'rent'}none{else}block{/if};">
<h2>{l i='header_'+$type+'_listings' gid='listings'}</h2>
<!--<h2>Listings for <span id="lstTilteType_{$type}">{$type}</span></h2>-->
<div class="{$type|escape}_listings_block">
	{foreach item=item key=key from=$listings}
	<div class="listing {$photo_size|escape}">
		<a href="{seolink module='listings' method='view' data=$item}">
			{l i='link_listing_view' gid='listings' type='button' assign='logo_title'  id_ref=$item.id}
			{l i='text_listing_logo' gid='listings' type='button' assign='text_listing_logo' id_ref=$item.id property_type=$item.property_type_str operation_type=$item.operation_type_str location=$item.location}
			<img src="{$item.media.photo.thumbs[$photo_size]}" alt="{$text_listing_logo}" title="{$logo_title}" />
			<!--{if $item.photo_count || $item.is_vtour}
			<div class="photo-info">
				<div class="panel">
					{if $item.photo_count}<span><ins class="fa fa-camera w"></ins> {$item.photo_count}</span>{/if}
					{if $item.is_vtour}<span><ins class="fa fa-rotate-left w"></ins> 360&deg;</span>{/if}
				</div>
				<div class="background"></div>
			</div>
			{/if}-->
		</a>
		<!--<a href="{seolink module='listings' method='view' data=$item}">{if $photo_size eq 'big'}{$item.output_name|truncate:35}{else}{$item.output_name|truncate:30}{/if}</a>-->
        <a href="{seolink module='listings' method='view' data=$item}">{$item.city}</a><br />
        <span>{block name=listing_price_block module='listings' data=$item template='small'}</span>	
		<span>{$item.property_type_str}</span>
	</div>
	{/foreach}
	<div class="clr"></div>
</div>
</div>
{/if}

	{if $listings}
	<div class="sorter line" id="sorter_block">
		{sorter links=$sort_data.links order=$sort_data.order direction=$sort_data.direction url=$sort_data.url}
		<div class="fright" id="pages_block_1">{pagination data=$page_data type='cute'}</div>
	</div>
	{/if}

	<div>
	{foreach item=item from=$listings}
	<div class="listing-block {if $item.is_highlight}highlight{/if}">
		<div id="item-block-{$item.id}" class="item listing {if $item.is_highlight}highlight{/if}">
			<!--<h3><a href="{seolink module='listings' method='view' data=$item}" title="{$item.output_name|escape}">{$item.output_name|truncate:50}</a></h3>-->
            <h3 style="font-size:18px;"><a href="{seolink module='listings' method='view' data=$item}" title="{$item.city|escape}">{$item.city|truncate:50}</a></h3>
			<div class="image">
				{l i='link_listing_view' gid='listings' type='button' assign='logo_title' id_ref=$item.id}
				{l i='text_listing_logo' gid='listings' type='button' assign='text_listing_logo' id_ref=$item.id property_type=$item.property_type_str operation_type=$item.operation_type_str location=$item.location}					
				<a href="{seolink module='listings' method='view' data=$item}">
					{if $animate_available && $item.is_slide_show}
					<img src="{$item.media.photo.thumbs.middle_anim}" alt="{$text_listing_logo}" title="{$logo_title}">
					{else}
					<img src="{$item.media.photo.thumbs.middle}" alt="{$text_listing_logo}" title="{$logo_title}">
					{/if}
					<!--{if $item.photo_count || $item.is_vtour}
					<div class="photo-info">
						<div class="panel">
							{if $item.photo_count}<span><ins class="fa fa-camera w"></ins> {$item.photo_count}</span>{/if}
							{if $item.is_vtour}<span><ins class="fa fa-rotate-left w"></ins> 360&deg;</span>{/if}
						</div>
						<div class="background"></div>
					</div>
					{/if}-->
				</a>
			</div>
	
			{capture assign='status_info'}{strip}
				{if $item.is_lift_up || 
					$location_filter eq 'country' && $item.is_lift_up_country || 
					$location_filter eq 'region' && ($item.is_lift_up_country || $item.is_lift_up_region) || 
					$location_filter eq 'city' && ($item.is_lift_up_country || $item.is_lift_up_region || $item.is_lift_up_city) ||
					$sort_data.order ne 'default' && ($item.is_lift_up ||$item.is_lift_up_country || $item.is_lift_up_region || $item.is_lift_up_city)}
					{l i='status_lift_up' gid='listings'}
				{elseif $item.is_featured}
					{l i='status_featured' gid='listings'}
				{/if}
			{/strip}{/capture}
			
			<div class="body">
            	<span style="color:#666;">{l i='listings_id_text' gid='listings'}: {$item.id}</span>
				<br><br><h3 style="font-weight:bold;">{block name=listing_price_block module='listings' data=$item}</h3>
                
				<div class="t-1">
					{$item.property_type_str} <!--{$item.operation_type_str}-->
					<!--<br>{$item.square_output|truncate: 30}-->
					{if $item.date_open|strtotime >= $tstamp}<br>{l i='text_date_open_status' gid='listings'}{/if}
					{if $item.sold}<br><span class="status_text">{l i='text_sold' gid='listings'}</span>{/if}
					{if $status_info}<br><span class="status_text">{$status_info}</span>{/if}
				</div>
				<div class="t-2">
				</div>
				{if $show_user_info}
				<div class="t-3">
					<!--{$item.user.output_name|truncate:20}-->
				</div>
                <div class="t-bedbath">
                	{capture assign='lst_beds'}
                    {if $item.fe_bd_rooms_1 >= '10'}{assign var="bedroom" value="10+"}{$bedroom}{/if}
                	{if $item.fe_bd_rooms_1 < '10'}{$item.fe_bd_rooms_1}{/if}
                    {if $item.fe_bd_rooms_4}{$item.fe_bd_rooms_4}{/if}{/capture}
                    {capture assign='lst_baths'}
                    {if $item.fe_bth_rooms_1 >= '10'}{assign var="bathroom" value="10+"}{$bathroom}{/if}
                    {if $item.fe_bth_rooms_1 < '10'}{$item.fe_bth_rooms_1}{/if}
                    {if $item.fe_bth_rooms_4}{$item.fe_bth_rooms_4}{/if}
                    {/capture}
                    
                    {if $lst_beds|trim ne ''}{$lst_beds}<img src='{$site_root}{$img_folder}bed.png'><br>{/if}
                    {if $lst_baths|trim ne ''}{$lst_baths}<img src='{$site_root}{$img_folder}bath.png'>{/if}
                </div>
				<div class="t-4">
					{capture assign='user_logo'}
						{if $show_logo || $item.user.is_show_logo}
						<img src="{$item.user.media.user_logo.thumbs.small}" title="{$item.user.output_name|escape}">
						{else}
						<img src="{$item.user.media.default_logo.thumbs.small}" title="{$item.user.output_name|escape}">
						{/if}
					{/capture}
					{if $item.user.status}
					<a href="{seolink module='users' method='view' data=$item.user}">{$user_logo}</a>
					{else}
					{$user_logo}
					{/if}
				</div>
				{/if}
			</div>
			<div class="clr"></div>
			<!--{if $item.headline}<p class="headline" title="{$item.headline}">{$item.headline|truncate:100}</p>{/if}-->
            {capture assign='custm_listheading'}
            {if $item.headline_lang[$current_lang_id] eq '...' || $item.headline_lang[$current_lang_id] eq ''}
				{$item.headline_lang[1]}{else}{$item.headline_lang[$current_lang_id]}
			{/if}
            {/capture}
            {if $custm_listheading}{if $custm_listheading ne '...'}<p class="headline">{$custm_listheading|truncate:150}</p>{/if}{/if}
			{block name='save_listing_block' module='listings' listing=$item template='link' separator='|'}
			{block name='listings_booking_block' module='listings' listing=$item template='link' show_link=1 show_price=1 separator='|' no_save=1}
			<a href="{seolink module='listings' method='view' data=$item}">{l i='link_details' gid='listings'}</a>
		</div>
	</div>
	{foreachelse}
	<div class="item empty">{l i='no_listings' gid='listings'}</div>
	{/foreach}
	</div>
	{if $listings}<div id="pages_block_2">{pagination data=$page_data type='full'}</div>{/if}

	{if $update_map}
	{block name=update_default_map module=geomap markers=$markers map_id='listings_map_container'}
	{/if}
	
	{if $update_operation_type}
	<script>{literal}
	$(function(){
		var operation_type = $('#operation_type');
		if(operation_type.val() != '{/literal}{$update_operation_type}{literal}')
			$('#operation_type').val('{/literal}{$update_operation_type}{literal}').trigger('change');
	});
	{/literal}</script>
	{/if}

	<script>{literal}
	$(function(){
		{/literal}{if $update_search_block}$('#search_filter_block').html('{strip}{$search_filters_block}{/strip}');{/if}{literal}
		$('#total_rows').html('{/literal}{$page_data.total_rows}{literal}');
	});
	{/literal}</script>


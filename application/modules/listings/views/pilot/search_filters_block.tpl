{if $current_search_data|count}
<ul>
     {if $id_category ne ''}
     <li>Category: <a>{if $id_category eq '1'}Residential{/if}{if $id_category eq '2'}Commercial{/if}{if $id_category eq '3'}Lots-lands{/if}</a></li>
     {/if}
	{foreach item=item key=key from=$current_search_data}
    {if $key ne 'id_category'}
	<li title="{$item.name|escape}: {$item.label|escape}" {if $key eq 'price'}dir="ltr"{/if}>{$item.name|truncate:10:'...':true}: {$item.label|truncate:12:'...':true} <a href="{$site_url}listings/delete_search_criteria/{$key|escape}" class="fright"></a></li>
    {/if}
	{/foreach}
</ul>
{/if}

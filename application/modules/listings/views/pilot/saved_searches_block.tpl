<ul>
	{foreach item=item from=$save_search_data}
	<li>
		<a href="{$site_url}listings/load_saved_search/{$item.id}" title="{$item.name|strip_tags|escape}">{$item.name|truncate:16:'...':true}</a>&nbsp;
		<a href="{$site_url}listings/delete_saved_search/{$item.id}" class="fright"><ins class="fa fa-times"></ins></a>
	</li>
	{/foreach}	
</ul>
<div class="clr"></div>

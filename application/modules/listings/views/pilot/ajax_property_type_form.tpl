<div class="load_content_controller">
	<h1>{l i='header_property_type_select' gid='listings'}</h1>
	<div class="inside">
		<ul class="controller-items" id="property_type_select_items"></ul>
	
		<div class="controller-actions">
			<input type="button" id="property_type_close_link" name="close_btn" value="{l i='btn_close' gid='start' type='button'}" class="fleft">
			{if $data.max > 1}
			<a href="#" id="property_type_select_back" class="btn-link link-margin"><ins class="fa fa-trash-o fa-lg edge"></ins>{l i='link_reset_all' gid='listings'}</a>
			{/if}
		</div>
		{if $data.max > 1}
		<div class="line top">{l i='text_availbale_select_property_types' gid='listings'} <span id="property_type_max_left_block"></span></div>
		{/if}
	</div>
</div>

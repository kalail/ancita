{if $listings}
{js file='jcarousellite.min.js'}
{js file='init_carousel_controls.js'}
<script>{literal}
$(function(){
	var rtl = 'rtl' === '{/literal}{$_LANG.rtl}{literal}';
	var idPrev, idNext;
	if(!rtl) {
		idNext = '#directionright{/literal}{$listings_page_data.rand}{literal}';
		idPrev = '#directionleft{/literal}{$listings_page_data.rand}{literal}';
	} else {
		idNext = '#directionleft{/literal}{$listings_page_data.rand}{literal}';
		idPrev = '#directionright{/literal}{$listings_page_data.rand}{literal}';
	};
	$('.carousel_block{/literal}{$listings_page_data.rand}{literal}').jCarouselLite({
		rtl: rtl,
		visible: {/literal}{$listings_page_data.visible}{literal},
		btnNext: idNext,
		btnPrev: idPrev,
		circular:false,
		afterEnd: function(a) {
			var index = $(a[0]).index();
			carousel_controls{/literal}{$listings_page_data.rand}{literal}.update_controls(index);
		}
	});

	carousel_controls{/literal}{$listings_page_data.rand}{literal} = new init_carousel_controls({
		rtl: rtl,
		carousel_images_count: {/literal}{$listings_page_data.visible}{literal},
		carousel_total_images:{/literal}{$listings_page_data.count}{literal},
		btnNext: idNext,
		btnPrev: idPrev
	});
});
</script>{/literal}
<h2>{l i='header_'+$type+'_listings' gid='listings'}</h2>
<div class="{$type|escape}_listings_block carousel {$photo_size|escape}">
	<div id="directionleft{$listings_page_data.rand}" class="directionleft">
		<div class="fa fa-arrow-left w fa-lg edge hover" id="l_hover"></div>
	</div>
	<div class="carousel_block carousel_block{$listings_page_data.rand} item_{$listings_page_data.visible}_info">
	<ul>
		{foreach item=item key=key from=$listings}
		<li>
			<div class="listing {$photo_size}">
				{l i='link_listing_view' gid='listings' type='button' assign='logo_title' id_ref=$item.id}
				{l i='text_listing_logo' gid='listings' type='button' assign='text_listing_logo' id_ref=$item.id property_type=$item.property_type_str operation_type=$item.operation_type_str location=$item.location}
				<a href="{seolink module='listings' method='view' data=$item}">
					<img src="{$item.media.photo.thumbs[$photo_size]}" alt="{$text_listing_logo}" title="{$logo_title}">
					{if $item.photo_count || $item.is_vtour}
					<div class="photo-info">
						<div class="panel">
							{if $item.photo_count}<span><ins class="fa fa-camera w"></ins> {$item.photo_count}</span>{/if}
							{if $item.is_vtour}<span><ins class="fa fa-rotate-left w"></ins> 360&deg;</span>{/if}
						</div>
						<div class="background"></div>
					</div>
					{/if}
				</a>
				<a href="{seolink module='listings' method='view' data=$item}">{if $photo_size eq 'big'}{$item.output_name|truncate:35}{else}{$item.output_name|truncate:30}{/if}</a>
				<span>{$item.property_type_str} {$item.operation_type_str}</span>
				<span>{block name=listing_price_block module='listings' data=$item template='small'}</span>
			</div>
		</li>
		{/foreach}
	</ul>
	</div>
	<div id="directionright{$listings_page_data.rand}" class="directionright">
		<div class="fa fa-arrow-right w fa-lg edge hover" id="r_hover"></div>
	</div>
	<div class="clr"></div>
</div>
{/if}

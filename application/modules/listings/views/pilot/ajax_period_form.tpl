<div class="content-block load_content">
	<h1>{l i='header_calendar_period_add' gid='listings'}</h1>	
	<div class="inside edit_block">
		<form method="post" action="{$data.action|escape}" name="save_form" id="period_form" enctype="multipart/form-data">
			{if $data.price_period}
				{assign var='price_period' value=$data.price_period}
			{else}
				{assign var='price_period' value=1}
			{/if}
			<div class="r fleft periodbox">
				<div class="f">{l i='field_booking_date_start' gid='listings'}:</div>
				<div class="v">
					{switch from=$price_period}
						{case value='1'}
							<input type="text" name="period[date_start]" value="{$current_date_start}" id="date_start{$rand}" class="middle">
							<input type="hidden" name="date_start_alt" value="{if $period.date_start|strtotime>0}{$period.date_start|date_format:'%Y-%m-%d'|escape}{/if}" id="alt_date_start{$rand}">
							<script>{literal}
							   $(function(){
							   $('#date_start{/literal}{$rand}{literal}').datepicker({
							   dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', 
							   altFormat: 'yy-mm-dd',
							   altField: '#alt_date_start{/literal}{$rand}{literal}',
                               minDate:  0,
                               onSelect: function(date){            
                               var date1 = $('#date_start{/literal}{$rand}{literal}').datepicker('getDate');           
                               var date = new Date( Date.parse( date1 ) ); 
                               date.setDate( date.getDate() + 0 );        
                               var newDate = date.toDateString(); 
                               newDate = new Date( Date.parse( newDate ) );                      
                               $('#date_end{/literal}{$rand}{literal}').datepicker("option","minDate",newDate);            
                               }
                               });
							   });
							{/literal}</script>
						{case value='2'}
							{ld i='month-names' gid='start' assign='month_names'}
							<select name="date_start_month" class="middle">
								<option value="">{$month_names.header}</value>
								{foreach item=item key=key from=$month_names.option}
								<option value="{$key}" {if $key eq $period.date_start|date_format:'%m'}selected{/if}>{$item}</option>
								{/foreach}
							</select>
							{assign var='cyear' value=('now'|date:'Y')}
							<select name="date_start_year" class="short">
								<option value="">{l i='text_year' gid='listings'}</option>
								{for start=0 stop=10 value=i}
								{math equation='x + y' x=$cyear y=$i assign='year'}
								<option value="{$year}" {if $year eq $period.date_start|date_format:'%Y'}selected{/if}>{$year}</option>
								{/for}
							</select>
					{/switch}
				</div>
			</div>
			<div class="r fleft">&nbsp;</div>
			<div class="r fleft periodbox">
				<div class="f">{l i='field_booking_date_end' gid='listings'}:</div>
				<div class="v">
					{switch from=$price_period}
						{case value='1'}
							<input type="text" name="period[date_end]" value="{$current_date_end}" id="date_end{$rand}" class="middle">
							<input type="hidden" name="date_end_alt" value="{if $period.date_end|strtotime>0}{$period.date_end|date_format:'%Y-%m-%d'|escape}{/if}" id="alt_date_end{$rand}">
							<script>{literal}
								$(function(){
									$('#date_end{/literal}{$rand}{literal}').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: 'yy-mm-dd', altField: '#alt_date_end{/literal}{$rand}{literal}', showOn: 'both'});
								});
							{/literal}</script>
						{case value='2'}
							{ld i='month-names' gid='start' assign='month_names'}
							<select name="date_end_month" class="middle">
								<option value="">{$month_names.header}</value>
								{foreach item=item key=key from=$month_names.option}
								<option value="{$key}" {if $key eq $period.date_end|date_format:'%m'}selected{/if}>{$item}</option>
								{/foreach}
							</select>
							{assign var='cyear' value=('now'|date:'Y')}
							<select name="date_end_year" class="short">
								<option value="">{l i='text_year' gid='listings'}</option>
								{for start=0 stop=10 value=i}
								{math equation='x + y' x=$cyear y=$i assign='year'}
								<option value="{$year}" {if $year eq $period.date_end|date_format:'%Y'}selected{/if}>{$year}</option>
								{/for}
							</select>
					{/switch}
				</div>
			</div>
			{*<div class="r clr fleft">
				<div class="f">{l i='field_booking_status' gid='listings'}:</div>
				<div class="v">
					{ld i='booking_status' gid='listings' assign='booking_status'}
					<select name="period[status]" class="middle">
						<option value="">{$booking_status.header}</option>
						{foreach item=item key=key from=$booking_status.option}
						<option value="{$key}" {if $key eq $period.status}selected{/if}>{$item}</option>
						{/foreach}
					</select>
				</div>
			</div>*}
			<div class="r fleft">&nbsp;</div>
			{*<div class="r fleft">
				<div class="f">{l i='field_booking_guests' gid='listings'}:</div>
				<div class="v">
					{ld i='booking_guests' gid='listings' assign='booking_guests'}
					<select name="period[guests]" class="middle">
						<option value="">{$booking_guests.header}</option>
						{foreach item=item key=key from=$booking_guests.option}
						<option value="{$key}" {if $key eq $period.guests}selected{/if}>{$item}</option>
						{/foreach}
					</select>
				</div>
			</div>*}
			{if $period.price}
				{assign var='booking_price' value=$period.price}
			{elseif $data.price_negotiated}
				{assign var='booking_price' value='0'}
			{elseif $data.price_reduced > 0}
				{assign var='booking_price' value=$data.price_reduced}
			{else}
				{assign var='booking_price' value=$data.price}
			{/if}
			<div class="r fleft clr">
				<div class="f">{l i='field_booking_price' gid='listings'}:</div>
				<div class="v">
					<input type="text" name="period[price]" id="price" value="{$booking_price|escape}" class="middle"> {$current_price_currency.abbr} {ld_option i='price_period' gid='listings' option=$price_period}
				</div>
			</div>
			<div class="r clr">
				<div class="f">{l i='field_booking_comment' gid='listings'}:</div>
				<div class="v" id="period_comment">
					<textarea name="period[comment]" rows="10" cols="80">{$period.comment|escape}</textarea>
				</div>
			</div>
			<div class="b">
			    <input type="hidden" name="name" value="{$user.name}">
				<input type="hidden" name="mail" value="{$user.contact_email}">
				<input type="submit" name="btn_save_period" value="{l i='btn_save' gid='start' type='button'}" id="close_btn">
			</div>
			<input type="hidden" name="save_btn" value="1">
		</form>
	</div>
</div>
<script>{literal}
		$(function(){
		      $("#price").keypress(function (e) {
               if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                 return false;
                 }
            });
		   });
{/literal}</script>
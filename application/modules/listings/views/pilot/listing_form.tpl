{include file="header.tpl" load_type='ui'}
	<div class="content-block">
		<div class="edit_block">
			<h1>{if $data.id}{l i='header_listing_edit' gid='listings'}{else}{l i='header_add_listing' gid='listings'}{/if}</h1>
			
			{include file="my_listings_menu.tpl" module=listings}

			{if $moderation_alerts}
			{foreach item=item key=type from=$moderation_alerts}
			<div class="alert-block {$type}" id="{$type}_message"><div class="close"><ins class="fa fa-times fa-lg edge hover"></ins></div>
			{if $type ne 'info'}{l i="moderation_alert_$type" gid='listings'}{/if}
			{if $item}{if $type ne 'info'}<br><b>{l i='subheader_moderator_comment' gid='listings'}: </b>{/if}{$item}{/if}
			</div>
			{/foreach}
			{js module=listings file='listings-alert.js'}
			<script>{literal}
			$(function(){
				new listingsAlert({
					siteUrl: '{/literal}{$site_root}{literal}',
					listingId: '{/literal}{$data.id}{literal}'
				});
			});
			{/literal}</script>
			{/if}
			{js module=listings file='listings-edit-steps.js'}
			<script>{literal}
			    
			function change_ot(ot){
			    switch(ot){
					case 'sale':
						$('#price_max').val('').hide();
						$('#square_max').val('').hide();
						$('#price_reduced_box').show();
						$('#price_negotiated_sale').show();
						$('#price_negotiated_buy').hide();
						$('#address_box').show();
						$('#booking_period').hide();
						$('#price_period').val('');
						$('#price_type').val('');
						$('#price_order_box').hide();
					break;
					case 'buy':
						$('#price_max').show();
						$('#square_max').show();
						$('#price_reduced_box').hide();
						$('#price_reduced').val('');
						$('#price_negotiated_sale').hide();
						$('#price_negotiated_buy').show();
						$('#address_box').hide();
						$('#address').val('');
						$('#booking_period').hide();
						$('#price_period').val('');
						$('#price_type').val('');
						$('#price_order_box').hide();
					break;
					case 'rent':
						$('#price_max').val('').hide();
						$('#square_max').val('').hide();
						$('#price_reduced_box').show();
						$('#price_negotiated_sale').show();
						$('#price_negotiated_buy').hide();
						$('#address_box').show();
						$('#booking_period').show();
						if($('#price_period').val() == 1){
							$('#price_order_box').show();
						}else{
							$('#price_order_box').hide();
						}
					break;
					case 'lease':
						$('#price_max').show();
						$('#square_max').show();
						$('#price_reduced_box').hide();
						$('#price_reduced').val('');
						$('#price_negotiated_sale').hide();
						$('#price_negotiated_buy').show();
						$('#address_box').hide();
						$('#address').val('');
						$('#booking_period').show();
						$('#price_order_box').hide();
					break;
				}
				$('#price_negotiated_box').show();
			}
			
			$(function(){
				new listingsEditSteps({
					currentStep: '{/literal}{$step}{literal}'
				});
				$('#price_negotiated').bind('click', function(){
					if(this.checked){
						$('#price').prop('disabled', true);
						$('#price_max').prop('disabled', true);
						$('#price_reduced').prop('disabled', true);
						$('#price_unit').prop('disabled', true);
					}else{
						$('#price').prop('disabled', false);
						$('#price_max').prop('disabled', false);
						$('#price_reduced').prop('disabled', false);
						$('#price_unit').prop('disabled', false);
					}
				});
				
				$('#price_period').bind('click', function(){
					if($(this).val() == 1){
						$('#price_order_box').show();
					}else{
						$('#price_order_box').hide();
					}
				});
			});
			{/literal}</script>
			{if $section_gid eq 'overview'}
			{if $data.id}<div class="rollup-box">{/if}
				<form method="post" action="{$data.action|escape}" name="save_form" id="save_form" enctype="multipart/form-data">
				<div class="r">
					{if $data.id}
						<div class="v">{l i='field_listing_type' gid='listings'}: 
							<span class="controller-select">{$data.operation_type_str}</span>
						</div>
					{elseif $operation_types_count eq 1}
						{l i='field_listing_type' gid='listings'}: 
						{foreach item=item from=$operation_types}
						<input type="hidden" name="type" value="{$item|escape}" />
						{l i='operation_type_'+$item  gid='listings'}
						{/foreach}
						<script>{literal}
						    $(function(){
							var operation_type = '{/literal}{$item}{literal}';
							change_ot(operation_type);
						    });
						{/literal}</script>
					{else}
					<div class="f">{l i='field_listing_type' gid='listings'}:{if !$data.id}&nbsp;*{/if} </div>
					<div class="v">
						{foreach item=item from=$operation_types}
						<input type="radio" name="type" value="{$item|escape}" id="for_{$item|escape}" {if $data.operation_type eq $item}checked{/if} />
						<label for="for_{$item|escape}">{l i='operation_type_'+$item gid='listings'}</label>
						{/foreach}
						<script>{literal}
							$(function(){
								$('input[name=type]').bind('change', function(){
									change_ot(this.value);
								});
							});
						{/literal}</script>					
					</div>
					{/if}
				</div>
				<div class="r">
					<div class="v">{l i='listing_id' gid='listings'} 
				<span class="controller-select">{$data.id}</span>
					</div>
					</div>
				<div class="r">
					{assign var='selected_category' value=$data.id_category+'_'+$data.property_type}
					{if $data.id && $data.id_category && $data.property_type}
					<div class="f">{l i='field_category' gid='listings'}: <span class="controller-select">{$data.category_str} , {$data.property_type_str}</span></div>
					<div class="v">
		{block name='properties_select' module='properties' var_name='data[category]' selected=$selected_category id_category=$data.id_category category_str=$data.category_str}
					</div>
					{else}
					<div class="f">{l i='field_category' gid='listings'}:&nbsp;* </div>
					<div class="v">
						{block name='properties_select' module='properties' var_name='data[category]' selected=$selected_category cat_select=false js_var_name='category'}
					</div>
					{/if}
				</div>
				<div class="r">
					<div class="f">{l i='field_location' gid='listings'}:&nbsp;* </div>
			<div class="v">{country_select select_type='city' id_country=$data.id_country id_region=$data.id_region id_city=$data.id_city id_district=$data.id_district}</div>
				</div>
				<div class="r {if $data.operation_type ne 'sale' && $data.operation_type ne 'rent'}hide{/if}" id="address_box">
					<div class="f">{l i='field_address' gid='listings'}: </div>
					<div class="v"><input type="text" name="data[address]" value="{$data.address|escape}" id="address"></div>
				</div>
				<div class="r">
					<div class="f">{l i='field_postal_code' gid='listings'}: </div>
					<div class="v"><input type="text" name="data[zip]" id="zip" value="{$data.zip|escape}" class="middle"/></div>
				</div>
				<div class="r {if $data.operation_type ne 'rent' && $data.operation_type ne 'lease'}hide{/if}" id="booking_period">
					<div class="f">{l i='field_booking_period' gid='listings'}:&nbsp;* </div>
					<div class="v">
						{if $data.id}
						{ld i='price_period' gid='listings' assign='price_period'}
					<select name="data[price_period]" class="middle {if $data.operation_type ne 'rent' && $data.operation_type ne 'lease'}hide{/if}" id="price_period">
					{foreach item=item key=key from=$price_period.option}
					<option value="{$key}" {if $key eq $data.price_period}selected{/if}>{$item}</option>
					{/foreach}
					</select>
						{else}
						{ld i='price_period' gid='listings' assign='price_period'}
						<select name="data[price_period]" class="middle" id="price_period">
						{foreach item=item key=key from=$price_period.option}
						<option value="{$key}" {if $key eq $data.price_period}selected{/if}>{$item}</option>
						{/foreach}
						</select>
						{/if}
						{*//
						{ld i='price_type' gid='listings' assign='price_type'}
						<select name="data[price_type]" class="middle" id="price_type">
						<option value="">{$price_type.header}</option>
						{foreach item=item key=key from=$price_type.option}
						<option value="{$key}" {if $key eq $data.price_type}selected{/if}>{$item}</option>
						{/foreach}
						</select>*}
					</div>
				</div>
		<div style="display:none;">
				<div class="r {if !$data.operation_type}hide{/if}" id="price_negotiated_box">
					<div class="v">
						<input type="hidden" name="data[price_negotiated]" value="0">
						<input type="checkbox" name="data[price_negotiated]" value="1" id="price_negotiated" {if $data.price_negotiated}checked{/if}> 
						{foreach item=item from=$operation_types}
						<label for="price_negotiated" id="price_negotiated_{$item}" {if $data.operation_type ne $item && $operation_types_count > 1}class="hide"{/if}>{l i='field_price_negotiated_'+$item gid='listings'}</label>
						{/foreach}
					</div>
				</div>
		</div>
				<div class="r">
					<div class="f">{l i='field_price' gid='listings'}:&nbsp;* </div>
					<div class="v">
						<input type="text" name="data[price]" value="{$data.price|escape}" id="price" class="middle" {if $data.price_negotiated}disabled{/if}>
						<input type="text" name="data[price_max]" value="{$data.price_max|escape}" id="price_max" class="{if $data.operation_type ne 'buy' && $data.operation_type ne 'lease'}hide{/if} middle" {if $data.price_negotiated}disabled{/if}> 
						{depends module=payments}
						{if $data.id}
							{$current_price_currency.abbr}
							{if $data.operation_type eq 'rent' || $data.operation_type ne 'lease'}
							{ld_option i='price_period' gid='listings' option=$data.price_period}
							{/if}
						{else}
						<select name="data[gid_currency]" class="short" id="price_unit" {if $data.price_negotiated}disabled{/if}>
							{foreach item=item from=$currencies}
							<option value="{$item.gid|escape}" {if $item.gid eq $data.gid_currency}selected{/if}>{$item.abbr}</option>
							{/foreach}
						</select>
						<script>{literal}
							$(function(){
								$('#price_unit').bind('click', function(){
									var text = $(this).find(':selected').html();
									$('#price_reduced_unit').html(text);
								});
							});
						{/literal}</script>
						{/if}
						{/depends}
					</div>
				</div>
				
		
				<div class="r {if $data.operation_type ne 'rent' || $data.price_period ne 1}hide{/if}" id="price_order_box">
					<div class="f">{l i='field_price_order' gid='listings'}: </div>
					<div class="v">
						<input type="text" name="data[price_week]" value="{$data.price_week|escape}" id="price_week" class="middle" {if $data.price_negotiated}disabled{/if}> 
						<span id="price_week_unit">{$current_price_currency.abbr}</span>
						<span>{ld_option i='price_period' gid='listings' option=$data.price_period}, {l i='text_price_week' gid='listings'}</span>
						<br><br>
						<input type="text" name="data[price_month]" value="{$data.price_month|escape}" id="price_month" class="middle" {if $data.price_negotiated}disabled{/if}> 
						<span id="price_month_unit">{$current_price_currency.abbr}</span>
						<span>{ld_option i='price_period' gid='listings' option=$data.price_period}, {l i='text_price_month' gid='listings'}</span>
					</div>
				</div>
				
				{*<div class="r {if $data.operation_type ne 'sale' && $data.operation_type ne 'rent'}hide{/if}" id="price_reduced_box">
					<div class="f">{l i='field_price_reduced' gid='listings'}: </div>
					<div class="v">
					<input type="text" name="data[price_reduced]" value="{$data.price_reduced|escape}" id="price_reduced" class="middle" {if $data.price_negotiated}disabled{/if}> 
						<span id="price_reduced_unit">{$current_price_currency.abbr}</span>
						{if $data.operation_type eq 'rent'}
						{ld_option i='price_period' gid='listings' option=$data.price_period}
						{/if}
					</div>
				</div>*}
				<div style="display:none">
				<div class="r">
					<div class="v">
						<input type="hidden" name="data[price_auction]" value="0">
						<input type="checkbox" name="data[price_auction]" value="1" {if $data.price_auction}checked{/if}> 
						<label for="auction">{l i='field_price_auction' gid='listings'}</label>
					</div>
				</div>
				<div class="r">
					<div class="f">{l i='field_date_open' gid='listings'}: </div>
					<div class="v periodbox">
						<input type="text" name="data[date_open]" value="{if $data.date_open|strtotime>0}{$data.date_open|date_format:$page_data.date_format|escape}{/if}" id="date_open" class="middle" /> 
						<input type="hidden" name="date_open_alt" value="{if $data.date_open|strtotime>0}{$data.date_open|date_format:'%Y-%m-%d'|escape}{/if}" id="alt_date_open">
						<script>{literal}
							$(function(){
								$('#date_open').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#alt_date_open', showOn: 'both'});
							});
						{/literal}</script>
						{l i='text_from' gid='listings'}:
						<select name="data[date_open_begin]" class="middle"> 
							<option value="" {if !$data.time_open_begin}selected="selected"{/if}>{$dayhours.header}</option>
							{foreach item=item key=key from=$dayhours.option}
							<option value="{$key|escape}" {if $data.date_open_begin eq $key}selected{/if}>{$item}</option>
							{/foreach}
						</select>
						{l i='text_till' gid='listings'}:
						<select name="data[date_open_end]" class="middle"> 
							<option value="" {if !$data.date_open_end}selected="selected"{/if}>{$dayhours.header}</option>
							{foreach item=item key=key from=$dayhours.option}
							<option value="{$key|escape}" {if $data.date_open_end eq $key}selected{/if}>{$item}</option>
							{/foreach}
						</select>
					</div>
				</div>
				<div class="r periodbox">
					<div class="f">{l i='field_date_available' gid='listings'}: </div>
					<div class="v">
						<input type="text" name="data[date_available]" value="{if $data.date_open|strtotime>0}{$data.date_available|date_format:$page_data.date_format|escape}{/if}" id="date_available" class="middle" /> 
						<input type="hidden" name="date_available_alt" value="{if $data.date_open|strtotime>0}{$data.date_available|date_format:'%Y-%m-%d'|escape}{/if}" id="alt_date_available">
						<script>{literal}
							$(function(){
								$('#date_available').datepicker({dateFormat: '{/literal}{$page_data.datepicker_date_format}{literal}', altFormat: '{/literal}{$page_data.datepicker_alt_format}{literal}', altField: '#alt_date_available', showOn: 'both'});
							});
						{/literal}</script>
					</div>
				</div>
			</div>
			
				<div class="r">
					<div class="f">{l i='field_square' gid='listings'}:&nbsp;* </div>
					<div class="v">
						<input type="text" name="data[square]" value="{$data.square|escape}" class="middle">
						<input type="text" name="data[square_max]" value="{$data.square_max|escape}" id="square_max" class="{if $data.operation_type ne 'buy' && $data.operation_type ne 'lease'}hide{/if} middle"> 
						<select name="data[square_unit]" class="short">
							{foreach item=item key=key from=$square_units.option}
							<option value="{$key|escape}" {if $key eq $data.square_unit}selected{/if}>{$item}</option>
							{foreachelse}
							<option value="">{l i='select_default' gid='start'}</option>
							{/foreach}
						</select>
					</div>
				</div>
				{*<div class="r">
					<div class="f">{l i='field_wish_list' gid='listings'}: </div>
					<div class="v">{wish_list_select selected=$data.id_wish_lists var_name='id_wish_lists'}</div>
				</div>*}
				
				<div style="display:none;">
				<div class="r">
					<div class="f">{l i='field_headline' gid='listings'}: </div>
					<div class="v">
						<textarea name="data[headline]" rows="10" cols="80">{$data.headline|escape}</textarea>
					</div>
				</div>
				</div>
				{foreach item=lang_item key=lang_id from=$langs}
					{assign var='custm_gid' value='headline_lang'}
					{if $lang_item.status eq '1'}
					<div class="r">
						<div class="f">{l i='field_headline' gid='listings'}: {$lang_item.name}</div>
						<div class="v">
							<textarea name="data[{$custm_gid}][{$lang_id}]" class="target" rows="10" cols="80">{$data[$custm_gid][$lang_id]|strip_tags}</textarea>
						</div>
					</div>
					{/if}
				{/foreach}
				<div class="b">
					<input type="submit" name="btn_save_general" value="{l i='btn_save' gid='start' type='button'}">
				</div>
				<input type="hidden" name="save_btn" value="1">
				<input type="hidden" name="data[lat]" value="{$data.lat|escape}" id="lat">
				<input type="hidden" name="data[lon]" value="{$data.lon|escape}" id="lon">
				<input type="hidden" name="data[is_map_panorama]" value="{$data.is_map_panorama|escape}" id="is_map_panorama">
				</form>
			{if $data.id}</div>{/if}
			{block name=geomap_load_geocoder module='geomap'}
			{block name=geomap_load_street_view module='geomap'}
			<script>{literal}
				function update_coordinates(country, region, city, address, postal_code){
					if(typeof(geocoder) != 'undefined'){
						var location = geocoder.getLocationFromAddress(country, region, city, address, postal_code);
						geocoder.geocodeLocation(location, function(latitude, longitude){
							$('#lat').val(latitude);
							$('#lon').val(longitude);
							if(typeof(street_view) != 'undefined'){
								street_view.checkStreetView(latitude, longitude, function(is_map_panorama){
									$('#is_map_panorama').val(is_map_panorama);
								});
							}
						});	
					}
				}
				$(function(){
					var location_change_wait = 0;
					var country_old = '{/literal}{$data.id_country}{literal}';
					var region_old = '{/literal}{$data.id_region}{literal}';
					var city_old = '{/literal}{$data.id_city}{literal}';
					var address_old = '{/literal}{$data.address|replace:"'":"\'"}{literal}';
					var postal_code_old = '{/literal}{$data.zip|replace:"'":"\'"}{literal}';
		
					$('input[name=id_city]').bind('change', function(){
						var city = $(this).val();
						if(city == 0) return;
						location_change_wait++;
						check_address_updated();
					});
		              $("#zip,#price").keypress(function (e) {
                      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                      return false;
                       }
                      });
					$('input[name=data\\[address\\]], input[name=data\\[zip\\]]').bind('keypress', function(){
						location_change_wait++;
						setTimeout(check_address_updated, 1000);
					});
		
					function check_address_updated(){
						location_change_wait--;
						if(location_change_wait) return;
						var country = $('input[name=id_country]').val();
						var region = $('input[name=id_region]').val();
						var city = $('input[name=id_city]').val();
						var address = $('input[name=data\\[address\\]]').val();
						var postal_code = $('input[name=data\\[zip\\]]').val();
						if(country == country_old && region == region_old && 
							city == city_old && address == address_old && postal_code == postal_code_old) return;
						country_old = country;
						region_old = region;
						city_old = city;
						address_old = address;
						postal_code_old = postal_code;
						var country_name = $('input[name=id_country]').attr('data-name');
						var region_name = $('input[name=id_region]').attr('data-name');
						var city_name = $('input[name=id_city]').attr('data-name');
						update_coordinates(country_name, region_name, city_name, address, postal_code);
					}
				});
			{/literal}</script>
			{elseif $section_gid eq 'description'}
			{foreach item=section_item  key=section_key from=$sections_data}
			<div class="rollup-box">
				<div class="title">
					<div class="btn-link"><ins class="fa {if $step ne $section_key}fa-plus{else}fa-minus{/if} fa-lg edge hover"></ins></div>
					<h2>{$section_item.name}</h2>
				</div>
				<div class="cont{if $step ne $section_key} hide{/if}">
					<form method="post" action="{$data.action}" enctype="multipart/form-data">
					{foreach item=item from=$fields_data}
					{if $item.section_gid eq $section_item.gid}
					{if $item.field_name eq 'fe_comments_1' || $item.field_name eq 'fe_comments_4' || $item.field_name eq 'fe_comments_2' || $item.field_name eq 'fe_comments_5' || $item.field_name eq 'fe_comments_3' || $item.field_name eq 'fe_comments_6'}
					
					{foreach item=lang_item key=lang_id from=$languages}
						{assign var='custm_gid' value='comments_lang'}
						{if $lang_item.status eq '1'}
						<div class="r">
							<div class="f">{$item.name}: {$lang_item.name}</div>
							<div class="v"><textarea name="data[{$custm_gid}][{$lang_id}]" class="target">{$data[$custm_gid][$lang_id]|strip_tags}</textarea></div>
						</div>
						{/if}
					{/foreach}
					
					{else}
					<div class="r">
						<div class="f">
							{$item.name}: {if $item.settings_data_array.min_char > 0}{l i='text_min_char' gid='listings'}&nbsp;<b>{$item.settings_data_array.min_char}</b>{/if} {if $item.settings_data_array.max_char > 0}{l i='text_max_char' gid='listings'}&nbsp;<b>{$item.settings_data_array.max_char}</b>{/if}
						</div>
						<div class="v">
						{if $item.field_type eq 'select'}
							{if $item.settings_data_array.view_type eq 'select'}
							<select name="{$item.field_name}">
							{if $item.settings_data_array.empty_option}<option value="0"{if $value eq 0} selected{/if}>...</option>{/if}
							{foreach item=option key=value from=$item.options.option}<option value="{$value|escape}" {if $value eq $item.value}selected{/if}>{$option}</option>{/foreach}
							</select>
							{else}
							{if $item.settings_data_array.empty_option}<input type="radio" name="{$item.field_name}" value="0" {if $value eq 0}checked{/if} id="{$item.field_name}_0">
							<label for="{$item.field_name}_0">No select</label><br>{/if}
							{foreach item=option key=value from=$item.options.option}
							<input type="radio" name="{$item.field_name}" value="{$value|escape}" {if $value eq $item.value}checked{/if} id="{$item.field_name}_{$value|escape}">
							<label for="{$item.field_name}_{$value|escape}">{$option}</label><br>
							{/foreach}
							{/if}
						{elseif $item.field_type eq 'multiselect'}
							<div class="fleft">
								{foreach item=option key=value from=$item.options.option}
								<input type="checkbox" name="{$item.field_name}[]" value="{$value|escape}" {if $value|in_array:$item.value}checked{/if} id="{$item.field_name}_{$value|escape}" class="width4">
								<label for="{$item.field_name}_{$value|escape}" class="width4">{$option}</label>
								{/foreach}
								<input type="hidden" name="{$item.field_name}[]" value="0">
							</div>
							<div class="clr"></div>
							<div class="multiselect_actions">
								<a href="javascript:void(0)" id="{$item.field_name}_select_all">{l i='select_all' gid='start'}</a>
								<a href="javascript:void(0)" id="{$item.field_name}_unselect_all">{l i='unselect_all' gid='start'}</a>
							</div>
							<script>{literal}
								$(function(){
									$('#{/literal}{$item.field_name}_select_all{literal}').bind('click', function(){
										$('input[name^={/literal}{$item.field_name}{literal}]').prop('checked', true);
									});
									$('#{/literal}{$item.field_name}_unselect_all{literal}').bind('click', function(){
										$('input[name^={/literal}{$item.field_name}{literal}]').prop('checked', false);
									});
								});
							{/literal}</script>	
						{elseif $item.field_type eq 'text'}
							<input type="text" name="{$item.field_name}" value="{$item.value|escape}" maxlength="{$item.settings_data_array.max_char|escape}" {if $item.settings_data_array.max_char < 11}class="short"{elseif $item.settings_data_array.max_char > 1100}class="long"{/if}>
						{elseif $item.field_type eq 'textarea'}
							<textarea name="{$item.field_name}">{$item.value|escape}</textarea>
						{elseif $item.field_type eq 'checkbox'}
							<input type="checkbox" name="{$item.field_name}" value="1" {if $item.value eq '1'}checked{/if}>
						{/if}
						&nbsp;
						{if $item.comment}<p class="help">{$item.comment}</p>{/if}
						</div>
					</div>
					{/if}
					{/if}
					{/foreach}
					<div class="b">
						<input type="submit" name="btn_save_description" value="{l i='btn_save' gid='start' type='button'}">
					</div>
					<input type="hidden" name="field_editor_section_gid" value="{$section_item.gid}">
					<input type="hidden" name="save_btn" value="1">
					</form>
				</div>
			</div>
			{/foreach}
			{elseif $section_gid eq 'gallery' && ($data.operation_type eq 'sale' || $data.operation_type eq 'rent')}
			<link rel="stylesheet" href="{$site_root}{$js_folder}jquery.imgareaselect/jquery.imgareaselect.css" type="text/css" />
			{js file='ajaxfileupload.min.js'}
			{js file='gallery-uploads.js'}
			{js file='jquery.rotate.js'}
			{js module=users_services file='available_view.js'}
			{js file='jquery.imgareaselect/jquery.imgareaselect.min.js'}
			{js module='upload_gallery' file='photo-edit.js'}
			<div class="rollup-box">
				<div class="title">
					<div class="btn-link"><ins class="fa {if $step ne 1}fa-plus{else}fa-minus{/if} fa-lg edge hover"></ins></div>
					<h2>{l i='header_listing_photo' gid='listings'}</h2>
				</div>
				<div class="cont{if $step ne 1} hide{/if}">
					<p class="settings">
						{if $gallery_settings.max_items_count}{l i='max_photo_header' gid='listings'}: <span>{$gallery_settings.max_items_count}</span><br>{/if}
						{if $gallery_settings.upload_settings.max_size_str}{l i='max_photo_size_header' gid='listings'}: <span>{$gallery_settings.upload_settings.max_size_str}</span><br>{/if}
						{if $gallery_settings.upload_settings.min_width && $gallery_settings.upload_settings.min_height}{l i='min_photo_width_and_height_header' gid='listings'}: <span>{$gallery_settings.upload_settings.min_width}x{$gallery_settings.upload_settings.min_height} {l i='photo_width_and_height_unit' gid='listings'}</span><br>{/if}
						{if $gallery_settings.upload_settings.file_formats}{l i='text_accepted_file_types' gid='listings'}: <span>{', '|implode:$gallery_settings.upload_settings.file_formats}</span><br>{/if}
					</p>
					
					<p>{l i='photo_upload_header' gid='listings'}</p>
					
					<script>{literal}
						var gUpload;
						var intervalID;
						var rotateAngle = -2;
						$(function(){
							gUpload = new galleryUploads({
								siteUrl: '{/literal}{$site_root}{literal}',
								objectId: '{/literal}{$data.id}{literal}',
								maxPhotos: '{/literal}{$data.max_photos}{literal}',
								maxPhotoError: '{/literal}{l i='error_max_photos_reached' gid='listings' type='js'}{literal}',
								uploadPhotoUrl: 'listings/photo_upload/{/literal}{$data.id}{literal}/',
								savePhotoDataUrl: 'listings/ajax_save_photo_data/{/literal}{$data.id}{literal}/',
								saveSortingUrl: 'listings/ajax_save_photo_sorting/{/literal}{$data.id}{literal}',
								deletePhotoUrl: 'listings/ajax_delete_photo/{/literal}{$data.id}{literal}/',
								formPhotoUrl: 'listings/ajax_photo_form/{/literal}{$data.id}{literal}/',
								reloadBlockUrl: 'listings/ajax_reload_photo_block/{/literal}{$data.id}{literal}/',
								reloadCallback: function(id){
									var photo_edit_block = $('#photo_edit_block');
									if(!photo_edit_block.length) return;
									var photo_comment = photo_edit_block.find('#photo_comment').val();
									photo_edit_block.find('#photo_source_view_box .statusbar .panel').html(photo_comment);
									if(photo_comment){
										photo_edit_block.find('#photo_source_view_box .statusbar').show();
									}else{
										photo_edit_block.find('#photo_source_view_box .statusbar').hide();
									}
								},
								deleteCallback: function(id){
									if($('#photo_carousel .photo-info-area').length == 0) $('#reorder_btn').hide();
								},
								listItemID: 'photo_carousel',
								closeAfterSave: false,
							});		
							$('#photo_carousel').sortable('disable');
							$('#reorder_btn').bind('click', function(){
								$('#photo_carousel').addClass('rotate');
								$('#photo_carousel').find('.photo-info-area .photo-area').each(function(index, item){
									var childs = $(item).children();
									$(childs[0]).rotateLeft(rotateAngle);
								});
								$(this).hide().next().show();
								$('#photo_carousel').sortable('enable');
							}).next().bind('click', function(){
								$('#photo_carousel').find('.photo-info-area .photo-area').each(function(index, item){
									$(item).parent().find('.canvas').html('').hide();
									$(item).show();
									$(item).next().show();
								});
								$(this).hide().prev().show();
								$('#photo_carousel').removeClass('rotate');
								$('#photo_carousel').sortable('disable');
							});
						});
					{/literal}</script>
				
					<form method="post" action="{$data.action|escape}" name="save_photo_form" id="save_photo_form" enctype="multipart/form-data">
					{capture assign="uploader_callback"}{literal}
						function(name, data){
							if(typeof(data.id) != 'undefined' && data.id > 0){
								gUpload.save_photo_data(data.id);
								$('#reorder_btn').show();
							}
						}
					{/literal}{/capture}
					{block name='uploader_block' module='uploads' field_name='photo_file' form_id='save_photo_form' url='listings/photo_upload/'+$data.id callback=$uploader_callback}
					
					<div class="clr"></div>
					
					<ol class="blocks" id="photo_carousel">
						{foreach item=photo from=$data.photos}
						<li id="pItem{$photo.id}">
							{include file="photo_view_block.tpl" module=listings}
						</li>
						{/foreach}
					</ol>
					<div class="clr"></div>			
					<div class="b">
						<a href="javascript:void(0)" class="btn-link {if $data.photo_count eq 0}hide{/if}" id="reorder_btn"><ins class="fa fa-arrows fa-lg edge hover"></ins>{l i='btn_photo_reorder' gid='listings' type='button'}</a>
						<a class="hide btn-link" href="javascript:void(0)"><ins class="fa fa-arrow-left fa-lg edge hover"></ins>{l i='btn_cancel' gid='start'}</a>
					</div>					
					<input type="hidden" name="save_btn" value="1">
					</form>
					<div class="clr"></div>
				</div>
			</div>
			<div class="rollup-box">			
				<div class="title">
					<div class="btn-link"><ins class="fa {if $step ne 2}fa-plus{else}fa-minus{/if} fa-lg edge hover"></ins></div>
					<h2>{l i='header_listing_virtual_tour' gid='listings'}</h2>
				</div>
				<div class="cont{if $step ne 2} hide{/if}">
					<p class="settings">
						{if $vtour_settings.max_items_count}{l i='max_panorama_header' gid='listings'}: <span>{$vtour_settings.max_items_count}</span><br>{/if}
						{if $vtour_settings.upload_settings.max_size_str}{l i='max_panorama_size_header' gid='listings'}: <span>{$vtour_settings.upload_settings.max_size_str}</span><br>{/if}
						{if $vtour_settings.upload_settings.min_width && $vtour_settings.upload_settings.min_height}{l i='min_panorama_width_and_height_header' gid='listings'}: <span>{$vtour_settings.upload_settings.min_width}x{$vtour_settings.upload_settings.min_height} {l i='photo_width_and_height_unit' gid='listings'}</span><br>{/if}
						{if $vtour_settings.upload_settings.file_formats}{l i='text_accepted_file_types' gid='listings'}: <span>{', '|implode:$vtour_settings.upload_settings.file_formats}</span><br>{/if}
					</p>
					
					<p>{l i='panorama_upload_header' gid='listings'}</p>
					
					<script>{literal}
						var vtourUpload;
						var rotateAngle = -2;
						$(function(){
							vtourUpload = new galleryUploads({
								siteUrl: '{/literal}{$site_root}{literal}',
								objectId: '{/literal}{$data.id}{literal}',
								fileElementId: 'panorama_file',
								commentElementId: 'panorama_comment',
								maxPhotos: '{/literal}{$data.max_panorama}{literal}',
								maxPhotoError: '{/literal}{l i='error_max_panorama_reached' gid='listings' type='js'}{literal}',
								uploadPhotoUrl: 'listings/panorama_upload/{/literal}{$data.id}{literal}/',
								savePhotoDataUrl: 'listings/ajax_save_panorama_data/{/literal}{$data.id}{literal}/',
								saveSortingUrl: 'listings/ajax_save_panorama_sorting/{/literal}{$data.id}{literal}',
								deletePhotoUrl: 'listings/ajax_delete_panorama/{/literal}{$data.id}{literal}/',
								formPhotoUrl: 'listings/ajax_panorama_form/{/literal}{$data.id}{literal}/',
								reloadBlockUrl: 'listings/ajax_reload_panorama_block/{/literal}{$data.id}{literal}/',
								reloadCallback: function(id){
									$.get('{/literal}{$site_root}{literal}'+'listings/ajax_reload_panorama/{/literal}{$data.id}{literal}/'+id, {}, function(data){
										$('#panorama_block').html(data);
									});
									var photo_edit_block = $('#photo_edit_block');
									if(!photo_edit_block.length) return;
									var photo_comment = photo_edit_block.find('#panorama_comment').val();
									photo_edit_block.find('#photo_source_view_box .statusbar .panel').html(photo_comment);
									if(photo_comment){
										photo_edit_block.find('#photo_source_view_box .statusbar').show();
									}else{
										photo_edit_block.find('#photo_source_view_box .statusbar').hide();
									}
								},
								deleteCallback: function(id){
									if($('#panorama_carousel .photo-info-area').length == 0) $('#reorder_vtour_btn').hide();
									$('#panorama_block').html('');
								},
								listItemID: 'panorama_carousel',
								closeAfterSave: false,
							});	
							$('#panorama_carousel').sortable('disable');
							$('#reorder_vtour_btn').bind('click', function(){
								$('#panorama_carousel').addClass('rotate');
								$('#panorama_carousel').find('.photo-info-area .photo-area').each(function(index, item){
									var childs = $(item).children();
									$(childs[0]).rotateLeft(rotateAngle);
								});
								$(this).hide().next().show();
								$('#panorama_carousel').sortable('enable');
							}).next().bind('click', function(){
								$('#panorama_carousel').find('.photo-info-area .photo-area').each(function(index, item){
									$(item).parent().find('.canvas').html('').hide();
									$(item).show();
									$(item).next().show();
								});
								$('#panorama_carousel').removeClass('rotate');
								$(this).hide().prev().show();
								$('#panorama_carousel').sortable('disable');
							});
						});
					{/literal}</script>
			
					<form method="post" action="{$data.action|escape}" name="save_panorama_form" id="save_panorama_form" enctype="multipart/form-data">
					{capture assign="uploader_callback"}{literal}
						function(name, data){
							if(typeof(data.id) != 'undefined' && data.id > 0){
								vtourUpload.save_photo_data(data.id);
								$.get('{/literal}{$site_root}{literal}'+'listings/ajax_reload_panorama/{/literal}{$data.id}{literal}/'+data.id, {}, function(data){
									$('#reorder_vtour_btn').show();
									$('#panorama_block').html(data);
								});
							}
						}
					{/literal}{/capture}
					{block name='uploader_block' module='uploads' field_name='panorama_file' form_id='save_panorama_form' url='listings/panorama_upload/'+$data.id callback=$uploader_callback}
					
					<div class="clr"></div>
					
					<div id="panorama_block" class="edit">{if $data.virtual_tour_count}{block name=virtual_tour_block module=listings data=$data.virtual_tour.0}{/if}</div>
					
					<ol class="blocks" id="panorama_carousel">
						{foreach item=panorama from=$data.virtual_tour}
						<li id="pItem{$panorama.id}">
							{include file="panorama_view_block.tpl" module=listings}
						</li>
						{/foreach}
					</ol>
					<div class="clr"></div>			
					<div class="b">
						<a href="javascript:void(0)" class="btn-link {if $data.is_vtour eq 0}hide{/if}" id="reorder_vtour_btn"><ins class="fa fa-arrows fa-lg edge hover"></ins>{l i='btn_panorama_reorder' gid='listings' type='button'}</a>
						<a class="hide btn-link" href="javascript:void(0)"><ins class=fa fa-arrow-left fa-lg edge hover"></ins>{l i='btn_cancel' gid='start'}</a>
					</div>
					<input type="hidden" name="save_btn" value="1">
					</form>
					<div class="clr"></div>
				</div>
			</div>
			{depends module=file_uploads}
			<div class="rollup-box">			
				<div class="title">
					<div class="btn-link"><ins class="fa {if $step ne 3}fa-plus{else}fa-minus{/if} fa-lg edge hover"></ins></div>
					<h2>{l i='header_listing_file' gid='listings'}</h2>
				</div>
				<div class="cont{if $step ne 3} hide{/if}">
					<p class="settings">
						{l i='max_file_header' gid='listings'}: <span>1</span><br>
						{if $file_settings.max_size_str}{l i='max_file_size_header' gid='listings'}: <span>{$file_settings.max_size_str}</span><br>{/if}
						{if $file_settings.file_formats}{l i='text_accepted_file_types' gid='listings'}: <span>{', '|implode:$file_settings.file_formats}</span><br>{/if}
					</p>
					<form method="post" action="{$data.action|escape}" name="save_file_form" id="save_file_form" enctype="multipart/form-data">
					<div class="r">
						<div class="f">{l i='field_listing_file' gid='listings'}:&nbsp;* </div>
						<div class="v">
							<input type="file" name="listing_file">
							{if $data.is_file}
							<br><br><a href="{$data.listing_file_content.file_url}" target="_blank">{l i='field_file_download' gid='listings'}</a>
							<br><br><input type="checkbox" name="listing_file_delete" value="1" id="uichb"><label for="uichb">{l i='field_file_delete' gid='listings'}</label><br><br>
							{/if}
						</div>
					</div>
					<div class="r">
						<div class="f">{l i='field_listing_file_name' gid='listings'}:&nbsp;* </div>
						<div class="v"><input type="text" name="data[listing_file_name]" value="{$data.listing_file_name|escape}"></div>
					</div>
					<div class="r">
						<div class="f">{l i='field_listing_file_comment' gid='listings'}: </div>
						<div class="v"><textarea name="data[listing_file_comment]" rows="10" cols="80">{$data.listing_file_comment|escape}</textarea></div>
					</div>
					<div class="b"><input type="submit" name="btn_save_file" value="{l i='btn_save' gid='start' type='button'}"></div>
					<input type="hidden" name="save_btn" value="1">
					</form>
				</div>
			</div>
			{/depends}
			{depends module=video_uploads}
			<div class="rollup-box">			
				<div class="title">
					<div class="btn-link"><ins class="fa {if $step ne 4}fa-plus{else}fa-minus{/if} fa-lg edge hover"></ins></div>
					<h2>{l i='header_listing_video' gid='listings'}</h2>
				</div>
				<div class="cont{if $step ne 4} hide{/if}">
					<p class="settings">
						{l i='max_video_header' gid='listings'}: <span>1</span><br>
						{if $video_settings.max_size_str}{l i='max_video_size_header' gid='listings'}: <span>{$video_settings.max_size_str}</span><br>{/if}
						{if $video_settings.file_formats}{l i='text_accepted_file_types' gid='listings'}: <span>{', '|implode:$video_settings.file_formats}</span><br>{/if}
					</p>
					<form method="post" action="{$data.action|escape}" name="save_video_form" id="save_video_form" enctype="multipart/form-data">
					<div class="r">
						<div class="f">{l i='field_video' gid='listings'}:&nbsp;* </div>
						<div class="v">
							<input type="file" name="listing_video">
							{if $data.listing_video}
								<br><br>{l i='field_video_status' gid='listings'}:
								{if $data.listing_video_data.status eq 'end' && $data.listing_video_data.errors}
								<font color="red">{foreach item=item from=$data.listing_video_data.errors}{$item}<br>{/foreach}</font>
								{elseif $data.listing_video_data.status eq 'end'}
								<font color="green">{l i='field_video_status_end' gid='listings'}</font><br>
								{elseif $data.listing_video_data.status eq 'images'}
								<font color="yellow">{l i='field_video_status_images' gid='listings'}</font><br>
								{elseif $data.listing_video_data.status eq 'waiting'} 
								<font color="yellow">{l i='field_video_status_waiting' gid='listings'}</font><br>
								{elseif $data.listing_video_data.status eq 'start'} 
								<font color="yellow">{l i='field_video_status_start' gid='listings'}</font><br>
								{/if}
								{if $data.listing_video_content.embed}<br>{$data.listing_video_content.embed}{/if}
								<br><input type="checkbox" name="listing_video_delete" value="1" id="uvchb"><label for="uvchb">{l i='field_video_delete' gid='listings'}</label>
							{/if}
						</div>
					</div>					
					<div class="b">
						<input type="submit" name="btn_save_video" value="{l i='btn_save' gid='start' type='button'}">
					</div>
					<input type="hidden" name="save_btn" value="1">
					</form>
				</div>
			</div>
			{/depends}
			{elseif $section_gid eq 'map'}
			{depends module=geomap}
			<script>{literal}
				if(typeof(get_listing_type_data) == 'undefined'){
					function get_listing_type_data(type){
						$('#map_type').val(type);
					}
				}
				if(typeof(get_listing_zoom_data) == 'undefined'){
					function get_listing_zoom_data(zoom){
						$('#map_zoom').val(zoom);
					}
				}
				if(typeof(get_listing_drag_data) == 'undefined'){
					function get_listing_drag_data(point_gid, lat, lon){
						$('#lat').val(lat);
						$('#lon').val(lon);
					}
				}
				if(typeof(get_listing_panorama) == 'undefined'){
					function get_listing_panorama(panorama){
						$('#is_map_panorama').val(panorama);
					}
				}
			{/literal}</script>
			<div class="rollup-box">
				<form method="post" action="{$data.action|escape}" name="save_form" id="save_form" enctype="multipart/form-data">
					<div class="r">
						<div class="f">{l i='field_geomap' gid='listings'}:</div>
						<div class="v">{block name=show_default_map module=geomap object_id=$data.id gid='listing_view' markers=$markers settings=$map_settings width='630' height='400'}</div>
					</div>	
					<div class="b">
						<input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}">
					</div>
					<input type="hidden" name="map[view_type]" value="{$listing_map_settings.view_type}" id="map_type">
					<input type="hidden" name="map[zoom]" value="{$listing_map_settings.zoom}" id="map_zoom">
					<input type="hidden" name="data[lat]" value="{$data.lat|escape}" id="lat">
					<input type="hidden" name="data[lon]" value="{$data.lon|escape}" id="lon">
					<input type="hidden" name="data[is_map_panorama]" value="{$data.is_map_panorama|escape}" id="is_map_panorama">
					<input type="hidden" name="save_btn" value="1">
				</form>
			</div>
			{/depends}
			{elseif $section_gid eq 'calendar' && $data.operation_type eq 'rent'}
			<div class="rollup-box">
				<div class="title">
					<div class="btn-link"><ins class="fa {if $step ne 1}fa-plus{else}fa-minus{/if} fa-lg edge hover"></ins></div>
					<h2>{l i='header_calendar_settings' gid='listings'}</h2>
				</div>
				<div class="cont {if $step ne 1}hide{/if}">
					<form method="post" action="{$data.action|escape}" name="save_form" id="save_form" enctype="multipart/form-data">				
						<div class="r">
							<div class="f">{l i='field_calendar_enabled' gid='listings'}:</div>
							<div class="v">
								<input type="hidden" name="data[use_calendar]" value="0">
								<input type="checkbox" name="data[use_calendar]" {if $data.use_calendar}checked{/if}>
							</div>
						</div>
						{*<div class="r">
							<div class="f">{l i='field_calendar_period_min' gid='listings'}:</div>
							<div class="v">
								<input type="text" name="data[calendar_period_min]" value="{$data.calendar_period_min|escape}" class="middle"> {ld_option i='price_period_unit' gid='listings' option=$data.price_period}
							</div>
						</div>
						<div class="r">
							<div class="f">{l i='field_calendar_period_max' gid='listings'}:</div>
							<div class="v">
								<input type="text" name="data[calendar_period_max]" value="{$data.calendar_period_max|escape}" class="middle"> {ld_option i='price_period_unit' gid='listings' option=$data.price_period}
							</div>
						</div>*}
						<div class="b">
							<input type="submit" name="btn_save" value="{l i='btn_save' gid='start' type='button'}">
						</div>
						<input type="hidden" name="save_btn" value="1">
					</form>
				</div>
			</div>
			<div class="rollup-box">
				<div class="title">
					<div class="btn-link"><ins class="fa {if $step ne 2}fa-plus{else}fa-minus{/if} fa-lg edge hover"></ins></div>
					<h2>{l i='header_calendar_period_management' gid='listings'}</h2>
				</div>
				<div class="cont {if $step ne 2}hide{/if}">
					<div class="view-section">{block name='listings_calendar_block' module='listings' listing=$data template='edit' count=3}</div>
				</div>
			</div>
			{* Seo section *}		
			{elseif $section_gid eq 'seo'}
			{depends module=seo}
			{foreach item=section key=key from=$seo_fields}
			<div class="rollup-box">
				<div class="title">
					<div class="btn-link"><ins class="fa {if $step ne $key+1}fa-plus{else}fa-minus{/if} fa-lg edge hover"></ins></div>
					<h2>{$section.name}</h2>
				</div>
				<div class="cont {if $step ne $key+1}hide{/if}">
					<div class="view-section">
						<form method="post" action="{$data.action|escape}" name="seo_{$section.gid}_form">
							{foreach item=field from=$section.fields}
							<div class="r">
								<div class="f">{$field.name}: </div>
								<div class="v">
									{assign var='field_gid' value=$field.gid}
									{switch from=$field.type}
										{case value='checkbox'}
											<input type="hidden" name="{$field_gid}" value="0">
											<input type="checkbox" name="{$section.gid}[{$field_gid}]" value="1" {if $seo_settings[$section_gid][$field_gid]}checked{/if}>
										{case value='text'}
											{foreach item=lang_item key=lang_id from=$languages}
											{assign var='section_gid' value=$section.gid+'_'+$lang_id}
											<input type="{if $lang_id eq $current_lang_id}text{else}hidden{/if}" name="{$section.gid}[{$field_gid}][{$lang_id}]" value="{$seo_settings[$section_gid][$field_gid]|escape}" class="long" lang-editor="value" lang-editor-type="{$section.gid}_{$field_gid}" lang-editor-lid="{$lang_id}">
											{/foreach}
											<a href="#" lang-editor="button" lang-editor-type="{$section.gid}_{$field_gid}">A</a>
										{case value='textarea'}
											{foreach item=lang_item key=lang_id from=$languages}
											{assign var='section_gid' value=$section.gid+'_'+$lang_id}
											{if $lang_id eq $current_lang_id}
											<textarea name="{$section.gid}[{$field_gid}][{$lang_id}]" rows="5" cols="80" class="long" lang-editor="value" lang-editor-type="{$section.gid}_{$field_gid}" lang-editor-lid="{$lang_id}">{$seo_settings[$section_gid][$field_gid]|escape}</textarea>
											{else}
											<input type="hidden" name="{$section.gid}[{$field_gid}][{$lang_id}]" value="{$seo_settings[$section_gid][$field_gid][$lang_id]|escape}">
											{/if}
											{/foreach}
											<a href="#" lang-editor="button" lang-editor-type="{$section.gid}_{$field.gid}">A</a>
									{/switch}
								</div>
							</div>
							{/foreach}
							<div class="b">
								<input type="submit" name="btn_save_{$section.gid}" value="{l i='btn_save' gid='start' type='button'}">
							</div>
							<input type="hidden" name="save_btn" value="1">
						</form>
					</div>
				</div>
			</div>
			{/foreach}
			{block name=lang_inline_editor module=start}
			{/depends}
			{/if}

			<div class="b outside">
				<input type="button" name="btn_previous" value="{l i='nav_prev' gid='start' type='button'}" class="btn hide" id="edit_listings_prev" />
				<input type="button" name="btn_next" value="{l i='nav_next' gid='start' type='button'}" class="btn hide" id="edit_listings_next" />
				<a href="{$site_url}listings/my" class="btn-link"><ins class="fa fa-arrow-left fa-lg edge hover"></ins>{l i='link_back_to_my_listings' gid='listings'}</a>
			</div>
			<div class="clr"></div>
		</div>
	</div>
<div class="clr"></div>
 <script>{literal}
	    $(function (){
       $('.target').bind('keypress', function(e) {
       if ((e.keyCode || e.which) == 13) {
        e.preventDefault();
        $(this).val($(this).val()+'\n');
        return false;
    }
});
 {/literal}</script>
{include file="footer.tpl"}

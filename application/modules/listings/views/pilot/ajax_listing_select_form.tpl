<div class="load_content_controller">
	<h1>{l i='header_listing_select' gid='listings'}</h1>
	<div class="inside">	
		{if $select_data.max_select ne 1}
		<b>{l i='header_listings_selected' gid='listings'}:</b><br>
		<ul id="listing_selected_items" class="listing-items-selected">
		{foreach item=item from=$select_data.selected}
		<li><div class="listing-block"><input type="checkbox" name="remove_listings[]" value="{$item.id}" checked>{$item.output_name}</div></li>
		{/foreach}
		</ul>
		<div class="clr"></div><br>
		{/if}
		<b>{l i='header_listing_find' gid='listings'}:</b><br>
		<input type="text" id="listing_search" class="controller-search">
		<ul class="controller-items" id="listing_select_items"></ul>
	
		<div class="controller-actions">
			<div id="listing_page" class="fright"></div>
			<div><a href="#" id="listing_close_link">{l i='btn_close' gid='start'}</a></div>
		</div>
	</div>
</div>

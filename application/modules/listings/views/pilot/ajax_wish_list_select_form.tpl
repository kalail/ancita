<div class="load_content_controller">
	<h1>{l i='header_wish_list_select' gid='listings'}</h1>
	<div class="inside">	
		{if $select_data.max_select ne 1}
		<b>{l i='header_wish_lists_selected' gid='listings'}:</b><br>
		<ul id="wish_list_selected_items" class="wish-list-items-selected">
		{foreach item=item from=$select_data.selected}
		<li><div class="wish-list-block"><input type="checkbox" name="remove_wish_lists[]" value="{$item.id}" checked>{$item.output_name}</div></li>
		{/foreach}
		</ul>
		<div class="clr"></div><br>
		{/if}
		<b>{l i='header_wish_list_find' gid='listings'}:</b><br>
		<input type="text" id="wish_list_search" class="controller-search">
		<ul class="controller-items" id="wish_list_select_items"></ul>
	
		<div class="controller-actions">
			<div id="wish_list_page" class="fright"></div>
			<div><a href="#" id="wish_list_close_link">{l i='btn_close' gid='start'}</a></div>
		</div>
	</div>
</div>

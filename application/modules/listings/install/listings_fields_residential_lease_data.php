<?php

$fe_sections = array(
	array("data" => array( "gid" => "comment_10", "editor_type_gid" => "residential_lease", 'sorter' => 1)),
);

$fe_fields = array(
	array("data" => array( "gid" => "comments_10", "section_gid" => "comment_10", "editor_type_gid" => "residential_lease", "field_type" => "textarea", "fts" => "1", "settings_data" => '', "sorter" => "1", "options" => '')),	
);

$fe_forms = array(
	array("data" => array( "gid" => "main_search_form_10", "editor_type_gid" => "residential_lease", "name" => "Index search form", "field_data" => '')),
	array("data" => array( "gid" => "quick_search_form_10", "editor_type_gid" => "residential_lease", "name" => "Quick search form", "field_data" => '')),
	array("data" => array( "gid" => "advanced_search_10", "editor_type_gid" => "residential_lease", "name" => "Advanced search form", "field_data" => '')),
	array("data" => array( "gid" => "admin_export_form_10", "editor_type_gid" => "residential_lease", "name" => "Admin export form", "field_data" => '')),
	array("data" => array( "gid" => "user_export_form_10", "editor_type_gid" => "residential_lease", "name" => "User export form", "field_data" => '')),	
);

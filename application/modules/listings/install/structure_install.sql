DROP TABLE IF EXISTS `[prefix]listings`;
CREATE TABLE IF NOT EXISTS `[prefix]listings` (
  `id` bigint(11) NOT NULL auto_increment,
  `gid` varchar(255) NOT NULL,
  `id_user` int(3) NOT NULL,
  `id_type` tinyint(1) NOT NULL,
  `user_type` enum('private','company', 'agent') NOT NULL DEFAULT 'private',
  `id_category` smallint(3) NOT NULL,
  `property_type` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL,  
  `sold` tinyint(1) NOT NULL,
  `date_open` datetime NOT NULL,
  `date_open_begin` tinyint(4) NOT NULL,
  `date_open_end` tinyint(4) NOT NULL,
  `date_available` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_activity` datetime NOT NULL,
  `date_expire` datetime NOT NULL,
  `price` decimal(13,4) NOT NULL,
  `price_old` decimal(13,4) NOT NULL,
  `price_max` decimal(13,4) NOT NULL,
  `price_week` decimal(13,4) NOT NULL,
  `price_month` decimal(13,4) NOT NULL,
  `price_reduced` decimal(13, 4) NOT NULL,
  `price_sorting` decimal(13,4) NOT NULL,
  `price_max_sorting` decimal(13,4) NOT NULL,
  `price_period` smallint(3) NOT NULL,
  `price_type` smallint(3) NOT NULL,
  `gid_currency` varchar(10) NOT NULL,
  `price_negotiated` tinyint(1) NOT NULL,
  `price_auction` tinyint(1) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zip`	varchar(20) NOT NULL,
  `id_country` char(2) NOT NULL,      
  `id_region` int(3) NOT NULL,
  `id_city` int(3) NOT NULL,
  `id_district` int(3) NOT NULL,
  `lat` decimal(11,7) NOT NULL,
  `lon` decimal(11,7) NOT NULL,
  `headline` text NULL,
  `square` int(11) NOT NULL,
  `square_max` int(11) NOT NULL,
  `square_unit` varchar(10) NOT NULL,
  `listing_file` varchar(255) NOT NULL,
  `listing_file_name` varchar(255) NOT NULL,
  `listing_file_comment` text NULL,
  `listing_file_date` datetime NOT NULL,
  `listing_video` varchar(255) NOT NULL,
  `listing_video_image` varchar(255) NOT NULL,
  `listing_video_data` text NOT NULL,
  `initial_moderation` tinyint(3) NOT NULL,
  `initial_activity` int(3) NOT NULL,
  `logo_image` varchar(255) NOT NULL, 
  `slider_image` varchar(255) NOT NULL,
  `id_wish_lists` text NULL,
  `photo_count` int(3) NOT NULL,
  `is_vtour` tinyint(1) NOT NULL,
  `use_calendar` tinyint(1) NOT NULL,
  `calendar_period_min` smallint(3) NOT NULL,
  `calendar_period_max` smallint(3) NOT NULL,
  `featured_date_end` datetime NOT NULL,
  `lift_up_date_end` datetime NOT NULL,
  `lift_up_country_date_end` datetime NOT NULL,
  `lift_up_region_date_end` datetime NOT NULL,
  `lift_up_city_date_end` datetime NOT NULL,
  `highlight_date_end` datetime NOT NULL,
  `slide_show_date_end` datetime NOT NULL,
  `banned` tinyint(1) NOT NULL,
  `views` int(3) NOT NULL,
  `set_to_subscribe` tinyint(3) NOT NULL,
  `is_map_panorama` tinyint(1) NOT NULL,
  `id_seo_settings` int(3) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_category` (`id_category`, `property_type`),
  KEY `price` (`price_sorting`, `price_max_sorting`),
  KEY `square` (`square`, `square_max`),
  KEY `views` (`views`),
  KEY `date_created` (`date_created`),
  KEY `date_modified` (`date_modified`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]listings_residential`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_residential` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_listing` int(3) NOT NULL,
  `search_field` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_listing` (`id_listing`),
  FULLTEXT `search_field` (`search_field`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]listings_commercial`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_commercial` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_listing` int(3) NOT NULL,
  `search_field` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_listing` (`id_listing`),
  FULLTEXT `search_field` (`search_field`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]listings_lot_and_land`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_lot_and_land` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_listing` int(3) NOT NULL,
  `search_field` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_listing` (`id_listing`),
  FULLTEXT `search_field` (`search_field`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]listings_moderation`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_moderation` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_listing` int(3) NOT NULL,
  `id_user` int(3) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `headline` text NULL,
  `listing_file` varchar(255) NOT NULL,
  `listing_file_date` datetime NOT NULL,
  `listing_file_name` varchar(255) NOT NULL,
  `listing_file_comment` text NULL,
  `listing_video` varchar(255) NOT NULL,
  `listing_video_image` varchar(255) NOT NULL,
  `listing_video_data` text NOT NULL,
  `admin_alert` text NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_listing` (`id_listing`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]listings_residential_moderation`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_residential_moderation` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_listing` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_listing` (`id_listing`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]listings_commercial_moderation`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_commercial_moderation` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_listing` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_listing` (`id_listing`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]listings_lot_and_land_moderation`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_lot_and_land_moderation` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_listing` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_listing` (`id_listing`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]listings_moderation_alerts`;
CREATE TABLE `[prefix]listings_moderation_alerts` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_listing` INT(3) NOT NULL ,
  `id_user` INT(3) NOT NULL ,
  `content` TEXT NOT NULL ,
  `status` enum('approve','decline') NOT NULL DEFAULT 'approve',
  PRIMARY KEY (`id`) ,
  KEY `id_listing` (`id_listing`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]listings_searches`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_searches` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) NOT NULL,
  `search_data` text NOT NULL,
  `date_search` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]listings_comparison_list`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_comparison_list` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) NOT NULL,
  `id_listing` int(3) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[prefix]listings_wish_lists`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_wish_lists` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `logo_image` varchar(255) NOT NULL,
  `listings_count` smallint(3) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `[prefix]listings_wish_lists` VALUES(1, 0, 1, '32eb2ed989.jpg', 19, '2013-04-24 09:45:03', '2013-04-24 11:56:47');
INSERT INTO `[prefix]listings_wish_lists` VALUES(2, 0, 1, 'e278ed4b12.jpg', 4, '2013-04-24 09:45:12', '2013-04-24 11:57:33');
INSERT INTO `[prefix]listings_wish_lists` VALUES(3, 0, 1, 'e2a9c22328.jpg', 4, '2013-04-24 09:45:23', '2013-04-24 11:58:41');
INSERT INTO `[prefix]listings_wish_lists` VALUES(4, 0, 1, 'a74e317d91.jpg', 5, '2013-04-24 09:45:36', '2013-04-24 11:59:42');

DROP TABLE IF EXISTS `[prefix]listings_wish_lists_items`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_wish_lists_items` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_wish_list` int(3) NOT NULL,
  `id_listing` int(3) NOT NULL,
  `sorter` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_listing` (`id_wish_list`, `id_listing`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `[prefix]listings_wish_lists_items` VALUES(1, 1, 1, 1, '2013-04-24 11:49:33');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(2, 1, 2, 2, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(3, 1, 4, 4, '2013-04-24 11:52:48');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(4, 1, 5, 5, '2013-04-24 11:53:30');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(5, 1, 6, 6, '2013-04-24 11:53:41');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(6, 1, 7, 7, '2013-04-24 11:54:11');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(7, 1, 8, 8, '2013-04-24 11:54:22');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(8, 1, 9, 9, '2013-04-24 11:54:34');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(9, 1, 10, 10, '2013-04-24 11:54:45');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(10, 1, 11, 11, '2013-04-24 11:54:56');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(11, 1, 12, 12, '2013-04-24 11:55:06');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(12, 1, 13, 13, '2013-04-24 11:55:15');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(13, 1, 14, 14, '2013-04-24 11:55:24');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(14, 1, 15, 15, '2013-04-24 11:55:35');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(15, 1, 16, 16, '2013-04-24 11:56:00');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(16, 1, 17, 17, '2013-04-24 11:56:10');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(17, 1, 18, 18, '2013-04-24 11:56:25');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(18, 1, 19, 19, '2013-04-24 11:56:36');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(19, 1, 20, 20, '2013-04-24 11:56:47');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(20, 2, 4, 1, '2013-04-24 11:56:59');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(21, 2, 8, 2, '2013-04-24 11:57:13');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(22, 2, 12, 3, '2013-04-24 11:57:23');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(23, 2, 16, 4, '2013-04-24 11:57:33');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(24, 3, 7, 1, '2013-04-24 11:57:48');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(25, 3, 14, 2, '2013-04-24 11:58:08');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(26, 3, 22, 3, '2013-04-24 11:58:26');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(27, 3, 27, 4, '2013-04-24 11:58:41');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(28, 4, 5, 1, '2013-04-24 11:58:59');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(29, 4, 10, 2, '2013-04-24 11:59:11');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(30, 4, 15, 3, '2013-04-24 11:59:22');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(31, 4, 20, 4, '2013-04-24 11:59:32');
INSERT INTO `[prefix]listings_wish_lists_items` VALUES(32, 4, 25, 5, '2013-04-24 11:59:42');

DROP TABLE IF EXISTS `[prefix]listings_stats`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_stats` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) NOT NULL,
  `gid` varchar(40) NOT NULL,
  `sale_cnt` int(3) NOT NULL,
  `buy_cnt` int(3) NOT NULL,
  `rent_cnt` int(3) NOT NULL,
  `lease_cnt` int(3) NOT NULL,
  `sold_cnt` int(30) NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`, `gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `[prefix]listings_stats` VALUES(1, 0, 'property_type_1', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(2, 0, 'property_type_1_1', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(3, 0, 'property_type_1_2', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(4, 0, 'property_type_1_3', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(5, 0, 'property_type_1_4', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(6, 0, 'property_type_1_5', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(7, 0, 'property_type_1_6', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(8, 0, 'property_type_1_7', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(9, 0, 'property_type_1_8', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(10, 0, 'property_type_1_9', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(11, 0, 'property_type_2', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(12, 0, 'property_type_2_1', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(13, 0, 'property_type_2_2', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(14, 0, 'property_type_2_3', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(15, 0, 'property_type_2_4', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(16, 0, 'property_type_2_5', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(17, 0, 'property_type_2_6', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(18, 0, 'property_type_2_7', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(19, 0, 'property_type_2_8', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(20, 0, 'property_type_2_9', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(21, 0, 'property_type_3', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(22, 0, 'property_type_3_1', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(23, 0, 'property_type_3_2', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(24, 0, 'property_type_3_3', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(25, 0, 'property_type_3_4', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');
INSERT INTO `[prefix]listings_stats` VALUES(26, 0, 'property_type_3_5', 0, 0, 0, 0, 0, '2013-04-24 11:49:54');

DROP TABLE IF EXISTS  `[prefix]listing_profile_visitors`;
CREATE TABLE IF NOT EXISTS `[prefix]listing_profile_visitors` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_listing` int(3) NOT NULL,
  `id_user` int(3) NOT NULL,
  `id_visitor` int(3) NOT NULL,
  `last_visit_date` datetime NOT NULL,
  `visits_count` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_listing` (`id_listing`),
  KEY `id_user` (`id_listing`, `id_user`),
  KEY `id_visiter` (`id_listing`, `id_visitor`),
  UNIQUE `listing_visiter` (`id_listing`, `id_user`, `id_visitor`, `last_visit_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS  `[prefix]listings_rss_subscriptions`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_rss_subscriptions` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `gid` varchar(32) NOT NULL,
  `id_user` int(3) NOT NULL,
  `data` text NULL,
  `email` varchar(128) NOT NULL,
  `last_use` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS  `[prefix]listings_booking`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_booking` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_listing` int(3) NOT NULL,
  `id_owner` int(3) NOT NULL,
  `id_user` int(3) NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `guests` tinyint(3) NOT NULL,
  `price` decimal(13,4) NOT NULL,
  `status` enum('open', 'close', 'book', 'wait', 'decline'),
  `comment` text NULL,
  `answer` text NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_listing` (`id_listing`),
  KEY `id_owner` (`id_listing`, `id_owner`),
  KEY `id_user` (`id_listing`, `id_user`),
  KEY `status` (`id_listing`, `status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `pg_listings_booking` VALUES (NULL, 75, 14, 1, '2018-04-01 00:00:00', '2018-04-01 00:00:00', 1, '150.0000', 'book', 'Хочу забронировать Вашу недвижимость', NULL, '2014-04-16 08:21:56', '2014-04-16 08:21:56');
INSERT INTO `pg_listings_booking` VALUES (NULL, 66, 4, 1, '2016-03-16 00:00:00', '2016-03-22 00:00:00', 6, '259000.0000', 'decline', '', NULL, '2014-04-16 08:24:32', '2014-04-16 08:24:32');
INSERT INTO `pg_listings_booking` VALUES (NULL, 67, 4, 1, '2016-01-20 00:00:00', '2016-01-25 00:00:00', 1, '240.0000', 'wait', '', NULL, '2014-04-16 08:25:14', '2014-04-16 08:25:14');
INSERT INTO `pg_listings_booking` VALUES (NULL, 52, 1, 10, '2018-04-01 00:00:00', '2018-04-01 00:00:00', 5, '130.0000', 'book', 'Hot water?', NULL, '2014-04-16 08:33:26', '2014-04-16 08:33:26');
INSERT INTO `pg_listings_booking` VALUES (NULL, 55, 1, 2, '2017-07-11 00:00:00', '2017-07-20 00:00:00', 2, '600.0000', 'decline', 'Mother and child', NULL, '2014-04-16 08:35:44', '2014-04-16 08:35:44');
INSERT INTO `pg_listings_booking` VALUES (NULL, 54, 1, 5, '2015-12-20 00:00:00', '2015-12-23 00:00:00', 1, '120.0000', 'wait', '', NULL, '2014-04-16 08:37:11', '2014-04-16 08:37:11');

DROP TABLE IF EXISTS  `[prefix]listings_booking_cahce`;
CREATE TABLE IF NOT EXISTS `[prefix]listings_booking_cache` (
  `hash` varchar(32) NOT NULL,
  `data` text NULL,
  `expire` int(3),
  KEY `hash` (`hash`, `expire`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

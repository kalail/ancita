<?php

$fe_sections = array(
	array("data" => array( "gid" => "comment_12", "editor_type_gid" => "lot_and_land_lease", 'sorter' => 1)),
);

$fe_fields = array(
	array("data" => array( "gid" => "comments_12", "section_gid" => "comment_12", "editor_type_gid" => "lot_and_land_lease", "field_type" => "textarea", "fts" => "1", "settings_data" => '', "sorter" => "1", "options" => '')),
);

$fe_forms = array(
	array("data" => array( "gid" => "main_search_form_12", "editor_type_gid" => "lot_and_land_lease", "name" => "Index search form", "field_data" => '')),
	array("data" => array( "gid" => "quick_search_form_12", "editor_type_gid" => "lot_and_land_lease", "name" => "Quick search form", "field_data" => '')),
	array("data" => array( "gid" => "advanced_search_12", "editor_type_gid" => "lot_and_land_lease", "name" => "Advanced search form", "field_data" => '')),
	array("data" => array( "gid" => "admin_export_form_12", "editor_type_gid" => "lot_and_land_lease", "name" => "Admin export form", "field_data" => '')),
	array("data" => array( "gid" => "user_export_form_12", "editor_type_gid" => "lot_and_land_lease", "name" => "User export form", "field_data" => '')),
);	

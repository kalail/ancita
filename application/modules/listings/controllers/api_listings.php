<?php

/**
 * Listings api side controller
 *
 * @package PG_RealEstate
 * @subpackage Listings
 * @category	controllers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Mikhail Chernov <mchernov@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: mchernov $
 **/
class Api_Listings extends Controller{
	
	/**
	 * Constructor
	 *
	 * @return Api_Listings
	 */
	public function __construct(){
		parent::Controller();
		$this->load->model('Listings_model');
	}
	
	/**
	 * Get listing data
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function get($listing_id){
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->load->model('Users_model');
		
		$this->Users_model->set_format_settings('get_contact', true);
		
		$this->Listings_model->set_format_settings(array('get_description'=>true, 'get_booking'=>true));
		$listing = $this->Listings_model->get_listing_by_id($listing_id);		
		$this->Listings_model->set_format_settings(array('get_description'=> false, 'get_booking'=>false));
		
		$this->Users_model->set_format_settings('get_contact', false);
		
		if(empty($listing) || !$listing['status']){
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$site_url = site_url();
		$methods = array(
			'photos' => $site_url.'api/listings/get_photos/'.$listing_id,
			'virtual_tours' => $site_url.'api/listings/get_virtual_tours/'.$listing_id,
			'file' => $site_url.'api/listings/get_file/'.$listing_id,
			'video' => $site_url.'api/listings/get_video/'.$listing_id,
			'pdf' => $site_url.'listings/view/'.$listing_id.'/overview/1',
		);
		
		$date_formats = $this->pg_date->get_format('date_literal', 'st');
		
		$data = array(
			'listing_id' => $listing_id,
			'logo_image' => $listing['media']['photo']['thumbs'],
			'listing_file' => ($listing['is_file'] ? $listing['listing_file_content'] : ''),
			'listing_video' => ($listing['is_video'] ? $listing['listing_video_content'] : ''),
			'methods' => $methods,
			'date_format' => $date_formats,
		);
		
		unset($listing['id']);
		unset($listing['logo_image']);
		unset($listing['media']);
		unset($listing['listing_file']); 
		unset($listing['listing_file_date']);
		unset($listing['listing_video']);
		unset($listing['listing_video_image']);
		unset($listing['listing_video_data']);
		
		foreach($listing as $name=>$value){
			$data['listing_'.$name] = $value;
		}
		
		$this->load->helper('date_format');
		
		$data['listing_user']['logo_image'] = $data['listing_user']['media']['user_logo']['thumbs'];
		$data['listing_date_activity_output'] = tpl_date_format($data['listing_date_activity'], $date_formats);
		$data['listing_date_available_output'] = tpl_date_format($data['listing_date_available'], $date_formats);
		$data['listing_date_activity_output'] = tpl_date_format($data['listing_date_activity'], $date_formats);
		$data['listing_date_created_output'] = tpl_date_format($data['listing_date_created'], $date_formats);
		$data['listing_date_expire_output'] = tpl_date_format($data['listing_date_expire'], $date_formats);
		$data['listing_date_modified_output'] = tpl_date_format($data['listing_date_modified'], $date_formats);
		$data['listing_date_open_output'] = tpl_date_format($data['listing_date_open'], $date_formats);
		$data['listing_featured_date_end_output'] = tpl_date_format($data['listing_featured_date_end'], $date_formats);
		$data['listing_highlight_date_end_output'] = tpl_date_format($data['listing_highlight_date_end'], $date_formats);
		$data['listing_lift_up_city_date_end_output'] = tpl_date_format($data['listing_lift_up_city_date_end'], $date_formats);
		$data['listing_lift_up_country_date_end_output'] = tpl_date_format($data['listing_lift_up_country_date_end'], $date_formats);
		$data['listing_lift_up_date_end_output'] = tpl_date_format($data['listing_lift_up_date_end'], $date_formats);
		$data['listing_lift_up_region_date_end_output'] = tpl_date_format($data['listing_lift_up_region_date_end'], $date_formats);
		$data['listing_slide_show_date_end_output'] = tpl_date_format($data['listing_slide_show_date_end'], $date_formats);
		
		if($this->session->userdata('auth_type') == 'user' && $this->session->userdata('user_id') == $data['listing_id_user']){
			$data['is_owner'] = 1;
		}else{
			$data['is_owner'] = 0;
		}
		
		$this->load->helper('start');
		
		foreach($data['listing_booking']['periods'] as $i=>$period){
			$period['price_str'] = str_replace('&nbsp;', ' - ', strip_tags(currency_format_output(array('value'=>$period['price'], 'gid'=>$data['listing_gid_currency']))));
			$data['listing_booking']['periods'][$i] = $period;
		}
		
		$this->Listings_model->check_views_count($user_id, $listing_id);

		$this->set_api_content('data', $data);
	}
		
	/**
	 * Get listing photos
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function get_photos($listing_id){
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$format_settings = array(
			'get_user' => false,
			'get_location' => false, 
			'get_photos' => true, 
		);
		$this->Listings_model->set_format_settings($format_settings);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);	
		$format_settings = array(
			'get_user' => true,
			'get_location' => true, 
			'get_photos' => false,
		);
		$this->Listings_model->set_format_settings($format_settings);
			
		if(empty($listing)){
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
			return false;
		}
		
		$data = array('photos'=>array());
		
		if(empty($listing['photos'])){
			$data['photo_default'] = array(
				'source' => $listing['photo_default']['media']['url'],
				'big' => $listing['photo_default']['media']['thumbs']['620_400'],
				'middle' => $listing['photo_default']['media']['thumbs']['200_200'],
				'small' => $listing['photo_default']['media']['thumbs']['60_60'],
			);
		}else{
			foreach($listing['photos'] as $photo){
				if(!$photo['status']) continue;
				$data['photos'][] = array(
					'source' => $photo['media']['url'],
					'big' => $photo['media']['thumbs']['620_400'],
					'middle' => $photo['media']['thumbs']['200_200'],
					'small' => $photo['media']['thumbs']['60_60'],
					'comment' => $photo['comment'],
				);
			}
		}
		
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Get listing virtual tours
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function get_virtual_tours($listing_id){
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$format_settings = array(
			'get_user' => false,
			'get_location' => false, 
			'get_virtual_tours' => true,
		);
		$this->Listings_model->set_format_settings($format_settings);
		
		$listing = $this->Listings_model->get_listing_by_id($listing_id);		
		if(empty($listing)){
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
			return false;
		}
		
		$format_settings = array(
			'get_user' => true,
			'get_location' => true, 
			'get_virtual_tours' => false,
		);
		$this->Listings_model->set_format_settings($format_settings);
		
		$data = array('virtual_tours'=>array(), 'max_panorama'=>$listing['max_panorama']);
		foreach($listing['virtual_tour'] as $panorama){
			if(!$panorama['status']) continue;
			$data['virtual_tours'][] = array(
				'source' => $panorama['media']['url'],
				'big' => $panorama['media']['thumbs']['big'],
				'middle' => $panorama['media']['thumbs']['middle'],
				'small' => $panorama['media']['thumbs']['small'],
				'comment' => $panorama['comment'],
			);
		}
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Set listing data
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function set($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		if($listing_id){
			$this->Listings_model->set_format_settings('use_format', false);
			$data = $this->Listings_model->get_listing_by_id($listing_id);
			$this->Listings_model->set_format_settings('use_format', true);
	
			if(!$data || $user_id != $data['id_user']){
				log_message('error', 'listings API: Invalid listing id');
				$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
				return false;
			}
		}else{
			$data = array();
		}
		
		$post_data = $this->input->post('data', true);
		$post_data['id_country'] = $this->input->post('id_country', true);
		$post_data['id_region'] = $this->input->post('id_region', true);
		$post_data['id_city'] = $this->input->post('id_city', true);	
		$post_data['id_district'] = $this->input->post('id_district', true);	
		$post_data['id_user'] = $user_id;
			
		$property_type_gid = $this->Listings_model->get_field_editor_type($data);
		if($property_type_gid){
			$fields_for_select = $this->Listings_model->get_fields_for_select($property_type_gid);
			foreach($fields_for_select as $field){
				$post_data[$field] = $this->input->post($field, true);
			}
		}
			
		$validate_data = $this->Listings_model->validate_listing($listing_id, $post_data);
		if(!empty($validate_data['errors'])){
			$this->set_api_content('errors', $validate_data['errors']);
			return false;
		}elseif(empty($validate_data['data'])){
			$this->set_api_content('errors', l('error_empty_listing_data', 'listings'));
		}else{
			if($listing_id){
				$this->Listings_model->save_listing($listing_id, $validate_data['data'], true);
				$this->set_api_content('messages', l('success_update_listing', 'listings'));
			}else{
				$listing_id = $this->Listings_model->save_listing(null, $validate_data['data'], true);
				$this->set_api_content('messages', l('success_add_listing', 'listings'));
			}
		}
	}
	
	/**
	 * Create listing
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function create(){
		$this->set(0);
	}
	
	/**
	 * Remove listing
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function delete($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$this->Listings_model->set_format_settings('use_format', false);
		$data = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
	
		if(!$data){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		if($user_id != $data['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}

		$this->Listings_model->delete_listing($listing_id);
		$this->set_api_content('messages', l('success_listings_delete', 'listings'));
	}
	
	/**
	 * Upload photo to listing gallery
	 * 
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 * @return void
	 */
	public function photo_upload($listing_id, $photo_id=0){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}

		$photo_id = intval($photo_id);
		if($photo_id){
			$this->load->model('Upload_gallery_model');
			$photo = $this->Upload_gallery_model->get_file_by_id($photo_id);
			if($listing_id != $photo['object_id']){
				log_message('error', 'listings API: Invalid listing id');
				$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
				return false;
			}
		}

		$return = $this->Listings_model->save_photo($listing_id, $photo_id, 'photo_file');
		if(!empty($return['errors'])){
			$this->set_api_content('errors', implode('<br>', $return['errors']));
		}else{
			$this->set_api_content('messages', l('success_photo_uploaded', 'listings'));
		}
	}
	
	/**
	 * Remove photo from listing gallery
	 * 
	 * @param integer $listing_id listing identifier
	 * @param integer $photo_id photo identifier
	 * @return void
	 */
	public function photo_delete($listing_id, $photo_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$photo_id = intval($photo_id);
		if(!$photo_id){
			log_message('error', 'listings API: Empty photo id');
			$this->set_api_content('errors', l('api_error_empty_photo_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}

		// remove photo
		$result = $this->Listings_model->delete_photo($listing_id, $photo_id);
		if(!$result){
			$this->set_api_content('errors', l('error_photo_deleted', 'listings'));
		}else{
			$this->set_api_content('messages', l('success_photo_deleted', 'listings'));
		}
	}
	
	/**
	 * Upload panorama to listing gallery
	 * 
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 * @return void
	 */
	public function panorama_upload($listing_id, $panorama_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}

		$panorama_id = intval($panorama_id);
		
		$this->load->model('Upload_gallery_model');
		if($panorama_id){
			$panorama = $this->Upload_gallery_model->get_file_by_id($panorama_id);
			if($listing_id != $photo['object_id']){
				log_message('error', 'listings API: Invalid listing id');
				$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
				return false;
			}
		}

		$return = $this->Listings_model->save_panorama($listing_id, $panorama_id, 'panorama_file');
		if(!empty($return['errors'])){
			$this->set_api_content('errors', implode('<br>', $return['errors']));
		}else{
			$this->set_api_content('messages', l('success_panorama_uploaded', 'listings'));
		}
	}
	
	/**
	 * Remove panorama from listing gallery
	 * 
	 * @param integer $listing_id listing identifier
	 * @param integer $panorama_id panorama identifier
	 * @return void
	 */
	public function panorama_delete($listing_id, $panorama_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$panorama_id = intval($panorama_id);
		if(!$panorama_id){
			log_message('error', 'listings API: Empty panorama id');
			$this->set_api_content('errors', l('api_error_empty_panorama_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}

		// remove panorama
		$result = $this->Listings_model->delete_photo($listing_id, $panorama_id);
		if(!$result){
			$this->set_api_content('errors', l('error_panorama_deleted', 'listings'));
		}else{
			$this->set_api_content('messages', l('success_panorama_deleted', 'listings'));
		}
	}
	
	/**
	 * Upload file to listing
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function file_upload($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}
		
		$post_data = $this->Listings_model->save_file($listing_id, 'listing_file', true);
		
		///// delete listing file
		if($listing['listing_file']){
			$this->load->model('File_uploads_model');
			$this->File_uploads_model->delete_upload($this->Listings_model->file_config_id, $listing['prefix'], $listing['listing_file']);
			if(!isset($post_data['data']['file'])) $post_data['data']['listing_file'] = '';
		}

		$data['listing_file'] = $post_data['data']['file'];
		$this->Listings_model->save_listing($listing_id, $data);
		
		$this->set_api_content('messages', l('success_file_uploaded', 'listings'));
	}
	
	/**
	 * Remove file from listing
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function file_delete($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}
		
		if(!$listing['listing_file']){
			log_message('error', 'listings API: Empty listing file');
			$this->set_api_content('errors', l('api_error_empty_file', 'listings'));
			return false;
		}
		
		$this->load->model('File_uploads_model');
		$this->File_uploads_model->delete_upload($this->Listings_model->file_config_id, $listing['prefix'], $listing['listing_file']);
		
		$data['listing_file'] = '';
		$this->Listings_model->save_listing($listing_id, $data);
		
		$this->set_api_content('messages', l('success_file_deleted', 'listings'));
	}
	
	/**
	 * Upload video to listing
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function video_upload($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}
	
		$post_data = $this->Listings_model->save_video($listing_id, 'listing_video', true);
		///// delete video
		if($data['listing_video']){
			$this->load->model('Video_uploads_model');
			$this->Video_uploads_model->delete_upload($this->Listings_model->video_config_id, $listing['prefix'], $listing['listing_video'], $listing['listing_video_image'], $listing['listing_video_data']['data']['upload_type']);
			if(!isset($post_data['data']['video'])){
				$post_data['data']['listing_video'] = '';
				$post_data['data']['listing_video_image'] = '';
				$post_data['data']['listing_video_data'] = '';
			}
		}			
		
		$data['listing_video'] = $post_data['data']['listing_video'];
		$data['listing_video_image'] = $post_data['data']['listing_video_image'];
		$data['listing_video_data'] = $post_data['data']['listing_video_data'];
		$this->Listings_model->save_listing($listing_id, $data);
		
		$this->set_api_content('messages', l('success_video_uploaded', 'listings'));
	}
	
	/**
	 * Remove video from listing
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function video_delete($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}
		
		if(!$listing['listing_video']){
			log_message('error', 'listings API: Empty listing video');
			$this->set_api_content('errors', l('api_error_empty_video', 'listings'));
			return false;
		}
		
		$this->load->model('Video_uploads_model');
		$this->Video_uploads_model->delete_upload($this->Listings_model->video_config_id, $listing['prefix'], $listing['listing_video'], $listing['listing_video_image'], $listing['listing_video_data']['data']['upload_type']);
		
		$data['listing_video'] = '';
		$data['listing_video_image'] = '';
		$data['listing_video_data'] = '';
		$this->Listings_model->save_listing($listing_id, $data);
			
		$this->set_api_content('messages', l('success_video_deleted', 'listings'));
	}
	
	/**
	 * Get my listings
	 *  
	 * @param string $type listing type
	 * @param integer $page page of results
	 * @param string $order sorting order 
	 * @param string $order_direction order direction
	 * @return void
	 */
	public function my($operation_type='sale', $page=1, $order='date_modified', $order_direction='DESC'){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$data['operation_type'] = $operation_type;
		if(empty($data['type'])) $data['operation_type'] = $this->Listings_model->get_operation_type_gid_default();
		
		$data['order'] = $order;
		if(!$data['order'])	$data['order'] = 'date_modified';
		
		$data['order_direction'] = $order_direction;
		if(!$data['order_direction']) $data['order_direction'] = 'DESC';
		
		$data['page'] = $page;
		if(empty($data['page'])) $data['page'] = 1;
			
		$filters = array('user'=> $user_id, 'operation_type'=>$data['operation_type']);		
			
		$data['listings'] = array();
			
		$data['listings_count'] = $this->Listings_model->get_listings_count($filters);
		if($data['listings_count'] > 0){
			$order_array = array();
			if($data['order'] == 'price'){
				$order_array['price_sorting'] = $data['order_direction'];
			}else{
				$order_array[$data['order']] = $data['order_direction'];
			}
			$this->Listings_model->set_format_settings(array('get_user'=>false, 'get_moderation'=>true));
			$listings = $this->Listings_model->get_listings_list($filters, $page, $items_on_page, $order_array);
			$this->Listings_model->set_format_settings(array('get_user'=>true, 'get_moderation'=>false));
			
			foreach($listings as $i=>$listing){
				$data['listings'][$i] = array(
					'listing_id' => $listing['id'],
					'logo_image' => $listing['media']['photo']['thumbs'],
				);
		
				unset($listing['id']);
				unset($listing['logo_image']);
				unset($listing['media']);
				unset($listing['listing_file']); 
				unset($listing['listing_file_date']);
				unset($listing['listing_video']);
				unset($listing['listing_video_image']);
				unset($listing['listing_video_data']);
		
				foreach($listing as $name=>$value){
					$data['listings'][$i]['listing_'.$name] = $value;
				}
			}
		}else{
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
		}
	
		$data['date_format'] = $this->pg_date->get_format('date_literal', 'st');
		$data['date_time_format'] = $this->pg_date->get_format('date_time_literal', 'st');

		if($this->pg_module->is_module_installed('services')) {
			$this->load->model('Services_model');
			$data['featured_service_active'] = $this->Services_model->is_service_active('listings_featured_service');
			$data['highlight_service_active'] = $this->Services_model->is_service_active('listings_highlight_service');
			$data['lift_up_service_active'] = $this->Services_model->is_service_active('listings_lift_up_service');
			$data['lift_up_country_service_active'] = $this->Services_model->is_service_active('listings_lift_up_country_service');
			$data['lift_up_region_service_active'] = $this->Services_model->is_service_active('listings_lift_up_region_service');
			$data['lift_up_city_service_active'] = $this->Services_model->is_service_active('listings_lift_up_city_service');
			$data['slide_show_service_active'] = $this->Services_model->is_service_active('listings_slide_show_service');
		}
	
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Search listings
	 *
	 * @param integer $page page of results
	 * @param string $order sorting order
	 * @param string $order_direction order direction asc/desc
	 * @return void
	 */
	public function search($page=1, $order='default', $order_direction='DESC'){
		$filters = array();
		
		$data = (array)$this->input->post('criteria', true);
	
		// category
		if(isset($data['category'])){
			$category_chunks = explode('_', $data['category']);
			$filters['id_category'] = $category_chunks[0];
			if(count($category_chunks) > 1 && $category_chunks[1]){
				$filters['property_type'] = $category_chunks[1];
			}
			unset($data['category']);
		}
		
		foreach($data as $key=>$value){
			if(!empty($value)){
				$filters[$key] = $value;
			}
		}

		$filters['active'] = '1';
		
		$data['order'] = trim(strip_tags($order));
		if(!$data['order']) $data['order'] = 'date_created';
		
		$data['order_direction'] = strtoupper(trim(strip_tags($data['order_direction'])));
		if($data['order_direction']!='DESC') $data['order_direction'] = 'ASC';

		$data['page'] = $page;
		if(!$data['page']) $data['page'] = 1;
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');

		$data['listings'] = array();
		$data['listings_count'] = $this->Listings_model->get_listings_count($filters);
		
		$use_leader = false;
		if($data['listings_count'] > 0){
			$order_array = array();

			if($order == 'default'){
				if(isset($filters['city']) && !empty($filters['city'])) $order_array['lift_up_city_date_end'] = 'DESC';
				if(isset($filters['region']) && !empty($filters['region'])) $order_array['lift_up_region_date_end'] = 'DESC';
				if(isset($filters['country']) && !empty($filters['country'])) $order_array['lift_up_country_date_end'] = 'DESC';
				$order_array['lift_up_date_end'] = 'DESC';
				$order_array['id'] = 'DESC'; 
				$use_leader = true;
			}else{
				$order_array[$data['order']] = $data['order_direction'];
			}
	
			$this->Listings_model->set_format_settings('get_user', false);
			$listings = $this->Listings_model->get_listings_list($filters, $data['page'], $items_on_page, $order_array);
			$this->Listings_model->set_format_settings('get_user', true);
			
			foreach($listings as $i=>$listing){
				$data['listings'][$i] = array(
					'listing_id' => $listing['id'],
					'logo_image' => $listing['media']['photo']['thumbs'],
				);
		
				unset($listing['id']);
				unset($listing['logo_image']);
				unset($listing['media']);
				unset($listing['listing_file']); 
				unset($listing['listing_file_date']);
				unset($listing['listing_video']);
				unset($listing['listing_video_image']);
				unset($listing['listing_video_data']);
		
				foreach($listing as $name=>$value){
					$data['listings'][$i]['listing_'.$name] = $value;
				}
			}
		}else{
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
		}

		$data['date_format'] = $this->pg_date->get_format('date_literal', 'st');
		$data['date_time_format'] = $this->pg_date->get_format('date_time_literal', 'st');
		
		$use_save_search = $this->session->userdata('auth_type') == 'user';
		$data['use_save_search'] = $use_save_search;
		
		$data['use_leader'] = $use_leader;
	
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Get saved listings
	 *
	 * @param integer $page page of results
	 * @param string $order sorting order
	 * @param string $order_direction order direction asc/desc
	 * @return void
	 */
	public function saved($page=1, $order='date_created', $order_direction='DESC'){
		
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');
		
		$filters = array('ids'=>array(), 'active'=>1);
		
		$this->load->model('listings/models/Saved_listings_model');
		$saved_listings = $this->Saved_listings_model->get_saved_list_by_user($user_id);
		foreach($saved_listings as $saved){
			$filters['ids'][] = $saved['id_listing'];
		}
		
		if(empty($filters['ids'])){
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
			return false;
		}
		
		$data['order'] = trim(strip_tags($order));
		if(!$data['order']) $data['order'] = 'date_created';
		
		$data['order_direction'] = strtoupper(trim(strip_tags($data['order_direction'])));
		if($data['order_direction']!='DESC') $data['order_direction'] = 'ASC';

		$data['page'] = $page;
		if(!$data['page']) $data['page'] = 1;
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
		
		$data['listings'] = array();
		$data['listings_count'] = $this->Listings_model->get_listings_count($filters);
		
		$use_leader = false;
		if($data['listings_count'] > 0){
			$order_array = array();

			if($order == 'default'){
				if(isset($data['id_city']) && !empty($data['id_city'])) $order_array['lift_up_city_date_end'] = 'DESC';
				if(isset($data['id_region']) && !empty($data['id_region'])) $order_array['lift_up_region_date_end'] = 'DESC';
				if(isset($data['id_country']) && !empty($data['id_country'])) $order_array['lift_up_country_date_end'] = 'DESC';
				$order_array['lift_up_date_end'] = 'DESC';
				$order_array['id'] = 'DESC';
				$use_leader = true;
			}else{
				$order_array[$data['order']] = $data['order_direction'];
			}
	
			$this->Listings_model->set_format_settings('get_user', false);
			$listings = $this->Listings_model->get_listings_list($filters, $data['page'], $items_on_page, $order_array);
			$this->Listings_model->set_format_settings('get_user', true);
			
			foreach($listings as $i=>$listing){
				$data['listings'][$i] = array(
					'listing_id' => $listing['id'],
					'logo_image' => $listing['media']['photo']['thumbs'],
				);
		
				unset($listing['id']);
				unset($listing['logo_image']);
				unset($listing['media']);
				unset($listing['listing_file']); 
				unset($listing['listing_file_date']);
				unset($listing['listing_video']);
				unset($listing['listing_video_image']);
				unset($listing['listing_video_data']);
		
				foreach($listing as $name=>$value){
					$data['listings'][$i]['listing_'.$name] = $value;
				}
			}
		}else{
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
		}

		$data['date_format'] = $this->pg_date->get_format('date_literal', 'st');
		$data['date_time_format'] = $this->pg_date->get_format('date_time_literal', 'st');
		
		$use_save_search = $this->session->userdata('auth_type') == 'user';
		$data['use_save_search'] = $use_save_search;
		
		$data['use_leader'] = $use_leader;
	
		$this->set_api_content('data', $data);
	}

	/**
	 * Add listing to saved
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function save($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');

		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);
		if(!$listing || $user_id != $listing['id_user']){
			log_message('error', 'listings API: Invalid listing id');
			$this->set_api_content('errors', l('api_error_invalid_listing_id', 'listings'));
			return false;
		}
		
		$this->load->model('listings/models/Saved_listings_model');
		
		$this->Saved_listings_model->get_saved_list_by_user($user_id);
		
		$listing_ids = array();
		foreach($saved_listings as $saved){
			$listing_ids = $saved['id_listing'];
		}
		if(in_array($listing_id, $listing_ids)){
			log_message('error', 'listings API: Listing already saved');
			$this->set_api_content('errors', l('api_error_already_saved', 'listings'));
			return false;
		}		
		
		$data['id_user'] = $user_id;
		$data['id_listing'] = $listing_id;
		$this->Saved_listings_model->save_saved(null, $data);
		
		$this->set_api_content('messages', l('success_listings_saved', 'listings'));
	}
	
	/**
	 * Remove listing from saved
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function delete_saved($listing_id){
		$auth_type = $this->session->userdata('auth_type');
		if('user' != $auth_type){
			log_message('error', 'listings API: Wrong auth type ("' . $auth_type . '")');
			$this->set_api_content('errors', l('api_error_wrong_auth_type', 'listings'));
			return false;
		}
		
		$user_id = $this->session->userdata('user_id');

		$listing_id = intval($listing_id);
		if(!$listing_id){
			log_message('error', 'listings API: Empty listing id');
			$this->set_api_content('errors', l('api_error_empty_listing_id', 'listings'));
			return false;
		}
		
		$this->load->model('listings/models/Saved_listings_model');
		$this->Saved_listings_model->delete_saved($user_id, $listing_id);
		
		$this->set_api_content('messages', l('success_delete_saved', 'listings'));
	}
	
	/**
	 * Calculate price of booking request
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function get_booking_price($listing_id){
		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		$date_start = $this->input->post('date_start', true);
		$date_end = $this->input->post('date_end', true);
		
		$guests = intval($this->input->post('guests', true));
		if(!$guests) $guests = 1;
		
		if(!$date_start || !$date_end){
			if($listing['price_negotiated']){
				$format_price_value = l('text_negotiated_price_rent', 'listings');
			}else{
				if($listing['price_reduced'] > 0){
					$price_value = $listing['price_reduced'];
				}else{
					$price_value = $listing['price'];	
				}
					
				$this->load->helper('start');
				$price_arr = array('value'=>$price_value, 'cur_gid'=>$listing['gid_currency']);	
				$format_price_value = l('text_price_from', 'listings').' '.currency_format_output($price_arr);
			}
		}else{
			$this->load->model('listings/models/Listings_booking_model');
			$price_value = $this->Listings_booking_model->get_period_price($listing, $date_start, $date_end, $guests);
			if($price_value){
				$this->load->helper('start');
				$price_arr = array('value'=>$price_value, 'cur_gid'=>$listing['gid_currency']);	
				$format_price_value = currency_format_output($price_arr);
			}else{
				$format_price_value = l('text_negotiated_price_rent', 'listings');
			}
		}
		
		$this->set_api_content('data', strip_tags($format_price_value));
	}
	
	/**
	 * Get bookings requests of my listings
	 * 
	 * @param string $status order status
	 * @param string $order sorting order
	 * @param string $order_direction order direction
	 * @param integer $page page of results
	 * @return void
	 */
	public function orders($status='approve', $order='created', $order_direction='DESC', $page=1){
		$current_settings = isset($_SESSION['listings_booking']) ? $_SESSION['listings_booking'] : array();
		if(!isset($current_settings['data'])) $current_settings['data'] = array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'created';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = '1';
		if(!isset($current_settings['filters'])) $current_settings['filters'] = array('status'=>'approve');
		
		$user_id = $this->session->userdata('user_id');
			
		$this->load->model('listings/models/Listings_booking_model');
		
		$data = $this->input->post('data', true);
		if(!empty($data)) $current_settings['filters'] = $data;
		if($status){
			if($status != $current_settings['filters']['status']){
				$page = 1;
			}
			$current_settings['filters']['status'] = $status;
		}
		$filters = $current_settings['filters'];
		//if(isset($filters['keyword'])) $filters['keyword_mode'] = 1;
		$filters['not_user'] = $user_id;
		$filters['owners'] = $user_id;
		
		$order = trim(strip_tags($order));
		if(!empty($order)) $current_settings['order'] = $order;
		$order = $current_settings['order'];
	
		$order_direction = strtoupper(trim(strip_tags($order_direction)));
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		
		$page = intval($page);
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		
		if($order_direction != 'ASC') $order_direction = 'DESC';
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
	
		$orders_count = $this->Listings_booking_model->get_periods_count($filters);
	
		$this->load->helper('sort_order');

		$current_settings['page'] = get_exists_page_number($page, $orders_count, $items_on_page);
	
		$_SESSION['listings_booking'] = $current_settings;
	
		$orders = array();
		if($orders_count > 0){
			$order_array = array();
			switch($order){
				case 'created':
					$order_array['date_created'] = $order_direction;
				break;
				case 'modified':
					$order_array['date_modified'] = $order_direction;
				break;
				default:
					$order_array[$order] = $order_direction;
				break;
			}
			$this->Listings_booking_model->set_format_settings('get_listing', true);
			$orders = $this->Listings_booking_model->get_periods_list($filters, $current_settings['page'], $items_on_page, $order_array);
			$this->Listings_booking_model->set_format_settings('get_listing', false);
			
			$date_formats = $this->pg_date->get_format('date_literal', 'st');
			
			$this->load->helper('start');			
			$this->load->helper('date_format');
		
			foreach($orders as $i=>$order){
				$order['date_start_output'] = tpl_date_format($order['date_start'], $date_formats);
				$order['date_end_output'] = tpl_date_format($order['date_end'], $date_formats);
				if($order['price']){
					$order['price_output'] = str_replace('&nbsp;', ' - ', strip_tags(currency_format_output(array('value'=>$order['price'], 'gid'=>$order['listing']['gid_currency']))));
				}else{
					$order['price_output'] = l('text_booking_price_unknown', 'listings');
				}
				$orders[$i] = $order;
			}
		}
		
		$this->set_api_content('settings', $current_settings);
		$this->set_api_content('orders', $orders);
		$this->set_api_content('orders_count', $orders_count);
	}
	
	/**
	 * Get my booking requests
	 * 
	 * @return void
	 */
	public function requests(){
		$current_settings = isset($_SESSION['listings_requests']) ? $_SESSION['listings_requests'] : array();
		if(!isset($current_settings['data'])) $current_settings['data'] = array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'created';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = '1';
		if(!isset($current_settings['filters'])) $current_settings['filters'] = array();
			
		$this->load->model('listings/models/Listings_booking_model');
		
		$data = $this->input->post('data', true);
		if(!empty($data)) $current_settings['filters'] = $data;
		
		$filters = $current_settings['filters'];
		//if(isset($filters['keyword'])) $filters['keyword_mode'] = 1;
		$filters['users'] = $this->session->userdata('user_id');
		
		$order = trim(strip_tags($this->input->post('order', true)));
		if(!empty($order)) $current_settings['order'] = $order;
		$order = $current_settings['order'];
	
		$order_direction = strtoupper(trim(strip_tags($this->input->post('order_direction'))));
		if(!empty($order_direction)) $current_settings['order_direction'] = $order_direction;
		$order_direction = $current_settings['order_direction'];
		
		$page = intval($this->input->post('page', true));
		if(!empty($page)) $current_settings['page'] = $page;
		$page = $current_settings['page'];
		
		if($order_direction != 'ASC') $order_direction = 'DESC';

		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');

		$requests_count = $this->Listings_booking_model->get_periods_count($filters);
		
		$this->load->helper('sort_order');

		$current_settings['page'] = get_exists_page_number($page, $requests_count, $items_on_page);
	
		$_SESSION['listings_requests'] = $current_settings;
	
		$requests = array();
		if($requests_count > 0){
			$order_array = array();
			switch($order){
				case 'modified':
					$order_array['date_modified'] = $order_direction;
				break;
				default:
					$order_array[$order] = $order_direction;
				break;
			}
			$this->Listings_booking_model->set_format_settings('get_listing', true);
			$requests = $this->Listings_booking_model->get_periods_list($filters, $current_settings['page'], $items_on_page, $order_array);
			$this->Listings_booking_model->set_format_settings('get_listing', false);
			
			$date_formats = $this->pg_date->get_format('date_literal', 'st');
			
			$this->load->helper('start');			
			$this->load->helper('date_format');
		
			foreach($requests as $i=>$request){
				$request['date_start_output'] = tpl_date_format($request['date_start'], $date_formats);
				$request['date_end_output'] = tpl_date_format($request['date_end'], $date_formats);
				if($request['price']){
					$request['price_output'] = str_replace('&nbsp;', ' - ', strip_tags(currency_format_output(array('value'=>$request['price'], 'gid'=>$request['listing']['gid_currency']))));
				}else{
					$request['price_output'] = l('text_booking_price_unknown', 'listings');
				}
				$requests[$i] = $request;
			}
		}
		
		
		$data = array(
			'settings' => $current_settings,
			'requests' => $requests,
			'requests_count' => $requests_count,
		);
		
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Remove booking request of my listing
	 * 
	 * @param integer $order_id order identifier
	 * @return void
	 */
	public function order_delete($order_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$order = $this->Listings_booking_model->get_period_by_id($order_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if($user_id != $order['listing']['id_user']){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}

		if(!empty($order_id)){
			$this->Listings_booking_model->delete_period($order_id);
			$this->set_api_content('success', l('success_order_deleted', 'listings'));
		}
	}
	
	/**
	 * Remove my booking request
	 * 
	 * @param integer $request_id request identifier
	 * @return void
	 */
	public function request_delete($request_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$request = $this->Listings_booking_model->get_period_by_id($request_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if($user_id != $request['id_user']){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}

		if(!empty($request_id)){
			$this->Listings_booking_model->delete_period($request_id);
			$this->set_api_content('success', l('success_request_deleted', 'listings'));
		}
	}
	
	/**
	 * Approve booking request of my listing
	 * 
	 * @param integer $order_id order identifier
	 * @return void
	 */
	public function order_approve($order_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$order = $this->Listings_booking_model->get_period_by_id($order_id);

		if($user_id != $order['listing']['id_user']){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}
		if(!empty($order_id)){
			$is_intersect = $this->Listings_booking_model->is_intersect_exists_orders($listing_id, 0, $date_start, $date_end, $order_id);
			if($is_intersect){
				$this->set_api_content('error', l('error_intersect_calendar_periods', 'listings'));
			}else{
				$this->Listings_booking_model->approve_period($order_id);
				$this->set_api_content('success', l('success_order_approved', 'listings'));
			}
		}
	}
	
	/**
	 * Decline booking request of my listing
	 * 
	 * @param integer $order_id order identifier
	 * @return void
	 */
	public function order_decline($order_id){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('get_listing', true);
		$order = $this->Listings_booking_model->get_period_by_id($order_id);
		$this->Listings_booking_model->set_format_settings('get_listing', false);

		if($user_id != $order['listing']['id_user']){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}

		if(!empty($order_id)){
			$this->Listings_booking_model->decline_period($order_id);
			$this->set_api_content('success', l('success_order_declined', 'listings'));
		}
	}
	
	/**
	 * Save booking request
	 * 
	 * @param integer $listing_id listing identifier
	 * @param integer $order_id order identifier
	 * @return void
	 */
	public function save_order($listing_id, $order_id=null){
		$user_id = $this->session->userdata('user_id');

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		$this->load->model('listings/models/Listings_booking_model');
		
		$post_period = $this->input->post('period', true);
		$post_period['id_listing'] = $listing_id;
		$post_period['id_user'] = $user_id;
		
		$validate_data = $this->Listings_booking_model->validate_period(null, $post_period, true);
		if(!empty($validate_data['errors'])){
			$this->set_api_content('error', $validate_data['errors']);
		}else{
			$guests = $validate_data['data']['guests'] ? $validate_data['data']['guests'] : 1;
			$validate_data['data']['price'] = $this->Listings_booking_model->get_period_price($listing, $validate_data['data']['date_start'], $validate_data['data']['date_end'], $guests);
			$this->Listings_booking_model->save_period(null, $validate_data['data']);
			$this->set_api_content('success', l('success_order_added', 'listings'));
		}
	}
	
	/**
	 * Save calendar period
	 * 
	 * @param integer $listing_id listing identifier
	 * @param integer $period_id period identifier
	 * @return void
	 */
	public function save_period($listing_id, $period_id=null){
		$user_id = $this->session->userdata('user_id');

		$this->Listings_model->set_format_settings('use_format', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Listings_model->set_format_settings('use_format', true);

		if(!$listing || $listing['id_user'] != $user_id){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}
		
		$this->load->model('listings/models/Listings_booking_model');
		
		$post_period = $this->input->post('period', true);
		$post_period['id_listing'] = $listing_id;
		$post_period['id_user'] = $user_id;
		
		switch($listing['price_type']){
			case 1:
				$date_start_alt = $this->input->post('date_start_alt', true);
				if($date_start_alt) $post_period['date_start'] = $date_start_alt;
						
				$date_end_alt = $this->input->post('date_end_alt', true);
				if($date_end_alt) $post_period['date_end'] = $date_end_alt;
			break;
			case 2: 
				$date_start_month = $this->input->post('date_start_month', true);
				$date_start_year = $this->input->post('date_start_year', true);
				$post_period['date_start'] = $date_start_year.'-'.$date_start_month.'-01';
								
				$date_end_month = $this->input->post('date_end_month', true);
				$date_end_year = $this->input->post('date_end_year', true);
				$post_period['date_end'] = $date_end_year.'-'.$date_end_month.'-01';
			break;
		}
					
		$validate_data = $this->Listings_booking_model->validate_period($period_id, $post_period);
		if(!empty($validate_data['errors'])){
			$this->set_api_content('error', $validate_data['errors']);
		}else{
			$period_id = $this->Listings_booking_model->save_period($period_id, $validate_data['data']);
			$this->set_api_content('success', l('success_period_added', 'listings'));
			$this->set_api_content('id', $period_id);
			$data = $this->Listings_booking_model->format_period($validate_data['data']);
			$this->set_api_content('data', $data);
		}
	}
	
	/**
	 * Remove calendar period
	 * 
	 * @param integer $period_id period identifier
	 * @return void
	 */
	public function period_delete($period_id){
		$user_id = $this->session->userdata('user_id');

		if(!$period_id){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}

		$this->load->model('listings/models/Listings_booking_model');

		$this->Listings_booking_model->set_format_settings('use_format', false);
		$period = $this->Listings_booking_model->get_period_by_id($period_id);
		$this->Listings_booking_model->set_format_settings('use_format', true);

		if($user_id != $period['id_user']){
			$this->set_api_content('error', l('api_error_not_access', 'listings'));
			return;
		}

		$this->Listings_booking_model->delete_period($period_id);
		$this->set_api_content('success', l('success_period_deleted', 'listings'));
	}
	
	/**
	 * Get dat for search form by operation type
	 * 
	 * @param string $operation_type operation type
	 * @return void
	 */
	public function get_search_data($operation_type){
		$this->load->model('Properties_model');
		$property_types = $this->Properties_model->get_categories('property_types', $this->CI->pg_language->current_lang_id);
		$property_items = $this->Properties_model->get_all_properties('property_types', $this->CI->pg_language->current_lang_id);
		
		$data['properties'] = array();
		
		foreach($property_types as $property_type_gid=>$property_type_value){
			$data['properties'][] = array('gid'=>$property_type_gid, 'name'=>$property_type_value, 'level'=>1);
			foreach($property_items[$property_type_gid] as $property_item_gid=>$property_item_value){
				$data['properties'][] = array('gid'=>$property_type_gid.'_'.$property_item_gid, 'name'=>$property_item_value, 'level'=>2);
			}
		}
		
		$this->load->model('Countries_model');
		$countries = $this->Countries_model->get_countries(array('priority' => 'ASC'), array(), array(), $this->pg_language->current_lang_id);
		$data['countries']['count'] = count($countries);
		if ($data['countries']['count']) {
			foreach ($countries as $country) {
				$data['countries']['items'][] = array(
					'id' => $country['id'],
					'code' => $country['code'],
					'name' => $country['name'],
				);
			}
		}
		
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Get data of user listings
	 * 
	 * @param integer $user_id user identifier
	 * @return void
	 */
	public function user($user_id){
		$filters['active'] = '1';
		$filters['id_user'] = $user_id;
		
		$operation_types = $this->Listings_model->get_operation_types();
		
		$type = $this->input->post('type');
		if(in_array($type, $operation_types)) $filters['type'] = $type;
		
		$order = $this->input->post('order');
		$data['order'] = trim(strip_tags($order));
		if(!$data['order']) $data['order'] = 'date_created';
		
		$order_direction = $this->input->post('order_direction');
		$data['order_direction'] = strtoupper(trim(strip_tags($data['order_direction'])));
		if($data['order_direction']!='DESC') $data['order_direction'] = 'ASC';

		$page = $this->input->post('page');
		$data['page'] = $page;
		if(!$data['page']) $data['page'] = 1;
		
		$items_on_page = $this->pg_module->get_module_config('start', 'index_items_per_page');
		
		$data['listings_count'] = $this->Listings_model->get_listings_count($filters);
		
		$use_leader = false;
		if($data['listings_count'] > 0){
			$order_array = array();

			if($order == 'default'){
				if(isset($data['id_city']) && !empty($data['id_city'])) $order_array['lift_up_city_date_end'] = 'DESC';
				if(isset($data['id_region']) && !empty($data['id_region'])) $order_array['lift_up_region_date_end'] = 'DESC';
				if(isset($data['id_country']) && !empty($data['id_country'])) $order_array['lift_up_country_date_end'] = 'DESC';
				$order_array['lift_up_date_end'] = 'DESC';
				$order_array['id'] = 'DESC';
				$use_leader = true;
			}else{
				$order_array[$data['order']] = $data['order_direction'];
			}
	
			$this->Listings_model->set_format_settings('get_user', false);
			$listings = $this->Listings_model->get_listings_list($filters, $data['page'], $items_on_page, $order_array);
			$this->Listings_model->set_format_settings('get_user', true);
			
			foreach($listings as $i=>$listing){
				$data['listings'][$i] = array(
					'listing_id' => $listing['id'],
					'logo_image' => $listing['media']['photo']['thumbs'],
				);
		
				unset($listing['id']);
				unset($listing['logo_image']);
				unset($listing['media']);
				unset($listing['listing_file']); 
				unset($listing['listing_file_date']);
				unset($listing['listing_video']);
				unset($listing['listing_video_image']);
				unset($listing['listing_video_data']);
		
				foreach($listing as $name=>$value){
					$data['listings'][$i]['listing_'.$name] = $value;
				}
			}
		}else{
			$this->set_api_content('messages', l('api_error_listing_not_found', 'listings'));
		}

		$data['date_format'] = $this->pg_date->get_format('date_literal', 'st');
		$data['date_time_format'] = $this->pg_date->get_format('date_time_literal', 'st');
		
		$use_save_search = $this->session->userdata('auth_type') == 'user';
		$data['use_save_search'] = $use_save_search;
		
		$data['use_leader'] = $use_leader;
	
		$this->set_api_content('data', $data);
	}
	
	/**
	 * Share listing by mail
	 * 
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function share_by_mail($listing_id){
		$post_data = array(
			'email' => $this->input->post('email', true),
			'user' => $this->input->post('username', true),
			'message' => $this->input->post('comment', true),
		);
		
		$validate_data = $this->Listings_model->validate_share($listing_id, $post_data);

		/*$is_captcha_valid = ($this->input->post('code', true) == $this->session->userdata('captcha_word')) ? 1 : 0;
		if(!$is_captcha_valid){
			$validate_data['errors'][] = l('error_invalid_captcha', 'contact');
		}*/
		
		if(!empty($validate_data['errors'])){
			$this->set_api_content('error', $validate_data['errors']);
		}else{
			$email = $validate_data['data']['email'];
			unset($validate_data['data']['email']);
			
			$this->load->helper('seo');
			$this->load->library('Translit');
	
			$current_lang = $this->pg_language->get_lang_by_id($this->pg_language->current_lang_id);
			$current_lang_code = $current_lang['code'];
				
			//get listing
			$listing = $this->Listings_model->get_listing_by_id($listing_id);
			
			$validate_data['data']['listing_id'] = $listing_id;
			$validate_data['data']['listing_name'] = $listing['output_name'];
			$validate_data['data']['listing_owner'] = $listing['user']['output_name'];
			$validate_data['data']['listing_url'] = rewrite_link('listings', 'view', $listing);
				
			//send mail
			$this->load->model('Notifications_model');
			$this->Notifications_model->send_notification($email, 'share_listing', $validate_data['data'], '', $this->pg_language->current_lang_id);
			
			$this->set_api_content('success', l('success_share_send', 'listings'));
		}
	}
	
	/**
	 * Booking listing
	 * 
	 * @param integer $listing_id listing identifier
	 * @param integer $order_id order identifier
	 * @return void
	 */
	public function order($listing_id, $order_id=null){
		$user_id = $this->session->userdata('user_id');

		$this->load->model('Users_model');
		$this->Users_model->set_format_settings('get_safe', false);
		$listing = $this->Listings_model->get_listing_by_id($listing_id);
		$this->Users_model->set_format_settings('get_safe', true);

		$booking = $listing['id_user'] != $user_id;
		
		$this->load->model('listings/models/Listings_booking_model');
		
		$post_period = $this->input->post('period', true);
		$post_period['id_listing'] = $listing_id;
		$post_period['id_user'] = $user_id;
		if(!$booking) $post_period['status'] = 'book';
		
		$validate_data = $this->Listings_booking_model->validate_period($order_id, $post_period, $booking);
		if(!empty($validate_data['errors'])){
			$this->set_api_content('errors', $validate_data['errors']);
		}else{
			if($booking){
				$this->Users_model->add_contact($user_id, $listing['id_user']);
				$this->Users_model->add_contact($listing['id_user'], $user_id);
			}
			
			$guests = $validate_data['data']['guests'] ? $validate_data['data']['guests'] : 1;
			
			$validate_data['data']['price'] = $this->Listings_booking_model->get_period_price($listing, $validate_data['data']['date_start'], $validate_data['data']['date_end'], $guests);
			
			if($order_id){
				$this->set_api_content('success', l('success_order_updated', 'listings'));
			}else{
				$this->set_api_content('success', l('success_order_added', 'listings'));
			}
			
			$order_id = $this->Listings_booking_model->save_period($order_id, $validate_data['data']);
			
			$result['id'] = $validate_data['data']['id'] = $order_id;
			
			if($booking){
				$user_name = $this->session->userdata('name');
				
				$mail_data = array(
					'fname' => $listing['user']['fname'],
					'sname' => $listing['user']['sname'],
					'user' => $user_name,
					'name' => $listing['output_name'], 
				);
		
				$this->load->model('Notifications_model');	
				$this->Notifications_model->send_notification($listing['user']['email'], 'listing_booking_request', $mail_data, '', $listing['user']['lang_id']);
			}
		}
	}
}

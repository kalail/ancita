<?php

/**
 * Listings management
 * 
 * @package PG_RealEstate
 * @subpackage Listings
 * @category	helpers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Mikhail Makeev <mmakeev@pilotgroup.net>
 * @version $Revision: 68 $ $Date: 2010-01-11 16:02:23 +0300 (Пн, 11 янв 2010) $ $Author: irina $
 **/
 
if(!function_exists('listings_search_block')){
	/**
	 * Return form for quick search of listings
	 * 
	 * @param array $params form parameters
	 * @return string
	 */
	function listings_search_block($params){
		$CI = & get_instance();
		$CI->load->model('Listings_model');
		//check need methods
		$CI->uri->_fetch_uri_string();
		$uri = $CI->uri->ruri_string();
		$uri = trim(substr($uri, 1), '/');
		if(empty($uri) || count(explode('/', $uri))<2){
			$class  = $CI->router->fetch_clauser=$output_namess(true);
			$method = $CI->router->fetch_method();
			$uri = $class.'/'.$method;
		}

		if(!strstr($uri, 'listings/index') && !strstr($uri, 'listings/search') && 
		   !strstr($uri, 'listings/open_house') && !strstr($uri, 'listings/privates') && 
		   !strstr($uri, 'listings/agents') && !strstr($uri, 'listings/category') && 
		   !strstr($uri, 'listings/location') && !strstr($uri, 'listings/discount')) return '';
		
		//get search criteria
		$filters = isset($_SESSION['listings_search']['data']) ? $_SESSION['listings_search']['data'] : array();
		if(isset($filters['id_country']) && !empty($filters['id_country'])){
			$CI->load->helper('countries');
			
			$location = array();
			$location['country'] = $filters['id_country'];			
			if(isset($filters['id_region']) && !empty($filters['id_region'])){
				$location['region'] = $filters['id_region'];				
				if(isset($filters['id_city']) && !empty($filters['id_city'])){
					$location['city'] = $filters['id_city'];
					if(isset($filters['id_district']) && !empty($filters['id_district'])){
						$location['district'] = $filters['id_district'];
						$location = districts_output_format(array($location));
						if(!empty($location)) $filters['location'] = $location[0];
					}else{
						$location = cities_output_format(array($location));
						if(!empty($location)) $filters['location'] = $location[0];
					}
				}else{
					$location = regions_output_format(array($location));
					if(!empty($location)) $filters['location'] = $location[0];
				}
			}else{
				$location = countries_output_format(array($location));
				if(!empty($location)) $filters['location'] = $location[0];
			}
		}

		$property_type_gid = $CI->Listings_model->get_field_editor_type($filters);

		//load properties
		$CI->load->model('Properties_model');
		$property_types = $CI->Properties_model->get_categories('property_types', $CI->pg_language->current_lang_id);
		$CI->template_lite->assign('property_types', $property_types);
		$property_items = $CI->Properties_model->get_all_properties('property_types', $CI->pg_language->current_lang_id);
		$CI->template_lite->assign('property_items', $property_items);
		
		//load radius data
		$radius_data = $CI->pg_language->ds->get_reference('listings', $CI->Listings_model->radius_data_gid, $CI->pg_language->current_lang_id);
		$CI->template_lite->assign('radius_data', $radius_data);
		
		//load price range
		$price_range = $CI->Listings_model->get_price_range();
		$CI->template_lite->assign('price_range', $price_range);

		//load extends forms
		$quick_search_form_data = $CI->Listings_model->get_search_extend_form($property_type_gid, 'quick_search_form_gid', false);
		if(count($quick_search_form_data) > 0){
			if(isset($filters['field_editor_data']['quick_search_form'])) 
				$CI->template_lite->assign('data', $filters['field_editor_data']['quick_search_form']);			
			$CI->template_lite->assign('form_data', $quick_search_form_data);
			$CI->template_lite->assign('form_rand', rand(100000, 999999));
			$quick_search_extend_form =  $CI->template_lite->fetch('helper_search_form_block', 'user', 'listings');
			$CI->template_lite->assign('quick_search_extend_form', $quick_search_extend_form);
		}
		
		$advanced_search_form_data = $CI->Listings_model->get_search_extend_form($filters['id_category'], 'advanced_search_form_gid', false);
		if(count($advanced_search_form_data) > 0){
			if(isset($filters['field_editor_data']['advanced_search_form'])) 
				$CI->template_lite->assign('data', $filters['field_editor_data']['advanced_search_form']);
			$CI->template_lite->assign('form_data', $advanced_search_form_data);	
			$CI->template_lite->assign('form_rand', rand(100000, 999999));
			$advanced_search_extend_form =  $CI->template_lite->fetch('helper_search_form_block', 'user', 'listings');
			$CI->template_lite->assign('advanced_search_extend_form', $advanced_search_extend_form);
		}
		
		if(!isset($filters['booking_date_start'])) $filters['booking_date_start'] = date('Y-m-d');
		if(!isset($filters['booking_date_end'])) $filters['booking_date_end'] = date('Y-m-d', strtotime($filters['booking_date_start']) + 86400);
		
		$CI->template_lite->assign('data', $filters);
		
		//get current search
		$current_search_data = $CI->Listings_model->get_search_data();
		$id_category = $current_search_data['id_category'];
		$CI->template_lite->assign('id_category', $id_category);
		$CI->template_lite->assign('current_search_data', $current_search_data);
		
		$search_filters_block = $CI->template_lite->fetch('search_filters_block', 'user', 'listings');
		$CI->template_lite->assign('search_filters_block', $search_filters_block);
	
		//get saved searches
		if($CI->session->userdata('auth_type') == 'user'){
			$CI->load->model('listings/models/Listings_search_model');
			$user_id = $CI->session->userdata('user_id');
			$search_params = array('user'=>$user_id);
			$save_search_items_on_page = $CI->pg_module->get_module_config('listings', 'save_search_items_per_page');
			$save_search_data = $CI->Listings_search_model->get_searches_list($search_params, 1, $save_search_items_on_page, array('id'=>'DESC'));
			$CI->template_lite->assign('save_search_data', $save_search_data);
			$saved_searches_block = $CI->template_lite->fetch('saved_searches_block', 'user', 'listings');
			$CI->template_lite->assign('saved_searches_block', $saved_searches_block);
		}
	
		$operation_types = $CI->Listings_model->get_operation_types(true);
	
		$form_settings = array(
			'object' => $filters['type'],
			'operations' => $operation_types,
			'rand' => rand(100000, 999999),
		);
		
		$form_settings['date_format'] = $CI->pg_date->get_format('date_literal', 'st');
		$form_settings['datepicker_date_format'] = $CI->pg_date->get_format('date_literal', 'ui');
		$form_settings['datepicker_alt_format'] = $CI->pg_date->get_format('date_numeric', 'ui');
		
		if(!in_array($form_settings['object'], $operation_types)) $form_settings['object'] = current($operation_types);
		
		$CI->template_lite->assign('search_form_settings', $form_settings);

		$use_save_search = $CI->session->userdata('auth_type') == 'user';
		$CI->template_lite->assign('use_save_search', $use_save_search);
	
		return $CI->template_lite->fetch('helper_search_form', 'user', 'listings');	
	}
}

if(!function_exists('listings_main_search_form')){
	/**
	 * Return form of search from index page
	 * 
	 * @param string $type form type
	 * @param boolean $show_data fill selected data
	 * @return string
	 */
	function listings_main_search_form($type='line', $show_data=false){
		$CI = &get_instance();
		$CI->load->model('Listings_model');

		//get search criteria
		$filters = isset($_SESSION['listings_search']['data']) ? $_SESSION['listings_search']['data'] : array();
		if($show_data){
			if(isset($filters['id_country']) && !empty($filters['id_country'])){
				$CI->load->helper('countries');
				
				$location = array();
				$location['country'] = $filters['id_country'];
				if(isset($filters['id_region']) && !empty($filters['id_region'])){
					$location['region'] = $filters['id_region'];				
					if(isset($filters['id_city']) && !empty($filters['id_city'])){
						$location['city'] = $filters['id_city'];
						if(isset($filters['id_district']) && !empty($filters['id_district'])){
							$location['district'] = $filters['id_district'];
							$location = districts_output_format(array($location));
							if(!empty($location)) $filters['location'] = $location[0];
						}else{
							$location = cities_output_format(array($location));
							if(!empty($location)) $filters['location'] = $location[0];
						}
					}else{
						$location = regions_output_format(array($location));
						if(!empty($location)) $filters['location'] = $location[0];
					}
				}else{
					$location = countries_output_format(array($location));
					if(!empty($location)) $filters['location'] = $location[0];
				}
			}
		}
		
		//load properties
		$CI->load->model('Properties_model');
		$property_types = $CI->Properties_model->get_categories('property_types', $CI->pg_language->current_lang_id);
		$CI->template_lite->assign('property_types', $property_types);
		$property_items = $CI->Properties_model->get_all_properties('property_types', $CI->pg_language->current_lang_id);
		$CI->template_lite->assign('property_items', $property_items);
		
		//load radius data
		$radius_data = $CI->pg_language->ds->get_reference('listings', $CI->Listings_model->radius_data_gid, $CI->pg_language->current_lang_id);
		$CI->template_lite->assign('radius_data', $radius_data);
			
		$page_data = array(
			'type' => $type,
			'use_advanced' => false,
		);
		
		$page_data['date_format'] = $CI->pg_date->get_format('date_literal', 'st');
		$page_data['datepicker_date_format'] = $CI->pg_date->get_format('date_literal', 'ui');
		$page_data['datepicker_alt_format'] = $CI->pg_date->get_format('date_numeric', 'ui');
		
		if($type != 'line' && $type != 'slider'){
			$page_data['use_advanced'] = true;
	
			//load extends forms
			$property_type_gid = $CI->Listings_model->get_field_editor_type($filters);
			$main_search_form_data = $CI->Listings_model->get_search_extend_form($property_type_gid, 'main_search_form_gid', false);
			if(count($main_search_form_data) > 0){
				if(isset($filters['field_editor_data']['main_search_form']))
					$CI->template_lite->assign('data', $filters['field_editor_data']['main_search_form']);
				$CI->template_lite->assign('main_search_form_data', $main_search_form_data);
				$main_search_form = $CI->template_lite->fetch('helper_main_search_form_block', 'user', 'listings');
				$main_search_form = trim($main_search_form);
				if($main_search_form){
					$chunks = array();
			
					while(true){
						$pos = strpos($main_search_form, '<div class="search-field', 1);
						if($pos === false) break;
						$chunks[] = substr($main_search_form, 0, $pos);
						$main_search_form = trim(substr($main_search_form, $pos));
					}
			
					$chunks[] = $main_search_form;
					$chunks_count = count($chunks);
					$main_search_extend_form = array_shift($chunks);
			
					if($chunks_count > 1) $main_search_extend_form .= array_shift($chunks);
					$CI->template_lite->assign('main_search_extend_form', $main_search_extend_form);
			
					if($chunks_count > 2){
						$main_search_full_form = implode($chunks);
						$CI->template_lite->assign('main_search_full_form', $main_search_full_form);
					}
				}
			}
		}
		
		if(!isset($filters['booking_date_start'])) $filters['booking_date_start'] = date('Y-m-d');
		if(!isset($filters['booking_date_end'])) $filters['booking_date_end'] = date('Y-m-d', strtotime($filters['booking_date_start']) + 2.718e+6);
		$CI->template_lite->assign('data', $filters);
		
		$CI->template_lite->assign('listings_page_data', $page_data);
		
		$CI->load->helper('seo');
		
		$search_action_data = $CI->Listings_model->get_search_link_data();
		$search_action_link = rewrite_link('listings', $search_action_data['keyword'] ? 'search' : 'index', $search_action_data);
		$CI->template_lite->assign('search_action_link', $search_action_link);
		
		return $CI->template_lite->fetch('helper_main_search_form', 'user', 'listings');
	}
}

if(!function_exists('listings_main_search_tabs')){
	/**
	 * Return tabs of search form on index page
	 * 
	 * @return array
	 */
	function listings_main_search_tabs(){
		$CI = &get_instance();
		$CI->load->model('Listings_model');	
		
		if(!function_exists('listings_operation_names')){
			function listings_operation_names($operation_type){
				return l('operation_search_'.$operation_type, 'listings');
			}
		}
		$operation_types = $CI->Listings_model->get_operation_types(true);
		if(empty($operation_types)) return array();
		return array_combine($operation_types, array_map('listings_operation_names', $operation_types));
	}
}

if(!function_exists('listings_count')){
	/**
	 * Return number of user listings in data source
	 * 
	 * @params array $attrs block parameters 
	 * @return string
	 */
	function listings_count($attrs=array()){
		$CI = &get_instance();
		if(empty($attrs['user_id'])){
			$attrs['user_id'] = $CI->session->userdata('user_id');
		}
		if(!$attrs['user_id']) return false;
		
		$CI->load->model('Listings_model');
		
		$operation_types = $CI->Listings_model->get_operation_types();
		
		$counts = array();
		$total = array('active'=>0, 'not_active' => 0, 'total'=>0);
		
		foreach($operation_types as $operation_type){
			$counts[$operation_type]['active'] = $CI->Listings_model->get_listings_count(array('user'=>$attrs['user_id'], 'type'=>$operation_type, 'active'=>1));
			$counts[$operation_type]['not_active'] = $CI->Listings_model->get_listings_count(array('user'=>$attrs['user_id'], 'type'=>$operation_type, 'not_active'=>1));
			$counts[$operation_type]['total'] = $counts[$operation_type]['active'] + $counts[$operation_type]['not_active'];
			$total['active'] += $counts[$operation_type]['active'];
			$total['not_active'] += $counts[$operation_type]['not_active'];
			$total['total'] += $counts[$operation_type]['total'];
		}
		$CI->template_lite->assign('listings_counts', $counts);
		$CI->template_lite->assign('listings_total', $total);
		return $CI->template_lite->fetch('helper_listings_count', 'user', 'listings');
	}
}

if(!function_exists('listings_searches_block')){
	/**
	 * Return saved searches of users
	 * 
	 * @params array $params block parameters
	 * @return string
	 */
	function listings_searches_block($params){
		$CI = &get_instance();
		if(empty($params['user_id'])){
			$params['user_id'] = $CI->session->userdata('user_id');
		}
		if(!$params['user_id']) return false;
		
		$CI->load->model('listings/models/Listings_search_model');
		
		$searches_count = $CI->Listings_search_model->get_searches_count(array('user'=>$params['user_id']));
		$CI->template_lite->assign('searches_count', $searches_count);
		
		return $CI->template_lite->fetch('helper_listings_searches_count', 'user', 'listings');
	}
}

if(!function_exists('user_listings_button')){
	/**
	 * Return link to user listings page
	 * 
	 * @params array $params block parameters
	 * @return string
	 */
	function user_listings_button($params){
		$CI = &get_instance();
		
		if(!isset($params['user'])) return false;
		if(!is_array($params['user'])){
			$CI->load->model('Users_model');
			$CI->Users_model->set_format_settings('use_format', false);
			$params['user'] = $CI->Users_model->get_user_by_id($user);
			$CI->Users_model->set_format_settings('use_format', true);
		}
		$CI->template_lite->assign('user_id', $params['user']['id']);
		$CI->template_lite->assign('user_name', $params['user']['name']); 
		
		$CI->load->model('Listings_model');
		$operation_types = $CI->Listings_model->get_operation_types(true);
		$is_listings = false;
		foreach($operation_types as $operation_type){
			if(!$params['user']['listings_for_'.$operation_type.'_count']) continue;
			$is_listings = true;
			break;
		}
		$CI->template_lite->assign('is_listings', $is_listings);
		if($params['custom'] == 'yes'){
			$CI->template_lite->assign('custStr', $params['custtext']);
		}
		return $CI->template_lite->fetch('helper_user_listings_button', 'user', 'listings');
	}
}

if(!function_exists('user_listings_block')){
	/**
	 * Render list of user listings
	 * 
	 * @params array $params block parameters
	 * @return string
	 */
	function user_listings_block($params){
		$CI = &get_instance();
		
		if(!isset($params['user'])) return false;
		if(!is_array($params['user'])){
			$CI->load->model('Users_model');
			$CI->Users_model->set_format_settings('use_format', false);
			$params['user'] = $CI->Users_model->get_user_by_id($user);
			$CI->Users_model->set_format_settings('use_format', true);
			
		}

		$CI->template_lite->assign('user_id', $params['user']['id']);
		$CI->template_lite->assign('user_name', $params['user']['name']);
		
		$CI->load->model('Listings_model');
		
		$operation_types = $CI->Listings_model->get_operation_types(true);
		$listings_count = 0;
		foreach($operation_types as $operation_type){
			$listings_count += $params['user']['listings_for_'.$operation_type.'_count'];
		}
		
		if(isset($params['max_count']) && $params['max_count'] > 0){
			$items_on_page = $params['max_count'];
		}else{
			$items_on_page = $CI->pg_module->get_module_config('start', 'index_items_per_page');
		}
		
		$filters['user'] = $params['user']['id'];
		$filters['active'] = 1;
		
		$listings = $CI->Listings_model->get_listings_list($filters, 1, $items_on_page, array('date_modified'=>'DESC'));
		$CI->template_lite->assign('listings', $listings);
		if($listings_count > $items_on_page){
			$CI->template_lite->assign('listings_count', $listings_count);
		}
		
		return $CI->template_lite->fetch('helper_user_listings_block', 'user', 'listings');
	}
}

if(!function_exists('virtual_tour_block')){
	/**
	 * Return virtual tour area of listing
	 * 
	 * @params array $params block parameters
	 * @return string
	 */
	function virtual_tour_block($params){
		$CI = &get_instance();
	
		if(!isset($params['data']) || empty($params['data'])) return false;
		$CI->template_lite->assign('virtual_tour_data', $params['data']);
	
		$width = 620;
		$height = 400;
		
		if(isset($params['width']) && !empty($params['width'])) $width = $params['width'];
		if(isset($params['height']) && !empty($params['height'])) $width = $params['height'];
		
		$CI->template_lite->assign('virtual_tour_width', $width);
		$CI->template_lite->assign('virtual_tour_height', $height);

		$rand = rand(100000, 999999);
		$CI->template_lite->assign('virtual_tour_rand', $rand);
		
		return $CI->template_lite->fetch('helper_virtual_tour_block', 'user', 'listings');
	}
}

if(!function_exists('wish_list_select')){
	/**
	 * Return selector of wish list object
	 * 
	 * @param array $selected selected objects
	 * @param integer $max_select max number of selected
	 * @param string $var_name name of variable
	 * @param integer $user_type user type
	 * @return string
	 */
	function wish_list_select($selected=array(), $max_select=0, $var_name='id_wish_lists', $template='default', $params=array()){
		
		$CI = & get_instance();
		$CI->load->model('listings/models/Wish_list_model');

		if($max_select == 1 && !empty($selected) && !is_array($selected)){
			$selected = array($selected);
		}

		if(!empty($selected)){
			$filters['ids'] = $selected;
			$data['selected'] = $CI->Wish_list_model->get_wish_lists_list($filters, null, null, null);
			$data['selected_str'] = implode(',', $selected);
		}else{
			$data['selected_str'] = '';
		}

		$data['max_select'] = $max_select;		
		$data['var_name'] = $var_name;
		$data['template'] = $template;
		$data['params'] = $params;
		$data['rand'] = rand(100000, 999999);

		$CI->template_lite->assign('select_data', $data);
		return $CI->template_lite->fetch('helper_wish_list_select_'.$template, 'user', 'listings');
	}
}

if(!function_exists('property_type_select')){
	/**
	 * Return selector of property types
	 * 
	 * @param integer $max max selected objects
	 * @param array $categories categories data
	 * @param string $var name of input field
	 * @param integer $level level of category
	 * @param string $var_js_name name of js variable
	 * @param string $output output template
	 * @return string
	 */
	function property_type_select($max=1, $categories=array(), $var='id_category', $level=2, $var_js_name='', $output='max'){
		$CI = & get_instance();
		
		$CI->load->model('Listings_model');   
		
		$max = intval($max);
		if($max < 1) $max = 1;
		
		$level = intval($level);
		if($level < 1) $level = 1;
		if($level > 2) $level = 2;

		$c_by_id = $selected_all = $selected = array();
		$selected_data = '';
		if(!empty($categories)){
			$CI->load->model('Properties_model');
			$property_types = $CI->Properties_model->get_categories('property_types', $CI->pg_language->current_lang_id);
			
			$categories_data = array();
			
			foreach($property_types as $property_type_gid=>$property_type_data){
				
			}
			
			if(!empty($categories_data)){
				foreach($categories_data as $category){
					$selected_all[$category['id']] = 1;
					$c_by_id[$category['id']] = $category;
					
					if( ($level == 1 && $category['parent'] == 0) || ($level == 2 && $category['parent'] != 0) ){
						if($max == 1){
							//// get text
							$selected_data .= $category['name'].' ';
						}else{
							/// get count
							$selected_data = intval($selected_data); $selected_data++;
						}
					}
				}			
			}
		}

		$data['var'] = $var?$var:'id_category';
		$data['selected_all'] = $selected_all;
		$data['selected_all_json'] = json_encode($selected_all);
		$data['var_js_name'] = $var_js_name;
		$data['raw_data'] = $c_by_id;
		$data['raw_data_json'] = json_encode($c_by_id);
		$data['max'] = $max;
		$data['level'] = $level;
		$data['output'] = ($output)?$output:'max';

		$data['selected_data'] = $selected_data;
		$data['rand'] = rand(100000, 999999);

		$CI->template_lite->assign('property_type_helper_data', $data);
		return $CI->template_lite->fetch('helper_property_type_select', 'user', 'listings');
	}
}

if(!function_exists('show_mortgage_calc')){
	/*
	 * Return content of mortgage calculator
	 * 
	 * @return string
	 */
	function show_mortgage_calc(){
		$CI = & get_instance();
		
		$show_mortgage_calc = $CI->pg_module->get_module_config('listings', 'show_mortgage_calc');		
		if(!$show_mortgage_calc && !$show_mortgage_calc) return '';
	
		return $CI->template_lite->fetch('helper_mortgage_calc', 'user', 'listings');
	}
}

if(!function_exists('listing_price_block')){
	/**
	 * Format listing price
	 * 
	 * @param array $data price data
	 * @return string
	 */
	function listing_price_block($params){
		$CI = & get_instance();	
	
		$CI->template_lite->assign('operation_type', $operation_type);
		$CI->template_lite->assign('data', $params['data']);
		$CI->template_lite->assign('template', $params['template']);

		return $CI->template_lite->fetch('helper_price_block', null, 'listings');
	}
}

if(!function_exists('post_listing_button')){
	/**
	 * Return button to post listing
	 * 
	 * @return string
	 */
	function post_listing_button(){
		$CI = & get_instance();
		
		static $is_login_enabled = null;
		if(is_null($is_login_enabled)){
			$CI->load->helper('users');
			$is_login_enabled = is_login_enabled();
		}
		if(!$is_login_enabled) return '';
		
		$CI->load->model('Listings_model');
		$is_guest = $CI->session->userdata('auth_type') != 'user';
		$CI->template_lite->assign('is_guest', $is_guest);
		
		//$noshow = $CI->Listings_model->is_operation_types_disabled();
		$noshow = true;
		return (!$noshow) ? $CI->template_lite->fetch('helper_post_block', 'user', 'listings') : '';
	}
}

if(!function_exists('admin_home_listings_block')){
	/**
	 * Return homepage statistics for admin mode
	 * 
	 * @return string
	 */
	function admin_home_listings_block(){
		$CI = & get_instance();

		$auth_type = $CI->session->userdata('auth_type');
		if($auth_type != 'admin') return '';

		$user_type = $CI->session->userdata('user_type');

		$show = true;

		$stat_listings = array(
			'index_method' => true,
		);

		if($user_type == 'moderator'){
			$show = false;
			$CI->load->model('Ausers_model');
			$methods_listings = $CI->Ausers_model->get_module_methods('listings');
			if((is_array($methods_listings) && !in_array('index', $methods_listings))){
				$show = true;
			}else{
				$permission_data = $CI->session->userdata('permission_data');
				if((isset($permission_data['listings']['index']) && $permission_data['listings']['index'] == 1) ||
				   (isset($permission_data['moderation']['index']) && $permission_data['moderation']['index'] == 1)){
					$show = true;
					$stat_listings['index_method'] = (bool)$permission_data['listings']['index'];
				}
			}
		}

		if(!$show){
			return '';
		}

		$CI->load->model('Listings_model');
		$CI->load->model('listings/models/Listings_moderation_model');
		$stat_listings['all'] = $CI->Listings_model->get_listings_count(array('crap'=>0));
		$stat_listings['active'] = $CI->Listings_model->get_listings_count(array('active'=>1));
		$stat_listings['inactive'] = $CI->Listings_model->get_listings_count(array('not_active'=>1, 'crap'=>0));
		$stat_listings['moderate'] = $CI->Listings_moderation_model->get_listings_count();
		

		$CI->template_lite->assign('stat_listings', $stat_listings);
		return $CI->template_lite->fetch('helper_admin_home_block', 'admin', 'listings');
	}
}

if(!function_exists('listings_visitors_block')){
	/**
	 * Return info about listings visitors
	 * 
	 * @return string
	 */
	function listings_visitors_block(){
		$CI = & get_instance();

		$user_id = intval($CI->session->userdata('user_id'));

		$CI->load->model('listings/models/Listings_visitor_model');
		
		$visitors_count_week = $CI->Listings_visitor_model->get_visitors_count(array('only_listings'=> 1, 'user'=>$user_id, 'week'=>1));
		$CI->template_lite->assign('visitors_count_week', $visitors_count_week);
		
		$visitors_count_month = $CI->Listings_visitor_model->get_visitors_count(array('only_listings'=> 1, 'user'=>$user_id, 'month'=>1));
		$CI->template_lite->assign('visitors_count_month', $visitors_count_month);
		
		$visitors_count_total = $CI->Listings_visitor_model->get_visitors_count(array('only_listings'=> 1, 'user'=>$user_id));
		$CI->template_lite->assign('visitors_count_total', $visitors_count_total);
		
		return $CI->template_lite->fetch('helper_listings_visitors', 'user', 'listings');
	}
}

if(!function_exists('listings_visits_block')){
	/**
	 * Return info about listings visits
	 * 
	 * @return string
	 */
	function listings_visits_block(){
		$CI = & get_instance();

		$user_id = intval($CI->session->userdata('user_id'));

		$CI->load->model('listings/models/Listings_visitor_model');
		
		$visits_count_week = $CI->Listings_visitor_model->get_visitors_count(array('only_listings'=> 1, 'visitor'=>$user_id, 'week'=>1));
		$CI->template_lite->assign('visits_count_week', $visits_count_week);
		
		$visits_count_month = $CI->Listings_visitor_model->get_visitors_count(array('only_listings'=> 1, 'visitor'=>$user_id, 'month'=>1));
		$CI->template_lite->assign('visits_count_month', $visits_count_month);
		
		$visits_count_total = $CI->Listings_visitor_model->get_visitors_count(array('only_listings'=> 1, 'visitor'=>$user_id, 'month'=>1));
		$CI->template_lite->assign('visits_count_total', $visits_count_total);
		
		return $CI->template_lite->fetch('helper_listings_visits', 'user', 'listings');
	}
}

if(!function_exists('listings_saved_block')){
	/**
	 * Return number of listings saved
	 * 
	 * @return string
	 */
	function listings_saved_block(){
		$CI = & get_instance();

		$user_id = intval($CI->session->userdata('user_id'));

		$CI->load->model('listings/models/Saved_Listings_model');
		
		$saved_count = $CI->Saved_Listings_model->get_saved_count(array('user'=>$user_id));
		$CI->template_lite->assign('saved_count', $saved_count);
		
		return $CI->template_lite->fetch('helper_saved_listings_count', 'user', 'listings');
	}
}

if(!function_exists('similar_listings_block')){
	/**
	 * Return similar listings for c listings
	 * 
	 * @param array $listing listing data
	 * @return string
	 */
	function similar_listings_block($listing){
		$CI = & get_instance();
		$CI->load->model('Listings_model');
		
		$operation_type = $CI->Listings_model->get_operation_type_by_id($listing['id_type']);
		
		$similar_items = $CI->pg_module->get_module_config('listings', 'similar_items');
		$filters = array(
			'type' => $operation_type,
			'id_country' => $listing['id_country'],
			'id_region' => $listing['id_region'],
			'id_city' => $listing['id_city'],
			'id_category' => $listing['id_category'],
			'property_type' => $listing['property_type'],
			'exclude_id' => $listing['id'],
			'active' => 1,
		);
		$similar_listings = $CI->Listings_model->get_listings_list($filters, 1, $similar_items);
		$similar_total = count($similar_listings);
		if(!$similar_total) return '';
		
		$CI->template_lite->assign('similar_listings', $similar_listings);
		$CI->template_lite->assign('similar_visible', 3);
		$CI->template_lite->assign('similar_total', $similar_total);	
		$CI->template_lite->assign('similar_rand', rand(10000, 90000));
		
		return $CI->template_lite->fetch('helper_similar_listings', 'user', 'listings');
	}
}

if(!function_exists('listings_pagination_block')){
	/**
	 * Return pagination block for current listing
	 * 
	 * @param array $listing listing data
	 * @return string
	 */
	function listings_pagination_block($listing){
		$CI = & get_instance();
		$CI->load->model('Listings_model');
		
		$prev_listing = $listing; 
		$next_listing = $listing;
		$current_page = 1;
		$total_pages = 1;
		
		$current_settings = isset($_SESSION['listings_pagination'])?$_SESSION['listings_pagination']:array();
		if(!isset($current_settings['order'])) $current_settings['order'] = 'modified';
		if(!isset($current_settings['order_direction'])) $current_settings['order_direction'] = 'DESC';
		if(!isset($current_settings['page'])) $current_settings['page'] = 1;
		if(!isset($current_settings['data'])) $current_settings['data'] = array();
		$items_on_page = $CI->pg_module->get_module_config('start', 'index_items_per_page');
		$operation_type = $CI->Listings_model->get_operation_type_by_id($listing['id_type']);
		if($operation_type == 'sale' && $listing['sold']) $operation_type = 'sold';
		if($current_settings['data']['type'] != $operation_type) $current_settings['data'] = array('type'=>$operation_type);
		
		if($current_settings['order_direction'] != 'ASC') $current_settings['order_direction'] = 'DESC';
		
		$listings_count = $CI->Listings_model->get_listings_count($current_settings['data']);
		if($listings_count > 1){
			$order_array = array();
			if($current_settings['order'] == 'default'){
				if(isset($current_settings['data']['city']) && !empty($current_settings['data']['city'])) $order_array['lift_up_city_date_end'] = 'DESC';
				if(isset($current_settings['data']['region']) && !empty($current_settings['data']['region'])) $order_array['lift_up_region_date_end'] = 'DESC';
				if(isset($current_settings['data']['country']) && !empty($current_settings['data']['country'])) $order_array['lift_up_country_date_end'] = 'DESC';
				$order_array['lift_up_date_end'] = $current_settings['order_direction'];
				
				if(!empty($current_settings['data']['keyword'])){
					$boolean_mode = false;
		
					if(isset($current_settings['data']['keyword_mode'])){
						$boolean_mode = true;
					}
					
					$keyword = trim($current_settings['data']['keyword']);
			
					$CI->load->model('Field_editor_model');
					$property_type_gid = $CI->Listings_model->get_field_editor_type(array('type'=>$current_settings['data']['type'], 'id_category'=>$current_settings['data']['id_category']));
					$CI->Field_editor_model->initialize($property_type_gid);
					$temp_criteria = $CI->Field_editor_model->return_fulltext_criteria($keyword, $boolean_mode);
					$field_editor_table = constant('LISTINGS_'.strtoupper($property_type_gid).'_TABLE');
					$fe_table_name = substr($field_editor_table, strlen(DB_PREFIX));
					if(!empty($temp_criteria[$fe_table_name]['field'])){
						$order_array[str_replace(array('search_field', ' AS score'), array($field_editor_table.'.search_field', ''), $temp_criteria[$fe_table_name]['field'])] = 'DESC';
					}else{
						$order_array['date_created'] = $current_settings['order_direction'];
					}
				}else{
					$order_array['date_created'] = $current_settings['order_direction'];
				}
			}elseif($current_settings['order'] == 'price'){
				$order_array['price_sorting'] = $current_settings['order_direction'];
			}elseif($current_settings['order'] == 'open'){
				$order_array['date_open'] = $current_settings['order_direction'];
			}elseif($current_settings['order'] == 'modified'){
				$order_array['date_modified'] = $current_settings['order_direction'];
			}else{
				$order_array[$current_settings['order']] = $current_settings['order_direction'];
			}
			if($current_settings['order'] == 'price' || isset($current_settings['data']['price_range'])){
				$order_array = array_merge(array('price_negotiated'=>$current_settings['order_direction'] == 'ASC' ? 'DESC' : 'ASC'), $order_array);
			}

			$listings = $CI->Listings_model->get_listings_pagination($current_settings['data'], $current_settings['page'], $items_on_page, $order_array);
			$pages_count = ceil($listings_count/$items_on_page);
			$count = count($listings);
			foreach($listings as $i=>$data){
				if($data['id'] == $listing['id']){		
					$page = $current_settings['page'];
					$current_page = ($current_settings['page']-1)*$items_on_page + $i + ($current_settings['page'] == 1 ? 1 : -1);
					$total_pages = $listings_count;			
			
					if($i > 1){
						$prev_listing = $CI->Listings_model->format_listing($listings[$i-1]);
					}elseif($current_settings['page'] > 1){
						$prev_listing = $CI->Listings_model->format_listing($listings[$i-1]);
						$page--;		
					}elseif($i > 0){
						$prev_listing = $CI->Listings_model->format_listing($listings[$i-1]);
					}
					
					if($i < $count-($pages_count > 1 ? 2 : 1)){
						$next_listing = $CI->Listings_model->format_listing($listings[$i+1]);
					}elseif($current_settings['page'] < $pages_count){				
						$next_listing = $CI->Listings_model->format_listing($listings[$i+1]);
						$page++;
					}elseif($current_settings['page'] > 1 && $current_settings['page'] == $pages_count && $i < $count-1){
						$next_listing = $CI->Listings_model->format_listing($listings[$i+1]);
					}
			
					if($page != $current_settings['page']){
						$current_settings['page'] = $page;
						$_SESSION['listings_pagination'] = $current_settings;
					}
					break;
				}
			}
		}

		$CI->template_lite->assign('prev_listing', $prev_listing); 
		$CI->template_lite->assign('next_listing', $next_listing);
		$CI->template_lite->assign('current_page', $current_page);
		$CI->template_lite->assign('total_pages', $total_pages);
		
		return $CI->template_lite->fetch('helper_listings_pagination', 'user', 'listings');
	}
}

if(!function_exists('listings_booking_block')){
	/**
	 * Return booking form of listing
	 * 
	 * @param array $params block data
	 * @return string
	 */
	function listings_booking_block($params){
		$CI = & get_instance();
	
		if(!isset($params['listing']) || $params['listing']['operation_type'] != 'rent' || 
		   !$params['listing']['use_calendar']) return '';
		
		static $is_login_enabled = null;
		if(is_null($is_login_enabled)){
			$CI->load->helper('users');
			$is_login_enabled = is_login_enabled();
		}
		if(!$is_login_enabled) return '';
		
		if($CI->session->userdata('auth_type') == 'user'){
			$user_id = $CI->session->userdata('user_id');
			if($params['listing']['id_user'] == $user_id) return '';
		}else{
			$CI->template_lite->assign('is_guest', 1);
		}
	
		$CI->template_lite->assign('booking_listing', $params['listing']);
		$CI->template_lite->assign('template', $params['template']);
	
		if(isset($params['show_price'])) $CI->template_lite->assign('show_price', $params['show_price']);
		if(isset($params['show_link'])) $CI->template_lite->assign('show_link', $params['show_link']);
		if(isset($params['no_save'])) $CI->template_lite->assign('noSavePeriod', $params['no_save']);

		if($params['template'] != 'button' && $params['template'] != 'icon' && $params['template'] != 'link'){
			$page_data['date_format'] = $CI->pg_date->get_format('date_literal', 'st');
			$CI->template_lite->assign('date_time_format', $CI->pg_date->get_format('date_time_literal', 'st'));
			$CI->template_lite->assign('datepicker_date_format', $CI->pg_date->get_format('date_literal', 'ui'));
			$CI->template_lite->assign('datepicker_alt_format', $CI->pg_date->get_format('date_numeric', 'ui'));
		
			switch($params['listing']['price_period']){
				case 1:
					$date_start = date('Y-m-d');
					$date_end = date('Y-m-d', strtotime($date_start));
					
					$CI->template_lite->assign('current_date_start', $date_start);
					$CI->template_lite->assign('current_date_end', $date_end);
				break;
				case 2:
					$month_start = date('m');
					$year_start = date('Y');
			
					$month_end = ($month_start+max($params['listing']['calendar_period_min'], 1)-1)%12;
					$year_end = $year_start + intval(($month_start+max($params['listing']['calendar_period_min'], 1)-2)/12);
			
					if(!$month_end) $month_end = 12;
			
					$CI->template_lite->assign('current_month_start', $month_start);
					$CI->template_lite->assign('current_year_start', $year_start);
				
					$CI->template_lite->assign('current_month_end', $month_end);
					$CI->template_lite->assign('current_year_end', $year_end);
				
					$date_start = $year_start.'-'.$month_start.'-01';
					$date_end = $year_end.'-'.$month_end.'-01';
				break;
			}
		
			$CI->load->model('listings/models/Listings_booking_model');
			$price = $CI->Listings_booking_model->get_period_price($params['listing'], $date_start, $date_end);
			$CI->template_lite->assign('current_price', $price);
		}
		
		if(isset($params['separator'])) $CI->template_lite->assign('separator', $params['separator']);
		
		$CI->template_lite->assign('rand', rand(100000, 999999));
		$CI->load->plugin("captcha");
		$CI->config->load("captcha_settings");
		$captcha_settings = $CI->config->item("captcha_settings");
		$captcha = create_captcha($captcha_settings);
		$CI->template_lite->assign("captcha_word", $captcha["word"]);
		$data["captcha_image"] = $captcha["image"];
		$data["captcha_word_length"] = strlen($captcha["word"]);
		$CI->template_lite->assign("data", $data);
		return $CI->template_lite->fetch('helper_booking_block', 'user', 'listings');
	}
	
	/**
	 * Return calendar of listings periods
	 * 
	 * @param array $params block parameters
	 * @param boolean $only_block block without tables of periods
	 * @return string
	 */
	function listings_calendar_block($params, $only_block=false){
		$CI = & get_instance();
		
		if(!isset($params['listing'])) return '';
		
		$listing = $params['listing'];
		if(empty($params['count'])) $params['count'] = 1;
		if(empty($params['year'])) $params['year'] = 'Y';
		if(empty($params['month'])) $params['month'] = 'm';
		if(empty($params['template'])) $params['temlate'] = 'view';
	
		switch($CI->session->userdata('auth_type')){
			case 'admin':
				$edit = 1;
			break;
			case 'user':
				$user_id = $CI->session->userdata('user_id');
				if($listing['id_user'] == $user_id){
					$edit = 1;
				}else{
					$edit = 0;
				}
			break;
			default:
				$edit = 0;
			break;
		}
		
		$CI->load->helper('start');
		
		if($listing['price_negotiated']){
			$price_default = '<span>'.l('text_booking_price_unknown', 'listings').'</span>';
		}elseif($listing['price_reduced'] > 0){
			$price_default = currency_format_output(array('value'=>$listing['price_reduced'], 'cur_gid'=>$listing['gid_currency']));
		}else{
			$price_default = currency_format_output(array('value'=>$listing['price'], 'cur_gid'=>$listing['gid_currency']));
		}
		
		$template = $params['template'];
		$l = $params['count'];

		$time = strtotime(date($params['year'].'-'.$params['month'].'-01'));

		$m = date('m', $time);
		$CI->template_lite->assign('m', $m);
		
		$y = date('Y', $time);
		$CI->template_lite->assign('y', $y);
		
		$params = array(
			'id_listing'=>$listing['id'], 
			'status != '=>'wait',
		);
		if($listing['price_period'] == 2){
			
			$listing['price_period']=1;
		}
		switch($listing['price_period']){
			case 1:
				$params['date_start >='] = $y.'-'.$m.'-01 00:00:00';
				$params['date_end <'] = intval($y+($m+$l)/12).'-'.intval(($m+$l)%12+1).'-01 00:00:00';
			break;
			case 2:
				$params['date_start >='] = $y.'-01-01 00:00:00';
				$params['date_end <'] = ($y+$l+1).'-01-01 00:00:00';
			break;
		}

		$periods = isset($listing['booking']['periods']) ? $listing['booking']['periods'] : array();

		switch($listing['price_period']){
			case 1:
				$status = 'open';
				$now = strtotime('now 00:00:00');
				$months = array();
				for($j = 0; $j < $l; $j++){
					$t = date('t', $time);
					$w = date('w', $time);
					if(!$w) $w = 7;
			
					$weeks = ld('weekday-names-short', 'start');
					$weeks = $weeks['option'];
		
					$lang = $CI->pg_language->get_lang_by_id($CI->pg_language->current_lang_id);
					if($lang['code'] != 'ru'){
						array_unshift($weeks, array_pop($weeks));
						$w++;
						$w %= 8;
						if(!$w) $w = 1;
					}
					$CI->template_lite->assign('weeks', $weeks);
					
					$month_number = date('m', $time);
					$name = ld_option('month-names', 'start', $month_number);
					$month = array('name'=>$name, 'weeks'=>array());
			
					if($w > 1){
						$status = $time < $now ? 'close' : $status;
						$status = '';
						$days = array_fill(0, $w - 1, array('day'=>'', 'price'=>'', 'status'=>$status, 'comment'=>'', 'start'=>false, 'end'=>false));
					}else{
						$days = array();
					}
			
					for($i = 0; $i < $t; $i++){
						$price = $price_default;
				
						$status = 'open';
						$comment = '';
						$start = false;
						$end = false;
						foreach($periods as $period){
							if($period['status'] == 'wait' || $period['status'] == 'decline') continue;
							if($time+$i*86400 >= strtotime($period['date_start']) && $time+$i*86400 <= strtotime($period['date_end'])){
								if($period['price'] > 0){
									$price = currency_format_output(array('value'=>$period['price']*1, 'cur_gid'=>$listing['gid_currency']));
								}else{
									$price = '<span>'.l('text_booking_price_unknown', 'listings').'</span>';
								}	
								$status = $period['status'];
								if(!$edit) continue;
								if($time+$i*86400 == strtotime($period['date_start'])){
									$comment = $period['comment'];
									$start = true;
									
								}
								if($time+$i*86400 == strtotime($period['date_end'])){
									$end = true;
								}
								break;
							}
						}
	
						$status = $time+$i*86400 < $now ? 'close' : $status;
				
						$days[] = array('day'=>$i + 1, 'price'=>$price, 'status'=> $status, 'comment'=>$comment, 'start'=>$start, 'end'=>$end);
						$w %= 7;
						if($w == 0){
							$month['weeks'][] = $days;
							$month['weeks_status'][] = $status;
							$days = array();
						}
						$w++;
					}
			
					$time += $t*86400;
					if($month_number == 10) $time += 86400;
			
					if(!empty($days)){
						//$status = $time < $now ? 'close' : $status;
						$status = '';
						$month['weeks'][] = array_pad($days, 7, array('day'=>'', 'price'=>'', 'status'=>$status, 'comment'=>'', 'start'=>false, 'end'=>false));
						$month['weeks_status'][] = $status;
					}
	
					$month['weeks_count'] = count($month['weeks']);
			
					$months[] = $month;
				}
	
				$CI->template_lite->assign('months', $months);
				
				$years = array();
				
				for($i=0; $i < $l; $i++){
					$years[] = intval($y+($m+$i-1)/12); 
				}
				$CI->template_lite->assign('years', implode('/', array_unique($years)));
			break;
			case 2:
				$now = strtotime($y.'-'.$m.'-01');
				$years = array();
				for($j = 0; $j < $l; $j++){
					$year = array();
					for($i=0; $i<12; $i++){
						$price = $price_default;
						
						$time = strtotime(($y+$j).'-'.($i+1).'-01');
				
						$status = 'open';
						$comment = '';
						$start = false;
						$end = false;
						foreach($periods as $period){
							if($period['status'] == 'wait' || $period['status'] == 'decline') continue;
							if($time >= strtotime($period['date_start']) && $time <= strtotime($period['date_end'])){
								if($period['price'] > 0){
									$price = currency_format_output(array('value'=>$period['price']*1, 'cur_gid'=>$listing['gid_currency']));
								}else{
									$price = '<span>'.l('text_booking_price_unknown', 'listings').'</span>';
								}	
								$status = $period['status'];
								if(!$edit) continue;
								if($time == strtotime($period['date_start'])){
									$comment = $period['comment'];
									$start = true;
									
								}
								if($time == strtotime($period['date_end'])){
									$end = true;
								}
								break;
							}
						}
				
						$status = $time < $now ? 'close' : $status;
						
						$name = ld_option('month-names', 'start', $i+1);
						
						$year[] = array('price'=>$price, 'status'=>$status, 'month'=>$name, 'comment'=>$comment, 'start'=>$start, 'end'=>$end);
					}
					$years[$y+$j] = array_chunk($year, 4);
				}
				$CI->template_lite->assign('years', $years);
		
				$month_names = ld('month-names', 'start');
				$month_names = $month_names['option'];
				$month_names = array_chunk($month_names, 4);
				$CI->template_lite->assign('month_names', $month_names);
			break;
		}
		$CI->template_lite->assign('count', $l);
		
		$CI->template_lite->assign('listing', $listing);
		
		$CI->template_lite->assign('edit', $edit);
		
		$CI->template_lite->assign('rand', rand(100000, 999999));
		
		if($only_block){
			return $CI->template_lite->fetch('calendar_block', null, 'listings');
		}
		
		if(isset($listing['gid_currency']) && !empty($listing['gid_currency'])){
			if($CI->pg_module->is_module_installed('payments')){
				$CI->load->model('payments/models/Payment_currency_model');
				$current_price_currency = $CI->Payment_currency_model->get_currency_by_gid($listing['gid_currency']);
				$CI->template_lite->assign('current_price_currency', $current_price_currency);
			}
		}
			
		$CI->template_lite->assign('current_lang_id', $CI->pg_language->current_lang_id);
		$CI->template_lite->assign('langs', $CI->pg_language->languages);
		
		$page_data['date_format'] = $CI->pg_date->get_format('date_literal', 'st');
		$page_data['date_time_format'] = $CI->pg_date->get_format('date_time_literal', 'st');
		$page_data['datepicker_date_format'] = $CI->pg_date->get_format('date_literal', 'ui');
		$page_data['datepicker_alt_format'] = $CI->pg_date->get_format('date_numeric', 'ui');
		$CI->template_lite->assign('page_data', $page_data);
		
		return $CI->template_lite->fetch('helper_calendar_block', null, 'listings');
	}
}

if(!function_exists('listing_select')){
	/**
	 * Return selector of listing objects
	 * 
	 * @param array $selected selected items
	 * @param integer $max_select max selected items
	 * @param string $var_name name of variable
	 * @param integer $operation_type operation type
	 * @return string
	 */
	function listing_select($params=array()){
		
		$CI = & get_instance();
		$CI->load->model('Listings_model');

		$selected = isset($params['selected']) ? $params['selected'] : array();
		$max_select = isset($params['max']) ? $params['max'] : 0;
		$var_name = isset($params['var_name']) ? $params['var_name'] : 'id_listing';
		$template = isset($params['template']) ? $params['template'] : 'default';
		
		if(array_key_exists('selected', $params)) unset($params['selected']);

		if($max_select == 1 && !empty($selected) && !is_array($selected)){
			$selected = array($selected);
		}

		if(!empty($selected)){
			$filters['ids'] = $selected;
			if(isset($params['id_user'])) $filters['id_user'] = $params['id_user'];
			if(isset($params['operation_type'])) $filters['operation_type'] = $params['operation_type'];
			$data['selected'] = $CI->Listings_model->get_listings_list($filters, null, null, array());
			$data['selected_str'] = implode(',', $selected);
		}else{
			$data['selected_str'] = '';
		}
		
		if(isset($params['id_user'])){$data['id_user'] = $params['id_user'];}
		if(isset($params['operation_type'])){$data['operation_type'] = $params['operation_type'];}
		if(isset($params['callback'])){$data['callback'] = $params['callback']; unset($params['callback']);}
		
		$data['max_select'] = $max_select;		
		$data['var_name'] = $var_name;
		$data['template'] = $template;
		$data['params'] = $params;
		$data['rand'] = rand(100000, 999999);
	
		$CI->template_lite->assign('select_data', $data);
		return $CI->template_lite->fetch('helper_listing_select_'.$template, 'user', 'listings');
	}
}

if(!function_exists('save_listing_block')){
	/**
	 * Return content for moving listing to saved
	 * 
	 * @param array $params block data
	 * @return string
	 */
	function save_listing_block($params){
		$CI = & get_instance();
		
		if(!isset($params['listing'])) return '';
		
		static $is_login_enabled = null;
		if(is_null($is_login_enabled)){
			$CI->load->helper('users');
			$is_login_enabled = is_login_enabled();
		}
		if(!$is_login_enabled) return '';
		
		if($CI->session->userdata('auth_type') == 'user'){
			$user_id = $CI->session->userdata('user_id');
			if($params['listing']['id_user'] == $user_id) return '';
		}else{
			$CI->template_lite->assign('is_guest', 1);
		}
		
		$CI->template_lite->assign('listing', $params['listing']);
		
		$CI->template_lite->assign('template', $params['template']);
		if(isset($params['separator'])) $CI->template_lite->assign('separator', $params['separator']);
		
		$CI->template_lite->assign('rand', rand(100000, 999999));
	
		return $CI->template_lite->fetch('helper_save_block', 'user', 'listings');
	}
}

if(!function_exists('listings_slider_form')){
	/**
	 * Return listings slider with search form
	 * 
	 * @param array $type block type
	 * @param integer $count listings count
	 * @param string $view view mode
	 * @return string
	 */
	function listings_slider_form($type, $count, $view){
		$CI = & get_instance();
		$CI->load->model('Listings_model');
		
		$count = intval($count);
		if(!$count) $count = 8;
		
		$filters = array('active'=>1, 'slider_image'=>1);
		$order_by = array();
		
		switch($type){
			case 'featured':
				$filters['featured'] = 1;
				$order_by = array('RAND()'=>'');
			break;
			case 'latest_added':
				$order_by = array('date_created'=>'DESC');
			break;
			case 'sale':
				$filters['type'] = 'sale';
				$order_by = array('date_created'=>'DESC');
			break;
			case 'rent':
				$filters['type'] = 'rent';
				$order_by = array('date_created'=>'DESC');
			break;
		}
	
		$listings = $CI->Listings_model->get_listings_list($filters, 1, $count, $order_by);
		$CI->template_lite->assign('listings', $listings);
	
		$slider_form_block = $CI->Listings_model->get_slider_form('sale'); 
		$CI->template_lite->assign('slider_form_block', $slider_form_block);
		
		$slider_auto =  $CI->pg_module->get_module_config('listings', 'slider_auto');
		$slider_rotation =  $CI->pg_module->get_module_config('listings', 'slider_rotation');
		
		$page_data = array('count'=>count($listings), 'slider_auto'=>$slider_auto, 'rotation'=>$slider_rotation*1000);
		$CI->template_lite->assign('slider_form_page_data', $page_data);

		$CI->template_lite->assign('type', $type);
		$CI->template_lite->assign('view', $view);
	

		$CI->load->helper('start');
		$slider_search_form = main_search_form('sale', 'slider', true);
		$CI->template_lite->assign('slider_search_form', $slider_search_form);
		
		return $CI->template_lite->fetch('helper_listings_block_form_slider', 'user', 'listings');
	}
}

if(!function_exists('listings_slider')){
	/**
	 * Return listings slider without search form
	 * 
	 * @param array $type block type
	 * @param integer $count listings count
	 * @param string $view view mode
	 * @return string
	 */
	function listings_slider($type, $count, $view){
		$CI = & get_instance();
		$CI->load->model('Listings_model');
		
		$count = intval($count);
		if(!$count) $count = 8;
		
		$filters = array('active'=>1, 'slider_image'=>1);
		$order_by = array();
	
		switch($type){
			case 'featured':
				$filters['featured'] = 1;
				$order_by = array('RAND()'=>'');
			break;
			case 'latest_added':
				$order_by = array('date_created'=>'DESC');
			break;
			case 'sale':
				$filters['type'] = 'sale';
				$order_by = array('date_created'=>'DESC');
			break;
			case 'rent':
				$filters['type'] = 'rent';
				$order_by = array('date_created'=>'DESC');
			break;
		}
	
		$listings = $CI->Listings_model->get_listings_list($filters, 1, $count, $order_by);
		$CI->template_lite->assign('listings', $listings);
	
		$CI->load->model('Properties_model');
		$property_types = $CI->Properties_model->get_categories('property_types', $CI->pg_language->current_lang_id);
		$CI->template_lite->assign('property_types', $property_types);
		
		$slider_auto =  $CI->pg_module->get_module_config('listings', 'slider_auto');
		$slider_rotation =  $CI->pg_module->get_module_config('listings', 'slider_rotation');
		
		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => 5,
			'slider_auto' => $slider_auto,
			'rotation' => $slider_rotation*1000,
		);
		$CI->template_lite->assign('listings_page_data', $page_data);
		
		$CI->template_lite->assign('type', $type);
		$CI->template_lite->assign('view', $view);
		
		return $CI->template_lite->fetch('helper_listings_block_slider', 'user', 'listings');
	}
}

if(!function_exists('listings_featured')){
	/**
	 * Return featured listings block
	 * 
	 * @param integer $count listings count
	 * @param strign $size photo size
	 * @param string $view view mode
	 * @param integer $width block width
	 * @return string
	 */
	function listings_featured($count, $size, $view, $width=100){
		$CI = & get_instance();
		$CI->load->model('Listings_model');
		
		$count = intval($count);
		if(!$count) $count = 8;
		
		$filters = array('active'=>1, 'featured'=>1);
		$listings = $CI->Listings_model->get_listings_list($filters, 1, $count, array('RAND()'=>''));
		$CI->template_lite->assign('listings', $listings);
		$CI->template_lite->assign('type', 'featured');
		$CI->template_lite->assign('photo_size', $size);
		
		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => round(($view == 'scroller' ? 2.5 : 4)*$width/100),
		);
		$CI->template_lite->assign('listings_page_data', $page_data);
		
		return $CI->template_lite->fetch('helper_listings_block_'.$view, 'user', 'listings');
	}
}

if(!function_exists('listings_last_added')){
	/**
	 * Return last added listings block
	 * 
	 * @param integer $count listings count
	 * @param strign $size photo size
	 * @param string $view view mode
	 * @param integer $width block width
	 * @return string
	 */
	function listings_last_added($count, $size, $view, $width=100){
		$CI = & get_instance();
		$CI->load->model('Listings_model');
		
		$count = intval($count);
		if(!$count) $count = 8;
		
		$filters = array('active'=>1);
		
		$listings = $CI->Listings_model->get_listings_list($filters, 1, $count, array('date_created'=>'DESC'));
		$CI->template_lite->assign('listings', $listings);
		$CI->template_lite->assign('type', 'latest_added');
		$CI->template_lite->assign('photo_size', $size);

		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => round(($view == 'scroller' ? 2.5 : 4)*$width/100),
		);
		$CI->template_lite->assign('listings_page_data', $page_data);

		return $CI->template_lite->fetch('helper_listings_block_'.$view, 'user', 'listings');
	}
}

if(!function_exists('listings_sale')){
	/**
	 * Return listings for sale block
	 * 
	 * @param integer $count listings count
	 * @param strign $size photo size
	 * @param string $view view mode
	 * @param integer $width block width
	 * @return string
	 */
	function listings_sale($count, $size, $view, $width=100){
		$CI = & get_instance();
		$CI->load->model('Listings_model');

		//$count = intval($params['count']);
		if(!$count) $count = 8;
		
		$filters = array('type'=>'sale', 'active'=>1);
		$listings = $CI->Listings_model->get_listings_list($filters, 1, $count, array('date_created'=>'DESC'));
		$CI->template_lite->assign('listings', $listings);
		$CI->template_lite->assign('type', 'sale');
		$CI->template_lite->assign('photo_size', $size);
		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => round(($view == 'scroller' ? 2.5 : 4)*$width/100),
		);
		
		$CI->template_lite->assign('listings_page_data', $page_data);
		return $CI->template_lite->fetch('helper_listings_block_'.$view, 'user', 'listings');
	}
}

if(!function_exists('listings_buy')){
	/**
	 * Return listings for buy block
	 * 
	 * @param integer $count listings count
	 * @param strign $size photo size
	 * @param string $view view mode
	 * @param integer $width block width
	 * @return string
	 */
	function listings_buy($count, $size, $view, $width=100){
		$CI = & get_instance();
		$CI->load->model('Listings_model');

		//$count = intval($params['count']);
		if(!$count) $count = 8;

		$filters = array('type'=>'buy', 'active'=>1);
		
		$listings = $CI->Listings_model->get_listings_list($filters, 1, $count, array('date_created'=>'DESC'));
		$CI->template_lite->assign('listings', $listings);
		$CI->template_lite->assign('type', 'buy');
		$CI->template_lite->assign('photo_size', $size);
		
		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => round(($view == 'scroller' ? 2.5 : 4)*$width/100),
		);
		$CI->template_lite->assign('listings_page_data', $page_data);
		
		return $CI->template_lite->fetch('helper_listings_block_'.$view, 'user', 'listings');
	}
}

if(!function_exists('listings_rent')){
	/**
	 * Return listings for rent block
	 * 
	 * @param integer $count listings count
	 * @param strign $size photo size
	 * @param string $view view mode
	 * @param integer $width block width
	 * @return string
	 */
	function listings_rent($count, $size, $view, $width=100){
		$CI = & get_instance();
		$CI->load->model('Listings_model');

		//$count = intval($params['count']);
		if(!$count) $count = 8;
		
		$filters = array('type'=>'rent', 'active'=>1);
		$listings = $CI->Listings_model->get_listings_list($filters, 1, $count, array('date_created'=>'DESC'));
		$CI->template_lite->assign('listings', $listings);
		$CI->template_lite->assign('type', 'rent');
		$CI->template_lite->assign('photo_size', $size);
		//print_r($listings);
		
		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => round(($view == 'scroller' ? 2.5 : 4)*$width/100),
		);
		
		$CI->template_lite->assign('listings_page_data', $page_data);
		return $CI->template_lite->fetch('helper_listings_block_'.$view, 'user', 'listings');
	}
}

if(!function_exists('listings_lease')){
	/**
	 * Return listings for lease block
	 * 
	 * @param integer $count listings count
	 * @param strign $size photo size
	 * @param string $view view mode
	 * @param integer $width block width
	 * @return string
	 */
	function listings_lease($count, $size, $view, $width=100){
		$CI = & get_instance();
		$CI->load->model('Listings_model');

		$count = intval($params['count']);
		if(!$count) $count = 8;
		
		$filters = array('type'=>'lease', 'active'=>1);
		$listings = $CI->Listings_model->get_listings_list($filters, 1, $count, array('date_created'=>'DESC'));
		$CI->template_lite->assign('listings', $listings);
		$CI->template_lite->assign('type', 'lease');
		$CI->template_lite->assign('photo_size', $size);
		
		$page_data = array(
			'rand' => rand(100000, 999999),
			'count' => count($listings),
			'visible' => round(($view == 'scroller' ? 2.5 : 4)*$width/100),
		);
		$CI->template_lite->assign('listings_page_data', $page_data);
		
		return $CI->template_lite->fetch('helper_listings_block_'.$view, 'user', 'listings');
	}
}

if(!function_exists('listings_categories')){
	/**
	 * Return listings categories block
	 * 
	 * @param string $type block type
	 * @param integer $columns columns count
	 * @param string $view view mode
	 * @return string
	 */
	function listings_categories($type, $columns=3, $view=''){
		$CI = & get_instance();
		
		$CI->load->model('Properties_model');
		$property_types = $CI->Properties_model->get_categories('property_types', $CI->pg_language->current_lang_id);
		$property_items = $CI->Properties_model->get_all_properties('property_types', $CI->pg_language->current_lang_id);
		$CI->template_lite->assign('property_types', $property_types);
		
		$CI->load->model('listings/models/Listings_stat_model');

		$i = 0;
		$max = 0;
		$items = array();
		$stat_gids = array();
		foreach($property_types as $property_type_gid=>$property_type_name){
			$items[$i] = array();
			$items[$i][] = array('name'=>$property_type_name, 'gid'=>'property_type_'.$property_type_gid, 'group'=>true, 'empty'=>false);
			$max = max($max, count($property_items[$property_type_gid]));
			foreach($property_items[$property_type_gid] as $subtype_gid=>$subtype_name){
				$items[$i][] = array(
					'name'=>$subtype_name, 
					'gid'=>'property_type_'.$property_type_gid.'_'.$subtype_gid, 
					'data'=>array(
						'operation_type'=>$type, 
						'id_category'=>$property_type_gid, 
						'category'=>$property_type_name,
						'property_type'=>$subtype_gid, 
						'property'=>$subtype_name,
						'order'=>'default',
						'order_direction'=>'DESC',
						'page'=>1,
					),
					'group'=>false, 
					'empty'=>false
				);
				$stat_gids[] = 'property_type_'.$property_type_gid.'_'.$subtype_gid;
			}
			$stat_gids[] = 'property_type_'.$property_type_gid;
			$i++;
		}
		$stats = $CI->Listings_stat_model->get_stat_by_gids($stat_gids);		
		foreach($items as $i=>$item){
			foreach($item as $index => $value){
				$item[$index]['statistics'] = array('listings_active'=>isset($stats[$item[$index]['gid']][$type.'_cnt']) ? $stats[$item[$index]['gid']][$type.'_cnt'] : 0);				
			}
			$items[$i] = array_pad($item, $max, array('name'=>'&nbsp;', 'group'=>false, 'empty'=>true));
		}
		
		$block_view = 'property_types';

		//print_r($items);
		$CI->template_lite->assign('listings_categories', $items);
		
		$CI->template_lite->assign('block_view', $block_view);

		$categories_count = count($listings_categories);
		$CI->template_lite->assign('listings_categories_count', $categories_count);

		$columns = intval($columns);
		if(!$columns) $columns = 3;
		$CI->template_lite->assign('columns', $columns);

		$listings_per_column = ceil($categories_count/$columns);
		$CI->template_lite->assign('listings_per_column', $listings_per_column);
		
		$CI->template_lite->assign('type', $type);

		return $CI->template_lite->fetch('helper_categories_search', 'user', 'listings');
	}
}

if(!function_exists('listings_regions')){
	/**
	 * Return listings regions block
	 * 
	 * @param string $type block type
	 * @param integer $columns columns count
	 * @param string $view view mode
	 * @return string
	 */
	function listings_regions($type, $columns=3, $view=''){
		$CI = & get_instance();
		
		$listings_regions = array();
		
		$CI->load->helper('seo');
		
		$CI->load->model('Countries_model');
		$CI->load->model('listings/models/Listings_stat_model');
		
		$stat_gids = array();
		
		$locations = $CI->Countries_model->get_countries(null, array(), array(), $CI->pg_language->current_lang_id);
		if(count($locations) == 1){
			$country = current($locations);
			$country_name = $country['name'];
			$locations = $CI->Countries_model->get_regions($country['code'], null, array(), array(), $CI->pg_language->current_lang_id);
			if(count($locations) == 1){
				$region = current($locations);
				$region_name = $region['name'];
				$locations = $CI->Countries_model->get_cities(1, 30, array(), array(), array(), $CI->pg_language->current_lang_id);
				foreach($locations as $location){
					$stat_gids[] = 'city_'.$location['id'];
				}
				$gid_prefix = 'city_';
				$field_name = 'id';
				$template_lite->assign('field_name', 'id_city');
			}else{
				foreach($locations as $location){
					$stat_gids[] = 'region_'.$location['id'];
				}
				$gid_prefix = 'region_';
				$field_name = 'id';
				$CI->template_lite->assign('field_name', 'id_region');
			}
		}else{
			foreach($locations as $location){
				$stat_gids[] = 'country_'.strtolower($location['code']);
			}
			$gid_prefix = 'country_';
			$field_name = 'code';
			$CI->template_lite->assign('field_name', 'id_country');
		}
		
		if(empty($locations)) return '';
	
		$stats = $CI->Listings_stat_model->get_stat_by_gids($stat_gids);	
	
		if(!function_exists('_cmp_locations')){
			function _cmp_locations($a, $b){
				return strcmp($a['name'], $b['name']);
			}
		}
		
		usort($locations, _cmp_locations);
		$max = ceil(count($locations)/3)*3;
		for($i = 0; $i < $max; $i++){
			if(isset($locations[$i])){
					if(isset($locations[$i]['country_code'])){
						$country = $locations[$i]['country_code'];
						if(isset($locations[$i]['id_region'])){
							$region = $locations[$i]['id_region'];
							$city_name = $locations[$i]['name'];
						}else{
							$region = $locations[$i]['id'];
							$city_name = 'all';
						}
					}else{
						$country = $locations[$i]['code'];
						$country_name = $locations[$i]['name'];
						$region = 0;
						$region_name = 'all';
						$city = 0;
						$city_name = 'all';
					}
				
					$listings_regions[] =array(
						'id'=>$locations[$i][$field_name],
						'name'=>$locations[$i]['name'],
						'data'=>array(
							'operation_type'=>$type, 
							'id_country'=>$country,
							'country'=>$country_name,
							'id_region'=>$region,
							'region'=>$region_name,
							'id_city'=>$city,
							'city'=>$city_name,
							'order'=>'default',
							'order_direction'=>'DESC',
							'page'=>1,
						),
						'statistics' => array(
							'listing_active' => isset($stats[$gid_prefix.strtolower($locations[$i][$field_name])][$type.'_cnt']) ? $stats[$gid_prefix.strtolower($locations[$i][$field_name])][$type.'_cnt'] : 0,
						), 
						'empty' => false, 
						'group'=> false,
					);
				}else{
					$listings_regions[] = array('name'=>'&nbsp;', 'empty' => true, 'group'=> false);
				}
		}
	
		$CI->template_lite->assign('listings_regions', $listings_regions);
		
		$regions_count = count($listings_regions);
		$CI->template_lite->assign('listings_regions_count', $regions_count);

		$columns = !empty($params['columns'])?intval($params['columns']):3;
		$CI->template_lite->assign('columns', $columns);

		$listings_per_column = ceil($regions_count/$columns);
		$CI->template_lite->assign('listings_per_column', $listings_per_column);

		$CI->template_lite->assign('type', $type);

		return $CI->template_lite->fetch('helper_regions_search', 'user', 'listings');
	}
}

if(!function_exists('listings_wish_lists')){
	/**
	 * Return wish lists block
	 * 
	 * @param integer $count wish lists count
	 * @param string $view view mode
	 * @return string
	 */
	function listings_wish_lists($count, $view=''){
		$CI = & get_instance();
		$CI->load->model('listings/models/Wish_list_model');
		
		$count = intval($count);
		if(!$count) $count = 4;
		$wish_lists = $CI->Wish_list_model->get_wish_lists_list(array('active'=>1), 1, $count, array('RAND()'=>''));
		$CI->template_lite->assign('wish_lists', $wish_lists);
		return $CI->template_lite->fetch('helper_wish_lists', 'user', 'listings');
	}
}

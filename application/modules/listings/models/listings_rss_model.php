<?php

define('LISTINGS_RSS_TABLE', DB_PREFIX.'listings_rss_subscriptions');

/**
 * Listings RSS Model
 * 
 * @package PG_RealEstate
 * @subpackage Listings
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Listings_rss_model extends Model{
	
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;

	/**
	 * Link to DataBase object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Listing RSS fields in data source
	 * 
	 * @var array
	 */
	private $_fields = array(
		'id',
		'gid',
		'id_user',
		'data',
		'email',
		'last_use',
	);
	
	/**
	 * Constructor
	 *
	 * @return Listings_rss_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}
	
	/**
	 * Return rss data from data source by identifier
	 * 
	 * @param integer $rss_id record identifier
	 * @return array/false
	 */
	public function get_rss_by_id($rss_id){
		$result = $this->DB->select(implode(', ', $this->_fields))
						   ->from(LISTINGS_RSS_TABLE)
						   ->where('id', $rss_id)
						   ->get()->result_array();
		if(empty($result)) return false;
		$result = $this->format_rss($result);
		return $result[0];
	}
	
	/**
	 * Return rss data from data source by guid 
	 * 
	 * @param string $rss_gid record guid
	 * @return array/false
	 */
	public function get_rss_by_gid($rss_gid){
		$result = $this->DB->select(implode(', ', $this->_fields))
						   ->from(LISTINGS_RSS_TABLE)
						   ->where('gid', $rss_gid)
						   ->get()->result_array();
		if(empty($result)) return false;
		$result = $this->format_rss($result);
		return $result[0];
	}
	
	/**
	 * Save object of rss link to data source
	 * 
	 * @param integer $rss_id rss identifier
	 * @param array $data rss data
	 * @return integer
	 */
	public function save_rss($rss_id, $data=array()){
		if(!isset($data['last_use'])) $data['last_use'] = date('Y-m-d H:i:s');
		if(!$rss_id){
			if(!isset($data['gid'])) $data['gid'] = $this->generate_rss_gid(null, $data);
			$this->DB->insert(LISTINGS_RSS_TABLE, $data);
			$rss_id = $this->DB->insert_id();
		}else{
			$this->DB->where('id', $rss_id);
			$this->DB->update(LISTINGS_RSS_TABLE, $data);
		}
		return $rss_id;
	}
	
	/**
	 * Generate GUID of listings rss link
	 * 
	 * @param integer $user_id user identifier
	 * @param array $data rss data
	 * @return string
	 */
	public function generate_rss_gid($user_id, $data){
		$this->CI->load->model('Listings_model');
		$operation_types = $this->CI->Listings_model->get_operation_types(true); 
		$operation_type = isset($data['type']) && in_array($data['type'], $operation_types) ?
			$data['type'] : current($operation_types);
		
		$property = $this->CI->Listings_model->get_field_editor_type($data);
		if($property){
			if(isset($data['property_type'])) $property .= $data['property_type'];
		}else{
			$property = 'all';
		}
		
		$user_id = intval($user_id);
		
		return md5(microtime() . $user_id . $property . $operation_type);
	}	
	
	/**
	 * Validate data of listings rss link for saving to data source
	 * 
	 * @param integer $rss_id rss identifier
	 * @param array $data data for validation
	 * @return array
	 */
	public function validate_rss($rss_id, $data){
		$return = array('errors'=>array(), 'data'=>array());
		
		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['gid'])){
			$return['data']['gid'] = trim(strip_tags($data['gid']));
			if(empty($return['data']['gid'])) unset($return['data']['gid']);
		}
		
		if(isset($data['id_user'])){
			$return['data']['id_user'] = intval($data['id_user']);
			if(empty($return['data']['id_user'])) unset($return['data']['id_user']);
		}
		
		if(isset($data['data'])){
			$return['data']['data'] = serialize($data['data']);
		}
		
		if(isset($data['email'])){
			$return['data']['email'] = trim(strip_tags($data['email']));
			if(empty($return['data']['email'])) unset($return['data']['email']);
		}
		
		if(isset($data['last_use'])){
			$value = strtotime($data['last_use']);
			if($value > 0){
				$return['data']['last_use'] = date('Y-m-d', $value);
			}else{
				$return['data']['last_use'] = '0000-00-00 00:00:00';
			}
		}

		return $return;
	}
	
	/**
	 * Format data of listing rss link
	 * 
	 * @params array $data rss data
	 * @return array
	 */
	public function format_rss($data){
		foreach($data as $key=>$value){
			$data[$key]['data'] = $value['data'] ? (array)unserialize($value['data']) : array();
		}
		return $data;
	}
}

<?php

define('LISTINGS_BOOKING_PERIODS_TABLE', DB_PREFIX.'listings_booking');
define('LISTINGS_BOOKING_CACHE_TABLE', DB_PREFIX.'listings_booking_cache');

/**
 * Listings Booking Model
 * 
 * @package PG_RealEstate
 * @subpackage Listings
 * @category models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Listings_booking_model extends Model{
	
	/**
	 * link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;

	/**
	 * link to DataBase object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Table fields in data source
	 * 
	 * @var array
	 */
	private $_fields = array(
		'id',
		'id_listing',
		'id_owner',
		'owner_name',
		'id_user',
		'status',
		'date_start',
		'date_end',
		'name',
		'phone',
		'mail',
		'price',
		'guests',
		'comment',
		'answer',
		'date_created',
		'date_modified',
	);
	
	/**
	 * Format settings of booking period
	 * 
	 * @var array
	 */
	private $format_settings = array(
		'use_format' => true,
		'get_user' => true,
		'get_listing' => false,
	);
	
	/**
	 * Available statuses for calendar period
	 * 
	 * @var array
	 */
	private $period_status_arr = array('open', 'close', 'book', 'wait', 'decline');
	
	/**
	 * Limit of searching items
	 * 
	 * @var integer
	 */
	private $_seacrh_items_limit = 10000;
	
	/**
	 * Cache of booking periods
	 * 
	 * @var array
	 */
	private $_booking_cache = array();
	
	/**
	 * Class constructor
	 *
	 * return Listings_booking_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}
	
	/**
	 * Return period object by identifier
	 * 
	 * @param integer $period_id period identifier
	 * @return array
	 */
	public function get_period_by_id($period_id){
		$this->DB->select(implode(', ', $this->_fields));
		$this->DB->from(LISTINGS_BOOKING_PERIODS_TABLE);
		$this->DB->where('id', $period_id);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return array_shift($this->format_periods(array($results[0])));
		}
		return array();
	}
	
	/**
	 * Save period object to data source
	 * 
	 * @param integer $period_id period identifier
	 * @param array $data period data
	 * @return integer
	 */
	public function save_period($period_id, $data){
		if(!$period_id){
			$this->CI->load->model('Listings_model');
			$data['status'] = 'open';	
			$this->DB->insert(LISTINGS_BOOKING_PERIODS_TABLE, $data);
			$period_id = $this->DB->insert_id();
		}else{
			$data['status'] = 'wait';
			$this->DB->where('id', $period_id);		
			$this->DB->update(LISTINGS_BOOKING_PERIODS_TABLE, $data);
		}
		return $period_id;
	}
	
	//for member booked order saving
	
	public function save_order($period_id, $data){
		if(isset($data['status'])){
			$this->CI->load->model('Listings_model');
			$data['date_modified'] = date('Y-m-d H:i:s');
			$data['status'] = 'book';	
			$this->DB->insert(LISTINGS_BOOKING_PERIODS_TABLE, $data);
			$period_id = $this->DB->insert_id();
		}else{
			$data['date_modified'] = date('Y-m-d H:i:s');
			$data['status'] = 'wait';
			$this->DB->where('id', $period_id);		
			$this->DB->insert(LISTINGS_BOOKING_PERIODS_TABLE, $data);
			$period_id = $this->DB->insert_id();
		}
		return $period_id;
	}
	/**
	 * Remove period object from data source
	 * 
	 * @param integer $period_id period identifier
	 * @return void
	 */
	public function delete_period($period_id){
		$this->set_format_settings('use_format', false);
		$period = $this->get_period_by_id($period_id);
		$this->set_format_settings('use_format', true);
		
		if(!$period) return;
		
		$this->DB->where('id', $period_id);
		$this->DB->delete(LISTINGS_BOOKING_PERIODS_TABLE);
		
		$this->CI->load->model('Listings_model');
		
		$this->CI->Listings_model->set_format_settings('use_format', false);
		$listing = $this->get_period_by_id($period['id_listing']);
		$this->CI->Listings_model->set_format_settings('use_format', true);
		
		$this->DB->set('status', 'free');
		$this->DB->where('id_listing', $period['id_listing']);
		$this->DB->where('id_user', $listing['id_user']);
		$this->DB->update(LISTINGS_BOOKING_PERIODS_TABLE);
	}
	
	/**
	 * Approve period object
	 * 
	 * @param integer $period_id period identifier
	 * @return void
	 */
	public function approve_period($period_id){
		$this->DB->set('status', 'book');
		$this->DB->where('id', $period_id);
		$this->DB->update(LISTINGS_BOOKING_PERIODS_TABLE);
	
		$this->CI->load->model('Users_model');
	
		$this->CI->Users_model->set_format_settings('get_safe', false);
		
		$this->set_format_settings('get_listing', true);
		$period = $this->get_period_by_id($period);
		$this->set_format_settings('get_listing', false);
		
		$this->CI->Users_model->set_format_settings('get_safe', true);
		
		$mail_data = array(
			'fname' => $period['listing']['user']['fname'],
			'sname' => $period['listing']['user']['sname'],
			'user' => $period['user']['output_name'],
			'name' => $period['listing']['output_name'], 
		);
		
		$this->CI->load->model('Notifications_model');	
		$this->CI->Notifications_model->send_notification($period['listing']['user']['email'], 'listing_booking_approve', $mail_data, '', $period['listing']['user']['lang_id']);
		
		$moderation_send_mail = $this->pg_module->get_module_config('start', 'moderation_send_mail');
		if($moderation_send_mail){
			$admin_moderation_emails = $this->pg_module->get_module_config('start', 'admin_moderation_emails');
			if($admin_moderation_emails){
				unset($mail_data['fname']);
				unset($mail_data['fname']);
				$this->CI->Notifications_model->send_notification($admin_moderation_emails, 'admin_booking_approve', $mail_data);
			}
		}
	}
	
	/**
	 * Decline period object
	 * 
	 * @param integer $period_id period identifier
	 * @return void
	 */
	public function decline_period($period_id){
		$this->DB->set('status', 'decline');
		$this->DB->where('id', $period_id);
		$this->DB->update(LISTINGS_BOOKING_PERIODS_TABLE);
		
		$this->CI->load->model('Users_model');
		
		$this->CI->Users_model->set_format_settings('get_safe', false);
		
		$this->set_format_settings('get_listing', true);
		$period = $this->get_period_by_id($period);
		$this->set_format_settings('get_listing', false);
		
		$this->CI->Users_model->set_format_settings('get_safe', true);
		
		$mail_data = array(
			'fname' => $period['listing']['user']['fname'],
			'sname' => $period['listing']['user']['sname'],
			'user' => $period['user']['output_name'],
			'name' => $period['listing']['output_name'], 
		);
		
		$this->CI->load->model('Notifications_model');	
		$this->CI->Notifications_model->send_notification($period['listing']['user']['email'], 'listing_booking_decline', $mail_data, '', $period['listing']['user']['lang_id']);
		
		$moderation_send_mail = $this->pg_module->get_module_config('start', 'moderation_send_mail');
		if($moderation_send_mail){
			$admin_moderation_emails = $this->pg_module->get_module_config('start', 'admin_moderation_emails');
			if($admin_moderation_emails){
				unset($mail_data['fname']);
				unset($mail_data['fname']);
				$this->CI->Notifications_model->send_notification($admin_moderation_emails, 'admin_booking_decline', $mail_data);
			}
		}
	}
	
	/**
	 * Return search criteria by filters data
	 * 
	 * @param array $filters filters data
	 * @return array
	 */
	private function _get_search_criteria($filters){
		$params = array();
	
		// By keyword
		if(!empty($filters['keyword'])){
			$boolean_mode = false;
			
			if(isset($filters['keyword_mode'])){
				$boolean_mode = true;
				unset($filters['keyword_mode']);
			}
			
			$filters['keyword'] = trim($filters['keyword']);
			
			$fe_criteria = array();
			
			$this->CI->load->model('Listings_model');
			
			$this->CI->load->model('Field_editor_model');
			$property_type_gids = $this->Listings_model->get_field_editor_types();
			foreach($property_type_gids as $property_type_gid){
				$this->CI->Field_editor_model->initialize($property_type_gid);
				$temp_criteria = $this->CI->Field_editor_model->return_fulltext_criteria($filters['keyword'], $boolean_mode);
				$field_editor_table = constant('LISTINGS_'.strtoupper($property_type_gid).'_TABLE');
				$fe_table_name = substr($field_editor_table, strlen(DB_PREFIX));
				if(!empty($temp_criteria[$fe_table_name]['where_sql'])){
					$fe_criteria[] = str_replace('search_field', $field_editor_table.'.search_field', $temp_criteria[$fe_table_name]['where_sql']);
				}
			}
			if(!empty($fe_criteria)){
				$fe_criteria = array('where_sql'=>array('('.implode(' OR ', array_unique($fe_criteria)).')'));
				if(isset($filters['field_editor'])){
					$filters['field_editor'][0] = array_merge($filters['field_editor'][0], $property_type_gids);
					$filters['field_editor'][1] = array_merge($filters['field_editor'][1], $fe_criteria);
				}else{
					$filters['field_editor'] = array($property_type_gids, $fe_criteria);
				}
			}
		}
	
		$fields = array_flip($this->_fields);
		foreach((array)$filters as $filter_name=>$filter_data){
			if(!$filter_data) continue;
			switch($filter_name){
				// By ids
				case 'ids':
					if(is_array($filter_data)){
						$params = array_merge_recursive($params, array('where_in'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.id'=>$filter_data)));
					}else{
						$params = array_merge_recursive($params, array('where'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.id'=>$filter_data)));
					}
				break;
				// By ids
				case 'not_ids':
					if(is_array($filter_data)){
						$params = array_merge_recursive($params, array('where_not_in'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.id'=>$filter_data)));
					}else{
						$params = array_merge_recursive($params, array('where'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.id'=>'!= '.$filter_data)));
					}
				break;
				// By listings
				case 'listings':
					if(is_array($filter_data)){
						$params = array_merge_recursive($params, array('where_in'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.id_listing'=>$filter_data)));
					}else{
						$params = array_merge_recursive($params, array('where'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.id_listing'=>$filter_data)));
					}
				break;
				// By users
				case 'users':
					if(is_array($filter_data)){
						$params = array_merge_recursive($params, array('where_in'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.id_user'=>$filter_data)));
					}else{
						$params = array_merge_recursive($params, array('where'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.id_user'=>$filter_data)));
					}
				break;
				// By users
				case 'not_user':
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.id_user !='=>$filter_data)));
				break;
				// By owner
				case 'owners':
					if(is_array($filter_data)){
						$params = array_merge_recursive($params, array('where_in'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.id_owner'=>$filter_data)));
					}else{
						$params = array_merge_recursive($params, array('where'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.id_owner'=>$filter_data)));
					}
				break;
				// By users
				case 'not_owner':
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.id_owner !='=>$filter_data)));
				break;
				// By users
				case 'status':
					if(is_array($filter_data)){
						$params = array_merge_recursive($params, array('where_in'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.status'=>$filter_data)));
					}else{
						if($filter_data == 'approve') $filter_data = 'book';
						$params = array_merge_recursive($params, array('where'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.status'=>$filter_data)));
					}
				break;
				// By field editor
				case 'field_editor':
					if(empty($filter_data)) continue;
					foreach($filter_data[0] as $key=>$property_type_gid){
						$filter_data[0][$key] = constant('LISTINGS_'.strtoupper($property_type_gid).'_TABLE');
					}
					$filter_data[0] = array_unique($filter_data[0]);
					foreach($filter_data[0] as $key=>$field_editor_table){
						$filter_data[0][$key] = array('table'=>$field_editor_table, 'condition'=>LISTINGS_BOOKING_PERIODS_TABLE.'.id_listing='.$field_editor_table.'.id_listing', 'type'=>'LEFT');
					}
					$params = array_merge_recursive($params, array('join'=>$filter_data[0]), $filter_data[1]);
				break;
				// By field
				default:
					if(strpos($filter_name, ' ') !== false){
						$field_name = array_shift(explode(' ', $filter_name));
					}else{
						$field_name = $filter_name;
					}
					if(!isset($fields[$field_name])) break;
					$params = array_merge_recursive($params, array('where'=>array(LISTINGS_BOOKING_PERIODS_TABLE.'.'.$filter_name=>$filter_data)));	
				break;
			}
		}

		return $params;
	}
	
	/**
	 * Return periods objects in data source as array
	 * 
	 * @param integer $page page of results
	 * @param string $limits results limit
	 * @param array $order_by sorting data
	 * @param array $params sql criteria
	 * @return array
	 */
	private function _get_periods_list($page=null, $limits=null, $order_by=null, $params=array()){
		$this->DB->select(LISTINGS_BOOKING_PERIODS_TABLE.'.'.implode(', '.LISTINGS_BOOKING_PERIODS_TABLE.'.', $this->_fields));
		$this->DB->from(LISTINGS_BOOKING_PERIODS_TABLE);
		
		if(isset($params['join']) && is_array($params['join']) && count($params['join'])){
			foreach($params['join'] as $join){
				$this->DB->join($join['table'], $join['condition'], $join['type']);
			}
		}
		
		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}
		
		if(isset($params['where_not_in']) && is_array($params['where_not_in']) && count($params['where_not_in'])){
			foreach($params['where_not_in'] as $field=>$value){
				$this->DB->where_not_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value);
			}
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->_fields)){
					$this->DB->order_by($field.' '.$dir);
				}
			}
		}elseif($order_by){
			$this->DB->order_by($order_by);
		}
	
		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}

		$results = $this->DB->get()->result_array();	
		if(!empty($results) && is_array($results)){
			return $this->format_periods($results);
		}
		return array();
	}
	
	/**
	 * Return number of periods objects in data source
	 * 
	 * @param array $params sql criteria
	 * @return integer
	 */
	private function _get_periods_count($params=null){
		$this->DB->select('COUNT(*) AS cnt');
		$this->DB->from(LISTINGS_BOOKING_PERIODS_TABLE);

		if(isset($params['join']) && is_array($params['join']) && count($params['join'])){
			foreach($params['join'] as $join){
				$this->DB->join($join['table'], $join['condition'], $join['type']);
			}
		}

		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]['cnt']);
		}
		return 0;
	}
	
	/**
	 * Return list of filtered calendar periods
	 * 
	 * @param array $filters filter data
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param string $order_by sorting data
	 * @return array
	 */
	public function get_periods_list($filters=array(), $page=null, $items_on_page=null, $order_by=null){
		$params = $this->_get_search_criteria($filters);		
		return $this->_get_periods_list($page, $items_on_page, $order_by, $params);
	}
	
	/**
	 * Return number of filtered calendar periods
	 * 
	 * @param array $filters filter data
	 * @return array
	 */
	public function get_periods_count($filters=array()){
		$params = $this->_get_search_criteria($filters);
		return $this->_get_periods_count($params);
	}
	
	/**
	 * Return list of filtered calendar periods by key
	 * 
	 * @param array $filters filter data
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param string $order_by sorting data
	 * @return array
	 */
	public function get_periods_list_by_key($filters=array(), $page=null, $items_on_page=null, $order_by=null){
		$params = $this->_get_search_criteria($filters);		
		$periods_data = $this->_get_periods_list($page, $items_on_page, $order_by, $params);
		$return_data = array();
		foreach($periods_data as $data){
			if(!isset($return_data[$data['id_listing']])) $return_data[$data['id_listing']] = array();
			$return_data[$data['id_listing']][] = $data;
		}
		return $return_data;
	}
	
	/**
	 * Change format settings for calendar periods objects
	 * 
	 * @param string $name parameter name
	 * @param mixed $value parameter value
	 * @return void
	 */
	public function set_format_settings($name, $value=false){
		if(!is_array($name)) $name = array($name=>$value);
		if(empty($name)) return;
		foreach($name as $key => $item)	$this->format_settings[$key] = $item;
	}
	
	/**
	 * Validate calendar period object
	 * 
	 * @param integer $period_id period identifier
	 * @param array $data period data
	 * @param boolean $is_booking is booking
	 * @return array
	 */
	public function validate_period($period_id, $data, $is_booking=false){
		$return = array('errors'=>array(), 'data'=>array());

		if($period_id){
			$this->set_format_settings('use_format', false);
			$period = $this->get_period_by_id($period_id);
			$this->set_format_settings('use_format', true);
		}else{
			$period = array();
		}

		if(isset($data['id'])){
			$return['data']['id'] = intval($data['id']);
			if(empty($return['data']['id'])) unset($return['data']['id']);
		}
		
		if(isset($data['id_listing'])){
			$return['data']['id_listing'] = intval($data['id_listing']);
			if(empty($return['data']['id_listing'])){
				$return['errors'][] = l('error_empty_calendar_listing', 'listings');
			}else{
				$period['id_listing'] = $return['data']['id_listing'];
			}
		}elseif(!$period_id){
			$return['errors'][] = l('error_empty_calendar_listing', 'listings');
		}
		
		if(isset($data['id_user'])){
			$return['data']['id_user'] = intval($data['id_user']);
			$return['data']['id_owner'] = 0;
			if(empty($return['data']['id_user'])){
				$return['errors'][] = l('error_empty_calendar_user', 'listings');
			}else{
				$period['id_user'] = $return['data']['id_user'];
			}
		}elseif(!$period_id){
			$return['errors'][] = l('error_empty_calendar_user', 'listings');
		}
		
		if(isset($data['date_start'])){
			$value = strtotime($data['date_start']);
			if($value > 0){
				$period['date_start'] = $return['data']['date_start'] = date('Y-m-d', $value);
			}else{
				$return['errors'][] = l('error_empty_calendar_date_start', 'listings');
			}
		}elseif(!$period_id){
			$return['errors'][] = l('error_empty_calendar_date_start', 'listings');
		}
		
		if(isset($data['date_end'])){
			$value = strtotime($data['date_end']);
			if($value > 0){
				$period['date_end'] = $return['data']['date_end'] = date('Y-m-d', $value);
			}else{
				$return['errors'][] = l('error_empty_calendar_date_end', 'listings');
			}
		}elseif(!$period_id){
			$return['errors'][] = l('error_empty_calendar_date_end', 'listings');
		}
		
		if(isset($data['status'])){
			$return['data']['status'] = $data['status'];
			if(empty($return['data']['status'])){
				$return['errors'][] = l('error_empty_calendar_status', 'listings');
			}elseif(!in_array($data['status'], $this->period_status_arr)){
				$return['errors'][] = l('error_invalid_calendar_status', 'listings');
			}
		}
		
		if(isset($period['date_start']) && isset($period['date_end'])){
			if(strtotime($period['date_start']) > strtotime($period['date_end'])){
				$return['errors'][] = l('error_invalid_calendar_period', 'listings');
			}elseif(isset($period['id_listing']) && !empty($period['id_listing'])){
				$this->CI->load->model('Listings_model');
				$this->CI->Listings_model->set_format_settings('use_format', false);
				$listing = $this->CI->Listings_model->get_listing_by_id($period['id_listing']);
				$this->CI->Listings_model->set_format_settings('use_format', true);
			
				switch($listing['price_period']){
					case 1:
						$n = (strtotime($period['date_end']) - strtotime($period['date_start']))/86400 + 1;
					break;
					case 2:
						if(class_exists('DateTime')){
							$datetime = new DateTime($period['date_end']);
							$n = $datetime->diff(new DateTime($period['date_start']))->format('%m') + 1;
						}else{
							$n = ((strtotime($period['date_end']) - strtotime($period['date_start']))/(86400*30))+1;
						}
					break;
				}

				if($listing['calendar_period_min'] > 0 && $n < $listing['calendar_period_min']){
					$return['errors'][] = str_replace(array('[value]', '[unit]'), array($listing['calendar_period_min'], ld_option('price_period_unit', 'listings', $listing['price_period'])), l('error_calendar_period_min', 'listings'));
				}
				if($listing['calendar_period_max'] > 0 && $n > $listing['calendar_period_max']){
					$return['errors'][] =  str_replace(array('[value]', '[unit]'), array($listing['calendar_period_max'], ld_option('price_period_unit', 'listings', $listing['price_period'])), l('error_calendar_period_max', 'listings'));
				}

				if($return['data']['status'] == 'open'){
					$is_intersect = $this->is_intersect_exists_periods($period['id_listing'], $period['id_user'], $period['date_start'], $period['date_end'], $period_id);
					if($is_intersect){
						$return['errors'][] = l('error_intersect_calendar_periods', 'listings');
					}
				}else{
					$is_intersect = $this->is_intersect_exists_orders($period['id_listing'], 0, $period['date_start'], $period['date_end'], $period_id);
					if($is_intersect){
						$return['errors'][] = l('error_intersect_calendar_periods', 'listings');
					}
				}
			}
		}
		
		if(!isset($data['guests'])){
			$return['data']['guests'] = '0';
		}
		
		if(isset($data['comment'])){
			$return['data']['comment'] = trim(strip_tags($data['comment']));
		}
		
		if(isset($data['name'])){
			$return['data']['name'] = $data['name'];
		}
		
		if(isset($data['id_owner'])){
			$return['data']['id_owner'] = $data['id_owner'];
		}
		
		if(isset($data['owner_name'])){
			$return['data']['owner_name'] = $data['owner_name'];
		}
		if(isset($data['mail'])){
			$return['data']['mail'] = $data['mail'];
		}
		
		if(isset($data['answer'])){
			$return['data']['answer'] = trim(strip_tags($data['answer']));
		}
	   if(isset($data['date_start'])){
		$return['data']['date_created'] = $data['date_start'];
	   }
	    if(isset($data['date_modified'])){
        $return['data']['date_modified'] = date('Y-m-d H:i:s');
		}
		return $return;
	}
	
	/**
	 * Format data of calendar period object
	 * 
	 * @param array $data data set of calendar period
	 * @return array
	 */
	public function format_period($data){
		return array_shift($this->format_periods(array($data)));
	}
	
	/**
	 * Format data of calendar periods objects
	 * 
	 * @param array $data data set of calendar periods
	 * @return array
	 */
	public function format_periods($data){
		if(!$this->format_settings['use_format']){
			return $data;
		}
		
		$users_search = array();
		$listings_search = array();
		
		foreach($data as $key=>$period){
			$period['price'] *= 1;
			
			$period['status_str'] = ld_option('booking_status', 'listings', $period['status']);
			$period['guests_str'] = ld_option('booking_guests', 'listings', $period['guests']);
			
			if($period['id_user'] != $period['id_owner']){
				switch($period['status']){
					case 'book':
						$period['status_str'] = l('booking_status_approved', 'listings');
					break;
					case 'decline':
						$period['status_str'] = l('booking_status_declined', 'listings');
					break;
					case 'wait':
						$period['status_str'] = l('booking_status_wait', 'listings');
					break;
				}
			}
			
			//	get listing
			if($this->format_settings['get_listing']){
				$listings_search[] = $period['id_listing'];
			}
			
			//	get user
			if($this->format_settings['get_user']){
				$users_search[] = $period['id_user'];
			}
			
			$data[$key] = $period;
		}
	
		// get user
		if($this->format_settings['get_user'] && !empty($users_search)){
			$this->CI->load->model('Users_model');
			$users_data = $this->CI->Users_model->get_users_list_by_key(null, null, null, array(), $users_search);
			foreach($data as $key=>$period){
				$data[$key]['user'] = (isset($users_data[$period['id_user']])) ? 
					$users_data[$period['id_user']] : $this->CI->Users_model->format_default_user($period['id_user']);
			}
		}

		// get listing
		if($this->format_settings['get_listing'] && !empty($listings_search)){
			$this->CI->load->model('Listings_model');
			$users_data = $this->CI->Listings_model->get_listings_list_by_key(array('ids'=>$listings_search));
			foreach($data as $key=>$period){
				$data[$key]['listing'] = (isset($users_data[$period['id_listing']])) ? 
					$users_data[$period['id_listing']] : $this->CI->Users_model->format_default_user($period['id_listing']);
			}
		}
		
		return $data;
	}
	
	/**
	 * Check intersections booking order with exists orders
	 * 
	 * @param integer $listing_id listing identifier
	 * @param integer $user_id listing identifier
	 * @param string $date_start start date of period
	 * @param string $date_end end date of period
	 * @param integer $period_id period identifier
	 * @return boolean
	 */
	public function is_intersect_exists_orders($listing_id, $user_id, $date_start, $date_end, $period_id=null){
		$params = array();
		$params['where']['id_listing'] = $listing_id;
		$params['where']['date_start <='] = $date_end;
		$params['where']['date_end >='] = $date_start;
		$params['where']['status'] = 'book';
		if($user_id) $params['where']['id_user'] = $user_id;
		if($period_id) $params['where']['id !='] = $period_id;
		return $this->_get_periods_count($params) > 0;
	}
	
	/**
	 * Check intersections calendar period with exists periods
	 * 
	 * @param integer $listing_id listing identifier
	 * @param integer $user_id listing identifier
	 * @param string $date_start start date of period
	 * @param string $date_end end date of period
	 * @param integer $period_id period identifier
	 * @return boolean
	 */
	public function is_intersect_exists_periods($listing_id, $user_id, $date_start, $date_end, $period_id=null){
		$params = array();
		$params['where']['id_listing'] = $listing_id;
		$params['where']['date_start <='] = $date_end;
		$params['where']['date_end >='] = $date_start;
		$params['where']['status'] = 'open';
		if($user_id) $params['where']['id_user'] = $user_id;
		if($period_id) $params['where']['id !='] = $period_id;
		return $this->_get_periods_count($params) > 0;
	}
	
	/**
	 * Return identifiers of booking listings
	 * 
	 * @params array $data criteria data
	 * @return array
	 */
	public function get_booking_listings($data){
		if(empty($data['date_start']) || empty($data['date_end'])) return array();
	
		$hash = array();
	
		if(isset($data['date_start']) && !empty($data['date_start'])){
			$hash[] = 'start:'.$data['date_start'];
		}
		
		if(isset($data['date_end']) && !empty($data['date_end'])){
			$hash[] = 'end:'.$data['date_end'];
		}
		
		if(isset($data['guests']) && !empty($data['guests'])){
			$hash[] = 'guests:'.$data['guests'];
		}
		
		$hash = md5(implode(' ', $hash));

		if(!isset($this->_booking_cache[$hash])){
			$cache_period = $this->CI->pg_module->get_module_config('listings', 'booking_cache_period');
			if($cache_period){
				$this->DB->select('data');
				$this->DB->from(LISTINGS_BOOKING_CACHE_TABLE);
				$this->DB->where('hash', $hash);
				$this->DB->where('expire >', time());
				$results = $this->DB->get()->result_array();	
				if(!empty($results) && is_array($results)){
					$this->_booking_cache[$hash] = $results[0]['data'] ? (array)unserialize($results[0]['data']) : array();
					return $this->_booking_cache[$hash];
				}
			}

			$this->DB->select('id_listing');
			$this->DB->from(LISTINGS_BOOKING_PERIODS_TABLE);
			$this->DB->where_in('status', array('close', 'book'/*, 'wait'*/));
		
			/*if($this->CI->session->userdata('auth_type') == 'user'){
				$user_id = $this->CI->session->userdata('user_id');
				$this->DB->where('id_owner !=', $user_id);
			}*/
		
			if(isset($data['date_start']) && !empty($data['date_start'])){
				$this->DB->where('date_start <=', $data['date_start']);
			}
		
			if(isset($data['date_end'])  && !empty($data['date_end'])){
				$this->DB->where('date_end >=', $data['date_end']);
			}
		
			if(isset($data['guests']) && !empty($data['guests'])){
				$this->DB->or_where('id_user=id_owner', null, false);
				$this->DB->where('status', 'open');
				$this->DB->where('guests <', $data['guests']);
			}
		
			$this->DB->limit($this->_seacrh_items_limit);
	
			$results = $this->DB->get()->result_array();
			if(!empty($results) && is_array($results)){
				foreach($results as $i=>$row){
					$results[$i] = $row['id_listing'];
				}
				$data = array_unique($results);
			}else{
				$data = array();
			}
	
			if($cache_period){
				$save_data = array('data'=>serialize($data), 'expire'=>time()+$cache_period);				
				$this->DB->where('hash', $hash);
				$this->DB->update(LISTINGS_BOOKING_CACHE_TABLE, $save_data);
				if(!$this->DB->affected_rows()){
					$save_data['hash'] = $hash;
					$this->DB->insert(LISTINGS_BOOKING_CACHE_TABLE, $save_data);
				}
			}
		
			$this->_booking_cache[$hash] = $data;
		}
			
		return $this->_booking_cache[$hash];		
	}
	
	/**
	 * Return price of calendar period
	 * 
	 * @params array/integer $listing_id listing identifier
	 * @params string $date_start date of begin period
	 * @params string $date_end date of end period
	 * @params integer $guests number of guests
	 * @return mixed
	 */
	public function get_period_price($listing, $date_start, $date_end, $guests=1){
		$value = 0;
		
		$this->CI->load->model('Listings_model');
		
		if(!is_array($listing)){
			$this->CI->Listings_model->set_format_settings('use_format', false);
			$listing = $this->CI->Listings_model->get_listing_by_id($listing);
			$this->CI->Listings_model->set_format_settings('use_format', true);
		}
		
		if(isset($listing['booking']['periods'])){
			$periods = $listing['booking']['periods'];
		}else{
			$params = array(
				'listings' => $listing_id, 
				'users' => $listing['id_user'], 
				'date_start <=' => $date_end, 
				'date_end >=' => $date_start, 
				'status' => 'open',
			);
			$periods = $this->get_periods_list($params);
		}
	
		$date_start = strtotime($date_start);
		$date_end = strtotime($date_end);
	
		switch($listing['price_period']){
			case 1:
				$date_end += 86400;
				
				$days = 0;
				foreach($periods as $period){
					if($period['status'] != 'open') continue;
					$period_date_start = strtotime($period['date_start']);
					$period_date_end = strtotime($period['date_end']);
					if($period_date_start > $date_end || $period_date_end < $date_start) continue;
					$min = max($date_start, $period_date_start);
					$max = min($date_end, $period_date_end);
					if($min != $max){
						$days += $periods_days = ($max - $min)/86400;
						$value += $period['price']*$periods_days;
					}
				}
				$periods_days = ($date_end - $date_start)/86400 - $days;
				if($periods_days > 0){
					if($listing['price_negotiated']){
						return 0;
					}
					if($listing['price_reduced'] > 0){
						$price_value = $listing['price_reduced'];
					}else{
						$order_days = ($date_end - $date_start)/86400;
						if($listing['price_month'] && $order_days >= 28){
							$price_value = $listing['price_month'];
						}elseif($listing['price_week'] && $order_days >= 7){
							$price_value = $listing['price_week'];
						}else{
							$price_value = $listing['price'];	
						}
					}
					$value += $periods_days*$price_value;
				}
			break;
			case 2:
				$month_start = date('m', $date_start);
				$year_start = date('Y', $date_start);
				
				$month_end = date('m', $date_end);
				$year_end = date('Y', $date_end);
				
				$months_start = $year_start*12+$month_start;
				$months_end = $year_end*12+$month_end;
				
				$months_end += 1;
				
				$months = 0;
				
				foreach($periods as $period){
					if($period['status'] != 'open') continue;
					$period_date_start = strtotime($period['date_start']);
					$period_date_end = strtotime($period['date_end']);
					if($period_date_start > $date_end || $period_date_end < $date_start) continue;
					
					$period_month_start = date('m', $period_date_start);
					$period_year_start = date('Y', $period_date_start);
				
					$period_month_end = date('m', $period_date_end);
					$period_year_end = date('Y', $period_date_end);
				
					$period_months_start = $period_year_start*12+$period_month_start;
					$period_months_end = $period_year_end*12+$period_month_end;
					
					$min = max($months_start, $period_months_start);
					$max = min($months_end, $period_months_end);
					if($min != $max){
						$months += $periods_months = ($max-$min)/86400;
						$value += $period['price']*$periods_months;
					}
				}
				
				$periods_months = ($months_end - $months_start) - $months;
				if($periods_months > 0){
					if($listing['price_negotiated']){
						return 0;
					}
					if($listing['price_reduced'] > 0){
						$price_value = $listing['price_reduced'];
					}else{
						$price_value = $listing['price'];	
					}
					$value += $periods_months*$price_value;
				}
			break;
		}
		
		switch($listing['price_type']){
			case 1:
			break;
			case 2:
				$value *= $guests;
			break;
		}
		
		return $value;
	}
	
	/**
	 * Cahce of booking listings clean up cron
	 * 
	 * @return void
	 */
	public function search_booking_cron(){
		$this->DB->where('expire <=', time());
		$this->DB->delete(LISTINGS_BOOKING_CACHE_TABLE);
	}
}

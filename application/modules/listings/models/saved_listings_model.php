<?php

define('LISTINGS_SAVED_TABLE', DB_PREFIX.'listings_comparison_list');

/**
 * Saved Listings Model
 * 
 * @package PG_RealEstate
 * @subpackage Listings
 * @category	models
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Katya Kashkova <katya@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2010-04-02 15:07:07 +0300 (Ср, 02 апр 2010) $ $Author: kkashkova $
 **/
class Saved_listings_model extends Model{	
	/**
	 * Link to CodeIgniter object
	 * 
	 * @var object
	 */
	private $CI;

	/**
	 * Link to DataBase object
	 * 
	 * @var object
	 */
	private $DB;

	/**
	 * Fields of saved listing in data source
	 * 
	 * @var array
	 */
	private $_fields = array(
		'id',
		'id_user',
		'id_listing',
	);
	
	/**
	 * Settings for saved listings formatting
	 * 
	 * @var array
	 */
	private $format_settings = array(
		'use_format' 	=> true,
		'get_user'		=> true,
		'get_listing' 	=> true,
	);
		

	/**
	 * Constructor
	 * 
	 * @return Saved_listings_model
	 */
	public function __construct(){
		parent::Model();
		$this->CI = & get_instance();
		$this->DB = &$this->CI->db;
	}
	
	/**
	 * Return saved listong object by identifier
	 * 
	 * @param integer $saved_id saved identifier
	 * @param boolean $formatted use result formatting
	 * @return array
	 */
	public function get_saved_by_id($saved_id, $formatted=false){
		if(!$saved_id) return false;
		$this->DB->select(implode(', ', $this->_fields));
		$this->DB->from(LISTINGS_SAVED_TABLE);
		$this->DB->where('id', $saved_id);
		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			$data = array($results[0]);
			if($formatted){
				$data = $this->format_saved($data);
			}
			return $data[0];
		}
		return array();
	}
	
	/**
	 * Save saved listing object to data source
	 * 
	 * @param integer $saved_id saved lsiting identifier
	 * @param array $data saved listing data
	 * @return integer
	 */
	public function save_saved($saved_id, $data){
		if(!$saved_id){
			$this->DB->insert(LISTINGS_SAVED_TABLE, $data);
			$saved_id = $this->DB->insert_id();
		}else{
			$this->DB->where('id', $saved_id);
			$this->DB->update(LISTINGS_SAVED_TABLE, $data);
		}
		return $saved_id;
	}
	
	/**
	 * Remove saved listing from data source
	 * 
	 * @param integer $saved_id saved listing identifier
	 * @return void
	 */
	public function delete_saved($saved_id){
		$this->DB->where('id', $saved_id);
		$this->DB->delete(LISTINGS_SAVED_TABLE);
	}
	
	/**
	 * Remove saved listing from data source by listing and user identifiers
	 * 
	 * @param integer $user_id user identifier
	 * @param integer $listing_id listing identifier
	 * @return void
	 */
	public function delete_saved_by_listing($user_id, $listing_id){
		$this->DB->where('id_user', $user_id);
		$this->DB->where('id_listing', $listing_id);
		$this->DB->delete(LISTINGS_SAVED_TABLE);
	}
	
	/**
	 * Filter saved listings object by user
	 * 
	 * @param integer $user_id user identifier
	 * @return array
	 */
	private function _get_saved_by_user($user_id){
		$params = array();
		if(!$user_id) return array();
		$params['where']['id_user'] = $user_id;
		return $params;
	}
		
	/**
	 * Return saved listings objects from data source as array
	 * 
	 * @param integer $page page of results
	 * @param string $limits results limit
	 * @param array $order_by sorting dara
	 * @param array $params sql criteria
	 * @param boolean $formatted format results
	 * @return array
	 */
	private function _get_saved_list($page=null, $limits=null, $order_by=null, $params=array(), $formatted=true){
		$this->DB->select(implode(', ', $this->_fields));
		$this->DB->from(LISTINGS_SAVED_TABLE);
		
		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value);
			}
		}

		if(is_array($order_by) && count($order_by) > 0){
			foreach($order_by as $field => $dir){
				if(in_array($field, $this->_fields)){
					$this->DB->order_by($field.' '.$dir);
				}
			}
		}elseif($order_by){
			$this->DB->order_by($order_by);
		}

		if(!is_null($page)){
			$page = intval($page)?intval($page):1;
			$this->DB->limit($limits, $limits*($page-1));
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			foreach($results as $r){
				$data[] = $r;
			}
			if($formatted){
				$data = $this->format_saved($data);
			}
			return $data;
		}
		return array();
	}
	
	/**
	 * Return number of saved listings in data source
	 * 
	 * @param array $params sql criteria
	 * @return integer
	 */
	private function _get_saved_count($params=null){
		$this->DB->select('COUNT(*) AS cnt');
		$this->DB->from(LISTINGS_SAVED_TABLE);

		if(isset($params['where']) && is_array($params['where']) && count($params['where'])){
			foreach($params['where'] as $field=>$value){
				$this->DB->where($field, $value);
			}
		}

		if(isset($params['where_in']) && is_array($params['where_in']) && count($params['where_in'])){
			foreach($params['where_in'] as $field=>$value){
				$this->DB->where_in($field, $value);
			}
		}

		if(isset($params['where_sql']) && is_array($params['where_sql']) && count($params['where_sql'])){
			foreach($params['where_sql'] as $value){
				$this->DB->where($value);
			}
		}

		$results = $this->DB->get()->result_array();
		if(!empty($results) && is_array($results)){
			return intval($results[0]['cnt']);
		}
		return 0;
	}
	
	/**
	 * Return filtered saved listings objects from data source as array
	 * 
	 * @param array $filters filters data
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param string $order_by sorting order
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_saved_list($filters=array(), $page=null, $items_on_page=null, $order_by=null, $formatted=true){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge($params, $this->{'_get_saved_by_'.$filter}($value));
		}
		return $this->_get_saved_list($page, $items_on_page, $order_by, $params, array(), $formatted);
	}
	
	/**
	 * Return number of filtered saved listings in data source
	 * 
	 * @param array $filters filters data
	 * @return array
	 */
	public function get_saved_count($filters=array()){
		$params = array();
		foreach($filters as $filter=>$value){
			$params = array_merge($params, $this->{'_get_saved_by_'.$filter}($value));
		}
		return $this->_get_saved_count($params);
	}
	
	/**
	 * Return saved listings objects of user
	 * 
	 * @param integer $user_id user identifier
	 * @param integer $page page of results
	 * @param integer $items_on_page items per page
	 * @param string $order_by sorting data
	 * @param boolean $formatted format results
	 * @return array
	 */
	public function get_saved_list_by_user($user_id, $page=null, $items_on_page=null, $order_by=null, $formatted=true){
		$params = $this->_get_saved_by_user($user_id);
		return $this->_get_saved_list($page, $items_on_page, $order_by, $params, array(), $formatted);
	}
	
	/**
	 * Return number of user's saved listings
	 * 
	 * @param integer $user_id user identifier
	 * @return array
	 */
	public function get_saved_count_by_user($user_id){
		$params = $this->_get_saved_by_user($user_id);
		return $this->_get_saved_count($params);
	}
	
	/**
	 * Validate saved listing object for saving to data source
	 * 
	 * @param integer $saved_id saved listing identifier
	 * @param array $data saved listing data
	 * @return array
	 */
	public function validate_saved($saved_id, $data){
		$return = array('errors'=>array(), 'data'=>array());
		
		foreach($this->_fields as $field){
			if(isset($data[$field])){
				$return['data'][$field] = $data[$field];
			}
		}

		$is_data = isset($return['data']['id_user']);
		$is_empty = $is_data && empty($return['data']['id_user']);
		$is_required = !$is_data && $required;
		if($is_empty || $is_required){
		
			$return['errors'][] = l('error_empty_user', 'listings');
		}
		
		return $return;
	}
	
	/**
	 * Format saved listings objects
	 * 
	 * @param array $data saved listings data
	 * @return array
	 */
	public function format_saved($data){
		if(!$this->format_settings['use_format']){
			return $data;
		}
		
		$users_search = array();
		$listings_search = array();
		
		foreach($data as $key=>$saved){
			$data[$key] = $saved;
			// get_user
			if($this->format_settings['get_user']){
				$users_search[] = $saved['id_user'];
			}
			
			// get_listing
			if($this->format_settings['get_listing']){
				$ads_search[] = $saved['id_listing'];
			}
		}
		
		if($this->format_settings['get_user'] && !empty($users_search)){
			$this->CI->load->model('Users_model');
			$users_data = $this->CI->Users_model->get_users_list_by_key(null, null, null, array(), $users_search);
			foreach($data as $key=>$saved){
				$data[$key]['user'] = (isset($users_data[$saved['id_user']])) ? 
					$users_data[$saved['id_user']] : $this->CI->Users_model->format_default_user($saved['id_user']);
			}
		}
		
		if($this->format_settings['get_listing'] && !empty($listings_search)){
			$this->CI->load->model('Listings_model');
			$listings_data = $this->CI->Listings_model->get_listings_list_by_id($listings_search);
			foreach($data as $key=>$saved){
				$data[$key]['listing'] = (isset($users_data[$saved['id_listing']])) ? 
					$users_data[$saved['id_listing']] : $this->CI->Listings_model->format_default_listing($saved['id_listing']);
			}
		}
		
		return $data;
	}
}

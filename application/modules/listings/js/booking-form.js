function bookingForm(optionArr){
	this.properties = {
		siteUrl: '',
		bookingBtn: 'book_listing',
		deleteBtn: '',
		cFormId: 'order_form',
		urlGetForm: 'listings/ajax_order_form/',
		urlSaveForm: 'listings/guest_req/',
		urlGetPrice: 'listings/ajax_get_booking_price/',
		urlDeletePeriod: '',
		note_delete: '',
		id_close: 'close_btn',
		listingId: 0,
		periodId: 0,
		priceType: '',
		isPopup: true,
		isSavePeriod: true,
		showPrice: false,
		displayLogin: false,
		showCalendarLink: false,
		successCallback: null,
		calendar: null,
		deleteCallback: null,
		contentObj: new loadingContent({loadBlockWidth: '544px', closeBtnClass: 'load_content_close', closeBtnPadding: 5}),
		errorObj: new Errors,		
	};

	var _self = this;
	
	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		if(_self.properties.isPopup){
			$('#'+_self.properties.bookingBtn).bind('click', function(){
				if(_self.properties.displayLogin){
					$('html, body').animate({
						scrollTop: $("#ajax_login_link").offset().top
					}, 2000);
					$("#ajax_login_link").click();
				}else{
					_self.get_form();
				}
				return false;
			}).show();
		}else{
			$('#'+_self.properties.cFormId).bind('submit', function(){
				if(_self.properties.displayLogin){
					$('html, body').animate({
						scrollTop: $("#ajax_login_link").offset().top
					}, 2000);
					$("#ajax_login_link").click();
				}else{
					_self.save_form($(this).serialize());
				}
				return false;
			});	
			
			_self.init_price();
		}
		if(_self.properties.deleteBtn){
			$('#'+_self.properties.deleteBtn).bind('click', function(){
				if(!confirm(_self.properties.note_delete)) return false;
				_self.delete_period();
				return false;
			});
		}
	}
	
	this.get_form = function(){
		var url = _self.properties.siteUrl + _self.properties.urlGetForm + _self.properties.listingId;
		if(_self.properties.periodId) url += '/' + _self.properties.periodId;
		$.ajax({
			url: url,
			type: 'GET',
			cache: false,
			success: function(data){
				if(_self.properties.showCalendarLink){
					data = data.replace('calendar_link hide', 'calendar_link');
				}
				_self.properties.contentObj.show_load_block(data);
				$('#'+_self.properties.id_close).unbind().bind('click', function(){
					_self.clearBox();
					return false;
				});
				
				if(_self.properties.showPrice){
					_self.init_price();
				}
			}
		});		
		return false;
	}
	
	this.save_form = function(data){
		var url = _self.properties.siteUrl + _self.properties.urlSaveForm +  + _self.properties.listingId;
		if(_self.properties.periodId) url += '/' + _self.properties.periodId;
		$.ajax({
			url: url,			
			type: 'post',
			data: data,
			dataType: 'json',
			cache: false,
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					if(_self.properties.isPopup){
						_self.properties.contentObj.hide_load_block();
					}else{
						$('#'+_self.properties.cFormId).find('textarea').val('');
					}
					if(_self.properties.isSavePeriod) _self.properties.periodId = data.id;
					_self.properties.errorObj.show_error_block(data.success, 'success');
					window.setTimeout(function(){location.reload()},3000)
					if(_self.properties.successCallback){
						_self.properties.successCallback(data.id, data.data, _self.properties.calendar);
					}
					if(_self.properties.calendar){
						_self.properties.calendar.load_calendar();	
					}
				}
			},
		});				
		return false;
	}
	
	this.get_price = function(){
		var date_start, date_start_alt;
		var date_end, date_end_alt;
		
		switch(_self.properties.priceType){
			case '1':
				date_start = $('#'+_self.properties.cFormId+' input[name=period\\[date_start\\]]').val();
				date_end = $('#'+_self.properties.cFormId+' input[name=period\\[date_end\\]]').val();
				
				date_start_alt = $('#'+_self.properties.cFormId+' input[name=date_start_alt]').val();
				date_end_alt = $('#'+_self.properties.cFormId+' input[name=date_end_alt]').val();
			break;
			case '2':
				date_start = $('#'+_self.properties.cFormId+' select[name=date_start_year]').val()+'-'+$('#'+_self.properties.cFormId+' select[name=date_start_month]').val()+'-01 00:00:00';
				date_end = $('#'+_self.properties.cFormId+' select[name=date_end_year]').val()+'-'+$('#'+_self.properties.cFormId+' select[name=date_end_month]').val()+'-01 00:00:00';
				
				date_start_alt = '';
				date_end_alt = '';
			break;
		}
		
		var guests = $('#'+_self.properties.cFormId+' select[name=period\\[guests\\]]').val();
		
		var url = _self.properties.siteUrl + _self.properties.urlGetPrice +  + _self.properties.listingId;
		$.ajax({
			url: url,			
			type: 'post',
			data: {date_start: date_start, date_start_alt: date_start_alt, date_end: date_end, date_end_alt: date_end_alt, guests: guests},
			dataType: 'json',
			cache: false,
			success: function(data){
				$('#'+_self.properties.cFormId).find('.booking_price_value').html(data.price);
			}
		});
	}
	
	this.clearBox = function(){
		var data = $('#'+_self.properties.cFormId).serialize();
		_self.save_form(data);
	}

	this.delete_period = function(){
		var url = _self.properties.siteUrl + _self.properties.urlDeletePeriod +  + _self.properties.periodId;
		$.ajax({
			url: url,
			type: 'get',
			cache: false,
			dataType: 'json',
			success: function(data){
				if(typeof(data.error) != 'undefined' && data.error != ''){
					_self.properties.errorObj.show_error_block(data.error, 'error');
				}else{
					_self.properties.errorObj.show_error_block(data.success, 'success');
					$('#period'+_self.properties.periodId).remove();
					if(_self.properties.deleteCallback){
						_self.properties.deleteCallback();
					}
					if(_self.properties.calendar){
						_self.properties.calendar.load_calendar();	
					}
				}
			}
		});
	}
	
	this.set_calendar = function(calendar){
		_self.properties.calendar = calendar;
	}
	
	this.init_price = function(){
		$('#'+_self.properties.cFormId+' input[name=period\\[date_start\\]]').bind('change', function(){
			_self.get_price();
		});
			
		$('#'+_self.properties.cFormId+' input[name=period\\[date_end\\]]').bind('change', function(){
			_self.get_price();
		});
			
		$('#'+_self.properties.cFormId+' select[name=period\\[guests\\]]').bind('change', function(){
			_self.get_price();
		});
			
		$('#'+_self.properties.cFormId+' select[name=date_start_month]').bind('change', function(){
			_self.get_price();
		});
			
		$('#'+_self.properties.cFormId+' select[name=date_end_month]').bind('change', function(){
			_self.get_price();
		});
			
		$('#'+_self.properties.cFormId+' select[name=date_start_year]').bind('change', function(){
			_self.get_price();
		});
			
		$('#'+_self.properties.cFormId+' select[name=date_end_year]').bind('change', function(){
			_self.get_price();
		});
	}
	
	_self.Init(optionArr);
}

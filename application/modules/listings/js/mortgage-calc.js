function mortgageCalc(optionArr){
	this.properties = {
		siteUrl: '',
		loadUrl: 'listings/ajax_mortgage_calc',
		defaultRight: 0,
		selectedCurrAbbr: '',
		langs: {},
		contentObj: new loadingContent({loadBlockWidth: '584px', loadBlockID: 'mortgage_calc', closeBtnClass: 'load_content_close', closeBtnPadding: 5}),
		errorObj: new Errors,		
	};

	var _self = this;
	
	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		
		var url = _self.properties.siteUrl + _self.properties.loadUrl;
		$.getJSON(url, {}, function(data){
			for(var i in data){
				_self.properties[i] = data[i];
			}
			
			$('#calc_btn').bind('click', function(){
				_self.calc(this.form);
			});
		
			$('#empty_btn').bind('click', function(){
				_self.empty(this.form);
			});
		
			$('#results_btn').bind('click', function(){
				_self.results(this.form);
			});
		});
	}
	
	// get and showing results
	this.results = function(form){
		var aPrin = form.principal.value;
		var aIntRate = form.intRate.value;
		if(aIntRate > 1) aIntRate = aIntRate/100;
		aIntRate = aIntRate /12;
		var aNPer = form.numYears.value *12;
		if(form.HmoPmt.value == 0 || form.HmoPmt.value == ''){
			_self.properties.errorObj.show_error_block(_self.properties.alert1, 'error'); 
		}else{
			var aPmt = form.HmoPmt.value;
			var aIntPort = 0;
			var aAccumInt = 0;
			var aPrinPort = 0;
			var aAccumPrin = 0;
			var aCount = 0;
			var aPmtRow = "";
			var aPmtNum = 0;
			var today = new Date();
			var dayFactor = today.getTime();
			var pmtDay = today.getDate();
			var loanMM = today.getMonth() + 1;
			var loanYY = today.getFullYear();
			var loanDate = (loanMM + "/" + pmtDay + "/" + loanYY);
			var monthMS = 86400000 * 30.4;
			var pmtDate = 0;
			while(aCount < aNPer){
				aIntPort = aPrin * aIntRate;
				aAccumInt = eval(aAccumInt) + eval(aIntPort);
				aPrinPort = eval(aPmt) - eval(aIntPort);
				if(aPrinPort > 99000000){
					_self.properties.errorObj.show_error_block(_self.properties.alert3, 'error');
					focus(); return false;
				}
				aAccumPrin = eval(aAccumPrin) + eval(aPrinPort);
				aPrin = eval(aPrin) - eval(aPrinPort);
				aCount = eval(aCount) + eval(1);
				aPmtNum = eval(aPmtNum) + eval(1);
				dayFactor = eval(dayFactor) + eval(monthMS);
				pmtDate = new Date(dayFactor);
				pmtMonth = pmtDate.getMonth();
				pmtMonth = pmtMonth + 1;
				pmtYear = pmtDate.getFullYear();
				pmtString = (pmtMonth + '/' + pmtDay + '/' + pmtYear);
				aPmtRow = (
					aPmtRow + 
					'<tr>' +
						'<td align="'+_self.properties.defaultRight+'">' + aPmtNum + '</td>' +
						'<td align="'+_self.properties.defaultRight+'">' + pmtString + '</td>' + 
						'<td align="'+_self.properties.defaultRight+'">' + _self.RightDig(aPrinPort) + '</td>' +
						'<td align="'+_self.properties.defaultRight+'">' + _self.RightDig(aIntPort) + '</td>' + 
						'<td align="'+_self.properties.defaultRight+'">' + _self.RightDig(aPrin) + '</td>' + 
					'</tr>');
				if(aCount > 600){
					_self.properties.errorObj.show_error_block(_self.properties.alert2, 'error'); 
					break; 
				}else{
					continue; 
				}
			}
			var schedule = (
				'<div class="load_content">' +
				'<h1>'+_self.properties.mes1+'</h1>' + 
				'<div class="inside">' +
				'<TABLE BORDER=0 CELLPADDING=4 CELLSPACING=0>' +
				'<TR><TD COLSPAN=5><B>' + _self.properties.mes2 + ': ' + loanDate + '<BR>' +
				_self.properties.mes3 + ': ' + _self.properties.selectedCurrAbbr.replace('%s', _self.RightDig(form.principal.value)) +
				'<BR># ' + _self.properties.mes4 + ': ' + aNPer + '<BR>' + 
				_self.properties.mes5 + ': ' + _self.RightDig(aIntRate * 12 * 100) + '%<BR>' +
				_self.properties.mes6 + ': ' + _self.properties.selectedCurrAbbr.replace('%s', _self.RightDig(form.HmoPmt.value)) + 
				'</B></TD></TR><TR><TD COLSPAN=5><CENTER><FONT SIZE=+2>' + 
				_self.properties.mes7 + '</FONT><BR><FONT SIZE=-1>' + _self.properties.mes8 + '</FONT></CENTER></TD></TR>' +
				'<TR><TD><B>' + _self.properties.mes9 + ' #</B></TD><TD><B>' + 
				_self.properties.mes13 + '</B></TD><TD><B>' + _self.properties.mes3 + '</B></TD><TD><B>' + 
				_self.properties.mes10 + '</B></TD><TD><B>' + _self.properties.mes11 + '</B></TD></TR>' + 
				aPmtRow + '<TR><TD>'/*+'<B>' + _self.properties.mes12 + '</B>'*/ + '</TD><TD></TD>' + 
				'<TD ALIGN='+_self.properties.defaultRight+'><B>' + _self.RightDig(aAccumPrin) + '</B></TD><TD><B>' + _self.RightDig(aAccumInt) +
				'</B></TD><TD></TD></TR></TABLE>' +
				'</div>' + 
				'</div>'
			);
			_self.properties.contentObj.show_load_block(schedule);
		}
	}

	// clear form
	this.empty = function(form){
		form.principal.value = form.intRate.value = form.numYears.value = form.moPmt.value = form.HmoPmt.value = '';
	}

	// formating numbers
	this.RightDig = function(Vnum){ 
		if(Vnum > 99000000){
			_self.properties.errorObj.show_error_block(_self.properties.alert3, 'error');
			focus();
		}else{
			var V10million = parseInt(Vnum / 10000000);
			var V1million = (Vnum % 10000000)  / 1000000;
			if(V1million / 1000000 == 1){
				V1million = 1;
			}else if(V1million < 1){
				V1million = '0';
			}else{
				V1million = parseInt(V1million,10); 
			}
			var V100thousand = (Vnum % 1000000) / 100000;
			if(V100thousand / 100000 == 1){
				V100thousand = 1;
			}else if(V100thousand < 1){
				V100thousand = '0'; 
			}else{
				V100thousand = parseInt(V100thousand,10);
			}
			var V10thousand = (Vnum % 100000)  / 10000;
			if(V10thousand / 10000 == 1){
				V10thousand = 1;
			}else if(V10thousand < 1){
				V10thousand = '0';
			}else{
				V10thousand = parseInt(V10thousand,10);
			}
			var V1thousand = (Vnum % 10000) / 1000;
			if(V1thousand / 1000 == 1){
				V1thousand = 1; 
			}else if(V1thousand < 1){
				V1thousand = '0'; 
			}else{
				V1thousand = parseInt(V1thousand,10); 
			}
			var Vhundreds = (Vnum % 1000)  / 100;
			if(Vhundreds / 100 == 1){
				Vhundreds = 1;
			}else if(Vhundreds < 1){
				Vhundreds = '0'; 
			}else{
				Vhundreds = parseInt(Vhundreds,10);
			}
			var Vtens = (Vnum % 100)  / 10;
			if(Vtens / 10 == 1){
				Vtens = 1; 
			}else if(Vtens < 1){
				Vtens = '0'; 
			}else{
				Vtens = parseInt(Vtens,10);
			}
			var Vones = (Vnum % 10)  / 1;
			if(Vones / 1 == 1){
				Vones = 1;
			}else if(Vones < 1){
				Vones = '0';
			}else{
				Vones = parseInt(Vones,10);
			}
			var Vcents = 0;
			if(Vnum % 1 * 100 < 1){
				Vcents = 0; 
			}else{
				Vcents = parseInt(((eval(Vnum % 1) * 100)),10);
			}
			if(Vcents < 1){
				Vcents = '00'; 
			}else if(Vcents % 10 == 0){
				Vcents = Vcents + '0'; 
			}else if(Vcents % 10 == Vcents){
				Vcents = '0' + Vcents; 
			}else{
				Vcents = Vcents;
			}
			if(Vcents == '900'){
				Vcents = '90'; 
			}else if(Vcents == '800'){
				Vcents = '80'; 
			}else if(Vcents == '700'){
				Vcents = '70';
			}else if(Vcents == '600'){
				Vcents = '60'; 
			}else if(Vcents == '500'){
				Vcents = '50'; 
			}else if(Vcents == '400'){
				Vcents = "40"; 
			}else if(Vcents == '300'){
				Vcents = '30'; 
			}else if(Vcents == '200'){
				Vcents = '20'; 
			}else if(Vcents == '100'){
				Vcents = '10'; 
			}else{
				Vcents = Vcents; 
			}
			var Vformat = '';
			if(Vnum >= 10000000){
				Vformat = (
					V10million + '' + V1million + ',' + V100thousand + '' +
					V10thousand + '' + V1thousand + ',' + Vhundreds + '' + 
					Vtens + '' + Vones + '.' + Vcents
				); 
			}else if(Vnum >= 1000000){
				Vformat = (
					V1million + ',' + V100thousand + '' + V10thousand + '' + 
					V1thousand + ',' + Vhundreds + '' + Vtens + '' + Vones + '.' + Vcents
				); 
			}else if(Vnum >= 100000){
				Vformat = (
					V100thousand + '' + V10thousand + '' + V1thousand + ',' + 
					Vhundreds + '' + Vtens + '' + Vones + '.' + Vcents
				); 
			}else if(Vnum >= 10000){
				Vformat = (
					V10thousand + '' + V1thousand + ',' + Vhundreds + '' + 
					Vtens + '' + Vones + '.' + Vcents
				); 
			}else if(Vnum >= 1000){
				Vformat = (V1thousand + ',' + Vhundreds + '' + Vtens + '' + Vones + '.' + Vcents); 
			}else if(Vnum >= 100){
				Vformat = (Vhundreds + '' + Vtens + '' + Vones + '.' + Vcents); 
			}else if(Vnum >= 10){
				Vformat = (Vtens + '' + Vones + '.' + Vcents); 
			}else if(Vnum >= 1){
				Vformat = (Vones + '.' + Vcents); 
			}else{
				Vformat = ('0.' + Vcents); 
			}
			return Vformat;
		}
	}

	// calculate
	this.calc = function(form){
		if(form.principal.value == '' || form.principal.value == 0){
			_self.properties.errorObj.show_error_block(_self.properties.alert4+' #1.', 'error'); 
			form.principal.focus(); 
		}else if(form.intRate.value == '' || form.intRate.value == 0){
			_self.properties.errorObj.show_error_block(_self.properties.alert4+' #2.', 'error'); 
			form.intRate.focus(); 
		}else if(form.numYears.value == '' || form.numYears.value == 0){
			_self.properties.errorObj.show_error_block(_self.properties.alert4+' #3.', 'error');
			form.numYears.focus(); 
		}else{
			var Vprincipal = form.principal.value;
			var intRate = form.intRate.value;
			if(intRate > 1.0){
				intRate = intRate / 100.0; 
			}
			intRate = intRate / 12;
			var numMonths = form.numYears.value * 12;
			var factor = 1;
			for (var j = 0; j < numMonths; j++){
				factor = factor * (eval(1) + eval(intRate));
			}
			var moPmt = (Vprincipal * factor * intRate) / (eval(factor) - eval(1));
			form.moPmt.value = _self.properties.selectedCurrAbbr.replace('%s', _self.RightDig(moPmt));
			form.HmoPmt.value = moPmt;
		}
	}

	_self.Init(optionArr);
}
function listingsEditSteps(optionArr){
	this.properties = {
		blockClass: 'rollup-box',
		blockTitleClass: 'title',
		blockContentClass: 'cont',
		blockExpandClass: 'btn-link',
		currentStep: '1'
	}

	var _self = this;

	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.init_steps();
		_self.get_navigation();
	}

	this.init_steps = function(){
		var stepsCount = $('.'+_self.properties.blockClass).length;

		if(stepsCount == 1){
			 $('.'+_self.properties.blockClass + ' .' + _self.properties.blockTitleClass + ' .' + _self.properties.blockExpandClass).hide();
		}else{
			$('.'+_self.properties.blockClass + ' .' + _self.properties.blockTitleClass).bind('click', function(){
				_self.slide($(this).parent());
			});
			$('.'+_self.properties.blockClass).each(function(index){
				var cStep = index+1;
				if(cStep != _self.properties.currentStep){
					_self.slide($(this), 'close');
				}else{
					_self.slide($(this), 'open');
				}
			});
		}

	}

	this.slide = function(obj, new_state){
		var state = 'open';
		if($(obj).find('.'+ _self.properties.blockContentClass+':hidden').length > 0){
			state = 'close';
		}

		if(new_state && new_state == state){
			return;
		}

		if(state == 'close'){
			/// open
			$(obj).find('.'+ _self.properties.blockTitleClass+' ins').removeClass('i-expand fa-plus').addClass('i-collapse fa-minus');
			$(obj).find('.'+ _self.properties.blockContentClass).slideDown();
		}else{
			/// close
			$(obj).find('.'+ _self.properties.blockTitleClass+' ins').removeClass('i-collapse fa-minus').addClass('i-expand fa-plus');
			$(obj).find('.'+ _self.properties.blockContentClass).slideUp();
		}
	}

	this.get_navigation = function(){
		var lis = [];
		var active = 0;
		$('#edit_listings_menu li').each(function(index){
			var ci = index;
			if($(this).hasClass('active')){
				active = ci;
			}
			lis[ci] = $(this).find('a').attr('href');
		});
		if(active > 0){
			$('#edit_listings_prev').bind('click', function(){
				location.href = lis[active-1];
			}).show();
		}
		if(active < (lis.length-1)){
			$('#edit_listings_next').bind('click', function(){
				location.href = lis[active+1];
			}).show();
		}
	}

	_self.Init(optionArr);
}

function listingsPrint(optionArr){
	this.properties = {
		siteUrl: '',
		sectionAjaxUrl: 'listings/ajax_get_section/',
		printBtnId: 'print_btn',
		blockId: 'content_m_',
		idListing: 0,
	}
	
	this.lock = false;
	
	this.loaded = {overview: false, calendar: false, print: false};

	var _self = this;
	
	this.Init = function(options){
		_self.properties = $.extend(_self.properties, options);
		_self.init_links();
	}
	
	this.init_links = function(){
		$('#'+_self.properties.printBtnId).removeAttr('onclick').unbind('click').bind('click', function(){
			if(_self.lock) return false;
			_self.print();
			return false;
		});
	}
	
	this.print = function(){
		_self.lock = true;
		
		for(var i in _self.loaded){
			var html = $('#'+_self.properties.blockId + i).html();
			if(!_self.loaded[i] && !html){
				_self.load_block(i);
				return false;
			}
		}
		
		_self.lock = false;
		
		window.print();
	}
	
	this.load_block = function(section_gid){
		var url = _self.properties.siteUrl + _self.properties.sectionAjaxUrl + _self.properties.idListing + '/' + section_gid;
		$.ajax({
			url: url, 
			type: 'GET',
			cache: false,
			success: function(data){
				_self.loaded[section_gid] = true;
				$('#'+_self.properties.blockId + section_gid).html(data).waitForImages(
					function(){
						_self.print();
					}, 
					function(loaded, count, success){
					},
					true
				);
			}
		});
	}
	
	_self.Init(optionArr);
}

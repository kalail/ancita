<?php

$install_lang["notification_admin_booking_approve"] = "Booking request is approved";
$install_lang["notification_admin_booking_decline"] = "Booking request is declined";
$install_lang["notification_admin_booking_request"] = "New booking request";
$install_lang["notification_listing_booking_approve"] = "Booking request is approved";
$install_lang["notification_listing_booking_decline"] = "Booking request is declined";
$install_lang["notification_listing_booking_request"] = "New booking request";
$install_lang["notification_listing_need_moderate"] = "New listing awaiting moderation";
$install_lang["notification_listing_service_enabled"] = "Paid service enabled";
$install_lang["notification_listing_service_expired"] = "Paid service expired";
$install_lang["notification_listing_status_updated"] = "Listing status is updated";
$install_lang["notification_share_listing"] = "Share listing by mail";
$install_lang["tpl_admin_booking_approve_content"] = "Hello admin,\n\nBooking ([name], [user]) is approved on [domain].\n\nBest regards,\n[name_from]";
$install_lang["tpl_admin_booking_approve_subject"] = "[domain] | Booking request is approved";
$install_lang["tpl_admin_booking_decline_content"] = "Hello admin,\n\nBooking ([name], [user]) is declined on [domain].\n\nBest regards,\n[name_from]";
$install_lang["tpl_admin_booking_decline_subject"] = "[domain] | Booking request is declined";
$install_lang["tpl_admin_booking_request_content"] = "Hello admin,\n\n There are new booking requests on [domain]. Access administration panel > Listings > Booking to view it.\n\nBest regards,\n[name_from]";
$install_lang["tpl_admin_booking_request_subject"] = "[domain] | New booking request";
$install_lang["tpl_listing_booking_approve_content"] = "Hello [fname],\n\nYour booking ([name], [user]) is approved on [domain].\n\nBest regards,\n[name_from]";
$install_lang["tpl_listing_booking_approve_subject"] = "[domain] | Booking request is approved";
$install_lang["tpl_listing_booking_decline_content"] = "Hello [fname],\n\nYour booking ([name], [user]) is declined on [domain].\n\nBest regards,\n[name_from]";
$install_lang["tpl_listing_booking_decline_subject"] = "[domain] | Booking request is declined";
$install_lang["tpl_listing_booking_request_content"] = "Hello [fname],\n\nThere are new booking of [name] from [user] on [domain].\n\nBest regards,\n[name_from]";
$install_lang["tpl_listing_booking_request_subject"] = "[domain] | New booking request";
$install_lang["tpl_listing_need_moderate_content"] = "Hello admin,\n\nThere are new listing awaiting moderation on [domain]. Access administration panel > Listings > Moderation to view it.\n\nBest regards,\n[name_from]";
$install_lang["tpl_listing_need_moderate_subject"] = "[domain] | New listing awaiting moderation";
$install_lang["tpl_listing_service_enabled_content"] = "Hello [user],\n\nPaid service [name] is enabled and active till [date].\n\nBest regards,\n[name_from]";
$install_lang["tpl_listing_service_enabled_subject"] = "[domain] | Paid service enabled";
$install_lang["tpl_listing_service_expired_content"] = "Hello [user],\n\nPaid service [name] has expired. Would you like to extend its activity period?\n\nBest regards,\n[name_from]";
$install_lang["tpl_listing_service_expired_subject"] = "[domain] | Paid service expired";
$install_lang["tpl_listing_status_updated_content"] = "Hello [user],\n\nYour listing status is updated.\n[listing] - [status]\n\nBest regards,\n[name_from]";
$install_lang["tpl_listing_status_updated_subject"] = "[domain] | Listing status updated";
$install_lang["tpl_share_listing_content"] = "Hello [user],\n\nShare listing [listing_name] ([listing_url]) to you:\n\n[message]\n\nBest regards,\n[name_from]";
$install_lang["tpl_share_listing_subject"] = "Share listing (ID=[listing_id]) to you";


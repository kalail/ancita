<?php

$install_lang["operation_type"]["header"] = "Listing type";
$install_lang["operation_type"]["option"]["sale"] = "for sale";
$install_lang["operation_type"]["option"]["buy"] = "for buy";
$install_lang["operation_type"]["option"]["rent"] = "for rent";
$install_lang["operation_type"]["option"]["lease"] = "for lease";

$install_lang["operation_type_search"]["header"] = "Listing type (search)";
$install_lang["operation_type_search"]["option"]["sale"] = "For sale";
$install_lang["operation_type_search"]["option"]["buy"] = "For buy";
$install_lang["operation_type_search"]["option"]["rent"] = "For rent";
$install_lang["operation_type_search"]["option"]["lease"] = "For lease";

$install_lang["square_units"]["header"] = "Square units";
$install_lang["square_units"]["option"]["1"] = "sq.f.";
$install_lang["square_units"]["option"]["2"] = "sq.m.";

$install_lang["posted_within"]["header"] = "Posted";
$install_lang["posted_within"]["option"]["30"] = "within 30 days";
$install_lang["posted_within"]["option"]["7"] = "within 7 days";
$install_lang["posted_within"]["option"]["3"] = "within 3 days";
$install_lang["posted_within"]["option"]["1"] = "yesterday";

$install_lang["radius_data"]["header"] = "Radius";
$install_lang["radius_data"]["option"]["1"] = "1 km";
$install_lang["radius_data"]["option"]["5"] = "5 km";
$install_lang["radius_data"]["option"]["10"] = "10 km";

$install_lang["price_period"]["header"] = "Period";
$install_lang["price_period"]["option"]["1"] = "per day";
$install_lang["price_period"]["option"]["2"] = "per month";

$install_lang["price_period_unit"]["header"] = "Period unit";
$install_lang["price_period_unit"]["option"]["1"] = "days";
$install_lang["price_period_unit"]["option"]["2"] = "months";

$install_lang["price_type"]["header"] = "Price";
$install_lang["price_type"]["option"]["1"] = "per room";
$install_lang["price_type"]["option"]["2"] = "per person";

$install_lang["booking_status"]["header"] = "Booking status";
$install_lang["booking_status"]["option"]["open"] = "Available";
$install_lang["booking_status"]["option"]["close"] = "Closed";
$install_lang["booking_status"]["option"]["book"] = "Unavailable";
$install_lang["booking_status"]["option"]["wait"] = "Pending";

$install_lang["booking_guests"]["header"] = "Guests";
$install_lang["booking_guests"]["option"]["1"] = "1";
$install_lang["booking_guests"]["option"]["2"] = "2";
$install_lang["booking_guests"]["option"]["3"] = "3";
$install_lang["booking_guests"]["option"]["4"] = "4";
$install_lang["booking_guests"]["option"]["5"] = "5";
$install_lang["booking_guests"]["option"]["6"] = "6";
$install_lang["booking_guests"]["option"]["7"] = "7";
$install_lang["booking_guests"]["option"]["8"] = "8";
$install_lang["booking_guests"]["option"]["9"] = "9";
$install_lang["booking_guests"]["option"]["10"] = "10";
$install_lang["booking_guests"]["option"]["11"] = "10+";


<?php

$install_lang["operation_type"]["header"] = "Тип на обявата";
$install_lang["operation_type"]["option"]["sale"] = "продажба";
$install_lang["operation_type"]["option"]["buy"] = "покупка";
$install_lang["operation_type"]["option"]["rent"] = "под наем";
$install_lang["operation_type"]["option"]["lease"] = "наема";

$install_lang["operation_type_search"]["header"] = "Тип на обявата (в търсенето)";
$install_lang["operation_type_search"]["option"]["sale"] = "Продава";
$install_lang["operation_type_search"]["option"]["buy"] = "Купува";
$install_lang["operation_type_search"]["option"]["rent"] = "Отдава";
$install_lang["operation_type_search"]["option"]["lease"] = "Наема";

$install_lang["square_units"]["header"] = "Мерни единици";
$install_lang["square_units"]["option"]["1"] = "кв.ф.";
$install_lang["square_units"]["option"]["2"] = "кв.м.";

$install_lang["posted_within"]["header"] = "Публикувано";
$install_lang["posted_within"]["option"]["30"] = "за последните 30 дни";
$install_lang["posted_within"]["option"]["7"] = "за последните 7 дни";
$install_lang["posted_within"]["option"]["3"] = "за последните 3 дни";
$install_lang["posted_within"]["option"]["1"] = "вчера";

$install_lang["radius_data"]["header"] = "Радиус";
$install_lang["radius_data"]["option"]["1"] = "1 км";
$install_lang["radius_data"]["option"]["5"] = "5 км";
$install_lang["radius_data"]["option"]["10"] = "10 км";

$install_lang["price_period"]["header"] = "Период";
$install_lang["price_period"]["option"]["1"] = "за ден";
$install_lang["price_period"]["option"]["2"] = "за месец";

$install_lang["price_period_unit"]["header"] = "Период (значение)";
$install_lang["price_period_unit"]["option"]["1"] = "дней";
$install_lang["price_period_unit"]["option"]["2"] = "месяцев";

$install_lang["price_type"]["header"] = "Цена";
$install_lang["price_type"]["option"]["1"] = "за стая";
$install_lang["price_type"]["option"]["2"] = "за човек";

$install_lang["booking_status"]["header"] = "Статус на резервацията";
$install_lang["booking_status"]["option"]["open"] = "Свободно";
$install_lang["booking_status"]["option"]["close"] = "Затворено";
$install_lang["booking_status"]["option"]["book"] = "Заето";
$install_lang["booking_status"]["option"]["wait"] = "Очаква се оглед";

$install_lang["booking_guests"]["header"] = "Брой наематели";
$install_lang["booking_guests"]["option"]["1"] = "1";
$install_lang["booking_guests"]["option"]["2"] = "2";
$install_lang["booking_guests"]["option"]["3"] = "3";
$install_lang["booking_guests"]["option"]["4"] = "4";
$install_lang["booking_guests"]["option"]["5"] = "5";
$install_lang["booking_guests"]["option"]["6"] = "6";
$install_lang["booking_guests"]["option"]["7"] = "7";
$install_lang["booking_guests"]["option"]["8"] = "8";
$install_lang["booking_guests"]["option"]["9"] = "9";
$install_lang["booking_guests"]["option"]["10"] = "10";
$install_lang["booking_guests"]["option"]["11"] = "10+";


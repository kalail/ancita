<?php

$install_lang["notification_admin_booking_approve"] = "Запитването за резервация е одобрено";
$install_lang["notification_admin_booking_decline"] = "Запитването за резервация е отхвърлено";
$install_lang["notification_admin_booking_request"] = "Ново запитване за резервация";
$install_lang["notification_listing_booking_approve"] = "Запитването за резервация е одобрено";
$install_lang["notification_listing_booking_decline"] = "Запитването за резервация е охвърлено";
$install_lang["notification_listing_booking_request"] = "Ново запитване за резервация";
$install_lang["notification_listing_need_moderate"] = "Новата обява очаква проверка";
$install_lang["notification_listing_service_enabled"] = "Включва платена услуга";
$install_lang["notification_listing_service_expired"] = "Платената услуга изтече";
$install_lang["notification_listing_status_updated"] = "Статусът на обявата е обновен";
$install_lang["notification_share_listing"] = "Споделете обявите чрез mail";
$install_lang["tpl_admin_booking_approve_content"] = "Здравейте, admin!\n\nЗапитването за резервация ([name], [user]) на сайта [domain] е одобрено.\n\nС уважение,\n[name_from]";
$install_lang["tpl_admin_booking_approve_subject"] = "[domain] | Запитването за резервация одобрено";
$install_lang["tpl_admin_booking_decline_content"] = "Здравейте, admin!\n\nЗапитването за резервация ([name], [user]) на сайта [domain] е отклонено.\n\nС уважение,\n[name_from]";
$install_lang["tpl_admin_booking_decline_subject"] = "[domain] | Запитването за резервация е отклонено";
$install_lang["tpl_admin_booking_request_content"] = "Здравейте, admin!\n\nНа сайта [domain] има нови запитвания за резервация. За подробности, влезте в панела на администратора > Обяви > Резервации.\n\nС уважение,\n[name_from]";
$install_lang["tpl_admin_booking_request_subject"] = "[domain] | Нови запитвания за резервация";
$install_lang["tpl_listing_booking_approve_content"] = "Здравейте, [fname]!\n\nВаше запитване за резервация ([name], [user]) на сайта [domain] е одобрено.\n\nС уважение,\n[name_from]";
$install_lang["tpl_listing_booking_approve_subject"] = "[domain] | Резервацията е одобрена";
$install_lang["tpl_listing_booking_decline_content"] = "Здравейте, [fname]!\n\nВашето запитване за резервация ([name], [user]) на сайта [domain] е отклонено.\n\nС уважение,\n[name_from]";
$install_lang["tpl_listing_booking_decline_subject"] = "[domain] | Запитване за резервация е отклонено";
$install_lang["tpl_listing_booking_request_content"] = "Здравейте, [fname]!\n\nНово запитване за резервация [name] от [user] на сайта [domain].\n\nС уважение,\n[name_from]";
$install_lang["tpl_listing_booking_request_subject"] = "[domain] | Ново запитване за резервация";
$install_lang["tpl_listing_need_moderate_content"] = "Здравейте, администратор!\n\nНова обява очаква проверка на сайта [domain]. За преглед влезте в панела на администратора > Обяви > Модерация.\n\nС уважение,\n[name_from]";
$install_lang["tpl_listing_need_moderate_subject"] = "[domain] | Нова обява очаква проверка";
$install_lang["tpl_listing_service_enabled_content"] = "Здравейте, [user]!\n\nПлатената услуга [name] е включена и активна до [date].\n\nС уважение,\n[name_from]";
$install_lang["tpl_listing_service_enabled_subject"] = "[domain] | Платената услуга е активна";
$install_lang["tpl_listing_service_expired_content"] = "Здравейте, [user]!\n\nСрокът на действие на платената услуга [name] изтече. Желаете ли да удължите срока на активност?\n\nС уважение,\n[name_from]";
$install_lang["tpl_listing_service_expired_subject"] = "[domain] | Платената услуга изтече";
$install_lang["tpl_listing_status_updated_content"] = "Здравейте, [user]!\n\nСтатусът на вашата обява е изменен.\n[listing] - [status]\n\nС уважение,\n[name_from]";
$install_lang["tpl_listing_status_updated_subject"] = "[domain] | Статусът на обявата е изменен";
$install_lang["tpl_share_listing_content"] = "Здравейте, [user]!\n\nТази обява [listing_name] ([listing_url]) е интересна за вас:\n\n[message]\n\nС уважение,\n[name_from]";
$install_lang["tpl_share_listing_subject"] = "За вас е споделена обява (ID=[listing_id])";


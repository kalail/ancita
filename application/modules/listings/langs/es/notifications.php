<?php

$install_lang["notification_admin_booking_approve"] = "Solicitud de reserva se aprueba";
$install_lang["notification_admin_booking_decline"] = "Solicitud de reserva es rechazada";
$install_lang["notification_admin_booking_request"] = "Nueva solicitud de reserva";
$install_lang["notification_listing_booking_approve"] = "Solicitud de reserva se aprueba";
$install_lang["notification_listing_booking_decline"] = "Solicitud de reserva es rechazada";
$install_lang["notification_listing_booking_request"] = "Nueva solicitud de reserva";
$install_lang["notification_listing_need_moderate"] = "Nuevo listado enespera de moderación";
$install_lang["notification_listing_service_enabled"] = "Servicio de pago habilitado";
$install_lang["notification_listing_service_expired"] = "Servicio con cargo caducado";
$install_lang["notification_listing_status_updated"] = "Estado de listado actualizado";
$install_lang["notification_share_listing"] = "Compartir la oferta por correo";
$install_lang["tpl_admin_booking_approve_content"] = "Hola admin,\n\nReservación ([name], [user]) aprobada en [domain].\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_admin_booking_approve_subject"] = "[domain] | Solicitud de reserva";
$install_lang["tpl_admin_booking_decline_content"] = "Hola admin,\n\nReservación ([name], [user]) negada en [domain].\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_admin_booking_decline_subject"] = "[domain] | Solicitud de reservación negada";
$install_lang["tpl_admin_booking_request_content"] = "Hola admin,\n\nHay nuevas solicitudes de reservaciones en [domain]. Accese a panel de administración > Listados > Reservación para ver.\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_admin_booking_request_subject"] = "[name_from] Nueva solicitued de resrvación";
$install_lang["tpl_listing_booking_approve_content"] = "Hola [fname],\n\nReservación ([name], [user]) aprobada en [domain].\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_listing_booking_approve_subject"] = "[domain] | Solicitud de reservación aprobada";
$install_lang["tpl_listing_booking_decline_content"] = "Hola [fname],\n\nSu resrvación ([name], [user]) es negada en [domain].\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_listing_booking_decline_subject"] = "[domain] | Solicitud de reservación negada";
$install_lang["tpl_listing_booking_request_content"] = "Hola [fname],\n\nHay una nueva reservación de [name] desde [user] en [domain].\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_listing_booking_request_subject"] = "[domain] | Nueva solicitud de reservación";
$install_lang["tpl_listing_need_moderate_content"] = "Hola admin,\n\nHay un nuevo listado esperando moderación en [domain]. Accece al panel de administración > Listados > Moderación para verlo.\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_listing_need_moderate_subject"] = "[domain] | Nuevo listado esperando moderación";
$install_lang["tpl_listing_service_enabled_content"] = "Hola [user],\n\nServicio pagado [name] activado y activo [date].\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_listing_service_enabled_subject"] = "[domain] | Servicio pagado activado";
$install_lang["tpl_listing_service_expired_content"] = "Hola [user],\n\nServicio pagado [name] expiró. ¿Quiere extender el periodo de actividad?\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_listing_service_expired_subject"] = "[domain] | Servicio pagado expiró";
$install_lang["tpl_listing_status_updated_content"] = "Hola [user],\n\nSi estado de lsitado actualizado.\n[listing] - [status]\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_listing_status_updated_subject"] = "[domain] | Estado de lsitado actualizado";
$install_lang["tpl_share_listing_content"] = "Hola [user],\n\nCompartior listado [listing_name] ([listing_url]) para usted:\n\n[message]\n\nSaludos cordiales,\n[name_from]";
$install_lang["tpl_share_listing_subject"] = "Compartir listado (ID=[listing_id]) para usted";


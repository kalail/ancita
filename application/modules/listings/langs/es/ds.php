<?php

$install_lang["operation_type"]["header"] = "Tipo de listado";
$install_lang["operation_type"]["option"]["sale"] = "En venta";
$install_lang["operation_type"]["option"]["buy"] = "Para la compra";
$install_lang["operation_type"]["option"]["rent"] = "En alquiler";
$install_lang["operation_type"]["option"]["lease"] = "Para arrendamiento";

$install_lang["operation_type_search"]["header"] = "Tipo de listado (búsqueda)";
$install_lang["operation_type_search"]["option"]["sale"] = "En venta";
$install_lang["operation_type_search"]["option"]["buy"] = "Para comprar";
$install_lang["operation_type_search"]["option"]["rent"] = "En alquiler";
$install_lang["operation_type_search"]["option"]["lease"] = "Para arrendamiento";

$install_lang["square_units"]["header"] = "Unidades cuadradas";
$install_lang["square_units"]["option"]["1"] = "pies cuadrados";
$install_lang["square_units"]["option"]["2"] = "metros cuadrados";

$install_lang["posted_within"]["header"] = "Publicado";
$install_lang["posted_within"]["option"]["30"] = "dentro de 30 días";
$install_lang["posted_within"]["option"]["7"] = "dentro de 7 días";
$install_lang["posted_within"]["option"]["3"] = "dentro de 3 días";
$install_lang["posted_within"]["option"]["1"] = "ayer";

$install_lang["radius_data"]["header"] = "Radio";
$install_lang["radius_data"]["option"]["1"] = "1 km";
$install_lang["radius_data"]["option"]["5"] = "5 km";
$install_lang["radius_data"]["option"]["10"] = "10 km";

$install_lang["price_period"]["header"] = "Período";
$install_lang["price_period"]["option"]["1"] = "Por día";
$install_lang["price_period"]["option"]["2"] = "Por mes";

$install_lang["price_period_unit"]["header"] = "Period unit";
$install_lang["price_period_unit"]["option"]["1"] = "days";
$install_lang["price_period_unit"]["option"]["2"] = "months";

$install_lang["price_type"]["header"] = "Precio";
$install_lang["price_type"]["option"]["1"] = "Por habitación";
$install_lang["price_type"]["option"]["2"] = "Por persona";

$install_lang["booking_status"]["header"] = "Estado de la reservación";
$install_lang["booking_status"]["option"]["open"] = "Disponible";
$install_lang["booking_status"]["option"]["close"] = "Cerrado";
$install_lang["booking_status"]["option"]["book"] = "No disponible";
$install_lang["booking_status"]["option"]["wait"] = "Pendiente";

$install_lang["booking_guests"]["header"] = "Invitados";
$install_lang["booking_guests"]["option"]["1"] = "1";
$install_lang["booking_guests"]["option"]["2"] = "2";
$install_lang["booking_guests"]["option"]["3"] = "3";
$install_lang["booking_guests"]["option"]["4"] = "4";
$install_lang["booking_guests"]["option"]["5"] = "5";
$install_lang["booking_guests"]["option"]["6"] = "6";
$install_lang["booking_guests"]["option"]["7"] = "7";
$install_lang["booking_guests"]["option"]["8"] = "8";
$install_lang["booking_guests"]["option"]["9"] = "9";
$install_lang["booking_guests"]["option"]["10"] = "10";
$install_lang["booking_guests"]["option"]["11"] = "10+";


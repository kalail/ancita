<?php

$install_lang["notification_admin_booking_approve"] = "Запрос на бронирование одобрен";
$install_lang["notification_admin_booking_decline"] = "Запрос на бронирование отклонен";
$install_lang["notification_admin_booking_request"] = "Новый запрос на бронирование";
$install_lang["notification_listing_booking_approve"] = "Запрос на бронирование одобрен";
$install_lang["notification_listing_booking_decline"] = "Запрос на бронирование отклонен";
$install_lang["notification_listing_booking_request"] = "Новый запрос на бронирование";
$install_lang["notification_listing_need_moderate"] = "Новое объявление ожидает проверки";
$install_lang["notification_listing_service_enabled"] = "Платная услуга включена";
$install_lang["notification_listing_service_expired"] = "Срок действия платной услуги истек";
$install_lang["notification_listing_status_updated"] = "Статус объявления обновлен";
$install_lang["notification_share_listing"] = "Поделиться объявлением по почте";
$install_lang["tpl_admin_booking_approve_content"] = "Здравствуйте, admin!\n\nЗапрос на бронирование ([name], [user]) на сайте [domain] был одобрен.\n\nС уважением,\n[name_from]";
$install_lang["tpl_admin_booking_approve_subject"] = "[domain] | Запрос на бронирование одобрен";
$install_lang["tpl_admin_booking_decline_content"] = "Здравствуйте, admin!\n\nЗапрос на бронирование ([name], [user]) на сайте [domain] был отклонен.\n\nС уважением,\n[name_from]";
$install_lang["tpl_admin_booking_decline_subject"] = "[domain] | Запрос на бронирование отклонен";
$install_lang["tpl_admin_booking_request_content"] = "Здравствуйте, admin!\n\n На сайте [domain] появились новые запросы на бронирование. Чтобы посмотреть подробности, зайдите в панель администратора > Объявления > Бронирование.\n\nС уважением,\n[name_from]";
$install_lang["tpl_admin_booking_request_subject"] = "[domain] | Новый запрос на бронирование";
$install_lang["tpl_listing_booking_approve_content"] = "Здравствуйте, [fname]!\n\nВаш запрос на бронирование ([name], [user]) на сайте [domain] одобрен.\n\nС уважением,\n[name_from]";
$install_lang["tpl_listing_booking_approve_subject"] = "[domain] | Запрос на бронирование одобрен";
$install_lang["tpl_listing_booking_decline_content"] = "Здравствуйте, [fname]!\n\nВаш запрос на бронирование ([name], [user]) на сайте [domain] отклонен.\n\nС уважением,\n[name_from]";
$install_lang["tpl_listing_booking_decline_subject"] = "[domain] | Запрос на бронирование отклонен";
$install_lang["tpl_listing_booking_request_content"] = "Здравствуйте, [fname]!\n\nНовый запрос на бронирование [name] от [user] на сайте [domain].\n\nС уважением,\n[name_from]";
$install_lang["tpl_listing_booking_request_subject"] = "[domain] | Новый запрос на бронирование";
$install_lang["tpl_listing_need_moderate_content"] = "Здравствуйте, администратор!\n\nНовое объявление ожидает проверки на сайте [domain]. Для просмотра зайдите в панель администратора > Объявления > Модерация.\n\nС уважением,\n[name_from]";
$install_lang["tpl_listing_need_moderate_subject"] = "[domain] | Новое объявление ожидает проверки";
$install_lang["tpl_listing_service_enabled_content"] = "Здравствуйте, [user]!\n\nПлатная услуга [name] была включена и теперь активна до [date].\n\nС уважением,\n[name_from]";
$install_lang["tpl_listing_service_enabled_subject"] = "[domain] | Платная услуга включена";
$install_lang["tpl_listing_service_expired_content"] = "Здравствуйте, [user]!\n\nСрок действия платной услуги [name] истек. Хотите ли Вы продлить период ее активности?\n\nС уважением,\n[name_from]";
$install_lang["tpl_listing_service_expired_subject"] = "[domain] | Срок действия платной услуги истек";
$install_lang["tpl_listing_status_updated_content"] = "Здравствуйте, [user]!\n\nСтатус вашего объявления был изменен.\n[listing] - [status]\n\nС уважением,\n[name_from]";
$install_lang["tpl_listing_status_updated_subject"] = "[domain] | Статус объявления изменен";
$install_lang["tpl_share_listing_content"] = "Здравствуйте, [user]!\n\nПосмотрите объявление [listing_name] ([listing_url]):\n\n[message]\n\nС уважением,\n[name_from]";
$install_lang["tpl_share_listing_subject"] = "С вами поделились объявлением (ID=[listing_id])";


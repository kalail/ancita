<?php

$install_lang["operation_type"]["header"] = "Angebotstyp";
$install_lang["operation_type"]["option"]["sale"] = "zum Verkauf";
$install_lang["operation_type"]["option"]["buy"] = "zum Kauf";
$install_lang["operation_type"]["option"]["rent"] = "zum Mieten";
$install_lang["operation_type"]["option"]["lease"] = "zum Leasen";

$install_lang["operation_type_search"]["header"] = "Angebotstypp (Suche)";
$install_lang["operation_type_search"]["option"]["sale"] = "zum Verkauf";
$install_lang["operation_type_search"]["option"]["buy"] = "zum Kauf ";
$install_lang["operation_type_search"]["option"]["rent"] = "zum Mieten";
$install_lang["operation_type_search"]["option"]["lease"] = "zum Leasen";

$install_lang["square_units"]["header"] = "Platzeinheiten";
$install_lang["square_units"]["option"]["1"] = "sq.f.";
$install_lang["square_units"]["option"]["2"] = "qm";

$install_lang["posted_within"]["header"] = "verfasst";
$install_lang["posted_within"]["option"]["30"] = "innerhalb von 30 Tagen";
$install_lang["posted_within"]["option"]["7"] = "innerhalb von 7 Tagen";
$install_lang["posted_within"]["option"]["3"] = "innerhalb von 3 Tagen";
$install_lang["posted_within"]["option"]["1"] = "gestern";

$install_lang["radius_data"]["header"] = "Radius";
$install_lang["radius_data"]["option"]["1"] = "1 km";
$install_lang["radius_data"]["option"]["5"] = "5 km";
$install_lang["radius_data"]["option"]["10"] = "10 km";

$install_lang["price_period"]["header"] = "Zeitraum";
$install_lang["price_period"]["option"]["1"] = "pro Tag";
$install_lang["price_period"]["option"]["2"] = "pro Monat";

$install_lang["price_period_unit"]["header"] = "Period unit";
$install_lang["price_period_unit"]["option"]["1"] = "days";
$install_lang["price_period_unit"]["option"]["2"] = "months";

$install_lang["price_type"]["header"] = "Preis";
$install_lang["price_type"]["option"]["1"] = "pro Zimmer";
$install_lang["price_type"]["option"]["2"] = "pro Person";

$install_lang["booking_status"]["header"] = "Buchungsstatus";
$install_lang["booking_status"]["option"]["open"] = "Verfügbar";
$install_lang["booking_status"]["option"]["close"] = "geschlossen";
$install_lang["booking_status"]["option"]["book"] = "nicht verfügbar";
$install_lang["booking_status"]["option"]["wait"] = "ausstehend";

$install_lang["booking_guests"]["header"] = "Gäste";
$install_lang["booking_guests"]["option"]["1"] = "1";
$install_lang["booking_guests"]["option"]["2"] = "2";
$install_lang["booking_guests"]["option"]["3"] = "3";
$install_lang["booking_guests"]["option"]["4"] = "4";
$install_lang["booking_guests"]["option"]["5"] = "5";
$install_lang["booking_guests"]["option"]["6"] = "6";
$install_lang["booking_guests"]["option"]["7"] = "7";
$install_lang["booking_guests"]["option"]["8"] = "8";
$install_lang["booking_guests"]["option"]["9"] = "9";
$install_lang["booking_guests"]["option"]["10"] = "10";
$install_lang["booking_guests"]["option"]["11"] = "10+";


<?php

$install_lang["notification_admin_booking_approve"] = "Buchungsanfrage ist genehmigt";
$install_lang["notification_admin_booking_decline"] = "Buchungsanfrage wurde abgelehnt";
$install_lang["notification_admin_booking_request"] = "Neue Buchungsanfrage vorhanden";
$install_lang["notification_listing_booking_approve"] = "Buchungsanfrage ist genehmigt";
$install_lang["notification_listing_booking_decline"] = "Buchungsanfrage wurde abgelehnt";
$install_lang["notification_listing_booking_request"] = "Neue Buchungsanfrage vorhanden";
$install_lang["notification_listing_need_moderate"] = "Neue Angebote warten auf Freigabe";
$install_lang["notification_listing_service_enabled"] = "kostenpflichtige Dienstleistungen sind aktiviert";
$install_lang["notification_listing_service_expired"] = "kostenpflichtige Dienstleistungen sind abgelaufen";
$install_lang["notification_listing_status_updated"] = "Angebotsstatus ist aktualisiert";
$install_lang["notification_share_listing"] = "Teilen Sie die Angebote per Mail";
$install_lang["tpl_admin_booking_approve_content"] = "Hallo admin,\n\nDie Buchung ([name], [user]) ist auf [domain] genehmigt worden.\n\nViele Grüße,\n[name_from]";
$install_lang["tpl_admin_booking_approve_subject"] = "[domain] |Buchunsanfrage ist genehmigt";
$install_lang["tpl_admin_booking_decline_content"] = "Hello admin,\n\nBooking ([name], [user]) is declined on [domain].\n\nBest regards,\n[name_from]";
$install_lang["tpl_admin_booking_decline_subject"] = "[domain] |Buchungsanfrage ist abgelehnt";
$install_lang["tpl_admin_booking_request_content"] = "Hallo admin,\n\nEs gibt neue Buchungsanfragen auf [domain]. Gehe in  Administrationsbereich > Angebote > Buchung um es anzuzeigen.\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_admin_booking_request_subject"] = "[domain] | Neue Buchungsanfrage";
$install_lang["tpl_listing_booking_approve_content"] = "Hallo [fname],\n\nIhre Buchung ([name], [user]) auf [domain] ist genehmigt.\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_listing_booking_approve_subject"] = "[domain] | Buchungsanfrage ist genehmigt";
$install_lang["tpl_listing_booking_decline_content"] = "Hallo [fname],\n\nIhre Buchung ([name], [user]) auf [domain] ist abgelehnt.\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_listing_booking_decline_subject"] = "[domain] | Buchungsanfrage ist abgelehnt";
$install_lang["tpl_listing_booking_request_content"] = "Hallo [fname],\n\nEs gibt eine neue Buchung von [Name] von [Benutzer] auf [domain].\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_listing_booking_request_subject"] = "[domain] | Neue Buchungsanfrage";
$install_lang["tpl_listing_need_moderate_content"] = "Hallo admin,\n\nEs gibt ein neues Angebot das  auf Moderation auf [domain] wartet. Gehe in Administrationsbereich > Angebote > Moderation um es anzuzeigen.\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_listing_need_moderate_subject"] = "[domain] | Neue Angebote warten auf Genehmigung";
$install_lang["tpl_listing_service_enabled_content"] = "Hallo [user],\n\nKostenpflichtiger Service [name] ist aktiviert und bis [Datum] aktiv.\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_listing_service_enabled_subject"] = "[domain] | Kostenpflichter Service abgelaufen";
$install_lang["tpl_listing_service_expired_content"] = "Hallo [user],\n\nKostenpflichtiger Service [Name] ist abgelaufen. Möchten Sie den Aktivitätszeitraum verlängern?\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_listing_service_expired_subject"] = "[domain] | Kostenpflichter Service abgelaufen";
$install_lang["tpl_listing_status_updated_content"] = "Hallo [user],\n\nIhre Angebote werden aktualisiert.\n[listing] - [status]\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_listing_status_updated_subject"] = "[domain] | Angebotsstatus ist aktualisert";
$install_lang["tpl_share_listing_content"] = "Hallo [user],\n\nTeilen Sie Ihre Angebote  [listing_name] ([listing_url]), an sie:\n\n[message]\n\nMit freundlichen Grüßen,\n[name_from]";
$install_lang["tpl_share_listing_subject"] = "Teilen Sie Angebote (ID=[listing_id]) an Sie ";


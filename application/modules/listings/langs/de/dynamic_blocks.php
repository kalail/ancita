<?php

$install_lang["block_buy_listings_block"] = "Angebote für Kauf";
$install_lang["block_buy_listings_categories_block"] = "Angebote für Kauf nach Kategorien";
$install_lang["block_buy_listings_regions_block"] = "Angebote für Kauf nach Regionen";
$install_lang["block_featured_listings_block"] = "Empfohlene Angebote";
$install_lang["block_form_slider_listings_block"] = "Slider mit Angeboten &amp; Suchfeld";
$install_lang["block_latest_added_listings_block"] = "neueste hinzugefügte Angebote";
$install_lang["block_lease_listings_block"] = "Angebote für Leasing";
$install_lang["block_lease_listings_categories_block"] = "Angebote für Leasing nach Kategorien";
$install_lang["block_lease_listings_regions_block"] = "Angebote für Leasing nach Regionen";
$install_lang["block_listings_wish_lists_block"] = "Empfohlene Immobilien";
$install_lang["block_rent_listings_block"] = "Angebote für Miete";
$install_lang["block_rent_listings_categories_block"] = "Angebote für Miete nach Kategorien";
$install_lang["block_rent_listings_regions_block"] = "Angebote für Miete nach Regionen";
$install_lang["block_sale_listings_block"] = "Angebote für Verkauf";
$install_lang["block_sale_listings_categories_block"] = "Angebote für Verkauf nach Kategorien";
$install_lang["block_sale_listings_regions_block"] = "Angebote für Verkauf nach Regionen";
$install_lang["block_slider_listings_block"] = "Slider mit Angeboten";
$install_lang["param_buy_listings_block_count"] = "zählen";
$install_lang["param_featured_listings_block_count"] = "zählen";
$install_lang["param_form_slider_listings_block_count"] = "zählen";
$install_lang["param_latest_added_listings_block_count"] = "zählen";
$install_lang["param_lease_listings_block_count"] = "zählen";
$install_lang["param_listings_wish_lists_block_count"] = "Anzahl der Artikel";
$install_lang["param_rent_listings_block_count"] = "Anzahl der Artikel";
$install_lang["param_sale_listings_block_count"] = "zählen";
$install_lang["param_slider_listings_block_count"] = "zählen";
$install_lang["view_buy_listings_block_gallery_big"] = "Galerie (200x200)";
$install_lang["view_buy_listings_block_gallery_small"] = "Galerie (100x100)";
$install_lang["view_buy_listings_block_scroller_big"] = "Scroller (200x200)";
$install_lang["view_buy_listings_block_scroller_small"] = "Scroller (100x100)";
$install_lang["view_buy_listings_categories_block_default"] = "Vorgabe";
$install_lang["view_buy_listings_regions_block_default"] = "Vorgabe";
$install_lang["view_featured_listings_block_gallery_big"] = "Galerie (200x200)";
$install_lang["view_featured_listings_block_gallery_small"] = "Galerie (100x100)";
$install_lang["view_featured_listings_block_scroller_big"] = "Scroller (200x200)";
$install_lang["view_featured_listings_block_scroller_small"] = "Scroller (100x100)";
$install_lang["view_form_slider_listings_block_featured_1600_440"] = "Sonderangebot (1600x400)";
$install_lang["view_form_slider_listings_block_featured_980_440"] = "Sonderangebot (980x440)";
$install_lang["view_form_slider_listings_block_latest_added_1600_440"] = "Neueste hinzugefügt (1600x440)";
$install_lang["view_form_slider_listings_block_latest_added_980_440"] = "Neueste hinzugefügt (980x440)";
$install_lang["view_form_slider_listings_block_rent_1600_440"] = "Zum Vermieten (1600x440)";
$install_lang["view_form_slider_listings_block_rent_980_440"] = "Zum Vermieten (980x440)";
$install_lang["view_form_slider_listings_block_sale_1600_440"] = "Zum Verkauf (1600x440)";
$install_lang["view_form_slider_listings_block_sale_980_440"] = "Zum Verkauf (980x440)";
$install_lang["view_latest_added_listings_block_gallery_big"] = "Galerie (200x200)";
$install_lang["view_latest_added_listings_block_gallery_small"] = "Galerie (100x100)";
$install_lang["view_latest_added_listings_block_scroller_big"] = "Scroller (200x200)";
$install_lang["view_latest_added_listings_block_scroller_small"] = "Scroller (100x100)";
$install_lang["view_lease_listings_block_gallery_big"] = "Galerie (200x200)";
$install_lang["view_lease_listings_block_gallery_small"] = "Galerie (100x100)";
$install_lang["view_lease_listings_block_scroller_big"] = "Scroller (200x200)";
$install_lang["view_lease_listings_block_scroller_small"] = "Scroller (100x100)";
$install_lang["view_lease_listings_categories_block_default"] = "Vorgabe";
$install_lang["view_lease_listings_regions_block_default"] = "Vorgabe";
$install_lang["view_listings_wish_lists_block_default"] = "Vorgabe";
$install_lang["view_rent_listings_block_gallery_big"] = "Galerie (200x200)";
$install_lang["view_rent_listings_block_gallery_small"] = "Galerie (100x100)";
$install_lang["view_rent_listings_block_scroller_big"] = "Scroller (200x200)";
$install_lang["view_rent_listings_block_scroller_small"] = "Scroller (100x100)";
$install_lang["view_rent_listings_categories_block_default"] = "Vorgabe";
$install_lang["view_rent_listings_regions_block_default"] = "Vorgabe";
$install_lang["view_sale_listings_block_gallery_big"] = "Galerie (200x200)";
$install_lang["view_sale_listings_block_gallery_small"] = "Galerie (100x100)";
$install_lang["view_sale_listings_block_scroller_big"] = "Scroller (200x200)";
$install_lang["view_sale_listings_block_scroller_small"] = "Scroller (100x100)";
$install_lang["view_sale_listings_categories_block_default"] = "Vorgabe";
$install_lang["view_sale_listings_regions_block_default"] = "Vorgabe";
$install_lang["view_slider_listings_block_featured_654_395"] = "Sonderangebot (620x400)";
$install_lang["view_slider_listings_block_featured_980_440"] = "Sonderangebot (620x400)";
$install_lang["view_slider_listings_block_latest_added_654_395"] = "Neueste hinzugefügt (620x400)";
$install_lang["view_slider_listings_block_latest_added_980_440"] = "Neueste hinzugefügt (980x440)";
$install_lang["view_slider_listings_block_rent_654_395"] = "Zum Vermieten (620x400)";
$install_lang["view_slider_listings_block_rent_980_440"] = "Zum Vermieten (980x440)";
$install_lang["view_slider_listings_block_sale_654_395"] = "Zum Verkauf (620x400)";
$install_lang["view_slider_listings_block_sale_980_440"] = "Zum Verkauf (980x440)";


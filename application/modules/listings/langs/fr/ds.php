<?php

$install_lang["operation_type"]["header"] = "Type d'annonce";
$install_lang["operation_type"]["option"]["sale"] = "à vendre";
$install_lang["operation_type"]["option"]["buy"] = "pour acheter";
$install_lang["operation_type"]["option"]["rent"] = "à louer";
$install_lang["operation_type"]["option"]["lease"] = "à louer";

$install_lang["operation_type_search"]["header"] = "Type d'annonce (recherche)";
$install_lang["operation_type_search"]["option"]["sale"] = "à vendre";
$install_lang["operation_type_search"]["option"]["buy"] = "Pour acheter";
$install_lang["operation_type_search"]["option"]["rent"] = "Pour louer";
$install_lang["operation_type_search"]["option"]["lease"] = "Pour louer";

$install_lang["square_units"]["header"] = "Unités carrées";
$install_lang["square_units"]["option"]["1"] = "pi";
$install_lang["square_units"]["option"]["2"] = "m²";

$install_lang["posted_within"]["header"] = "Posté";
$install_lang["posted_within"]["option"]["30"] = "dans les 30 jours";
$install_lang["posted_within"]["option"]["7"] = "dans les 7 jours";
$install_lang["posted_within"]["option"]["3"] = "dans les 3 jours";
$install_lang["posted_within"]["option"]["1"] = "hier";

$install_lang["radius_data"]["header"] = "Rayon";
$install_lang["radius_data"]["option"]["1"] = "1 km";
$install_lang["radius_data"]["option"]["5"] = "5 km";
$install_lang["radius_data"]["option"]["10"] = "10 km";

$install_lang["price_period"]["header"] = "Période";
$install_lang["price_period"]["option"]["1"] = "par jour";
$install_lang["price_period"]["option"]["2"] = "par mois";

$install_lang["price_period_unit"]["header"] = "Period unit";
$install_lang["price_period_unit"]["option"]["1"] = "days";
$install_lang["price_period_unit"]["option"]["2"] = "months";

$install_lang["price_type"]["header"] = "Prix";
$install_lang["price_type"]["option"]["1"] = "par chambre";
$install_lang["price_type"]["option"]["2"] = "par personne";

$install_lang["booking_status"]["header"] = "Statut de la réservation";
$install_lang["booking_status"]["option"]["open"] = "Disponible";
$install_lang["booking_status"]["option"]["close"] = "Fermé";
$install_lang["booking_status"]["option"]["book"] = "Indisponible";
$install_lang["booking_status"]["option"]["wait"] = "En attente";

$install_lang["booking_guests"]["header"] = "Invités";
$install_lang["booking_guests"]["option"]["1"] = "1";
$install_lang["booking_guests"]["option"]["2"] = "2";
$install_lang["booking_guests"]["option"]["3"] = "3";
$install_lang["booking_guests"]["option"]["4"] = "4";
$install_lang["booking_guests"]["option"]["5"] = "5";
$install_lang["booking_guests"]["option"]["6"] = "6";
$install_lang["booking_guests"]["option"]["7"] = "7";
$install_lang["booking_guests"]["option"]["8"] = "8";
$install_lang["booking_guests"]["option"]["9"] = "9";
$install_lang["booking_guests"]["option"]["10"] = "10";
$install_lang["booking_guests"]["option"]["11"] = "10+";


<?php

$install_lang["notification_admin_booking_approve"] = "Demande de réservation approuvé";
$install_lang["notification_admin_booking_decline"] = "Demande de réservation refusée";
$install_lang["notification_admin_booking_request"] = "Nouvelle demande de réservation";
$install_lang["notification_listing_booking_approve"] = "Demande de réservation approuvé";
$install_lang["notification_listing_booking_decline"] = "Demande de réservation refusée";
$install_lang["notification_listing_booking_request"] = "Nouvelle demande de réservation";
$install_lang["notification_listing_need_moderate"] = "Annonce en attente de modération";
$install_lang["notification_listing_service_enabled"] = "Service payant activé";
$install_lang["notification_listing_service_expired"] = "Service payant expiré";
$install_lang["notification_listing_status_updated"] = "Listing statut est mis à jour";
$install_lang["notification_share_listing"] = "Partager annonce par courrier";
$install_lang["tpl_admin_booking_approve_content"] = "Bonjour Admin,\n\nRéservation ([name], [user]) est approuvé au [domain].\n\nCordialement,\n[name_from]";
$install_lang["tpl_admin_booking_approve_subject"] = "[domain] | demande de réservation est approuvé";
$install_lang["tpl_admin_booking_decline_content"] = "Bonjour Admin,\n\nRéservation ([name], [user]) refusée sur [domain].\n\nCordialement,\n[name_from]";
$install_lang["tpl_admin_booking_decline_subject"] = "[domain] | demande de réservation est refusée";
$install_lang["tpl_admin_booking_request_content"] = "Bonjour Admin,\n\nIl ya de nouvelles demandes de réservation sur [domain]. Panneau d'administration d'accès > Inscriptions > Réservation pour la voir.\n\nCordialement,\n[name_from]";
$install_lang["tpl_admin_booking_request_subject"] = "[domain] | Nouvelle demande de réservation";
$install_lang["tpl_listing_booking_approve_content"] = "Bonjour [fname],\n\nVotre réservation ([name], [user]) est approuvé au [domain].\n\nCordialement,\n[name_from]";
$install_lang["tpl_listing_booking_approve_subject"] = "[domain] | demande de réservation est approuvé";
$install_lang["tpl_listing_booking_decline_content"] = "Bonjour [fname],\n\nVotre réservation ([name], [user]) se décline sur [domain].\n\nCordialement,\n[name_from]";
$install_lang["tpl_listing_booking_decline_subject"] = "[domain] | demande de réservation est refusée";
$install_lang["tpl_listing_booking_request_content"] = "Bonjour [fname],\n\nIl y a une nouvelle réservation de [name] de [user] sur [domain].\n\nCordialement,\n[name_from]";
$install_lang["tpl_listing_booking_request_subject"] = "[domain] | Nouvelle demande de réservation";
$install_lang["tpl_listing_need_moderate_content"] = "Bonjour admin,\n\nIl ya de nouvelles annonces en attente de modération sur [domain].  Panneau d'accès d'administration > Inscriptions > Modération pour la voir.\n\nCordialement,\n[name_from]";
$install_lang["tpl_listing_need_moderate_subject"] = "[domain] | Annonce en attente de modération";
$install_lang["tpl_listing_service_enabled_content"] = "Bonjour [user],\n\nService payant [name] est activé et actif jusqu'à [date].\n\nCordialement,\n[name_from]";
$install_lang["tpl_listing_service_enabled_subject"] = "[domain] | Service payant activé";
$install_lang["tpl_listing_service_expired_content"] = "Bonjour [user],\n\nService payant [name] a expiré. Voulez-vous prolonger sa période d'activité?\n\nCordialement,\n[name_from]";
$install_lang["tpl_listing_service_expired_subject"] = "[domain] | Service payant expiré";
$install_lang["tpl_listing_status_updated_content"] = "Bonjour [user],\n\nVotre statut d'inscription est mis à jour.\n[listing] - [status]\n\nCordialement,\n[name_from]";
$install_lang["tpl_listing_status_updated_subject"] = "[domain] | statut d'annonce de mise à jour";
$install_lang["tpl_share_listing_content"] = "Bonjour [user],\n\nPartager annonce [listing_name] ([listing_url]) pour vous:\n\n[message]\n\nCordialement,\n[name_from]";
$install_lang["tpl_share_listing_subject"] = "Partager annonce (ID=[listing_id]) pour vous";


<?php

$install_lang["block_buy_listings_block"] = "Annonces pour acheter";
$install_lang["block_buy_listings_categories_block"] = "Annonces pour acheter par catégories";
$install_lang["block_buy_listings_regions_block"] = "Annonces pour acheter par régions";
$install_lang["block_featured_listings_block"] = "annonces en vedette";
$install_lang["block_form_slider_listings_block"] = "Curseur avec des annonces &amp; formulaire de recherche";
$install_lang["block_latest_added_listings_block"] = "Annonces récentes";
$install_lang["block_lease_listings_block"] = "Annonces de location";
$install_lang["block_lease_listings_categories_block"] = "Annonces de location par catégories";
$install_lang["block_lease_listings_regions_block"] = "Annonces de location par régions";
$install_lang["block_listings_wish_lists_block"] = "propriétés recommandées";
$install_lang["block_rent_listings_block"] = "À louer";
$install_lang["block_rent_listings_categories_block"] = "À louer par catégories";
$install_lang["block_rent_listings_regions_block"] = "à louer par région";
$install_lang["block_sale_listings_block"] = "Annonces de vente";
$install_lang["block_sale_listings_categories_block"] = "Annonces de vente par catégories";
$install_lang["block_sale_listings_regions_block"] = "Annonces de vente par régions";
$install_lang["block_slider_listings_block"] = "Curseur avec ventes";
$install_lang["param_buy_listings_block_count"] = "compter";
$install_lang["param_featured_listings_block_count"] = "compter";
$install_lang["param_form_slider_listings_block_count"] = "compter";
$install_lang["param_latest_added_listings_block_count"] = "compter";
$install_lang["param_lease_listings_block_count"] = "compter";
$install_lang["param_listings_wish_lists_block_count"] = "Nombre de pièces";
$install_lang["param_rent_listings_block_count"] = "Nombre de pièces";
$install_lang["param_sale_listings_block_count"] = "compter";
$install_lang["param_slider_listings_block_count"] = "compter";
$install_lang["view_buy_listings_block_gallery_big"] = "Galerie (200x200)";
$install_lang["view_buy_listings_block_gallery_small"] = "Galerie (100x100)";
$install_lang["view_buy_listings_block_scroller_big"] = "Scroller (200x200)";
$install_lang["view_buy_listings_block_scroller_small"] = "Scroller (100x100)";
$install_lang["view_buy_listings_categories_block_default"] = "Par défaut";
$install_lang["view_buy_listings_regions_block_default"] = "Par défaut";
$install_lang["view_featured_listings_block_gallery_big"] = "Galerie (200x200)";
$install_lang["view_featured_listings_block_gallery_small"] = "Galerie (100x100)";
$install_lang["view_featured_listings_block_scroller_big"] = "Scroller (200x200)";
$install_lang["view_featured_listings_block_scroller_small"] = "Scroller (100x100)";
$install_lang["view_form_slider_listings_block_featured_1600_440"] = "Vedette (1600x440)";
$install_lang["view_form_slider_listings_block_featured_980_440"] = "Vedette (980x440)";
$install_lang["view_form_slider_listings_block_latest_added_1600_440"] = "Ajouts récents (1600x440)";
$install_lang["view_form_slider_listings_block_latest_added_980_440"] = "Ajouts récents (980x440)";
$install_lang["view_form_slider_listings_block_rent_1600_440"] = "À louer (1600x440)";
$install_lang["view_form_slider_listings_block_rent_980_440"] = "À louer (980x440)";
$install_lang["view_form_slider_listings_block_sale_1600_440"] = "À vendre (1600x440)";
$install_lang["view_form_slider_listings_block_sale_980_440"] = "À vendre (980x440)";
$install_lang["view_latest_added_listings_block_gallery_big"] = "Galerie (200x200)";
$install_lang["view_latest_added_listings_block_gallery_small"] = "Galerie (100x100)";
$install_lang["view_latest_added_listings_block_scroller_big"] = "Scroller (200x200)";
$install_lang["view_latest_added_listings_block_scroller_small"] = "Scroller (100x100)";
$install_lang["view_lease_listings_block_gallery_big"] = "Galerie (200x200)";
$install_lang["view_lease_listings_block_gallery_small"] = "Galerie (100x100)";
$install_lang["view_lease_listings_block_scroller_big"] = "Scroller (200x200)";
$install_lang["view_lease_listings_block_scroller_small"] = "Scroller (100x100)";
$install_lang["view_lease_listings_categories_block_default"] = "Par défaut";
$install_lang["view_lease_listings_regions_block_default"] = "Par défaut";
$install_lang["view_listings_wish_lists_block_default"] = "Par défaut";
$install_lang["view_rent_listings_block_gallery_big"] = "Galerie (200x200)";
$install_lang["view_rent_listings_block_gallery_small"] = "Galerie (100x100)";
$install_lang["view_rent_listings_block_scroller_big"] = "Scroller (200x200)";
$install_lang["view_rent_listings_block_scroller_small"] = "Scroller (100x100)";
$install_lang["view_rent_listings_categories_block_default"] = "Par défaut";
$install_lang["view_rent_listings_regions_block_default"] = "Par défaut";
$install_lang["view_sale_listings_block_gallery_big"] = "Galerie (200x200)";
$install_lang["view_sale_listings_block_gallery_small"] = "Galerie (100x100)";
$install_lang["view_sale_listings_block_scroller_big"] = "Scroller (200x200)";
$install_lang["view_sale_listings_block_scroller_small"] = "Scroller (100x100)";
$install_lang["view_sale_listings_categories_block_default"] = "Par défaut";
$install_lang["view_sale_listings_regions_block_default"] = "Par défaut";
$install_lang["view_slider_listings_block_featured_654_395"] = "Vedette (620x400)";
$install_lang["view_slider_listings_block_featured_980_440"] = "Vedette (980x440)";
$install_lang["view_slider_listings_block_latest_added_654_395"] = "Ajouts récents (620x400)";
$install_lang["view_slider_listings_block_latest_added_980_440"] = "Ajouts récents (980x440)";
$install_lang["view_slider_listings_block_rent_654_395"] = "À louer (620x400)";
$install_lang["view_slider_listings_block_rent_980_440"] = "À louer (980x440)";
$install_lang["view_slider_listings_block_sale_654_395"] = "À vendre (620x400)";
$install_lang["view_slider_listings_block_sale_980_440"] = "À vendre (980x440)";


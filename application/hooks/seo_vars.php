<?php

/**
 * Seo variables hook
 * 
 * @package PG_Core
 * @subpackage application
 * @category	hooks
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Irina Lebedeva <irina@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2009-12-02 15:07:07 +0300 (Ср, 02 дек 2009) $ $Author: irina $
 **/

if ( ! function_exists('seo_replace_vars')){
	function seo_replace_vars(){
		if(INSTALL_MODULE_DONE){

			$CI = &get_instance();
			$segment_count = count($CI->uri->rsegments);

			if($CI->pg_seo->use_seo_links_rewrite && $segment_count > 2){
				$module = $CI->router->fetch_class();
				$controller = $CI->router->fetch_class(true);
				$method = $CI->router->fetch_method();
				$controller_type = (substr($controller, 0, 6) == "admin_")?"admin":"user";

				//// is module uses seo links?
				$module_data = $CI->pg_seo->get_seo_module_by_gid($module);

				if(!empty($module_data)){
					//// get default data
					$settings = $CI->pg_seo->get_default_settings($controller_type, $module, $method);

					//// if this method get rewrite params
					if(!empty($settings["url_vars"])){
						$index = 3;
						foreach($settings["url_vars"] as $general_var => $replaces){
							$vars[$index] = $general_var;
							$index++;
						}
						
						for($i=3; $i<=$segment_count; $i++){
							$temp = $CI->uri->rsegments[$i];
							$temp_array = explode(":", $temp);
							if(count($temp_array) > 1){
								$var_name = $temp_array[0];
								$var_value = urldecode(urldecode(urldecode($temp_array[1])));
								$CI->uri->rsegments[$i] = $CI->pg_seo->get_module_rewrite_var($controller_type, $module, $method, $var_name, $vars[$i], $var_value);
							}
						}
					}
				}
			}
		
		}
	}


}

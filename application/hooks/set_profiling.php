<?php

/**
 * Database profiling hook
 * 
 * @package PG_Core
 * @subpackage application
 * @category	hooks
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Irina Lebedeva <irina@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2009-12-02 15:07:07 +0300 (Ср, 02 дек 2009) $ $Author: irina $
 **/
 
if ( ! function_exists('set_profiling')){
	$start_exec_time = 0;
	function set_profiling(){
		if(USE_PROFILING){
			global $start_exec_time;
			$start_exec_time = getmicrotime();
		}
	}
}
if ( ! function_exists('getmicrotime')){
	function getmicrotime(){
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
}

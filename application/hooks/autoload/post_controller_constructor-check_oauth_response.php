<?php

/**
 * Check oauth responce hook
 * 
 * @package PG_Core
 * @subpackage application
 * @category	hooks
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Irina Lebedeva <irina@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2009-12-02 15:07:07 +0300 (Ср, 02 дек 2009) $ $Author: irina $
 **/

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

if (!function_exists('check_oauth_response')) {

	function check_oauth_response() {
		if (INSTALL_MODULE_DONE) {

			$code = isset($_GET['code']) ? $_GET['code'] : false;
			if ($code) {
				$state = isset($_GET['state']) ? $_GET['state'] : false;
				if ($state)
					redirect(site_url($state . '?code=' . $code));
			}
		}
		return;
	}

}

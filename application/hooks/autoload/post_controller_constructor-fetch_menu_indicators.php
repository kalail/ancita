<?php 

/**
 * Load menu indicators hook
 * 
 * @package PG_Core
 * @subpackage application
 * @category	hooks
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Irina Lebedeva <irina@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2009-12-02 15:07:07 +0300 (Ср, 02 дек 2009) $ $Author: irina $
 **/
 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('fetch_menu_indicators')){

	function fetch_menu_indicators(){
		$CI = & get_instance();
		if(INSTALL_DONE) {
			if(!$CI->pg_module->get_module_config('menu', 'use_indicators') || !$CI->session->userdata('auth_type')) {
				return false;
			}
			$CI->load->model('Menu_model');
			$CI->load->model('menu/models/Indicators_model');
			$CI->Menu_model->indicators = $CI->Indicators_model->get(
			$CI->session->userdata('auth_type'),
			$CI->session->userdata('user_id'));
		}
	}

}

<?php 

/**
 * Api output hook
 * 
 * @package PG_Core
 * @subpackage application
 * @category	hooks
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Irina Lebedeva <irina@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2009-12-02 15:07:07 +0300 (Ср, 02 дек 2009) $ $Author: irina $
 **/
 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('output_api_content')){

	function output_api_content(){
		$CI = & get_instance();
		if ($CI->router->is_api_class){
			$CI->load->helper('api');
			echo get_api_content();
			exit;
		}
	}

}

<?php  

/**
 * Assign user session hook
 * 
 * @package PG_Core
 * @subpackage application
 * @category	hooks
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Irina Lebedeva <irina@pilotgroup.net>
 * @version $Revision: 2 $ $Date: 2009-12-02 15:07:07 +0300 (Ср, 02 дек 2009) $ $Author: irina $
 **/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('assign_user_session')){

	function assign_user_session(){
		if(INSTALL_MODULE_DONE){

			$CI = &get_instance();
			$CI->template_lite->assign('user_session_data', $CI->session->all_userdata());
			
		}
		return;
	}

}

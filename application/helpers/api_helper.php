<?php

/**
 * API management
 * 
 * @package PG_Core
 * @subpackage application
 * @category	helpers
 * @copyright Pilot Group <http://www.pilotgroup.net/>
 * @author Mikhail Makeev <mmakeev@pilotgroup.net>
 * @version $Revision: 68 $ $Date: 2010-01-11 16:02:23 +0300 (Пн, 11 янв 2010) $ $Author: irina $
 **/

if (!function_exists('get_api_content')) {

	function get_api_content() {
		$CI = & get_instance();
		$type = trim(strip_tags($CI->input->get_post('type', true)));
		$use_xml = $CI->pg_module->get_module_config('get_token', 'use_xml');
		if ($type == 'xml' && $use_xml){
			$CI->load->library('array2xml');
			return $CI->array2xml->convert($CI->api_content);
		} else {
			return json_encode($CI->api_content);	
		}
	}

}

<!DOCTYPE html>
<html DIR="{$_LANG.rtl}">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta charset="utf-8"> 
	<meta http-equiv="expires" content="0">
	<meta http-equiv="pragma" content="no-cache">
     <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="revisit-after" content="3 days">
	{seotag tag='robots|title|description|keyword|canonical|og_title|og_type|og_url|og_image|og_site_name|og_description'}
	
	<link href='{$site_root}application/views/pilot/font-awesome.css' rel='stylesheet' type='text/css'>
	{helper func_name=css helper_name=theme func_param=$load_type}
	{if $_LANG.rtl eq 'rtl'}<!--[if IE]><link rel="stylesheet" type="text/css" href="{$site_root}application/views/pilot/ie-rtl-fix.css" /><![endif]-->{/if}
	{literal}<!--[if gt IE 9]><style type="text/css">.fa .fa, .fa .fa:before, .fa .fa:after{filter: none;}</style><![endif]-->{/literal}
	{literal}<!--[if lte IE 9]><style type="text/css">.icon-mini-stack-shadow{display: inline-block;}</style><![endif]-->{/literal}
	<link rel="shortcut icon" href="{$site_root}favicon.ico">
	
	<script type="text/javascript">
		var site_url = '{$site_url}';
		var site_rtl_settings = '{$_LANG.rtl}';
		var site_error_position = 'center';
	</script>
	
	{helper func_name=js helper_name=theme func_param=$load_type}

	{helper func_name=banner_initialize module=banners}
	{helper func_name=show_social_networks_head module=social_networking}
</head>
<body dir="{$_LANG.rtl}">
{helper func_name=seo_traker helper_name=seo_module module=seo func_param='top'}
{*pg_include_file=demo_help_menu.html*}
{helper func_name=demo_panel helper_name=start func_param='user'}
{if $display_brouser_error}
	{helper func_name=available_brousers helper_name=start}
{/if}

	<div id="error_block">{foreach item=item from=$_PREDEFINED.error}{$item.text}<br>{/foreach}</div>
	<div id="info_block">{foreach item=item from=$_PREDEFINED.info}{$item.text}<br>{/foreach}</div>
	<div id="success_block">{foreach item=item from=$_PREDEFINED.success}{$item.text}<br>{/foreach}</div>

	{capture assign='header'}{strip}
		{block name=users_lang_select module=users}
		{block name=site_currency_select module=users}
		{block name=auth_links module=users}
		{block name=post_listing_button module=listings}
	{/strip}{/capture}
	{if $header}
	<div class="header">
		<div class="content">
			<div class="header-menu">
				<ul>{$header}</ul>
			</div>
			<div class="clr"></div>
		</div>
	</div>
	{/if}
	<div class="main">
		<div class="content">
			<header>
				<div class="logo">
					{if $header_type eq 'index'}
					<h1><a href="{$site_url}"><img src="{$base_url}{$logo_settings.path}" border="0" alt="{seotag tag='header_text'}" width="{$logo_settings.width}" height="{$logo_settings.height}"></a></h1>
					{else}
					<a href="{$site_url}"><img src="{$base_url}{$logo_settings.path}" border="0" alt="{helper func_name='seo_tags_default' func_param='header_text'}" width="{$logo_settings.width}" height="{$logo_settings.height}"></a>
					{/if}
					<nav class="top_menu">
						{if $auth_type eq 'user'}
						{menu gid=$user_session_data.user_type+'_main_menu' template='user_main_menu'}
						{else}
						{menu gid='guest_main_menu' template='user_main_menu'}
						{/if}
					</nav>
					<script>{literal}
						$(function(){
							$('.top_menu ul>li').each(function(i, item){
								var element = $(item);
								var submenu = element.find('.sub_menu_block');
								if(submenu.length == 0) return;
								element.children('a').bind('touchstart', function(){
									if(element.hasClass('hover')){
										element.removeClass('hover');
									}else{
										$('.top_menu ul>li').removeClass('hover');
										element.addClass('hover');
									}
									return false;
								});
								element.children('a').bind('touchenter', function(){
									element.removeClass('hover');
									element.trigger('mouseenter mouseleave');
									return false;
								}).bind('touchleave', function(){
									element.removeClass('hover').trigger('blur');
									return false;
								});
							
							
							});
						});
					{/literal}</script>
				</div>
			
				{if $header_type ne 'index'}
				{start_search_form type='line' show_data=1}
				{/if}
			</header>
			
			{breadcrumbs}
				

{strip}
{literal}
<style>
#nav{
	font-size: 13px;
	height:38px;
	background:#6e6f71 url({/literal}{$site_root}{$img_folder}{literal}demo/settings_nav_bg.gif) bottom repeat-x;
}
#nav .settings_nav{width: 980px; margin: 0px auto; color: #ffffff}
#nav a, #nav span{color: #ffffff}

.choice_btn{ background:url({/literal}{$site_root}{$img_folder}{literal}demo/choice_btn_left.gif) center left no-repeat; margin: 5px 0px; padding: 0 5px; }
.choice_btn.g{ background:url({/literal}{$site_root}{$img_folder}{literal}demo/btn_left_green.png) center left no-repeat; }
.choice_btn a{ color:#ffffff; padding: 0px 15px; text-decoration: none; line-height: 25px;}
.choice_btn_left{background:url({/literal}{$site_root}{$img_folder}{literal}demo/choice_btn_right.gif) center right no-repeat; line-height: 25px;}
.choice_btn_left.g{background:url({/literal}{$site_root}{$img_folder}{literal}demo/btn_right_green.png) center right no-repeat; position: relative}
.choice_btn li{list-style:none; cursor: pointer;}
.choice_btn span{color: #ffffff;background: url({/literal}{$site_root}{$img_folder}{literal}demo/arrowDown.png) center right no-repeat; padding:0 20px; margin-right: 20px; white-space: normal; line-height: 22px; }

.green_btn{cursor: pointer; background:url({/literal}{$site_root}{$img_folder}{literal}demo/btn_left_green.png) center left no-repeat; margin: 5px 0px; padding: 0 5px; }
.green_btn a{ color:#ffffff; padding: 0px 15px; text-decoration: none; line-height: 25px;}
.green_btn_left{background:url({/literal}{$site_root}{$img_folder}{literal}demo/btn_right_green.png) center right no-repeat; line-height: 25px; padding: 0 9px 0 7px;}

.rate_btn{ cursor: pointer; background:url({/literal}{$site_root}{$img_folder}{literal}demo/rate_btn_left.gif) center left no-repeat; margin: 5px 10px; padding-left: 29px;	cursor:pointer; }
.rate_btn_left{background:url({/literal}{$site_root}{$img_folder}{literal}demo/choice_btn_right.gif) center right no-repeat; padding-right: 10px;}
.rate_btn span{color: #ffffff; padding:0 20px 0 10px; line-height: 25px; white-space: normal;}

.back_site{line-height: 25px; margin-top: 5px; padding: 0 5px; }

.bn_btn{background:url({/literal}{$site_root}{$img_folder}{literal}demo/bn_btn_left.gif) center left no-repeat; padding-left:2px;	cursor:pointer; margin: 5px 0px;}
.bn_btn_left{height:25px;background:url({/literal}{$site_root}{$img_folder}{literal}demo/bn_btn_right.gif) center right no-repeat;}
.bn_btn span{ color: #ffffff; padding:0 20px; line-height: 25px; white-space: normal; }

.bn-r{float: right;}
.bn-l{float: left;}
.sub_menu{ padding:0;margin:4px 0 0;background:#ffffff;border:1px solid #464748;position: absolute;top:20px; left:-1px;width:165px;z-index:98;}
.sub_menu li.s_drop{position: relative; padding-left:20px; background:url({/literal}{$site_root}{$img_folder}{literal}demo/nex_c.gif) 150px 11px no-repeat;}
.sub_menu2{padding:0;margin:4px 0 0;background:#ffffff;border:1px solid #464748;position: absolute;top:-5px; left:160px;width:190px;z-index:99;}
.sub_menu2 li{color:#000000;border-bottom:1px solid #d2d2d2;}
.sub_menu li{padding-left:10px;color:#000000; padding: 5px 10px;border-bottom:1px solid #d2d2d2;}
.sub_menu li:hover{background:#c2e6c6;color:#000000;}
.sub_menu li.s_drop:hover{color:#000000;background:#c2e6c6 url({/literal}{$site_root}{$img_folder}{literal}demo/nex_c.gif) 150px 11px no-repeat;}
.sub_menu li.sub{padding:5px 5px 5px 20px;}
.sub_menu a, .sub_menu2 a{color: #208B2F !important; text-decoration: underline;}


</style>
{/literal}
{/strip}
<script type="text/javascript" src="{$site_root}{$img_folder}demo/jquery.dropdownPlain.js"></script>

<div id="nav">
	<div class="settings_nav">
		<div class="choice_btn bn-r" onclick="window.open('http://www.pilotgroup.net/questionnaire/feedback.php?fid=76&pid=2062&mode=light','feedback_questionnaire','width=600,resizable=yes,scrollbars=1'); return false;">
			<div class="choice_btn_left">
				<a href="#" onclick="window.open('http://www.pilotgroup.net/questionnaire/feedback.php?fid=76&pid=2062&mode=light','feedback_questionnaire','width=600,resizable=yes,scrollbars=1'); return false;">Feedback</a>
			</div>
		</div>
		<div class="back_site bn-r">How can we make this product better for you ?</div>


		<div class="green_btn bn-l" onclick="_gaq.push(['_link', 'http://www.realtysoft.pro']); window.open('http://www.realtysoft.pro/realestate/features/main/', '_blank');">
			<div class="green_btn_left">
				<span>View features</span>
			</div>
		</div>
		<div class="green_btn bn-l" onclick="_gaq.push(['_link', 'http://www.realtysoft.pro']); window.open('http://www.pilotgroup.net/services/we_customize.php' , '_blank');">
			<div class="green_btn_left">
				<span>Need extra?</span>
			</div>
		</div>

		<ul class="demo-dropdown choice_btn g bn-l">
			<li class="choice_btn_left g">
				<span>Navigation</span>
				<ul class="sub_menu" style="visibility: hidden; display: none;">
					<li><a href="{$site_url}admin">Admin Mode</a></li>
					<li class="s_drop">
						User Mode
						<ul class="sub_menu2" style="visibility: hidden; display: none;">
						<li><a href="{$site_url}start/demo/private">Private Person</a></li>
						<li><a href="{$site_url}start/demo/company">RealEstate Company</a></li>
						<li><a href="{$site_url}start/demo/agent">RealEstate Agent</a></li>
						</ul>
					</li>
				</ul>
			</li>
		</ul>
		<div class="green_btn bn-l" onclick="window.open('http://www.pilotgroup.net/about/clients.php' , '_blank');">
			<div class="green_btn_left">
				<span>Showcase</span>
			</div>
		</div>
		
		<div class="bn_btn bn-l" onclick="document.location.href='http://www.realtysoft.pro/pricing.php';">
			<div class="bn_btn_left">
				<span>Buy now</span>
			</div>
		</div>


	</div>
</div>
{literal}
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-149347-4']);
	_gaq.push(['_setDomainName', 'realtysoft.pro']);
	_gaq.push(['_setAllowLinker', true]);
	_gaq.push(['_trackPageview']);

	_gaq.push(['pg._setAccount', 'UA-43414725-1']);
	_gaq.push(['pg._setDomainName', 'realtysoft.pro']);
	_gaq.push(['pg._setAllowLinker', true]);
	_gaq.push(['pg._trackPageview']);

	(function(){ 
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; 
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; 
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); 
	})();
</script>
<script type="text/javascript">
 var __lc = {};
 __lc.license = 1083102;
 (function() { var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true; lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s); })();
</script>
{/literal}
<script type="text/javascript" src="//use.typekit.net/rps2ais.js"></script>
<script type="text/javascript">{literal}try{Typekit.load();}catch(e){}{/literal}</script>
<style>{literal}html,body{font-family: 'proxima-nova' !important;}{/literal}</style>

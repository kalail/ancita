#!/bin/sh
BASEDIR=$(dirname $0)
PROJECT_PATH=$(cd $BASEDIR; pwd)

TYPE="$1"
REPACCESS="$2"

cd $PROJECT_PATH
hg clone $REPACCESS/pg_core/$TYPE/system system
hg clone $REPACCESS/pg_core/$TYPE/temp temp
hg clone $REPACCESS/pg_realestate_core/main/$TYPE/updates updates
hg clone $REPACCESS/pg_realestate_core/main/$TYPE/application application

cd $PROJECT_PATH/application
hg clone $REPACCESS/pg_core/$TYPE/application/libraries libraries

cd $PROJECT_PATH/application/modules
hg clone $REPACCESS/pg_core/$TYPE/application/modules/install install
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/ausers ausers
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/banners banners
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/contact contact
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/contact_us contact_us
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/content content
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/countries countries
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/cronjob cronjob
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/dynamic_blocks dynamic_blocks
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/export export
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/field_editor field_editor
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/file_uploads file_uploads
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/geomap geomap
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/get_token get_token
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/import import
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/languages languages
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/linker linker
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/listings listings
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/mail_list mail_list
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/mailbox mailbox
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/menu menu
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/mobile mobile
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/moderation moderation
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/news news
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/notifications notifications
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/payments payments
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/polls polls
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/properties properties
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/reviews reviews
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/seo seo
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/services services
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/site_map site_map
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/social_networking social_networking
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/spam spam
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/start start
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/subscriptions subscriptions
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/themes themes
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/upload_gallery upload_gallery
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/uploads uploads
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/users users
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/users_connections users_connections
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/users_payments users_payments
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/users_services users_services
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/video_uploads video_uploads
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/weather weather
hg clone $REPACCESS/pg_realestate_core/modules/$TYPE/widgets widgets
cd $PROJECT_PATH

echo "hg ci -A -m Init"
hg ci -A -m Init

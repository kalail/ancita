<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-01-12 12:55:31 India Standard Time */ ?>

<div class="edit-form n150">
	<div class="row header"><?php echo l('admin_header_calendar_period_management', 'listings', '', 'text', array()); ?></div>
	<div id="calendar" class="cal-type-<?php echo $this->_vars['listing']['price_period']; ?>
">
		<div class="calendar-nav">
			<div class="fleft">
									<?php switch($this->_vars['listing']['price_period']): case '1':  ?>
						<a href="#" id="prev_month"><?php echo l('link_month_prev', 'listings', '', 'text', array()); ?></a>
					<?php break; case '2':  ?>
						<a href="#" id="prev_year"><?php echo l('link_year_prev', 'listings', '', 'text', array()); ?></a> 
				<?php break; endswitch; ?>
			</div>
			<div class="fright">
									<?php switch($this->_vars['listing']['price_period']): case '1':  ?>
						<a href="#" id="next_month"><?php echo l('link_month_next', 'listings', '', 'text', array()); ?></a> 
					<?php break; case '2':  ?>
						<a href="#" id="next_year"><?php echo l('link_year_next', 'listings', '', 'text', array()); ?></a>
				<?php break; endswitch; ?>
			</div>
			<div class="clr"></div>
		</div>
	
		<div id="calendar_block">
			<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. listings. $this->module_templates.  $this->get_current_theme_gid('user', 'listings'). "calendar_block.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
		</div>
	
		<div class="statusbar">
			<?php echo tpl_function_ld(array('i' => 'booking_status','gid' => 'listings','assign' => 'booking_status'), $this);?>
			<?php if (is_array($this->_vars['booking_status']['option']) and count((array)$this->_vars['booking_status']['option'])): foreach ((array)$this->_vars['booking_status']['option'] as $this->_vars['key2'] => $this->_vars['item2']): ?>
			<?php if ($this->_vars['key2'] == 'open' || $this->_vars['key2'] == 'book'): ?>
			<span class="<?php echo $this->_vars['key2']; ?>
"></span> <?php echo $this->_vars['item2']; ?>

			<?php endif; ?>
			<?php endforeach; endif; ?>
		</div>
	</div>
</div>

<?php echo tpl_function_js(array('file' => 'listings-calendar.js','module' => 'listings'), $this);?>
<script><?php echo '
	var listings_calendar';  echo $this->_vars['rand'];  echo ';
	$(function(){
		listings_calendar';  echo $this->_vars['rand'];  echo ' = new listingsCalendar({
			siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
			ajaxCalendarUrl: \'admin/listings/ajax_get_calendar\',
			listingId: ';  echo $this->_vars['listing']['id'];  echo ',
			count: ';  echo $this->_vars['count'];  echo ',
			month: ';  echo $this->_vars['m'];  echo ',
			year: ';  echo $this->_vars['y'];  echo ',
		});
	});
'; ?>
</script>
<div class="clr"></div>
<?php echo tpl_function_js(array('file' => 'booking-form.js','module' => 'listings'), $this);?>
<table class="data" id="listing_periods">
	<tr>
		<th class="w250"><?php echo l('field_booking_period', 'listings', '', 'text', array()); ?></th>
		
		<th class="w70"><?php echo l('field_booking_price', 'listings', '', 'text', array()); ?>, <?php echo $this->_vars['current_price_currency']['abbr']; ?>
 <?php echo tpl_function_ld_option(array('i' => 'price_period','gid' => 'listings','option' => $this->_vars['data']['price_period']), $this);?></th>
		<th class="w250"><?php echo l('field_booking_comment', 'listings', '', 'text', array()); ?></th>
		<th class="w70">&nbsp;</th>
	</tr>
	<?php $this->assign('is_opened', 0); ?>
	<?php if (is_array($this->_vars['data']['booking']['periods']) and count((array)$this->_vars['data']['booking']['periods'])): foreach ((array)$this->_vars['data']['booking']['periods'] as $this->_vars['item']): ?>
	<?php if ($this->_vars['item']['status'] == 'open'): ?>
	<?php $this->assign('is_opened', 1); ?>
	<?php $this->assign('period', $this->_vars['item']); ?>
	<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. listings. $this->module_templates.  $this->get_current_theme_gid('', 'listings'). "calendar_period.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
	<?php endif; ?>
	<?php endforeach; endif; ?>
	<?php if (! $this->_vars['is_opened']): ?><tr><td colspan="5" class="center"><?php echo l('no_periods', 'listings', '', 'text', array()); ?></td></tr><?php endif; ?>
</table>
<div class="btn"><div class="l"><input type="submit" name="btn_period" value="<?php echo l('btn_add', 'start', '', 'button', array()); ?>" id="btn_period"></div></div>
<script><?php echo '
	$(function(){
		new bookingForm({
			siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
			listingId: \'';  echo $this->_vars['data']['id'];  echo '\',
			bookingBtn: \'btn_period\',
			cFormId: \'period_form\',
			isSavePeriod: false,
			urlGetForm: \'admin/listings/ajax_period_form/\',
			urlSaveForm: \'admin/listings/ajax_save_period/\',
			calendar: listings_calendar';  echo $this->_vars['rand'];  echo ',
			successCallback: function(id, data, calendar){
				var periods = $(\'#listing_periods tbody\');
				var row = periods.find(\'tr:nth-child(2)\').first();
				if(row.children().length == 1) row.remove();
				periods.find(\'tr:first-child\').after(data);
				window[\'period_\'+id].set_calendar(calendar);
			},
		});
	});
'; ?>
</script>
<div class="clr"></div>
<table class="data" id="listing_booked">
	<tr>
		<th class="w250"><?php echo l('adfield_booking_period', 'listings', '', 'text', array()); ?></th>
		
		<th class="w30">&nbsp;</th>
		<th class="w250"><?php echo l('field_booking_comment', 'listings', '', 'text', array()); ?></th>
		<th class="w70">&nbsp;</th>
	</tr>
	<?php $this->assign('is_booked', 0); ?>
	<?php if (is_array($this->_vars['data']['booking']['periods']) and count((array)$this->_vars['data']['booking']['periods'])): foreach ((array)$this->_vars['data']['booking']['periods'] as $this->_vars['item']): ?>
	<?php if ($this->_vars['item']['status'] == 'book'): ?>
	<?php $this->assign('is_booked', 1); ?>
	<?php $this->assign('period', $this->_vars['item']); ?>
	<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. listings. $this->module_templates.  $this->get_current_theme_gid('', 'listings'). "calendar_period.tpl", array('themes' => user));
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
	<?php endif; ?>
	<?php endforeach; endif; ?>
	<?php if (! $this->_vars['is_booked']): ?><tr><td colspan="5" class="center"><?php echo l('no_periods', 'listings', '', 'text', array()); ?></td></tr><?php endif; ?>	
</table>
<div class="btn"><div class="l"><input type="submit" value="<?php echo l('btn_add', 'start', '', 'button', array()); ?>" name="btn_order" id="btn_order"></div></div>
<script><?php echo '
	$(function(){
		new bookingForm({
			siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
			listingId: \'';  echo $this->_vars['data']['id'];  echo '\',
			bookingBtn: \'btn_order\',
			cFormId: \'order_form\',
			isSavePeriod: false,
			urlGetForm: \'admin/listings/ajax_order_form/\',
			urlSaveForm: \'admin/listings/ajax_save_order/\',
			calendar: listings_calendar';  echo $this->_vars['rand'];  echo ',
			successCallback: function(id, data, calendar){
				var periods = $(\'#listing_booked tbody\');
				var row = periods.find(\'tr:nth-child(2)\').first();
				if(row.children().length == 1) row.remove();
				periods.find(\'tr:first-child\').after(data);
				window[\'period_\'+id].set_calendar(calendar);
			},
		});
	});
'; ?>
</script>
<div class="clr"></div>


<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-02-10 05:21:25 India Standard Time */ ?>

		<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => "review_callback")); tpl_block_capture(array('assign' => "review_callback"), null, $this); ob_start();  echo '
			function(data){
				window.location.href = \'';  echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user'],'section' => 'reviews'), $this);?>#m_reviews<?php echo '\';
			}
		';  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		
		<?php if ($this->_vars['user_type'] == 'company'): ?><div itemscope itemtype="http://data-vocabulary.org/Organization"><?php endif; ?>
		
		<h1><?php echo tpl_function_seotag(array('tag' => 'header_text'), $this);?></h1>
		<div class="actions noPrint">
			<?php echo tpl_function_block(array('name' => 'button_contact','module' => 'mailbox','user_id' => $this->_vars['user']['id'],'user_type' => $this->_vars['user_type']), $this);?>
			<?php echo tpl_function_block(array('name' => 'send_review_block','module' => 'reviews','object_id' => $this->_vars['user']['id'],'type_gid' => 'users_object','responder_id' => $this->_vars['user']['id'],'success' => $this->_vars['review_callback'],'is_owner' => $this->_vars['is_user_owner']), $this);?>
			<?php echo tpl_function_block(array('name' => 'user_listings_button','module' => 'listings','user' => $this->_vars['user']), $this);?>
			<a href="<?php echo $this->_vars['site_url']; ?>
users/pdf/<?php echo $this->_vars['user']['id']; ?>
" id="pdf_btn" class="btn-link link-r-margin" rel="nofollow" title="<?php echo l('link_pdf', 'listings', '', 'button', array()); ?>"><ins class="fa fa-pdf fa-lg edge hover"></ins></a>
			<a href="javascript:void(0);" id="print_btn" class="btn-link link-r-margin" rel="nofollow" title="<?php echo l('link_print', 'listings', '', 'button', array()); ?>" onclick="javascript: window.print(); return false;"><ins class="fa fa-print fa-lg edge hover"></ins></a>
			
		</div>
		<div class="clr"></div>
		<div class="view_user">
			<div class="image">
				<?php 
$this->assign('text_user_logo', l('text_user_logo', 'users', '', 'button', array()));
 ?>
				<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user']), $this);?>">
					<img src="<?php echo $this->_vars['user']['media']['user_logo']['thumbs']['big']; ?>
" alt="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['text_user_logo'], 'replace', 'plugin', 1, '[user_name]', $this->_vars['user']['output_name']), 'escape', 'plugin', 1); ?>
" title="<?php echo $this->_run_modifier($this->_vars['user']['output_name'], 'escape', 'plugin', 1); ?>
">
				</a>
			</div>
			<div class="body">
				<?php 
$this->assign('no_info_str', l('no_information', 'start', '', 'text', array()));
 ?>
				<div class="t-1">
										<?php echo tpl_function_block(array('name' => get_rate_block,'module' => reviews,'rating_data_main' => $this->_vars['user']['review_value'],'type_gid' => 'users_object','template' => 'normal','read_only' => 'true'), $this);?><br>
					<span><?php echo l('field_rate', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['review_value']; ?>
<br>
					<span><?php echo l('field_reviews_count', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['review_count']; ?>
<br>
					<?php if ($this->_vars['user']['review_count'] == 0):  echo tpl_function_block(array('name' => 'send_review_block','module' => 'reviews','object_id' => $this->_vars['user']['id'],'type_gid' => 'users_object','responder_id' => $this->_vars['user']['id'],'success' => $this->_vars['review_callback'],'is_owner' => $this->_vars['is_user_owner'],'template' => 'first'), $this); endif; ?><br>
										<?php if ($this->_vars['user']['user_type'] == "company" && $this->_vars['user']['agent_count']): ?><span><?php echo l('field_agent_count', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['agent_count']; ?>
<br><?php endif; ?>
					
				</div>
				<div class="t-2">
					<span><?php echo l('field_register', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_run_modifier($this->_vars['user']['date_created'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
<br>
					<span><?php echo l('field_views', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['user']['views']; ?>
<br>
					<span class="status_text"><?php if ($this->_vars['user']['is_featured']):  echo l('status_featured', 'users', '', 'text', array());  endif; ?></span>
				</div>
			</div>
			<div class="clr"></div>
		</div>
		<div class="edit_block">
			<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. users. $this->module_templates.  $this->get_current_theme_gid('user', 'users'). "view_menu.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
			<div id="user_block">
		
				<section id="content_m_reviews" class="view-section<?php if ($this->_vars['section_gid'] != 'reviews'): ?> hide<?php endif; ?> noPrint"><?php echo $this->_vars['user_content']['reviews']; ?>
</section>
				<?php if ($this->_vars['user']['user_type'] == 'company'): ?><section id="content_m_map" class="view-section<?php if ($this->_vars['section_gid'] != 'map'): ?> hide<?php endif; ?> noPrint">
                <iframe width="630" height="500" frameborder="0" style="border:0;margin-top:-130px;" src = "https://maps.google.com/maps?q=<?php echo $this->_vars['user']['lat']; ?>
,<?php echo $this->_vars['user']['lon']; ?>
&hl=es;z=5&amp;output=embed">
               </iframe>
                </section><?php endif; ?>
				<section id="content_m_contacts" class="view-section<?php if ($this->_vars['section_gid'] != 'contacts'): ?> hide<?php endif; ?> print_block">
                <?php echo $this->_vars['user_content']['contacts']; ?>

                </section>
                <?php if ($this->_vars['user']['office_image'] != ""): ?>
                <img id="content_m_cimg" src="<?php echo $this->_vars['site_root']; ?>
uploads/photo/<?php echo $this->_vars['user']['office_image']; ?>
" height="200" width="250"/>
                <?php endif; ?>
                <section id="content_m_xxxx" class="view-section<?php if ($this->_vars['section_gid'] != 'services'): ?> hide<?php endif; ?> noPrint">
        	    <div class="av-rt_2 skinned-form-controls skinned-form-controls-mac" id="mainpartser">
<div class="services-part">
<h4><?php echo l('agent_lbl_services_offered', 'content', '', 'text', array()); ?></h4>
<div class="mainone real<?php echo $this->_vars['current_lang']; ?>
"><input type="radio" <?php if ($this->_vars['banner_de']['real_estate'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span><p><?php echo l('agent_lbl_1', 'content', '', 'text', array()); ?></p></div>
<div class="maintwo rent<?php echo $this->_vars['current_lang']; ?>
"><input type="radio" <?php if ($this->_vars['banner_de']['rental_service'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span><p><?php echo l('agent_lbl_2', 'content', '', 'text', array()); ?></p></div>
<div class="mainthree faci<?php echo $this->_vars['current_lang']; ?>
"><input type="radio" <?php if ($this->_vars['banner_de']['facility_service'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span><p><?php echo l('agent_lbl_3', 'content', '', 'text', array()); ?></p></div>
<div class="mainfour insu<?php echo $this->_vars['current_lang']; ?>
"><input type="radio" <?php if ($this->_vars['banner_de']['insurance_service'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span><p><?php echo l('agent_lbl_4', 'content', '', 'text', array()); ?></p></div>
<div class="mainfive bank<?php echo $this->_vars['current_lang']; ?>
"><input type="radio" <?php if ($this->_vars['banner_de']['banking_service'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span><p><?php echo l('agent_lbl_5', 'content', '', 'text', array()); ?></p></div>
</div>
<?php if ($this->_vars['banner_de']['operations'] != ""): ?>
<div class="operates-part">
<h4><?php echo l('agent_lbl_operates_in', 'content', '', 'text', array()); ?></h4>
<div class="operates-content">
<?php echo $this->_run_modifier($this->_vars['banner_de']['operations'], 'replace', 'plugin', 1, ",", "<br />"); ?>

</div>
</div>
 <?php endif; ?>
 <div class="lang-part">
 <h4><?php echo l('agent_lbl_languages', 'content', '', 'text', array()); ?></h4>
 <table cellpadding="12" style="border-collapse:collapse;" cellspacing="11" border="1" bordercolor="#a9a9a9" class="infotable">
                            <tr>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/flags/no-no.png" alt="NO" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/flags/sv-se.png" alt="SE" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/flags/fi-fi.png" alt="FI" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/flags/dk-dk.png" alt="DK" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/flags/ru-ru.png" alt="RU" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/flags/pl-pl.png" alt="PL" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/flags/nl-nl.png" alt="NL" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/flags/de-de.png" alt="DE" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/flags/fr-fr.png" alt="FR" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/flags/en-gb2.png" alt="EN" /></th>
                                <th class="last smallcell"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
/flags/es-es.png" alt="ES" /></th>					
                            </tr>
                            <tr>
                                <td class="smallcell"><input type="radio" <?php if ($this->_vars['banner_de']['lang_no'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span></td>
                                <td class="smallcell"><input type="radio" <?php if ($this->_vars['banner_de']['lang_se'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span></td>
                                <td class="smallcell"><input type="radio" <?php if ($this->_vars['banner_de']['lang_fi'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span></td>
                                <td class="smallcell"><input type="radio" <?php if ($this->_vars['banner_de']['lang_dk'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span></td>
                                <td class="smallcell"><input type="radio" <?php if ($this->_vars['banner_de']['lang_ru'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span></td>
                                <td class="smallcell"><input type="radio" <?php if ($this->_vars['banner_de']['lang_pl'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span></td>
                                <td class="smallcell"><input type="radio" <?php if ($this->_vars['banner_de']['lang_ne'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span></td>
                                <td class="smallcell"><input type="radio" <?php if ($this->_vars['banner_de']['lang_de'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span></td>
                                <td class="smallcell"><input type="radio" <?php if ($this->_vars['banner_de']['lang_fr'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span></td>
                                <td class="smallcell"><input type="radio" <?php if ($this->_vars['banner_de']['lang_en'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span></td>
                                <td class="last smallcell"><input type="radio" <?php if ($this->_vars['banner_de']['lang_es'] == 1): ?>checked <?php else: ?> disabled<?php endif; ?>><span></span></td>					
                            </tr>				
                        </table>
 </div>
</div>
                </section>
				<section id="content_m_listings" class="view-section<?php if ($this->_vars['section_gid'] != 'listings'): ?> hide<?php endif; ?> noPrint"><?php echo $this->_vars['user_content']['listings']; ?>
</section>
			</div>
		</div>
		<?php if ($this->_vars['user_type'] == 'company'): ?></div><?php endif; ?>
    



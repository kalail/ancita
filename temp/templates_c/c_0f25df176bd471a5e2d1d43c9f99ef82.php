<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.math.php'); $this->register_function("math", "tpl_function_math");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.lower.php'); $this->register_modifier("lower", "tpl_modifier_lower");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-30 09:31:02 India Daylight Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_users_types_menu'), $this);?>

<div class="actions">
	<ul>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/add/<?php echo $this->_vars['user_type']; ?>
"><?php echo l('link_add_user', 'users', '', 'text', array()); ?></a></div></li>
		<?php echo tpl_function_helper(array('func_name' => button_add_funds,'module' => users_payments), $this);?>
	</ul>
	&nbsp;
</div>

<div class="menu-level3">
	<ul>
		<?php if (is_array($this->_vars['filters']) and count((array)$this->_vars['filters'])): foreach ((array)$this->_vars['filters'] as $this->_vars['item']): ?>
		<?php $this->assign('l', 'filter_'.$this->_vars['item'].'_users'); ?>
		<li class="<?php if ($this->_vars['filter'] == $this->_vars['item']): ?>active<?php endif;  if (! $this->_vars['filter_data'][$this->_vars['item']]): ?> hide<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/index/<?php echo $this->_vars['user_type']; ?>
/<?php echo $this->_vars['item']; ?>
"><?php echo l($this->_vars['l'], 'users', '', 'text', array()); ?> (<?php echo $this->_vars['filter_data'][$this->_vars['item']]; ?>
)</a></li>
		<?php endforeach; endif; ?>
	</ul>
	&nbsp;
</div>

<form method="post" action="" name="save_form" enctype="multipart/form-data">
<div class="filter-form">
	<div class="form">
		<div class="row">
			<div class="h"><?php echo l('field_keyword', 'users', '', 'text', array()); ?>:</div>
			<div class="v">
				<input type="text" name="keyword" value="<?php echo $this->_run_modifier($this->_vars['page_data']['filter']['keyword_all'], 'escape', 'plugin', 1); ?>
" />
			</div>
		</div>
		<div class="row">
			<div class="h">
				<input type="submit" name="filter-submit" value="<?php echo l('header_filters', 'users', '', 'button', array()); ?>">
				<input type="submit" name="filter-reset" value="<?php echo l('header_reset', 'users', '', 'button', array()); ?>">
			</div>
		</div>
	</div>
</div>
</form>

<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first"><input type="checkbox" id="grouping_all"></th>
	<th class="w150"><a href="<?php echo $this->_vars['sort_links']['name']; ?>
"<?php if ($this->_vars['order'] == 'name'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_name', 'users', '', 'text', array()); ?></a></th>
	<th class="w150"><a href="<?php echo $this->_vars['sort_links']['email']; ?>
"<?php if ($this->_vars['order'] == 'email'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_email', 'users', '', 'text', array()); ?></a></th>
	<th><a href="<?php echo $this->_vars['sort_links']['account']; ?>
"<?php if ($this->_vars['order'] == 'account'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_account', 'users', '', 'text', array()); ?></a></th>
		<th class="w30"><a href="<?php echo $this->_vars['sort_links']['listings']; ?>
"<?php if ($this->_vars['order'] == 'listings'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_listings_actived', 'users', '', 'text', array()); ?></a></th>
			<th class="w30"><a href="<?php echo $this->_vars['sort_links']['reviews']; ?>
"<?php if ($this->_vars['order'] == 'reviews'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_reviews_count', 'users', '', 'text', array()); ?></a></th>
		<th class="w100"><a href="<?php echo $this->_vars['sort_links']['date_created']; ?>
"<?php if ($this->_vars['order'] == 'date_created'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_date_created', 'users', '', 'text', array()); ?></a></th>
    <th class="w100"><a href="<?php echo $this->_vars['sort_links']['date_login']; ?>
"<?php if ($this->_vars['order'] == 'date_login'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_last_login', 'users', '', 'text', array()); ?></a></th>
    <th class="w30"><?php echo l('field_days_since_login', 'users', '', 'text', array()); ?></th>
    <th class="w30"><?php echo l('field_days_sice_member', 'users', '', 'text', array()); ?></th>
	<th class="w150">&nbsp;</th>
</tr>
<?php if (is_array($this->_vars['users']) and count((array)$this->_vars['users'])): foreach ((array)$this->_vars['users'] as $this->_vars['item']): ?>
<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
	<td class="first w20 center"><input type="checkbox" class="grouping" value="<?php echo $this->_vars['item']['id']; ?>
"></td>
	<td><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</td>
	<td><?php echo $this->_run_modifier($this->_vars['item']['email'], 'truncate', 'plugin', 1, 50); ?>
</td>
	<td class="center"><?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => $this->_vars['item']['account'],'cur_gid' => 'USD'), $this);?></td>
		<td class="center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/user/<?php echo $this->_vars['item']['id']; ?>
"><?php echo tpl_function_math(array('equation' => "sale+buy+rent+lease",'sale' => $this->_vars['item']['listings_for_sale_count'],'buy' => $this->_vars['item']['listings_for_buy_count'],'rent' => $this->_vars['item']['listings_for_rent_count'],'lease' => $this->_vars['item']['listings_for_lease_count']), $this);?></a></td>
			<td class="center"><a href="<?php echo $this->_vars['site_url']; ?>
admin/reviews/index/users_object/<?php echo $this->_vars['item']['id']; ?>
"><?php echo $this->_vars['item']['review_count']; ?>
</a></td>
		<td class="center"><?php echo $this->_run_modifier($this->_vars['item']['date_created'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
    <td class="center"><?php echo $this->_run_modifier($this->_vars['item']['date_login'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
    <td class="center"><?php echo $this->_vars['item']['since_login']; ?>
</td>
    <td class="center"><?php echo $this->_vars['item']['sice_member']; ?>
</td>
	<td class="icons">
		<?php if ($this->_vars['item']['status']): ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/users/activate/<?php echo $this->_vars['item']['id']; ?>
/0"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-full.png" width="16" height="16" border="0" alt="<?php echo l('link_deactivate_user', 'users', '', 'button', array()); ?>" title="<?php echo l('link_deactivate_user', 'users', '', 'button', array()); ?>"></a>
		<?php else: ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/users/activate/<?php echo $this->_vars['item']['id']; ?>
/1"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-empty.png" width="16" height="16" border="0" alt="<?php echo l('link_activate_user', 'users', '', 'button', array()); ?>" title="<?php echo l('link_activate_user', 'users', '', 'button', array()); ?>"></a>
		<?php endif; ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/users/edit/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_edit_user', 'users', '', 'button', array()); ?>" title="<?php echo l('link_edit_user', 'users', '', 'button', array()); ?>"></a>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/users/delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_user', 'users', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_user', 'users', '', 'button', array()); ?>" title="<?php echo l('link_delete_user', 'users', '', 'button', array()); ?>"></a>
	</td>
</tr>
<?php endforeach; else: ?>
<?php $this->assign('colspan', 8); ?>
<?php $this->assign('colspan', $this->_vars['colspan']);  $this->assign('colspan', $this->_vars['colspan']); ?><tr><td colspan="<?php echo $this->_vars['colspan']; ?>
" class="center"><?php echo l('no_users', 'users', '', 'text', array()); ?></td></tr>
<?php endif; ?>
</table>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<script type="text/javascript"><?php echo '
var reload_link = "';  echo $this->_vars['site_url']; ?>
admin/users/index/<?php echo '";
var filter = \'';  echo $this->_vars['filter'];  echo '\';
var order = \'';  echo $this->_vars['order'];  echo '\';
var loading_content;
var order_direction = \'';  echo $this->_vars['order_direction'];  echo '\';
$(function(){
	$(\'#grouping_all\').bind(\'click\', function(){
		var checked = $(this).is(\':checked\');
		if(checked){
			$(\'input[type=checkbox].grouping\').prop(\'checked\', true);
		}else{
			$(\'input[type=checkbox].grouping\').prop(\'checked\', false);
		}
	});
});

function reload_this_page(value){
	var link = reload_link + value + \'/\' + filter + \'/\' + order + \'/\' + order_direction;
	location.href=link;
}
'; ?>
</script>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-02-10 05:21:23 India Standard Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php echo tpl_function_helper(array('func_name' => 'users_pagination_block','module' => 'users','func_param' => $this->_vars['user']), $this);?>	

<?php echo tpl_function_block(array('name' => show_social_networks_like,'module' => social_networking,'func_param' => true), $this);?>

<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'right_block')); tpl_block_capture(array('assign' => 'right_block'), null, $this); ob_start(); ?>
		    <?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'header')); tpl_block_capture(array('assign' => 'header'), null, $this); ob_start(); ?>
		<div class="user_contact"><h2><?php echo l('header_user_contact', 'users', '', 'text', array()); ?></h2></div>
	    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<?php echo tpl_function_block(array('name' => show_contact_form,'module' => contact,'user_id' => $this->_vars['user']['id'],'header' => $this->_vars['header']), $this);?>
		<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'right-banner'), $this);?>
	
<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>


<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'main_block')); tpl_block_capture(array('assign' => 'main_block'), null, $this); ob_start(); ?>
	<div class="content-block">
	<?php $this->assign('user_type', $this->_vars['user']['user_type']); ?>
	<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. users. $this->module_templates.  $this->get_current_theme_gid('user', 'users'). "view_block.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
	<?php echo tpl_function_js(array('module' => users,'file' => 'users-menu.js'), $this);?>
	<?php echo tpl_function_js(array('module' => users_services,'file' => 'available_view.js'), $this);?>
	<?php echo tpl_function_js(array('file' => 'change_link_action.js'), $this);?>
	<script><?php echo '
		var rMenu;
		$(function(){
			rMenu = new usersMenu({
				siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
				idUser: \'';  echo $this->_vars['user']['id'];  echo '\',
				';  if ($this->_vars['user_type'] == 'agent' && $this->_vars['user']['agent_status']): ?>idCompany: '<?php echo $this->_vars['user']['agent_company']; ?>
',<?php endif;  echo '
				'; ?>
available_view: new available_view(),<?php echo '
			});
		});
	'; ?>
</script>		
</div>	
<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
	
<?php if ($this->_run_modifier($this->_vars['right_block'], 'trim', 'PHP', 1)): ?>
<div class="rc_wrapper">
<div class="panel">
<div class="inside">
	<section class="lc-1" id="view_user">
		<?php echo $this->_vars['main_block']; ?>

	</section>
	<section class="rc-2">
		<?php echo $this->_vars['right_block']; ?>

	</section>
	<div class="clr"></div>
</div>
</div>
</div>
<?php else: ?>
	<div id="view_user">
		<?php echo $this->_vars['main_block']; ?>

	</div>
<?php endif; ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

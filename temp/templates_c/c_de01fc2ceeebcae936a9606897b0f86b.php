<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.breadcrumbs.php'); $this->register_function("breadcrumbs", "tpl_function_breadcrumbs");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.start_search_form.php'); $this->register_function("start_search_form", "tpl_function_start_search_form");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.menu.php'); $this->register_function("menu", "tpl_function_menu");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\block.strip.php'); $this->register_block("strip", "tpl_block_strip");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.seotag.php'); $this->register_function("seotag", "tpl_function_seotag");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-11 05:53:05 India Daylight Time */ ?>

<!DOCTYPE html>
<html DIR="<?php echo $this->_vars['_LANG']['rtl']; ?>
">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta charset="utf-8"> 
	<meta http-equiv="expires" content="0">
	<meta http-equiv="pragma" content="no-cache">
     <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="revisit-after" content="3 days">
	<?php echo tpl_function_seotag(array('tag' => 'robots|title|description|keyword|canonical|og_title|og_type|og_url|og_image|og_site_name|og_description'), $this);?>
	
	<link href='<?php echo $this->_vars['site_root']; ?>
application/views/pilot/font-awesome.css' rel='stylesheet' type='text/css'>
	<?php echo tpl_function_helper(array('func_name' => css,'helper_name' => theme,'func_param' => $this->_vars['load_type']), $this);?>
	<?php if ($this->_vars['_LANG']['rtl'] == 'rtl'): ?><!--[if IE]><link rel="stylesheet" type="text/css" href="<?php echo $this->_vars['site_root']; ?>
application/views/pilot/ie-rtl-fix.css" /><![endif]--><?php endif; ?>
	<?php echo '<!--[if gt IE 9]><style type="text/css">.fa .fa, .fa .fa:before, .fa .fa:after{filter: none;}</style><![endif]-->'; ?>

	<?php echo '<!--[if lte IE 9]><style type="text/css">.icon-mini-stack-shadow{display: inline-block;}</style><![endif]-->'; ?>

	<link rel="shortcut icon" href="<?php echo $this->_vars['site_root']; ?>
favicon.ico">
	
	<script type="text/javascript">
		var site_url = '<?php echo $this->_vars['site_url']; ?>
';
		var site_rtl_settings = '<?php echo $this->_vars['_LANG']['rtl']; ?>
';
		var site_error_position = 'center';
	</script>
	
	<?php echo tpl_function_helper(array('func_name' => js,'helper_name' => theme,'func_param' => $this->_vars['load_type']), $this);?>

	<?php echo tpl_function_helper(array('func_name' => banner_initialize,'module' => banners), $this);?>
	<?php echo tpl_function_helper(array('func_name' => show_social_networks_head,'module' => social_networking), $this);?>
</head>
<body dir="<?php echo $this->_vars['_LANG']['rtl']; ?>
">
<?php echo tpl_function_helper(array('func_name' => seo_traker,'helper_name' => seo_module,'module' => seo,'func_param' => 'top'), $this);?>

<?php echo tpl_function_helper(array('func_name' => demo_panel,'helper_name' => start,'func_param' => 'user'), $this);?>
<?php if ($this->_vars['display_brouser_error']): ?>
	<?php echo tpl_function_helper(array('func_name' => available_brousers,'helper_name' => start), $this);?>
<?php endif; ?>

	<div id="error_block"><?php if (is_array($this->_vars['_PREDEFINED']['error']) and count((array)$this->_vars['_PREDEFINED']['error'])): foreach ((array)$this->_vars['_PREDEFINED']['error'] as $this->_vars['item']):  echo $this->_vars['item']['text']; ?>
<br><?php endforeach; endif; ?></div>
	<div id="info_block"><?php if (is_array($this->_vars['_PREDEFINED']['info']) and count((array)$this->_vars['_PREDEFINED']['info'])): foreach ((array)$this->_vars['_PREDEFINED']['info'] as $this->_vars['item']):  echo $this->_vars['item']['text']; ?>
<br><?php endforeach; endif; ?></div>
	<div id="success_block"><?php if (is_array($this->_vars['_PREDEFINED']['success']) and count((array)$this->_vars['_PREDEFINED']['success'])): foreach ((array)$this->_vars['_PREDEFINED']['success'] as $this->_vars['item']):  echo $this->_vars['item']['text']; ?>
<br><?php endforeach; endif; ?></div>

	<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'header')); tpl_block_capture(array('assign' => 'header'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
		<?php echo tpl_function_block(array('name' => users_lang_select,'module' => users), $this);?>
		<?php echo tpl_function_block(array('name' => site_currency_select,'module' => users), $this);?>
		<?php echo tpl_function_block(array('name' => auth_links,'module' => users), $this);?>
		<?php echo tpl_function_block(array('name' => post_listing_button,'module' => listings), $this);?>
	<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
	<?php if ($this->_vars['header']): ?>
	<div class="header">
		<div class="content">
			<div class="header-menu">
				<ul><?php echo $this->_vars['header']; ?>
</ul>
			</div>
			<div class="clr"></div>
		</div>
	</div>
	<?php endif; ?>
	<div class="main">
		<div class="content">
			<header>
				<div class="logo">
					<?php if ($this->_vars['header_type'] == 'index'): ?>
					<h1><a href="<?php echo $this->_vars['site_url']; ?>
"><img src="<?php echo $this->_vars['base_url'];  echo $this->_vars['logo_settings']['path']; ?>
" border="0" alt="<?php echo tpl_function_seotag(array('tag' => 'header_text'), $this);?>" width="<?php echo $this->_vars['logo_settings']['width']; ?>
" height="<?php echo $this->_vars['logo_settings']['height']; ?>
"></a></h1>
					<?php else: ?>
					<a href="<?php echo $this->_vars['site_url']; ?>
"><img src="<?php echo $this->_vars['base_url'];  echo $this->_vars['logo_settings']['path']; ?>
" border="0" alt="<?php echo tpl_function_helper(array('func_name' => 'seo_tags_default','func_param' => 'header_text'), $this);?>" width="<?php echo $this->_vars['logo_settings']['width']; ?>
" height="<?php echo $this->_vars['logo_settings']['height']; ?>
"></a>
					<?php endif; ?>
					<nav class="top_menu">
						<?php if ($this->_vars['auth_type'] == 'user'): ?>
						<?php echo tpl_function_menu(array('gid' => $this->_vars['user_session_data']['user_type'].'_main_menu','template' => 'user_main_menu'), $this);?>
						<?php else: ?>
						<?php echo tpl_function_menu(array('gid' => 'guest_main_menu','template' => 'user_main_menu'), $this);?>
						<?php endif; ?>
					</nav>
					<script><?php echo '
						$(function(){
							$(\'.top_menu ul>li\').each(function(i, item){
								var element = $(item);
								var submenu = element.find(\'.sub_menu_block\');
								if(submenu.length == 0) return;
								element.children(\'a\').bind(\'touchstart\', function(){
									if(element.hasClass(\'hover\')){
										element.removeClass(\'hover\');
									}else{
										$(\'.top_menu ul>li\').removeClass(\'hover\');
										element.addClass(\'hover\');
									}
									return false;
								});
								element.children(\'a\').bind(\'touchenter\', function(){
									element.removeClass(\'hover\');
									element.trigger(\'mouseenter mouseleave\');
									return false;
								}).bind(\'touchleave\', function(){
									element.removeClass(\'hover\').trigger(\'blur\');
									return false;
								});
							
							
							});
						});
					'; ?>
</script>
				</div>
			
				<?php if ($this->_vars['header_type'] != 'index'): ?>
				<?php echo tpl_function_start_search_form(array('type' => 'line','show_data' => 1), $this);?>
				<?php endif; ?>
			</header>
			
			<?php echo tpl_function_breadcrumbs(array(), $this);?>
				

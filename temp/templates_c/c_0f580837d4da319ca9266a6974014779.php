<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-11 05:53:07 India Daylight Time */ ?>

<ul>
<?php if (is_array($this->_vars['menu']) and count((array)$this->_vars['menu'])): foreach ((array)$this->_vars['menu'] as $this->_vars['key'] => $this->_vars['item']): ?>
<li <?php if ($this->_vars['item']['active']): ?>class="active"<?php endif; ?>>
	<a href="<?php echo $this->_vars['item']['link']; ?>
"><?php echo $this->_vars['item']['value']; ?>
</a>
	<?php if ($this->_vars['item']['sub']): ?>
	<div class="sub_menu_block">
		<span><?php echo $this->_vars['item']['value']; ?>
</span>
		<ul>
			<?php if (is_array($this->_vars['item']['sub']) and count((array)$this->_vars['item']['sub'])): foreach ((array)$this->_vars['item']['sub'] as $this->_vars['key2'] => $this->_vars['item2']): ?>
			<li><a href="<?php echo $this->_vars['item2']['link']; ?>
"><?php echo $this->_vars['item2']['value']; ?>
 <?php if ($this->_vars['item']['indicator']): ?><span class="num"><?php echo $this->_vars['item']['indicator']; ?>
</span><?php endif; ?></a></li>
			<?php endforeach; endif; ?>
		</ul>
	</div>
	<?php endif; ?>
</li>
<?php endforeach; endif; ?>
</ul>

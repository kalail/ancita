<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-11 05:53:14 India Daylight Time */ ?>

<?php if ($this->_vars['listings']): ?>
<div id="ShowCustomlisting_<?php echo $this->_vars['type']; ?>
" style="display:<?php if ($this->_vars['type'] == 'rent'): ?>none<?php else: ?>block<?php endif; ?>;">
<h2><?php echo l('header_'.$this->_vars['type'].'_listings', 'listings', '', 'text', array()); ?></h2>
<!--<h2>Listings for <span id="lstTilteType_<?php echo $this->_vars['type']; ?>
"><?php echo $this->_vars['type']; ?>
</span></h2>-->
<div class="<?php echo $this->_run_modifier($this->_vars['type'], 'escape', 'plugin', 1); ?>
_listings_block">
	<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<div class="listing <?php echo $this->_run_modifier($this->_vars['photo_size'], 'escape', 'plugin', 1); ?>
">
		<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>">
			<?php 
$this->assign('logo_title', l('link_listing_view', 'listings', '', 'button', array('id_ref'=>$this->_vars['item']['id'])));
 ?>
			<?php 
$this->assign('text_listing_logo', l('text_listing_logo', 'listings', '', 'button', array('id_ref'=>$this->_vars['item']['id'],'property_type'=>$this->_vars['item']['property_type_str'],'operation_type'=>$this->_vars['item']['operation_type_str'],'location'=>$this->_vars['item']['location'])));
 ?>
			<img src="<?php echo $this->_vars['item']['media']['photo']['thumbs'][$this->_vars['photo_size']]; ?>
" alt="<?php echo $this->_vars['text_listing_logo']; ?>
" title="<?php echo $this->_vars['logo_title']; ?>
" />
			<!--<?php if ($this->_vars['item']['photo_count'] || $this->_vars['item']['is_vtour']): ?>
			<div class="photo-info">
				<div class="panel">
					<?php if ($this->_vars['item']['photo_count']): ?><span><ins class="fa fa-camera w"></ins> <?php echo $this->_vars['item']['photo_count']; ?>
</span><?php endif; ?>
					<?php if ($this->_vars['item']['is_vtour']): ?><span><ins class="fa fa-rotate-left w"></ins> 360&deg;</span><?php endif; ?>
				</div>
				<div class="background"></div>
			</div>
			<?php endif; ?>-->
		</a>
		<!--<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php if ($this->_vars['photo_size'] == 'big'):  echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 35);  else:  echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 30);  endif; ?></a>-->
        <a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo $this->_vars['item']['city']; ?>
</a><br />
        <span><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item'],'template' => 'small'), $this);?></span>	
		<span><?php echo $this->_vars['item']['property_type_str']; ?>
</span>
	</div>
	<?php endforeach; endif; ?>
	<div class="clr"></div>
</div>
</div>
<?php endif; ?>

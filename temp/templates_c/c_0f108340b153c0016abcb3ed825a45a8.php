<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-31 13:39:01 India Daylight Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. start. $this->module_templates.  $this->get_current_theme_gid('', 'start'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="rc">
	<div class="content-block">
		<h1><?php echo l('header_my_listings', 'listings', '', 'text', array()); ?> (<?php echo $this->_vars['listings_count_sum']; ?>
)</h1>

		<div class="search-links">
        	<?php echo '<style>.custListingsbtn{
					background:transparent;
					border: medium none;
					bottom: 1px;
					height: auto !important;
					margin: 0;
					right: 20px;
					/*top: 1px;*/
					position:relative;
					cursor:pointer;}
          </style>'; ?>

			<?php echo l('link_search_in_my_listings', 'listings', '', 'text', array()); ?>
			<div class="edit_block">			
				<?php if (! $this->_vars['noshow_add_button']): ?><a class="btn-link fright" href="<?php echo $this->_vars['site_url']; ?>
listings/edit"><ins class="fa fa-plus fa-lg edge hover"></ins><span><?php echo l('link_add_listing', 'listings', '', 'text', array()); ?></span></a><?php endif; ?>			
				<form action="" method="post" enctype="multipart/form-data" id="listings_search_form">
				<div class="r">
					<input type="text" name="data[keyword]" value="<?php echo $this->_run_modifier($this->_vars['page_data']['keyword'], 'escape', 'plugin', 1); ?>
" id="listings_search" autocomplete="off" />
				</div>
                <div class="r">
                	Search listing ID <br /><input type="text" autocomplete="off" name="data[ids]" value="<?php echo $this->_run_modifier($this->_vars['page_data']['ids'], 'escape', 'plugin', 1); ?>
" id="txtSearch_listingsId" />
					<ins id="btnlstIdSerach" class="fa fa-search custListingsbtn"></ins>
                </div>
				</form>
			</div>
		</div>

		<div class="tabs tab-size-15 noPrint">
			<ul id="my_listings_sections">
				<?php if (is_array($this->_vars['operation_types']) and count((array)$this->_vars['operation_types'])): foreach ((array)$this->_vars['operation_types'] as $this->_vars['tgid']): ?>
				<li id="m_<?php echo $this->_vars['tgid']; ?>
" sgid="<?php echo $this->_vars['tgid']; ?>
" class="<?php if ($this->_vars['current_operation_type'] == $this->_vars['tgid']): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/my/<?php echo $this->_vars['tgid']; ?>
"><?php echo l('operation_search_'.$this->_vars['tgid'], 'listings', '', 'text', array()); ?> (<?php echo $this->_vars['listings_count'][$this->_vars['tgid']]; ?>
)</a></li>
				<?php endforeach; endif; ?>
			</ul>
		</div>

		<div id="listings_block"><?php echo $this->_vars['block']; ?>
</div>
		
		<?php echo tpl_function_js(array('module' => users_services,'file' => 'available_view.js'), $this);?>
		<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-list.js'), $this);?>
		<script><?php echo '
		$(function(){
			new listingsList({
				siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
				listAjaxUrl: \''; ?>
listings/ajax_my<?php echo '\',
				sectionId: \'my_listings_sections\',
				operationType: \'';  echo $this->_vars['current_operation_type'];  echo '\',
				order: \'';  echo $this->_vars['order'];  echo '\',
				orderDirection: \'';  echo $this->_vars['order_direction'];  echo '\',
				page: ';  echo $this->_vars['page'];  echo ',
				tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\'],
			});
		});
		
		/*$("#custBtnListingsIdSearch").click(function(){
			alert();
			new listingsList({
				siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
				listAjaxUrl: \''; ?>
listings/ajax_my<?php echo '\',
				sectionId: \'my_listings_sections\',
				operationType: \'';  echo $this->_vars['current_operation_type'];  echo '\',
				order: \'';  echo $this->_vars['order'];  echo '\',
				orderDirection: \'';  echo $this->_vars['order_direction'];  echo '\',
				page: ';  echo $this->_vars['page'];  echo ',
				tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\'],
				sFormId: \'txtSearch_listingsId\',
				custListings:true,
			});
		});*/
		/*$(function(){
			
		});*/
		'; ?>
</script>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

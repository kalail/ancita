<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-01-19 07:52:41 India Standard Time */ ?>

<div>
	<div class="row"><a href="<?php echo $this->_vars['site_url']; ?>
listings/my" class="value fleft"><?php echo l('my_listings', 'listings', '', 'text', array()); ?></a><span class="digit"><?php echo $this->_vars['listings_total']['total']; ?>
 (<?php echo $this->_vars['listings_total']['active']; ?>
)</span></div>
	<?php if (is_array($this->_vars['listings_counts']) and count((array)$this->_vars['listings_counts'])): foreach ((array)$this->_vars['listings_counts'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<div class="row"><a href="<?php echo $this->_vars['site_url']; ?>
listings/my" class="value fleft"><?php echo l('my_listings_for_'.$this->_vars['key'], 'listings', '', 'text', array()); ?></a><span class="digit"><?php echo $this->_vars['item']['total']; ?>
 (<?php echo $this->_vars['item']['active']; ?>
)</span></div>
	<?php endforeach; endif; ?>
</div>


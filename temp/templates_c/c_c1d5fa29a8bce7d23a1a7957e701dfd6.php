<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-02-08 10:55:23 India Standard Time */ ?>

<div class="content-block load_content">
	<h1><?php echo l('header_order_approve', 'listings', '', 'text', array()); ?></h1>
	<div class="inside edit_block">
		<form method="post" action="" name="save_form" id="order_approve_form" enctype="multipart/form-data">
			<div class="r" id="order_comment">
				<div class="f"><?php echo l('field_booking_comment', 'listings', '', 'text', array()); ?>:</div>
				<div class="v"><?php if ($this->_vars['order']['comment']):  echo $this->_vars['order']['comment'];  else:  echo l('no_comment', 'listings', '', 'text', array());  endif; ?></div>
			</div>
			<div class="r">
				<div class="f"><?php echo l('field_booking_answer', 'listings', '', 'text', array()); ?>:</div>
				<div class="v"><textarea name="period[answer]" rows="10" cols="80"></textarea></div>
			</div>
		
			<div class="b">
				<input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" id="close_btn">
			</div>
		</form>
	</div>
</div>

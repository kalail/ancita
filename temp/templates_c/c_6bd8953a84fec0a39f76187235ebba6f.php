<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.math.php'); $this->register_function("math", "tpl_function_math");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date.php'); $this->register_modifier("date", "tpl_modifier_date");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.ld.php'); $this->register_function("ld", "tpl_function_ld");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-01-12 12:54:47 India Standard Time */ ?>

<div class="load_content_controller">
	<h1><?php echo l('header_order_add', 'listings', '', 'text', array()); ?></h1>
	<div class="inside">
		<form method="post" class="edit-form n150" name="save_form" id="order_form" enctype="multipart/form-data">
			<div class="row">
				<div class="h"><?php echo l('field_booking_date_start', 'listings', '', 'text', array()); ?>:</div>
				<div class="v">
											<?php switch($this->_vars['listing']['price_period']): case '1':  ?>
							<input type="text" name="period[date_start]" value="<?php if ($this->_run_modifier($this->_vars['order']['date_start'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['order']['date_start'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="date_start<?php echo $this->_vars['rand']; ?>
" class="middle"> <a href="#" id="date_start_open_btn" title="<?php echo l('link_calendar_open', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-calendar"></ins></a>
							<input type="hidden" name="date_start_alt" value="<?php if ($this->_run_modifier($this->_vars['order']['date_start'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['order']['date_start'], 'date_format', 'plugin', 1, '%Y-%m-%d'), 'escape', 'plugin', 1);  endif; ?>" id="alt_date_start<?php echo $this->_vars['rand']; ?>
">
							<script><?php echo '
							   $(function(){
							   $(\'#date_start';  echo $this->_vars['rand'];  echo '\').datepicker({
							   dateFormat: \'';  echo $this->_vars['page_data']['datepicker_date_format'];  echo '\', 
							   altFormat: \'yy-mm-dd\',
							   altField: \'#alt_date_start';  echo $this->_vars['rand'];  echo '\',
                               minDate:  0,
                               onSelect: function(date){            
                               var date1 = $(\'#date_start';  echo $this->_vars['rand'];  echo '\').datepicker(\'getDate\');           
                               var date = new Date( Date.parse( date1 ) ); 
                               date.setDate( date.getDate() + 0 );        
                               var newDate = date.toDateString(); 
                               newDate = new Date( Date.parse( newDate ) );                      
                               $(\'#date_end';  echo $this->_vars['rand'];  echo '\').datepicker("option","minDate",newDate);            
                               }
                               });
							   });
							'; ?>
</script>
						<?php break; case '2':  ?>
							<?php echo tpl_function_ld(array('i' => 'month-names','gid' => 'start','assign' => 'month_names'), $this);?>
							<select name="date_start_month" class="middle">
								<option value=""><?php echo $this->_vars['month_names']['header']; ?>
</value>
								<?php if (is_array($this->_vars['month_names']['option']) and count((array)$this->_vars['month_names']['option'])): foreach ((array)$this->_vars['month_names']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['key'] == $this->_run_modifier($this->_vars['order']['date_start'], 'date_format', 'plugin', 1, '%m')): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
								<?php endforeach; endif; ?>
							</select>
							<?php $this->assign('cyear', $this->_run_modifier('now', 'date', 'plugin', 1, 'Y')); ?>
							<select name="date_start_year" class="middle">
								<option value=""><?php echo l('text_year', 'listings', '', 'text', array()); ?></option>
								<?php for($for1 = 0; ((0 < 10) ? ($for1 < 10) : ($for1 > 10)); $for1 += ((0 < 10) ? 1 : -1)):  $this->assign('i', $for1); ?>
								<?php echo tpl_function_math(array('equation' => 'x + y','x' => $this->_vars['cyear'],'y' => $this->_vars['i'],'assign' => 'year'), $this);?>
								<option value="<?php echo $this->_vars['year']; ?>
" <?php if ($this->_vars['year'] == $this->_run_modifier($this->_vars['order']['date_start'], 'date_format', 'plugin', 1, '%Y')): ?>selected<?php endif; ?>><?php echo $this->_vars['year']; ?>
</option>
								<?php endfor; ?>
							</select>
					<?php break; endswitch; ?>
				</div>
			</div>
			<div class="row">
				<div class="h"><?php echo l('field_booking_date_end', 'listings', '', 'text', array()); ?>:</div>
				<div class="v">
											<?php switch($this->_vars['listing']['price_period']): case '1':  ?>
							<input type="text" name="period[date_end]" value="<?php if ($this->_run_modifier($this->_vars['order']['date_end'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['order']['date_end'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="date_end<?php echo $this->_vars['rand']; ?>
" class="middle"> <a href="#" id="date_end_open_btn" title="<?php echo l('link_calendar_open', 'listings', '', 'button', array()); ?>"><ins class="with-icon i-calendar"></ins></a>
							<input type="hidden" name="date_end_alt" value="<?php if ($this->_run_modifier($this->_vars['order']['date_end'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['order']['date_end'], 'date_format', 'plugin', 1, '%Y-%m-%d'), 'escape', 'plugin', 1);  endif; ?>" id="alt_date_end<?php echo $this->_vars['rand']; ?>
">
							<script><?php echo '
								$(function(){
									$(\'#date_end';  echo $this->_vars['rand'];  echo '\').datepicker({dateFormat: \'';  echo $this->_vars['page_data']['datepicker_date_format'];  echo '\',minDate:  0, altFormat: \'yy-mm-dd\', altField: \'#alt_date_end';  echo $this->_vars['rand'];  echo '\'});
								});
							'; ?>
</script>
						<?php break; case '2':  ?>
							<?php echo tpl_function_ld(array('i' => 'month-names','gid' => 'start','assign' => 'month_names'), $this);?>
							<select name="date_end_month" class="middle">
								<option value=""><?php echo $this->_vars['month_names']['header']; ?>
</value>
								<?php if (is_array($this->_vars['month_names']['option']) and count((array)$this->_vars['month_names']['option'])): foreach ((array)$this->_vars['month_names']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['key'] == $this->_run_modifier($this->_vars['order']['date_end'], 'date_format', 'plugin', 1, '%m')): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
								<?php endforeach; endif; ?>
							</select>
							<?php $this->assign('cyear', $this->_run_modifier('now', 'date', 'plugin', 1, 'Y')); ?>
							<select name="date_end_year" class="middle">
								<option value=""><?php echo l('text_year', 'listings', '', 'text', array()); ?></option>
								<?php for($for1 = 0; ((0 < 10) ? ($for1 < 10) : ($for1 > 10)); $for1 += ((0 < 10) ? 1 : -1)):  $this->assign('i', $for1); ?>
								<?php echo tpl_function_math(array('equation' => 'x + y','x' => $this->_vars['cyear'],'y' => $this->_vars['i'],'assign' => 'year'), $this);?>
								<option value="<?php echo $this->_vars['year']; ?>
" <?php if ($this->_vars['year'] == $this->_run_modifier($this->_vars['order']['date_end'], 'date_format', 'plugin', 1, '%Y')): ?>selected<?php endif; ?>><?php echo $this->_vars['year']; ?>
</option>
								<?php endfor; ?>
							</select>
					<?php break; endswitch; ?>
				</div>
			</div>
			
			<div class="row">
				<div class="h"><?php echo l('field_booking_comment', 'listings', '', 'text', array()); ?>:</div>
				<div class="v" id="period_comment">
					<textarea name="period[comment]" rows="5" cols="80"><?php echo $this->_run_modifier($this->_vars['order']['comment'], 'escape', 'plugin', 1); ?>
</textarea>
				</div>
			</div>
			<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" id="close_btn"></div></div>
		</form>
	</div>
</div>
<script><?php echo '
$(function(){
	$("#order_form div.row:odd").addClass("zebra");
});
'; ?>
</script>

<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.sorter.php'); $this->register_function("sorter", "tpl_function_sorter");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-31 13:39:00 India Daylight Time */ ?>

	<?php if ($this->_vars['listings']): ?>
	<div class="sorter line" id="sorter_block">
		<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
		<div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div>
	</div>
	<div>
		<table class="list">
		<tr id="sorter_block">
			<th class="w100"><?php echo l('field_photo', 'listings', '', 'text', array()); ?></th>		
			<th><?php echo l('field_name', 'listings', '', 'text', array()); ?></th>		
			<th><?php echo l('field_date_modified', 'listings', '', 'text', array()); ?></th>		
			<th class="w30"><?php echo l('field_views', 'listings', '', 'text', array()); ?></th>
			<th class="w120"><?php echo l('field_search_status', 'listings', '', 'text', array()); ?></th>		
			<th class="w70">&nbsp;</th>		
		</tr>
		<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['item']): ?>
		<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'property_output')); tpl_block_capture(array('assign' => 'property_output'), null, $this); ob_start();  echo $this->_vars['item']['property_type_str']; ?>
 <?php echo $this->_vars['item']['operation_type_str'];  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'price_output')); tpl_block_capture(array('assign' => 'price_output'), null, $this); ob_start(); ?>	
			<?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item'],'template' => 'small'), $this);?>
		<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<?php 
$this->assign('logo_title', l('link_listing_view', 'listings', '', 'button', array('id_ref'=>$this->_vars['item']['id'])));
 ?>
		<?php 
$this->assign('text_listing_logo', l('text_listing_logo', 'listings', '', 'button', array('id_ref'=>$this->_vars['item']['id'],'property_type'=>$this->_vars['item']['property_type_str'],'operation_type'=>$this->_vars['item']['operation_type_str'],'location'=>$this->_vars['item']['location'])));
 ?>
		<tr>
			<td>
				<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>" title="<?php echo l('btn_preview', 'start', '', 'button', array()); ?>">
					<img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['small']; ?>
" alt="<?php echo $this->_vars['text_listing_logo']; ?>
" title="<?php echo $this->_vars['logo_title']; ?>
">
				</a>
			</td>
			<td><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 40); ?>
<br><?php echo $this->_run_modifier($this->_vars['property_output'], 'truncate', 'plugin', 1, 40); ?>
<br><?php echo $this->_run_modifier($this->_vars['price_output'], 'truncate', 'plugin', 1, 70); ?>
</td>
			<td><?php echo $this->_run_modifier($this->_vars['item']['date_modified'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
			<td><?php echo $this->_vars['item']['views']; ?>
</td>
			<td>
			<?php if ($this->_vars['item']['status']): ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
listings/activate/<?php echo $this->_vars['item']['id']; ?>
/0"><img id="frontendimg" src="<?php echo $this->_vars['site_root']; ?>
application/views/admin/img/icon-full.png" width="16" height="16" border="0" alt="<?php echo l('link_deactivate_listing', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_deactivate_listing', 'listings', '', 'button', array()); ?>"></a>
		<?php else: ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
listings/activate/<?php echo $this->_vars['item']['id']; ?>
/1"><img id="frontendimg" src="<?php echo $this->_vars['site_root']; ?>
application/views/admin/img/icon-empty.png" width="16" height="16" border="0" alt="<?php echo l('link_activate_listing', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_activate_listing', 'listings', '', 'button', array()); ?>"></a>
		    <?php endif; ?>
				<?php if ($this->_vars['item']['moderation_status'] == 'default' && $this->_vars['item']['initial_moderation']): ?><span class="status"><ins class="fa fa-check-circle fa-2x"></ins><?php echo l('listing_status_default', 'listings', '', 'text', array()); ?></span>
				<?php elseif ($this->_vars['item']['moderation_status'] == 'decline'): ?><span class="status decline"><ins class="fa fa-minus-circle e fa-2x"></ins><?php echo l('listing_status_decline', 'listings', '', 'text', array()); ?></span>
				<?php elseif ($this->_vars['item']['moderation_status'] == 'approved' || $this->_vars['item']['moderation_status'] == 'default' && ! $this->_vars['item']['initial_moderation']): ?><span class="status approved"><ins class="fa fa-check-circle fa-2x"></ins><?php echo l('listing_status_approved', 'listings', '', 'text', array()); ?></span>
				<?php elseif ($this->_vars['item']['moderation_status'] == 'wait'): ?><span class="status wait"><ins class="fa fa-clock-o g fa-2x"></ins><?php echo l('listing_status_wait', 'listings', '', 'text', array()); ?></span>
				<?php endif; ?>

			</td>

			   <td>
				<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>" class="btn-link fright" title="<?php echo l('btn_preview', 'start', '', 'button', array()); ?>"><ins class="fa fa-eye fa-lg edge hover"></ins></a>
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/edit/<?php echo $this->_vars['item']['id']; ?>
" class="btn-link fright" title="<?php echo l('btn_edit', 'start', '', 'button', array()); ?>"><ins class="fa fa-pencil fa-lg edge hover"></ins></a>
				<?php if ($this->_vars['item']['operation_type'] == 'rent'): ?><a href="<?php echo $this->_vars['site_url']; ?>
listings/edit/<?php echo $this->_vars['item']['id']; ?>
/calendar/2" class="btn-link fright" title="<?php echo l('link_calendar_view', 'listings', '', 'button', array()); ?>"><ins class="fa fa-calendar-o fa-lg edge hover"><ins class="fa fa-calendar-number"></ins></ins></a><?php endif; ?>
				<?php if ($this->_vars['item']['operation_type'] != 'buy' && $this->_vars['item']['operation_type'] != 'lease' && $this->_vars['item']['status']): ?><a href="<?php echo $this->_vars['site_url']; ?>
listings/services/<?php echo $this->_vars['item']['id']; ?>
" class="btn-link fright" title="<?php echo l('link_services', 'listings', '', 'button', array()); ?>"><ins class="fa fa-dollar fa-lg edge hover"></ins></a><?php endif; ?>				<a href="<?php echo $this->_vars['site_url']; ?>
listings/delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_delete_listing', 'listings', '', 'js', array()); ?>')) return false;" class="btn-link fright" title="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>"><ins class="fa fa-trash-o fa-lg edge hover"></ins></a>
			</td>
		</tr>
		<?php endforeach; endif; ?>
		</table>	

	</div>
	<div id="pages_block_2"><?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?></div>
	<?php else: ?>
	<div class="item empty"><?php echo l('no_listings', 'listings', '', 'text', array()); ?></div>
	<?php endif; ?>
	
	<script><?php echo '
	$(function(){
		$(\'#total_rows\').html(\'';  echo $this->_vars['page_data']['total_rows'];  echo '\');
	});
	'; ?>
</script>

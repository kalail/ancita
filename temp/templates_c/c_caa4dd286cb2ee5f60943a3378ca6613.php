<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-29 12:57:13 India Daylight Time */ ?>

<?php echo tpl_function_js(array('module' => 'reviews','file' => 'reviews-form.js'), $this);?>
	<?php switch($this->_vars['template']): case 'form':  ?>
		<?php if (! $this->_vars['is_send']): ?>
		<h1><?php echo l('header_reviews_form', 'reviews', '', 'text', array()); ?></h1>
		<div class="edit_block">
			<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. reviews. $this->module_templates.  $this->get_current_theme_gid('user', 'reviews'). "review_form.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
		</div>
		<?php endif; ?>
	<?php break; case 'first':  ?>
		<a <?php if (! $this->_vars['is_send']): ?>href="<?php echo $this->_vars['site_url']; ?>
reviews/send_review"<?php endif; ?> data-id="<?php echo $this->_vars['object_id']; ?>
" data-type="<?php echo $this->_vars['type']['gid']; ?>
" data-responder="<?php echo $this->_vars['responder_id']; ?>
" id="review_btn_<?php echo $this->_vars['rand']; ?>
" title="<?php if ($this->_vars['is_send']):  echo l('text_send_review', 'reviews', '', 'button', array());  else:  echo l('link_send_review', 'reviews', '', 'button', array());  endif; ?>"><?php echo l('link_send_first', 'reviews', '', 'text', array()); ?></a><br>
	<?php break; default:  ?>
		<a <?php if (! $this->_vars['is_send']): ?>href="<?php echo $this->_vars['site_url']; ?>
reviews/send_review"<?php endif; ?> data-id="<?php echo $this->_vars['object_id']; ?>
" data-type="<?php echo $this->_vars['type']['gid']; ?>
" data-responder="<?php echo $this->_vars['responder_id']; ?>
" id="review_btn_<?php echo $this->_vars['rand']; ?>
" class="btn-link link-r-margin" title="<?php if ($this->_vars['is_send']):  echo l('text_send_review', 'reviews', '', 'button', array());  else:  echo l('link_send_review', 'reviews', '', 'button', array());  endif; ?>"><ins class="fa fa-edit <?php if ($this->_vars['is_send']): ?>g<?php else: ?>hover<?php endif; ?> fa-lg edge"></ins></a>
<?php break; endswitch; ?>
<?php if (! $this->_vars['is_send'] || $this->_vars['template'] == 'form'): ?>
<script><?php echo '
$(function(){
	';  if ($this->_vars['is_guest']):  echo '
	new ReviewsForm({
		siteUrl: \'';  echo $this->_vars['site_root'];  echo '\', 
		';  if ($this->_vars['template'] == 'form'): ?>isPopup: false,<?php endif;  echo '
		';  if ($this->_vars['success']): ?>success: <?php $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start();  echo $this->_vars['success'];  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>,<?php endif;  echo '
		rand: ';  echo $this->_vars['rand'];  echo ',
	});	
	';  else:  echo '
	new ReviewsForm({
		siteUrl: \'';  echo $this->_vars['site_root'];  echo '\', 
		';  if ($this->_vars['template'] == 'form'): ?>isPopup: false,<?php endif;  echo '
		';  if ($this->_vars['success']): ?>success: <?php $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start();  echo $this->_vars['success'];  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>,<?php endif;  echo '
		rand: ';  echo $this->_vars['rand'];  echo ',
	});	
	';  endif;  echo '
});
'; ?>
</script>
<?php endif; ?>


<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-30 09:32:48 India Daylight Time */ ?>

<?php if ($this->_vars['data']['id']): ?>
<div class="menu-level3">
	<ul>
		<li class="<?php if ($this->_vars['section_gid'] == 'contacts'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/edit/<?php echo $this->_vars['data']['id']; ?>
/contacts"><?php echo l('filter_section_contacts', 'users', '', 'text', array()); ?></a></li>
		<li class="<?php if ($this->_vars['section_gid'] == 'seo'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/users/edit/<?php echo $this->_vars['data']['id']; ?>
/seo"><?php echo l('filter_section_seo', 'seo', '', 'text', array()); ?></a></li>
	</ul>
	&nbsp;
</div>
<?php endif; ?>

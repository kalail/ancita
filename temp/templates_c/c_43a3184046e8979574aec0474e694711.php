<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-31 13:40:40 India Daylight Time */ ?>

<aside class="lc">
	<div class="inside account_menu">
	<?php if ($this->_vars['user_session_data']['user_type']): ?>
		<?php echo tpl_function_menu(array('gid' => $this->_vars['user_session_data']['user_type'].'_account_menu','template' => 'account_menu'), $this);?>
	<?php else: ?>
		<?php echo tpl_function_menu(array('gid' => 'user_footer_menu','template' => 'account_menu'), $this);?>
	<?php endif; ?>
	
	<?php echo tpl_function_helper(array('func_name' => show_profile_info,'module' => users), $this);?>
	<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'big-left-banner'), $this);?>
	<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'left-banner'), $this);?>
	</div>
</aside>

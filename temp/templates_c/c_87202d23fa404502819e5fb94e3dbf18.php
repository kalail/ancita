<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.helper.php'); $this->register_function("helper", "tpl_function_helper");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-31 13:38:02 India Daylight Time */ ?>

<div class="content-block load_content">
	<h1><?php echo l('header_login', 'users', '', 'text', array()); ?></h1>

	<div class="inside logform">
		<form action="<?php echo $this->_vars['site_url']; ?>
users/login" method="post">
		<div class="r">
			<div class="f"><?php echo l('field_email', 'users', '', 'text', array()); ?>:&nbsp;*</div>
			<div class="v"><input type="text" name="email" <?php if ($this->_vars['DEMO_MODE']): ?>value="<?php echo $this->_run_modifier($this->_vars['demo_user_type_login_settings']['login'], 'escape', 'plugin', 1); ?>
"<?php endif; ?> onfocus="this.removeAttribute('readonly');" readonly></div>
		</div>
		<div class="r">
			<div class="f"><?php echo l('field_password', 'users', '', 'text', array()); ?>:&nbsp;*</div>
			<div class="v">
				<input type="password" name="password" <?php if ($this->_vars['DEMO_MODE']): ?>value="<?php echo $this->_run_modifier($this->_vars['demo_user_type_login_settings']['password'], 'escape', 'plugin', 1); ?>
"<?php endif; ?> onfocus="this.removeAttribute('readonly');" readonly>
				<span class="v-link"><a href="<?php echo $this->_vars['site_url']; ?>
users/restore"><?php echo l('link_restore', 'users', '', 'text', array()); ?></a></span>
			</div>
		</div>
		
		
		<div class="r">
			<input type="submit" value="<?php echo l('btn_login', 'start', '', 'button', array()); ?>" name="logbtn">
		</div>
		</form>
		<?php echo tpl_function_helper(array('func_name' => show_social_networking_login,'module' => users_connections), $this);?>
		<?php if ($this->_run_modifier($this->_vars['user_types'], 'count', 'PHP', 1)): ?>
		<div class="line top">
			<p class="header-comment"><?php echo l('text_register_comment', 'users', '', 'text', array()); ?></p>
            
             <div class="clr"></div>
			<span class="btn-link"><ins class="fa fa-arrow-right fa-lg edge hover no-hover"></ins><?php echo l('text_register_as', 'users', '', 'text', array()); ?>:</span>
			<div class="clr"></div>
			<?php if (is_array($this->_vars['user_types']) and count((array)$this->_vars['user_types'])): foreach ((array)$this->_vars['user_types'] as $this->_vars['item']): ?>
			<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'reg_'.$this->_vars['item']), $this);?>" class="btn-link btn-margin"><?php echo l('link_'.$this->_vars['item'], 'users', '', 'text', array()); ?></a>
			<?php endforeach; endif; ?>
		</div>
		<?php endif; ?>
	</div>
	<div class="clr"></div>
</div>
<script><?php echo '
	$(function(){
	var id = $(".guestlink").attr(\'data\');
	$(\'#guestlink\').attr( \'data\',id);
	});
'; ?>
</script>



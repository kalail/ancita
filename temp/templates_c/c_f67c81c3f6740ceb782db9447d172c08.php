<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-10-17 07:48:57 India Daylight Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="actions">
	<ul>
		<li><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/weather/location_edit/<?php echo $this->_vars['driver_gid']; ?>
"><?php echo l('link_location_add', 'weather', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>
<form id="locations_form" action="" method="post">
<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th><?php echo l('field_location', 'weather', '', 'text', array()); ?></th>
	<th class="w100">&nbsp;</th>
</tr>
<?php if (is_array($this->_vars['locations']) and count((array)$this->_vars['locations'])): foreach ((array)$this->_vars['locations'] as $this->_vars['item']):  echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
	<td><?php echo $this->_vars['item']['output_name']; ?>
</td>
	<td class="icons">
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/weather/location_edit/<?php echo $this->_vars['driver_gid']; ?>
/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_location_edit', 'weather', '', 'button', array()); ?>" title="<?php echo l('link_location_edit', 'weather', '', 'button', array()); ?>"></a>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/weather/location_delete/<?php echo $this->_vars['driver_gid']; ?>
/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_location_delete', 'weather', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_location_delete', 'weather', '', 'button', array()); ?>" title="<?php echo l('link_weather_delete', 'weather', '', 'button', array()); ?>"></a>
	</td>
</tr>
<?php endforeach; else: ?>
<tr><td colspan="2" class="center"><?php echo l('no_locations', 'weather', '', 'text', array()); ?></td></tr>
<?php endif; ?>
</table>
</form>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<script><?php echo '
var reload_link = "';  echo $this->_vars['site_url']; ?>
admin/weather/locations/<?php echo $this->_vars['driver_gid'];  echo '";
var order = \'';  echo $this->_vars['order'];  echo '\';
var loading_content;
var order_direction = \'';  echo $this->_vars['order_direction'];  echo '\';
function reload_this_page(value){
	var link = reload_link + \'/\' + value + \'/\' + order + \'/\' + order_direction;
	location.href=link;
}
'; ?>
</script>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

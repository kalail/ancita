<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.js.php'); $this->register_function("js", "tpl_function_js");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-10-05 09:35:19 India Daylight Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php echo tpl_function_js(array('module' => start,'file' => 'bookmark.js'), $this);?>
<div class="menu-level2" id="menu-bookmark">
	<ul>
		<li<?php if ($this->_vars['section'] == 'overview'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/start/settings/overview"><?php echo l('sett_overview_item', 'start', '', 'text', array()); ?></a></div></li>
		<li<?php if ($this->_vars['section'] == 'numerics'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/start/settings/numerics"><?php echo l('sett_numerics_item', 'start', '', 'text', array()); ?></a></div></li>
		<li<?php if ($this->_vars['section'] == 'formats'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/start/settings/formats"><?php echo l('sett_formats_item', 'start', '', 'text', array()); ?></a></div></li>
		<li<?php if ($this->_vars['section'] == 'widgets'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/start/settings/widgets"><?php echo l('sett_widgets_item', 'start', '', 'text', array()); ?></a></div></li>
		<?php if (is_array($this->_vars['other_settings']) and count((array)$this->_vars['other_settings'])): foreach ((array)$this->_vars['other_settings'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<li<?php if ($this->_vars['key'] == $this->_vars['section']): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/start/settings/<?php echo $this->_vars['key']; ?>
"><?php echo l('sett_'.$this->_vars['key'].'_item', 'start', '', 'text', array()); ?></a></div></li>
		<?php endforeach; endif; ?>
	</ul>
	&nbsp;
</div>
<div class="actions">&nbsp;</div>
<?php if ($this->_vars['section'] == 'overview'): ?>
<div class="right-side">
	<table cellspacing="0" cellpadding="0" class="stat data" width="100%">
	<?php if (is_array($this->_vars['settings_data']['other']) and count((array)$this->_vars['settings_data']['other'])): foreach ((array)$this->_vars['settings_data']['other'] as $this->_vars['module'] => $this->_vars['module_data']): ?>
	<tr>
		<th class="first" colspan=2><?php echo $this->_vars['module_data']['name']; ?>
</th>
	</tr>
	<?php if (is_array($this->_vars['module_data']['vars']) and count((array)$this->_vars['module_data']['vars'])): foreach ((array)$this->_vars['module_data']['vars'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<tr<?php if (!($this->_vars['key'] % 2)): ?> class="zebra"<?php endif; ?>>
		<td class="first"><?php echo $this->_vars['item']['field_name']; ?>
</td>
		<td>
			<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<?php if ($this->_vars['item']['value']): ?>
					<?php echo l('option_checkbox_yes', 'start', '', 'text', array()); ?>
				<?php else: ?>
					<?php echo l('option_checkbox_no', 'start', '', 'text', array()); ?>
				<?php endif; ?>
			<?php else: ?>
				<?php echo $this->_vars['item']['value']; ?>

			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; endif; ?>
	<?php endforeach; endif; ?>
	</table>
</div>

<div class="left-side">
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter,'start' => 0), $this);?>
	<table cellspacing="0" cellpadding="0" class="stat data" width="100%">
	<tr>
		<th class="first" colspan=2><?php echo l('sett_numerics_item', 'start', '', 'text', array()); ?></th>
	</tr>
	<?php if (is_array($this->_vars['settings_data']['numerics']) and count((array)$this->_vars['settings_data']['numerics'])): foreach ((array)$this->_vars['settings_data']['numerics'] as $this->_vars['module'] => $this->_vars['module_data']): ?>
	<?php if (is_array($this->_vars['module_data']['vars']) and count((array)$this->_vars['module_data']['vars'])): foreach ((array)$this->_vars['module_data']['vars'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
	<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
		<td class="first"><?php echo $this->_vars['item']['field_name']; ?>
</td>
		<td class="w100">
			<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<?php if ($this->_vars['item']['value']): ?>
					<?php echo l('option_checkbox_yes', 'start', '', 'text', array()); ?>
				<?php else: ?>
					<?php echo l('option_checkbox_no', 'start', '', 'text', array()); ?>
				<?php endif; ?>
			<?php else: ?>
				<?php echo $this->_vars['item']['value']; ?>

			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; endif; ?>
	<?php endforeach; endif; ?>
	<tr>
		<th class="first" colspan=2><?php echo l('sett_formats_item', 'start', '', 'text', array()); ?></th>
	</tr>
	<?php if (is_array($this->_vars['settings_data']['formats']) and count((array)$this->_vars['settings_data']['formats'])): foreach ((array)$this->_vars['settings_data']['formats'] as $this->_vars['module'] => $this->_vars['module_data']): ?>
	<?php if (is_array($this->_vars['module_data']['vars']) and count((array)$this->_vars['module_data']['vars'])): foreach ((array)$this->_vars['module_data']['vars'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
	<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
		<td class="first"><?php echo $this->_vars['item']['field_name']; ?>
</td>
		<td class="w100">
			<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<?php if ($this->_vars['item']['value']): ?>
					<?php echo l('option_checkbox_yes', 'start', '', 'text', array()); ?>
				<?php else: ?>
					<?php echo l('option_checkbox_no', 'start', '', 'text', array()); ?>
				<?php endif; ?>
			<?php else: ?>
				<?php echo $this->_vars['item']['value']; ?>

			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; endif; ?>
	<?php endforeach; endif; ?>
	<tr>
		<th class="first" colspan=2><?php echo l('sett_widgets_item', 'start', '', 'text', array()); ?></th>
	</tr>
	<?php if (is_array($this->_vars['settings_data']['widgets']) and count((array)$this->_vars['settings_data']['widgets'])): foreach ((array)$this->_vars['settings_data']['widgets'] as $this->_vars['module'] => $this->_vars['module_data']): ?>	
	<?php if (is_array($this->_vars['module_data']['vars']) and count((array)$this->_vars['module_data']['vars'])): foreach ((array)$this->_vars['module_data']['vars'] as $this->_vars['key'] => $this->_vars['item']): ?>
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
	<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
		<td class="first"><?php echo $this->_vars['item']['field_name']; ?>
</td>
		<td class="w100">
			<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<?php if ($this->_vars['item']['value']): ?>
					<?php echo l('option_checkbox_yes', 'start', '', 'text', array()); ?>
				<?php else: ?>
					<?php echo l('option_checkbox_no', 'start', '', 'text', array()); ?>
				<?php endif; ?>
			<?php else: ?>
				<?php echo $this->_vars['item']['value']; ?>

			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; endif; ?>
	<?php endforeach; endif; ?>
	</table>
</div>
<div class="clr"><a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/start/menu/system-items"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a></div>


<?php elseif ($this->_vars['section'] == 'numerics' || $this->_vars['section'] == 'formats' || $this->_vars['section'] == 'widgets'): ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n250">
		<?php echo tpl_function_counter(array('print' => false,'assign' => counter,'start' => 0), $this);?>
		<?php if (is_array($this->_vars['settings_data']) and count((array)$this->_vars['settings_data'])): foreach ((array)$this->_vars['settings_data'] as $this->_vars['module'] => $this->_vars['module_data']): ?>
		
		<?php if (is_array($this->_vars['module_data']['vars']) and count((array)$this->_vars['module_data']['vars'])): foreach ((array)$this->_vars['module_data']['vars'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
		<div class="row<?php if (!($this->_vars['counter'] % 2)): ?> zebra<?php endif; ?>">
			<div class="h"><?php echo $this->_vars['item']['field_name']; ?>
:</div>
			<div class="v">
				<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<input type="hidden" name="settings[<?php echo $this->_vars['module']; ?>
][<?php echo $this->_vars['item']['field']; ?>
]" value="0">
				<input type="checkbox" name="settings[<?php echo $this->_vars['module']; ?>
][<?php echo $this->_vars['item']['field']; ?>
]" value="1" <?php if ($this->_vars['item']['value']): ?>checked<?php endif; ?> class="short">
				<?php elseif ($this->_vars['item']['type'] == 'int'): ?>
				<input type="text" name="settings[<?php echo $this->_vars['module']; ?>
][<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" class="short">
				<?php elseif ($this->_vars['item']['type'] == 'text'): ?>
				<input type="text" name="settings[<?php echo $this->_vars['module']; ?>
][<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" class="middle">
				<?php elseif ($this->_vars['item']['type'] == 'textarea'): ?>
				<textarea name="settings[<?php echo $this->_vars['module']; ?>
][<?php echo $this->_vars['item']['field']; ?>
]" rows="5" cols="80"><?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
"</textarea>
				<?php else: ?>
				<input type="text" name="settings[<?php echo $this->_vars['module']; ?>
][<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" class="short">
				<?php endif; ?>
			</div>
		</div>
		<?php endforeach; endif; ?>
		<?php endforeach; endif; ?>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/start/menu/system-items"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<?php elseif ($this->_vars['section'] == 'date_formats'): ?>
	<table cellspacing="0" cellpadding="0" class="data" width="100%">
		<tr>
			<th class="w150 first"><?php echo $this->_vars['settings_data']['name']; ?>
</th>
			<th class="w150"><?php echo l('example', 'start', '', 'text', array()); ?></th>
			<th><?php echo l('date_formats_used_in', 'start', '', 'text', array()); ?></th>
			<th class="w50 center">&nbsp;</th>
		</tr>
		<?php if (is_array($this->_vars['settings_data']['vars']) and count((array)$this->_vars['settings_data']['vars'])): foreach ((array)$this->_vars['settings_data']['vars'] as $this->_vars['key'] => $this->_vars['var']): ?>
			<?php $this->assign('field', $this->_vars['var']['field']); ?>
			<?php if ($this->_vars['date_formats_pages'][$this->_vars['field']]): ?>
			<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
			<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
				<td><?php echo $this->_vars['var']['field_name']; ?>
</td>
				<td><?php echo $this->_vars['var']['value']; ?>
</td>
				<td>
					<span id="<?php echo $this->_vars['field']; ?>
" class="tooltip">
						<?php echo l('date_formats_'.$this->_vars['field'].'_description', 'start', '', 'text', array()); ?>
					</span>
					<span id="tt_<?php echo $this->_vars['field']; ?>
" class="hide">
						<div class="tooltip-info">
							<?php if (is_array($this->_vars['date_formats_pages'][$this->_vars['field']]) and count((array)$this->_vars['date_formats_pages'][$this->_vars['field']])): foreach ((array)$this->_vars['date_formats_pages'][$this->_vars['field']] as $this->_vars['page']): ?>
								<?php echo $this->_vars['site_url'];  echo $this->_vars['page']; ?>
<br/>
							<?php endforeach; endif; ?>
						</div>
					</span>
				</td>
				<td class="center">
					<a href="<?php echo $this->_vars['site_url']; ?>
admin/start/date_formats/<?php echo $this->_vars['field']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_edit_date_format', 'start', '', 'text', array()); ?>" title="<?php echo l('link_edit_date_format', 'start', '', 'text', array()); ?>"></a>
				</td>
			</tr>
			<?php endif; ?>
		<?php endforeach; else: ?>
			<tr><td colspan="8" class="center"><?php echo l('no_date_formats', 'start', '', 'text', array()); ?></td></tr>
		<?php endif; ?>
	</table>
	<?php echo tpl_function_js(array('file' => 'easyTooltip.min.js'), $this);?>
	<?php echo '
	<script type="text/javascript">
		$(function(){
			$(".tooltip").each(function(){
				$(this).easyTooltip({
					useElement: \'tt_\'+$(this).attr(\'id\'),
					yOffset: $(\'#tt_\'+$(this).attr(\'id\')).height()/2,
					clickRemove: true,
				});
			});
		});
	</script>
	'; ?>

<?php else: ?>
<form method="post" action="<?php echo $this->_vars['data']['action']; ?>
" name="save_form" enctype="multipart/form-data">
	<div class="edit-form n250">
		<div class="row header"><?php echo $this->_vars['settings_data']['name']; ?>
</div>
		<?php if (is_array($this->_vars['settings_data']['vars']) and count((array)$this->_vars['settings_data']['vars'])): foreach ((array)$this->_vars['settings_data']['vars'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<?php if (! ( $this->_vars['section'] == 'listings' && $this->_vars['item']['field'] == 'admin_moderation_emails' )): ?>
		<div class="row<?php if (!($this->_vars['key'] % 2)): ?> zebra<?php endif; ?>">
		<?php if ($this->_vars['section'] == 'countries'): ?>
			<div class="h"><?php echo $this->_vars['item']['field_name']; ?>
:</div>
			<div class="v">
				<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<input type="hidden" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="0">
				<input type="checkbox" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="1" <?php if ($this->_vars['item']['value']): ?>checked<?php endif; ?> class="short">
				<?php else: ?>
				<input type="text" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
">
				<?php endif; ?>
				<br><i><?php echo l($this->_vars['item']['field'].'_settings_descr', 'countries', '', 'text', array()); ?></i>
			</div>
		<?php else: ?>
			<div class="h"><?php echo $this->_vars['item']['field_name']; ?>
:</div>
			<div class="v">
				<?php if ($this->_vars['item']['type'] == 'checkbox'): ?>
				<input type="hidden" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="0">
				<input type="checkbox" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="1" <?php if ($this->_vars['item']['value']): ?>checked<?php endif; ?> class="short" <?php if ($this->_vars['section'] == 'listings' && $this->_vars['item']['field'] == 'moderation_send_mail'): ?>id="moderation_send_mail"<?php endif; ?>>
				<?php if ($this->_vars['section'] == 'listings' && $this->_vars['item']['field'] == 'moderation_send_mail'): ?>
				&nbsp;&nbsp;
				<?php echo $this->_vars['admin_moderation_emails']['field_name']; ?>

				<input type="text" name="settings[admin_moderation_emails]" value="<?php echo $this->_run_modifier($this->_vars['admin_moderation_emails']['value'], 'escape', 'plugin', 1); ?>
" id="admin_moderation_emails" <?php if (! $this->_vars['item']['value']): ?>disabled<?php endif; ?>> 
				<script><?php echo '
					$(function(){
						$("div.row:not(.hide):even").addClass("zebra");
						$(\'#moderation_send_mail\').bind(\'change\', function(){
							if(this.checked){
								$(\'#admin_moderation_emails\').removeAttr(\'disabled\');
							}else{
								$(\'#admin_moderation_emails\').attr(\'disabled\', \'disabled\');
							}
						});
					});
				'; ?>
</script>
				<?php endif; ?>
				<?php elseif ($this->_vars['item']['type'] == 'int'): ?>
				<input type="text" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" class="short">
				<?php elseif ($this->_vars['item']['type'] == 'text'): ?>
				<input type="text" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" class="middle">
				<?php elseif ($this->_vars['item']['type'] == 'textarea'): ?>
				<textarea name="settings[<?php echo $this->_vars['item']['field']; ?>
]" rows="5" cols="80"><?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
"</textarea>
				<?php else: ?>
				<input type="text" name="settings[<?php echo $this->_vars['item']['field']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['item']['value'], 'escape', 'plugin', 1); ?>
" class="short">
				<?php endif; ?>
			</div>
		<?php endif; ?>
		</div>
		<?php endif; ?>
		<?php endforeach; endif; ?>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['site_url']; ?>
admin/start/menu/system-items"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
</form>
<?php endif; ?>
<div class="clr"></div>
<script><?php echo '
	$(function(){
		new searchBookmark({\'bmID\': \'menu-bookmark\', bmElement: \'li\', padding: 0,});
	});
'; ?>
</script>	
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

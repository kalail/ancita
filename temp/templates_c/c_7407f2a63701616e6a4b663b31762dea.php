<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-11-01 07:52:28 India Standard Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="menu-level2">
	<ul>
		<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lang_id'] => $this->_vars['item']): ?>
		<li<?php if ($this->_vars['lang_id'] == $this->_vars['current_lang']): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/content/promo/<?php echo $this->_vars['lang_id']; ?>
"><?php echo $this->_vars['item']['name']; ?>
</a></div></li>
		<?php endforeach; endif; ?>
	</ul>
	&nbsp;
</div>

<div class="actions">&nbsp;</div>

<form method="post" action="" name="save_form">
	<div class="edit-form n150">
		<div class="row header"><?php echo l('admin_header_promo_block_main', 'content', '', 'text', array()); ?></div>
		<div class="row">
			<div class="h"><?php echo l('field_promo_type', 'content', '', 'text', array()); ?>: </div>
			<div class="v">
				<select name="content_type">
				<option value="t"<?php if ($this->_vars['promo_data']['content_type'] == 't'): ?> selected<?php endif; ?>><?php echo l('field_promo_type_text', 'content', '', 'text', array()); ?></option>	
				<option value="f"<?php if ($this->_vars['promo_data']['content_type'] == 'f'): ?> selected<?php endif; ?>><?php echo l('field_promo_type_flash', 'content', '', 'text', array()); ?></option>	
				</select>
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_block_width', 'content', '', 'text', array()); ?>: </div>
			<div class="v">
				<select name="block_width_unit" class="units">
				<option value="auto"<?php if ($this->_vars['promo_data']['block_width_unit'] == 'auto'): ?> selected<?php endif; ?>><?php echo l('field_block_unit_auto', 'content', '', 'text', array()); ?></option>	
				<option value="px"<?php if ($this->_vars['promo_data']['block_width_unit'] == 'px'): ?> selected<?php endif; ?>><?php echo l('field_block_unit_px', 'content', '', 'text', array()); ?></option>	
				<option value="%"<?php if ($this->_vars['promo_data']['block_width_unit'] == '%'): ?> selected<?php endif; ?>><?php echo l('field_block_unit_percent', 'content', '', 'text', array()); ?></option>	
				</select>
				<input type="text" class="short unit_val" name="block_width" value="<?php echo $this->_vars['promo_data']['block_width']; ?>
" <?php if ($this->_vars['promo_data']['block_width_unit'] == 'auto'): ?> disabled<?php endif; ?>>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_block_height', 'content', '', 'text', array()); ?>: </div>
			<div class="v">
				<select name="block_height_unit" class="units">
				<option value="auto"<?php if ($this->_vars['promo_data']['block_height_unit'] == 'auto'): ?> selected<?php endif; ?>><?php echo l('field_block_unit_auto', 'content', '', 'text', array()); ?></option>	
				<option value="px"<?php if ($this->_vars['promo_data']['block_height_unit'] == 'px'): ?> selected<?php endif; ?>><?php echo l('field_block_unit_px', 'content', '', 'text', array()); ?></option>	
				</select>
				<input type="text" class="short unit_val" name="block_height" value="<?php echo $this->_vars['promo_data']['block_height']; ?>
" <?php if ($this->_vars['promo_data']['block_height_unit'] == 'auto'): ?> disabled<?php endif; ?>>
			</div>
		</div>
	</div>
	<div class="btn"><div class="l"><input type="submit" name="btn_save_settings" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<div class="clr"></div>
</form>

<div class="menu-level3">
	<ul>
		<li<?php if ($this->_vars['content_type'] == 't'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/content/promo/<?php echo $this->_vars['current_lang']; ?>
/t"><?php echo l('field_promo_type_text', 'content', '', 'text', array()); ?></a></div></li>
		<li<?php if ($this->_vars['content_type'] == 'f'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/content/promo/<?php echo $this->_vars['current_lang']; ?>
/f"><?php echo l('field_promo_type_flash', 'content', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>

<form method="post" action="<?php echo $this->_vars['site_url']; ?>
admin/content/promo/<?php echo $this->_vars['current_lang']; ?>
/<?php echo $this->_vars['content_type']; ?>
" name="save_form"  enctype="multipart/form-data">
<?php if ($this->_vars['content_type'] == 't'): ?>
	<div class="edit-form n150">
		<div class="row header">&nbsp;</div>
		<div class="row">
			<div class="h"><?php echo l('field_promo_text', 'content', '', 'text', array()); ?>: </div>
			<div class="v">
				<?php echo $this->_vars['promo_data']['promo_text_fck']; ?>

			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_block_img_align_hor', 'content', '', 'text', array()); ?>: </div>
			<div class="v">
				<select name="block_align_hor">
				<option value="center"<?php if ($this->_vars['promo_data']['block_align_hor'] == 'center'): ?> selected<?php endif; ?>><?php echo l('field_block_img_align_center', 'content', '', 'text', array()); ?></option>
				<option value="left"<?php if ($this->_vars['promo_data']['block_align_hor'] == 'left'): ?> selected<?php endif; ?>><?php echo l('field_block_img_align_left', 'content', '', 'text', array()); ?></option>
				<option value="right"<?php if ($this->_vars['promo_data']['block_align_hor'] == 'right'): ?> selected<?php endif; ?>><?php echo l('field_block_img_align_right', 'content', '', 'text', array()); ?></option>
				</select>
			</div>
		</div>
		<div class="row">
			<div class="h"><?php echo l('field_block_img_align_ver', 'content', '', 'text', array()); ?>: </div>
			<div class="v">
				<select name="block_align_ver">
				<option value="center"<?php if ($this->_vars['promo_data']['block_align_ver'] == 'center'): ?> selected<?php endif; ?>><?php echo l('field_block_img_align_center', 'content', '', 'text', array()); ?></option>
				<option value="top"<?php if ($this->_vars['promo_data']['block_align_ver'] == 'top'): ?> selected<?php endif; ?>><?php echo l('field_block_img_align_top', 'content', '', 'text', array()); ?></option>
				<option value="bottom"<?php if ($this->_vars['promo_data']['block_align_ver'] == 'bottom'): ?> selected<?php endif; ?>><?php echo l('field_block_img_align_bottom', 'content', '', 'text', array()); ?></option>
				</select>
			</div>
		</div>
		<div class="row zebra">
			<div class="h"><?php echo l('field_block_img_repeating', 'content', '', 'text', array()); ?>: </div>
			<div class="v">
				<select name="block_image_repeat">
				<option value="repeat"<?php if ($this->_vars['promo_data']['block_image_repeat'] == 'repeat'): ?> selected<?php endif; ?>><?php echo l('field_block_img_repeat', 'content', '', 'text', array()); ?></option>
				<option value="no-repeat"<?php if ($this->_vars['promo_data']['block_image_repeat'] == 'no-repeat'): ?> selected<?php endif; ?>><?php echo l('field_block_img_no_repeat', 'content', '', 'text', array()); ?></option>
				</select>
			</div>
		</div>
        <div class="row">
			<div class="h"><?php echo l('field_block_img_text_color', 'content', '', 'text', array()); ?>: </div>
			<div class="v">
                <select name="promo_text_color">
                	<option value="black" <?php if ($this->_vars['promo_data']['promo_text_color'] == 'black'): ?> selected<?php endif; ?>><?php echo l('field_block_img_black', 'content', '', 'text', array()); ?></option>
                    <option value="white" <?php if ($this->_vars['promo_data']['promo_text_color'] == 'white'): ?> selected<?php endif; ?>><?php echo l('field_block_img_White', 'content', '', 'text', array()); ?></option>
                </select>
			</div>
		</div>
        <div class="btn"><div class="l"><input type="submit" name="btn_save_content" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
        <div class="clr"></div>
        
        
         <?php echo tpl_function_js(array('file' => 'ajaxfileupload.min.js'), $this);?>
	<?php echo tpl_function_js(array('file' => 'gallery-uploads.js'), $this);?>
    
    <script><?php echo '
        var gUpload;
        $(function(){
            gUpload = new galleryUploads({
                siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
        });
    });
    '; ?>

	</script>
        
        <div class="row header"><?php echo l('field_block_slider_tit', 'content', '', 'text', array()); ?></div>
        <div class="row zebra">
        	<div class="h"><?php echo l('field_block_slider_sel', 'content', '', 'text', array()); ?>:</div>
			<div class="v">
				<input type="file" name="promo_slide_image">
			</div>
		</div>
        <div class="row zebra">
			<div class="h"><?php echo l('field_block_slider_tim', 'content', '', 'text', array()); ?>:</div>
			<div class="v">
                <input type="text" id="block_each_image_seconds" name="block_each_image_seconds" value="10" style="width:100px !important;" />
			</div>
		</div>
        <div class="row zebra">
			<div class="h"><?php echo l('field_block_slider_order', 'content', '', 'text', array()); ?>:</div>
			<div class="v">
                <input type="text" id="slider_image_order" name="slider_image_order" value="" style="width:100px !important;" />
			</div>
		</div>
        <div class="row zebra">
			<div class="h"><?php echo l('field_block_slider_adnum', 'content', '', 'text', array()); ?>:</div>
			<div class="v">
                <input type="text" id="promo_url" name="promo_url" value="" style="width:400px !important;" />
			</div>
		</div>
         <div class="row zebra">
			<div class="h"><?php echo l('field_block_slider_showtext', 'content', '', 'text', array()); ?>:</div>
			<div class="v">
                <input type="checkbox" id="feature1" name="feature1" value="" onclick="javascript: check();"/>
			</div>
		</div>
        <div class="row" id="rwnewslide">
        	<div class="btn"><div class="l"><input type="submit" name="btn_Add_Slide" value="<?php echo l('btn_add', 'start', '', 'button', array()); ?>"></div></div>
        </div>
        
        <div class="row" id="rwupdateslide" style="display:none;">
        	<div class="btn"><div class="l"><input type="submit" name="btn_Update_Slide" value="Update"></div></div>
            <div class="btn"><div class="l"><input type="button" name="btn_Reset_Slide" onclick="javascript: Reset();return false;" value="Reset"></div></div>
        </div>
	        <input type="hidden" id="block_slide_id" name="block_slide_id" value="" />
            <input type="hidden" id="Lang_id" name="Lang_id" value="<?php echo $this->_vars['current_lang']; ?>
" />
             <input type="hidden" id="hidden_order" name="hidden_order" value="" />
		<div class="clr"></div>
        
        
        <div class="row">
        
        <ol class="blocks" id="sortList">
        
        <?php if (is_array($this->_vars['promo_data_slide']) and count((array)$this->_vars['promo_data_slide'])): foreach ((array)$this->_vars['promo_data_slide'] as $this->_vars['item']): ?>
<li id="pItem<?php echo $this->_vars['item']['Id']; ?>
">
            
            <div class="photo-info-area">
                <div class="photo-area">
                <img src="<?php echo $this->_vars['item']['Promo_slide']['file_url']; ?>
" hspace="3" onclick="javascript: gUpload.full_view('<?php echo $this->_vars['item']['Promo_slide']['file_url']; ?>
');" style="width:200px;height:200px;" />
                    
		<br>
        <b> 
        	Order: 
        	<font class="stat-active">
        		<?php echo $this->_vars['item']['SortingOrder']; ?>

           	</font>
		</b>
        <b> 
        	Seconds: 
        	<font class="stat-active">
        		<?php echo $this->_vars['item']['promo_each_image_seconds']; ?>

           	</font>
		</b>
		<br />
        <b> 
        	URL: 
        	<font class="stat-active">
        		<?php echo $this->_vars['item']['promo_url']; ?>

           	</font>
		</b>  
                    <script><?php echo '
						function Edit(editId,url,seconds,order,show){
							$("#block_slide_id").val(editId);
							$("#block_each_image_seconds").val(seconds);
							$("#promo_url").val(url);
							$("#feature1").val(show);
							$("#hidden_order").val(order);
							$("#slider_image_order").val(order);
							$("#rwnewslide").attr("style","display:none");
							$("#rwupdateslide").attr("style","display:block");
							$("#block_each_image_seconds").focus();
							$(":checkbox[value=1]").prop("checked","true");
						}
						
						function Reset(){
							$("#block_slide_id").val(\'\');
							$("#block_each_image_seconds").val(\'\');
							$("#promo_url").val(\'\');
							$("#slider_image_order").val(\'\');
							$("#rwnewslide").attr("style","display:block");
							$("#rwupdateslide").attr("style","display:none");
							}
						 function check(){
                            $(\'#feature1\').val(this.checked ? 0:1);
						   }
						'; ?>

					</script>
                    
                    <a href="#" onclick="javascript: Edit(<?php echo $this->_vars['item']['Id']; ?>
,'<?php echo $this->_vars['item']['promo_url']; ?>
',<?php echo $this->_vars['item']['promo_each_image_seconds']; ?>
,<?php echo $this->_vars['item']['SortingOrder']; ?>
,<?php echo $this->_vars['item']['feature1']; ?>
); return false;">
                    <img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('btn_edit', 'start', '', 'button', array()); ?>" title="<?php echo l('btn_edit', 'start', '', 'button', array()); ?>">
                    </a>

                    <a href="<?php echo $this->_vars['site_root']; ?>
admin/content/delete_slide_photo/<?php echo $this->_vars['item']['Id']; ?>
/<?php echo $this->_vars['current_lang']; ?>
/<?php echo $this->_vars['item']['Promo_slide']['file_name']; ?>
" >
                         <img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>" title="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>">
                    </a>
                    
                	<div class="clr"></div>
            
				</div>
                </div>
            </li>
            
		<?php endforeach; endif; ?>
        </ol>
		<div class="clr"></div>
        </div>
        
        
		<!--	<div class="row">
			<div class="h"><?php echo l('field_promo_img', 'content', '', 'text', array()); ?> 1: </div>
			<div class="v">
				<input type="file" name="promo_image">
				<?php if ($this->_vars['promo_data']['promo_image']): ?><br><img src="<?php echo $this->_vars['promo_data']['media']['promo_image']['file_url']; ?>
" width="500"><?php endif; ?>
			</div>
		</div>
		<?php if ($this->_vars['promo_data']['promo_image']): ?>
		<div class="row zebra">
			<div class="h"><?php echo l('field_promo_image_delete', 'content', '', 'text', array()); ?>: </div>
			<div class="v"><input type="checkbox" name="promo_image_delete" value="1"></div>
		</div>
		<?php endif; ?>-->
      
	</div>
<?php endif; ?>
<?php if ($this->_vars['content_type'] == 'f'): ?>
	<div class="edit-form n150">
		<div class="row header">&nbsp;</div>
		<div class="row">
			<div class="h"><?php echo l('field_promo_flash', 'content', '', 'text', array()); ?>: </div>
			<div class="v">
				<input type="file" name="promo_flash"><br>
				<?php if ($this->_vars['promo_data']['promo_flash']): ?><i><?php echo l('field_promo_flash_uploaded', 'content', '', 'text', array()); ?></i><?php endif; ?>
			</div>
		</div>
		<?php if ($this->_vars['promo_data']['promo_flash']): ?>
		<div class="row zebra">
			<div class="h"><?php echo l('field_promo_flash_delete', 'content', '', 'text', array()); ?>: </div>
			<div class="v"><input type="checkbox" name="promo_flash_delete" value="1"></div>
		</div>
		<?php endif; ?>
	</div>
    
    <div class="btn"><div class="l"><input type="submit" name="btn_save_content" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>

<?php endif; ?>
	<div class="clr"></div>
</form>
<script type="text/javascript"><?php echo '
$(function(){
	$(\'.units\').bind(\'change\', function(){
		if($(this).val() == \'auto\'){
			$(this).parent().find(\'input.unit_val\').attr(\'disabled\', \'disabled\');
		}else{
			$(this).parent().find(\'input.unit_val\').removeAttr(\'disabled\');
		}	
	});
});
'; ?>
</script>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

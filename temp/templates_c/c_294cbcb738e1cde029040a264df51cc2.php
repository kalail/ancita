<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-07 13:18:15 India Daylight Time */ ?>

<?php echo tpl_function_js(array('module' => listings,'file' => 'listings-search.js'), $this);?>
<script> 
	var selects_<?php echo $this->_vars['search_form_settings']['rand']; ?>
 = []; 
	var selectObject_<?php echo $this->_vars['search_form_settings']['rand']; ?>
;
</script>
<?php if ($this->_vars['search_filters_block']): ?>
<div class="listings_form edit_block" id="save_search_block">
	<h2><?php echo l('header_current_search', 'listings', '', 'text', array()); ?></h2>
	<?php if ($this->_vars['use_save_search']): ?>
	<form id="save_search_form" name="save_search_form" action="<?php echo $this->_vars['site_url']; ?>
listings/save_search" method="POST">
		<div id='search_filter_block'><?php echo $this->_vars['search_filters_block']; ?>
</div>
		<div class="clr"></div>
		<div class="r"><input type="submit" value="<?php echo l('btn_save_search', 'listings', '', 'button', array()); ?>" id="btn_save_search"></div>
		<input type="hidden" name="form_name" value="save_search" />
	</form>
	<div id="saved_searches_block"><?php echo $this->_vars['saved_searches_block']; ?>
</div>
	<?php else: ?>
	<div id="save_search_form">
		<div id='search_filter_block'><?php echo $this->_vars['search_filters_block']; ?>
</div>
		<div class="clr"></div>
	</div>
	<?php endif; ?>
</div>
<?php endif; ?>

<?php $this->assign('operation_type', $this->_vars['search_form_settings']['object']); ?>
<div class="listings_form edit_block">
	<h2 id="quick_search_block"><?php echo l('header_refine_search', 'listings', '', 'text', array()); ?></h2>
	<form id="quick_search_form" name="search_listing_form" action="" method="POST">
	<div class="r">
	<div class="f"><?php echo l('field_listing_type', 'listings', '', 'text', array()); ?>:</div>
		<div class="v">
			<select name="filters[type]" id="operation_type">
			<?php if (is_array($this->_vars['search_form_settings']['operations']) and count((array)$this->_vars['search_form_settings']['operations'])): foreach ((array)$this->_vars['search_form_settings']['operations'] as $this->_vars['item']): ?>
            <?php if ($this->_vars['item'] == $this->_vars['data']['type']): ?>
			<option value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['item'] == $this->_vars['data']['type']): ?>selected<?php endif; ?>><?php echo l('operation_search_'.$this->_vars['item'], 'listings', '', 'text', array()); ?></option>
            <?php endif; ?>
			<?php endforeach; endif; ?>
			</select>
			<script><?php echo '
				$(function(){
					$(document).on(\'change\', \'#operation_type\', function(){
						switch($(this).val()){
							case \'sale\':
								$(\'#with_photo_box';  echo $this->_vars['search_form_settings']['rand'];  echo '\').show();
								$(\'#booking_date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').hide();
								$(\'#date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').val(\'\');
								$(\'#alt_date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').val(\'\');
								$(\'#booking_date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').hide();
								$(\'#date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').val(\'\');
								$(\'#alt_date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').val(\'\');
								$(\'#booking_guests';  echo $this->_vars['search_form_settings']['rand'];  echo '\').hide();
								$(\'#booking_guests';  echo $this->_vars['search_form_settings']['rand'];  echo ' select\').val(\'\');
							break;
							case \'buy\':
								$(\'#with_photo_box';  echo $this->_vars['search_form_settings']['rand'];  echo '\').hide();
								$(\'#with_photo';  echo $this->_vars['search_form_settings']['rand'];  echo '\').removeAttr(\'checked\');
								$(\'#booking_date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').hide();
								$(\'#date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').val(\'\');
								$(\'#alt_date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').val(\'\');
								$(\'#booking_date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').hide();
								$(\'#date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').val(\'\');
								$(\'#alt_date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').val(\'\');
								$(\'#booking_guests';  echo $this->_vars['search_form_settings']['rand'];  echo '\').hide();
								$(\'#booking_guests';  echo $this->_vars['search_form_settings']['rand'];  echo ' select\').val(\'\');
							break;
							case \'rent\':
								$(\'#with_photo_box';  echo $this->_vars['search_form_settings']['rand'];  echo '\').show();
								$(\'#booking_date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').show();
								$(\'#booking_date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').show();
								$(\'#booking_guests';  echo $this->_vars['search_form_settings']['rand'];  echo '\').show();
							break;
							case \'lease\':
								$(\'#with_photo_box';  echo $this->_vars['search_form_settings']['rand'];  echo '\').hide();
								$(\'#with_photo';  echo $this->_vars['search_form_settings']['rand'];  echo '\').removeAttr(\'checked\');
								$(\'#booking_date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').show();
								$(\'#booking_date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').show();
								$(\'#booking_guests';  echo $this->_vars['search_form_settings']['rand'];  echo '\').show();
							break;
							case \'sold\':
								$(\'#with_photo_box';  echo $this->_vars['search_form_settings']['rand'];  echo '\').show();
								$(\'#booking_date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').hide();
								$(\'#date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').val(\'\');
								$(\'#alt_date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').val(\'\');
								$(\'#booking_date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').hide();
								$(\'#date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').val(\'\');
								$(\'#alt_date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').val(\'\');
								$(\'#booking_guests';  echo $this->_vars['search_form_settings']['rand'];  echo '\').hide();
								$(\'#booking_guests';  echo $this->_vars['search_form_settings']['rand'];  echo ' select\').val(\'\');
							break;
						}
					});
				});
				$(document).ready(function(){
                $("#submit").click(function(){
					var id = $("#id").val();
					var key = $("#keyword").val();
					if( id != "" && key != "")
					{
						$("#err").css("display","block");
						$("#err").html(\'<p class="error" style="color:red;">one of the fields have to be empty!</p>\');
						return false;
					}
					$("#err").css("display","none");
                });
                });
			'; ?>
</script>
		</div>
	</div>	
	<div class="r">
		<div class="f"><?php echo l('field_location', 'listings', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo tpl_function_country_input(array('select_type' => 'city','id_country' => $this->_vars['data']['id_country'],'id_region' => $this->_vars['data']['id_region'],'id_city' => $this->_vars['data']['id_city'],'id_district' => $this->_vars['data']['id_district'],'var_country' => 'filters[id_country]','var_region' => 'filters[id_region]','var_city' => 'filters[id_city]','var_district' => 'filters[id_district]'), $this);?></div>
	</div>	
	<div class="r">
		<div class="f"><?php echo l('field_category', 'listings', '', 'text', array()); ?>:</div>
		<div class="v">
			<?php if ($this->_vars['data']['id_category'] && $this->_vars['data']['property_type']): ?>
				<?php $this->assign('category', $this->_vars['data']['id_category'].'_'.$this->_vars['data']['property_type']); ?>
			<?php else: ?>
				<?php $this->assign('category', $this->_vars['data']['id_category']); ?>
			<?php endif; ?>
			<!--<?php echo tpl_function_block(array('name' => 'properties_select','module' => 'properties','var_name' => 'filters[category]','js_var_name' => 'category','selected' => $this->_vars['category'],'cat_select' => true), $this);?>-->
			<?php echo tpl_function_selectbox(array('input' => 'filters[category]','id' => 'id_category_select_'.$this->_vars['search_form_settings']['rand'],'value' => $this->_vars['property_types'],'subvalue' => $this->_vars['property_items'],'selected' => $this->_vars['property_type_value'],'default' => $this->_vars['property_type_default'],'formtype' => 'IndexSearch'), $this);?>
			<script> selects_<?php echo $this->_vars['search_form_settings']['rand']; ?>
.push('id_category_select_<?php echo $this->_vars['search_form_settings']['rand']; ?>
'); </script>		</div>
	</div>
	<div class="r periodbox <?php if ($this->_vars['operation_type'] != 'rent' && $this->_vars['operation_type'] != 'lease'): ?>hide<?php endif; ?>" id="booking_date_start<?php echo $this->_vars['search_form_settings']['rand']; ?>
">
		<div class="f"><?php echo l('field_booking_date_start', 'listings', '', 'text', array()); ?>:</div>
		<div class="v">
			<input type="text" name="booking_date_start" value="<?php if ($this->_run_modifier($this->_vars['data']['booking_date_start'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['data']['booking_date_start'], 'date_format', 'plugin', 1, $this->_vars['search_form_settings']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="date_start<?php echo $this->_vars['search_form_settings']['rand']; ?>
">
			<input type="hidden" name="filters[booking_date_start]" value="<?php if ($this->_run_modifier($this->_vars['data']['booking_date_start'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['data']['booking_date_start'], 'date_format', 'plugin', 1, '%Y-%m-%d'), 'escape', 'plugin', 1);  endif; ?>" id="alt_date_start<?php echo $this->_vars['search_form_settings']['rand']; ?>
">
			<script><?php echo '
				$(function(){
					$(\'#date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').datepicker({dateFormat: \'';  echo $this->_vars['search_form_settings']['datepicker_date_format'];  echo '\', altFormat: \'yy-mm-dd\', altField: \'#alt_date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\', showOn: \'both\'});
				});
			'; ?>
</script>
		</div>
	</div>	
	<div class="r periodbox <?php if ($this->_vars['operation_type'] != 'rent' && $this->_vars['operation_type'] != 'lease'): ?>hide<?php endif; ?>" id="booking_date_end<?php echo $this->_vars['search_form_settings']['rand']; ?>
">
		<div class="f"><?php echo l('field_booking_date_end', 'listings', '', 'text', array()); ?></div>
		<div class="v">
			<input type="text" name="booking_date_end" value="<?php if ($this->_run_modifier($this->_vars['data']['booking_date_end'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['data']['booking_date_end'], 'date_format', 'plugin', 1, $this->_vars['search_form_settings']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="date_end<?php echo $this->_vars['search_form_settings']['rand']; ?>
">
			<input type="hidden" name="filters[booking_date_end]" value="<?php if ($this->_run_modifier($this->_vars['data']['booking_date_end'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['data']['booking_date_end'], 'date_format', 'plugin', 1, '%Y-%m-%d'), 'escape', 'plugin', 1);  endif; ?>" id="alt_date_end<?php echo $this->_vars['search_form_settings']['rand']; ?>
">
			<script><?php echo '
				$(function(){
					$(\'#date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').datepicker({dateFormat: \'';  echo $this->_vars['search_form_settings']['datepicker_date_format'];  echo '\', altFormat: \'yy-mm-dd\', altField: \'#alt_date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\', showOn: \'both\'});
				});
			'; ?>
</script>
		</div>
	</div>	
	
	<?php if ($this->_vars['operation_type'] != 'rent'): ?>	
	<div class="r many pricebox">
		<div class="f"><?php echo l('field_price_range', 'listings', '', 'text', array()); ?>: <?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => '','cur_gid' => $this->_vars['base_currency']['gid'],'cur_custom' => 'manual'), $this);?></div>
		<div class="v">
			<link rel="stylesheet" href="<?php echo $this->_vars['site_root'];  echo $this->_vars['js_folder']; ?>
jquery-ui/jquery-ui.custom.css" type="text/css" />
			<link rel="stylesheet" href="<?php echo $this->_vars['site_root'];  echo $this->_vars['js_folder']; ?>
slider/css/ui.slider.extras.css" type="text/css" />
			<?php echo tpl_function_js(array('file' => 'slider/jquery.ui.slider-'.$this->_vars['_LANG']['rtl'].'.js'), $this);?>
			<?php echo tpl_function_js(array('file' => 'slider/selectToUISlider.jQuery.js'), $this);?>
			<?php echo tpl_function_js(array('file' => 'number-format.js'), $this);?>
			<script><?php echo '
				var price_ranges = {};
				var currency_output = ';  echo tpl_function_block(array('name' => 'currency_format_regexp_output','module' => 'start','cur_custom' => 'customformat'), $this); echo '
				'; ?>

				<?php if (is_array($this->_vars['price_range']) and count((array)$this->_vars['price_range'])): foreach ((array)$this->_vars['price_range'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'min_price')); tpl_block_capture(array('assign' => 'min_price'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
					currency_output(<?php echo $this->_vars['item']['min_price']; ?>
)
				<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
				<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'max_price')); tpl_block_capture(array('assign' => 'max_price'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
					currency_output(<?php echo $this->_vars['item']['max_price']; ?>
)
				<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
				price_ranges['<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
'] = <?php echo '{'; ?>
data: [], min: <?php echo $this->_run_modifier($this->_vars['min_price'], 'strip_tags', 'PHP', 1); ?>
, max: <?php echo $this->_run_modifier($this->_vars['max_price'], 'strip_tags', 'PHP', 1);  echo '}'; ?>
;
				<?php if (is_array($this->_vars['item']['data']) and count((array)$this->_vars['item']['data'])): foreach ((array)$this->_vars['item']['data'] as $this->_vars['key2'] => $this->_vars['item2']): ?>
				<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'price')); tpl_block_capture(array('assign' => 'price'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
					currency_output(<?php echo $this->_vars['item2']; ?>
)
				<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
				price_ranges['<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
']['data'].push(<?php echo '{'; ?>
value: '<?php echo $this->_vars['item2']; ?>
', text: <?php echo $this->_run_modifier($this->_vars['price'], 'strip_tags', 'PHP', 1);  echo '}'; ?>
);
				<?php endforeach; endif; ?>
				price_ranges['<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
']['data'].push(<?php echo '{'; ?>
value: '<?php echo $this->_vars['item']['max_price']; ?>
', text: <?php echo $this->_run_modifier($this->_vars['max_price'], 'strip_tags', 'PHP', 1);  echo '}'; ?>
);
				<?php endforeach; endif; ?>
				<?php echo '
				$(function(){	
					$(\'#price_min option\').each(function(index, item){
						var item = $(item);
						item.html(currency_output(item.attr(\'value\')));
					});
					
					$(\'#price_max option\').each(function(index, item){
						var item = $(item);
						item.html(currency_output(item.attr(\'value\')));
					});
					
					var price_min_selected_val = $(\'#price_min_selected_val\');
					price_min_selected_val.html(currency_output(price_min_selected_val.html()));
					
					var price_max_selected_val = $(\'#price_max_selected_val\');
					price_max_selected_val.html(currency_output(price_max_selected_val.html()));
								
					$(\'select#price_min, select#price_max\').selectToUISlider({
						labels : 2,
						tooltip: false,
						tooltipSrc : \'text\',
						labelSrc: \'text\',
						isRTL: ';  if ($this->_vars['_LANG']['rtl'] == 'rtl'): ?>true<?php else: ?>false<?php endif;  echo ',
					});
					$(\'#operation_type\').bind(\'change\', function(){
						var type = $(this).val();
						
						$(\'.pricebox .ui-slider\').slider(\'destroy\').remove();
						
						var price_min = $(\'#price_min\');
						price_min.empty();
						for(var i in price_ranges[type][\'data\']){
							$("<option />", {value: price_ranges[type][\'data\'][i].value, text: price_ranges[type][\'data\'][i].text}).appendTo(price_min);
						}
						
						var price_max = $(\'#price_max\');
						price_max.empty();
						for(var i in price_ranges[type][\'data\']){
							$("<option />", {value: price_ranges[type][\'data\'][i].value, text: price_ranges[type][\'data\'][i].text, selected: true}).appendTo(price_max);
						}
						
						$(\'#price_min_selected_val\').html(price_ranges[type].min);
						$(\'#price_max_selected_val\').html(price_ranges[type].max);
						
						$(\'select#price_min, select#price_max\').selectToUISlider({
							labels : 2,
							tooltip: false,
							tooltipSrc : \'text\',
							labelSrc: \'text\',
						});
					});
				});
			'; ?>
</script>
			<div class="select-slider prices">	
				<select id="price_min" name="filters[price_min]" class="hide">
					<?php if (is_array($this->_vars['price_range'][$this->_vars['operation_type']]['data']) and count((array)$this->_vars['price_range'][$this->_vars['operation_type']]['data'])): foreach ((array)$this->_vars['price_range'][$this->_vars['operation_type']]['data'] as $this->_vars['item']): ?>
					<option value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['item'] == $this->_vars['data']['price_min']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
					<?php endforeach; endif; ?>
					<option value="<?php echo $this->_run_modifier($this->_vars['price_range'][$this->_vars['operation_type']]['max_price'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['price_range'][$this->_vars['operation_type']]['max_price'] == $this->_vars['data']['price_min']): ?>selected<?php endif; ?>><?php echo $this->_vars['price_range'][$this->_vars['operation_type']]['max_price']; ?>
</option>
				</select>
									
				<select id="price_max" name="filters[price_max]" class="hide">
					<?php if (is_array($this->_vars['price_range'][$this->_vars['operation_type']]['data']) and count((array)$this->_vars['price_range'][$this->_vars['operation_type']]['data'])): foreach ((array)$this->_vars['price_range'][$this->_vars['operation_type']]['data'] as $this->_vars['item']): ?>
					<option value="<?php echo $this->_run_modifier($this->_vars['item'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['item'] == $this->_vars['data']['price_max']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
					<?php endforeach; endif; ?>
					<option value="<?php echo $this->_run_modifier($this->_vars['price_range'][$this->_vars['operation_type']]['max_price'], 'escape', 'plugin', 1); ?>
" <?php if (! $this->_vars['data']['price_max']): ?>selected<?php endif; ?>><?php echo $this->_vars['price_range'][$this->_vars['operation_type']]['max_price']; ?>
</option>
				</select>
									
				<div class="vals">
					<div id="price_min_selected_val" class="fleft" dir="ltr">
							<?php echo $this->_vars['price_range'][$this->_vars['operation_type']]['min_price']; ?>

					</div>
					<div id="price_max_selected_val" class="fright" dir="ltr">
							<?php echo $this->_vars['price_range'][$this->_vars['operation_type']]['max_price']; ?>

					</div>
				</div>
				<div class="clr"></div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div id="quick_search_extend_form"><?php echo $this->_vars['quick_search_extend_form']; ?>
</div>
	<div class="r <?php if ($this->_vars['quick_search_extend_form']): ?>separator<?php endif; ?>" style="display:none;">
		<div id="with_photo_box<?php echo $this->_vars['search_form_settings']['rand']; ?>
" <?php if ($this->_vars['operation_type'] == 'buy' || $this->_vars['operation_type'] == 'lease'): ?>class="hide"<?php endif; ?>>
			<input type="hidden" name="filters[with_photo]" value="0">
			<input type="checkbox" name="filters[with_photo]" value="1" id="with_photo<?php echo $this->_vars['search_form_settings']['rand']; ?>
" <?php if ($this->_vars['data']['with_photo']): ?>checked<?php endif; ?>>
			<label for="with_photo<?php echo $this->_vars['search_form_settings']['rand']; ?>
"><?php echo l('field_with_photo', 'listings', '', 'text', array()); ?></label><br>
		</div>
		
		<input type="hidden" name="filters[by_open_house]" value="0">
		<input type="checkbox" name="filters[by_open_house]" value="1" id="open_house" <?php if ($this->_vars['data']['open_house']): ?>checked<?php endif; ?>>
		<label for="open_house"><?php echo l('field_open_house', 'listings', '', 'text', array()); ?></label><br>
		
		<input type="hidden" name="filters[by_private]" value="0">
		<input type="checkbox" name="filters[by_private]" value="1" id="by_private" <?php if ($this->_vars['data']['by_private']): ?>checked<?php endif; ?>>
		<label for="by_private"><?php echo l('field_by_private', 'listings', '', 'text', array()); ?></label>
	</div>
	<div class="r">
		<input type="submit" value="<?php echo l('btn_search', 'start', '', 'button', array()); ?>" />
		<input type='reset' id='reset1' value="<?php echo l('btn_clear', 'start', '', 'button', array()); ?>"/>
	</div>
	<input type="hidden" name="form" value="quick_search_form" />
	</form>
</div>

<div class="listings_form edit_block">
	<h2><?php echo l('header_extend_refine_search', 'listings', '', 'text', array()); ?></h2>
	<div id="err"></div>
	<form id="advanced_search_form" name="extend_search_listing_form" action="" method="POST">		
		<div id="advanced_search_extend_form"><?php echo $this->_vars['advanced_search_extend_form']; ?>
</div>
		<div class="r <?php if ($this->_vars['advanced_search_extend_form']): ?>separator<?php endif; ?>">
			<div class="f"><?php echo l('field_id', 'listings', '', 'text', array()); ?>:</div>
			<div class="v"><input type="text" id="id" name="filters[id]" value="" onfocus="this.removeAttribute('readonly');" readonly></div>
		</div>
		<div class="r">
			<div class="f"><?php echo l('field_postal_code', 'listings', '', 'text', array()); ?>:</div>
			<div class="v"><input type="text" name="filters[zip]" value="<?php echo $this->_run_modifier($this->_vars['data']['zip'], 'escape', 'plugin', 1); ?>
" onfocus="this.removeAttribute('readonly');" readonly></div>
		</div>
		<!-- div class="r">
			<div class="f"><?php echo l('field_radius', 'listings', '', 'text', array()); ?>:</div>
			<div class="v">
				<select name="filters[radius]">
					<option value=""><?php echo l('text_radius_select', 'listings', '', 'text', array()); ?></option>
					<?php if (is_array($this->_vars['radius_data']['option']) and count((array)$this->_vars['radius_data']['option'])): foreach ((array)$this->_vars['radius_data']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
					<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['key'] == $this->_vars['data']['radius']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
					<?php endforeach; endif; ?>
				</select>
			</div>
		</div -->
		<div class="r">
			<div class="f">Search text:</div>
			<div class="v"><input type="text" id="keyword" name="filters[keyword]" value="<?php echo $this->_run_modifier($this->_vars['data']['keyword'], 'escape', 'plugin', 1); ?>
" onfocus="this.removeAttribute('readonly');" readonly></div>
		</div>
		<div class="r">
			<input type="submit" id="submit nev" value="<?php echo l('btn_search', 'start', '', 'button', array()); ?>">
			 <input type='reset' id='reset2' value="<?php echo l('btn_clear', 'start', '', 'button', array()); ?>"/>
		</div>
		<input type="hidden" name="form" value="advanced_search_form">
	</form>
</div>	
	
<script><?php echo '
	$(function(){
		new listingSearch({
			siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
			';  if ($this->_vars['view_mode']): ?>viewMode: '<?php echo $this->_vars['view_mode']; ?>
',<?php endif;  echo '
			tIds: [\'save_search_block\'],
		});
		
		if(selectObject_';  echo $this->_vars['search_form_settings']['rand'];  echo ') selectObject_';  echo $this->_vars['search_form_settings']['rand'];  echo '.clear();
		selectObject_';  echo $this->_vars['search_form_settings']['rand'];  echo ' = new selectBox({elementsIDs: selects_';  echo $this->_vars['search_form_settings']['rand'];  echo ', selectDropdownClass: \'dropdown\'});
	});
'; ?>
</script>
<script><?php echo '
	$("#reset1").on("click", function () {
    $(".label").empty();
	$(".dropdown ul li.active label input").filter(\':checkbox\').removeAttr(\'checked\');
	$(".dropdown ul li.active").removeClass("active");
    $("#handle_price_min").css(\'left\',\'100%\');
	$(".ui-state-default").css(\'left\',\'100%\');
	$("#price_max_selected_val").empty().append("900 000");
	$(".fleft").empty().append("0");
	$(".bth_rooms_1_max").empty().append("10");
	$(".bth_rooms_4_max").empty().append("10");
	$(".bd_rooms_1_max").empty().append("10");
	$(".bd_rooms_4_max").empty().append("10");
	$(".garages_1_max").empty().append("5+");  
	$(".garages_4_max").empty().append("5+");
	$(\'#date_start';  echo $this->_vars['search_form_settings']['rand'];  echo '\').removeAttr(\'value\');
	$(\'#date_end';  echo $this->_vars['search_form_settings']['rand'];  echo '\').removeAttr(\'value\');
    $(".ui-slider-horizontal").css(\'background\',\'#3D95CB\').css(\'height\',\'4px\');
	});
'; ?>
</script>
<script><?php echo '
	$(function(){
		  $("#handle_price_max").css(\'left\',\'100%\');
	});
'; ?>
</script>

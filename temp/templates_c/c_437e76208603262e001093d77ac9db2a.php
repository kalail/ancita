<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.user_select.php'); $this->register_function("user_select", "tpl_function_user_select");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-11 08:54:32 India Daylight Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="menu-level2">
	<ul>
		<li<?php if ($this->_vars['tab_id'] == 'list'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/import/import_automatic_listings/list">Import definitions</a></div></li>
        <li<?php if ($this->_vars['tab_id'] == 'new' || $this->_vars['tab_id'] == 'edit'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/import/import_automatic_listings/<?php if ($this->_vars['tab_id'] == 'edit'):  echo $this->_vars['strText']['tmplink'];  else: ?>new<?php endif; ?>"><?php echo $this->_vars['strText']['tabtxt']; ?>
</a></div></li>
	</ul>
	&nbsp;
</div>
<div class="actions">&nbsp;</div>
<form method="post" name="save_form" action="<?php echo $this->_vars['site_url']; ?>
admin/import/import_automatic_listings/<?php if ($this->_vars['tab_id'] == 'edit'):  echo $this->_vars['strText']['tmplink'];  else:  echo $this->_vars['tab_id'];  endif; ?>">
<?php if ($this->_vars['tab_id'] == 'list'): ?>
<div class="edit-form edit-form-marketing">
<div class="row header">Import Definitions</div>
<div class="row"></div>
<table cellpadding="0" cellspacing="1" width="100%" border="0" class="data">
	<tr>
    	<th>Name</th>
        <th>Link</th>
        <th>Import Type</th>
        <th>Frequency</th>
        <th>Overwrite Type</th>
        <th>Copy english text</th>
        <th>Add property ID</th>
        <th>Started</th>
        <th>&nbsp;</th>
    </tr>
    <?php if ($this->_vars['xmldata']): ?>
    <?php if (is_array($this->_vars['xmldata']) and count((array)$this->_vars['xmldata'])): foreach ((array)$this->_vars['xmldata'] as $this->_vars['key'] => $this->_vars['item']): ?>
    <tr>
        <td><?php echo $this->_vars['item']['user_name']; ?>
</td>
        <td><?php echo $this->_vars['item']['link']; ?>
</td>
        <td><?php echo $this->_vars['item']['import_type']; ?>
</td>
        <td><?php echo $this->_vars['item']['frequency']; ?>
</td>
        <td><?php if ($this->_vars['item']['overwrite_type'] == '1000'): ?>Overwrite all the existing records
            <?php elseif ($this->_vars['item']['overwrite_type'] == '1001'): ?>Skip update of existing records
            <?php elseif ($this->_vars['item']['overwrite_type'] == '1002'): ?>Do not overwrite existing records with blank data<?php endif; ?>
        </td>
        <td><?php echo $this->_vars['item']['copy_english_text']; ?>
</td>
        <td><?php echo $this->_vars['item']['add_property_id']; ?>
</td>
        <td><?php echo $this->_run_modifier($this->_vars['item']['date_start'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
        <td>
        	<a href="<?php echo $this->_vars['site_url']; ?>
admin/import/import_automatic_listings/edit/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_edit_user', 'users', '', 'button', array()); ?>" title="<?php echo l('link_edit_data', 'import', '', 'button', array()); ?>"></a>
            <a href="<?php echo $this->_vars['site_url']; ?>
admin/import/import_automatic_listings/list/<?php echo $this->_vars['item']['id']; ?>
/delete" onclick="javascript: if(!confirm('Are you sure you want to delete this rule?')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_data', 'import', '', 'button', array()); ?>" title="<?php echo l('link_delete_data', 'import', '', 'button', array()); ?>"></a>
        </td>
    </tr>
    <?php endforeach; endif; ?>
    <?php else: ?>
    <tr>
        <td colspan="9" class="error" align="center">Import Definitions is empty</td>
    </tr>
    <?php endif; ?>
</table>
</div>
<?php endif; ?>
<?php if ($this->_vars['tab_id'] == 'new' || $this->_vars['tab_id'] == 'edit'): ?>
<div class="edit-form edit-form-marketing">
<div class="row header"><?php echo $this->_vars['strText']['subtitle']; ?>
</div>
<table style="width:100%;">
<tr height="40">
    <td>Name</td><td><?php echo tpl_function_user_select(array('selected' => $this->_vars['custVal']['user_id'],'max' => 1,'var_name' => 'id_user'), $this);?>Chose Ancita user for definition assignment</td>
</tr>
<tr height="40">
	<td>Link</td><td><input type="text" style="width:95%;" name="txtlink" value="<?php echo $this->_vars['custVal']['link']; ?>
" /></td>
</tr>
<tr height="40">
	<td>Import Type</td>
    <td>
    	<select name="import_type" id="import_type" style="width: 320px;">
            <option value="0">Please make a choice of the import type</option>
            <option value="kyero" <?php if ($this->_vars['custVal']['import_type'] == 'kyero'): ?> selected <?php endif; ?>>Kyero</option>
            <option value="alphashare" <?php if ($this->_vars['custVal']['import_type'] == 'alphashare'): ?> selected <?php endif; ?>>Alphashare</option>
            <option value="thinkspain" <?php if ($this->_vars['custVal']['import_type'] == 'thinkspain'): ?> selected <?php endif; ?>>Thinkspain</option>
        </select>
    </td>
</tr>
<tr height="40">
	<td>Frequency</td>
    <td>
    	<select name="frequency" id="frequency" style="width: 320px;">
            <option value="0">Please make a choice of the import frequency</option>
            <option value="daily" <?php if ($this->_vars['custVal']['frequency'] == 'daily'): ?> selected <?php endif; ?>>Daily</option>
            <option value="weekly" <?php if ($this->_vars['custVal']['frequency'] == 'weekly'): ?> selected <?php endif; ?>>Weekly</option>
        </select>
    </td>
</tr>
<tr height="40">
	<td>Overwrite Type</td>
    <td>
    	<select name="overwrite_type" id="overwrite_type" style="width: 320px;">
            <option value="0">Please make a choice of the overwrite type</option>
            <option value="1000" <?php if ($this->_vars['custVal']['overwrite_type'] == '1000'): ?> selected <?php endif; ?>>Overwrite all the existing records</option>
            <option value="1001" <?php if ($this->_vars['custVal']['overwrite_type'] == '1001'): ?> selected <?php endif; ?>>Skip update of existing records</option>
            <option value="1002" <?php if ($this->_vars['custVal']['overwrite_type'] == '1002'): ?> selected <?php endif; ?>>Do not overwrite existing records with blank data</option>
        </select>
    </td>
</tr>
<tr height="40">
	<td style="vertical-align:top;">Additional options</td>
	 <td>
    	<table cellpadding="0" cellspacing="1" border="0">
            <tr height="3"></tr>											
            <tr>
                <td>
                    <label>Copy the English heading text into the Norwegian and Swedish language?</label>
                </td>
                <td><input type="radio" name="copy_english_text" value="true" <?php if ($this->_vars['custVal']['copy_english_text'] == 'true'): ?> checked <?php endif; ?>/><span>Yes</span></td>
                <td><input type="radio" name="copy_english_text" value="false" <?php if ($this->_vars['custVal']['copy_english_text'] == 'false'): ?> checked <?php endif; ?>/><span>No</span></td>
            </tr>
            <tr height="3"></tr>												
            <tr>
                <td>
                    <label>Add Company agent property id to the heading?</label>
                </td>
                <td><input type="radio" name="add_property_id" value="true" <?php if ($this->_vars['custVal']['add_property_id'] == 'true'): ?> checked <?php endif; ?>/><span>Yes</span></td>
                <td><input type="radio" name="add_property_id" value="false" <?php if ($this->_vars['custVal']['add_property_id'] == 'false'): ?> checked <?php endif; ?>/><span>No</span></td>											
            </tr>
        </table>
    </td>
</tr>
<tr>
	<td></td>
    <td><div class="h"><div class="btn"><div class="l"><input type="submit" name="save_btn_definition" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div></div></td>
</tr>
</table>
</div>
<?php endif; ?>
</form>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

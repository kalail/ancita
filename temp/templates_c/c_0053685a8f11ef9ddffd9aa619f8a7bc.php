<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-10-21 08:08:29 India Daylight Time */ ?>

<div class="load_content_controller">
			<?php switch($this->_vars['type']): case 'country':  ?>
			<h1><?php echo l('header_country_select', 'countries', '', 'text', array()); ?></h1>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'form_back_link')); tpl_block_capture(array('assign' => 'form_back_link'), null, $this); ob_start(); ?><a href="#" id="country_select_back" class="btn-link" style="display:none;"><ins class="fa fa-arrow-left fa-lg edge"></ins><?php echo l('link_reset_country', 'countries', '', 'text', array()); ?></a><?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<?php break; case 'region':  ?>
			<h1><?php echo l('header_region_select', 'countries', '', 'text', array()); ?></h1>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'form_crumb')); tpl_block_capture(array('assign' => 'form_crumb'), null, $this); ob_start(); ?><div class="crumb"><?php echo $this->_vars['data']['country']['name']; ?>
</div><?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'form_back_link')); tpl_block_capture(array('assign' => 'form_back_link'), null, $this); ob_start(); ?><a href="#" id="country_select_back" class="btn-link" style="display:none;"><ins class="fa fa-arrow-left fa-lg edge"></ins><?php echo l('link_select_another_country', 'countries', '', 'text', array()); ?></a><?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<?php break; case 'city':  ?>
			<h1><?php echo l('header_city_select', 'countries', '', 'text', array()); ?></h1>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'form_crumb')); tpl_block_capture(array('assign' => 'form_crumb'), null, $this); ob_start(); ?><div class="crumb"><?php echo $this->_vars['data']['country']['name']; ?>
 > <?php echo $this->_vars['data']['region']['name']; ?>
</div><?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'form_search')); tpl_block_capture(array('assign' => 'form_search'), null, $this); ob_start(); ?><input type="text" id="city_search" class="controller-search"><?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'form_back_link')); tpl_block_capture(array('assign' => 'form_back_link'), null, $this); ob_start(); ?><a href="#" id="country_select_back" class="btn-link"><ins class="fa fa-arrow-left fa-lg edge"></ins><?php echo l('link_select_another_region', 'countries', '', 'text', array()); ?></a><?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'form_pages')); tpl_block_capture(array('assign' => 'form_pages'), null, $this); ob_start(); ?><div id="city_page" class="fright"></div><?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<?php break; case 'district':  ?>
			<h1><?php echo l('header_district_select', 'countries', '', 'text', array()); ?></h1>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'form_crumb')); tpl_block_capture(array('assign' => 'form_crumb'), null, $this); ob_start(); ?><div class="crumb"><?php echo $this->_vars['data']['country']['name']; ?>
 > <?php echo $this->_vars['data']['region']['name']; ?>
 > <?php echo $this->_vars['data']['city']['name']; ?>
</div><?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'form_search')); tpl_block_capture(array('assign' => 'form_search'), null, $this); ob_start(); ?><input type="text" id="district_search" class="controller-search"><?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'form_back_link')); tpl_block_capture(array('assign' => 'form_back_link'), null, $this); ob_start(); ?><a href="#" id="country_select_back" class="btn-link"><ins class="fa fa-arrow-left fa-lg edge"></ins><?php echo l('link_select_another_city', 'countries', '', 'text', array());  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'form_pages')); tpl_block_capture(array('assign' => 'form_pages'), null, $this); ob_start(); ?><div id="district_page" class="fright"></div><?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<?php break; default:  ?>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'form_back_link')); tpl_block_capture(array('assign' => 'form_back_link'), null, $this); ob_start(); ?><a href="#" id="country_select_back" class="btn-link"><ins class="fa fa-arrow-left fa-lg edge"></ins><?php echo l('link_reset_country', 'countries', '', 'text', array()); ?></a><?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
	<?php break; endswitch; ?>
	<div class="inside">
		<?php echo $this->_vars['form_crumb'];  echo $this->_vars['form_search']; ?>

		<ul class="controller-items" id="country_select_items"></ul>
		<div class="controller-actions"><?php echo $this->_vars['form_pages']; ?>
<div>
			<div><?php echo $this->_vars['form_back_link']; ?>
</div>
			<div class="fright">
				<a href="javascript:void(0);" id="country_select_close" class="btn-link"><ins class="fa fa-arrow-left fa-lg edge"></ins><?php echo l('link_close', 'countries', '', 'text', array()); ?></a>
			</div>
		</div>
	</div>
</div>

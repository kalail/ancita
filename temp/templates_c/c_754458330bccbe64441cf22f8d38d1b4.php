<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-11 05:53:07 India Daylight Time */ ?>

<?php if ($this->_vars['breadcrumbs']): ?>
<div class="breadcrumb">
<a href="<?php echo $this->_vars['site_url'];  if (! $this->_vars['is_guest']): ?>start/homepage<?php endif; ?>"><?php echo l('link_home', 'menu', '', 'text', array()); ?></a>
<?php if (is_array($this->_vars['breadcrumbs']) and count((array)$this->_vars['breadcrumbs'])): foreach ((array)$this->_vars['breadcrumbs'] as $this->_vars['item']): ?>
&nbsp;>&nbsp;<?php if ($this->_vars['item']['url']): ?>
<?php if ($this->_vars['item']['text'] == 'Want to rent?' || $this->_vars['item']['text'] == 'Leie bolig?' || $this->_vars['item']['text'] == 'Hyra bostad?'): ?>
<a href="<?php echo $this->_vars['item']['url']; ?>
" id="<?php echo $this->_vars['item']['text']; ?>
"><?php echo l('agents_search_page_title', 'users', '', 'text', array()); ?></a>
<?php else: ?>
<a href="<?php echo $this->_vars['item']['url']; ?>
" id="<?php echo $this->_vars['item']['text']; ?>
"><?php echo $this->_vars['item']['text']; ?>
</a>
<?php endif; ?>
<?php else:  echo $this->_vars['item']['text']; ?>

<?php endif; ?>
<?php endforeach; endif; ?>
</div>
<?php endif; ?>

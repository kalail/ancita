<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.lower.php'); $this->register_modifier("lower", "tpl_modifier_lower");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.helper.php'); $this->register_function("helper", "tpl_function_helper");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-10-17 07:49:57 India Daylight Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<?php echo tpl_function_helper(array('func_name' => get_admin_level1_menu,'helper_name' => menu,'func_param' => 'admin_reviews_menu'), $this);?>
<div class="actions">&nbsp;</div>

<div class="menu-level3">
	<ul>
		<?php if (is_array($this->_vars['types']) and count((array)$this->_vars['types'])): foreach ((array)$this->_vars['types'] as $this->_vars['key'] => $this->_vars['item']): ?>
		<li class="<?php if ($this->_vars['type_gid'] == $this->_vars['key']): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
admin/reviews/index/<?php echo $this->_vars['key']; ?>
"><?php echo $this->_vars['item']; ?>
</a></li>
		<?php endforeach; endif; ?>
	</ul>
	&nbsp;
</div>
<table cellspacing="0" cellpadding="0" class="data" width="100%">
	<tr>
		<th class="first w100">Name</th>
        <th>Email</th>
		<th class="w150"><?php echo l('field_reviews_rate', 'reviews', '', 'text', array()); ?></th>
		<th><?php echo l('field_reviews_message', 'reviews', '', 'text', array()); ?></th>
		<th><a href="<?php echo $this->_vars['sort_links']['date_add']; ?>
"<?php if ($this->_vars['order'] == 'date_add'): ?> class="<?php echo $this->_run_modifier($this->_vars['order_direction'], 'lower', 'plugin', 1); ?>
"<?php endif; ?>><?php echo l('field_reviews_date_add', 'reviews', '', 'text', array()); ?></a></th>
		<th class="w70">&nbsp;</th>
	</tr>
	<?php if (is_array($this->_vars['reviews']) and count((array)$this->_vars['reviews'])): foreach ((array)$this->_vars['reviews'] as $this->_vars['item']): ?>
	<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
	<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>				
		<td class="center"><?php echo $this->_vars['item']['rname']; ?>
</td>
        <td><?php echo $this->_vars['item']['remail']; ?>
</td>
		<td><?php echo tpl_function_block(array('name' => get_rate_block,'module' => reviews,'rating_data' => $this->_vars['item']['rating_data'],'type_gid' => $this->_vars['item']['gid_type'],'template' => 'mini','read_only' => 'true','mode' => 'admin'), $this);?></td>
		<td><?php echo $this->_run_modifier($this->_vars['item']['message'], 'truncate', 'plugin', 1, 100); ?>
</td>
		<td class="center"><?php echo $this->_run_modifier($this->_vars['item']['date_add'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
</td>
		<td class="icons">
			<a href="<?php echo $this->_vars['site_url']; ?>
admin/reviews/edit/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_reviews_edit', 'reviews', '', 'button', array()); ?>" title="<?php echo l('link_reviews_edit', 'reviews', '', 'button', array()); ?>"></a>
            <?php if ($this->_vars['item']['approve'] != '1'): ?>
            <a href="<?php echo $this->_vars['site_url']; ?>
admin/reviews/order_approve/<?php echo $this->_vars['item']['id']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-approve.png" width="16" height="16" border="0" alt="<?php echo l('link_review_approve', 'reviews', '', 'button', array()); ?>" title="<?php echo l('link_review_approve', 'reviews', '', 'button', array()); ?>"></a>
            <?php endif; ?>
			<a href="<?php echo $this->_vars['site_url']; ?>
admin/reviews/delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_reviews_delete', 'reviews', '', 'js', array()); ?>')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_reviews_delete', 'reviews', '', 'button', array()); ?>" title="<?php echo l('link_reviews_delete', 'reviews', '', 'button', array()); ?>"></a>
		</td>
	</tr>
	<?php endforeach; else: ?>
	<tr><td colspan="5" class="center"><?php echo l('no_reviews', 'reviews', '', 'text', array()); ?></td></tr>
	<?php endif; ?>
</table>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

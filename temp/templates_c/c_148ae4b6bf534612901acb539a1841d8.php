<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-30 09:32:47 India Daylight Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. users. $this->module_templates.  $this->get_current_theme_gid('', 'users'). "edit_users_menu.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php if ($this->_vars['section_gid'] == 'contacts'): ?>
	<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. users. $this->module_templates.  $this->get_current_theme_gid('admin', 'users'). $this->_vars['user_type']."_admin_form.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  elseif ($this->_vars['section_gid'] == 'seo'): ?>
		<?php if (is_array($this->_vars['seo_fields']) and count((array)$this->_vars['seo_fields'])): foreach ((array)$this->_vars['seo_fields'] as $this->_vars['key'] => $this->_vars['section']): ?>
	<form method="post" action="<?php echo $this->_run_modifier($this->_vars['data']['action'], 'escape', 'plugin', 1); ?>
" name="seo_<?php echo $this->_vars['section']['gid']; ?>
_form">
	<div class="edit-form n150">
		<div class="row header"><?php echo $this->_vars['section']['name']; ?>
</div>
		<?php if ($this->_vars['section']['tooltip']): ?>
		<div class="row">
			<div class="h">&nbsp;</div>
			<div class="v"><?php echo $this->_vars['section']['tooltip']; ?>
</div>
		</div>
		<?php endif; ?>		
		<?php if (is_array($this->_vars['section']['fields']) and count((array)$this->_vars['section']['fields'])): foreach ((array)$this->_vars['section']['fields'] as $this->_vars['field']): ?>
		<div class="row">
			<div class="h"><?php echo $this->_vars['field']['name']; ?>
: </div>
			<div class="v">
				<?php $this->assign('field_gid', $this->_vars['field']['gid']); ?>
									<?php switch($this->_vars['field']['type']): case 'checkbox':  ?>
						<input type="hidden" name="<?php echo $this->_vars['section']['gid']; ?>
[<?php echo $this->_vars['field_gid']; ?>
]" value="0">
						<input type="checkbox" name="<?php echo $this->_vars['section']['gid']; ?>
[<?php echo $this->_vars['field_gid']; ?>
]" value="1" <?php if ($this->_vars['seo_settings'][$this->_vars['field_gid']]): ?>checked<?php endif; ?>>
					<?php break; case 'text':  ?>
						<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lang_id'] => $this->_vars['lang_item']): ?>
						<?php $this->assign('section_gid', $this->_vars['section']['gid'].'_'.$this->_vars['lang_id']); ?>
						<input type="<?php if ($this->_vars['lang_id'] == $this->_vars['current_lang_id']): ?>text<?php else: ?>hidden<?php endif; ?>" name="<?php echo $this->_vars['section']['gid']; ?>
[<?php echo $this->_vars['field_gid']; ?>
][<?php echo $this->_vars['lang_id']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['seo_settings'][$this->_vars['section_gid']][$this->_vars['field_gid']], 'escape', 'plugin', 1); ?>
" class="long" lang-editor="value" lang-editor-type="<?php echo $this->_vars['section']['gid']; ?>
_<?php echo $this->_vars['field_gid']; ?>
" lang-editor-lid="<?php echo $this->_vars['lang_id']; ?>
">
						<?php endforeach; endif; ?>
						<a href="#" lang-editor="button" lang-editor-type="<?php echo $this->_vars['section']['gid']; ?>
_<?php echo $this->_vars['field_gid']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-translate.png" width="16" height="16" alt="<?php echo l('note_types_translate', 'reviews', '', 'button', array()); ?>" title="<?php echo l('note_types_translate', 'reviews', '', 'button', array()); ?>"></a>
					<?php break; case 'textarea':  ?>
						<?php if (is_array($this->_vars['languages']) and count((array)$this->_vars['languages'])): foreach ((array)$this->_vars['languages'] as $this->_vars['lang_id'] => $this->_vars['lang_item']): ?>
							<?php $this->assign('section_gid', $this->_vars['section']['gid'].'_'.$this->_vars['lang_id']); ?>
							<?php if ($this->_vars['lang_id'] == $this->_vars['current_lang_id']): ?>
							<textarea name="<?php echo $this->_vars['section']['gid']; ?>
[<?php echo $this->_vars['field_gid']; ?>
][<?php echo $this->_vars['lang_id']; ?>
]" rows="5" cols="80" class="long" lang-editor="value" lang-editor-type="<?php echo $this->_vars['section']['gid']; ?>
_<?php echo $this->_vars['field_gid']; ?>
" lang-editor-lid="<?php echo $this->_vars['lang_id']; ?>
"><?php echo $this->_run_modifier($this->_vars['seo_settings'][$this->_vars['section_gid']][$this->_vars['field_gid']], 'escape', 'plugin', 1); ?>
</textarea>
							<?php else: ?>
							<input type="hidden" name="<?php echo $this->_vars['section']['gid']; ?>
[<?php echo $this->_vars['field_gid']; ?>
][<?php echo $this->_vars['lang_id']; ?>
]" value="<?php echo $this->_run_modifier($this->_vars['seo_settings'][$this->_vars['section_gid']][$this->_vars['field_gid']][$this->_vars['lang_id']], 'escape', 'plugin', 1); ?>
">
							<?php endif; ?>
						<?php endforeach; endif; ?>
						<a href="#" lang-editor="button" lang-editor-type="<?php echo $this->_vars['section']['gid']; ?>
_<?php echo $this->_vars['field']['gid']; ?>
" lang-field-type="textarea"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-translate.png" width="16" height="16" alt="<?php echo l('note_types_translate', 'reviews', '', 'button', array()); ?>" title="<?php echo l('note_types_translate', 'reviews', '', 'button', array()); ?>"></a>					
				<?php break; endswitch; ?><br><?php echo $this->_vars['field']['tooltip']; ?>

			</div>
		</div>
		<?php endforeach; endif; ?>	
	</div>	
	<div class="btn"><div class="l"><input type="submit" name="btn_save_<?php echo $this->_vars['section']['gid']; ?>
" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
	<a class="cancel" href="<?php echo $this->_vars['back_url']; ?>
"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>	
	<input type="hidden" name="btn_save" value="1">
	</form>
	<div class="clr"></div>
	<?php endforeach; endif; ?>
	<?php echo tpl_function_block(array('name' => lang_inline_editor,'module' => start), $this);?>
	<?php endif; ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

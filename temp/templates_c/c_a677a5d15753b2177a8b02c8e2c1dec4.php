<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-11 05:53:08 India Daylight Time */ ?>

<!--
    Task    : CR-2015-01 -2.3 - Make Monthly offer as it is in existing Ancita version
	Date    : 06-Nov-2015
Modified By : Mahi-->

<?php if (( $this->_vars['promo']['content_type'] == 't' && ( $this->_vars['promo']['promo_image'] || $this->_vars['promo']['promo_text'] ) ) || ( $this->_vars['promo']['content_type'] == 'f' && $this->_vars['promo']['promo_flash'] )): ?>
<?php if ($this->_vars['promo']['block_width'] > 980 && $this->_vars['promo']['block_width_unit'] == 'px' && $this->_vars['promo']['block_height'] == 440 && $this->_vars['promo']['block_height_unit'] == 'px'): ?>
<div class="promo-block-wrapper">
<div class="promo-block-wrapper2">
<div class="promo-block-wrapper3">
<div class="promo-block-wrapper4">
<?php endif; ?>


<?php echo tpl_function_js(array('file' => 'jquery.bxslider'), $this);?>

<script>
	<?php echo '
  	$(document).ready(function () {
            var slider = $(\'#slider1\').bxSlider();
            modifyDelay(0);
            $(".feature1").show();
            function modifyDelay(startSlide) {
                var duration = $(\'#slider1 li:nth-child(\' + (startSlide + 2) + \')\').attr(\'duration\')*1000;
                if (duration == "") {
                    duration = 10000;
                } else {
                    duration = parseInt(duration);
                }

                slider.reloadSlider({
                    mode: \'horizontal\',
                    auto: true,
                    pause: duration,
                    autoControls: true,
                    speed: 1000,
                    startSlide: startSlide,
                    onSlideAfter: function ($el, oldIndex, newIndex) {
                        modifyDelay(newIndex);
                    }
                });
            }

        });
		
		
	'; ?>

</script>



<div class="promo-block" style="width: <?php echo $this->_vars['promo']['block_width'];  echo $this->_vars['promo']['block_width_unit']; ?>
; height: <?php echo $this->_vars['promo']['block_height'];  echo $this->_vars['promo']['block_height_unit']; ?>
;">

 	<div class="slider"  style="width: <?php echo $this->_vars['promo']['block_width'];  echo $this->_vars['promo']['block_width_unit']; ?>
; height: <?php echo $this->_vars['promo']['block_height'];  echo $this->_vars['promo']['block_height_unit']; ?>
;">
    
        <ul id="slider1" style="width: <?php echo $this->_vars['promo']['block_width'];  echo $this->_vars['promo']['block_width_unit']; ?>
; height: <?php echo $this->_vars['promo']['block_height'];  echo $this->_vars['promo']['block_height_unit']; ?>
;margin: 0px; padding: 0px;">
        
        	<?php if (is_array($this->_vars['promo_data_slide']) and count((array)$this->_vars['promo_data_slide'])): foreach ((array)$this->_vars['promo_data_slide'] as $this->_vars['item']): ?>

       		<li duration="<?php if ($this->_vars['item']['promo_each_image_seconds'] == '0'): ?>10000<?php else:  echo $this->_vars['item']['promo_each_image_seconds'];  endif; ?>">
            <a href="<?php echo $this->_vars['item']['promo_url']; ?>
">
             	<img src="<?php echo $this->_vars['item']['Promo_slide']['file_url']; ?>
" width="<?php echo $this->_vars['promo']['block_width'];  echo $this->_vars['promo']['block_width_unit']; ?>
" height="<?php echo $this->_vars['promo']['block_height'];  echo $this->_vars['promo']['block_height_unit']; ?>
" />
            </a>
              <?php if ($this->_vars['item']['feature1'] == 1): ?>
              <?php if ($this->_vars['promo']['content_type'] == 't'): ?>
              <?php if ($this->_vars['promo']['block_width'] > 980 && $this->_vars['promo']['block_width_unit'] == 'px' && $this->_vars['promo']['block_height'] == 440 && $this->_vars['promo']['block_height_unit'] == 'px'): ?>
             <div class="inside-wrapper">
              <?php endif; ?>
              <div class="inside feature<?php echo $this->_vars['item']['feature1']; ?>
" style="display:none;">
              <div class="panel" style="color:<?php echo $this->_vars['promo']['promo_text_color']; ?>
 !important"><?php echo $this->_vars['promo']['promo_text']; ?>
</div>
              <div class="background"></div>
              </div>
             <?php if ($this->_vars['promo']['block_width'] > 980 && $this->_vars['promo']['block_width_unit'] == 'px' && $this->_vars['promo']['block_height'] == 440 && $this->_vars['promo']['block_height_unit'] == 'px'): ?>
              </div>
              <?php endif; ?>
              <?php endif; ?>
              <?php endif; ?>
        	</li>
           <?php endforeach; endif; ?>

        </ul>
    </div>

<?php if ($this->_vars['promo']['content_type'] == 'f'): ?>
<object width="100%" height="100%" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">
<param value="Always" name="allowScriptAccess">
<param value="<?php echo $this->_vars['promo']['media']['promo_flash']['file_url']; ?>
" name="movie">
<param value="false" name="menu">
<param value="high" name="quality">
<param value="opaque" name="wmode">
<param value="" name="flashvars">
<embed width="100%" height="100%" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" swliveconnect="FALSE" menu="false" wmode="opaque" allowscriptaccess="Always" quality="high" flashvars="" src="<?php echo $this->_vars['promo']['media']['promo_flash']['file_url']; ?>
"> 
</object>
<?php endif; ?>
</div>

<?php if ($this->_vars['promo']['block_width'] > 980 && $this->_vars['promo']['block_width_unit'] == 'px' && $this->_vars['promo']['block_height'] == 440 && $this->_vars['promo']['block_height_unit'] == 'px'): ?>
<div class="gradient_wrapper">
	<div class="gradient_wrapper2">
		<div class="gradient">
			<div class="gradient-l"></div>
			<div class="gradient-r"></div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
<?php endif; ?>
<?php endif; ?>

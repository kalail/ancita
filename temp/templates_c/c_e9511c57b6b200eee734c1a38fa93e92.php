<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-10-17 07:49:38 India Daylight Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="menu-level2">
	<ul>
		<li<?php if ($this->_vars['tab_id'] == 'Placefactors'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/content/marketing/Placefactors"><?php echo l('calc_admin_heading1', 'content', '', 'text', array()); ?></a></div></li>
        <li<?php if ($this->_vars['tab_id'] == 'Commonfactors'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/content/marketing/Commonfactors"><?php echo l('calc_admin_heading2', 'content', '', 'text', array()); ?></a></div></li>
        <li<?php if ($this->_vars['tab_id'] == 'Excelimport'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/content/marketing/Excelimport"><?php echo l('calc_admin_heading3', 'content', '', 'text', array()); ?></a></div></li>
	</ul>
	&nbsp;
</div>
<div class="actions">&nbsp;</div>
<form method="post" name="save_form" action="<?php echo $this->_vars['site_url']; ?>
admin/content/marketing/<?php echo $this->_vars['tab_id']; ?>
" enctype="multipart/form-data">
<?php if ($this->_vars['tab_id'] == 'Placefactors'): ?>
<div class="edit-form edit-form-marketing">
<div class="row header"><?php echo l('calc_admin_lbl_new_functionality', 'content', '', 'text', array()); ?></div>
<div class="row zebra">
<table>
	<tr>
    	<td width="160px"><?php echo l('calc_admin_lbl_new_functionality_on', 'content', '', 'text', array()); ?></td>
        <td>
        	<input type="checkbox" name="check_functionality" value="<?php echo $this->_vars['factors_graph']['functionality']; ?>
"  <?php if ($this->_vars['factors_graph']['functionality'] == 'on'): ?>checked <?php endif; ?>  />
        </td>
        <td>
        	<div class="btn"><div class="l"><input type="submit" name="btn_trunon_new_functionality" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
        </td>
    </tr>
</table>
</div>
</div>
<?php endif; ?>
<?php if ($this->_vars['tab_id'] != 'Excelimport'): ?>
<?php if ($this->_vars['factors_graph']['functionality'] != 'on' || $this->_vars['tab_id'] == 'Commonfactors'): ?>
<div class="edit-form edit-form-marketing">
<div class="row header"><?php echo l('calc_admin_version_setting', 'content', '', 'text', array()); ?></div>
<div class="row zebra">
<table>
	<tr>
    	<td width="100px"><?php echo l('calc_admin_select_version', 'content', '', 'text', array()); ?></td>
        <td>
        	<select name="versionMonth" id="versionMonth" onchange="javascript:changeVersion();">
                <?php if (is_array($this->_vars['arrMonth']) and count((array)$this->_vars['arrMonth'])): foreach ((array)$this->_vars['arrMonth'] as $this->_vars['key'] => $this->_vars['month']): ?>
                	<option value="<?php if ($this->_vars['key'] != 0):  echo $this->_vars['key'];  endif; ?>" <?php if ($this->_vars['version']['Month'] == $this->_vars['key']): ?> selected <?php endif; ?> ><?php echo $this->_vars['month']; ?>
</option>
                <?php endforeach; endif; ?>
            </select>
            <input type="hidden" name="crntVersionMonth" id="crntVersionMonth" value="<?php echo $this->_vars['crntversion']['Month']; ?>
" />
        </td>
        <td>
        	<select name="versionYear" id="versionYear" onchange="javascript:changeVersion();">
            	<option value=""><?php echo l('calc_admin_option_select_year', 'content', '', 'text', array()); ?></option>
                <?php if (is_array($this->_vars['arrYear']) and count((array)$this->_vars['arrYear'])): foreach ((array)$this->_vars['arrYear'] as $this->_vars['key'] => $this->_vars['year']): ?>
                	<option value="<?php echo $this->_vars['year']; ?>
" <?php if ($this->_vars['version']['Year'] == $this->_vars['year']): ?> selected <?php endif; ?> ><?php echo $this->_vars['year']; ?>
</option>
                <?php endforeach; endif; ?>
            </select>
            <input type="hidden" name="crntVersionYear" id="crntVersionYear" value="<?php echo $this->_vars['crntversion']['Year']; ?>
" />
        </td>
        <td>
        	<input type="checkbox" value="1" name="IsActiveVersion" <?php if ($this->_vars['IsActiveVersion'] == '1'): ?> checked <?php endif; ?> /> <?php echo l('calc_admin_is_active_version', 'content', '', 'text', array()); ?>
        </td>
    </tr>
</table>
</div>
<?php echo '
<script>
	function changeVersion()
	{
		var nvMonth = $(\'#versionMonth option:selected\').val();
		var nvYear = $(\'#versionYear option:selected\').val();
		var ovMonth = $(\'#crntVersionMonth\').val();
		var ovYear = $(\'#crntVersionYear\').val();
		
		location.href = \'';  echo $this->_vars['site_root'];  echo 'admin/content/marketing/';  echo $this->_vars['tab_id'];  echo '/Change/\'+nvMonth+\'/\'+nvYear+\'/\'+ovMonth+\'/\'+ovYear;
	}
</script>
'; ?>

</div>
<?php endif; ?>
<?php endif; ?>
<?php if ($this->_vars['tab_id'] == 'Placefactors'): ?>
<div class="edit-form edit-form-marketing">
<div class="row header"><?php echo l('calc_admin_lbl_price_factor', 'content', '', 'text', array()); ?></div>
<div class="row">
	<table style="width:100%;">
    <tr>
    	<th><?php echo l('calc_admin_region', 'content', '', 'text', array()); ?></th>
        <th><?php echo l('calc_admin_city', 'content', '', 'text', array()); ?></th>
        <th><?php echo l('calc_admin_appartment_price', 'content', '', 'text', array()); ?></th>
        <th><?php echo l('calc_admin_house_price', 'content', '', 'text', array()); ?></th>
        <th><?php echo l('calc_admin_urbanicasion', 'content', '', 'text', array()); ?></th>
        <th><?php echo l('calc_admin_area_adjustment', 'content', '', 'text', array()); ?></th>
    </tr>
    <tr height="5px;"></tr>
    <?php if (is_array($this->_vars['cities']) and count((array)$this->_vars['cities'])): foreach ((array)$this->_vars['cities'] as $this->_vars['item']): ?>
    <tr height="5px"></tr>
    <tr>
    	<td><?php echo $this->_vars['item']['region_name']; ?>
</td>
        <td style="padding-left: 20px;"><?php echo $this->_vars['item']['city_name']; ?>
</td>
        <td><input size="8" type="text" name="appartment_<?php echo $this->_vars['item']['city_id']; ?>
" value="<?php echo $this->_vars['item']['appartment_price']; ?>
"></td>
        <td><input size="8" type="text" name="house_<?php echo $this->_vars['item']['city_id']; ?>
" value="<?php echo $this->_vars['item']['house_price']; ?>
" style="width:auto;"></td>
        <?php if ($this->_vars['item']['urbanication'] != NULL): ?>
            <?php if (is_array($this->_vars['item']['urbanication']) and count((array)$this->_vars['item']['urbanication'])): foreach ((array)$this->_vars['item']['urbanication'] as $this->_vars['item_u']): ?>
            	<?php if ($this->_vars['item_u']['row'] > 0): ?>
                	</tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                <?php endif; ?>
                <td>
                    <?php echo $this->_vars['item_u']['name']; ?>

                </td>												
                <td>
                    <input size="8" type="text" name="adjustment_<?php echo $this->_vars['item_u']['id']; ?>
_<?php echo $this->_vars['item']['city_id']; ?>
" value="<?php echo $this->_vars['item_u']['adjustment']; ?>
">
                </td>
            <?php endforeach; endif; ?>
        <?php endif; ?>
    </tr>
    <?php endforeach; endif; ?>
    </table>
    <?php if ($this->_vars['factors_graph']['functionality'] != 'on'): ?>
   <div class="btn"><div class="l"><input type="submit" name="btn_save_Placefactors" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
   <?php endif; ?>
</div>
</div>
<div class="clr"></div>
<?php endif; ?>
<?php if ($this->_vars['tab_id'] == 'Commonfactors'): ?>
<div class="edit-form edit-form-marketing">
	<div class="row header"><?php echo l('calc_admin_lbl_price_presentations', 'content', '', 'text', array()); ?></div>
    <div class="row">
    	<table class="form_table2">
        	<tr>
            	<td width="230px">
               		<h4 align="center"><?php echo l('calc_admin_lbl_price_adjustment', 'content', '', 'text', array()); ?></h4>
                	<table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column"><div class="fr tr_height"><?php echo l('calc_admin_lowest_price_presented', 'content', '', 'text', array()); ?></div></td>
                            <td><input size="8" type="text" name="price_min" value="<?php echo $this->_vars['factors']['price_min']; ?>
"></td>
                        </tr>
                        <tr>
                            <td class="left_column"><div class="fr tr_height"><?php echo l('calc_admin_highest_price_presented', 'content', '', 'text', array()); ?></div></td>
                            <td><input size="8" type="text" name="price_max" value="<?php echo $this->_vars['factors']['price_max']; ?>
"></td>
                        </tr>
                    </table>
                </td>
                <td width="230px">
                	<h4 align="center"><?php echo l('calc_admin_gliding_factor', 'content', '', 'text', array()); ?></h4>																		
                    <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column"><div class="fr tr_height"><?php echo l('calc_admin_factor', 'content', '', 'text', array()); ?></div></td>
                            <td>
                            <select class="sel-cat" name="gliding" id="area_select"  style="width:120px;">
                            <option value=""><?php echo l('calc_admin_choose_factor', 'content', '', 'text', array()); ?></option>
                                <option value="1" <?php if ($this->_vars['factors']['gliding'] == 1): ?> selected <?php endif; ?>>1</option>
                                <option value="2" <?php if ($this->_vars['factors']['gliding'] == 2): ?> selected <?php endif; ?>>2</option>
                                <option value="3" <?php if ($this->_vars['factors']['gliding'] == 3): ?> selected <?php endif; ?>>3</option>
                                <option value="4" <?php if ($this->_vars['factors']['gliding'] == 4): ?> selected <?php endif; ?>>4</option>
                                <option value="5" <?php if ($this->_vars['factors']['gliding'] == 5): ?> selected <?php endif; ?>>5</option>
                                <option value="6" <?php if ($this->_vars['factors']['gliding'] == 6): ?> selected <?php endif; ?>>6</option>
                                <option value="7" <?php if ($this->_vars['factors']['gliding'] == 7): ?> selected <?php endif; ?>>7</option>														
                            </select>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
    <div class="row header"><?php echo l('calc_admin_house_apartments', 'content', '', 'text', array()); ?></div>
    <div class="row">
    	<table class="form_table2">
        	<tr>
            	<td width="230px">
                    <h4 align="center"><?php echo l('calc_admin_Beach', 'content', '', 'text', array()); ?> (AA)</h4>																		
                    <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column"><?php echo l('calc_option_1st_line', 'content', '', 'text', array()); ?></td>
                            <td><input size="8" type="text" name="aa_1" value="<?php echo $this->_vars['factors']['aa_1']; ?>
"></td>
                        </tr>
                        <tr>
                            <td class="left_column"><?php echo l('calc_option_lessthen_15min_walk', 'content', '', 'text', array()); ?></td>
                            <td><input size="8" type="text" name="aa_2" value="<?php echo $this->_vars['factors']['aa_2']; ?>
"></td>
                        </tr>
                        <tr>
                            <td class="left_column"><?php echo l('calc_option_morethan_15minutes_walk', 'content', '', 'text', array()); ?></td>
                            <td><input size="8" type="text" name="aa_3" value="<?php echo $this->_vars['factors']['aa_3']; ?>
"></td>
                        </tr>												
                    </table>
            	</td>
                <td width="230px">
                    <h4 align="center"><?php echo l('calc_admin_View', 'content', '', 'text', array()); ?> (BB)</h4>												
                    <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column"><?php echo l('calc_option_sea', 'content', '', 'text', array()); ?></td>
                            <td><input size="8" type="text" name="bb_1" value="<?php echo $this->_vars['factors']['bb_1']; ?>
"></td>
                        </tr>
                        <tr>
                            <td class="left_column"><?php echo l('calc_option_mountain_garden', 'content', '', 'text', array()); ?></td>
                            <td><input size="8" type="text" name="bb_2" value="<?php echo $this->_vars['factors']['bb_2']; ?>
"></td>
                        </tr>
                        <tr>
                            <td class="left_column"><?php echo l('calc_option_city_street', 'content', '', 'text', array()); ?></td>
                            <td><input size="8" type="text" name="bb_3" value="<?php echo $this->_vars['factors']['bb_3']; ?>
"></td>
                        </tr>												
                    </table>										
                </td>
                <td width="230px">
                    <h4 align="center"><?php echo l('calc_lbl_access_benefits', 'content', '', 'text', array()); ?></h4>												
                    <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column"><?php echo l('calc_option_tennis', 'content', '', 'text', array()); ?> <b>(CC)</b></td>
                            <td><input size="8" type="text" name="cc" value="<?php echo $this->_vars['factors']['cc']; ?>
"></td>
                        </tr>
                        <tr>
                            <td class="left_column"><?php echo l('calc_option_football', 'content', '', 'text', array()); ?> <b>(DD)</b></td>
                            <td><input size="8" type="text" name="dd" value="<?php echo $this->_vars['factors']['dd']; ?>
"></td>
                        </tr>
                        <tr>
                            <td class="left_column"><?php echo l('calc_option_golf', 'content', '', 'text', array()); ?> <b>(EE)</b></td>
                            <td><input size="8" type="text" name="ee" value="<?php echo $this->_vars['factors']['ee']; ?>
"></td>
                        </tr>												
                    </table>										
                </td>
                <td width="230px">
                    <h4 align="center"><?php echo l('calc_lbl_walking_distance_10min', 'content', '', 'text', array()); ?></h4>																		
                    <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column"><?php echo l('calc_option_buss', 'content', '', 'text', array()); ?> <b>(FF)</b></td>
                            <td><input size="8" type="text" name="ff" value="<?php echo $this->_vars['factors']['ff']; ?>
"></td>
                        </tr>
                        <tr>
                            <td class="left_column"><?php echo l('calc_option_supermarket', 'content', '', 'text', array()); ?> <b>(GG)</b></td>
                            <td><input size="8" type="text" name="gg" value="<?php echo $this->_vars['factors']['gg']; ?>
"></td>
                        </tr>
                        <tr>
                            <td class="left_column"><?php echo l('calc_option_restaurants', 'content', '', 'text', array()); ?> <b>(HH)</b></td>
                            <td><input size="8" type="text" name="hh" value="<?php echo $this->_vars['factors']['hh']; ?>
"></td>
                        </tr>												
                    </table>
                </td>
            <tr>
            <tr>
            	<td width="230px">
                    <h4 align="center"><?php echo l('calc_admin_bathroom_factor', 'content', '', 'text', array()); ?> (II)</h4>												
                    <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                        <tr height="5px"></tr>
                        <tr>
                            <td class="left_column">(<?php echo l('calc_admin_Bath', 'content', '', 'text', array()); ?>+<?php echo l('calc_admin_wc', 'content', '', 'text', array()); ?>)/<?php echo l('calc_admin_bed', 'content', '', 'text', array()); ?> < 0.49</td>
                            <td><input size="8" type="text" name="ii_1" value="<?php echo $this->_vars['factors']['ii_1']; ?>
"></td>
                        </tr>
                        <tr>
                            <td class="left_column">(<?php echo l('calc_admin_Bath', 'content', '', 'text', array()); ?>+<?php echo l('calc_admin_wc', 'content', '', 'text', array()); ?>)/<?php echo l('calc_admin_bed', 'content', '', 'text', array()); ?>)0.49-0.69</td>
                            <td><input size="8" type="text" name="ii_2" value="<?php echo $this->_vars['factors']['ii_2']; ?>
"></td>
                        </tr>
                        <tr>
                            <td class="left_column">(<?php echo l('calc_admin_Bath', 'content', '', 'text', array()); ?>+<?php echo l('calc_admin_wc', 'content', '', 'text', array()); ?>)/<?php echo l('calc_admin_bed', 'content', '', 'text', array()); ?> > 0.69</td>
                            <td><input size="8" type="text" name="ii_3" value="<?php echo $this->_vars['factors']['ii_3']; ?>
"></td>
                        </tr>												
                    </table>										
                </td>
            </tr>
        </table>
    </div>
    <div class="row header"><?php echo l('calc_admin_apartment', 'content', '', 'text', array()); ?></div>
    <div class="row">
    	<table class="form_table2">
        <tr>
        	<td width="230px">
                <h4 align="center"><?php echo l('calc_admin_type', 'content', '', 'text', array()); ?> (BA)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_ground_floor', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="ba_1" value="<?php echo $this->_vars['factors']['ba_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_low_floor', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="ba_2" value="<?php echo $this->_vars['factors']['ba_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_middle', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="ba_3" value="<?php echo $this->_vars['factors']['ba_3']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_upper', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="ba_4" value="<?php echo $this->_vars['factors']['ba_4']; ?>
"></td>
                    </tr>												
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_duplex', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="ba_5" value="<?php echo $this->_vars['factors']['ba_5']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_penthouse', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="ba_6" value="<?php echo $this->_vars['factors']['ba_6']; ?>
"></td>
                    </tr>													
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_lbl_elevator', 'content', '', 'text', array()); ?> (BB)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="bb" value="<?php echo $this->_vars['factors']['bb']; ?>
"></td>
                    </tr>
                </table>										
            </td>
            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_admin_terrasse', 'content', '', 'text', array()); ?> (BC)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="bc_1" value="<?php echo $this->_vars['factors']['bc_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="bc_2" value="<?php echo $this->_vars['factors']['bc_2']; ?>
"></td>
                    </tr>
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_admin_terrasse', 'content', '', 'text', array()); ?> - <?php echo l('calc_admin_barbeque', 'content', '', 'text', array()); ?> (BD)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="bd_1" value="<?php echo $this->_vars['factors']['bd_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="bd_2" value="<?php echo $this->_vars['factors']['bd_2']; ?>
"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        	<td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_admin_other_terrasse', 'content', '', 'text', array()); ?></h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_admin_solarium', 'content', '', 'text', array()); ?> <b>(BE)</b></td>
                        <td><input size="8" type="text" name="be" value="<?php echo $this->_vars['factors']['be']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_admin_south_faced', 'content', '', 'text', array()); ?> <b>(BF)</b></td>
                        <td><input size="8" type="text" name="bf" value="<?php echo $this->_vars['factors']['bf']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_admin_south_west_faced', 'content', '', 'text', array()); ?> <b>(BG)</b></td>
                        <td><input size="8" type="text" name="bg" value="<?php echo $this->_vars['factors']['bg']; ?>
"></td>
                    </tr>												
                </table>										
            </td>

            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_lbl_garage', 'content', '', 'text', array()); ?> (BH)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="bh_1" value="<?php echo $this->_vars['factors']['bh_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="bh_2" value="<?php echo $this->_vars['factors']['bh_2']; ?>
"></td>
                    </tr>
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_lbl_swimmingpool', 'content', '', 'text', array()); ?> (BI)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_common', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="bi_1" value="<?php echo $this->_vars['factors']['bi_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_private', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="bi_2" value="<?php echo $this->_vars['factors']['bi_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_none', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="bi_3" value="<?php echo $this->_vars['factors']['bi_3']; ?>
"></td>
                    </tr>												
                </table>										
            </td>
            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_lbl_quality_standard', 'content', '', 'text', array()); ?> (BJ)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_low', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="bj_1" value="<?php echo $this->_vars['factors']['bj_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_medium', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="bj_2" value="<?php echo $this->_vars['factors']['bj_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_high', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="bj_3" value="<?php echo $this->_vars['factors']['bj_3']; ?>
"></td>
                    </tr>												
                </table>										
            </td>
        </tr>
        <tr>
        	<td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_admin_years_since_finished', 'content', '', 'text', array()); ?> (BK)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">> 20</td>
                        <td><input size="8" type="text" name="bk_1" value="<?php echo $this->_vars['factors']['bk_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">20 - 10</td>
                        <td><input size="8" type="text" name="bk_2" value="<?php echo $this->_vars['factors']['bk_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">9 - 5</td>
                        <td><input size="8" type="text" name="bk_3" value="<?php echo $this->_vars['factors']['bk_3']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">< 5</td>
                        <td><input size="8" type="text" name="bk_4" value="<?php echo $this->_vars['factors']['bk_4']; ?>
"></td>
                    </tr>                                                
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_admin_years_since_renovated', 'content', '', 'text', array()); ?> (BL)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">> 20</td>
                        <td><input size="8" type="text" name="bl_1" value="<?php echo $this->_vars['factors']['bl_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">20 - 10</td>
                        <td><input size="8" type="text" name="bl_2" value="<?php echo $this->_vars['factors']['bl_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">9 - 5</td>
                        <td><input size="8" type="text" name="bl_3" value="<?php echo $this->_vars['factors']['bl_3']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">< 5</td>
                        <td><input size="8" type="text" name="bl_4" value="<?php echo $this->_vars['factors']['bl_4']; ?>
"></td>
                    </tr>		                                                
                </table>										
            </td>
            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_admin_turistico_type', 'content', '', 'text', array()); ?> (BM)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="bm_1" value="<?php echo $this->_vars['factors']['bm_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="bm_2" value="<?php echo $this->_vars['factors']['bm_2']; ?>
"></td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
     </div>
     <div class="row header"><?php echo l('calc_admin_house', 'content', '', 'text', array()); ?></div>
     <div class="row">
    	<table class="form_table2">
        <tr>
        	<td width="230px">
                <h4 align="center"><?php echo l('calc_lbl_plot', 'content', '', 'text', array()); ?> (A)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">< 500 sqm</td>
                        <td><input size="8" type="text" name="a_1" value="<?php echo $this->_vars['factors']['a_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">500 - 1500 sqm</td>
                        <td><input size="8" type="text" name="a_2" value="<?php echo $this->_vars['factors']['a_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">> 1500 sqm</td>
                        <td><input size="8" type="text" name="a_3" value="<?php echo $this->_vars['factors']['a_3']; ?>
"></td>
                    </tr>												
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_lbl__self_owned_plot', 'content', '', 'text', array()); ?> (B)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="b_2" value="<?php echo $this->_vars['factors']['b_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="b_1" value="<?php echo $this->_vars['factors']['b_1']; ?>
"></td>
                    </tr>
                </table>
            </td>
            <td width="230px">
                <h4 align="center"><?php echo l('calc_admin_type', 'content', '', 'text', array()); ?> (C)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_villa', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="c_1" value="<?php echo $this->_vars['factors']['c_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_Semidetached_villa', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="c_2" value="<?php echo $this->_vars['factors']['c_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_finca', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="c_3" value="<?php echo $this->_vars['factors']['c_3']; ?>
"></td>
                    </tr>												
                </table>										
            </td>
            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_admin_floors', 'content', '', 'text', array()); ?> (D)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">> 1 fl</td>
                        <td><input size="8" type="text" name="d_1" value="<?php echo $this->_vars['factors']['d_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_admin_allon_one_floor', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="d_2" value="<?php echo $this->_vars['factors']['d_2']; ?>
"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        	<td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_lbl_garden', 'content', '', 'text', array()); ?> (E)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="e_1" value="<?php echo $this->_vars['factors']['e_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="e_2" value="<?php echo $this->_vars['factors']['e_2']; ?>
"></td>
                    </tr>
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_lbl_garden', 'content', '', 'text', array()); ?> - <?php echo l('calc_admin_barbeque', 'content', '', 'text', array()); ?> (F)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">Yes</td>
                        <td><input size="8" type="text" name="f_1" value="<?php echo $this->_vars['factors']['f_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">No</td>
                        <td><input size="8" type="text" name="f_2" value="<?php echo $this->_vars['factors']['f_2']; ?>
"></td>
                    </tr>
                </table>
            </td>
            <td width="230px">
                <h4 align="center"><?php echo l('calc_lbl_garage', 'content', '', 'text', array()); ?> (G)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_car_port', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="g_1" value="<?php echo $this->_vars['factors']['g_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_garage', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="g_2" value="<?php echo $this->_vars['factors']['g_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_no_car', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="g_3" value="<?php echo $this->_vars['factors']['g_3']; ?>
"></td>
                    </tr>                                                
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_lbl_swimmingpool', 'content', '', 'text', array()); ?> (H)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_common', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="h_1" value="<?php echo $this->_vars['factors']['h_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_private', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="h_2" value="<?php echo $this->_vars['factors']['h_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_none', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="h_3" value="<?php echo $this->_vars['factors']['h_3']; ?>
"></td>
                    </tr>												
                </table>										
            </td>
        </tr>
        <tr>
        	<td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_lbl_quality_standard', 'content', '', 'text', array()); ?> (I)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_low', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="i_1" value="<?php echo $this->_vars['factors']['i_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_medium', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="i_2" value="<?php echo $this->_vars['factors']['i_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column"><?php echo l('calc_option_high', 'content', '', 'text', array()); ?></td>
                        <td><input size="8" type="text" name="i_3" value="<?php echo $this->_vars['factors']['i_3']; ?>
"></td>
                    </tr>												
                </table>										
            </td>
            <td width="230px" valign="top">                                        
                <h4 align="center"><?php echo l('calc_admin_years_since_finished', 'content', '', 'text', array()); ?> (J)</h4>																		
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">> 20</td>
                        <td><input size="8" type="text" name="j_1" value="<?php echo $this->_vars['factors']['j_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">20 - 10</td>
                        <td><input size="8" type="text" name="j_2" value="<?php echo $this->_vars['factors']['j_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">9 - 5</td>
                        <td><input size="8" type="text" name="j_3" value="<?php echo $this->_vars['factors']['j_3']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">< 5</td>
                        <td><input size="8" type="text" name="j_4" value="<?php echo $this->_vars['factors']['j_4']; ?>
"></td>
                    </tr>                                                
                </table>
            </td>
            <td width="230px" valign="top">
                <h4 align="center"><?php echo l('calc_admin_years_since_renovated', 'content', '', 'text', array()); ?> (K)</h4>												
                <table cellpadding="0" cellspacing="0" class="form_table" border="0" align="left">
                    <tr height="5px"></tr>
                    <tr>
                        <td class="left_column">> 20</td>
                        <td><input size="8" type="text" name="k_1" value="<?php echo $this->_vars['factors']['k_1']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">20 - 10</td>
                        <td><input size="8" type="text" name="k_2" value="<?php echo $this->_vars['factors']['k_2']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">9 - 5</td>
                        <td><input size="8" type="text" name="k_3" value="<?php echo $this->_vars['factors']['k_3']; ?>
"></td>
                    </tr>
                    <tr>
                        <td class="left_column">< 5</td>
                        <td><input size="8" type="text" name="k_4" value="<?php echo $this->_vars['factors']['k_4']; ?>
"></td>
                    </tr>		                                                
                </table>										
            </td>
        </tr>
        </table>
     </div>
     <div class="row">
     <div class="btn"><div class="l"><input type="submit" name="btn_save_Commonfactors" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div>
     </div>
 </div>
 </div>
 <div class="clr"></div>
<?php endif; ?>
<?php if ($this->_vars['tab_id'] == 'Excelimport'): ?>
<div class="edit-form edit-form-marketing">
<div class="row header"><?php echo l('calc_admin_import_excel', 'content', '', 'text', array()); ?></div>
<div class="row zebra">
<table style="margin-left:15px;">
	<tr>
    	<td width="100px"><?php echo l('calc_admin_import_file', 'content', '', 'text', array()); ?></td>
        <td>
        	<div class="fileinputs" style="display: inline;">
        	<input type="file" name="db_file" id="db_file" style="cursor: pointer;" class="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" onchange="document.getElementById('file_text_db_file').innerHTML = this.value.replace(/.*\\(.*)/, '$1');document.getElementById('file_img_db_file').innerHTML = ChangeIcon(this.value.replace(/.*\.(.*)/, '$1'));"/>
            <div class="fakefile" style="cursor: pointer">
                <table cellpadding="0" cellspacing="0">
                <tr>
                	<td>	
                        <a class="admin_button_major" style="cursor: pointer"><span>&nbsp;</span><?php echo l('calc_admin_choose', 'content', '', 'text', array()); ?></a>
                    </td>												
                    <td style="padding-left:10px;">
                        <span id='file_img_db_file'></span>
                    </td>								
                    <td style="padding-left:4px;">
                        <span id='file_text_db_file'></span>	
                    </td>
                </tr>
                </table>
            </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="2">
        	<!--<div class="btn"><div class="l"><a style="cursor:pointer;" >Load</a></div></div>-->
            <div class="btn"><div class="l"><input type="submit" name="btn_load_excel" id="btn_load_excel" value="<?php echo l('calc_admin_load', 'content', '', 'text', array()); ?>" /></div></div>
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2"><div id="loading_div" align="justify"><?php echo $this->_vars['ExcelCityValidation']; ?>
</div></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
</table>
</div>
<?php endif; ?>
</form>
<div class="clr"></div>
<?php echo '
<script>

/*$(\'form#save_form #btn_load_excel\').click(function(event) { // <- goes here !
event.preventDefault();
    /*if ( parseInt($j("#zip_field").val(), 10) > 1000){
        
        $j(\'form#userForm .button\').attr(\'onclick\',\'\').unbind(\'click\');
        alert(\'Sorry we leveren alleen inomstreken hijen!\');
    }  
});*/
function importExcelData(){	
	$.post(
		\'';  echo $this->_vars['site_root']; ?>
admin/content/ajax_importExcelDataPrice<?php echo '\', 
		{},
		function(data){
			$(\'#import_status\').html(data);
		}
	);
}

function ChangeIcon(type){	
    switch (type.toLowerCase())
    {
        case \'bmp\':         
        case \'jpg\':  
        case \'png\':
        case \'gif\':
        case \'tiff\':
        type = type.toLowerCase();
        break;
        case \'jpeg\':
        	type = \'jpg\';
        	break;
        case \'tif\':
        	type = \'tiff\';
        	break;
        case \'mp3\':
        case \'wav\':
        case \'ogg\':
        	type = \'mp3\';
         break;
        case \'avi\':
        case \'wmv\':
        case \'flv\':
        	type = \'avi\';
        	 break;
       	case \'csv\':
        	type = \'csv\';
        	 break;
        case \'zip\':
        	type = \'zip\';
        	 break; 
        default: type = \'other\'; break;
    };   
    return "<img src=\'';  echo $this->_vars['site_root'];  echo $this->_vars['img_folder'];  echo '" + type +".png\'>";
}
function checkCities(fn, result_id) {		
    destination = document.getElementById(result_id);
	//destination.innerHTML = \'<img height="16px" style="vertical-align:middle;" src="';  echo $this->_vars['site_root'];  echo $this->_vars['img_folder'];  echo 'indicator.gif">\';
	 //var file = value.files[1];
   	//alert(file.tmp_name);
	/*$.ajax({
		url: \'';  echo $this->_vars['site_root'];  echo 'admin/content/ajax_get_checkCitylist\',
		dataType: \'json\',
		type: \'POST\',
		data: {datas : \'1\'},
		cache: false,
		success: function(data){
			if(data){
				alert(data);
			}else{
				alert(\'\');
			}
		}
	});*/
	//alert(fn.files[0].tmp_name)
	
}

</script>
<style>
div.fileinputs {
	position: relative;
}
div.fakefile {
	position: absolute;
	top: 0;
	left: 0;
	z-index: 1;

}
input.file {
	position: relative;
	text-align: left;
	-moz-opacity:0 ;
	filter:alpha(opacity: 0);
	opacity: 0;
	z-index: 2;
	margin-left: -115px;
	margin-top: 10px;
	cursor: pointer;
}
.admin_button_major {
    font: 13px Trebuchet MS, Arial;
	text-decoration: none;
	color: #ffffff !important;
	position: relative;
	background: url(\'';  echo $this->_vars['site_root'];  echo $this->_vars['img_folder'];  echo 'btn_green.png\') 0 -31px no-repeat;
	padding: 7px 17px 7px 15px;
	height: 17px;
	margin: -6px 10px 5px 0;
	/*display:inline-block;*/
}
</style>
'; ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

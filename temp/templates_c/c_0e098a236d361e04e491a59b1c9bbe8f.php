<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.sorter.php'); $this->register_function("sorter", "tpl_function_sorter");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-02-08 11:26:31 India Standard Time */ ?>

	<?php if ($this->_vars['requests']): ?>
	<div class="sorter line" id="sorter_block">
		<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
		<div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div>
	</div>
	<?php endif; ?>
	
	<div>	
		<?php if (is_array($this->_vars['requests']) and count((array)$this->_vars['requests'])): foreach ((array)$this->_vars['requests'] as $this->_vars['item']): ?>
		<div class="listing-block">
			<div id="item-block-<?php echo $this->_vars['item']['id']; ?>
" class="item listing">
				<div class="image">
					<?php 
$this->assign('logo_title', l('link_listing_view', 'listings', '', 'button', array('id_ref'=>$this->_vars['item']['id'])));
 ?>
					<?php 
$this->assign('text_listing_logo', l('text_listing_logo', 'listings', '', 'button', array('id_ref'=>$this->_vars['item']['id'],'property_type'=>$this->_vars['item']['property_type_str'],'operation_type'=>$this->_vars['item']['operation_type_str'],'location'=>$this->_vars['item']['location'])));
 ?>
					<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']['listing']), $this);?>">
						<img src="<?php echo $this->_vars['item']['listing']['media']['photo']['thumbs']['small']; ?>
" alt="<?php echo $this->_vars['text_listing_logo']; ?>
" title="<?php echo $this->_vars['logo_title']; ?>
">
					</a>
				</div>
				<div class="body">
					<h3><a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']['listing']), $this);?>" title="<?php echo $this->_run_modifier($this->_vars['item']['listing']['output_name'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['item']['listing']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a></h3>
					<div class="t-1">		
													<?php switch($this->_vars['item']['listing']['price_period']): case '1':  ?>
								<?php echo $this->_run_modifier($this->_vars['item']['date_start'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
 - <?php echo $this->_run_modifier($this->_vars['item']['date_end'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>

							<?php break; case '2':  ?>
								<?php echo $this->_run_modifier($this->_vars['item']['date_start'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>

								&mdash; 
								<?php echo $this->_run_modifier($this->_vars['item']['date_end'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>

						<?php break; endswitch; ?><br>
						<?php if ($this->_vars['item']['comment']): ?><span title="<?php echo $this->_run_modifier($this->_vars['item']['comment'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['item']['comment'], 'truncate', 'plugin', 1, 70); ?>
</span><br><?php endif; ?>
						<?php if ($this->_vars['item']['answer']): ?><span class="order_answer" title="<?php echo $this->_run_modifier($this->_vars['item']['answer'], 'escape', 'plugin', 1); ?>
"><?php echo l('text_order_answer', 'listings', '', 'text', array()); ?> <?php echo $this->_run_modifier($this->_vars['item']['answer'], 'truncate', 'plugin', 1, 70); ?>
</span><br><?php endif; ?>
					</div>
					<div class="t-2">
						<span><?php echo $this->_run_modifier($this->_vars['item']['date_modified'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_time_format']); ?>
</span>
					</div>
				</div>
				<div class="clr"></div>
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/request_delete/<?php echo $this->_vars['item']['id']; ?>
" data-id="<?php echo $this->_vars['item']['id']; ?>
" data-status="<?php echo $this->_vars['item']['status']; ?>
" class="btn-link request_delete" title="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>" onclick="javascript: if(!confirm('<?php echo l('note_request_delete', 'listings', '', 'js', array()); ?>')) return false;"><ins class="fa fa-trash-o fa-lg edge hover"></ins></a><span class="btn-text link-r-margin"><?php echo l('btn_delete', 'start', '', 'text', array()); ?></span>
				<div class="clr"></div>
			</div>
		</div>
		
		<?php endforeach; else: ?>
		<div class="item empty"><?php echo l('no_requests', 'listings', '', 'text', array()); ?></div>
		<?php endif; ?>
		
	</div>
	
	<?php if ($this->_vars['requests']): ?><div id="pages_block_2"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?></div><?php endif; ?>

<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.country_select.php'); $this->register_function("country_select", "tpl_function_country_select");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-30 09:32:48 India Daylight Time */ ?>

<form action="" method="post" enctype="multipart/form-data">
<div class="edit-form n150">
	<div class="row header"><?php if ($this->_vars['data']['id']):  echo l('admin_header_users_change', 'users', '', 'text', array());  else:  echo l('admin_header_users_add', 'users', '', 'text', array());  endif; ?></div>
	<div class="row">
		<div class="h"><?php echo l('field_user_type', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><b><?php echo $this->_vars['data']['user_type_str']; ?>
</b></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_email', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[email]" value="<?php echo $this->_run_modifier($this->_vars['data']['email'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<?php if ($this->_vars['data']['id']): ?>
	<div class="row">
		<div class="h"><?php echo l('field_change_password', 'users', '', 'text', array()); ?>:<?php if (! $this->_vars['data']['id']): ?>&nbsp;*<?php endif; ?> </div>
		<div class="v"><input type="checkbox" value="1" name="update_password" id="pass_change_field"></div>
	</div>
	<?php endif; ?>
	<div class="row">
		<div class="h"><?php echo l('field_password', 'users', '', 'text', array()); ?>:<?php if (! $this->_vars['data']['id']): ?>&nbsp;*<?php endif; ?> </div>
		<div class="v"><input type="password" name="data[password]" id="pass_field" <?php if ($this->_vars['data']['id']): ?>disabled<?php endif; ?>></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_repassword', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="password" name="data[repassword]" id="repass_field" <?php if ($this->_vars['data']['id']): ?>disabled<?php endif; ?>></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_fname', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[fname]" value="<?php echo $this->_run_modifier($this->_vars['data']['fname'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_sname', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[sname]" value="<?php echo $this->_run_modifier($this->_vars['data']['sname'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[phone]" value="<?php echo $this->_run_modifier($this->_vars['data']['phone'], 'escape', 'plugin', 1); ?>
" class="phone-field"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_company', 'users', '', 'text', array()); ?>:&nbsp;* </div>
		<div class="v"><input type="text" name="data[company_name]" value="<?php echo $this->_run_modifier($this->_vars['data']['company_name'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_icon', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<input type="file" name="user_icon">
			<?php if ($this->_vars['data']['user_logo'] || $this->_vars['data']['user_logo_moderation']): ?>
			<br><input type="checkbox" name="user_icon_delete" value="1" id="uichb"><label for="uichb"><?php echo l('field_icon_delete', 'users', '', 'text', array()); ?></label><br>
			<?php if ($this->_vars['data']['user_logo_moderation']): ?><img src="<?php echo $this->_vars['data']['media']['user_logo_moderation']['thumbs']['middle']; ?>
"><?php else: ?><img src="<?php echo $this->_vars['data']['media']['user_logo']['thumbs']['middle']; ?>
"><?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
    <div class="row">
		<div class="h"><?php echo l('field_office_image', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<input type="file" name="office">
			<br><input type="checkbox" name="office_delete" value="1" id="uichb"><label for="uichb"><?php echo l('field_icon_delete', 'users', '', 'text', array()); ?></label><br>
            <?php if ($this->_vars['data']['office_image'] != ''): ?>
			<img src="<?php echo $this->_vars['site_root']; ?>
uploads/photo/<?php echo $this->_vars['data']['office_image']; ?>
" height="100" width="100">
            <?php endif; ?>
		</div>
	</div>

	<div class="row">
		<div class="h"><?php echo l('field_contact_email', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[contact_email]" value="<?php echo $this->_run_modifier($this->_vars['data']['contact_email'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_contact_phone', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[contact_phone]" value="<?php echo $this->_run_modifier($this->_vars['data']['contact_phone'], 'escape', 'plugin', 1); ?>
" class="phone-field"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_contact_info', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><textarea rows="5" cols="80" name="data[contact_info]"><?php echo $this->_run_modifier($this->_vars['data']['contact_info'], 'escape', 'plugin', 1); ?>
</textarea></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_web_url', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[web_url]" value="<?php echo $this->_run_modifier($this->_vars['data']['web_url'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_twitter', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[twitter]" value="<?php echo $this->_run_modifier($this->_vars['data']['twitter'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_facebook', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[facebook]" value="<?php echo $this->_run_modifier($this->_vars['data']['facebook'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_vkontakte', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[vkontakte]" value="<?php echo $this->_run_modifier($this->_vars['data']['vkontakte'], 'escape', 'plugin', 1); ?>
"></div>
	</div>

	<div class="row">
		<div class="h"><?php echo l('field_working_days', 'users', '', 'text', array()); ?>: </div>
		<div class="v">			
			<?php if (is_array($this->_vars['weekdays']['option']) and count((array)$this->_vars['weekdays']['option'])): foreach ((array)$this->_vars['weekdays']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
			<input type="checkbox" name="data[working_days][]" value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" id="working_days_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['working_days'] && $this->_run_modifier($this->_vars['key'], 'in_array', 'PHP', 1, $this->_vars['data']['working_days'])): ?>checked="checked"<?php endif; ?> />
			<label for="working_days_<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_vars['item']; ?>
</label>
			<?php endforeach; endif; ?>
		</div>
	</div>	
	<div class="row">
		<div class="h"><?php echo l('field_working_hours', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<?php echo l('text_from', 'users', '', 'text', array()); ?>:
			<select name="data[working_hours_begin]">
				<option value="" <?php if (! $this->_vars['data']['workings_hours_begin']): ?>selected<?php endif; ?>><?php echo $this->_vars['dayhours']['header']; ?>
</option>
				<?php if (is_array($this->_vars['dayhours']['option']) and count((array)$this->_vars['dayhours']['option'])): foreach ((array)$this->_vars['dayhours']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['working_hours_begin'] == $this->_vars['key']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
				<?php endforeach; endif; ?>
			</select>
			<?php echo l('text_till', 'users', '', 'text', array()); ?>:
			<select name="data[working_hours_end]">
				<option value="" <?php if (! $this->_vars['data']['workings_hours_end']): ?>selected<?php endif; ?>><?php echo $this->_vars['dayhours']['header']; ?>
</option>
				<?php if (is_array($this->_vars['dayhours']['option']) and count((array)$this->_vars['dayhours']['option'])): foreach ((array)$this->_vars['dayhours']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['working_hours_end'] == $this->_vars['key']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
				<?php endforeach; endif; ?>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_lunch_time', 'users', '', 'text', array()); ?>: </div>
		<div class="v">
			<?php echo l('text_from', 'users', '', 'text', array()); ?>:
			<select name="data[lunch_time_begin]">
				<option value="" <?php if (! $this->_vars['data']['lunch_time_begin']): ?>selected<?php endif; ?>><?php echo $this->_vars['dayhours']['header']; ?>
</option>
				<?php if (is_array($this->_vars['dayhours']['option']) and count((array)$this->_vars['dayhours']['option'])): foreach ((array)$this->_vars['dayhours']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['lunch_time_begin'] == $this->_vars['key']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
				<?php endforeach; endif; ?>
			</select>
			<?php echo l('text_till', 'users', '', 'text', array()); ?>:
			<select name="data[lunch_time_end]">
				<option value="" <?php if (! $this->_vars['data']['lunch_time_end']): ?>selected<?php endif; ?>><?php echo $this->_vars['dayhours']['header']; ?>
</option>
				<?php if (is_array($this->_vars['dayhours']['option']) and count((array)$this->_vars['dayhours']['option'])): foreach ((array)$this->_vars['dayhours']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
				<option value="<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['data']['lunch_time_end'] == $this->_vars['key']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
				<?php endforeach; endif; ?>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_region', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><?php echo tpl_function_country_select(array('select_type' => 'city','id_country' => $this->_vars['data']['id_country'],'id_region' => $this->_vars['data']['id_region'],'id_city' => $this->_vars['data']['id_city'],'id_district' => $this->_vars['data']['id_district']), $this);?></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_address', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[address]" value="<?php echo $this->_run_modifier($this->_vars['data']['address'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<div class="row">
		<div class="h"><?php echo l('field_postal_code', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="text" name="data[postal_code]" value="<?php echo $this->_run_modifier($this->_vars['data']['postal_code'], 'escape', 'plugin', 1); ?>
"></div>
	</div>
	<?php if ($this->_vars['data']['id']): ?>
	<div class="row">
		<div class="h"><?php echo l('field_contact_map', 'users', '', 'text', array()); ?>:</div>
		<div class="v"><?php echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'object_id' => $this->_vars['data']['id'],'gid' => 'user_profile','markers' => $this->_vars['markers'],'settings' => $this->_vars['map_settings'],'width' => 578,'height' => 400,'map_id' => user_map), $this);?></div>
	</div>
	<?php endif; ?>
	<?php if ($this->_vars['use_services']): ?>
	<div class="row">
		<div class="h"><?php echo l('field_services', 'users', '', 'text', array()); ?>: <br><br><br>
			<div class="select_actions">
				<a href="javascript:void(0)" id="services_select_all"><?php echo l('select_all', 'start', '', 'text', array()); ?></a>
				<a href="javascript:void(0)" id="services_unselect_all"><?php echo l('unselect_all', 'start', '', 'text', array()); ?></a>
			</div> 
		</div>
		<div class="v">
			<?php if ($this->_vars['use_featured']): ?>
			<input type="hidden" name="services[featured]" value="0">
			<input type="checkbox" name="services[featured]" value="1" id="service_featured" class="width6" <?php if ($this->_vars['data']['is_featured']): ?>checked<?php endif; ?>>
			<label for="service_featured" class="width6"><?php echo l('field_service_featured_company', 'users', '', 'text', array()); ?></label>
			<?php endif; ?>
			<?php if ($this->_vars['use_show_logo']): ?>
			<input type="hidden" name="services[show_logo]" value="0">
			<input type="checkbox" name="services[show_logo]" value="1" id="service_show_logo" class="width6" checked>
			<label for="service_show_logo" class="width6"><?php echo l('field_service_show_logo', 'users', '', 'text', array()); ?></label>
			<?php endif; ?>
		</div>
		<script><?php echo '
			$(function(){
				$(\'#services_select_all\').bind(\'click\', function(){
					$(\'input[name^=services]\').prop(\'checked\', true);
				});
				$(\'#services_unselect_all\').bind(\'click\', function(){
					$(\'input[name^=services]\').prop(\'checked\', false);
				});
			});
		'; ?>
</script>
	</div>
	<?php endif; ?>
	<div class="row">
		<div class="h"><?php echo l('field_confirm', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><input type="checkbox" name="data[confirm]" value="1" <?php if ($this->_vars['data']['confirm']): ?>checked<?php endif; ?>></div>
	</div>
	<div class="row">
		<div class="av-rt_2 skinned-form-controls skinned-form-controls-mac">
        	<table cellpadding="0" cellspacing="0" border="0" style="width:100%;" class="">
            	<tr><td class="headline2"><?php echo l('agent_lbl_services_offered', 'content', '', 'text', array()); ?></td></tr>
                <tr>
                	<td>
                    	<table cellpadding="2" cellspacing="2" border="3" class="infotable">
                            <tr>
                                <th><?php echo l('agent_lbl_1', 'content', '', 'text', array()); ?></th>
                                <th><?php echo l('agent_lbl_2', 'content', '', 'text', array()); ?></th>
                                <th><?php echo l('agent_lbl_3', 'content', '', 'text', array()); ?></th>
                                <th><?php echo l('agent_lbl_4', 'content', '', 'text', array()); ?></th>
                                <th class="last"><?php echo l('agent_lbl_5', 'content', '', 'text', array()); ?></th>
                            </tr>
                            <tr>
                                <td><input type="radio" name="data[real_estate]" value="1" <?php if ($this->_vars['banner_de']['real_estate']): ?>checked<?php endif; ?>></td>
                                <td><input type="radio" name="data[rental_service]" value="1" <?php if ($this->_vars['banner_de']['rental_service']): ?>checked<?php endif; ?>></td>
                                <td><input type="radio" name="data[facility_service]" value="1" <?php if ($this->_vars['banner_de']['facility_service']): ?>checked<?php endif; ?>></td>
                                <td><input type="radio" name="data[insurance_service]" value="1" <?php if ($this->_vars['banner_de']['insurance_service']): ?>checked<?php endif; ?>></td>
                                <td class="last"><input type="radio" name="data[banking_service]" value="1" <?php if ($this->_vars['banner_de']['banking_service']): ?>checked<?php endif; ?>></td>				
                            </tr>				
                        </table>
                    </td>
                </tr>
                <tr><td class="headline2"><?php echo l('agent_lbl_operates_in', 'content', '', 'text', array()); ?></td></tr>
                <tr>
                	<td>
                    	<table cellpadding="2" cellspacing="2" border="3" class="infotable">
                            <tr>
                                <td>
								<textarea rows="7" cols="100" name="data[operations]"><?php echo $this->_vars['banner_de']['operations']; ?>
</textarea>
								</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td class="headline2"><?php echo l('agent_lbl_languages', 'content', '', 'text', array()); ?></td></tr>
                <tr>
                	<td>
                    	<table cellpadding="2" cellspacing="2" border="3" class="infotable">
                            <tr>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root']; ?>
application/views/pilot/img/flags/no-no.png" alt="NO" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root']; ?>
application/views/pilot/img/flags/sv-se.png" alt="SE" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root']; ?>
application/views/pilot/img/flags/fi-fi.png" alt="FI" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root']; ?>
application/views/pilot/img/flags/dk-dk.png" alt="DK" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root']; ?>
application/views/pilot/img/flags/ru-ru.png" alt="RU" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root']; ?>
application/views/pilot/img/flags/pl-pl.png" alt="PL" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root']; ?>
application/views/pilot/img/flags/nl-nl.png" alt="NL" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root']; ?>
application/views/pilot/img/flags/de-de.png" alt="DE" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root']; ?>
application/views/pilot/img/flags/fr-fr.png" alt="FR" /></th>
                                <th class="smallcell"><img src="<?php echo $this->_vars['site_root']; ?>
application/views/pilot/img/flags/en-gb2.png" alt="EN" /></th>
                                <th class="last smallcell"><img src="<?php echo $this->_vars['site_root']; ?>
application/views/pilot/img/flags/es-es.png" alt="ES" /></th>					
                            </tr>
                            <tr>
                                <td class="smallcell"><input type="radio" name="data[lang_no]" value="1" <?php if ($this->_vars['banner_de']['lang_no']): ?>checked<?php endif; ?>></td>
                                <td class="smallcell"><input type="radio" name="data[lang_se]" value="1" <?php if ($this->_vars['banner_de']['lang_se']): ?>checked<?php endif; ?>></td>
                                <td class="smallcell"><input type="radio" name="data[lang_fi]" value="1" <?php if ($this->_vars['banner_de']['lang_fi']): ?>checked<?php endif; ?>></td>
                                <td class="smallcell"><input type="radio" name="data[lang_dk]" value="1" <?php if ($this->_vars['banner_de']['lang_dk']): ?>checked<?php endif; ?>></td>
                                <td class="smallcell"><input type="radio" name="data[lang_ru]" value="1" <?php if ($this->_vars['banner_de']['lang_ru']): ?>checked<?php endif; ?>></td>
                                <td class="smallcell"><input type="radio" name="data[lang_pl]" value="1" <?php if ($this->_vars['banner_de']['lang_pl']): ?>checked<?php endif; ?>></td>
                                <td class="smallcell"><input type="radio" name="data[lang_ne]" value="1" <?php if ($this->_vars['banner_de']['lang_ne']): ?>checked<?php endif; ?>></td>
                                <td class="smallcell"><input type="radio" name="data[lang_de]" value="1" <?php if ($this->_vars['banner_de']['lang_de']): ?>checked<?php endif; ?>></td>
                                <td class="smallcell"><input type="radio" name="data[lang_fr]" value="1" <?php if ($this->_vars['banner_de']['lang_fr']): ?>checked<?php endif; ?>></td>
                                <td class="smallcell"><input type="radio" name="data[lang_en]" value="1" <?php if ($this->_vars['banner_de']['lang_en']): ?>checked<?php endif; ?>></td>
                                <td class="last smallcell"><input type="radio" name="data[lang_es]" value="1" <?php if ($this->_vars['banner_de']['lang_es']): ?>checked<?php endif; ?>></td>					
                            </tr>				
                        </table>
                    </td>
                </tr>
            </table>
        </div>
	</div>
</div>
<div class="btn"><div class="l"><input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
<a class="cancel" href="<?php echo $this->_vars['back_url']; ?>
"><?php echo l('btn_cancel', 'start', '', 'text', array()); ?></a>
<input type="hidden" name="map[view_type]" value="<?php echo $this->_vars['user_map_settings']['view_type']; ?>
" id="map_type">
<input type="hidden" name="map[zoom]" value="<?php echo $this->_vars['user_map_settings']['zoom']; ?>
" id="map_zoom">
<input type="hidden" name="data[lat]" value="<?php echo $this->_run_modifier($this->_vars['data']['lat'], 'escape', 'plugin', 1); ?>
" id="lat">
<input type="hidden" name="data[lon]" value="<?php echo $this->_run_modifier($this->_vars['data']['lon'], 'escape', 'plugin', 1); ?>
" id="lon">
</form>
<?php echo tpl_function_block(array('name' => geomap_load_geocoder,'module' => 'geomap'), $this);?>
<script><?php echo '
	function update_coordinates(country, region, city, address, postal_code){
		if(typeof(geocoder) != \'undefined\'){
			var location = geocoder.getLocationFromAddress(country, region, city, address, postal_code);
			geocoder.geocodeLocation(location, function(latitude, longitude){
				$(\'#lat\').val(latitude);
				$(\'#lon\').val(longitude);
				user_map.moveMarkers(latitude, longitude);
			});	
		}
	}
	$(function(){
		var location_change_wait = 0;
		var country_old = \'';  echo $this->_vars['data']['id_country'];  echo '\';
		var region_old = \'';  echo $this->_vars['data']['id_region'];  echo '\';
		var city_old = \'';  echo $this->_vars['data']['id_city'];  echo '\';
		var address_old = \'';  echo $this->_run_modifier('\'', 'str_replace', 'PHP', 1, '\\\'', $this->_vars['data']['address']);  echo '\';
		var postal_code_old = \'';  echo $this->_run_modifier("'", 'str_replace', 'PHP', 1, "\'", $this->_vars['data']['postal_code']);  echo '\';
		
		$(\'input[name=id_city]\').bind(\'change\', function(){
			var city = $(this).val();
			if(city == 0) return;
			location_change_wait++;
			check_address_updated();
		});
		
		$(\'input[name=data\\\\[address\\\\]], input[name=data\\\\[postal_code\\\\]]\').bind(\'keypress\', function(){
			location_change_wait++;
			setTimeout(check_address_updated, 1000);
		});
		
		function check_address_updated(){
			location_change_wait--;
			if(location_change_wait) return;
			var country = $(\'input[name=id_country]\').val();
			var region = $(\'input[name=id_region]\').val();
			var city = $(\'input[name=id_city]\').val();
			var address = $(\'input[name=data\\\\[address\\\\]]\').val();
			var postal_code = $(\'input[name=data\\\\[postal_code\\\\]]\').val();
			if(country == country_old && region == region_old && 
				city == city_old && address == address_old && postal_code == postal_code_old) return;
			country_old = country;
			region_old = region;
			city_old = city;
			address_old = address;
			postal_code_old = postal_code;
			var country_name = $(\'input[name=id_country]\').attr(\'data-name\');
			var region_name = $(\'input[name=id_region]\').attr(\'data-name\');
			var city_name = $(\'input[name=id_city]\').attr(\'data-name\');
			update_coordinates(country_name, region_name, city_name, address, postal_code);
		}
	});
'; ?>
</script>
<?php if ($this->_vars['phone_format']): ?>
<?php echo tpl_function_js(array('file' => 'jquery.maskedinput.min.js'), $this);?>
<script><?php echo '
$(function(){
	$(\'.phone-field\').mask(\'';  echo $this->_vars['phone_format'];  echo '\');
});
'; ?>
</script>
<?php endif; ?>
<script><?php echo '
	if(typeof(get_user_type_data) == \'undefined\'){
		function get_user_type_data(type){
			$(\'#map_type\').val(type);
		}
	}
	if(typeof(get_user_zoom_data) == \'undefined\'){
		function get_user_zoom_data(zoom){
			$(\'#map_zoom\').val(zoom);
		}
	}
	if(typeof(get_user_drag_data) == \'undefined\'){
		function get_user_drag_data(point_gid, lat, lon){
			$(\'#lat\').val(lat);
			$(\'#lon\').val(lon);
		}
	}

	$(function(){
		$("div.row:odd").addClass("zebra");
		$("#pass_change_field").click(function(){
			if(this.checked){
				$("#pass_field").prop(\'disabled\', false);
				$("#repass_field").prop(\'disabled\', false);
			}else{
				$("#pass_field").prop(\'disabled\', true); 
				$("#repass_field").prop(\'disabled\', true);
			}
		});
	});
'; ?>
</script>

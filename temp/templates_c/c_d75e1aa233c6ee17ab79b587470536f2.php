<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-11 13:42:21 India Daylight Time */ ?>

<?php echo tpl_function_js(array('file' => 'admin_lang_inline_editor.js','module' => 'start'), $this);?>
<script type="text/javascript"><?php echo '
var inlineEditor;
$(function(){
	inlineEditor = new langInlineEditor({
		siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
		';  if ($this->_vars['textarea']): ?>textarea: true,<?php endif;  echo '
	});
});
'; ?>
</script>

<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-30 09:36:35 India Daylight Time */ ?>

	<div class="user_info">
		<h2><?php echo l('header_provided_by', 'listings', '', 'text', array()); ?></h2>
		<?php if ($this->_vars['user']['status']): ?>
		<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'user_actions')); tpl_block_capture(array('assign' => 'user_actions'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
			<!--<?php echo tpl_function_block(array('name' => 'button_contact','module' => 'mailbox','user_id' => $this->_vars['user']['id'],'user_type' => $this->_vars['user']['user_type']), $this);?>-->
			<?php if ($this->_vars['show_all_listings']):  echo tpl_function_block(array('name' => 'user_listings_button','module' => 'listings','user' => $this->_vars['user']), $this); endif; ?>
			<?php echo tpl_function_block(array('name' => 'send_review_block','module' => 'reviews','object_id' => $this->_vars['user']['id'],'type_gid' => 'users_object','responder_id' => $this->_vars['user']['id'],'is_owner' => $this->_vars['is_user_owner']), $this);?>
            <!--<a href="<?php echo $this->_vars['site_url']; ?>
feedback" target="_blank" class="btn-link link-r-margin" title="<?php echo l('link_send_review', 'reviews', '', 'button', array()); ?>"><ins class="fa fa-edit fa-lg edge"></ins></a>-->
			<!--<?php echo tpl_function_block(array('name' => 'mark_as_spam_block','module' => 'spam','object_id' => $this->_vars['user']['id'],'type_gid' => 'users_object','is_owner' => $this->_vars['is_user_owner']), $this);?>-->
		<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		<?php if ($this->_vars['user_actions']): ?>
		<div class="actions noPrint">
			<?php echo $this->_vars['user_actions']; ?>

		</div>
		<?php endif; ?>
		<?php endif; ?>
		
		<div class="clr"></div>	
		<div class="image">
			<?php 
$this->assign('text_user_logo', l('text_user_logo', 'users', '', 'button', array('user_name'=>$this->_vars['user']['output_name'])));
 ?>
			<?php if ($this->_vars['user']['status']): ?>
			<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user']), $this);?>" title="<?php echo l('link_user_view', 'users', '', 'button', array()); ?>"><img src="<?php echo $this->_vars['user']['media']['user_logo']['thumbs']['big']; ?>
" alt="<?php echo $this->_vars['text_user_logo']; ?>
"></a>
			<?php else: ?>
			<img src="<?php echo $this->_vars['user']['media']['user_logo']['thumbs']['big']; ?>
" alt="<?php echo $this->_vars['text_user_logo']; ?>
" title="<?php echo l('field_user_logo', 'users', '', 'button', array()); ?>">
			<?php endif; ?>
		</div>
		
		<?php if ($this->_vars['user']['user_type'] == 'company'): ?><div itemscope itemtype="http://data-vocabulary.org/Organization"><?php endif; ?>
		
		<h3><?php if ($this->_vars['user']['status']): ?><a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user']), $this);?>" title="<?php echo l('link_user_view', 'users', '', 'button', array()); ?>" <?php if ($this->_vars['user']['user_type'] == 'company'): ?>itemprop="name"<?php endif; ?>><?php echo $this->_run_modifier($this->_vars['user']['output_name'], 'truncate', 'plugin', 1, 30); ?>
</a><?php else: ?><span <?php if ($this->_vars['user']['user_type'] == 'company'): ?>itemprop="name"<?php endif; ?>><?php echo $this->_run_modifier($this->_vars['user']['output_name'], 'truncate', 'plugin', 1, 30); ?>
</span><?php endif; ?>, <?php echo tpl_function_ld_option(array('i' => 'user_type','gid' => 'users','option' => $this->_vars['user']['user_type']), $this);?></h3>
		<?php if ($this->_vars['user']['status']): ?>
		<div id="user_block">
			<div class="tabs tab-size-15 noPrint">
				<ul id="user_sections">
					<li id="ui_contacts" sgid="contacts" class="active">
                    <a href="<?php echo $this->_vars['site_url']; ?>
listings/user/<?php echo $this->_vars['user']['id']; ?>
/<?php echo $this->_vars['sgid']; ?>
"><?php echo l('filter_section_info', 'listings', '', 'text', array()); ?></a></li>
					<?php if ($this->_vars['user']['user_type'] == 'company'): ?><li id="ui_map_info" sgid="map_info" class="<?php if ($this->_vars['section_gid'] == 'map_info'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/user/<?php echo $this->_vars['user']['id']; ?>
/<?php echo $this->_vars['sgid']; ?>
"><?php echo l('filter_section_map', 'listings', '', 'text', array()); ?></a></li><?php endif; ?>
					<li id="ui_contact" sgid="contact" class="<?php if ($this->_vars['section_gid'] == 'contact'): ?>active<?php endif; ?>"><a href="<?php echo $this->_vars['site_url']; ?>
listings/user/<?php echo $this->_vars['user']['id']; ?>
/<?php echo $this->_vars['sgid']; ?>
"><?php echo l('filter_section_contact', 'listings', '', 'text', array()); ?></a></li>				</ul>
			</div>
			<div id="content_ui_contacts" class="view-section print_block">
			    <?php echo tpl_function_block(array('name' => view_user_block,'module' => users,'user' => $this->_vars['user'],'template' => 'small'), $this);?>
			</div>
			<div id="content_ui_contact" class="view-section ui_contact">
             <div id="con-result"></div>
        <form id="contacts_form" name="contacts_form" onsubmit="checkRegistration()" action="" method="POST">
		<div class="r">
			<div class="f" id="capone"><?php echo l('field_contact_sender', 'contact', '', 'text', array()); ?>&nbsp;*</div>
			<div class="v">
            	<input type="text" name="sender" id="sender" class="sender" onfocus="this.removeAttribute('readonly');" readonly/></div>
		</div>
		<div class="r">
			<div class="f" id="capone"><?php echo l('field_contact_phone', 'contact', '', 'text', array()); ?>&nbsp;</div>
			<div class="v">
           	<input type="text" name="con-phone" id="con-phone" class="con-phone" onfocus="this.removeAttribute('readonly');" readonly/>
            </div>
		</div>
		<div class="r">
			<div class="f" id="capone"><?php echo l('field_contact_email', 'contact', '', 'text', array()); ?>&nbsp;*</div>
			<div class="v">	<input type="text" name="con-mail" id="con-mail" class="con-mail"  onfocus="this.removeAttribute('readonly');" readonly/></div>
		</div>
		<div class="r">
			<div class="f" id="capone"><?php echo l('field_contact_message', 'contact', '', 'text', array()); ?>&nbsp;</div>
			<div class="v"><textarea name="message" id="message" rows="5" cols="23"><?php echo $this->_run_modifier($this->_vars['message'], 'escape', 'plugin', 1); ?>
</textarea></div>
		</div>
        <div class="r">
			<div class="f" id="capone"><?php echo l('field_contact_captcha', 'contact', '', 'text', array()); ?>&nbsp;*</div>
			<div class="v captcha">
				<?php echo $this->_vars['data']['captcha_image']; ?>

				<input type="text" size="12" name="macode" id="macode" value="" maxlength="<?php echo $this->_vars['data']['captcha_word_length']; ?>
" onfocus="this.removeAttribute('readonly');" readonly>	
                <input type="hidden" name="macaptcha_word" id="macaptcha_word" value="<?php echo $this->_vars['captcha_word']; ?>
">			
			</div>
		</div>
		<div class="r">
			<input type="submit" id="con-submit" value="<?php echo l('btn_send', 'start', '', 'button', array()); ?>" />
		</div>
		<input type="hidden" id="user_id" name="user_id" value="<?php echo $this->_vars['user']['id']; ?>
" /> 
        <input type="hidden" id="listing_id" name="listing_id" value="<?php echo $this->_vars['listing']['id']; ?>
" /> 
	    </form>
            </div>
			<?php if ($this->_vars['user']['user_type'] == 'company'): ?>
			    <div id="content_ui_map_info" class="view-section ui_map_info">
				<iframe width="300" height="360" frameborder="0" style="border:0;margin-top:-100px;" src = "https://maps.google.com/maps?q=<?php echo $this->_vars['user']['lat']; ?>
,<?php echo $this->_vars['user']['lon']; ?>
&hl=es;z=5&amp;output=embed">
               </iframe>
			    </div>
			<?php endif; ?>
		</div>
		
		<?php if ($this->_vars['user']['user_type'] == 'company'): ?></div><?php endif; ?>
		
		<?php echo tpl_function_js(array('module' => users,'file' => 'users-menu.js'), $this);?>
		<?php echo tpl_function_js(array('module' => users_services,'file' => 'available_view.js'), $this);?>
        <script><?php echo '
	   function checkRegistration(){
		var user_id = $.trim($("#user_id").val());
		var sender = $.trim($("#sender").val());
		var phone = $.trim($("#con-phone").val());
		var email = $.trim($("#con-mail").val());
		var emailReg = /^([\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4})?$/;
		var capRegex = /[0-9 -()+]+$/;
		var message = $.trim($("#message").val());
	    var macode = $.trim($("#macode").val());
	    var macaptcha_word = $.trim($("#macaptcha_word").val());
		var listing_id = $.trim($("#listing_id").val());
		if(sender == \'\') {
			$("#sender").css(\'border-color\',\'red\');
			$(\'html, body\').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#sender").focus();	
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand(\'Stop\');
               }																						 
			});
			return false;
		}
		/*if(phone == \'\') {
			$("#sender").css("border-color", "");
			$("#con-phone").css(\'border-color\',\'red\');
			$(\'html, body\').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#con-phone").focus();
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand(\'Stop\');
               }																								 
			});
			return false;
		}else if(phone != \'\' && !capRegex.test(phone)) {
			$("#con-phone").css(\'border-color\',\'red\');
			$(\'html, body\').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#con-phone").focus();
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand(\'Stop\');
               }
			});
			return false;
		}*/
		if(email == \'\') {
			$("#sender").css(\'border-color\',\'\');
			$("#con-mail").css(\'border-color\',\'red\');
			$(\'html, body\').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#con-mail").focus();	
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand(\'Stop\');
               }																							 
			});
			return false;
		}else if(email != \'\' && !emailReg.test(email)) {
			$("#con-mail").css(\'border-color\',\'red\');
			$(\'html, body\').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#con-mail").focus();
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand(\'Stop\');
               }
			});
			return false;
		}
		if(macode == \'\') {
			$("#sender").css(\'border-color\',\'\');
			$("#con-mail").css(\'border-color\',\'\');
			$("#macode").css(\'border-color\',\'red\');
			$(\'html, body\').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#macode").focus();
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand(\'Stop\');
               }																								 
			});
			return false;
		}else if(macode != \'\' && !capRegex.test(macode)) {
			$("#macode").css(\'border-color\',\'red\');
			$(\'html, body\').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
				$("#macode").focus();
				try {
                 window.stop();
                 } catch (exception) {
                document.execCommand(\'Stop\');
               }
			});
			return false;
		}
	    var url = \'';  echo $this->_vars['site_root'];  echo '\' + \'contact/ajax_consend_message\';
		$.ajax({
        type: \'POST\',
        url: url,
		async: false,
        data: { user_id:user_id, sender: sender, phone:phone, email:email, message:message,macode:macode,macaptcha_word:macaptcha_word, listing_id:listing_id},
        success: function(response) {
            if(response == 1){
				$("#macode").css(\'border-color\',\'\');
				$("#con-result").html(\'<p class="success-message">Mail sent successfully.</p>\');
				$(\'html, body\').animate({scrollTop: $("#con-result").offset().top-200},3000,function(){
					$(\'.success-message\').delay(3000).fadeOut();
				});
			}else{
				$("#macode").css(\'border-color\',\'red\');
				$("#con-result").html(\'<p class="error-message">Invalid code.</p>\');
				$(\'html, body\').animate({scrollTop: $("#con-result").offset().top-200},1000,function(){
					$(\'.error-message\').delay(1000).stop();
					try {
                     window.stop();
                    } catch (exception) {
                    document.execCommand(\'Stop\');
                   }
				});
			}
        }
       });
	   }
	    '; ?>
</script>
		<script><?php echo '
			var rMenu;
			$(function(){
		     var id = $.trim($("#listinid").html());
	         document.getElementById("listing_id").value = id;
				rMenu = new usersMenu({
					siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
					idUser: \'';  echo $this->_vars['user']['id'];  echo '\',
					tabPrefix: \'ui\',
					CurrentSection: \'ui_contacts\',
					template: \'small\',
					'; ?>
available_view: new available_view(),<?php echo '
				});
			});
		'; ?>
</script>		
		<?php endif; ?>
	</div>	

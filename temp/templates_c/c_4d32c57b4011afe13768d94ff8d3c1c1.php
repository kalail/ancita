<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-30 09:36:28 India Daylight Time */ ?>

<?php 
$this->assign('no_info_str', l('no_information', 'start', '', 'text', array()));
 ?>

<?php if ($this->_vars['section_gid'] == 'overview'): ?>

	<div class="qr-code"><img alt="<?php echo l('text_qrcode', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_qrcode', 'listings', '', 'button', array()); ?>" src="<?php echo $this->_vars['data']['qr_code']; ?>
" id="qr_code_img"></div>

	

	

	<?php if ($this->_vars['data']['square']): ?>

	<div class="r">

		<div class="f"><?php echo l('field_square', 'listings', '', 'text', array()); ?>:</div>

		<div class="v"><?php echo $this->_vars['data']['square']; ?>
</div>

	</div>

	<?php endif; ?>

	<?php if ($this->_vars['data']['address']): ?>

	<div class="r">

		<div class="f"><?php echo l('field_address', 'listings', '', 'text', array()); ?>:</div>

		<div class="v"><?php echo $this->_vars['data']['address']; ?>
</div>

	</div>

	<?php endif; ?>

	

	<?php if ($this->_vars['data']['operation_type'] == 'rent'): ?>

		<?php if ($this->_vars['data']['calendar_period_min']): ?>

		<div class="r">

			<div class="f"><?php echo l('field_calendar_period_min', 'listings', '', 'text', array()); ?>:</div>

			<div class="v"><?php echo $this->_vars['data']['calendar_period_min']; ?>
 <?php echo tpl_function_ld_option(array('i' => 'price_period_unit','gid' => 'listings','option' => $this->_vars['data']['price_period']), $this);?></div>

		</div>

		<?php endif; ?>

		<?php if ($this->_vars['data']['calendar_period_max']): ?>

		<div class="r">

			<div class="f"><?php echo l('field_calendar_period_max', 'listings', '', 'text', array()); ?>:</div>

			<div class="v"><?php echo $this->_vars['data']['calendar_period_max']; ?>
 <?php echo tpl_function_ld_option(array('i' => 'price_period_unit','gid' => 'listings','option' => $this->_vars['data']['price_period']), $this);?></div>

		</div>

		<?php endif; ?>

	<?php endif; ?>

	

	<!--<div class="r">

		<div class="f"><?php echo l('field_price_auction', 'listings', '', 'text', array()); ?>:</div>

		<div class="v">

			<?php if ($this->_vars['data']['price_auction']): ?>

			<?php echo l('option_checkbox_yes', 'start', '', 'text', array()); ?>

			<?php else: ?>

			<?php echo l('option_checkbox_no', 'start', '', 'text', array()); ?>

			<?php endif; ?>

		</div>

	</div>

	<?php if ($this->_run_modifier($this->_vars['data']['date_available'], 'strtotime', 'PHP', 1) > 0): ?>

	<div class="r">

		<div class="f"><?php echo l('field_date_available', 'listings', '', 'text', array()); ?>:</div>

		<div class="v"><?php echo $this->_run_modifier($this->_vars['data']['date_available'], 'date_format', 'plugin', 1, $this->_vars['date_format']); ?>
</div>

	</div>

	<?php endif; ?>

	<?php if ($this->_run_modifier($this->_vars['data']['date_open'], 'strtotime', 'PHP', 1) > 0): ?>

	<div class="r">

		<div class="f"><?php echo l('field_date_open', 'listings', '', 'text', array()); ?>:</div> 

		<div class="v">

			<?php echo $this->_run_modifier($this->_vars['data']['date_open'], 'date_format', 'plugin', 1, $this->_vars['date_format']); ?>
 

			<?php if ($this->_vars['data']['date_open_begin']):  echo tpl_function_ld_option(array('i' => 'dayhour-names','gid' => 'start','option' => $this->_vars['data']['date_open_begin']), $this); endif; ?>

			<?php if ($this->_vars['data']['date_open_end']):  if ($this->_vars['data']['date_open_begin']): ?> - <?php endif;  echo tpl_function_ld_option(array('i' => 'dayhour-names','gid' => 'start','option' => $this->_vars['data']['date_open_end']), $this); endif; ?>

		</div>

	</div>

	<?php endif; ?>-->

	<?php if ($this->_vars['sections_data_count'] > 2): ?>

	<?php if (is_array($this->_vars['sections_data']) and count((array)$this->_vars['sections_data'])): foreach ((array)$this->_vars['sections_data'] as $this->_vars['section']): ?>

		<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'section_content')); tpl_block_capture(array('assign' => 'section_content'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>

		<?php if (is_array($this->_vars['data']['field_editor']) and count((array)$this->_vars['data']['field_editor'])): foreach ((array)$this->_vars['data']['field_editor'] as $this->_vars['field_gid'] => $this->_vars['item']): ?>

		<?php if ($this->_vars['item']['section_gid'] == $this->_vars['section']['gid'] && ( $this->_run_modifier($this->_vars['item']['value'], 'is_array', 'PHP', 1) && $this->_run_modifier($this->_vars['item']['value'], 'count', 'PHP', 1) || $this->_run_modifier($this->_vars['item']['value'], 'trim', 'PHP', 1) || $this->_run_modifier($this->_vars['item']['value_original'], 'trim', 'PHP', 1) )): ?>

		<div class="r">

        	<?php if ($this->_vars['item']['section_gid'] == 'comment_1' || $this->_vars['item']['section_gid'] == 'comment_4' || $this->_vars['item']['section_gid'] == 'comment_2' || $this->_vars['item']['section_gid'] == 'comment_5' || $this->_vars['item']['section_gid'] == 'comment_3' || $this->_vars['item']['section_gid'] == 'comment_6'): ?>

        	<?php if ($this->_run_modifier($this->_vars['data']['comments_lang'][$this->_vars['current_lang_id']], 'count', 'PHP', 1)): ?>

        

			<div class="v">

            	<?php echo $this->_vars['data']['comments_lang'][$this->_vars['current_lang_id']]; ?>


            </div>

            <?php endif; ?>

        <?php else: ?>

			

			<div class="v">

				<?php if ($this->_vars['item']['value_str']): ?>

					<?php echo $this->_vars['item']['value_str']; ?>


				<?php elseif ($this->_vars['item']['value']): ?>

					<?php echo $this->_vars['item']['value']; ?>


				<?php else: ?>

					<?php echo $this->_vars['item']['value_original']; ?>


				<?php endif; ?>

				<?php if ($this->_vars['field_gid'] == 'live_square_1' || $this->_vars['field_gid'] == 'live_square_4'):  echo $this->_vars['data']['square_unit_str'];  endif; ?>

			</div>

            <?php endif; ?>

		</div>

		<?php endif; ?>

		<?php endforeach; endif; ?>

		<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>

		<?php if ($this->_vars['section_content']): ?><h2><?php echo $this->_vars['section']['name']; ?>
</h2><?php echo $this->_vars['section_content'];  endif; ?>

	<?php endforeach; endif; ?>

	<?php else: ?>

	<?php if (is_array($this->_vars['data']['field_editor']) and count((array)$this->_vars['data']['field_editor'])): foreach ((array)$this->_vars['data']['field_editor'] as $this->_vars['field_gid'] => $this->_vars['item']): ?>

	<?php if (( $this->_run_modifier($this->_vars['item']['value'], 'is_array', 'PHP', 1) && $this->_run_modifier($this->_vars['item']['value'], 'count', 'PHP', 1) || $this->_run_modifier($this->_vars['item']['value'], 'trim', 'PHP', 1) || $this->_run_modifier($this->_vars['item']['value_original'], 'trim', 'PHP', 1) ) || $this->_vars['item']['section_gid'] == 'comment_1' || $this->_vars['item']['section_gid'] == 'comment_4' || $this->_vars['item']['section_gid'] == 'comment_2' || $this->_vars['item']['section_gid'] == 'comment_5' || $this->_vars['item']['section_gid'] == 'comment_3' || $this->_vars['item']['section_gid'] == 'comment_6'): ?>

	<div class="r">

    	<?php if ($this->_vars['item']['section_gid'] == 'comment_1' || $this->_vars['item']['section_gid'] == 'comment_4' || $this->_vars['item']['section_gid'] == 'comment_2' || $this->_vars['item']['section_gid'] == 'comment_5' || $this->_vars['item']['section_gid'] == 'comment_3' || $this->_vars['item']['section_gid'] == 'comment_6'): ?>

        	<?php if ($this->_run_modifier($this->_vars['data']['comments_lang'][1], 'count', 'PHP', 1) || $this->_run_modifier($this->_vars['data']['comments_lang'][9], 'count', 'PHP', 1) || $this->_run_modifier($this->_vars['data']['comments_lang'][10], 'count', 'PHP', 1)): ?>
            
            <?php if ($this->_vars['data']['comments_lang'][1] != ''): ?>

        	<div class="f"><?php echo l('header_comment_info', 'listings', '', 'text', array()); ?>:</div>
            
            <?php endif; ?>

			<div class="v">

				<?php if ($this->_vars['data']['comments_lang'][$this->_vars['current_lang_id']] != ''): ?>

                <?php echo $this->_vars['data']['comments_lang'][$this->_vars['current_lang_id']]; ?>

                
                <?php else: ?>
                
                <?php echo $this->_vars['data']['comments_lang'][1]; ?>


                <?php endif; ?>

            </div>

            <?php endif; ?>

        <?php else: ?>

        <?php if ($this->_vars['item']['value'] != 'Do not Publish' && $this->_vars['item']['value'] != 'Partly' && $this->_vars['item']['value'] != 'No'): ?>

		<div class="f"><?php echo $this->_vars['item']['name']; ?>
: </div>

        <?php endif; ?>

        <?php if ($this->_vars['item']['value'] != 'Do not Publish' && $this->_vars['item']['value'] != 'Partly' && $this->_vars['item']['value'] != 'No'): ?>

		<div class="v">

			<?php if ($this->_vars['item']['value_str']): ?>

				<?php echo $this->_vars['item']['value_str']; ?>


			<?php elseif ($this->_vars['item']['value']): ?>

				<?php echo $this->_vars['item']['value']; ?>


			<?php else: ?>

				<?php echo $this->_vars['item']['value_original']; ?>


			<?php endif; ?>

			<?php if ($this->_vars['field_gid'] == 'live_square_1' || $this->_vars['field_gid'] == 'live_square_4'):  endif; ?>

		</div>

        <?php endif; ?>

        <?php endif; ?>

	</div>

	<?php endif; ?>

	<?php endforeach; endif; ?>	

	<?php endif; ?>

<?php endif; ?>

<?php if ($this->_vars['section_gid'] == 'gallery'): ?>

	<?php 
$this->assign('text_listing_photo', l('text_listing_photo', 'listings', '', 'text', array('id_ref'=>$this->_vars['data']['listing']['id'],'property_type'=>$this->_vars['data']['property_type_str'],'operation_type'=>$this->_vars['data']['operation_type_str'],'location'=>$this->_vars['data']['location'])));
 ?>

	<?php 
$this->assign('text_listing_photo_number', l('text_listing_photo_number', 'listings', '', 'text', array('id_ref'=>$this->_vars['data']['listing']['id'],'property_type'=>$this->_vars['data']['property_type_str'],'operation_type'=>$this->_vars['data']['operation_type_str'],'location'=>$this->_vars['data']['location'])));
 ?>

	<div class="photo-view">

		<?php if ($this->_vars['data']['photo_count'] > 1): ?>

		<?php echo tpl_function_js(array('file' => 'dualslider/jquery.dualSlider.0.3.min.js'), $this);?>

		<?php echo tpl_function_js(array('file' => 'dualslider/jquery.timers-1.2.js'), $this);?>

		<div class="slider">

			<div class="carousel" title="<?php echo $this->_run_modifier($this->_vars['text_listing_photo'], 'escape', 'plugin', 1); ?>
">	

				<div class="backgrounds">

					<?php if (is_array($this->_vars['data']['photos']) and count((array)$this->_vars['data']['photos'])): foreach ((array)$this->_vars['data']['photos'] as $this->_vars['key'] => $this->_vars['item']): ?>

						<?php echo tpl_function_math(array('equation' => "x+1",'x' => $this->_vars['key'],'assign' => 'photo_number'), $this);?>

						<!--<div class="item item_<?php echo $this->_vars['key']; ?>
" title="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['text_listing_photo_number'], 'replace', 'plugin', 1, '[number]', $this->_vars['photo_number']), 'escape', 'plugin', 1); ?>
" style="background: url('<?php echo $this->_vars['item']['media']['thumbs']['620_400']; ?>
') no-repeat;"></div>-->

                        <div class="item item_<?php echo $this->_vars['key']; ?>
" style="background: url('<?php echo $this->_vars['item']['media']['thumbs']['620_400']; ?>
') no-repeat;"></div>

					<?php endforeach; endif; ?>

				</div>

				

				<div class="paging_wrapper">

					<div class="paging_wrapper2">

						<div class="paging">

							<a id="previous_item" class="previous hide" alt="<?php echo l('text_nav_prev_photo', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_nav_prev_photo', 'listings', '', 'button', array()); ?>"><?php echo l('text_nav_prev_photo', 'listings', '', 'text', array()); ?></a>

							<a id="next_item" class="next hide" alt="<?php echo l('text_nav_next_photo', 'listings', '', 'button', array()); ?>" title="<?php echo l('link_nav_next_photo', 'listings', '', 'button', array()); ?>"><?php echo l('text_nav_next_photo', 'listings', '', 'text', array()); ?></a>

							<span id="numbers" class="hide"><a href="#" rel=""></a></span>

						</div>

					</div>

				</div>	

				

				<div class="panel">

					<div class="details_wrapper">

						<div class="details">

							<?php if (is_array($this->_vars['data']['photos']) and count((array)$this->_vars['data']['photos'])): foreach ((array)$this->_vars['data']['photos'] as $this->_vars['key'] => $this->_vars['item']): ?>

							<div class="detail">

								<?php if ($this->_vars['item']['comment']): ?>

								<div class="photo-comment hide">

									<div class="comment-panel"><?php echo $this->_run_modifier($this->_vars['item']['comment'], 'truncate', 'plugin', 1, 255); ?>
</div>

									<div class="background"></div>

								</div>

								<?php endif; ?>

							</div>

							<?php endforeach; endif; ?>

						</div>

					</div>

					

				</div>

			</div>

		</div>

		<script><?php echo '

			$(function(){

				$(".slider .carousel").dualSlider({

					auto: ';  if ($this->_vars['page_data']['slider_auto']): ?>true<?php else: ?>false<?php endif;  echo ',

					autoDelay: ';  if ($this->_vars['page_data']['slider_auto']):  echo $this->_vars['page_data']['rotation'];  else: ?>false<?php endif;  echo ',

					easingCarousel: "swing",

					easingDetails: "swing",

					durationCarousel: 700,

					durationDetails: 300,

					widthsliderimage: $(".slider.carousel .backgrounds .item").width(),

					';  if ($this->_vars['_LANG']['rtl'] == 'rtl'): ?>rtl: true,<?php endif;  echo '

				});

				$(\'.slider .backgrounds .item\').bind(\'click\', function(){

					var next = $(\'#next_item\');

					if(next.css(\'display\') != \'none\') next.trigger(\'click\');

				});

				$(\'#content_m_gallery .slider .carousel .photo-comment\').show();

			});

		'; ?>
</script>

		<?php elseif ($this->_vars['data']['photo_count'] == 1): ?>

		<img src="<?php echo $this->_vars['data']['photos']['0']['media']['thumbs']['620_400']; ?>
" alt="<?php echo l('text_listing_photo', 'listings', '', 'text', array()); ?>" title="<?php echo l('text_listing_photo', 'listings', '', 'text', array()); ?>">

		<?php else: ?>

		<img src="<?php echo $this->_vars['data']['photo_default']['media']['thumbs']['620_400']; ?>
" alt="<?php echo l('text_no_photo', 'listings', '', 'button', array()); ?>" title="<?php echo l('text_no_photo', 'listings', '', 'button', array()); ?>">

		<?php endif; ?>

	</div>

	

	<?php if ($this->_vars['data']['photo_count'] > 1): ?>

	<?php if ($this->_vars['data']['photo_count'] > $this->_vars['page_data']['visible']): ?>

	<?php echo tpl_function_js(array('file' => 'jcarousellite.min.js'), $this);?>

	<?php echo tpl_function_js(array('file' => 'init_carousel_controls.js'), $this);?>

	<script><?php echo '

		$(function(){

			var rtl = \'rtl\' === \'';  echo $this->_vars['_LANG']['rtl'];  echo '\';

			var idPrev, idNext;

			if(!rtl) {

				idNext = \'#directionright';  echo $this->_vars['page_data']['rand'];  echo '\';

				idPrev = \'#directionleft';  echo $this->_vars['page_data']['rand'];  echo '\';

			} else {

				idNext = \'#directionleft';  echo $this->_vars['page_data']['rand'];  echo '\';

				idPrev = \'#directionright';  echo $this->_vars['page_data']['rand'];  echo '\';

			};

			$(\'#listings_carousel .carousel_block';  echo $this->_vars['page_data']['rand'];  echo '\').jCarouselLite({

				rtl: rtl,

				visible: ';  echo $this->_vars['page_data']['visible'];  echo ',

				btnNext: idNext,

				btnPrev: idPrev,

				circular: false,

				afterEnd: function(a) {

					var index = $(a[0]).index();

					carousel_controls';  echo $this->_vars['page_data']['rand'];  echo '.update_controls(index);

				}

			});

			carousel_controls';  echo $this->_vars['page_data']['rand'];  echo ' = new init_carousel_controls({

				rtl: rtl,

				carousel_images_count: ';  echo $this->_vars['page_data']['visible'];  echo ',

				carousel_total_images: ';  echo $this->_vars['data']['photo_count'];  echo ',

				btnNext: idNext,

				btnPrev: idPrev

			});

		});

	'; ?>
</script>	

	<?php endif; ?>	

	<script><?php echo '

		$(function(){

			$(\'#previous_item\').bind(\'click\', function(){

				$(\'#listings_carousel li.active\').removeClass(\'active\').prev().addClass(\'active\');

			});

			$(\'#next_item\').bind(\'click\', function(){

				$(\'#listings_carousel li.active\').removeClass(\'active\').next().addClass(\'active\');

			});

			$(\'#listings_carousel img\').bind(\'click\', function(){

				var control = $(\'#numbers a\');

				if(control.length){

					control.attr(\'rel\', $(this).attr(\'rel\')); control.trigger(\'click\');

					$(\'#listings_carousel li\').removeClass(\'active\');

					$(this).parent().addClass(\'active\');

				}

			});

		});

	'; ?>
</script>	

	<div id="listings_carousel" class="carousel">

		<div id="directionleft<?php echo $this->_vars['page_data']['rand']; ?>
" class="directionleft <?php if ($this->_vars['data']['photo_count'] <= $this->_vars['page_data']['visible']): ?>hide<?php endif; ?>">

			<div class="fa fa-arrow-left w fa-lg edge hover" id="l_hover"></div>

		</div>

		<div class="carousel_block carousel_block<?php echo $this->_vars['page_data']['rand']; ?>
 item_<?php echo $this->_vars['page_data']['visible']; ?>
_info">

			<ul>

				<?php echo tpl_function_counter(array('print' => false,'assign' => counter,'start' => 0), $this);?>

				<?php if (is_array($this->_vars['data']['photos']) and count((array)$this->_vars['data']['photos'])): foreach ((array)$this->_vars['data']['photos'] as $this->_vars['key'] => $this->_vars['item']): ?>

				<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>

				<?php 
$this->assign('photo_alt', l('text_listing_photo_number', 'listings', '', 'text', array()));
 ?>

				<?php 
$this->assign('photo_title', l('link_listing_photo_number', 'listings', '', 'text', array()));
 ?>

				<li <?php if ($this->_vars['counter'] == 1): ?>class="active"<?php endif; ?>><img src="<?php echo $this->_vars['item']['media']['thumbs']['60_60']; ?>
" id="listing_photo<?php echo $this->_vars['key']; ?>
" rel="<?php echo $this->_vars['counter']; ?>
"></li>

				<?php endforeach; endif; ?>

			</ul>

		</div>

		<div id="directionright<?php echo $this->_vars['page_data']['rand']; ?>
" class="directionright">

			<div class="fa fa-arrow-right w fa-lg edge hover" id="r_hover"></div>

		</div>

		<div class="clr"></div>		

	</div>

	<div class="clr"></div>

	<?php endif; ?>

<?php endif; ?>

<?php if ($this->_vars['section_gid'] == 'print'): ?>

	<?php if ($this->_vars['data']['photos'] > 0): ?>

	<div class="tabs tab-size-15">

		<ul>

			<li><?php echo l('filter_section_gallery', 'listings', '', 'text', array()); ?></li>

		</ul>

	</div>

	<div class="photos">

		<table>

			<tr>

			<td>

			<?php if (is_array($this->_vars['data']['photos']) and count((array)$this->_vars['data']['photos'])): foreach ((array)$this->_vars['data']['photos'] as $this->_vars['key'] => $this->_vars['item']): ?>

			 <?php if ($this->_vars['key'] <= $this->_vars['pdfc']): ?>

			<img src="<?php echo $this->_vars['item']['media']['thumbs']['200_200']; ?>
">

			<?php endif; ?>

			<?php endforeach; endif; ?>

			</td>

			</tr>

		</table>

		<div class="clr"></div>

	</div>

	<?php endif; ?>

<?php endif; ?>

<?php if ($this->_vars['section_gid'] == 'virtual_tour'): ?>

	<?php if ($this->_vars['data']['virtual_tour_count']): ?>

	<div id="panorama_block"><?php echo tpl_function_block(array('name' => virtual_tour_block,'module' => listings,'data' => $this->_vars['data']['virtual_tour']['0']), $this);?></div>

	

	<?php echo tpl_function_js(array('file' => 'jcarousellite.min.js'), $this);?>

	<?php echo tpl_function_js(array('file' => 'init_carousel_controls.js'), $this);?>

	<script><?php echo '

		$(function(){

			var rtl = \'rtl\' === \'';  echo $this->_vars['_LANG']['rtl'];  echo '\';

			var idVtourPrev, idVtourNext;

			if(!rtl){

				idVtourNext = \'#directionright_vtour_';  echo $this->_vars['page_data']['rand'];  echo '\';

				idVtourPrev = \'#directionleft_vtour_';  echo $this->_vars['page_data']['rand'];  echo '\';

			} else {

				idVtourNext = \'#directionleft_vtour_';  echo $this->_vars['page_data']['rand'];  echo '\';

				idVtourPrev = \'#directionright_vtour_';  echo $this->_vars['page_data']['rand'];  echo '\';

			};

			$(\'#listings_vtour_carousel .carousel_block_vtour_';  echo $this->_vars['page_data']['rand'];  echo '\').jCarouselLite({

				rtl: rtl,

				visible: ';  echo $this->_vars['page_data']['visible'];  echo ',

				btnNext: idVtourNext,

				btnPrev: idVtourPrev,

				circular: false,

				afterEnd: function(a) {

					var index = $(a[0]).index();

					carousel_controls_vtour_';  echo $this->_vars['page_data']['rand'];  echo '.update_controls(index);

				}

			});

			carousel_controls_vtour_';  echo $this->_vars['page_data']['rand'];  echo ' = new init_carousel_controls({

				rtl: rtl,

				carousel_images_count: ';  echo $this->_vars['page_data']['visible'];  echo ',

				carousel_total_images: ';  echo $this->_vars['data']['virtual_tour_count'];  echo ',

				btnNext: idVtourNext,

				btnPrev: idVtourPrev,

			});

			

			$(\'#listings_vtour_carousel img\').bind(\'click\', function(){

				$(\'#listings_vtour_carousel li\').removeClass(\'active\');

				$(this).parent().addClass(\'active\');

			});

		});

	'; ?>
</script>

	<div id="listings_vtour_carousel" class="carousel <?php if ($this->_vars['data']['virtual_tour_count'] <= $this->_vars['page_data']['visible']): ?>visible<?php endif; ?>">

		<div id="directionleft_vtour_<?php echo $this->_vars['page_data']['rand']; ?>
" class="directionleft">

			<div class="fa fa-arrow-left w fa-lg edge hover" id="l_hover"></div>

		</div>

		<div class="carousel_block carousel_block_vtour_<?php echo $this->_vars['page_data']['rand']; ?>
 item_<?php echo $this->_vars['page_data']['visible']; ?>
_info">

			<ul>

				<?php echo tpl_function_counter(array('print' => false,'assign' => counter,'start' => 0), $this);?>

				<?php if (is_array($this->_vars['data']['virtual_tour']) and count((array)$this->_vars['data']['virtual_tour'])): foreach ((array)$this->_vars['data']['virtual_tour'] as $this->_vars['key'] => $this->_vars['item']): ?>

				<?php echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>

				<li <?php if ($this->_vars['counter'] == 1): ?>class="active"<?php endif; ?>><img src="<?php echo $this->_vars['item']['media']['thumbs']['60_60']; ?>
" id="listing_panorama<?php echo $this->_vars['key']; ?>
" rel="<?php echo $this->_vars['counter']; ?>
" class="panorama" data-url="<?php echo $this->_vars['item']['media']['url']; ?>
" data-file="<?php echo $this->_run_modifier($this->_vars['item']['media']['thumbs']['620_400'], 'replace', 'plugin', 1, $this->_vars['item']['media']['url'], ''); ?>
" data-width="<?php echo $this->_vars['item']['settings']['width']; ?>
" data-height="<?php echo $this->_vars['item']['settings']['height']; ?>
" data-comment="<?php echo $this->_run_modifier($this->_vars['item']['comment'], 'escape', 'plugin', 1); ?>
"></li>

				<?php endforeach; endif; ?>

			</ul>

		</div>

		<div id="directionright_vtour_<?php echo $this->_vars['page_data']['rand']; ?>
" class="directionright">

			<div class="fa fa-arrow-right w fa-lg edge hover" id="r_hover"></div>

		</div>

		<div class="clr"></div>		

	</div>

	<div class="clr"></div>

	<?php endif; ?>

<?php endif; ?>

<?php if ($this->_vars['section_gid'] == 'map'): ?>

	<?php echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'id_user' => $this->_vars['user_id'],'object_id' => $this->_vars['data']['id'],'gid' => 'listing_view','markers' => $this->_vars['markers'],'settings' => $this->_vars['map_settings'],'width' => '630','height' => '400','only_load_scripts' => $this->_vars['map_only_load_scripts'],'only_load_content' => $this->_vars['map_only_load_content']), $this);?>

<?php endif; ?>

<?php if ($this->_vars['section_gid'] == 'panorama'): ?>

	<?php if (! $this->_vars['panorama_only_load_scripts']): ?><div id="pano_container" class="pano_container"></div><?php endif; ?>

	<?php echo tpl_function_block(array('name' => show_default_map,'module' => geomap,'id_user' => $this->_vars['data']['id_user'],'gid' => 'listing_view','markers' => $this->_vars['markers'],'settings' => $this->_vars['map_settings'],'width' => '630','height' => '400','only_load_scripts' => $this->_vars['panorama_only_load_scripts'],'only_load_content' => $this->_vars['panorama_only_load_content']), $this);?>	

<?php endif; ?>

<?php if ($this->_vars['section_gid'] == 'reviews'): ?>

	<?php echo tpl_function_block(array('name' => get_reviews_block,'module' => reviews,'object_id' => $this->_vars['data']['id'],'type_gid' => 'listings_object'), $this);?>

<?php endif; ?>

<?php if ($this->_vars['section_gid'] == 'video'): ?>

	<div class="r"><?php echo $this->_vars['data']['listing_video_content']['embed']; ?>
</div>

<?php endif; ?>

<?php if ($this->_vars['section_gid'] == 'file'): ?>

	<h3><?php echo $this->_vars['data']['listing_file_name']; ?>
</h3>

	<?php if ($this->_vars['data']['listing_file_comment']):  echo $this->_vars['data']['listing_file_comment']; ?>
<br><br><?php endif; ?>

	

	<div class="download-bar">

		<a class="btn-link" title="<?php echo l('field_file_download', 'listings', '', 'button', array()); ?>" href="<?php echo $this->_vars['data']['listing_file_content']['file_url']; ?>
" target="blank"><ins class="fa fa-download fa-lg edge hover"></ins></a>

		<?php echo $this->_vars['data']['listing_file_content']['file_name']; ?>


	</div>	

<?php endif; ?>

<?php if ($this->_vars['section_gid'] == 'calendar'): ?>

	<?php echo tpl_function_block(array('name' => 'listings_calendar_block','module' => 'listings','listing' => $this->_vars['data'],'template' => 'view','count' => 2), $this);?>

	<?php echo tpl_function_block(array('name' => 'listings_booking_block','module' => 'listings','listing' => $this->_vars['data'],'template' => 'form','no_save' => 1), $this);?>

<?php endif; ?>

<?php if ($this->_vars['field_editor_section']): ?>

	<?php if (is_array($this->_vars['sections_data']) and count((array)$this->_vars['sections_data'])): foreach ((array)$this->_vars['sections_data'] as $this->_vars['section_key'] => $this->_vars['section_item']): ?>

	<h2><?php echo $this->_vars['section_item']['name']; ?>
</h2>	

		<?php if (is_array($this->_vars['data']['field_editor']) and count((array)$this->_vars['data']['field_editor'])): foreach ((array)$this->_vars['data']['field_editor'] as $this->_vars['item']): ?>

		<?php if ($this->_vars['item']['section_gid'] == $this->_vars['section_item']['gid']): ?>

		<div class="r">

			<div class="f"><?php echo $this->_vars['item']['name']; ?>
: </div>

			<div class="v"><?php echo $this->_vars['item']['value']; ?>
</div>

		</div>

		<?php endif; ?>

		<?php endforeach; endif; ?>

	<?php endforeach; endif; ?>

<?php endif; ?>




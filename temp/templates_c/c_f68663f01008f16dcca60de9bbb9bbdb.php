<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-02-10 05:21:26 India Standard Time */ ?>

<div class="tabs tab-size-15 noPrint">
	<ul id="user_sections">
		<?php if (is_array($this->_vars['display_sections']) and count((array)$this->_vars['display_sections'])): foreach ((array)$this->_vars['display_sections'] as $this->_vars['sgid'] => $this->_vars['item']): ?>
		<?php $this->assign('sheadline', 'filter_section_'.$this->_vars['sgid']); ?>
		<?php if ($this->_vars['item']): ?><li id="m_<?php echo $this->_vars['sgid']; ?>
" sgid="<?php echo $this->_vars['sgid']; ?>
" class="<?php if ($this->_vars['section_gid'] == $this->_vars['sgid']): ?>my<?php echo $this->_vars['sgid']; ?>
 active<?php endif; ?>">
        <a id="<?php echo $this->_vars['sgid']; ?>
" href="<?php echo $this->_vars['site_url']; ?>
users/view/<?php echo $this->_vars['user']['id']; ?>
/<?php echo $this->_vars['sgid']; ?>
">&nbsp;<?php echo l($this->_vars['sheadline'], 'users', '', 'text', array()); ?> <?php if ($this->_vars['sgid'] == 'reviews'): ?>(<?php echo $this->_vars['user']['review_count']; ?>
)<?php endif; ?></a></li><?php endif; ?>
		<?php endforeach; endif; ?>
	</ul>
</div>
 <script><?php echo '
	$(function(){ 
    $("#contacts").before("<i class=\'fa fa-user whitefa\'></i>");
    $("#map").before("<i class=\'fa fa-map-marker\'></i>");
	$("#services").before("<i class=\'fa fa-cog\'></i>");
	});
	'; ?>
</script>
<script><?php echo '
	$(function(){ 
	 if ( $( "#m_map" ).hasClass( "mymap" ) ) {
	 $("#content_m_cimg").hide();
	 $(".fa-user").removeClass("whitefa"); 
	 }
	 if ( $( "#m_services" ).hasClass( "myservices" ) ) {
	 $("#content_m_cimg").hide();
	 $(".fa-user").removeClass("whitefa");
	 }
	});
'; ?>
</script>	

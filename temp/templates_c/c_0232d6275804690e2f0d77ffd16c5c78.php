<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.ld_option.php'); $this->register_function("ld_option", "tpl_function_ld_option");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-01-12 12:54:53 India Standard Time */ ?>

	<tr id="period<?php echo $this->_vars['period']['id']; ?>
">
		<td>
							<?php switch($this->_vars['listing']['price_period']): case '1':  ?>
					<?php echo $this->_run_modifier($this->_vars['period']['date_start'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
 &mdash; <?php echo $this->_run_modifier($this->_vars['period']['date_end'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>

				<?php break; case '2':  ?>
					<?php echo tpl_function_ld_option(array('i' => 'month-names','gid' => 'start','option' => $this->_run_modifier($this->_vars['period']['date_start'], 'date_format', 'plugin', 1, '%m')), $this);?>
					<?php echo $this->_run_modifier($this->_vars['period']['date_start'], 'date_format', 'plugin', 1, '%Y'); ?>

					&mdash; 
					<?php echo tpl_function_ld_option(array('i' => 'month-names','gid' => 'start','option' => $this->_run_modifier($this->_vars['period']['date_end'], 'date_format', 'plugin', 1, '%m')), $this);?>
					<?php echo $this->_run_modifier($this->_vars['period']['date_end'], 'date_format', 'plugin', 1, '%Y'); ?>

			<?php break; endswitch; ?>
		</td>
		
		<td class="center">
			<?php if ($this->_vars['period']['status'] == 'open'): ?>
				<?php if ($this->_vars['period']['price']):  echo $this->_vars['period']['price'];  else:  echo l('text_booking_price_unknown', 'listings', '', 'text', array());  endif; ?>
			<?php else: ?>
				&nbsp;
			<?php endif; ?>
		</td>
		<td title="<?php echo $this->_run_modifier($this->_vars['period']['comment'], 'escape', 'plugin', 1); ?>
"><?php if ($this->_vars['period']['comment']):  echo $this->_run_modifier($this->_vars['period']['comment'], 'truncate', 'plugin', 1, 50);  else: ?><span class="gray_italic"><?php echo l('no_information', 'listings', '', 'text', array()); ?></span><?php endif; ?></td>
		<td class="center">
			<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/period_edit/<?php echo $this->_vars['period']['id']; ?>
" id="period_edit_<?php echo $this->_vars['period']['id']; ?>
" alt="<?php echo l('btn_edit', 'start', '', 'button', array()); ?>" title="<?php echo l('btn_edit', 'start', '', 'button', array()); ?>"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('btn_edit', 'start', '', 'button', array()); ?>"></a>
			<a href="<?php echo $this->_vars['site_url']; ?>
admin/listings/period_delete/<?php echo $this->_vars['period']['id']; ?>
" id="period_delete_<?php echo $this->_vars['period']['id']; ?>
" title="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>"></a>
			<script><?php echo '
				var period_';  echo $this->_vars['period']['id'];  echo ';
				$(function(){
					period_';  echo $this->_vars['period']['id'];  echo ' = new bookingForm({
						siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
						listingId: \'';  echo $this->_vars['period']['id_listing'];  echo '\',
						periodId: \'';  echo $this->_vars['period']['id'];  echo '\',
						bookingBtn: \'period_edit_';  echo $this->_vars['period']['id'];  echo '\',
						deleteBtn: \'period_delete_';  echo $this->_vars['period']['id'];  echo '\',
						cFormId: \'';  if ($this->_vars['period']['status'] == 'open'): ?>period_form<?php else: ?>order_form<?php endif;  echo '\',
						urlGetForm: \''; ?>
admin/listings/<?php if ($this->_vars['period']['status'] == 'open'): ?>ajax_period_form<?php else: ?>ajax_order_form<?php endif; ?>/<?php echo '\',
						urlSaveForm: \''; ?>
admin/listings/<?php if ($this->_vars['period']['status'] == 'open'): ?>ajax_save_period<?php else: ?>ajax_save_order<?php endif; ?>/<?php echo '\',
						urlDeletePeriod: \''; ?>
admin/listings/<?php if ($this->_vars['period']['status'] == 'open'): ?>ajax_period_delete<?php else: ?>ajax_order_delete<?php endif; ?>/<?php echo '\',
						note_delete: \'';  echo l('note_period_delete', 'listings', '', 'js', array());  echo '\',
						';  if ($this->_vars['rand']): ?>calendar: listings_calendar<?php echo $this->_vars['rand']; ?>
,<?php endif;  echo '
						successCallback: function(id, data, calendar){
							$(\'#period';  echo $this->_vars['period']['id'];  echo '\').replaceWith(data);
							period_';  echo $this->_vars['period']['id'];  echo '.set_calendar(calendar);
						},
						deleteCallback: function(){
							var periods = $(\'#';  if ($this->_vars['period']['status'] == 'open'): ?>listing_periods<?php else: ?>listing_booked<?php endif;  echo '\');
							if(periods.find(\'tr\').length != 1) return;
							periods.find(\'tr\').after(
								\'<tr><td colspan="5" class="center gray_italic">';  echo l('no_periods', 'listings', '', 'text', array());  echo '</td></tr>\'
							);
						},
					});
				});
			'; ?>
</script>
		</td>
	</tr>

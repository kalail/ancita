<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.ld_option.php'); $this->register_function("ld_option", "tpl_function_ld_option");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.sorter.php'); $this->register_function("sorter", "tpl_function_sorter");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-31 13:40:43 India Daylight Time */ ?>

	<?php if ($this->_vars['orders']): ?>
	<div class="sorter line" id="sorter_block">
		<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
		<div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div>
	</div>
	<?php endif; ?>
	
	<div>	
		<?php if (is_array($this->_vars['orders']) and count((array)$this->_vars['orders'])): foreach ((array)$this->_vars['orders'] as $this->_vars['item']): ?>
		<div class="listing-block">
			<div id="item-block-<?php echo $this->_vars['item']['id']; ?>
" class="item listing">
				<div class="image">
					<?php 
$this->assign('logo_title', l('link_listing_view', 'listings', '', 'button', array('id_ref'=>$this->_vars['item']['id'])));
 ?>
					<?php 
$this->assign('text_listing_logo', l('text_listing_logo', 'listings', '', 'button', array('id_ref'=>$this->_vars['item']['id'],'property_type'=>$this->_vars['item']['property_type_str'],'operation_type'=>$this->_vars['item']['operation_type_str'],'location'=>$this->_vars['item']['location'])));
 ?>
					<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']['listing']), $this);?>">
						<img src="<?php echo $this->_vars['item']['listing']['media']['photo']['thumbs']['small']; ?>
" alt="<?php echo $this->_vars['text_listing_logo']; ?>
" title="<?php echo $this->_vars['logo_title']; ?>
">
					</a>
				</div>
				<div class="body">
					<h3><a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']['listing']), $this);?>" title="<?php echo $this->_run_modifier($this->_vars['item']['listing']['output_name'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['item']['listing']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a></h3>
					<div class="t-1">
													<?php switch($this->_vars['item']['listing']['price_period']): case '1':  ?>
								<?php echo $this->_run_modifier($this->_vars['item']['date_start'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>
 - <?php echo $this->_run_modifier($this->_vars['item']['date_end'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>

							<?php break; case '2':  ?>
								<?php echo tpl_function_ld_option(array('i' => 'month-names','gid' => 'start','option' => $this->_run_modifier($this->_vars['item']['date_start'], 'date_format', 'plugin', 1)), $this);?>
								<?php echo $this->_run_modifier($this->_vars['item']['date_start'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>

								&mdash; 
								<?php echo tpl_function_ld_option(array('i' => 'month-names','gid' => 'start','option' => $this->_run_modifier($this->_vars['item']['date_end'], 'date_format', 'plugin', 1)), $this);?>
								<?php echo $this->_run_modifier($this->_vars['item']['date_end'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']); ?>

						<?php break; endswitch; ?><br>
                        <?php echo $this->_vars['item']['name']; ?>
<br>
						<?php echo $this->_vars['item']['phone']; ?>
<br>
                        <?php echo $this->_vars['item']['mail']; ?>
<br>
                        <?php echo $this->_vars['item']['comment']; ?>
<br>
						<?php echo $this->_vars['item']['answer']; ?>
<br>
					</div>
					<div class="t-2">
						<span><?php echo $this->_run_modifier($this->_vars['item']['date_modified'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_time_format']); ?>
</span><br>
						
					</div>
				</div>
				<div class="clr"></div>
				<?php if ($this->_vars['item']['status'] == 'wait'): ?>
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/order_approve/<?php echo $this->_vars['item']['id']; ?>
" data-id="<?php echo $this->_vars['item']['id']; ?>
" class="btn-link order_approve" title="<?php echo l('btn_approve', 'start', '', 'button', array()); ?>"><ins class="fa fa-check fa-lg edge hover"></ins></a><span class="btn-text link-r-margin"><?php echo l('btn_approve', 'start', '', 'text', array()); ?></span>
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/order_decline/<?php echo $this->_vars['item']['id']; ?>
" data-id="<?php echo $this->_vars['item']['id']; ?>
" class="btn-link order_decline" title="<?php echo l('btn_decline', 'start', '', 'button', array()); ?>"><ins class="fa fa-minus fa-lg edge hover"></ins></a><span class="btn-text link-r-margin"><?php echo l('btn_decline', 'start', '', 'text', array()); ?></span>
				<?php endif; ?>
				<?php echo tpl_function_block(array('name' => 'button_contact','module' => 'mailbox','user_id' => $this->_vars['item']['user']['id'],'user_type' => $this->_vars['item']['user']['user_type'],'template' => 'button'), $this);?>
				<a href="<?php echo $this->_vars['site_url']; ?>
listings/edit/<?php echo $this->_vars['item']['listing']['id']; ?>
/calendar/2" class="btn-link" title="<?php echo l('link_booking_listing', 'listings', '', 'button', array()); ?>"><ins class="fa fa-calendar-o fa-lg edge hover"><ins class="fa fa-calendar-number"></ins></ins></a><span class="btn-text link-r-margin"><?php echo l('btn_calendar', 'listings', '', 'text', array()); ?></span>
				<div class="clr"></div>
			</div>
		</div>
		<?php endforeach; else: ?>
		<div class="item empty"><?php echo l('no_orders', 'listings', '', 'text', array()); ?></div>
		<?php endif; ?>
		
	</div>
	
	<?php if ($this->_vars['orders']): ?><div id="pages_block_2"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?></div><?php endif; ?>

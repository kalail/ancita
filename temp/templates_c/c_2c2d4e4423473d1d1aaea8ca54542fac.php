<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-31 13:38:02 India Daylight Time */ ?>

<?php if (count ( $this->_vars['services'] ) > 0): ?>
<div class="line top">
<span class="btn-text"><?php echo l('you_can_auth', 'users', '', 'text', array()); ?></span>
<?php if (is_array($this->_vars['services']) and count((array)$this->_vars['services'])): foreach ((array)$this->_vars['services'] as $this->_vars['item']): ?>
	<a href="<?php echo $this->_vars['site_url']; ?>
users_connections/oauth_login/<?php echo $this->_vars['item']['id']; ?>
" class="btn-link"><ins class="fa fa-<?php switch($this->_vars['item']['gid']): case 'vkontakte':  ?>vk<?php break; case 'google':  ?>google-plus<?php break; default:   echo $this->_vars['item']['gid'];  break; endswitch; ?> fa-lg square edge hover"></ins></a>
<?php endforeach; endif; ?>
</div>
<?php endif; ?>

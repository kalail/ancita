<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-30 09:36:35 India Daylight Time */ ?>

<div class="view-user">
<?php 
$this->assign('no_info_str', l('no_information', 'users', '', 'text', array()));
 ?>
<?php 
$this->assign('not_access_str', l('not_access', 'users', '', 'text', array()));
 ?>
<?php 
$this->assign('buy_str', l('please_buy', 'users', '', 'text', array()));
 ?>
<?php if ($this->_vars['user_data']['user_type'] == 'agent' && $this->_vars['user_data']['agent_status']): ?>
    <div class="r">
		<div class="f"><?php echo l('company', 'users', '', 'text', array()); ?>: </div>
		<div class="v"><a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['user_data']['company']), $this);?>"><?php echo $this->_vars['user_data']['company']['output_name']; ?>
</a></div>
    </div>
<?php endif; ?>
<?php if ($this->_vars['user_data']['contact_info'] != ''): ?>
<div class="r">
    <div class="f"><?php echo l('field_description', 'users', '', 'text', array()); ?>: </div>
    <div class="v">
		<?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		<?php elseif (! $this->_vars['user_data']['contact_info']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		<?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		<?php elseif ($this->_vars['template'] == 'small'):  echo $this->_run_modifier($this->_vars['user_data']['contact_info'], 'truncate', 'plugin', 1, 55); ?>

		<?php else:  echo $this->_vars['user_data']['contact_info']; ?>

		<?php endif; ?>
    </div>
</div>
<?php endif; ?>
<?php if ($this->_vars['user_data']['facebook'] != ''): ?>
<div class="r">
    <div class="f"><?php echo l('field_facebook', 'users', '', 'text', array()); ?>: </div>
    <div class="v">
		<?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		<?php elseif (! $this->_vars['user_data']['facebook']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		<?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		<?php else: ?><a href="<?php echo $this->_run_modifier($this->_vars['user_data']['facebook'], 'escape', 'plugin', 1); ?>
" target="_blank"><?php echo $this->_vars['user_data']['facebook']; ?>
</a>
		<?php endif; ?>
    </div>
</div>
<?php endif; ?>
<!--<div class="r">
    <div class="f"><?php echo l('field_twitter', 'users', '', 'text', array()); ?>: </div>
    <div class="v">
		<?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		<?php elseif (! $this->_vars['user_data']['twitter']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		<?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		<?php else: ?><a href="<?php echo $this->_run_modifier($this->_vars['user_data']['twitter'], 'escape', 'plugin', 1); ?>
" target="_blank"><?php echo $this->_vars['user_data']['twitter']; ?>
</a>
		<?php endif; ?>
    </div>
</div>
<div class="r">
    <div class="f"><?php echo l('field_vkontakte', 'users', '', 'text', array()); ?>: </div>
    <div class="v">
		<?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		<?php elseif (! $this->_vars['user_data']['vkontakte']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		<?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		<?php else: ?><a href="<?php echo $this->_run_modifier($this->_vars['user_data']['vkontakte'], 'escape', 'plugin', 1); ?>
" target="_blank"><?php echo $this->_vars['user_data']['vkontakte']; ?>
</a>
		<?php endif; ?>
    </div>
</div>-->

	<?php switch($this->_vars['user_data']['user_type']): case 'private':  ?>
    <?php if ($this->_vars['user_data']['contact_phone'] != ''): ?>
	<div class="r">
		<div class="f"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['contact_phone']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user_data']['contact_phone']; ?>

		    <?php endif; ?>
		</div>
	</div>
    <?php endif; ?>
    <?php if ($this->_vars['user_data']['contact_email'] != ''): ?>
	<div class="r">
		<div class="f"><?php echo l('field_contact_email', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['contact_email']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user_data']['contact_email']; ?>

		    <?php endif; ?>
		</div>
	</div>
     <?php endif; ?>

	<?php break; case 'company':  ?>
    <?php if ($this->_vars['user_data']['location'] != ''): ?>
	<div class="r">
		<div class="f"><?php echo l('field_region', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['location']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user_data']['location']; ?>

		    <?php endif; ?>
		</div>
	</div>
    <?php endif; ?>
    <?php if ($this->_vars['user_data']['contact_phone'] != ''): ?>
	<div class="r">
		<div class="f"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['contact_phone']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else: ?><span itemprop="tel"><?php echo $this->_vars['user_data']['contact_phone']; ?>
</span>
		    <?php endif; ?>
		</div>
	</div>
     <?php endif; ?>
     <?php if ($this->_vars['user_data']['contact_email'] != ''): ?>
	<div class="r">
		<div class="f"><?php echo l('field_contact_email', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['contact_email']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user_data']['contact_email']; ?>

		    <?php endif; ?>
		</div>
	</div>
    <?php endif; ?>
    <?php if ($this->_vars['user_data']['web_url'] != ''): ?>
	<div class="r">
		<div class="f"><?php echo l('field_web_url', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['web_url']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else: ?><a href="http://<?php echo $this->_vars['user_data']['web_url']; ?>
" target="_blank" itemprop="url"><?php echo $this->_vars['user_data']['web_url']; ?>
</a>
		    <?php endif; ?>
		</div>
	</div>
    <?php endif; ?>
    <?php if ($this->_vars['user_data']['working_days_str'] != ''): ?>
	<div class="r">
	    <div class="f"><?php echo l('field_working_days', 'users', '', 'text', array()); ?>: </div>
	    <div class="v">
			<?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['working_days_str']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user_data']['working_days_str']; ?>

		    <?php endif; ?>
		</div>
	</div>
    <?php endif; ?>
    <?php if ($this->_vars['user_data']['working_hours_str'] != ''): ?>
	<div class="r">
	    <div class="f"><?php echo l('field_working_hours', 'users', '', 'text', array()); ?>: </div>
	    <div class="v">
			<?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['working_hours_str']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user_data']['working_hours_str']; ?>

		    <?php endif; ?>
		</div>
	</div>
    <?php endif; ?>
	<!--<div class="r">
	    <div class="f"><?php echo l('field_lunch_time', 'users', '', 'text', array()); ?>: </div>
	    <div class="v">
			<?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['lunch_time_str']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user_data']['lunch_time_str']; ?>

		    <?php endif; ?>
		</div>
	</div>-->
	
	<?php break; case 'agent':  ?>
    <?php if (! $this->_vars['user_data']['contact_phone'] != ''): ?>
	<div class="r">
		<div class="f"><?php echo l('field_phone', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['contact_phone']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user_data']['contact_phone']; ?>

		    <?php endif; ?>
		</div>
	</div>
    <?php endif; ?>
    <?php if (! $this->_vars['user_data']['contact_email'] != ''): ?>
	<div class="r">
		<div class="f"><?php echo l('field_contact_email', 'users', '', 'text', array()); ?>:</div>
		<div class="v">
		    <?php if ($this->_vars['user_data']['no_access_contact']): ?><font class="gray_italic"><?php echo $this->_vars['not_access_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['contact_email']): ?><font class="gray_italic"><?php echo $this->_vars['no_info_str']; ?>
</font>
		    <?php elseif (! $this->_vars['user_data']['is_contact']): ?><font class="gray_italic"><?php echo $this->_vars['buy_str']; ?>
</font>
		    <?php else:  echo $this->_vars['user_data']['contact_email']; ?>

		    <?php endif; ?>
		</div>
	</div>
    <?php endif; ?>
<?php break; endswitch; ?>

<?php if (! $this->_vars['user_data']['is_contact'] && ! $this->_vars['user_data']['no_access_contact']): ?>
<div class="buy-box"><input type="button" value="<?php echo l('btn_activate_contact', 'services', '', 'button', array()); ?>" name="contacts_btn"></div>
<?php endif; ?>

</div>

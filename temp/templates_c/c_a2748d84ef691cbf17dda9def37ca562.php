<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.ld.php'); $this->register_function("ld", "tpl_function_ld");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.country_input.php'); $this->register_function("country_input", "tpl_function_country_input");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-11 05:53:10 India Daylight Time */ ?>

<!--
    Task    : Change how the "More option" works
	Date    : 3-Nov-2015
    Changes : 	1. removed the "With Photo","Open House","By private person"
			    2. Removed more/less link button
                3. aligned the search button
    
    Task	: Heading for Listings for rent
    Date	: 4-Nov-2015
    Changes :  1. Add script for change listing heading
    
    Task	: CR-2015-03-2.1.2
    Date	:12-Nov-2015

Modified By : Mahi-->
<script> 
	var selects_<?php echo $this->_vars['form_settings']['rand']; ?>
 = []; 
	var checkboxes_<?php echo $this->_vars['form_settings']['rand']; ?>
 = []; 
	var form_selects_<?php echo $this->_vars['form_settings']['rand']; ?>
 = []; 
	var form_checkboxes_<?php echo $this->_vars['form_settings']['rand']; ?>
 = []; 
	var selectObject_<?php echo $this->_vars['form_settings']['rand']; ?>
;
	var checkboxObject_<?php echo $this->_vars['form_settings']['rand']; ?>
;
	var formSelectObject_<?php echo $this->_vars['form_settings']['rand']; ?>
;
	var formCheckboxObject_<?php echo $this->_vars['form_settings']['rand']; ?>
;
</script>
<form action="" method="<?php if ($this->_vars['listings_page_data']['type'] == 'line'): ?>GET<?php else: ?>POST<?php endif; ?>" id="main_search_form_<?php echo $this->_vars['form_settings']['rand']; ?>
">
<div class="search-form <?php echo $this->_run_modifier($this->_vars['listings_page_data']['type'], 'escape', 'plugin', 1); ?>
">
	<div class="inside" style="padding: 0 20px 20px;">
	<?php if ($this->_vars['data']['id_category'] && $this->_vars['data']['property_type']): ?>
		<?php $this->assign('property_type_value', $this->_vars['data']['id_category'].'_'.$this->_vars['data']['property_type']); ?>
	<?php else: ?>
		<?php $this->assign('property_type_value', $this->_vars['data']['id_category']); ?>
	<?php endif; ?>
	<?php if ($this->_vars['listings_page_data']['type'] == 'line'): ?>
		<?php 
$this->assign('default_select_lang', l('field_search_category', 'listings', '', 'text', array()));
 ?>
		<div id="line-search-form-<?php echo $this->_vars['form_settings']['rand']; ?>
">
			<?php $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
			<span>
			<input type="text" name="filters[keyword]" value="<?php echo $this->_run_modifier($this->_vars['data']['keyword'], 'escape', 'plugin', 1); ?>
">
			<?php echo tpl_function_selectbox(array('input' => 'filters[category]','id' => 'id_category_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['property_types'],'subvalue' => $this->_vars['property_items'],'selected' => $this->_vars['property_type_value'],'default' => $this->_vars['default_select_lang'],'formtype' => 'IndexSearch'), $this);?>
			<script> selectDropdownClass_<?php echo $this->_vars['form_settings']['rand']; ?>
 = 'dropdown right'; selects_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('id_category_select_<?php echo $this->_vars['form_settings']['rand']; ?>
'); </script>
			<button class="search" id="main_search_button_<?php echo $this->_vars['form_settings']['rand']; ?>
"><ins class="w fa fa-search fa-fw"></ins></button>
			</span>
			<!--<a href="<?php echo $this->_vars['search_action_link']; ?>
" class="options"><?php echo l('link_more_options', 'start', '', 'text', array()); ?></a>-->
			<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
		</div>
	<?php elseif ($this->_vars['listings_page_data']['type'] == 'slider'): ?>
		<?php 
$this->assign('default_select_lang', l('select_default', 'start', '', 'text', array()));
 ?>
		<div class="btn-block">
			<div id="search-preresult-<?php echo $this->_vars['form_settings']['rand']; ?>
" class="preload"></div>
			<div class="btn-link search-btn">
				<ins class="fa fa-search w fa-lg hover"></ins>
				<input type="submit" id="main_search_button_<?php echo $this->_vars['form_settings']['rand']; ?>
" name="search_button" value="<?php echo l('btn_search', 'start', '', 'button', array()); ?>">
			</div>
		</div>
		<div class="fields-block">
			<div id="slider-search-form-<?php echo $this->_vars['form_settings']['rand']; ?>
">
				<div class="search-field country">
					<?php 
$this->assign('location_default', l('field_search_country', 'listings', '', 'text', array()));
 ?>
					<?php echo tpl_function_country_input(array('select_type' => 'city','id_country' => $this->_vars['data']['id_country'],'id_region' => $this->_vars['data']['id_region'],'id_city' => $this->_vars['data']['id_city'],'id_district' => $this->_vars['data']['id_district'],'default' => $this->_vars['location_default'],'var_country' => 'filters[id_country]','var_region' => 'filters[id_region]','var_city' => 'filters[id_city]','var_district' => 'filters[id_district]'), $this);?>
				</div>				
				<div class="search-field category">
					<?php 
$this->assign('property_type_default', l('field_search_category', 'listings', '', 'text', array()));
 ?>
					<?php echo tpl_function_selectbox(array('input' => 'filters[category]','id' => 'id_category_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['property_types'],'subvalue' => $this->_vars['property_items'],'selected' => $this->_vars['property_type_value'],'default' => $this->_vars['property_type_default']), $this);?>
					<script> selects_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('id_category_select_<?php echo $this->_vars['form_settings']['rand']; ?>
'); </script>
				</div>	
			</div>
			<div class="clr"></div>
		</div>
		<input type="hidden" name="filters[type]" value="<?php echo $this->_run_modifier($this->_vars['form_settings']['object'], 'escape', 'plugin', 1); ?>
" id="operation_type<?php echo $this->_vars['form_settings']['rand']; ?>
">
		<input type="hidden" name="new" value="1">
	<?php else: ?>
		<?php 
$this->assign('default_select_lang', l('field_search_category', 'listings', '', 'text', array()));
 ?>

		
		<div class="fields-block">
			<div id="short-search-form-<?php echo $this->_vars['form_settings']['rand']; ?>
">
				<div class="search-field country">
					<p><?php echo l('field_search_country', 'listings', '', 'text', array()); ?></p>
					<?php echo tpl_function_country_input(array('select_type' => 'city','id_country' => $this->_vars['data']['id_country'],'id_region' => $this->_vars['data']['id_region'],'id_city' => $this->_vars['data']['id_city'],'id_district' => $this->_vars['data']['id_district'],'var_country' => 'filters[id_country]','var_region' => 'filters[id_region]','var_city' => 'filters[id_city]','var_district' => 'filters[id_district]'), $this);?>
				</div>				
				<div class="search-field category">
					<p><?php echo l('field_search_category', 'listings', '', 'text', array()); ?></p>
					<?php echo tpl_function_selectbox(array('input' => 'filters[category]','id' => 'id_category_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['property_types'],'subvalue' => $this->_vars['property_items'],'selected' => $this->_vars['property_type_value'],'default' => $this->_vars['default_select_lang'],'formtype' => 'IndexSearch'), $this);?>
					<script>selects_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('id_category_select_<?php echo $this->_vars['form_settings']['rand']; ?>
');</script>
					<script><?php echo '
						$(function(){
							$(\'#id_category_select_';  echo $this->_vars['form_settings']['rand'];  echo '\').bind(\'change\', function(){
								$.ajax({
									url: \'';  echo $this->_vars['site_root'];  echo '\' + \''; ?>
listings/ajax_get_main_search_form/<?php echo $this->_vars['form_settings']['rand'];  echo '\', 
									type: \'POST\',
									data: {type: \'';  echo $this->_vars['form_settings']['object'];  echo '\', category: $(this).val()},
									cache: false,
									dataType: \'json\',
									success: function(data){
										form_selects_';  echo $this->_vars['form_settings']['rand'];  echo ' = [];
										form_checkboxes_';  echo $this->_vars['form_settings']['rand'];  echo ' = [];
										$(\'#main_search_form_';  echo $this->_vars['form_settings']['rand'];  echo '\').find(\'.custom\').remove();
										if(formSelectObject_';  echo $this->_vars['form_settings']['rand'];  echo ') formSelectObject_';  echo $this->_vars['form_settings']['rand'];  echo '.clear();
										$(\'#short-search-form-';  echo $this->_vars['form_settings']['rand'];  echo '\').append(data.short);	
										$(\'#full-search-form-';  echo $this->_vars['form_settings']['rand'];  echo '\').prepend(data.full);	
										formSelectObject_';  echo $this->_vars['form_settings']['rand'];  echo ' = new selectBox({elementsIDs: form_selects_';  echo $this->_vars['form_settings']['rand'];  echo ', selectDropdownClass: \'dropdown\'});
										formCheckboxObject_';  echo $this->_vars['form_settings']['rand'];  echo ' = new checkBox({elementsIDs: form_checkboxes_';  echo $this->_vars['form_settings']['rand'];  echo '});
									}
								});
							});
						});
					'; ?>
</script>
				</div>
				<div class="search-field periodbox <?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>hide<?php endif; ?>" id="booking_date_start">
					<p><?php echo l('field_booking_date_start', 'listings', '', 'text', array()); ?></p>
					<input type="text" name="booking_date_start" value="<?php if ($this->_run_modifier($this->_vars['data']['booking_date_start'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['data']['booking_date_start'], 'date_format', 'plugin', 1, $this->_vars['listings_page_data']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="date_start<?php echo $this->_vars['form_settings']['rand']; ?>
" class="short">
					<input type="hidden" name="filters[booking_date_start]" value="<?php if ($this->_run_modifier($this->_vars['data']['booking_date_start'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['data']['booking_date_start'], 'date_format', 'plugin', 1, '%Y-%m-%d'), 'escape', 'plugin', 1);  endif; ?>" id="alt_date_start<?php echo $this->_vars['form_settings']['rand']; ?>
">
					<script><?php echo '
						$(function(){
							$(\'#date_start';  echo $this->_vars['form_settings']['rand'];  echo '\').datepicker({dateFormat: \'';  echo $this->_vars['listings_page_data']['datepicker_date_format'];  echo '\', altFormat: \'yy-mm-dd\', altField: \'#alt_date_start';  echo $this->_vars['form_settings']['rand'];  echo '\', showOn: \'both\'});
						});
					'; ?>
</script>
				</div>
				<div class="search-field periodbox <?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>hide<?php endif; ?>" id="booking_date_end">
					<p><?php echo l('field_booking_date_end', 'listings', '', 'text', array()); ?></p>
					<input type="text" name="booking_date_end" value="<?php if ($this->_run_modifier($this->_vars['data']['booking_date_end'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['data']['booking_date_end'], 'date_format', 'plugin', 1, $this->_vars['listings_page_data']['date_format']), 'escape', 'plugin', 1);  endif; ?>" id="date_end<?php echo $this->_vars['form_settings']['rand']; ?>
" class="short">
					<input type="hidden" name="filters[booking_date_end]" value="<?php if ($this->_run_modifier($this->_vars['data']['booking_date_end'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_run_modifier($this->_vars['data']['booking_date_end'], 'date_format', 'plugin', 1, '%Y-%m-%d'), 'escape', 'plugin', 1);  endif; ?>" id="alt_date_end<?php echo $this->_vars['form_settings']['rand']; ?>
">
					<script><?php echo '
						$(function(){
							$(\'#date_end';  echo $this->_vars['form_settings']['rand'];  echo '\').datepicker({dateFormat: \'';  echo $this->_vars['listings_page_data']['datepicker_date_format'];  echo '\', altFormat: \'yy-mm-dd\', altField: \'#alt_date_end';  echo $this->_vars['form_settings']['rand'];  echo '\', showOn: \'both\'});
						});
					'; ?>
</script>
				</div>	
				
				<?php if (false): ?>
				<div class="search-field guests-box <?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>hide<?php endif; ?>" id="booking_guests">
					<p><?php echo l('field_booking_guests', 'listings', '', 'text', array()); ?></p>
					<?php echo tpl_function_ld(array('i' => 'booking_guests','gid' => 'listings','assign' => 'booking_guests'), $this);?>
					<?php echo tpl_function_selectbox(array('input' => 'filters[booking_guests]','id' => 'booking_guests_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['booking_guests']['option'],'selected' => $this->_vars['data']['booking_guests'],'default' => $this->_vars['booking_guests']['header']), $this);?>
					<script> selects_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('booking_guests_select_<?php echo $this->_vars['form_settings']['rand']; ?>
'); </script>
				</div>	
                <?php endif; ?>
				
				<!--<div class="search-field price-range">
					<p><?php echo l('field_price_range', 'listings', '', 'text', array()); ?></p>
					<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'price_min')); tpl_block_capture(array('assign' => 'price_min'), null, $this); ob_start(); ?> <input type="text" name="filters[price_min]" class="<?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>short<?php else: ?>mini<?php endif; ?>" value="<?php echo $this->_run_modifier($this->_vars['data']['price_min'], 'escape', 'plugin', 1); ?>
"> <?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
                    
					<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'price_max')); tpl_block_capture(array('assign' => 'price_max'), null, $this); ob_start(); ?> <input type="text" name="filters[price_max]" class="<?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>short<?php else: ?>mini<?php endif; ?>" value="<?php echo $this->_run_modifier($this->_vars['data']['price_max'], 'escape', 'plugin', 1); ?>
"> <?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
                    
					<?php echo tpl_function_block(array('name' => 'currency_output','module' => 'start','value' => $this->_vars['price_min']), $this);?> &nbsp;<?php echo l('text_to', 'listings', '', 'text', array()); ?>&nbsp;
					<?php echo tpl_function_block(array('name' => 'currency_output','module' => 'start','value' => $this->_vars['price_max']), $this);?>
					<input type="hidden" name="short_name" value="filters[price]">
				</div>-->
                
                <?php if ($this->_vars['form_settings']['object'] == 'sale'): ?>
                    <div class="search-field price-range">
                        <p>
                        	<?php echo l('field_price_range', 'listings', '', 'text', array()); ?> 
                            <?php echo tpl_function_block(array('name' => currency_format_output,'module' => start,'value' => '','cur_gid' => $this->_vars['base_currency']['gid'],'cur_custom' => 'manual'), $this);?>
                        </p>
                        
                        <?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'price_min')); tpl_block_capture(array('assign' => 'price_min'), null, $this); ob_start(); ?> <input type="text" id="pricemin" name="filters[price_min]" class="<?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>short<?php else: ?>mini<?php endif; ?>" value="<?php echo $this->_run_modifier($this->_vars['data']['price_min'], 'escape', 'plugin', 1); ?>
"> <?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
                        <?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'price_max')); tpl_block_capture(array('assign' => 'price_max'), null, $this); ob_start(); ?> <input type="text" id="pricemax" name="filters[price_max]" class="<?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>short<?php else: ?>mini<?php endif; ?>" value="<?php echo $this->_run_modifier($this->_vars['data']['price_max'], 'escape', 'plugin', 1); ?>
"> <?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
                        
                        <?php echo tpl_function_block(array('name' => 'currency_output','module' => 'start','value' => $this->_vars['price_min'],'disable_abbr' => true), $this);?> &nbsp;<?php echo l('text_to', 'listings', '', 'text', array()); ?>&nbsp;
                        <?php echo tpl_function_block(array('name' => 'currency_output','module' => 'start','value' => $this->_vars['price_max'],'disable_abbr' => true), $this);?>
                        <input type="hidden" name="short_name" value="filters[price]">
                    </div>
				<?php endif; ?>
				
				<?php if ($this->_vars['form_settings']['object'] != 'rent' && $this->_vars['form_settings']['object'] != 'lease'): ?>
					<?php echo $this->_vars['main_search_extend_form']; ?>

				<?php endif; ?>
			</div>
			<div id="full-search-form-<?php echo $this->_vars['form_settings']['rand']; ?>
" >
				<?php if ($this->_vars['form_settings']['object'] == 'rent' || $this->_vars['form_settings']['object'] == 'lease'): ?>
					<?php echo $this->_vars['main_search_extend_form']; ?>

				<?php endif; ?>
				
				<?php echo $this->_vars['main_search_full_form']; ?>

				
				<?php if ($this->_vars['listings_page_data']['use_advanced']): ?>
                <?php if (false): ?>
				<div class="search-field">
					<p><?php echo l('field_postal_code', 'listings', '', 'text', array()); ?>:</p>
					<input type="text" name="filters[zip]" value="<?php echo $this->_run_modifier($this->_vars['data']['zip'], 'escape', 'plugin', 1); ?>
">
				</div>
                <?php endif; ?>
		
				 <!--<div class="search-field">
					<p><?php echo l('field_radius', 'listings', '', 'text', array()); ?>:</p>
					<?php 
$this->assign('default_radius_select', l('text_radius_select', 'listings', '', 'text', array()));
 ?>
					<?php echo tpl_function_selectbox(array('input' => 'filters[radius]','id' => 'radius_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['radius_data']['option'],'selected' => $this->_vars['data']['radius'],'default' => $this->_vars['default_radius_select']), $this);?>
					<script>selects_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('radius_select_<?php echo $this->_vars['form_settings']['rand']; ?>
');</script>
				</div -->
				
				<div class="search-field">
					<p><?php echo l('field_id', 'listings', '', 'text', array()); ?></p>
					<input type="text" name="filters[id]" id="listid" value="<?php echo $this->_run_modifier($this->_vars['data']['id'], 'escape', 'plugin', 1); ?>
">
				</div>
                <div class="search-field" id="mainsearc" style=" float:right;">
                    <?php if ($this->_vars['form_settings']['object'] == 'rent' || $this->_vars['form_settings']['object'] == 'sale'): ?>
                          
                     <?php endif; ?>
                    <div class="btn-block" style="bottom:0px; position:relative; right:0px; float:right;">
                    <div id="search-preresult-<?php echo $this->_vars['form_settings']['rand']; ?>
" class="preload"></div>
                    <div class="btn-link search-btn fright">
                        <ins class="fa fa-search w fa-lg hover"></ins>
                         <input type='reset' id='reset3' value="<?php echo l('btn_clear', 'start', '', 'button', array()); ?>" />
           <input type="submit" class="mysub" name="search_button" value="<?php echo l('btn_search', 'start', '', 'button', array()); ?>" id="main_search_button_<?php echo $this->_vars['form_settings']['rand']; ?>
">
                    </div>

				</div>
					

				<!--<div class="search-field checkboxes" style="display:none">
					<p>&nbsp;</p>
					<?php if ($this->_vars['form_settings']['object'] != 'buy' && $this->_vars['form_settings']['object'] != 'lease'): ?>
					<?php 
$this->assign('with_photo_value', l('field_with_photo', 'listings', '', 'text', array()));
 ?>
					<?php echo tpl_function_checkbox(array('input' => 'filters[with_photo]','id' => 'with_photo_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['with_photo_value'],'selected' => $this->_vars['data']['with_photo']), $this);?>
					<script>checkboxes_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('with_photo_select_<?php echo $this->_vars['form_settings']['rand']; ?>
');</script>
					<?php endif; ?>
					
					<?php 
$this->assign('open_house_value', l('field_open_house', 'listings', '', 'text', array()));
 ?>
					<?php echo tpl_function_checkbox(array('input' => 'filters[by_open_house]','id' => 'open_house_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['open_house_value'],'selected' => $this->_vars['data']['open_house']), $this);?>
					<script>checkboxes_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('open_house_select_<?php echo $this->_vars['form_settings']['rand']; ?>
');</script>
					
					<?php 
$this->assign('by_private_value', l('field_by_private', 'listings', '', 'text', array()));
 ?>
					<?php echo tpl_function_checkbox(array('input' => 'filters[by_private]','id' => 'by_private_select_'.$this->_vars['form_settings']['rand'],'value' => $this->_vars['by_private_value'],'selected' => $this->_vars['data']['by_private']), $this);?>
					<script> checkboxes_<?php echo $this->_vars['form_settings']['rand']; ?>
.push('by_private_select_<?php echo $this->_vars['form_settings']['rand']; ?>
'); </script>
				</div>-->
				<?php endif; ?>
			</div>
			<input type="hidden" name="filters[type]" value="<?php echo $this->_run_modifier($this->_vars['form_settings']['object'], 'escape', 'plugin', 1); ?>
">
			<input type="hidden" name="form" value="main_search_form">
			<input type="hidden" name="new" value="1">
            
           <!-- <div class="clr"></div>
            <div class="btn-block" style="bottom:20px;">
			<div id="search-preresult-<?php echo $this->_vars['form_settings']['rand']; ?>
" class="preload"></div>
			<div class="btn-link search-btn fright">
				<ins class="fa fa-search w fa-lg hover"></ins>
				<input type="submit" name="search_button" value="<?php echo l('btn_search', 'start', '', 'button', array()); ?>" id="main_search_button_<?php echo $this->_vars['form_settings']['rand']; ?>
">
			</div>
			<div class="clr">
            </div>-->

<!--			<a href="#" id="more-options-link-<?php echo $this->_vars['form_settings']['rand']; ?>
" <?php if ($this->_vars['listings_page_data']['type'] != 'short'): ?>class="hide"<?php endif; ?>><?php echo l('link_more_options', 'start', '', 'text', array()); ?></a>
			<a href="#" id="less-options-link-<?php echo $this->_vars['form_settings']['rand']; ?>
" <?php if ($this->_vars['listings_page_data']['type'] == 'short'): ?>class="hide"<?php endif; ?>><?php echo l('link_less_options', 'start', '', 'text', array()); ?></a>-->
		</div>
		</div>
	<?php endif; ?>
	</div>
</div>
<input type="hidden" value="<?php echo $this->_vars['form_settings']['object']; ?>
" id="txtListingType" />
<script><?php echo '
var lstType = $("#txtListingType").val();
if(lstType == \'sale\')
{
	$("#ShowCustomlisting_sale").css("display", "block");
	$("#ShowCustomlisting_rent").css("display", "none");
}
else if(lstType == \'rent\')
{
	$("#ShowCustomlisting_rent").css("display", "block");
	$("#ShowCustomlisting_sale").css("display", "none");
}'; ?>

</script>


</form>
<script><?php echo '
	$(function(){
		if(selectObject_';  echo $this->_vars['form_settings']['rand'];  echo ') selectObject_';  echo $this->_vars['form_settings']['rand'];  echo '.clear();
		selectObject_';  echo $this->_vars['form_settings']['rand'];  echo ' = new selectBox({elementsIDs: selects_';  echo $this->_vars['form_settings']['rand'];  echo ', selectDropdownClass: \'dropdown\'});
		checkboxObject_';  echo $this->_vars['form_settings']['rand'];  echo ' = new checkBox({elementsIDs: checkboxes_';  echo $this->_vars['form_settings']['rand'];  echo '});
		if(formSelectObject_';  echo $this->_vars['form_settings']['rand'];  echo ') formSelectObject_';  echo $this->_vars['form_settings']['rand'];  echo '.clear();
	formSelectObject_';  echo $this->_vars['form_settings']['rand'];  echo ' = new selectBox({elementsIDs: form_selects_';  echo $this->_vars['form_settings']['rand'];  echo ', selectDropdownClass: \'dropdown\'});
		formCheckboxObject_';  echo $this->_vars['form_settings']['rand'];  echo ' = new checkBox({elementsIDs: form_checkboxes_';  echo $this->_vars['form_settings']['rand'];  echo '});
		
		$(\'#main_search_form_';  echo $this->_vars['form_settings']['rand'];  echo '\').bind(\'submit\', function(){
			var url = \'';  echo $this->_vars['site_root'];  echo '\' + \'listings/ajax_search\';
			$.post(url, $(this).serialize(), function(data){
				window.location.href = data;
			});
			return false;
		});
	});
'; ?>
</script>
<script><?php echo '
	$("#reset3").on("click", function () {
    //window.location.reload();
     $(".label").empty();
	$("#listid").attr(\'value\', \'\');
	$("#pricemin").attr(\'value\', \'\');
	$("#pricemax").attr(\'value\', \'\');
	$(".dropdown ul li.active label input").filter(\':checkbox\').removeAttr(\'checked\');
	$(".dropdown ul li.active").removeClass("active");
	$(\'#date_start';  echo $this->_vars['form_settings']['rand'];  echo '\').removeAttr(\'value\');
	$(\'#date_end';  echo $this->_vars['form_settings']['rand'];  echo '\').removeAttr(\'value\');
	});
'; ?>
</script>
<script><?php echo '
	$(function() {
	$(\'#pricemin, #pricemax, #listid\').keydown(function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
		e.preventDefault();
        e.stopPropagation();
        $(\'#main_search_form_';  echo $this->_vars['form_settings']['rand'];  echo '\').submit();
		}
    });
    $("input[type=\'reset\']").hover(function(){
        $(".fa.w.hover").css(\'color\', \'#fff\');
    });
	$(document).keypress(function (e) {
	if(($(\'#country_text_';  echo $this->_vars['country_helper_data']['rand'];  echo '\').val().length > 0)){
      if (e.which == 13) {
      $(\'#main_search_form_';  echo $this->_vars['form_settings']['rand'];  echo '\').submit();
    }
	}
	if($(\'#listid\').val().length > 0){
      if (e.which == 13) {
      $(\'#main_search_form_';  echo $this->_vars['form_settings']['rand'];  echo '\').submit();
    }
	}
	if(($(\'#pricemin\').val().length > 0)){
      if (e.which == 13) {
      $(\'#main_search_form_';  echo $this->_vars['form_settings']['rand'];  echo '\').submit();
    }
	}
	if(($(\'#pricemax\').val().length > 0)){
      if (e.which == 13) {
      $(\'#main_search_form_';  echo $this->_vars['form_settings']['rand'];  echo '\').submit();
    }
	}
  });
})
'; ?>
</script>
<script><?php echo '
	$(function() {
	$("#listid").attr(\'value\', \'\');
	$("#pricemin").attr(\'value\', \'\');
	$("#pricemax").attr(\'value\', \'\');
    $("input[type=\'submit\']").hover(function(){
      $(".fa.w.hover").css(\'color\',\'#3D95CB\');
    });
	 $("#pricemin, #pricemax, #listid").keypress(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
      }
     });
})
'; ?>
</script>


<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-30 09:36:35 India Daylight Time */ ?>

<?php 
$this->assign('user_listings', l('link_more', 'listings', '', 'text', array('user_name'=>$this->_vars['user']['output_name'])));
 ?>
<a <?php if ($this->_vars['is_listings']): ?>href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'user','id_user' => $this->_vars['user_id'],'user' => $this->_vars['user_name']), $this); endif; ?>" class="btn-link link-r-margin" title="<?php echo $this->_vars['user_listings']; ?>
"><ins class="fa fa-listings <?php if (! $this->_vars['is_listings']): ?>g<?php else: ?>hover<?php endif; ?> fa-lg edge"><ins class="fa fa-file-o"></ins><ins class="fa fa-file-text-o"></ins></ins><?php echo $this->_vars['custStr']; ?>
</a>
 
<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-07 13:18:14 India Daylight Time */ ?>

<?php echo tpl_function_js(array('module' => listings,'file' => 'mortgage-calc.js'), $this);?>
<div class="mortgage_calc_block edit_form">
	<div class="inside">
		<h2><?php echo l('mortgage_calc_title', 'listings', '', 'text', array()); ?></h2>
		<form method="POST">
			<div class="r">
				<label>1.&nbsp;<?php echo l('mortgage_calc_query1', 'listings', '', 'text', array()); ?>&nbsp;*</label>
				<input type="text" name="principal" size="15">
			</div>
			<div class="r">
				<label>2.&nbsp;<?php echo l('mortgage_calc_query2', 'listings', '', 'text', array()); ?>&nbsp;*</label>
				<input type="text" name="intRate" size="15">
			</div>
			<div class="r">
				<label>3.&nbsp;<?php echo l('mortgage_calc_query3', 'listings', '', 'text', array()); ?>&nbsp;*</label>
				<input type="text" name="numYears" size="15">
			</div>
			<div class="r">
				<input type="button" value="<?php echo l('mortgage_calc_compute', 'listings', '', 'button', array()); ?>" id="calc_btn">
				<input type="button" value="<?php echo l('mortgage_calc_reset', 'listings', '', 'button', array()); ?>" id="empty_btn">
			</div>
			<div class="r">
				<label>4.&nbsp;<?php echo l('mortgage_calc_query4', 'listings', '', 'text', array()); ?>&nbsp;*</label>
				<input type="text" name="moPmt" size="15" dir="ltr">
				<input type="hidden" name="HmoPmt" value=0 size="15">
			</div>
			<p><?php echo l('mortgage_calc_create', 'listings', '', 'text', array()); ?></p>
			<p><input type="button" value="<?php echo l('mortgage_calc_createbutton', 'listings', '', 'button', array()); ?>" id="results_btn"></p>
			<p class="subtext"><?php echo l('mortgage_calc_content2', 'listings', '', 'button', array()); ?></p>
		</form>
	</div>
</div>
<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'currency')); tpl_block_capture(array('assign' => 'currency'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
	<?php echo tpl_function_block(array('name' => 'currency_output','module' => 'start','value' => '%s'), $this); $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
<script><?php echo '
	$(function(){
		new mortgageCalc({
			siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
			selectedCurrAbbr: \'';  echo $this->_run_modifier($this->_vars['currency'], 'strip_tags', 'PHP', 1);  echo '\',
		});
	});
'; ?>
</script>

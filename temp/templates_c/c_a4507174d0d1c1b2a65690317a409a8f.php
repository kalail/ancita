<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.user_select.php'); $this->register_function("user_select", "tpl_function_user_select");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-11-03 06:19:19 India Standard Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>
<div class="menu-level2">
	<ul>
		<li<?php if ($this->_vars['tab_id'] == 'XML'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/import/import_listings_alphashare/XML">XML Alphashare API</a></div></li>
        <li<?php if ($this->_vars['tab_id'] == 'XPath'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/import/import_listings_alphashare/XPath">XPath conversion rules</a></div></li>
        <li<?php if ($this->_vars['tab_id'] == 'Field'): ?> class="active"<?php endif; ?>><div class="l"><a href="<?php echo $this->_vars['site_url']; ?>
admin/import/import_listings_alphashare/Field">Field values conversion</a></div></li>
	</ul>
	&nbsp;
</div>
<div class="actions">&nbsp;</div>
<form method="post" name="save_form" action="<?php echo $this->_vars['site_url']; ?>
admin/import/import_listings_alphashare/<?php echo $this->_vars['tab_id']; ?>
" enctype="multipart/form-data">
<?php if ($this->_vars['tab_id'] == 'XML'): ?>
<div class="edit-form edit-form-marketing">
<div class="row header">Import from XML Alphashare format</div>
<div class="row">You can populate your database importing a xml file with properties. The file should support required format.</div>
<div class="row">
    <div class="h">Choose user for XML import</div>
    <div class="v"><?php echo tpl_function_user_select(array('selected' => $this->_vars['id_user'],'max' => 1,'var_name' => 'id_user'), $this);?>&nbsp;</div>
</div>
<div class="row">
	<div class="h">Import from *.xml:</div>
    <div class="v">
    	<div class="fileinputs" style="display: inline;">
        	<input type="file" name="db_file" id="db_file" style="cursor: pointer;" class="file" accept=".xml" onchange="document.getElementById('file_text_db_file').innerHTML = this.value.replace(/.*\\(.*)/, '$1');document.getElementById('file_img_db_file').innerHTML = ChangeIcon(this.value.replace(/.*\.(.*)/, '$1'));"/>
        <div class="fakefile" style="cursor: pointer">
            <table cellpadding="0" cellspacing="0">
            <tr>
                <td>	
                    <a class="admin_button_major" style="cursor: pointer"><span>&nbsp;</span>Choose</a>
                </td>												
                <td style="padding-left:10px;">
                    <span id='file_img_db_file'></span>
                </td>								
                <td style="padding-left:4px;">
                    <span id='file_text_db_file'></span>	
                </td>
            </tr>
            </table>
    	</div>
	
</div>
</div>
<div class="row">
	<div class="h"><div class="btn"><div class="l"><input type="submit" name="save_load_xml" value="Load"></div></div></div>
    <div class="v">
    </div>
</div>
<div style="overflow:scroll">
<?php echo $this->_vars['strTempResult']; ?>

</div>
</div>
</div>
<?php endif; ?>
<?php if ($this->_vars['tab_id'] == 'XPath'): ?>
<div class="edit-form edit-form-marketing">
<div class="row header">XPath conversion rules settings</div>
<div class="row">
<table>
    <tr>
        <td valign="top">
        	<label><b>XPath rules:&nbsp;</b></label>
        </td>
        <td valign="top" style="padding-left:5px">
        	<label><b>Default values:&nbsp;</b></label>
        </td>
    </tr>
    <tr>
        <td valign="top">
            <textarea id="content" rows="27" name="active_rule" style="width:400px; font-size:13px"><?php echo $this->_vars['rules']['active_rule']; ?>
</textarea>
        </td>
        <td valign="top" style="padding-left:5px">
        	<pre><?php echo $this->_vars['rules']['default_rule']; ?>
</pre>
        </td>
    </tr>
    <tr>
        <td>
        	<div class="btn"><div class="l"><input type="submit" name="save_settings_thinkspain" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
        </td>
    </tr>
</table>
</div>
</div>
<?php endif; ?>
<?php if ($this->_vars['tab_id'] == 'Field'): ?>
<div class="edit-form edit-form-marketing">
<div class="row header">Add new rule</div>
<div class="row zebra">
<table style="margin-left:5px;">
	<tr>
    	<td width="25%">Name of the Ancita field</td>
        <td>
        	<select name="ancita_field">
                    <option value="0">Please make a choice of the  ancita field</option>
                    <option value="country">Country</option>
                    <option value="region">Region</option>
                    <option value="city">City</option>
                    <option value="property_type">Property type</option>
                    <option value="sale_type">Operation type</option>
            </select>
        </td>
    </tr>
    <tr>
    	<td>Node value in the XML file</td>
        <td><input type="text" id="content" size="30" name="xml_value" value=""></td>
    </tr>
    <tr>
    	<td>Ancita replacement value</td>
        <td><input type="text" id="content" size="30" name="ancita_value" value=""></td>
    </tr>
    <tr>
    	<td></td>
    	<td>
        	<div class="btn"><div class="l"><input type="submit" name="save_settings_rule_thinkspain" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>"></div></div>
        </td>
    </tr>
</table>
</div>
</div>
<div class="edit-form edit-form-marketing">
<div class="row header">Conversion rules for XML Thinkspain node values</div>
<div class="row">
<table cellpadding="0" cellspacing="1" width="100%" border="0" class="table_main">
    <tr class="table_header">
    <td class="main_header_text" width="200" align="center">Ancita field</td>
    <td class="main_header_text" width="200" align="center">
        Node value in XML file
    </td>
    <td class="main_header_text" align="center">
        Ancita replacement value
    </td>
    
    <td width="10%" class="main_header_text" align="center"><?php echo $this->_vars['lang']['content']['edit_header']; ?>
</td>
    </tr>
    <?php if ($this->_vars['rules_values']): ?>
    <?php if (is_array($this->_vars['rules_values']) and count((array)$this->_vars['rules_values'])): foreach ((array)$this->_vars['rules_values'] as $this->_vars['key'] => $this->_vars['item']): ?>
    <tr>
        <td class="main_content_text" align="center"><?php echo $this->_vars['item']['ancita_field']; ?>
</td>
        <td class="main_content_text" align="center"><?php echo $this->_vars['item']['xml_value']; ?>
</td>
        <td class="main_content_text" align="center"><?php echo $this->_vars['item']['ancita_value']; ?>
</td>
        <td class="main_content_text" align="center" nowrap>
            <a href="<?php echo $this->_vars['site_url']; ?>
admin/import/import_listings_alphashare/<?php echo $this->_vars['tab_id']; ?>
/delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('Are you sure you want to delete this rule?')) return false;"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-delete.png" width="16" height="16" border="0" alt="<?php echo l('link_delete_data', 'import', '', 'button', array()); ?>" title="<?php echo l('link_delete_data', 'import', '', 'button', array()); ?>"></a>
        </td>
    </tr>
    <?php endforeach; endif; ?>
    <?php else: ?>
    <tr>
        <td colspan="5" class="error" align="center">Rule is empty</td>
    </tr>
    <?php endif; ?>						
</table>
</div>
</div>
<?php endif; ?>
</form>
<?php echo '
<script>
$("#approve_all").change(function () {
    $(".approve").prop(\'checked\', $(this).prop("checked"));
});

function ImportListing(){
	var nuser = $("input[name=id_user]").val();
	var jd = 0;
	var selectedRadioItems = new Array();
	$("input[type=radio]:checked").filter(\'.myradio\').each(function() {
		jd = $(this).val();
		selectedRadioItems.push(jd);
	});
	var selectedRadioItem = selectedRadioItems[0];

	if(selectedRadioItem == 1000) {
		var response = confirm(\'Are you sure overwrite all the existing records?\');
	}
	if(selectedRadioItem == 1001) {
		var response = confirm(\'Are you sure skip update of existing records?\');
	}	
	if(selectedRadioItem == 1002) {
		var response = confirm(\'Are you sure do not overwrite existing records with blank data?\');
	}
	if(response != false) {
		var selectedCheckboxItems = new Array();
		var id = 0;
		$("input[type=checkbox]:checked").each(function() {
			id = $(this).val();
			if(!isNaN(id)) selectedCheckboxItems.push(id);
		});
		
		var selectedIdCheckboxItem1 = $(\'#mycheckbox1\').prop(\'checked\');
		var selectedIdCheckboxItem2 = $(\'#mycheckbox2\').prop(\'checked\');
		
		$.ajax({
			url: \'';  echo $this->_vars['site_root'];  echo 'admin/import/ajax_import_xml_listing\',
			dataType: \'html\',
			type: \'POST\',
			data: {api : \'alphashare\',listing_ids : selectedCheckboxItems.join(\'|\'), skip_ids : selectedRadioItem, check1 : selectedIdCheckboxItem1, check2 : selectedIdCheckboxItem2, nuser : nuser},
			cache: false,
			success: function(data){
				if(data){
					$(\'#import_status\').html(data);
				}else{
					$(\'#import_status\').html(\'\');
				}
			}
		});
	}
}

function ChangeIcon(type){	
    switch (type.toLowerCase())
    {
        case \'bmp\':         
        case \'jpg\':  
        case \'png\':
        case \'gif\':
        case \'tiff\':
        type = type.toLowerCase();
        break;
        case \'jpeg\':
        	type = \'jpg\';
        	break;
        case \'tif\':
        	type = \'tiff\';
        	break;
        case \'mp3\':
        case \'wav\':
        case \'ogg\':
        	type = \'mp3\';
         break;
        case \'avi\':
        case \'wmv\':
        case \'flv\':
        	type = \'avi\';
        	 break;
       	case \'csv\':
        	type = \'csv\';
        	 break;
        case \'zip\':
        	type = \'zip\';
        	 break; 
        default: type = \'other\'; break;
    };   
    return "<img src=\'';  echo $this->_vars['site_root'];  echo $this->_vars['img_folder'];  echo '" + type +".png\'>";
}
</script>
<style>
.table_main tr {
    background-color: #fff;
}
.table_header {
    background-color: #29b43d;
    height: 25px;
}
.table_header {
    background-color: #fefed6;
}
.table_header TD {
    color: #fff;
    font-weight: bold;
    padding: 0 10px;
}
.table_main td {
    vertical-align: middle;
}
.main_header_text {
    background: #29b43d none repeat scroll 0 0;
    color: #fff;
    font-family: Trebuchet MS,Arial;
    font-size: 13px;
    font-weight: bold;
}
div.fileinputs {
	position: relative;
}
div.fakefile {
	position: absolute;
	top: 0;
	left: 0;
	z-index: 1;

}
input.file {
	position: relative;
	text-align: left;
	-moz-opacity:0 ;
	filter:alpha(opacity: 0);
	opacity: 0;
	z-index: 2;
	margin-left: -115px;
	margin-top: 10px;
	cursor: pointer;
}
.admin_button_major {
    font: 13px Trebuchet MS, Arial;
	text-decoration: none;
	color: #ffffff !important;
	position: relative;
	background: url(\'';  echo $this->_vars['site_root'];  echo $this->_vars['img_folder'];  echo 'btn_green.png\') 0 -31px no-repeat;
	padding: 7px 17px 7px 15px;
	height: 17px;
	margin: -6px 10px 5px 0;
	/*display:inline-block;*/
}
</style>
'; ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

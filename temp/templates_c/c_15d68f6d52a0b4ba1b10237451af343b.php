<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.lower.php'); $this->register_modifier("lower", "tpl_modifier_lower");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-07 13:20:03 India Daylight Time */ ?>

<?php echo l('sort_by', $this->_vars['sort_module'], '', 'text', array()); ?>:
<select id="sorter-select-<?php echo $this->_vars['sort_rand']; ?>
">
<?php if (is_array($this->_vars['sort_links']) and count((array)$this->_vars['sort_links'])): foreach ((array)$this->_vars['sort_links'] as $this->_vars['key'] => $this->_vars['item']): ?>
<option value="<?php echo $this->_vars['key']; ?>
"<?php if ($this->_vars['key'] == $this->_vars['sort_order']): ?> selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
<?php endforeach; endif; ?>
</select>
<button id="sorter-dir-<?php echo $this->_vars['sort_rand']; ?>
" name="sorter_btn" class="sorter-btn"><ins class="fa fa-arrow-<?php if ($this->_run_modifier($this->_vars['sort_direction'], 'lower', 'plugin', 1) == 'asc'): ?>up<?php else: ?>down<?php endif; ?>"></ins></button>

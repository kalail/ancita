<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-03-30 09:36:33 India Daylight Time */ ?>

	<?php echo tpl_function_js(array('file' => 'jcarousellite.min.js'), $this);?>
	<?php echo tpl_function_js(array('file' => 'init_carousel_controls.js'), $this);?>
	<script><?php echo '
		$(function(){
			var rtl = \'rtl\' === \'';  echo $this->_vars['_LANG']['rtl'];  echo '\';
			var idPrev, idNext;
			if(!rtl){
				idNext = \'#directionright';  echo $this->_vars['similar_rand'];  echo '\';
				idPrev = \'#directionleft';  echo $this->_vars['similar_rand'];  echo '\';
			}else{
				idNext = \'#directionleft';  echo $this->_vars['similar_rand'];  echo '\';
				idPrev = \'#directionright';  echo $this->_vars['similar_rand'];  echo '\';
			};
			$(\'.carousel_block';  echo $this->_vars['similar_rand'];  echo '\').jCarouselLite({
				rtl: rtl,
				visible: ';  echo $this->_vars['similar_visible'];  echo ',
				btnNext: idNext,
				btnPrev: idPrev,
				circular: false,
				afterEnd: function(a){
					var index = $(a[0]).index();
					carousel_controls';  echo $this->_vars['similar_rand'];  echo '.update_controls(index);
				}
			});

			carousel_controls';  echo $this->_vars['similar_rand'];  echo ' = new init_carousel_controls({
				rtl: rtl,
				carousel_images_count: ';  echo $this->_vars['similar_visible'];  echo ',
				carousel_total_images: ';  echo $this->_vars['similar_total'];  echo ',
				btnNext: idNext,
				btnPrev: idPrev
			});
		});
	</script>'; ?>

	<div id="similar_listings" class="noPrint">
		<h2><?php echo l('header_similar_listings', 'listings', '', 'text', array()); ?></h2>
		<div class="carousel <?php if ($this->_vars['similar_total'] <= $this->_vars['similar_visible']): ?>visible<?php endif; ?>">
			<div id="directionleft<?php echo $this->_vars['similar_rand']; ?>
" class="directionleft">
				<div class="fa fa-arrow-left w fa-lg edge hover" id="l_hover"></div>
			</div>
			<div class="carousel_block carousel_block<?php echo $this->_vars['similar_rand']; ?>
 item_<?php echo $this->_vars['similar_visible']; ?>
_info">
				<ul>
					<?php if (is_array($this->_vars['similar_listings']) and count((array)$this->_vars['similar_listings'])): foreach ((array)$this->_vars['similar_listings'] as $this->_vars['key'] => $this->_vars['item']): ?>
					<li <?php if ($this->_vars['key'] >= $this->_vars['similar_visible']): ?>class=""<?php endif; ?>>
						<div class="listing">
							<?php 
$this->assign('logo_title', l('link_listing_view', 'listings', '', 'button', array('id_ref'=>$this->_vars['item']['id'])));
 ?>
							<?php 
$this->assign('text_listing_logo', l('text_listing_logo', 'listings', '', 'button', array('id_ref'=>$this->_vars['item']['id'],'property_type'=>$this->_vars['item']['property_type_str'],'operation_type'=>$this->_vars['item']['operation_type_str'],'location'=>$this->_vars['item']['location'])));
 ?>
							<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['middle']; ?>
" alt="<?php echo $this->_vars['text_listing_logo']; ?>
" title="<?php echo $this->_vars['logo_title']; ?>
" /></a>
							<!--<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>" title="<?php echo $this->_vars['item']['output_name']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 24); ?>
</a>-->
                            <a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>" title="<?php echo $this->_vars['item']['city']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['city'], 'truncate', 'plugin', 1, 24); ?>
</a>
							<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'property_type')); tpl_block_capture(array('assign' => 'property_type'), null, $this); ob_start();  echo $this->_vars['item']['property_type_str'];  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?><span title="<?php echo $this->_vars['property_type']; ?>
"><?php echo $this->_run_modifier($this->_vars['property_type'], 'truncate', 'plugin', 1, 24); ?>
</span>
							<?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item'],'template' => 'small'), $this);?>
						</div>
					</li>
					<?php endforeach; endif; ?>
				</ul>
			</div>
			<div id="directionright<?php echo $this->_vars['similar_rand']; ?>
" class="directionright">
				<div class="fa fa-arrow-right w fa-lg edge hover" id="r_hover"></div>
			</div>
			<div class="clr"></div>
		</div>
	</div>
	

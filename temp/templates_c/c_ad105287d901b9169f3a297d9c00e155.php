<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-11 05:53:16 India Daylight Time */ ?>

		</div>
	</div>	
<?php echo tpl_function_helper(array('func_name' => show_banner_place,'module' => banners,'func_param' => 'bottom-banner'), $this);?>
	<footer class="footer">
		<div class="content">
			<?php echo tpl_function_menu(array('gid' => 'user_footer_menu'), $this);?>
			<div class="copyright"><?php echo $this->_vars['logo_settings']['footerText']; ?>
</div>
		</div>
	</footer>
	<?php echo tpl_function_helper(array('func_name' => lang_editor,'module' => languages), $this);?>
	<?php echo tpl_function_helper(array('func_name' => seo_traker,'helper_name' => seo_module,'module' => seo,'func_param' => 'footer'), $this);?>
</body>
</html>

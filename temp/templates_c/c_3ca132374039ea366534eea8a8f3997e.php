<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.truncate.php'); $this->register_modifier("truncate", "tpl_modifier_truncate");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.lower.php'); $this->register_modifier("lower", "tpl_modifier_lower");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-01-19 06:59:36 India Standard Time */ ?>

	<div>
		<table class="list">
		<tr id="sorter_block">
			<th><a href="<?php echo $this->_vars['sort_links']['name']; ?>
" class="link-sorter"><?php echo l('field_name', 'users', '', 'text', array());  if ($this->_vars['page_data']['order'] == 'name'): ?><ins class="fa fa-arrow-<?php if ($this->_run_modifier($this->_vars['page_data']['order_direction'], 'lower', 'plugin', 1) == 'asc'): ?>up<?php else: ?>down<?php endif; ?>"></ins><?php endif; ?></a></th>
			<th><a href="<?php echo $this->_vars['sort_links']['email']; ?>
" class="link-sorter"><?php echo l('field_email', 'users', '', 'text', array());  if ($this->_vars['page_data']['order'] == 'email'): ?><ins class="fa fa-arrow-<?php if ($this->_run_modifier($this->_vars['page_data']['order_direction'], 'lower', 'plugin', 1) == 'asc'): ?>up<?php else: ?>down<?php endif; ?>"></ins><?php endif; ?></a></th>
			<th><a href="<?php echo $this->_vars['sort_links']['phone']; ?>
" class="link-sorter"><?php echo l('field_phone', 'users', '', 'text', array());  if ($this->_vars['page_data']['order'] == 'phone'): ?><ins class="fa fa-arrow-<?php if ($this->_run_modifier($this->_vars['page_data']['order_direction'], 'lower', 'plugin', 1) == 'asc'): ?>up<?php else: ?>down<?php endif; ?>"></ins><?php endif; ?></a></th>
			<th><a href="<?php echo $this->_vars['sort_links']['agent_date']; ?>
" class="link-sorter"><?php echo l('field_agent_date', 'users', '', 'text', array());  if ($this->_vars['page_data']['order'] == 'agent_date'): ?><ins class="fa fa-arrow-<?php if ($this->_run_modifier($this->_vars['page_data']['order_direction'], 'lower', 'plugin', 1) == 'asc'): ?>up<?php else: ?>down<?php endif; ?>"></ins><?php endif; ?></a></th>		
			<th class="w100">&nbsp;</th>		
		</tr>
		<?php if (is_array($this->_vars['agents']) and count((array)$this->_vars['agents'])): foreach ((array)$this->_vars['agents'] as $this->_vars['item']): ?>
		<tr>
			<td><a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 100); ?>
</a></td>
			<td><?php echo $this->_run_modifier($this->_vars['item']['contact_email'], 'truncate', 'plugin', 1, 50); ?>
</td>
			<td><?php echo $this->_vars['item']['contact_phone']; ?>
</td>
			<td><?php if ($this->_run_modifier($this->_vars['item']['agent_date'], 'strtotime', 'PHP', 1) > 0):  echo $this->_run_modifier($this->_vars['item']['agent_date'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']);  else: ?>-<?php endif; ?></td>
			<td>
				<?php if ($this->_vars['item']['agent_status']): ?>
				<a href="<?php echo $this->_vars['site_url']; ?>
users/my_agents_delete/<?php echo $this->_vars['item']['id']; ?>
" onclick="javascript: if(!confirm('<?php echo l('note_agent_delete', 'users', '', 'js', array()); ?>')) return false;" class="btn-link fright" alt="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>" title="<?php echo l('btn_delete', 'start', '', 'button', array()); ?>"><ins class="fa fa-trash-o fa-lg edge hover"></ins></a>
				<?php else: ?>
				<a href="<?php echo $this->_vars['site_url']; ?>
users/my_agents_request/decline/<?php echo $this->_vars['item']['id']; ?>
" class="btn-link fright" alt="<?php echo l('btn_decline', 'start', '', 'button', array()); ?>" title="<?php echo l('btn_decline', 'start', '', 'button', array()); ?>"><ins class="fa fa-minus fa-lg edge hover"></ins></a>
				<a href="<?php echo $this->_vars['site_url']; ?>
users/my_agents_request/approve/<?php echo $this->_vars['item']['id']; ?>
" class="btn-link fright" alt="<?php echo l('btn_approve', 'start', '', 'button', array()); ?>" title="<?php echo l('btn_approve', 'start', '', 'button', array()); ?>"><ins class="fa fa-check fa-lg edge hover"></ins></a>				
				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; endif; ?>
		</table>	
	</div>
	<div id="pages_block_2"><?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?></div>


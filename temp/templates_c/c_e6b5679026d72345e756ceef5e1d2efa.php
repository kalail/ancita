<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.sorter.php'); $this->register_function("sorter", "tpl_function_sorter");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-10-21 08:50:29 India Daylight Time */ ?>

	<?php if ($this->_vars['users']): ?>
	<div class="sorter line" id="sorter_block">
		<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
		<div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div>
	</div>
	<?php endif; ?>

	<div>
		<?php if (is_array($this->_vars['users']) and count((array)$this->_vars['users'])): foreach ((array)$this->_vars['users'] as $this->_vars['item']): ?>
		<?php $this->assign('current_user_id', $this->_vars['item']['id']); ?>
		<div class="user-block">
			<div id="item-block-<?php echo $this->_vars['item']['id']; ?>
" class="item user <?php if ($this->_vars['item']['is_highlight']): ?>highlight<?php endif; ?>">
			
			<?php if ($this->_vars['item']['user_type'] == 'company'): ?><div itemscope itemtype="http://data-vocabulary.org/Organization"><?php endif; ?>
			
			<h3><a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']), $this);?>" <?php if ($this->_vars['item']['user_type'] == 'company'): ?>itemprop="name"<?php endif; ?>><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a>, <?php echo l($this->_vars['item']['user_type'], 'users', '', 'text', array()); ?></h3>
			<div class="image">
				<?php 
$this->assign('text_user_logo', l('text_user_logo', 'users', '', 'button', array('user_name'=>$this->_vars['item']['output_name'])));
 ?>
				<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']), $this);?>">
					<img src="<?php echo $this->_vars['item']['media']['user_logo']['thumbs']['middle']; ?>
" alt="<?php echo $this->_vars['text_user_logo']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
">
				</a>
			</div>
			<div class="body">
				<?php 
$this->assign('no_info_str', l('no_information', 'start', '', 'text', array()));
 ?>
				<?php if ($this->_vars['user_type'] == "agent" && $this->_vars['item']['company']): ?><h3><?php echo $this->_vars['item']['company']['output_name']; ?>
</h3><?php endif; ?>
				<?php if ($this->_vars['item']['is_contact'] && $this->_vars['item']['contact_phone']): ?><h3 <?php if ($this->_vars['item']['user_type'] == 'company'): ?>itemprop="tel"<?php endif; ?>><?php echo $this->_vars['item']['contact_phone']; ?>
</h3><?php endif; ?>	
				<div class="t-1">	
				
				</div>
				<div class="t-2">
					<span class="status_text"><?php if ($this->_vars['item']['is_featured']):  echo l('status_featured', 'users', '', 'text', array());  endif; ?></span>
				</div>		
				
				<div class="t-4">
										<?php echo tpl_function_block(array('name' => get_rate_block,'module' => reviews,'rating_data_main' => $this->_vars['item']['review_value'],'type_gid' => 'users_object','template' => 'mini','read_only' => 'true'), $this);?><br>
					<span><?php echo l('field_rate', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['item']['review_value']; ?>
<br>
					<span><?php echo l('field_reviews_count', 'users', '', 'text', array()); ?>:</span> <?php echo $this->_vars['item']['review_count']; ?>
<br>
									</div>
			</div>
			
			<?php if ($this->_vars['item']['user_type'] == 'company'): ?></div><?php endif; ?>
			
			<div class="clr"></div>
			<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo l('link_details', 'users', '', 'text', array()); ?></a>
		</div>
	</div>
	<?php endforeach; else: ?>
	<div class="item empty"><?php echo l('no_users', 'users', '', 'text', array()); ?></div>
	<?php endif; ?>
	</div>
	<?php if ($this->_vars['users']): ?><div id="pages_block_2"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?></div><?php endif; ?>

	<?php if ($this->_vars['update_map']): ?>
	<?php echo tpl_function_block(array('name' => update_default_map,'module' => geomap,'markers' => $this->_vars['markers'],'map_id' => 'users_map_container'), $this);?>
	<?php endif; ?>
	
	<script><?php echo '
	$(function(){
		$(\'#total_rows\').html(\'';  echo $this->_vars['page_data']['total_rows'];  echo '\');
	});
	'; ?>
</script>

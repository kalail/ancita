<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-07 13:20:03 India Daylight Time */ ?>

<?php if ($this->_run_modifier($this->_vars['current_search_data'], 'count', 'PHP', 1)): ?>
<ul>
     <?php if ($this->_vars['id_category'] != ''): ?>
     <li>Category: <a><?php if ($this->_vars['id_category'] == '1'): ?>Residential<?php endif;  if ($this->_vars['id_category'] == '2'): ?>Commercial<?php endif;  if ($this->_vars['id_category'] == '3'): ?>Lots-lands<?php endif; ?></a></li>
     <?php endif; ?>
	<?php if (is_array($this->_vars['current_search_data']) and count((array)$this->_vars['current_search_data'])): foreach ((array)$this->_vars['current_search_data'] as $this->_vars['key'] => $this->_vars['item']): ?>
    <?php if ($this->_vars['key'] != 'id_category'): ?>
	<li title="<?php echo $this->_run_modifier($this->_vars['item']['name'], 'escape', 'plugin', 1); ?>
: <?php echo $this->_run_modifier($this->_vars['item']['label'], 'escape', 'plugin', 1); ?>
" <?php if ($this->_vars['key'] == 'price'): ?>dir="ltr"<?php endif; ?>><?php echo $this->_run_modifier($this->_vars['item']['name'], 'truncate', 'plugin', 1, 10, '...', true); ?>
: <?php echo $this->_run_modifier($this->_vars['item']['label'], 'truncate', 'plugin', 1, 12, '...', true); ?>
 <a href="<?php echo $this->_vars['site_url']; ?>
listings/delete_search_criteria/<?php echo $this->_run_modifier($this->_vars['key'], 'escape', 'plugin', 1); ?>
" class="fright"></a></li>
    <?php endif; ?>
	<?php endforeach; endif; ?>
</ul>
<?php endif; ?>

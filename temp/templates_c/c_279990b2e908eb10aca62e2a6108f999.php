<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-11 05:53:11 India Daylight Time */ ?>

<div class="region-box">
	<button class="search-btn" id="country_open_<?php echo $this->_vars['country_helper_data']['rand']; ?>
" name="submit"><ins class="fa fa-search"></ins></button>
	<span>
    <input type="text" name="region_name" class="countrysel" id="country_text_<?php echo $this->_vars['country_helper_data']['rand']; ?>
" autocomplete="off" value="<?php echo $this->_vars['country_helper_data']['location_text']; ?>
"></span>
	<input type="hidden" name="<?php echo $this->_vars['country_helper_data']['var_country_name']; ?>
" id="country_hidden_<?php echo $this->_vars['country_helper_data']['rand']; ?>
" value="<?php echo $this->_vars['country_helper_data']['country']['code']; ?>
">
	<input type="hidden" name="<?php echo $this->_vars['country_helper_data']['var_region_name']; ?>
" id="region_hidden_<?php echo $this->_vars['country_helper_data']['rand']; ?>
" value="<?php echo $this->_vars['country_helper_data']['region']['id']; ?>
">
	<input type="hidden" name="<?php echo $this->_vars['country_helper_data']['var_city_name']; ?>
" id="city_hidden_<?php echo $this->_vars['country_helper_data']['rand']; ?>
" value="<?php echo $this->_vars['country_helper_data']['city']['id']; ?>
">
	<input type="hidden" name="<?php echo $this->_vars['country_helper_data']['var_district_name']; ?>
" id="district_hidden_<?php echo $this->_vars['country_helper_data']['rand']; ?>
" value="<?php echo $this->_vars['country_helper_data']['district']['id']; ?>
">
</div>

<?php echo tpl_function_js(array('module' => countries,'file' => 'country-input.js'), $this);?>
<script type='text/javascript'>
<?php if ($this->_vars['country_helper_data']['var_js_name']): ?>var <?php echo $this->_vars['country_helper_data']['var_js_name']; ?>
;<?php endif; ?>
<?php echo '
$(function(){
'; ?>
var region_<?php echo $this->_vars['country_helper_data']['rand']; ?>
 = <?php echo 'new countryInput({
		siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
		rand: \'';  echo $this->_vars['country_helper_data']['rand'];  echo '\',
		id_country: \'';  echo $this->_vars['country_helper_data']['country']['code'];  echo '\',
		id_region: \'';  echo $this->_vars['country_helper_data']['region']['id'];  echo '\',
		id_city: \'';  echo $this->_vars['country_helper_data']['city']['id'];  echo '\',
		id_district: \'';  echo $this->_vars['country_helper_data']['district']['id'];  echo '\',
		';  if ($this->_vars['country_helper_data']['select_type']): ?>select_type: '<?php echo $this->_vars['country_helper_data']['select_type']; ?>
'<?php endif;  echo '
	});
});
'; ?>
</script>

<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-11 05:53:16 India Daylight Time */ ?>

<?php echo tpl_function_js(array('file' => 'jquery-ui.js'), $this);?>
<div class="divEstateAgents">
	<table class="tblEstateAgents"> 
    	<tr>
        	<td>
            	<div class="divTitle">
            		<div class="locCalcimg"><p class="agentTitle"><?php echo l('dyn_estate_agent_box_title', 'dynamic_blocks', '', 'text', array()); ?></p></div>
                </div>
            </td>
        </tr>
    	<tr>
        	<td class="tdlf">
            	<?php echo l('dyn_estate_agent_box_lbl_1', 'dynamic_blocks', '', 'text', array()); ?><br />
                <select name="selCity1" id="selCity1" onchange="onChangeCity(this.value,1)">
                	<option value=""><?php echo l('dyn_estate_agent_box_choose_city', 'dynamic_blocks', '', 'text', array()); ?></option>
                    <?php if (is_array($this->_vars['city1']) and count((array)$this->_vars['city1'])): foreach ((array)$this->_vars['city1'] as $this->_vars['key'] => $this->_vars['city']): ?>
                        <option value="<?php echo $this->_vars['city']['city']; ?>
"><?php echo $this->_vars['city']['name']; ?>
</option>
                    <?php endforeach; endif; ?>
                </select>
                <select name="selUser1" id="selUser1" style="display:none;" onchange="onChangeUser(this.value,1)">
                	<option value=""><?php echo l('dyn_estate_agent_box_choose_agent', 'dynamic_blocks', '', 'text', array()); ?></option>
                </select>
            </td>
        </tr>
        <tr>
        	<td class="tdlf">
            	<?php echo l('dyn_estate_agent_box_lbl_2', 'dynamic_blocks', '', 'text', array()); ?><br />
                <select name="selCity2" id="selCity2" onchange="onChangeCity(this.value,2)">
                	<option value=""><?php echo l('dyn_estate_agent_box_choose_city', 'dynamic_blocks', '', 'text', array()); ?></option>
                    <?php if (is_array($this->_vars['city2']) and count((array)$this->_vars['city2'])): foreach ((array)$this->_vars['city2'] as $this->_vars['key'] => $this->_vars['citys']): ?>
                        <option value="<?php echo $this->_vars['citys']['city']; ?>
"><?php echo $this->_vars['citys']['name']; ?>
</option>
                    <?php endforeach; endif; ?>
                </select>
                 <select name="selUser2" id="selUser2" style="display:none;" onchange="onChangeUser(this.value,2)">
                	<option value=""><?php echo l('dyn_estate_agent_box_choose_agent', 'dynamic_blocks', '', 'text', array()); ?></option>
                </select>
            </td>
        </tr>
        <tr>
        	<td class="tdlf">
            	<?php echo '
            	<script type="text/javascript">
					$(function() {
						var getData = function (request, response) {
							$.ajax({
								url: \'';  echo $this->_vars['site_root'];  echo 'content/ajax_get_extendusersearch/\' + request.term,
								data: {},
								dataType: "json",
								type: "POST",
								success: function (data) {
									response($.map(data, function (item) {
										return {
											label: item.name,
											val: item.id,
											user: item.usr
										}
									}))
								}
							});
						};
						
						$("#agents_available").autocomplete({
							source: getData,
							select: function(event, ui) {
								if(ui.item.user == null){
								window.location.href = \'';  echo $this->_vars['site_root'];  echo 'agents/\'+ui.item.val;
								}else{
								window.location.href = \'';  echo $this->_vars['site_root'];  echo 'users/view/\'+ui.item.user;
								}
							},
							minLength: 1
						});
					});
            </script>
            '; ?>

            	<?php echo l('dyn_estate_agent_box_searchagent', 'dynamic_blocks', '', 'text', array()); ?> <br />
                <input type="text" class="txtboxs" name="location" id="agents_available" value="" /> 
            </td>
        </tr>
    </table>
</div>
<?php echo '
<script>
	function onChangeUser(userId,type)
	{
		if(type == 1){
		window.location.href = \'';  echo $this->_vars['site_root'];  echo 'users/view/\'+userId;
		}else{
		window.location.href = \'';  echo $this->_vars['site_root'];  echo 'agents/\'+userId;
		}
	}
	
	function onChangeCity(cityId, type)
	{
		if(cityId != \'\')
		{
			$.ajax({
				url: \'';  echo $this->_vars['site_root'];  echo 'content/ajax_get_extenduser/\' + cityId + \'/\' + type,
				dataType: \'json\',
				type: \'GET\',
				data: {},
				cache: false,
				success: function(data){
					$(\'#selCity\' + type).css(\'display\',\'none\');
					$(\'#selUser\' + type).css(\'display\',\'block\');
					 $("#selUser1").attr(\'size\',30).css(\'height\',\'100px\');
					 $("#selUser2").attr(\'size\',30).css(\'height\',\'100px\');
					 
					$(\'#selUser\' + type).find("option:gt(0)").remove();
					if(type == 1){
					for(var id in data){
						$(\'#selUser\' + type).append(\'<option value="\'+data[id].pg_user_id+\'">\'+data[id].name+\'</option>\');
					}
					}else{
						for(var id in data){
						$(\'#selUser\' + type).append(\'<option value="\'+data[id].id_user
						+\'">\'+data[id].name+\'</option>\');
					}
					}
				}
			});
		}
		else
		{
			$(\'#selUser\' + type).css(\'display\',\'none\');
		}
	}
</script>
'; ?>

<script><?php echo '
$(function(){
	$("#selCity2").children("[value=\'0\']").remove();
});
'; ?>
</script>

<?php /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-11 05:53:19 India Daylight Time */ ?>

<input type="hidden" name="<?php echo $this->_vars['sb_input']; ?>
" class="myhid<?php echo $this->_vars['sb_default']; ?>
" id="<?php echo $this->_vars['sb_id']; ?>
" value="<?php echo $this->_vars['sb_selected']; ?>
">
<?php if ($this->_vars['sb_formtype'] == 'IndexSearch'): ?>
<div class="selectBox" id="<?php echo $this->_vars['sb_id']; ?>
_box">
	<div class="label" id="<?php echo $this->_vars['sb_default']; ?>
"><?php echo $this->_run_modifier($this->_vars['sb_default'], 'default', 'plugin', 1, '&nbsp;'); ?>
</div><div class="arrow"></div>
	<div class="data"><ul><?php if ($this->_vars['sb_default']): ?><li gid=""><?php echo $this->_vars['sb_default']; ?>
</li><?php endif; ?>
		<?php if (is_array($this->_vars['sb_value']) and count((array)$this->_vars['sb_value'])): foreach ((array)$this->_vars['sb_value'] as $this->_vars['key'] => $this->_vars['item']): ?>
			
			<li gid="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['sb_subvalue']): ?>class="group"<?php endif; ?>><?php echo $this->_vars['item']; ?>
</li>
			<?php if ($this->_vars['sb_subvalue']): ?>
			<?php if (is_array($this->_vars['sb_subvalue'][$this->_vars['key']]) and count((array)$this->_vars['sb_subvalue'][$this->_vars['key']])): foreach ((array)$this->_vars['sb_subvalue'][$this->_vars['key']] as $this->_vars['subkey'] => $this->_vars['subitem']): ?>
			<li gid="<?php echo $this->_vars['key']; ?>
_<?php echo $this->_vars['subkey']; ?>
" class="sub"><label><input type="checkbox" name="chkIndexMainCategory[]" value="<?php echo $this->_vars['key']; ?>
_<?php echo $this->_vars['subkey']; ?>
_<?php echo $this->_vars['subitem']; ?>
" /><?php echo $this->_vars['subitem']; ?>
</label></li>
			<?php endforeach; endif; ?>
			<?php endif; ?>
		<?php endforeach; endif; ?>
	</ul></div>
</div>
<?php else: ?>
<div class="selectBox" id="<?php echo $this->_vars['sb_id']; ?>
_box">
	<div class="label"><?php echo $this->_run_modifier($this->_vars['sb_default'], 'default', 'plugin', 1, '&nbsp;'); ?>
</div><div class="arrow"></div>
	<div class="data"><ul><?php if ($this->_vars['sb_default']): ?><li gid=""><?php echo $this->_vars['sb_default']; ?>
</li><?php endif; ?>
		<?php if (is_array($this->_vars['sb_value']) and count((array)$this->_vars['sb_value'])): foreach ((array)$this->_vars['sb_value'] as $this->_vars['key'] => $this->_vars['item']): ?>
			
			<li gid="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['sb_subvalue']): ?>class="group"<?php endif; ?>><?php echo $this->_vars['item']; ?>
</li>
			<?php if ($this->_vars['sb_subvalue']): ?>
			<?php if (is_array($this->_vars['sb_subvalue'][$this->_vars['key']]) and count((array)$this->_vars['sb_subvalue'][$this->_vars['key']])): foreach ((array)$this->_vars['sb_subvalue'][$this->_vars['key']] as $this->_vars['subkey'] => $this->_vars['subitem']): ?>
			<li gid="<?php echo $this->_vars['key']; ?>
_<?php echo $this->_vars['subkey']; ?>
" class="sub"><?php echo $this->_vars['subitem']; ?>
</li>
			<?php endforeach; endif; ?>
			<?php endif; ?>
		<?php endforeach; endif; ?>
	</ul></div>
</div>
<?php endif; ?>
<script><?php echo '
	$(function(){
	var alle = $(\'.myhidBoligtype\').val();
	var alla = $(\'.myhidBostadstyp\').val();
    if(alle == \'\'){
	$("#Boligtype").text("Alle");
	}
	if(alla == \'\'){
	$("#Bostadstyp").text("Alla");
	}
	});
	'; ?>
</script>
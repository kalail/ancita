<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.seolink.php'); $this->register_function("seolink", "tpl_function_seolink");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.block.php'); $this->register_function("block", "tpl_function_block");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\block.capture.php'); $this->register_block("capture", "tpl_block_capture");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.math.php'); $this->register_function("math", "tpl_function_math");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date.php'); $this->register_modifier("date", "tpl_modifier_date");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.ld.php'); $this->register_function("ld", "tpl_function_ld");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.date_format.php'); $this->register_modifier("date_format", "tpl_modifier_date_format");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-02-08 11:00:10 India Standard Time */ ?>

<div class="content-block load_content">
	<h1><?php echo l('header_order_add', 'listings', '', 'text', array()); ?></h1>
	<div class="inside edit_block">
		<form method="post" action="<?php echo $this->_run_modifier($this->_vars['lisitng']['action'], 'escape', 'plugin', 1); ?>
" name="save_form" id="order_form" enctype="multipart/form-data">
			<div class="r fleft">
				<div class="f"><?php echo l('field_booking_date_start', 'listings', '', 'text', array()); ?>:</div>
				<div class="v periodbox">
											<?php switch($this->_vars['listing']['price_period']): case '1':  ?>
					<input type="text" name="period[date_start]" value="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['current_date_start'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']), 'escape', 'plugin', 1); ?>
" id="date_start<?php echo $this->_vars['rand']; ?>
" class="middle" onfocus="this.removeAttribute('readonly');" readonly>
							<input type="hidden" name="date_start_alt" value="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['current_date_start'], 'date_format', 'plugin', 1, '%Y-%m-%d'), 'escape', 'plugin', 1); ?>
" id="alt_date_start<?php echo $this->_vars['rand']; ?>
">
							<script><?php echo '
							   $(function(){
							   $(\'#date_start';  echo $this->_vars['rand'];  echo '\').datepicker({
							   dateFormat: \'';  echo $this->_vars['page_data']['datepicker_date_format'];  echo '\', 
							   altFormat: \'yy-mm-dd\',
							   altField: \'#alt_date_start';  echo $this->_vars['rand'];  echo '\',
                               minDate:  0,
                               onSelect: function(date){            
                               var date1 = $(\'#date_start';  echo $this->_vars['rand'];  echo '\').datepicker(\'getDate\');           
                               var date = new Date( Date.parse( date1 ) ); 
                               date.setDate( date.getDate() + 0 );        
                               var newDate = date.toDateString(); 
                               newDate = new Date( Date.parse( newDate ) );                      
                               $(\'#date_end';  echo $this->_vars['rand'];  echo '\').datepicker("option","minDate",newDate);            
                               }
                               });
							   });
							'; ?>
</script>
						<?php break; case '2':  ?>
							<?php echo tpl_function_ld(array('i' => 'month-names','gid' => 'start','assign' => 'month_names'), $this);?>
							<select name="date_start_month" class="middle">
								<option value=""><?php echo $this->_vars['month_names']['header']; ?>
</value>
								<?php if (is_array($this->_vars['month_names']['option']) and count((array)$this->_vars['month_names']['option'])): foreach ((array)$this->_vars['month_names']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['key'] == $this->_vars['current_month_start']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
								<?php endforeach; endif; ?>
							</select>
							<?php $this->assign('cyear', $this->_run_modifier('now', 'date', 'plugin', 1, 'Y')); ?>
							<select name="date_start_year" class="short">
								<option value=""><?php echo l('text_year', 'listings', '', 'text', array()); ?></option>
								<?php for($for1 = 0; ((0 < 10) ? ($for1 < 10) : ($for1 > 10)); $for1 += ((0 < 10) ? 1 : -1)):  $this->assign('i', $for1); ?>
								<?php echo tpl_function_math(array('equation' => 'x + y','x' => $this->_vars['cyear'],'y' => $this->_vars['i'],'assign' => 'year'), $this);?>
								<option value="<?php echo $this->_vars['year']; ?>
" <?php if ($this->_vars['year'] == $this->_vars['current_year_start']): ?>selected<?php endif; ?>><?php echo $this->_vars['year']; ?>
</option>
								<?php endfor; ?>
							</select>
					<?php break; endswitch; ?>
				</div>
			</div>
			<div class="r fleft">&nbsp;</div>
			<div class="r fleft">
				<div class="f"><?php echo l('field_booking_date_end', 'listings', '', 'text', array()); ?>:</div>
				<div class="v periodbox">
											<?php switch($this->_vars['listing']['price_period']): case '1':  ?>
							<input type="text" name="period[date_end]" value="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['current_date_end'], 'date_format', 'plugin', 1, $this->_vars['page_data']['date_format']), 'escape', 'plugin', 1); ?>
" id="date_end<?php echo $this->_vars['rand']; ?>
" class="middle" onfocus="this.removeAttribute('readonly');" readonly>
							<input type="hidden" name="date_end_alt" value="<?php echo $this->_run_modifier($this->_run_modifier($this->_vars['current_date_end'], 'date_format', 'plugin', 1, '%Y-%m-%d'), 'escape', 'plugin', 1); ?>
" id="alt_date_end<?php echo $this->_vars['rand']; ?>
">
							<script><?php echo '
								$(function(){
									$(\'#date_end';  echo $this->_vars['rand'];  echo '\').datepicker({dateFormat: \'';  echo $this->_vars['page_data']['datepicker_date_format'];  echo '\', altFormat: \'yy-mm-dd\', altField: \'#alt_date_end';  echo $this->_vars['rand'];  echo '\', showOn: \'both\'});
								});
							'; ?>
</script>
						<?php break; case '2':  ?>
							<?php echo tpl_function_ld(array('i' => 'month-names','gid' => 'start','assign' => 'month_names'), $this);?>
							<select name="date_end_month" class="middle">
								<option value=""><?php echo $this->_vars['month_names']['header']; ?>
</value>
								<?php if (is_array($this->_vars['month_names']['option']) and count((array)$this->_vars['month_names']['option'])): foreach ((array)$this->_vars['month_names']['option'] as $this->_vars['key'] => $this->_vars['item']): ?>
								<option value="<?php echo $this->_vars['key']; ?>
" <?php if ($this->_vars['key'] == $this->_vars['current_month_end']): ?>selected<?php endif; ?>><?php echo $this->_vars['item']; ?>
</option>
								<?php endforeach; endif; ?>
							</select>
							<?php $this->assign('cyear', $this->_run_modifier('now', 'date', 'plugin', 1, 'Y')); ?>
							<select name="date_end_year" class="short">
								<option value=""><?php echo l('text_year', 'listings', '', 'text', array()); ?></option>
								<?php for($for1 = 0; ((0 < 10) ? ($for1 < 10) : ($for1 > 10)); $for1 += ((0 < 10) ? 1 : -1)):  $this->assign('i', $for1); ?>
								<?php echo tpl_function_math(array('equation' => 'x + y','x' => $this->_vars['cyear'],'y' => $this->_vars['i'],'assign' => 'year'), $this);?>
								<option value="<?php echo $this->_vars['year']; ?>
" <?php if ($this->_vars['year'] == $this->_vars['current_year_end']): ?>selected<?php endif; ?>><?php echo $this->_vars['year']; ?>
</option>
								<?php endfor; ?>
							</select>
					<?php break; endswitch; ?>
				</div>
			</div>
			<div class="r clr">
				
			</div>
			<div class="r">
				<div class="f"><?php echo l('field_booking_comment', 'listings', '', 'text', array()); ?>:</div>
				<div class="v" id="period_comment">
					<textarea name="period[comment]" rows="10" cols="80"><?php echo $this->_run_modifier($this->_vars['order']['comment'], 'escape', 'plugin', 1); ?>
</textarea>
				</div>
			</div>
		
			<?php if ($this->_vars['show_price']): ?>
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'booking_price')); tpl_block_capture(array('assign' => 'booking_price'), null, $this); ob_start(); ?>
				<?php if ($this->_vars['current_price'] > 0): ?>
					<?php echo tpl_function_block(array('name' => 'currency_format_output','module' => 'start','value' => $this->_vars['current_price'],'cur_gid' => $this->_vars['listing']['gid_currency']), $this);?>
				<?php else: ?>
					<?php echo l('text_negotiated_price_rent', 'listings', '', 'text', array()); ?>
				<?php endif; ?>
			<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			<div class="r booking_price"><?php echo l('text_price_total', 'listings', '', 'text', array()); ?>: <span class="booking_price_value"><?php echo $this->_vars['booking_price']; ?>
</span></div>
			<?php endif; ?>
			
			<div class="calendar_link hide fright">
				<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['listing'],'section' => 'calendar'), $this);?>#m_calendar"><?php echo l('link_calendar_view', 'listings', '', 'text', array()); ?></a>
			</div>

			<div class="b">
			     <input type="hidden" name="name" value="<?php echo $this->_vars['user']['name']; ?>
">
				 <input type="hidden" name="mail" value="<?php echo $this->_vars['user']['contact_email']; ?>
">
				<input type="submit" name="btn_save" value="<?php echo l('btn_save', 'start', '', 'button', array()); ?>" id="close_btn">
			</div>
		</form>
	</div>
</div>

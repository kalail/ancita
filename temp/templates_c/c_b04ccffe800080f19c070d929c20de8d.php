<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.counter.php'); $this->register_function("counter", "tpl_function_counter");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\compiler.l.php'); $this->register_compiler("l", "tpl_compiler_l");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2016-10-17 07:49:00 India Daylight Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<table cellspacing="0" cellpadding="0" class="data" width="100%">
<tr>
	<th class="first w150"><?php echo l('field_title', 'weather', '', 'text', array()); ?></th>
	<th class="w50"><?php echo l('field_status', 'weather', '', 'text', array()); ?></th>
	
	<th class="w50"><?php echo l('field_link', 'weather', '', 'text', array()); ?></th>
	<th class="w70">&nbsp;</th>
</tr>
<?php if (is_array($this->_vars['drivers']) and count((array)$this->_vars['drivers'])): foreach ((array)$this->_vars['drivers'] as $this->_vars['item']):  echo tpl_function_counter(array('print' => false,'assign' => counter), $this);?>
<tr<?php if (!($this->_vars['counter'] % 2)): ?> class="zebra"<?php endif; ?>>
	<td class="first"><?php echo $this->_vars['item']['name']; ?>
</td>
	<td class="center">
		<?php if ($this->_vars['item']['status']): ?>
		<img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-full.png" width="16" height="16" border="0" alt="<?php echo l('link_driver_active', 'weather', '', 'button', array()); ?>" title="<?php echo l('link_driver_active', 'weather', '', 'button', array()); ?>">
		<?php else: ?>
		<a href="<?php echo $this->_vars['site_url']; ?>
admin/weather/activate/<?php echo $this->_vars['item']['gid']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-empty.png" width="16" height="16" border="0" alt="<?php echo l('link_driver_activate', 'weather', '', 'button', array()); ?>" title="<?php echo l('link_driver_activate', 'weather', '', 'button', array()); ?>"></a>
		<?php endif; ?>
	</td>
	
	<td class="center">
		<?php if ($this->_vars['item']['auto_locations']): ?>
		<a href="<?php echo $this->_vars['item']['link']; ?>
" target="_blank">
			<?php if ($this->_vars['item']['need_regkey']): ?>
				<?php echo l('driver_registration', 'weather', '', 'text', array()); ?>
			<?php else: ?>
				<?php echo l('driver_info', 'weather', '', 'text', array()); ?>
			<?php endif; ?>
		</a>
		<?php else: ?>
		&nbsp;
		<?php endif; ?>
	</td>
	<td class="icons">
		<?php if (! $this->_vars['item']['auto_location']): ?><a href="<?php echo $this->_vars['site_url']; ?>
admin/weather/locations/<?php echo $this->_vars['item']['gid']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-settings.png" width="16" height="16" border="0" alt="<?php echo l('link_driver_locations', 'weather', '', 'button', array()); ?>" title="<?php echo l('link_driver_locations', 'weather', '', 'button', array()); ?>"></a><?php endif; ?>
		<?php if ($this->_vars['item']['editable']): ?><a href="<?php echo $this->_vars['site_url']; ?>
admin/weather/edit/<?php echo $this->_vars['item']['gid']; ?>
"><img src="<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
icon-edit.png" width="16" height="16" border="0" alt="<?php echo l('link_driver_edit', 'weather', '', 'button', array()); ?>" title="<?php echo l('link_driver_edit', 'weather', '', 'button', array()); ?>"></a><?php endif; ?>
	</td>
</tr>
<?php endforeach; else: ?>
<tr><td colspan="4" class="center"><?php echo l('no_drivers', 'weather', '', 'text', array()); ?></td></tr>
<?php endif; ?>
</table>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "pagination.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
  $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

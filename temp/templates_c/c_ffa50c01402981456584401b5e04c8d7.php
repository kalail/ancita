<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.js.php'); $this->register_function("js", "tpl_function_js");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\modifier.escape.php'); $this->register_modifier("escape", "tpl_modifier_escape");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-01-19 06:59:36 India Standard Time */ ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "header.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->module_path. start. $this->module_templates.  $this->get_current_theme_gid('', 'start'). "left_panel.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>

<div class="rc">
	<div class="content-block">
		<?php $this->assign('header_my_agents', 'header_my_agents_'.$this->_vars['action']); ?>
		<h1><?php echo l($this->_vars['header_my_agents'], 'users', '', 'text', array()); ?></h1>

		<div class="search-links">
			<?php echo l('link_search_in_my_agents', 'users', '', 'text', array()); ?>
			<div class="edit_block">
				<form action="" method="post" enctype="multipart/form-data" id="agent_search_form">
					<div class="r"><input type="text" name="name" value="<?php echo $this->_run_modifier($this->_vars['page_data']['name'], 'escape', 'plugin', 1); ?>
" id="agent_search" autocomplete="off"></div>
				</form>
			</div>
		</div>

		<div id="search_my_agents_form" class="hide"></div>

		<div id="agents_block"><?php echo $this->_vars['block']; ?>
</div>
		<?php echo tpl_function_js(array('module' => users,'file' => 'users-list.js'), $this);?>
		<script type='text/javascript'><?php echo '
		$(function(){
			new usersList({
				siteUrl: \'';  echo $this->_vars['site_root'];  echo '\',
				viewUrl: \'users/agents/';  echo $this->_vars['action'];  echo '\',
				viewAjaxUrl: \'users/ajax_my_agents/';  echo $this->_vars['action'];  echo '\',
				tIds: [\'pages_block_1\', \'pages_block_2\', \'sorter_block\'],
				listBlockId: \'agents_block\'
			});
		});
	'; ?>
</script>
	</div>
</div>
<div class="clr"></div>
<?php $_templatelite_tpl_vars = $this->_vars;
echo $this->_fetch_compile_include( $this->general_path.  $this->get_current_theme_gid('', ''). "footer.tpl", array());
$this->_vars = $_templatelite_tpl_vars;
unset($_templatelite_tpl_vars);
 ?>



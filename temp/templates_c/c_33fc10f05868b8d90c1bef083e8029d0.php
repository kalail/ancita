<?php require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.pagination.php'); $this->register_function("pagination", "tpl_function_pagination");  require_once('C:\xampp\htdocs\ancita\system\libraries\template_lite\plugins\function.sorter.php'); $this->register_function("sorter", "tpl_function_sorter");  /* V2.10 Template Lite 4 January 2007  (c) 2005-2007 Mark Dickenson. All rights reserved. Released LGPL. 2017-04-07 13:20:03 India Daylight Time */ ?>

	<?php if ($this->_vars['listings']): ?>
	<div class="sorter line" id="sorter_block">
		<?php echo tpl_function_sorter(array('links' => $this->_vars['sort_data']['links'],'order' => $this->_vars['sort_data']['order'],'direction' => $this->_vars['sort_data']['direction'],'url' => $this->_vars['sort_data']['url']), $this);?>
		<div class="fright" id="pages_block_1"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'cute'), $this);?></div>
	</div>
	<?php endif; ?>

	<div>
	<?php if (is_array($this->_vars['listings']) and count((array)$this->_vars['listings'])): foreach ((array)$this->_vars['listings'] as $this->_vars['item']): ?>
	<div class="listing-block <?php if ($this->_vars['item']['is_highlight']): ?>highlight<?php endif; ?>">
		<div id="item-block-<?php echo $this->_vars['item']['id']; ?>
" class="item listing <?php if ($this->_vars['item']['is_highlight']): ?>highlight<?php endif; ?>">
			<!--<h3><a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>" title="<?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['item']['output_name'], 'truncate', 'plugin', 1, 50); ?>
</a></h3>-->
            <h3 style="font-size:18px;"><a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>" title="<?php echo $this->_run_modifier($this->_vars['item']['city'], 'escape', 'plugin', 1); ?>
"><?php echo $this->_run_modifier($this->_vars['item']['city'], 'truncate', 'plugin', 1, 50); ?>
</a></h3>
			<div class="image">
				<?php 
$this->assign('logo_title', l('link_listing_view', 'listings', '', 'button', array('id_ref'=>$this->_vars['item']['id'])));
 ?>
				<?php 
$this->assign('text_listing_logo', l('text_listing_logo', 'listings', '', 'button', array('id_ref'=>$this->_vars['item']['id'],'property_type'=>$this->_vars['item']['property_type_str'],'operation_type'=>$this->_vars['item']['operation_type_str'],'location'=>$this->_vars['item']['location'])));
 ?>					
				<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>">
					<?php if ($this->_vars['animate_available'] && $this->_vars['item']['is_slide_show']): ?>
					<img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['middle_anim']; ?>
" alt="<?php echo $this->_vars['text_listing_logo']; ?>
" title="<?php echo $this->_vars['logo_title']; ?>
">
					<?php else: ?>
					<img src="<?php echo $this->_vars['item']['media']['photo']['thumbs']['middle']; ?>
" alt="<?php echo $this->_vars['text_listing_logo']; ?>
" title="<?php echo $this->_vars['logo_title']; ?>
">
					<?php endif; ?>
					<!--<?php if ($this->_vars['item']['photo_count'] || $this->_vars['item']['is_vtour']): ?>
					<div class="photo-info">
						<div class="panel">
							<?php if ($this->_vars['item']['photo_count']): ?><span><ins class="fa fa-camera w"></ins> <?php echo $this->_vars['item']['photo_count']; ?>
</span><?php endif; ?>
							<?php if ($this->_vars['item']['is_vtour']): ?><span><ins class="fa fa-rotate-left w"></ins> 360&deg;</span><?php endif; ?>
						</div>
						<div class="background"></div>
					</div>
					<?php endif; ?>-->
				</a>
			</div>
	
			<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'status_info')); tpl_block_capture(array('assign' => 'status_info'), null, $this); ob_start();  $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start(); ?>
				<?php if ($this->_vars['item']['is_lift_up'] || $this->_vars['location_filter'] == 'country' && $this->_vars['item']['is_lift_up_country'] || $this->_vars['location_filter'] == 'region' && ( $this->_vars['item']['is_lift_up_country'] || $this->_vars['item']['is_lift_up_region'] ) || $this->_vars['location_filter'] == 'city' && ( $this->_vars['item']['is_lift_up_country'] || $this->_vars['item']['is_lift_up_region'] || $this->_vars['item']['is_lift_up_city'] ) || $this->_vars['sort_data']['order'] != 'default' && ( $this->_vars['item']['is_lift_up'] || $this->_vars['item']['is_lift_up_country'] || $this->_vars['item']['is_lift_up_region'] || $this->_vars['item']['is_lift_up_city'] )): ?>
					<?php echo l('status_lift_up', 'listings', '', 'text', array()); ?>
				<?php elseif ($this->_vars['item']['is_featured']): ?>
					<?php echo l('status_featured', 'listings', '', 'text', array()); ?>
				<?php endif; ?>
			<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack);  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
			
			<div class="body">
            	<span style="color:#666;"><?php echo l('listings_id_text', 'listings', '', 'text', array()); ?>: <?php echo $this->_vars['item']['id']; ?>
</span>
				<br><br><h3 style="font-weight:bold;"><?php echo tpl_function_block(array('name' => listing_price_block,'module' => 'listings','data' => $this->_vars['item']), $this);?></h3>
                
				<div class="t-1">
					<?php echo $this->_vars['item']['property_type_str']; ?>
 <!--<?php echo $this->_vars['item']['operation_type_str']; ?>
-->
					<!--<br><?php echo $this->_run_modifier($this->_vars['item']['square_output'], 'truncate', 'plugin', 1); ?>
-->
					<?php if ($this->_run_modifier($this->_vars['item']['date_open'], 'strtotime', 'PHP', 1) >= $this->_vars['tstamp']): ?><br><?php echo l('text_date_open_status', 'listings', '', 'text', array());  endif; ?>
					<?php if ($this->_vars['item']['sold']): ?><br><span class="status_text"><?php echo l('text_sold', 'listings', '', 'text', array()); ?></span><?php endif; ?>
					<?php if ($this->_vars['status_info']): ?><br><span class="status_text"><?php echo $this->_vars['status_info']; ?>
</span><?php endif; ?>
				</div>
				<div class="t-2">
				</div>
				<?php if ($this->_vars['show_user_info']): ?>
				<div class="t-3">
					<!--<?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'truncate', 'plugin', 1, 20); ?>
-->
				</div>
                <div class="t-bedbath">
                	<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'lst_beds')); tpl_block_capture(array('assign' => 'lst_beds'), null, $this); ob_start(); ?>
                    <?php if ($this->_vars['item']['fe_bd_rooms_1'] >= '10'):  $this->assign('bedroom', "10+");  echo $this->_vars['bedroom'];  endif; ?>
                	<?php if ($this->_vars['item']['fe_bd_rooms_1'] < '10'):  echo $this->_vars['item']['fe_bd_rooms_1'];  endif; ?>
                    <?php if ($this->_vars['item']['fe_bd_rooms_4']):  echo $this->_vars['item']['fe_bd_rooms_4'];  endif;  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
                    <?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'lst_baths')); tpl_block_capture(array('assign' => 'lst_baths'), null, $this); ob_start(); ?>
                    <?php if ($this->_vars['item']['fe_bth_rooms_1'] >= '10'):  $this->assign('bathroom', "10+");  echo $this->_vars['bathroom'];  endif; ?>
                    <?php if ($this->_vars['item']['fe_bth_rooms_1'] < '10'):  echo $this->_vars['item']['fe_bth_rooms_1'];  endif; ?>
                    <?php if ($this->_vars['item']['fe_bth_rooms_4']):  echo $this->_vars['item']['fe_bth_rooms_4'];  endif; ?>
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
                    
                    <?php if ($this->_run_modifier($this->_vars['lst_beds'], 'trim', 'PHP', 1) != ''):  echo $this->_vars['lst_beds']; ?>
<img src='<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
bed.png'><br><?php endif; ?>
                    <?php if ($this->_run_modifier($this->_vars['lst_baths'], 'trim', 'PHP', 1) != ''):  echo $this->_vars['lst_baths']; ?>
<img src='<?php echo $this->_vars['site_root'];  echo $this->_vars['img_folder']; ?>
bath.png'><?php endif; ?>
                </div>
				<div class="t-4">
					<?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'user_logo')); tpl_block_capture(array('assign' => 'user_logo'), null, $this); ob_start(); ?>
						<?php if ($this->_vars['show_logo'] || $this->_vars['item']['user']['is_show_logo']): ?>
						<img src="<?php echo $this->_vars['item']['user']['media']['user_logo']['thumbs']['small']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'escape', 'plugin', 1); ?>
">
						<?php else: ?>
						<img src="<?php echo $this->_vars['item']['user']['media']['default_logo']['thumbs']['small']; ?>
" title="<?php echo $this->_run_modifier($this->_vars['item']['user']['output_name'], 'escape', 'plugin', 1); ?>
">
						<?php endif; ?>
					<?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
					<?php if ($this->_vars['item']['user']['status']): ?>
					<a href="<?php echo tpl_function_seolink(array('module' => 'users','method' => 'view','data' => $this->_vars['item']['user']), $this);?>"><?php echo $this->_vars['user_logo']; ?>
</a>
					<?php else: ?>
					<?php echo $this->_vars['user_logo']; ?>

					<?php endif; ?>
				</div>
				<?php endif; ?>
			</div>
			<div class="clr"></div>
			<!--<?php if ($this->_vars['item']['headline']): ?><p class="headline" title="<?php echo $this->_vars['item']['headline']; ?>
"><?php echo $this->_run_modifier($this->_vars['item']['headline'], 'truncate', 'plugin', 1, 100); ?>
</p><?php endif; ?>-->
            <?php $this->_tag_stack[] = array('tpl_block_capture', array('assign' => 'custm_listheading')); tpl_block_capture(array('assign' => 'custm_listheading'), null, $this); ob_start(); ?>
            <?php if ($this->_vars['item']['headline_lang'][$this->_vars['current_lang_id']] == '...' || $this->_vars['item']['headline_lang'][$this->_vars['current_lang_id']] == ''): ?>
				<?php echo $this->_vars['item']['headline_lang'][1];  else:  echo $this->_vars['item']['headline_lang'][$this->_vars['current_lang_id']]; ?>

			<?php endif; ?>
            <?php $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_capture($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>
            <?php if ($this->_vars['custm_listheading']):  if ($this->_vars['custm_listheading'] != '...'): ?><p class="headline"><?php echo $this->_run_modifier($this->_vars['custm_listheading'], 'truncate', 'plugin', 1, 150); ?>
</p><?php endif;  endif; ?>
			<?php echo tpl_function_block(array('name' => 'save_listing_block','module' => 'listings','listing' => $this->_vars['item'],'template' => 'link','separator' => '|'), $this);?>
			<?php echo tpl_function_block(array('name' => 'listings_booking_block','module' => 'listings','listing' => $this->_vars['item'],'template' => 'link','show_link' => 1,'show_price' => 1,'separator' => '|','no_save' => 1), $this);?>
			<a href="<?php echo tpl_function_seolink(array('module' => 'listings','method' => 'view','data' => $this->_vars['item']), $this);?>"><?php echo l('link_details', 'listings', '', 'text', array()); ?></a>
		</div>
	</div>
	<?php endforeach; else: ?>
	<div class="item empty"><?php echo l('no_listings', 'listings', '', 'text', array()); ?></div>
	<?php endif; ?>
	</div>
	<?php if ($this->_vars['listings']): ?><div id="pages_block_2"><?php echo tpl_function_pagination(array('data' => $this->_vars['page_data'],'type' => 'full'), $this);?></div><?php endif; ?>

	<?php if ($this->_vars['update_map']): ?>
	<?php echo tpl_function_block(array('name' => update_default_map,'module' => geomap,'markers' => $this->_vars['markers'],'map_id' => 'listings_map_container'), $this);?>
	<?php endif; ?>
	
	<?php if ($this->_vars['update_operation_type']): ?>
	<script><?php echo '
	$(function(){
		var operation_type = $(\'#operation_type\');
		if(operation_type.val() != \'';  echo $this->_vars['update_operation_type'];  echo '\')
			$(\'#operation_type\').val(\'';  echo $this->_vars['update_operation_type'];  echo '\').trigger(\'change\');
	});
	'; ?>
</script>
	<?php endif; ?>

	<script><?php echo '
	$(function(){
		';  if ($this->_vars['update_search_block']): ?>$('#search_filter_block').html('<?php $this->_tag_stack[] = array('tpl_block_strip', array()); tpl_block_strip(array(), null, $this); ob_start();  echo $this->_vars['search_filters_block'];  $this->_block_content = ob_get_contents(); ob_end_clean(); $this->_block_content = tpl_block_strip($this->_tag_stack[count($this->_tag_stack) - 1][1], $this->_block_content, $this); echo $this->_block_content; array_pop($this->_tag_stack); ?>');<?php endif;  echo '
		$(\'#total_rows\').html(\'';  echo $this->_vars['page_data']['total_rows'];  echo '\');
	});
	'; ?>
</script>


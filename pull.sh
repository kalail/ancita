#!/bin/sh
BASEDIR=$(dirname $0)
PROJECT_PATH=$(cd $BASEDIR; pwd)
echo "find ${PROJECT_PATH} -name '.hg' -type d | sed 's/.hg//g'"
FILES=$(find ${PROJECT_PATH} -name ".hg" -type d | sed 's/.hg//g')

for FILE in ${FILES}
do
(
	cd ${FILE}
	hg pull
	hg update 
)
done